<?php
include "core/function_servicio.php";

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$intKey = isset($_GET["iden"]) ? fntCoreDecrypt($_GET["iden"]) : 0;
$strLenguaje = isset($_GET["len"]) ? trim($_GET["len"]) : "es";

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
    
if( intval($intKey) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $strQuery = "SELECT place_producto.id_place_producto,
                        place_producto.nombre,
                        place_producto.descripcion,
                        place_producto.precio,
                        place_producto.logo_url,
                        place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        place.id_place,
                        place.titulo
                 FROM   place,
                        place_producto
                            LEFT JOIN place_producto_clasificacion_padre_hijo
                                ON  place_producto.id_place_producto = place_producto_clasificacion_padre_hijo.id_place_producto
                        
                 WHERE  place_producto.id_place_producto = {$intKey}
                 AND    place.id_place = place.id_place  
                 
                 ";
    $intPlace = 0;
    $arrProducto = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrProducto["id_place_producto"] = $rTMP["id_place_producto"];
        $arrProducto["nombre"] = $rTMP["nombre"];
        $arrProducto["descripcion"] = $rTMP["descripcion"];
        $arrProducto["precio"] = $rTMP["precio"];
        $arrProducto["logo_url"] = $rTMP["logo_url"];
        $arrProducto["id_place"] = $rTMP["id_place"];
        $arrProducto["titulo"] = $rTMP["titulo"];
        
        if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
            
            $arrProducto["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            
        }
        
        $intPlace = $rTMP["id_place"]; 
            
    }
    
    $objDBClass->db_free_result($qTMP);
    
    
    $arrPlaceProducto = array();
    //Otros Productos
    $strQuery = "SELECT place.id_place,
                        place.titulo,
                        place.url_logo,
                        place_producto.id_place_producto,
                        place_producto.nombre,
                        place_producto.descripcion,
                        place_producto.precio,
                        place_producto.logo_url
                 FROM   place,
                        place_producto
                 WHERE  place.id_place = {$intPlace}
                 AND    place.id_place = place_producto.id_place
                 LIMIT 30
                 ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPlaceProducto[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
        $arrPlaceProducto[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
        $arrPlaceProducto[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
        $arrPlaceProducto[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
        $arrPlaceProducto[$rTMP["id_place_producto"]]["id_place"] = $rTMP["id_place"];
        $arrPlaceProducto[$rTMP["id_place_producto"]]["titulo"] = $rTMP["titulo"];
        $arrPlaceProducto[$rTMP["id_place_producto"]]["url_logo"] = $rTMP["url_logo"];
        $arrPlaceProducto[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
                    
    }
    $objDBClass->db_free_result($qTMP);
    
    
    
}    

    
?>
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/lister/rtl/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Mar 2019 21:46:35 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Lister</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
        <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
        
        <link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
        <link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> <!-- for live demo page -->
        <link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> <!-- for live demo page -->
        <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
    </head>
    <body>
        <style>
            
            .row {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-right: -15px;
                margin-left: -15px;
                margin: 0px;
                padding: 0px;
            }
            .InputdirectionLTR {
                direction: ltr;
                width: 100%;
            }
            
            .grediant-bt-dark:before {
                background: #dc3545 !important;
            }
            
            .text-color-global {
                color: black !important;
            }
            
            .cardNoBackground {
                background-color: transparent; border: 0px
            }
            
            .cursor_pointer {
                cursor: pointer;
            }
            
            .lis-id-info {
              background-color: #ff214f;
              color: white;
              width: 36px;
              height: 36px;
            line-height: 38px; 
            }
              .lis-id-info:hover {
                  opacity: 0.7;
                  color: white ;
                background-color: #ff214f; }
            
            .lis-id-info-sm {
              background-color: #ff214f;
              color: white;
              width: 26px;
              height: 26px;
              font-size: 14px;
            line-height: 28px; 
            }
              .lis-id-info-sm:hover {
                  opacity: 0.7;
                  color: white ;
                background-color: #ff214f; }
        
        
                   
            .select2-dropdown {
                background-color: white;
                border: 1px solid #aaa;
                border-radius: 4px;
                box-sizing: border-box;
                display: block;
                position: absolute;
                left: -100000px;
                width: 100%;
                z-index: 999999999999999;
                border: 2px solid red;
            }
            
            .bg-delete{
                background-color: #FFC0C0 !important;
            }
                
            .shadow_mia{
                box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.41);
            }
            
            
      
            .slideImgActiva {
                
                padding: 5px 0px 0px 0px ;
                border-radius: 20px !important;
                
                box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
                -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
                -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
                
            }
                
            
            .slideImgNoActiva {
                
                border-radius: 20px !important;
                padding: 10px 10px 10px 10px ;
                border-radius: 20px !important;
                
            }
            .slideImgNoActivaIMG {
                
                border-radius: 20px !important;
                
                box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);   
                -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);  
                -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
                
            }
            
            #lean_overlay {
                position: fixed;
                z-index: 1500;
                top: 0px;
                left: 0px;
                height: 100%;
                width: 100%;
                background: #000;
                display: none;
            }
            
            .popover {
                position: absolute;
                top: 0;
                left: 0;
                z-index: 9999999999999999999999999999;
                display: block;
                max-width: 276px;
                padding: 1px;
                text-align: left;
                text-align: start;
                background-color: #fff;
                border: 1px solid rgba(0,0,0,.2);
                border-radius: .3rem;
            }
            
            
            .transperant nav.navbar-toggleable-md.navbar-light {
                background: #dc3545;
                border: 0px;
            }
            
            #header-fix.nav-affix .navbar {
              background-color: #dc3545;
              /*
              box-shadow: 0 10px 20px -12px rgba(0, 0, 0, 0.42), 0 3px 20px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
              border-color: #000;
              */
              transition: 0.3s; }
            
            /*
            .login_form{
                display: none !important;
            }
            
            
            .slick-current {
                background: yellow !important; 
            }
            */
        </style>
        <!-- header -->
        <div id="header-fix" class="header fixed-top transperant  p-0">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-1" style="direction: ltr; ">
                <a class="navbar-brand mr-4 mr-md-5" href="index.html">
                    <img src="dist/images/logo_inguate.png" style="width: 134px; height: 40px;" alt="">
                </a> 

                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right text-right" style=""  >
                    <button class="btn btn-white float-right"  style="color: white; background: #dc3545; font-weight: bold; font-size: 20px;"><i class="fa fa-reorder "></i></button>
                    <ul class="dl-menu">

                        <li class="nav-item active dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Home</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li><a href="index.html">Home 1</a></li>
                                <li><a href="index-2.html">Home 2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Explore</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li><a href="listing-explore-place-profile.html">Explore Place</a></li>
                                <li><a href="listing-explore-event-profile.html">Explore Event</a></li>
                                <li><a href="listing-explore-property-profile.html">Explore Property</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Listing</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing By Categories</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="listing-categories-style1.html">Category 1</a></li>
                                        <li><a href="listing-categories-style2.html">Category 2</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing No Map</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="listing-no-map-sidebar.html">With Sidebar</a></li>
                                        <li><a href="listing-no-map-fullwidth.html">Full Width</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Half Screen Map</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back bg-dark"><a href="#">back</a></li>
                                        <li><a href="half-screen-search-sidebar-place.html">With Search Place</a></li>
                                        <li><a href="half-screen-search-sidebar-event.html">With Search Event</a></li>
                                        <li><a href="half-screen-search-sidebar-property.html">With Search Property</a></li>
                                        <li><a href="half-screen-with-categories-sidebar.html">With Categories Sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Blog</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back bg-dark"><a href="#">back</a></li>
                                        <li><a href="blog-grid.html"> Blog Grid</a></li>
                                        <li><a href="blog-listing.html"> Blog Listing</a></li>
                                        <li><a href="blog-single.html"> Single Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"> Contact</a></li>
                                <li><a href="user-profile.html"> User Profile</a></li>
                                <li><a href="faq.html"> Faq</a></li>
                                <li><a href="pricing.html"> Pricing Table</a></li>
                                <li><a href="coming-soon.html"> Coming Soon</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">404 Error</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back bg-dark"><a href="#">back</a></li>
                                        <li><a href="error-dark.html"> 404 Error Dark</a></li>
                                        <li><a href="error-light.html"> 404 Error Light</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>


                        <li><a href="#modal" class="text-white login_form"><i class="fa fa-sign-in pr-2"></i> Sign In | Register</a></li>

                        <li> <a href="add-listing.html" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>



                    </ul>
                </div>

                <div class="collapse navbar-collapse d-none d-lg-block" id="navbarNav" style="height: 50px;">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Home <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html">Home 1</a></li>
                                <li><a href="index-2.html">Home 2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Explore <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="listing-explore-place-profile.html">Explore Place</a></li>
                                <li><a href="listing-explore-event-profile.html">Explore Event</a></li>
                                <li><a href="listing-explore-property-profile.html">Explore Property</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Listing <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing By Categories</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="listing-categories-style1.html">Category 1</a></li>
                                        <li><a href="listing-categories-style2.html">Category 2</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing No Map</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="listing-no-map-sidebar.html">With Sidebar</a></li>
                                        <li><a href="listing-no-map-fullwidth.html">Full Width</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Half Screen Map</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="half-screen-search-sidebar-place.html">With Search Place</a></li>
                                        <li><a href="half-screen-search-sidebar-event.html">With Search Event</a></li>
                                        <li><a href="half-screen-search-sidebar-property.html">With Search Property</a></li>
                                        <li><a href="half-screen-with-categories-sidebar.html">With Categories Sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Blog</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="blog-grid.html"> Blog Grid</a></li>
                                        <li><a href="blog-listing.html"> Blog Listing</a></li>
                                        <li><a href="blog-single.html"> Single Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"> Contact</a></li>
                                <li><a href="user-profile.html"> User Profile</a></li>
                                <li><a href="faq.html"> Faq</a></li>
                                <li><a href="pricing.html"> Pricing Table</a></li>
                                <li><a href="coming-soon.html"> Coming Soon</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">404 Error</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="error-dark.html"> 404 Error Dark</a></li>
                                        <li><a href="error-light.html"> 404 Error Light</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="list-unstyled my-2 my-lg-0">
                        <li class="text-white"><i class="fa fa-sign-in pl-2"></i> <a href="#modal" class="text-white login_form">Sign In | Register</a></li>
                    </ul>
                    <a href="add-listing.html" class="btn btn-outline-light btn-sm mr-0 mr-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a>
                </div>
            </nav>                    
        </div>
        <!--End header -->
     

        <!-- Categories -->
        <input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intKey);?>">
    
        
        <div class="row justify-content-center  p-0 m-0" >
            <div class="col-12 text-dark" style="margin-top: 60px; direction: ltr;">
                
                <?php
                
                //drawdebug($arrProducto);
                
                ?>
                
                
                <div class="row justify-content-center">
                
                    <div class="col-12">
                        <h3>
                            <?php print $arrProducto["titulo"]?>
                        </h3>
                    </div>
                    
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-xs-12 ">
                                
                        <img src="<?php print $arrProducto["logo_url"]?>" style="" class="img-fluid d-md-fle  lis-border-width-4 rounded m-0" alt="" />
                    
                    </div>
                    <div class="col-lg-4 col-xs-12 ">
                        
                        <ul class="list-unstyled my-0 pr-0">
                            <li class="lis-font-weight-600 m-0  <?php print $boolMovil ? "h5" : "h3"?>">
                                <a href="#" class="lis-dark">
                                    <?php print $arrProducto["nombre"]?>
                                </a>
                            </li>
                            <li class="text-secondary mb-2" style="font-size: <?php print $boolMovil ? "13px" : "18px"?> ; line-height: 1.1;">
                                <?php print $arrProducto["titulo"]?>
                            </li>
                            <li class=" " style="font-size: <?php print $boolMovil ? "14px" : "19px"?>; line-height: 1.2;">
                                <?php print $arrProducto["descripcion"]?>
                            </li>
                            <li class=" <?php print $boolMovil ? "" : "h5"?>" style=" line-height: 1.2;">
                                Q<?php print number_format($arrProducto["precio"], 2)?>
                            </li>
                        </ul>                                
                    
                    </div>
                </div>
                <div class="row justify-content-center">
                
                    <div class="col-12">
                        <h3>
                            Other products
                        </h3>
                    </div>
                    
                </div>
                <div class="row justify-content-center">
                
                    <div class="col-12">
                        <?php
                        
                        fntDrawSearchTableProducto($arrPlaceProducto, $boolMovil);
                        
                        ?>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        <div  id="divContendo">
        </div>
            
        <section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image">
                <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none">
            </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Dashboard</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Profile</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> My Listing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Favorites</A></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <h5 class="footer-head">Pages</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <h5 class="footer-head">Help</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a> 
                        </div>
                        <p class="my-4">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu in  felis eu pede mollis enim.</p>
                        <a href="#" class="text-white"><u>App Download</u></a> 
                        <ul class="list-inline mb-0 mt-2">
                            <li class="list-inline-item"><A href="#"><img src="dist/images/play-store.png" alt="" class="img-fluid" /></a></li>
                            <li class="list-inline-item"><A href="#"><img src="dist/images/google-play.png" alt="" class="img-fluid" /></a></li>
                            <li class="list-inline-item"><A href="#"><img src="dist/images/windows.png" alt="" class="img-fluid" /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom mt-5 py-4">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 text-center text-md-right mb-3 mb-md-0">
                            <span> &copy; 2017 Lister. Powered by <a href="#" class="lis-primary">PSD2allconversion.</a></span>
                        </div> 
                        <div class="col-12 col-md-6 text-center text-md-left">
                            <ul class="list-inline footer-social mb-0">
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-tumblr"></i></a></li>
                                <li class="list-inline-item"><A href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>
        </section>
        
        
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50"> 
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>


        <style>
            
            .classMenuAutocomplete {
                padding: 10px;
            }
            
        </style>
        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>
        <script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
        <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
       
        <script >
            $(document).ready(function() { 
                
                
                
            });
                  
            
        </script>
    </body>

<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/lister/rtl/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Mar 2019 21:49:26 GMT -->
</html>