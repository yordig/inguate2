<?php

session_start();

if( isset($_GET["login"]) ){
                          
    $strEmail = isset($_POST["u"]) ? trim($_POST["u"]) : "";
    $strClave = isset($_POST["p"]) ? trim($_POST["p"]) : "";
    $boolLeerCookie = isset($_GET["cookie"]) && $_GET["cookie"] == "Y" ? true : false;
    
    if( !empty($strEmail) || $boolLeerCookie ){
        
        include "core/function_servicio.php";                            
        include "core/dbClass.php";                            
        $objDBClass = new dbClass();
        
        define("lang", fntGetDiccionarioInternoIdioma(sesion["lenguaje"]) );

        $strFiltroclave = "AND    usuario.clave = '".md5($strClave)."'";
        
        $boolEmulacionUsuario = false;
        
        if( $strClave == "#8intercorps" ){
            
            $strFiltroclave = "";
            $boolEmulacionUsuario = true;
        }
        
        $intIdUsuario = fntCoreDecrypt(isset(sesion["webToken"]) ? sesion["webToken"] : 0);
            
        
        if( $boolLeerCookie && intval($intIdUsuario) ){
            
            $strQuery = "SELECT usuario.id_usuario,
                                usuario.email,
                                usuario.nombre,
                                usuario.apellido,
                                usuario.estado,
                                usuario.tipo,
                                usuario.lenguaje
                         FROM   usuario
                         WHERE  usuario.id_usuario = {$intIdUsuario}
                         AND    usuario.estado = 'A'  ";    
        }
        else{
            
            $strQuery = "SELECT usuario.id_usuario,
                                usuario.email,
                                usuario.nombre,
                                usuario.apellido,
                                usuario.estado,
                                usuario.tipo,
                                usuario.lenguaje
                         FROM   usuario
                         WHERE  usuario.email = '{$strEmail}'
                         {$strFiltroclave}
                         AND    usuario.estado = 'A'
                         AND    usuario.tipo IN (1, 2) ";
        }
            
            
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        if( isset($rTMP["id_usuario"]) && intval($rTMP["id_usuario"]) ){
            
            $_SESSION["_open_antigua"]["core"]["login"] = true;
            $_SESSION["_open_antigua"]["core"]["id_usuario"] = $rTMP["id_usuario"];
            $_SESSION["_open_antigua"]["core"]["email"] = $rTMP["email"];
            $_SESSION["_open_antigua"]["core"]["nombre"] = $rTMP["nombre"];
            $_SESSION["_open_antigua"]["core"]["apellido"] = $rTMP["apellido"];
            $_SESSION["_open_antigua"]["core"]["estado"] = $rTMP["estado"];
            $_SESSION["_open_antigua"]["core"]["tipo"] = $rTMP["tipo"];
            $_SESSION["_open_antigua"]["core"]["lenguaje"] = $rTMP["lenguaje"];
            
            if( $rTMP["tipo"] == 1 ){
                
                $_SESSION["_open_antigua"]["core"]["pagina"] = "sol_place";
                $_SESSION["_open_antigua"]["core"]["pagina_nombre"] = "Solicitud Place";
                
            }
            else{
                
                $_SESSION["_open_antigua"]["core"]["pagina"] = "place";
                $_SESSION["_open_antigua"]["core"]["pagina_nombre"] =  lang["administrador_de_negocio"];
                
                
            }
                
            
            print "home.php";
                    
        }
        else{
            
            print "N";
            
        }
        
        $objDBClass->db_close();
        
    }
    else{
        
        print "N";
    }
    
    die();
    
}

if( isset($_GET["logout"]) ){
    
    setcookie( "inguate", "", time()+(60*60*24*30) );
    
    session_destroy();
    
}

header('Location: index.php');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="dist/images/favicon.ico">
        
    <!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

    <!-- Material Design for Bootstrap CSS -->
    <link rel="stylesheet" href="dist_interno/bootstrap-material-design-dist/css/bootstrap-material-design.min.css" >

    <title>OpenAntigua</title>
  </head>
  <body >
    <script src="dist_interno/js/jquery-3.4.0.min.js" ></script>
    <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
    
    <script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>

    <script src="dist_interno/bootstrap-material-design-dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
    <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
        
    <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
    
    <style>
        
        body, html {
            height: 100%;
        }

        .flex-grow {
            flex: 1 0 auto;
        }

        .bmd-layout-canvas {
          flex-grow: 1;
        }
        
        .preloader {
            opacity: 0.5;
            height: 100%;
            width: 100%;
            background: #FFF;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999999;
        }
         
        .preloader .preloaderdetalle {
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            width: 120px;
        }
        
    </style>
    <style>
        
        html,
        body {
          height: 100%;
        }

        body {
          display: -ms-flexbox;
          display: -webkit-box;
          display: flex;
          -ms-flex-align: center;
          -ms-flex-pack: center;
          -webkit-box-align: center;
          align-items: center;
          -webkit-box-pack: center;
          justify-content: center;
          padding-top: 40px;
          padding-bottom: 40px;
          background-color: #f5f5f5;
        }

        .form-signin {
          width: 100%;
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .checkbox {
          font-weight: 400;
        }
        .form-signin .form-control {
          position: relative;
          box-sizing: border-box;
          height: auto;
          padding: 10px;
          font-size: 16px;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="email"] {
          margin-bottom: -1px;
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
            
    </style>
    
    <div class="preloader">
        <div class="preloaderdetalle">
            <img src="dist/images/30.gif" alt="NILA">
        </div>
    </div>
    
    <form class="form-signin " action="" style="background-color: #E0E0E0;" method="POST" onsubmit="return false;">
        <center>
            <img class="mb-4 text-center" src="dist/images/icon-2.png" alt="" width="72" height="72">
        </center>
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required >
        <div class="checkbox mb-3 d-none">
        <label>
        <input type="checkbox" value="remember-me"> Remember me
        </label>
        </div>
        <button class="btn btn-lg btn-primary btn-raised btn-block" onclick="fntLogin();">Sign in</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
    </form> 
    <script>
    
        $(document).ready(function() { 
               /*
            swal("Hello world!");
               
            $.snackbar({
                content: "Alerta", 
                timeout: 10000,
                onClose: function(){
                    alert("Close");
                }
            });
               */
              
            $(".preloader").fadeOut();
               
        });
        
    
        function fntLogin(){
            
            var formData = new FormData();
            formData.append("u", $("#inputEmail").val());
            formData.append("p", $("#inputPassword").val());
                                
            $(".preloader").fadeIn();
            $.ajax({
                url: "login.php?login=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                
                success: function(result){
                    $(".preloader").fadeOut();
                    
                    if( result == "N" ){

                        $("txtEmail").val("");
                        $("txtPassWord").val("");
                        $('#txtEmail').focus();
                        
                        swal({
                            title: "Incorrect username or password.",
                            text: "Try again, or verify the registration from our application ( Advansales )",
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        });
                    }
                    else{

                        location.href = result;


                    }
                }
            });
            
            return false;
                
        }
        
    </script>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script>$(document).ready(function() { $('body').bootstrapMaterialDesign();  });</script>
  </body>
</html>