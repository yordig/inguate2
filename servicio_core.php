<?php  
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
die();
}

try {

    if( isset($_GET["servicio"])  ){
        
        $strServicio = trim($_GET["servicio"]);
                       
        if( $strServicio == "drawContenidoPaginaCore" ){
            
            include "core/core.php";
            
            $strPagina = isset($_GET["pagina"]) ? trim($_GET["pagina"]) : 0;
            $strNombrePagina = isset($_GET["nombre"]) ? trim($_GET["nombre"]) : 0;
            $boolContenido = isset($_GET["boolContenido"]) && $_GET["boolContenido"] == "true" ? true : false;
            
            $_SESSION["_open_antigua"]["core"]["pagina"] = $strPagina;
            $_SESSION["_open_antigua"]["core"]["pagina_nombre"] = $strNombrePagina;
                
            
            if( $boolContenido ){
                
                if( $strPagina == "homeI" ){
                    
                    include "clases/admin_inicio/admin_inicio_controller.php";
                    $objController = new admin_inicio_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                }
                
                if( $strPagina == "registro" ){
                    
                    include "clases/registro/registro_controller.php";
                    $objController = new registro_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                }  
                
                if( $strPagina == "place" ){
                    
                    include "clases/place/place_controller.php";
                    $objController = new place_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                }   
                
                if( $strPagina == "categoria" ){
                    
                    include "clases/categoria/categoria_controller.php";
                    $objController = new categoria_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                } 
                
                if( $strPagina == "amenidad" ){
                    
                    include "clases/amenidad/amenidad_controller.php";
                    $objController = new amenidad_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                }                
                
                if( $strPagina == "sol_place" ){
                    
                    include "clases/sol_place/sol_place_controller.php";
                    $objController = new sol_place_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                } 
                
                if( $strPagina == "categoria_especial" ){
                    
                    include "clases/categoria_especial/categoria_especial_controller.php";
                    $objController = new categoria_especial_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                }                
                
                if( $strPagina == "sol_categoria_especial" ){
                    
                    include "clases/sol_categoria_especial/sol_categoria_especial_controller.php";
                    $objController = new sol_categoria_especial_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                }                
                
                if( $strPagina == "lugar_dato" ){
                    
                    include "clases/lugar_dato/lugar_dato_controller.php";
                    $objController = new lugar_dato_controller();
                    
                    $objController->drawContentPage("home.php");
                    
                }                
                
            }
            
            die();
            
        }
        
        if( $strServicio == "setProductoFavorito" ){
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            
            $intIdProducto = isset($_GET["key"]) ? fntCoreDecrypt($_GET["key"]) : "";
            $boolFavorito = isset($_GET["fav"]) && $_GET["fav"] == "Y" ? true : false;
            $intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
            
            if( intval($intIdUsuario) ){
                
                $objDBClass = new dbClass();
                
                if( $boolFavorito ){
                    
                    $strQuery = "INSERT INTO usuario_place_producto_favorito(id_usuario, id_place_producto)
                                                                    VALUES({$intIdUsuario}, {$intIdProducto})";  
                    $objDBClass->db_consulta($strQuery);         
                
                }
                else{
                    
                    $strQuery = "DELETE FROM usuario_place_producto_favorito WHERE id_usuario = {$intIdUsuario} AND id_place_producto = {$intIdProducto}   ";
                     
                    $objDBClass->db_consulta($strQuery);               
                }
                
                $objDBClass->db_close();
                
                
            }
                    
            die();            
        }
        
        if( $strServicio == "setLogInRegistroPConfirmacion" ){
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            $objDBClass = new dbClass();
            
            $intIdUsuario = isset($_POST["hidKeyUsuario"]) ? trim(fntCoreDecrypt($_POST["hidKeyUsuario"])) : "";
            $strCodigoConfirmacion = isset($_POST["txtCodigoConfirmacion"]) ? trim(fntCoreClearToQuery($_POST["txtCodigoConfirmacion"])) : "";
            $strNuevoTelefono = isset($_POST["txtTelefonoNuevoEnvio"]) ? addslashes(fntCoreClearToQuery($_POST["txtTelefonoNuevoEnvio"])) : "";
            $boolReenvio = isset($_POST["reenvio"]) && $_POST["reenvio"] == "true" ? true : false;
            $boolCambioTelefonoReenvio = isset($_POST["hidCambioTelefono"]) && $_POST["hidCambioTelefono"] == "Y" ? true : false;
            
            $arr["reenvio"] = false;
            $arr["reenvio_telefono_nuevo"] = false;
            
            if( $boolCambioTelefonoReenvio ){
                
                $strQuery = "UPDATE usuario
                             SET    telefono = '{$strNuevoTelefono}'    
                             WHERE  id_usuario = {$intIdUsuario} ";
                
                $strQuery = "UPDATE usuario
                             SET    email = '{$strNuevoTelefono}'    
                             WHERE  id_usuario = {$intIdUsuario} ";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "DELETE FROM usuario_codigo_sms WHERE id_usuario = {$intIdUsuario} ";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "INSERT INTO usuario_codigo_sms(id_usuario, codigo)
                                                     VALUES( {$intIdUsuario}, SUBSTRING(MD5(RAND()) FROM 1 FOR 4) )";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "SELECT codigo
                             FROM   usuario_codigo_sms
                             WHERE  id_usuario = {$intIdUsuario}  ";    
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $strTexto = "Codigo de Confirmacion Inguate: ".$rTMP["codigo"];   
                fntEnvioCodigoConfirmacionUsuarioEmail($strNuevoTelefono, $strTexto);
                
                $arr["error"] = false;
                $arr["reenvio"] = true;
                $arr["reenvio_telefono_nuevo"] = true;
                $arr["reenvio_telefono_nuevo_telefono"] = $strNuevoTelefono;
                $arr["msn"] = "Codigo de Confirmacion Enviado.";
                    
                print json_encode($arr);
                die();
                
            }
                        
            if( $boolReenvio ){
                
                
                $strQuery = "SELECT telefono, email
                             FROM   usuario
                             WHERE  id_usuario = {$intIdUsuario}  ";    
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $strTelefono = $rTMP["telefono"]; 
                $strEmail = $rTMP["email"]; 
                
                $strQuery = "DELETE FROM usuario_codigo_sms WHERE id_usuario = {$intIdUsuario} ";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "INSERT INTO usuario_codigo_sms(id_usuario, codigo)
                                                     VALUES( {$intIdUsuario}, SUBSTRING(MD5(RAND()) FROM 1 FOR 4) )";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "SELECT codigo
                             FROM   usuario_codigo_sms
                             WHERE  id_usuario = {$intIdUsuario}  ";    
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $strTexto = "Codigo de Confirmacion Inguate: ".$rTMP["codigo"];
                //fntEnvioCodigoConfirmacionUsuario($strTelefono, $strTexto);   
                fntEnvioCodigoConfirmacionUsuarioEmail($strEmail, $strTexto);
                
                $arr["error"] = false;
                $arr["reenvio"] = true;
                $arr["msn"] = "Codigo de Confirmacion Enviado.";
                    
                print json_encode($arr);
                die();
                
            }  
            
            $strQuery = "SELECT id_usuario
                         FROM   usuario_codigo_sms
                         WHERE  codigo = '{$strCodigoConfirmacion}'  " ;
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intIdUsuario = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
            
            $arr["error"] = true;
            $arr["msn"] = "Codigo de Confirmacion Incorrecto.";
            
            if( $intIdUsuario ){
                
                $strQuery = "UPDATE usuario
                             SET    estado = 'A'
                             WHERE  id_usuario = {$intIdUsuario}  ";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "SELECT *
                             FROM   usuario
                             WHERE  id_usuario = {$intIdUsuario} ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $arr["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                $arr["inguate"]["nombre"] = $rTMP["nombre"];     
                $arr["inguate"]["tipoSesion"] = $rTMP["tipo_registro"];     
                $arr["inguate"]["tipoUsuario"] = $rTMP["tipo"];     
                $arr["inguate"]["lenguaje"] = $rTMP["lenguaje"];     
                $arr["inguate"]["ubicacion"] = $rTMP["ubicacion"];     
                
                $strCookie = implode(",", $arr["inguate"]);
                setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                
                //fntEnviarCorreo("bryant@ritzycapital.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"])
                fntEnviarCorreo("bryant@ritzycapital.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"]);
                
                $arr["error"] = false;
                $arr["msn"] = "";
                    
            }
            
            print json_encode($arr);
            die();
            
        }
        
        if( $strServicio == "setLogInRegistroP" ){
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            $objDBClass = new dbClass();
            
            $strTipoRegistro = isset($_POST["txtTipoLogIn"]) ? trim($_POST["txtTipoLogIn"]) : "";
            $strRegistro = isset($_POST["txtRegistro"]) ? trim($_POST["txtRegistro"]) : "";
            $strIdentificador = isset($_POST["txtIdentificador"]) ? trim($_POST["txtIdentificador"]) : "";
            $strUrlFB = isset($_POST["txtPictureUrl"]) ? trim($_POST["txtPictureUrl"]) : "";
            $strAction = isset($_POST["txtAction"]) ? trim($_POST["txtAction"]) : "";
            $strLenguaje = isset($_POST["slcIdiomaModalRegistro"]) ? trim($_POST["slcIdiomaModalRegistro"]) : "es";
            $intUbicacion = isset($_POST["ubicacion"]) ? trim($_POST["ubicacion"]) : "1";
            $intPlace = isset($_POST["hidKeyRegistroCore"]) ? intval(fntCoreDecrypt($_POST["hidKeyRegistroCore"])) : "0";
            
            $strEmail = isset($_POST["txtEmail"]) ? trim($_POST["txtEmail"]) : "";
            $strClave = isset($_POST["txtClave"]) ? trim($_POST["txtClave"]) : "";
            $strNombre = isset($_POST["txtNombre"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtNombre"])) : "";
            $strApellido = isset($_POST["txtApellido"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtApellido"])) : "";
            $strTelefono = isset($_POST["txtTelefono"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtTelefono"])) : "";
            $boolSaveSession = isset($_POST["chkSaveSession"]) ? true : false;
            
            $arr["error"] = "true";
            $arr["msn"] = "Datos Incorrectos, completa todos los campos";
            $arr["error_telefono_registro"] = "false";
            
            if( $strTipoRegistro == "E"  && empty($strEmail)  ){
                
                
                print json_encode($arr);            
                die();     
            }
            
            if( $strRegistro == "Y"  && ( empty($strEmail) || empty($strNombre) || empty($strTelefono) )  ){
                
                print json_encode($arr);            
                die();     
            }
            
            if( ( $strTipoRegistro == "G" || $strTipoRegistro == "F" ) && empty($strEmail) && empty($strTelefono) ){
                
                $arr["error_telefono_registro"] = "true";                                            
                print json_encode($arr);            
                die();     
            }
            
            $arr["error"] = "true";
            $arr["telefonoEnvio"] = "";
            $arr["emailEnvio"] = "";
            $arr["msn"] = "";
            $arr["sms_pendiente"] = "false";
        
            $boolInsert = true;
            $boolLogInGF = false;
            $boolConfirmacionSMS = false;
            
            $strQuery = "SELECT *
                         FROM   usuario
                         WHERE  email = '{$strEmail}' ";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intTipo = 3;
            
            if( isset($rTMP["id_usuario"]) && intval($rTMP["id_usuario"]) ){
                
                $intIdUsuario = $rTMP["id_usuario"];
                $strNombre = $rTMP["nombre"];
                $strTipoRegistro = $rTMP["tipo_registro"];
                $intTipo = $rTMP["tipo"];
                $strEstadoUsuario = $rTMP["estado"];
                
                $boolInsert = false;
                
                if( $strTipoRegistro == "G" || $strTipoRegistro == "F" ){
                    
                    $boolLogInGF = true;
                    if( $strEstadoUsuario == "P" ){
                        
                        $arr["error"] = "true";
                        $arr["sms_pendiente"] = "true";
                        $arr["u_key"] = fntCoreEncrypt($intIdUsuario);
                        $arr["msn"] = "Usuario No Confirmado por SMS";
                        print json_encode($arr);            
                        die();    
                            
                        
                    }
                        
                }
                elseif( $strTipoRegistro == "E" && $strRegistro == "N" ){
                    
                    if( $strEstadoUsuario == "A" ){
                        
                        if( md5($strClave) == $rTMP["clave"] ){
                            
                            $boolLogInGF = true;
                        
                        }
                        else{
                            
                            $arr["error"] = "true";
                            $arr["msn"] = "Usuario o Clave Incorrectos";
                            print json_encode($arr);            
                            die();
                        }
                        
                    }   
                    else{
                        
                        $arr["error"] = "true";
                        $arr["sms_pendiente"] = "true";
                        $arr["msn"] = "Usuario No Confirmado por SMS";
                        print json_encode($arr);            
                        die();    
                        
                    }                   
                    
                        
                    
                                                
                }
                elseif( $strRegistro == "Y" ){
                    
                    $arr["error"] = "true";
                    $arr["msn"] = "Usuario Registrado anteriormente";
                    print json_encode($arr);            
                    die();
                    
                }
                
                            
            }
            elseif( $strTipoRegistro == "E" && $strRegistro == "N" ){
                
                $arr["error"] = "true";
                $arr["msn"] = "Usuario No Registrado";
                print json_encode($arr);            
                die();
                    
            }
            
            if( $boolInsert ){
                
                $strClave = !empty($strClave) ? md5($strClave) : "";
                $strNombre = $strNombre." ".$strApellido;
                $strApellido = "";
                $strQuery = "INSERT INTO usuario(email, clave, nombre, apellido, 
                                                 estado, tipo, add_fecha, lenguaje, telefono, tipo_registro,identificador )
                                          VALUES('{$strEmail}', '{$strClave}', '{$strNombre}', '{$strApellido}',
                                                 'P', '{$intTipo}', SYSDATE(), '{$strLenguaje}', '{$strTelefono}', '{$strTipoRegistro}', '{$strIdentificador}')  ";
                $objDBClass->db_consulta($strQuery);
                
                $intIdUsuario = intval($objDBClass->db_last_id());
                
                if( $intPlace ){
                    
                    $strQuery = "INSERT INTO place_usuario_registro(id_place, id_usuario)
                                                             VALUES({$intPlace}, {$intIdUsuario})";
                    $objDBClass->db_consulta($strQuery);
                }
                
                if( !empty( $strUrlFB ) ){
                    
                    $ch = curl_init($strUrlFB);
                    $fp = fopen("../../file_inguate/usuario_perfil/".fntCoreEncrypt($intIdUsuario).'.png', 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);

                    
                }
                
                $boolLogInGF = false;
                $boolConfirmacionSMS = true;
                
                $strQuery = "INSERT INTO usuario_codigo_sms(id_usuario, codigo)
                                                     VALUES( {$intIdUsuario}, SUBSTRING(MD5(RAND()) FROM 1 FOR 4) )";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "SELECT codigo
                             FROM   usuario_codigo_sms
                             WHERE  id_usuario = {$intIdUsuario}  ";    
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $strTexto = "Codigo de Confirmacion Inguate: ".$rTMP["codigo"];
                //fntEnvioCodigoConfirmacionUsuario($strTelefono, $strTexto);
                fntEnvioCodigoConfirmacionUsuarioEmail($strEmail, $strTexto);
                
                $arr["telefonoEnvio"] = $strTelefono;
                $arr["emailEnvio"] = $strEmail;
                
            }
            
            if( intval($intIdUsuario) && $boolLogInGF ){
                
                
                $arrC["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                $arrC["inguate"]["nombre"] = $strNombre;     
                $arrC["inguate"]["tipoSesion"] = $strTipoRegistro;     
                $arrC["inguate"]["tipoUsuario"] = $intTipo;     
                $arrC["inguate"]["lenguaje"] = $strLenguaje;     
                $arrC["inguate"]["ubicacion"] = $intUbicacion;     
                
                $strCookie = implode(",", $arrC["inguate"]);
                setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                 
                 
                $arr["error"] = "false";
                $arr["logIn"] = "true";
                $arr["href"] = $strAction;
                                    
            }
                
            $arr["confirmacion_sms"] = $boolConfirmacionSMS;
            $arr["u_key"] = fntCoreEncrypt($intIdUsuario);
                
            
            print json_encode($arr);            
            die();        
            
        }
        
        if( $strServicio == "setLogInRegistroPIndex" ){
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            $objDBClass = new dbClass();
            
            $strTipoRegistro = isset($_POST["txtTipoLogIn"]) ? trim($_POST["txtTipoLogIn"]) : "";
            $intTipoRegistro = isset($_POST["txtTipoRegistro"]) ? intval($_POST["txtTipoRegistro"]) : "";
            $strRegistro = isset($_POST["txtRegistro"]) ? trim($_POST["txtRegistro"]) : "";
            $strIdentificador = isset($_POST["txtIdentificador"]) ? trim($_POST["txtIdentificador"]) : "";
            $strUrlFB = isset($_POST["txtPictureUrl"]) ? trim($_POST["txtPictureUrl"]) : "";
            $strAction = isset($_POST["txtAction"]) ? trim($_POST["txtAction"]) : "";
            $strLenguaje = isset($_POST["lenguaje"]) ? trim($_POST["lenguaje"]) : "es";
            $intUbicacion = isset($_POST["ubicacion"]) ? trim($_POST["ubicacion"]) : "1";
            $strTokenRecap = isset($_POST["hidTokenCap"]) ? trim($_POST["hidTokenCap"]) : "";
            
            $strEmail = isset($_POST["txtEmail"]) ? trim($_POST["txtEmail"]) : "";
            $strClave = isset($_POST["txtClave"]) ? trim($_POST["txtClave"]) : "";
            $strNombre = isset($_POST["txtNombre"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtNombre"])) : "";
            $strApellido = isset($_POST["txtApellido"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtApellido"])) : "";
            $strTelefono = isset($_POST["txtTelefono"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtTelefono"])) : "";
            $boolSaveSession = isset($_POST["chkSaveSession"]) ? true : false;
            
            $arr["error"] = "true";
            $arr["msn"] = "Datos Incorrectos, completa todos los campos";
            
            
            if( $strTipoRegistro != "F"  ){
                
                //Lo primerito, creamos una variable iniciando curl, pasándole la url
                $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
                 
                //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
                curl_setopt ($ch, CURLOPT_POST, 1);
                 
                //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
                curl_setopt ($ch, CURLOPT_POSTFIELDS, "secret=6LfJ7agUAAAAAOt0iGbsPjudsKSUX6PHiK1qq6ek&response=".$strTokenRecap);
                 
                //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                 
                //recogemos la respuesta
                $crulrespuesta = curl_exec ($ch);
                
                 
                //o el error, por si falla
                $error = curl_error($ch);
                 
                //y finalmente cerramos curl
                curl_close ($ch);
                
                $arrRespuesta = json_decode($crulrespuesta);
                
                if( !$arrRespuesta->success ){
                    
                    $arr["msn"] = "Datos Incorrectos, validacion reCAPTCHA invalida.";
                
                    print json_encode($arr);            
                    die();         
                    
                }
                    
            }
            
            if( $strTipoRegistro == "E"  && empty($strEmail)  ){
                
                
                print json_encode($arr);            
                die();     
            }
            
            if( $strRegistro == "Y"  && ( empty($strEmail) || empty($strNombre) || ( empty($strTelefono) && $strTipoRegistro != "F"  ) )  ){
                
                print json_encode($arr);            
                die();     
            }
            
            if( ( $strTipoRegistro == "G" || $strTipoRegistro == "F" ) && empty($strEmail) ){
                
                print json_encode($arr);            
                die();     
            }
            
            $arr["error"] = "true";
            $arr["msn"] = "";
        
            $boolInsert = true;
            $boolLogInGF = false;
            
            $strQuery = "SELECT *
                         FROM   usuario
                         WHERE  email = '{$strEmail}' ";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intTipo = $intTipoRegistro;
            
            if( isset($rTMP["id_usuario"]) && intval($rTMP["id_usuario"]) ){
                
                $intIdUsuario = $rTMP["id_usuario"];
                $strNombre = $rTMP["nombre"];
                $strTipoRegistro = $rTMP["tipo_registro"];
                $intTipo = $rTMP["tipo"];
                
                $boolInsert = false;
                
                if( $strTipoRegistro == "G" || $strTipoRegistro == "F" ){
                    
                    $boolLogInGF = true;
                        
                }
                elseif( $strTipoRegistro == "E" && $strRegistro == "N" ){
                    
                    if( md5($strClave) == $rTMP["clave"] ){
                        
                        $boolLogInGF = true;
                    
                    }
                    else{
                        
                        $arr["error"] = "true";
                        $arr["msn"] = "Usuario o Clave Incorrectos";
                        print json_encode($arr);            
                        die();
                    }
                    
                                                
                }
                elseif( $strRegistro == "Y" ){
                    
                    $arr["error"] = "true";
                    $arr["msn"] = "Usuario Registrado anteriormente";
                    print json_encode($arr);            
                    die();
                    
                }
                
                            
            }
            elseif( $strTipoRegistro == "E" && $strRegistro == "N" ){
                
                $arr["error"] = "true";
                $arr["msn"] = "Usuario No Registrado";
                print json_encode($arr);            
                die();
                    
            }
            
            if( $boolInsert ){
                
                $strClave = !empty($strClave) ? md5($strClave) : "";
                $strNombre = $strNombre." ".$strApellido;
                $strApellido = "";
                $strQuery = "INSERT INTO usuario(email, clave, nombre, apellido, 
                                                 estado, tipo, add_fecha, lenguaje, telefono, tipo_registro,identificador )
                                          VALUES('{$strEmail}', '{$strClave}', '{$strNombre}', '{$strApellido}',
                                                 'A', '{$intTipo}', SYSDATE(), '{$strLenguaje}', '{$strTelefono}', '{$strTipoRegistro}', '{$strIdentificador}')  ";
                $objDBClass->db_consulta($strQuery);
                
                $intIdUsuario = $objDBClass->db_last_id();
                
                if( !empty( $strUrlFB ) ){
                    
                    $ch = curl_init($strUrlFB);
                    $fp = fopen("../../file_inguate/usuario_perfil/".fntCoreEncrypt($intIdUsuario).'.png', 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);

                    
                }
                
                
                $arr[2] = "NEGOCIO";
                $arr[3] = "USUARIO NORMAL";
                $arr[4] = "INFLUENCER";
                                
                fntEnviarCorreo("nextbars@gmail.com", "Nuevo Registro", "{$strEmail} - {$strNombre} - {$strApellido} - {$strTelefono} se ha registrado:, es de tipo: {$arr[$intTipo]} ");
                
                $boolLogInGF = true;
                
            }
            
            if( intval($intIdUsuario) && $boolLogInGF ){
                
                
                $arr["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                $arr["inguate"]["nombre"] = $strNombre;     
                $arr["inguate"]["tipoSesion"] = $strTipoRegistro;     
                $arr["inguate"]["tipoUsuario"] = $intTipo;     
                $arr["inguate"]["lenguaje"] = $strLenguaje;     
                $arr["inguate"]["ubicacion"] = $intUbicacion;     
                
                $strCookie = implode(",", $arr["inguate"]);
                setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                 
                 
                $arr["error"] = "false";
                $arr["logIn"] = "true";
                $arr["href"] = $strAction;
                                    
            }
                
            
            print json_encode($arr);            
            die();        
            
        }
        
        if( $strServicio == "setPlaceFavorito" ){
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            
            $intIdProducto = isset($_GET["key"]) ? fntCoreDecrypt($_GET["key"]) : "";
            $boolFavorito = isset($_GET["fav"]) && $_GET["fav"] == "Y" ? true : false;
            $intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
            
            if( intval($intIdUsuario) ){
                
                $objDBClass = new dbClass();
                
                if( $boolFavorito ){
                    
                    $strQuery = "INSERT INTO usuario_place_favorito(id_usuario, id_place)
                                                                    VALUES({$intIdUsuario}, {$intIdProducto})";  
                    $objDBClass->db_consulta($strQuery);         
                
                }
                else{
                    
                    $strQuery = "DELETE FROM usuario_place_favorito WHERE id_usuario = {$intIdUsuario} AND id_place = {$intIdProducto}   ";
                     
                    $objDBClass->db_consulta($strQuery);               
                }
                
                $objDBClass->db_close();
                
            }
                
                
            die();            
        }
        
        if( $strServicio == "setUsuarioNegocio" ){
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            
            $arrCookie = fntGetCookieSesion();
            $objDBClass = new dbClass();
            $intIdUsuario = fntCoreDecrypt($arrCookie["webToken"]);
            
            $strQuery = "UPDATE usuario     
                         SET    tipo = '2',
                                id_usuario_place_restrinccion = '1'
                         WHERE  id_usuario = {$intIdUsuario} ";
            $objDBClass->db_consulta($strQuery);
            $objDBClass->db_close();
            
            $arr["inguate"]["webToken"] = $arrCookie["webToken"];     
            $arr["inguate"]["nombre"] = $arrCookie["nombre"];     
            $arr["inguate"]["tipoSesion"] = $arrCookie["tipoSesion"];     
            $arr["inguate"]["tipoUsuario"] = 2;     
            $arr["inguate"]["lenguaje"] = $arrCookie["lenguaje"];     
            $arr["inguate"]["ubicacion"] = $arrCookie["ubicacion"];     
            
            $strCookie = implode(",", $arr["inguate"]);
            setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
            
                        
                
        }
        
        if( $strServicio == "setAlertaVista" ){
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            $objDBClass = new dbClass();
            
            $intKey = isset($_GET["key"]) ? fntCoreDecrypt($_GET["key"]) : 0;
            
            $strQuery = "UPDATE usuario_alerta 
                         SET    visto = 'Y'
                         WHERE  id_usuario_alerta = {$intKey} ";
            $objDBClass->db_consulta($strQuery);
            $objDBClass->db_close();
            
            $arr["error"] = "N";
            
            print json_encode($arr);
            
            die();                
        }
        
        if( $strServicio == "setLenguajeCookie" ){
            
            $strLenguaje = isset($_GET["lenguaje"]) ? $_GET["lenguaje"] : "es";
            
            include "core/function_servicio.php";
            include "core/dbClass.php";
            $objDBClass = new dbClass();
            
            $arrT = fntGetCookieSesion();
            
            $intIdUsuario = fntCoreDecrypt($arrT["webToken"]);            
            $strQuery = "UPDATE usuario
                         SET    lenguaje = '{$strLenguaje}'
                         WHERE  id_usuario = {$intIdUsuario} ";
            
            $objDBClass->db_consulta($strQuery);    
            $objDBClass->db_close();    
            
            
            $arr["inguate"]["webToken"] = $arrT["webToken"];     
            $arr["inguate"]["nombre"] = $arrT["nombre"];     
            $arr["inguate"]["tipoSesion"] = $arrT["tipoSesion"];     
            $arr["inguate"]["tipoUsuario"] = $arrT["tipoUsuario"];     
            $arr["inguate"]["lenguaje"] = $strLenguaje;     
            $arr["inguate"]["ubicacion"] = $arrT["ubicacion"];     
            
            $strCookie = implode(",", $arr["inguate"]);
            setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
            
            $arrr["Y"] = "Y";
            print json_encode($arrr);
            die();            
        }
        
        if( $strServicio == "setUbicacionCookie" ){
            
            $intUbicacion = isset($_GET["ubicacion"]) ? $_GET["ubicacion"] : "1";
            
            include "core/function_servicio.php";
            
            $arrT = fntGetCookieSesion();
            
            $arr["inguate"]["webToken"] = $arrT["webToken"];     
            $arr["inguate"]["nombre"] = $arrT["nombre"];     
            $arr["inguate"]["tipoSesion"] = $arrT["tipoSesion"];     
            $arr["inguate"]["tipoUsuario"] = $arrT["tipoUsuario"];     
            $arr["inguate"]["lenguaje"] = $arrT["lenguaje"];     
            $arr["inguate"]["ubicacion"] = $intUbicacion;     
            
            $strCookie = implode(",", $arr["inguate"]);
            setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
            
            $arrr["Y"] = "Y";
            print json_encode($arrr);
            die();            
        }
        
        if( $strServicio == "setLikePost" ){
            
            include "core/function_servicio.php";
            
            $intPost = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario && $intPost ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $strQuery = "INSERT INTO post_like(id_post, id_usuario)
                                            VALUES({$intPost}, {$intIdUsuario})";
                
                $objDBClass->db_consulta($strQuery);
                
                
                $strQuery = "SELECT post.id_post,
                                    post.contenido,
                                    post.id_usuario,
                                    post.add_fecha,
                                    post_galeria.id_post_galeria,
                                    post_galeria.imagen_url,
                                    usuario.nombre nombre_propietario,
                                    usuario.apellido apellido_propietario
                             FROM   post
                                        LEFT JOIN post_galeria
                                            ON  post_galeria.id_post = post.id_post,
                                    usuario
                             WHERE  post.id_post = {$intPost} 
                             AND    post.id_usuario = usuario.id_usuario
                             ORDER BY id_post_galeria ASC       
                             ";
                $arrPost = array();
                $qTMP = $objDBClass->db_consulta($strQuery);
                while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
                    
                    $arrPost["id_post"] = $rTMP["id_post"];
                    $arrPost["contenido"] = $rTMP["contenido"];
                    $arrPost["id_usuario"] = $rTMP["id_usuario"];
                    $arrPost["add_fecha"] = $rTMP["add_fecha"];
                    $arrPost["nombre_propietario"] = $rTMP["nombre_propietario"]." ".$rTMP["apellido_propietario"];
                    
                    if( intval($rTMP["id_post_galeria"]) )
                        $arrPost["galeria"][$rTMP["id_post_galeria"]]["imagen_url"] = $rTMP["imagen_url"];
                        
                }
                $objDBClass->db_free_result($qTMP);
                
                if( $arrPost["id_usuario"] != $intIdUsuario ){
                    
                    $strQuery = "INSERT INTO post(id_usuario, contenido, propietario, id_post_original, id_usuario_propietario)
                                            VALUES({$intIdUsuario}, '{$arrPost["contenido"]}', 'N', {$intPost}, {$arrPost["id_usuario"]})";
                    
                    $objDBClass->db_consulta($strQuery);
                    $intPost = intval($objDBClass->db_last_id());
                    
                    if( isset($arrPost["galeria"]) && count($arrPost["galeria"]) > 0 ){
                        
                        while( $rTMP = each($arrPost["galeria"]) ){
                            
                            $strQuery = "INSERT INTO post_galeria(id_post, imagen_url)
                                                            VALUES({$intPost}, '{$rTMP["value"]["imagen_url"]}')";
                            $objDBClass->db_consulta($strQuery);
                            
                            
                        }
                        
                    }
                    
                    
                }
                
                $objDBClass->db_close();
                                
            }
            
            die();                
            
        }
        
        if( $strServicio == "setLikeProducto" ){
            
            include "core/function_servicio.php";
            
            $intPlaceProducto = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario && $intPlaceProducto ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $strQuery = "INSERT INTO place_producto_like(id_place_producto , id_usuario)
                                            VALUES({$intPlaceProducto}, {$intIdUsuario})";
                
                $objDBClass->db_consulta($strQuery);
                
                drawdebug($strQuery);
                
                         
                $strQuery = "DELETE FROM place_producto_no_like WHERE  id_place_producto = {$intPlaceProducto} AND id_usuario = {$intIdUsuario}";
                
                $objDBClass->db_consulta($strQuery);
                
                         
                $objDBClass->db_close();
                                
            }
            
            die();                
            
        }
        
        if( $strServicio == "setNoLikeProducto" ){
            
            include "core/function_servicio.php";
            
            $intPlaceProducto = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario && $intPlaceProducto ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $strQuery = "INSERT INTO place_producto_no_like(id_place_producto , id_usuario)
                                            VALUES({$intPlaceProducto}, {$intIdUsuario})";
                
                $objDBClass->db_consulta($strQuery);
                
                         
                $strQuery = "DELETE FROM place_producto_like WHERE  id_place_producto = {$intPlaceProducto} AND id_usuario = {$intIdUsuario}";
                
                $objDBClass->db_consulta($strQuery);
                
                         
                $objDBClass->db_close();
                                
            }
            
            die();                
            
        }
        
        if( $strServicio == "setComentarioPost" ){
            
            include "core/function_servicio.php";
            
            $intPost = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
            
            $strComentario = isset($_POST["txtComentario"]) ? addslashes($_POST["txtComentario"]) : "";
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intPost && $intIdUsuario && !empty($strComentario) ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $strQuery = "INSERT INTO post_comentario(id_post , id_usuario, comentario)
                                            VALUES({$intPost}, {$intIdUsuario}, '{$strComentario}')";
                
                $objDBClass->db_consulta($strQuery);
                
                $objDBClass->db_close();
                                
            }
            
            die();                
            
        }
        
        if( $strServicio == "getSession" ){
            
            include "core/function_servicio.php";
            
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            $arr["session"] = $intIdUsuario ? "Y" : "N";
            
            print json_encode($arr);
            
            die();                
            
        }
        
        if( $strServicio == "setRegistroEmailConfirmacion" ){
            
            include "core/function_servicio.php";
            $strLenguaje = isset($_POST["hidModalLogInIdioma"]) && $_POST["hidModalLogInIdioma"] == "es" ? "es" : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
                        
            $strNombreRegistro = isset($_POST["txtNombreRegistro"]) ? $_POST["txtNombreRegistro"]  : "";
            $strEmailRegistro = isset($_POST["txtEmailRegistro"]) ? $_POST["txtEmailRegistro"]  : "";
            $strTelefonoRegistro = isset($_POST["txtTelefonoRegistro"]) ? $_POST["txtTelefonoRegistro"]  : "";
            $intReferenciaLugar = isset($_POST["hidReferenciaLugar"]) ? intval(fntCoreDecrypt($_POST["hidReferenciaLugar"]))  : 0;
            $strCodigoArea = isset($_POST["hidCodigoAreaRegistro"]) ? trim(($_POST["hidCodigoAreaRegistro"]))  : 0;
            $strTokenRecap = isset($_POST["txtTokenRecap"]) ? $_POST["txtTokenRecap"]  : 0;
            
                 
            $arr["error"] = false;
            $arr["error_recap"] = false;
            
            if( empty($strNombreRegistro) || empty($strEmailRegistro) || empty($strTelefonoRegistro) || empty($strTokenRecap) ){
                
                $arr["msn"] = lang["datos_error_1"];
                
                $arr["error"] = true;
                print json_encode($arr);            
                die();         
                
            }
            
            /*
            //Lo primerito, creamos una variable iniciando curl, pasándole la url
            $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
             
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt ($ch, CURLOPT_POST, 1);
             
            //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
            curl_setopt ($ch, CURLOPT_POSTFIELDS, "secret=6LfJ7agUAAAAAOt0iGbsPjudsKSUX6PHiK1qq6ek&response=".$strTokenRecap);
             
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
             
            //recogemos la respuesta
            $crulrespuesta = curl_exec ($ch);
            
             
            //o el error, por si falla
            $error = curl_error($ch);
             
            //y finalmente cerramos curl
            curl_close ($ch);
            
            $arrRespuesta = json_decode($crulrespuesta);
            
            drawdebug($arrRespuesta);
            if( !$arrRespuesta->success ){
                
                $arr["msn"] = lang["error_captcha"];
            
                print json_encode($arr);            
                die();         
                
            }
            */   
            
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
                                
            $strQuery = "SELECT id_usuario, estado
                         FROM   usuario   
                         WHERE  email = '{$strEmailRegistro}'";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_fetch_array($qTMP);
            
            $intIdUsuario = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
            
            if( !$intIdUsuario ){
                
                $strTelefonoRegistro = $strCodigoArea.$strTelefonoRegistro;
                
                $strQuery = "INSERT INTO usuario(email,  nombre, apellido, 
                                                 estado, tipo, add_fecha, lenguaje, telefono, tipo_registro, identificador,
                                                 clave )
                                          VALUES('{$strEmailRegistro}', '{$strNombreRegistro}', '',
                                                 'P', '3', SYSDATE(), '{$strLenguaje}', '{$strTelefonoRegistro}', 'E', '',
                                                 '')  ";
                $objDBClass->db_consulta($strQuery);
                
                $intIdUsuario = $objDBClass->db_last_id();
                
                if( intval($intReferenciaLugar) ){
                    
                    $strQuery = "INSERT INTO place_usuario_registro(id_place, id_usuario)
                                                            VALUES( {$intReferenciaLugar}, {$intIdUsuario} )";
                    $objDBClass->db_consulta($strQuery);  
                
                }
                
                fntSendEmailVerificacionUsuario($strEmailRegistro, $strNombreRegistro, fntCoreEncrypt($intIdUsuario.",codigo_1"));
                
                $arr["error"] = false;
                $arr["msn"] = "";
                
            }
            else{
                
                if( $rTMP["estado"] != "P" ){
                    
                    $arr["msn"] = lang["mismo_email"];
                    $arr["error"] = true;
                    print json_encode($arr);            
                    die();    
                                        
                }
                
            }
                   
            $objDBClass->db_close();
            
            print json_encode($arr);            
            die();
             
            
        }
        
        if( $strServicio == "setRegistroEmailConfirmacionClave" ){
            
            include "core/function_servicio.php";
            $strLenguaje = isset($_POST["hidModalLogInIdioma"]) && $_POST["hidModalLogInIdioma"] == "es" ? "es" : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
                        
            $strEmailRegistro = isset($_POST["txtEmailRegistroClave"]) ? $_POST["txtEmailRegistroClave"]  : "";
            $strTelefonoRegistro = isset($_POST["txtTelefonoClave"]) ? $_POST["txtTelefonoClave"]  : "";
            $strCodigoArea = isset($_POST["hidCodigoAreaRegistroClave"]) ? trim(($_POST["hidCodigoAreaRegistroClave"]))  : 0;
            $strTokenRecap = isset($_POST["txtTokenRecap"]) ? $_POST["txtTokenRecap"]  : 0;
            
                 
            $arr["error"] = false;
            $arr["error_recap"] = false;
            
            if( empty($strEmailRegistro) || empty($strTelefonoRegistro) || empty($strTokenRecap) ){
                
                $arr["msn"] = lang["datos_error_1"];
                
                $arr["error"] = true;
                print json_encode($arr);            
                die();         
                
            }
            
            /*
            //Lo primerito, creamos una variable iniciando curl, pasándole la url
            $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
             
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt ($ch, CURLOPT_POST, 1);
             
            //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
            curl_setopt ($ch, CURLOPT_POSTFIELDS, "secret=6LfJ7agUAAAAAOt0iGbsPjudsKSUX6PHiK1qq6ek&response=".$strTokenRecap);
             
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
             
            //recogemos la respuesta
            $crulrespuesta = curl_exec ($ch);
            
             
            //o el error, por si falla
            $error = curl_error($ch);
             
            //y finalmente cerramos curl
            curl_close ($ch);
            
            $arrRespuesta = json_decode($crulrespuesta);
            
            drawdebug($arrRespuesta);
            if( !$arrRespuesta->success ){
                
                $arr["msn"] = lang["error_captcha"];
            
                print json_encode($arr);            
                die();         
                
            }
            */   
            
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
                                
            $strQuery = "SELECT id_usuario, estado, nombre
                         FROM   usuario   
                         WHERE  email = '{$strEmailRegistro}'";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_fetch_array($qTMP);
            
            $intIdUsuario = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
            
            if( $intIdUsuario ){
                
                fntSendEmailVerificacionUsuario($strEmailRegistro, $rTMP["nombre"], fntCoreEncrypt($intIdUsuario.",codigo_1"));
                
                $arr["error"] = false;
                $arr["msn"] = "";
                
            }
            else{
                    
                $arr["msn"] = lang["email_no_registrado"];
                $arr["error"] = true;
                print json_encode($arr);            
                die();    
                                        
                
            }
                   
            $objDBClass->db_close();
            
            print json_encode($arr);            
            die();
             
            
        }
        
        if( $strServicio == "setRegistroPConfirmacion" ){
                                    
            include "core/function_servicio.php";
            $strLenguaje = isset($_POST["hidModalLogInIdioma"]) && $_POST["hidModalLogInIdioma"] == "es" ? "es" : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $strUid = isset($_POST["uid"]) ? $_POST["uid"]  : "";
            $strToken = isset($_POST["token"]) ? $_POST["token"]  : "";
            
            $strNombreRegistro = isset($_POST["txtNombreRegistro"]) ? $_POST["txtNombreRegistro"]  : "";
            $strEmailRegistro = isset($_POST["txtEmailRegistro"]) ? $_POST["txtEmailRegistro"]  : "";
            $strTelefonoRegistro = isset($_POST["txtTelefonoRegistro"]) ? $_POST["txtTelefonoRegistro"]  : "";
            $strClaveRegistro = isset($_POST["txtClaveRegristroConfirmacion"]) ? $_POST["txtClaveRegristroConfirmacion"]  : "";
            $intReferenciaLugar = isset($_POST["hidReferenciaLugar"]) ? intval(fntCoreDecrypt($_POST["hidReferenciaLugar"]))  : 0;
            $intTrafico = isset($_POST["hidTrafico"]) ? intval(fntCoreDecrypt($_POST["hidTrafico"]))  : 0;
            $strCodigoArea = isset($_POST["hidCodigoAreaRegistro"]) ? trim(($_POST["hidCodigoAreaRegistro"]))  : 0;
            
                 
            $arr["error"] = false;
            $arr["error_recap"] = false;
            
            if( empty($strNombreRegistro) || empty($strEmailRegistro) || empty($strTelefonoRegistro) || empty($strToken) || empty($strClaveRegistro) ){
                
                $arr["msn"] = lang["datos_error_1"];
                
                $arr["error"] = true;
                print json_encode($arr);            
                die();         
                
            }
            
            //Lo primerito, creamos una variable iniciando curl, pasándole la url
            $ch = curl_init('https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyCc8-C6Tuc7vNHZ2WGVaDsuGgGsygmh3fs');
             
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt ($ch, CURLOPT_POST, 1);
             
            //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
            curl_setopt ($ch, CURLOPT_POSTFIELDS, "idToken=".$strToken);
             
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
             
            //recogemos la respuesta
            $crulrespuesta = curl_exec ($ch);

             
            //o el error, por si falla
            $error = curl_error($ch);
             
            //y finalmente cerramos curl
            curl_close ($ch);

            $arrRespuesta = json_decode($crulrespuesta);

            if( !( isset($arrRespuesta->users[0]->localId) && $strUid == $arrRespuesta->users[0]->localId ) ){
                
                $arr["msn"] = "Usuario invalido";
                $arr["error_recap"] = true;
                $arr["error"] = true;

                print json_encode($arr);            
                die();         
                
            }
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
                                
            $strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";
                      
                           
            $strQuery = "SELECT id_usuario, estado
                         FROM   usuario   
                         WHERE  email = '{$strEmailRegistro}'";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_fetch_array($qTMP);
            
            $intIdUsuario = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
            
            if( !$intIdUsuario ){
                
                $strClaveRegistro = md5($strClaveRegistro);
                
                $strTelefonoRegistro = $strCodigoArea.$strTelefonoRegistro;
                
                $strQuery = "INSERT INTO usuario(email,  nombre, apellido, 
                                                 estado, tipo, add_fecha, lenguaje, telefono, tipo_registro, identificador,
                                                 clave, trafico )
                                          VALUES('{$strEmailRegistro}', '{$strNombreRegistro}', '',
                                                 'A', '3', SYSDATE(), '{$strLenguaje}', '{$strTelefonoRegistro}', 'E', '',
                                                 '{$strClaveRegistro}', {$intTrafico})  ";
                $objDBClass->db_consulta($strQuery);
                
                $intIdUsuario = $objDBClass->db_last_id();
                
                if( intval($intReferenciaLugar) ){
                    
                    $strQuery = "INSERT INTO place_usuario_registro(id_place, id_usuario)
                                                            VALUES( {$intReferenciaLugar}, {$intIdUsuario} )";
                    $objDBClass->db_consulta($strQuery);  
                
                }
                
                
                $strQuery = "SELECT *
                             FROM   usuario
                             WHERE  id_usuario = {$intIdUsuario} ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $arrQ["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                $arrQ["inguate"]["nombre"] = $rTMP["nombre"];     
                $arrQ["inguate"]["tipoSesion"] = $rTMP["tipo_registro"];     
                $arrQ["inguate"]["tipoUsuario"] = $rTMP["tipo"];     
                $arrQ["inguate"]["lenguaje"] = $rTMP["lenguaje"];     
                $arrQ["inguate"]["ubicacion"] = $rTMP["ubicacion"];     
                
                $strCookie = implode(",", $arrQ["inguate"]);
                setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                
                fntEnviarCorreo("info@inguate.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"]);
                
                $arr["error"] = false;
                $arr["token_registro"] = fntCoreEncrypt($intIdUsuario);
                
            }
            else{
                
                if( $rTMP["estado"] != "P" ){
                    
                    $arr["msn"] = lang["mismo_email"];
                    $arr["error"] = true;
                    print json_encode($arr);            
                    die();    
                                        
                }
                
            }
                   
            $objDBClass->db_close();
            
            print json_encode($arr);            
            die();
            
        }
        
        if( $strServicio == "setRegistroPConfirmacionClave" ){
                                    
            include "core/function_servicio.php";
            $strLenguaje = isset($_POST["hidModalLogInIdioma"]) && $_POST["hidModalLogInIdioma"] == "es" ? "es" : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $strUid = isset($_POST["uid"]) ? $_POST["uid"]  : "";
            $strToken = isset($_POST["token"]) ? $_POST["token"]  : "";
            
            $strEmailRegistro = isset($_POST["txtEmailRegistroClave"]) ? $_POST["txtEmailRegistroClave"]  : "";
            $strTelefonoRegistro = isset($_POST["txtTelefonoClave"]) ? $_POST["txtTelefonoClave"]  : "";
            $strClaveRegistro = isset($_POST["txtClaveRegristroConfirmacionClave"]) ? $_POST["txtClaveRegristroConfirmacionClave"]  : "";
            $strCodigoArea = isset($_POST["hidCodigoAreaRegistroClave"]) ? trim(($_POST["hidCodigoAreaRegistroClave"]))  : 0;
            
                 
            $arr["error"] = false;
            $arr["error_recap"] = false;
            
            if( empty($strEmailRegistro) || empty($strTelefonoRegistro) || empty($strToken) || empty($strClaveRegistro) ){
                
                $arr["msn"] = lang["datos_error_1"];
                
                $arr["error"] = true;
                print json_encode($arr);            
                die();         
                
            }
            
            //Lo primerito, creamos una variable iniciando curl, pasándole la url
            $ch = curl_init('https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyCc8-C6Tuc7vNHZ2WGVaDsuGgGsygmh3fs');
             
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt ($ch, CURLOPT_POST, 1);
             
            //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
            curl_setopt ($ch, CURLOPT_POSTFIELDS, "idToken=".$strToken);
             
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
             
            //recogemos la respuesta
            $crulrespuesta = curl_exec ($ch);

             
            //o el error, por si falla
            $error = curl_error($ch);
             
            //y finalmente cerramos curl
            curl_close ($ch);

            $arrRespuesta = json_decode($crulrespuesta);

            if( !( isset($arrRespuesta->users[0]->localId) && $strUid == $arrRespuesta->users[0]->localId ) ){
                
                $arr["msn"] = "Usuario invalido";
                $arr["error_recap"] = true;
                $arr["error"] = true;

                print json_encode($arr);            
                die();         
                
            }
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
                                
            $strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";
                      
                           
            $strQuery = "SELECT id_usuario, estado
                         FROM   usuario   
                         WHERE  email = '{$strEmailRegistro}'";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_fetch_array($qTMP);
            
            $intIdUsuario = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
            
            if( $intIdUsuario ){
                
                $strClaveRegistro = md5($strClaveRegistro);
                
                $strTelefonoRegistro = $strCodigoArea.$strTelefonoRegistro;
                
                $strQuery = "UPDATE usuario
                             SET    clave = '{$strClaveRegistro}',
                                    telefono = '{$strTelefonoRegistro}'
                             WHERE  id_usuario = {$intIdUsuario} ";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "SELECT *
                             FROM   usuario
                             WHERE  id_usuario = {$intIdUsuario} ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMPQ = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                      
                $arrQ["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                $arrQ["inguate"]["nombre"] = $rTMPQ["nombre"];     
                $arrQ["inguate"]["tipoSesion"] = $rTMPQ["tipo_registro"];     
                $arrQ["inguate"]["tipoUsuario"] = $rTMPQ["tipo"];     
                $arrQ["inguate"]["lenguaje"] = $rTMPQ["lenguaje"];     
                $arrQ["inguate"]["ubicacion"] = $rTMPQ["ubicacion"];     
                
                $strCookie = implode(",", $arrQ["inguate"]);
                setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                
                //fntEnviarCorreo("info@inguate.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"]);
                
                $arr["error"] = false;
                $arr["token_registro"] = fntCoreEncrypt($intIdUsuario);
                
            }
            else{
                
                $arr["msn"] = lang["email_no_registrado"];
                $arr["error"] = true;
                print json_encode($arr);            
                die();    
                                        
                
            }
                   
            $objDBClass->db_close();
            
            print json_encode($arr);            
            die();
            
        }
        
        if( $strServicio == "getCorreoExiste" ){
                                    
            include "core/function_servicio.php";
            $strLenguaje = isset($_GET["hidModalLogInIdioma"]) && $_GET["hidModalLogInIdioma"] == "es" ? "es" : "en";
            $boolRestablecerClave = isset($_GET["clave"]) && $_GET["clave"] == "Y" ? true : false;
            $strEmailRegistro = isset($_GET["email"])  ? addslashes($_GET["email"]) : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            if(  empty($strEmailRegistro)  ){
                
                $arr["msn"] = lang["datos_error_1"];
                
                $arr["error"] = true;
                print json_encode($arr);            
                die();         
                
            }
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
                                
            $strQuery = "SELECT id_usuario, estado
                         FROM   usuario   
                         WHERE  email = '{$strEmailRegistro}'";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_fetch_array($qTMP);
            
            $intIdUsuario = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
            
            $arr["msn"] = $boolRestablecerClave ? lang["email_no_registrado"] : lang["mismo_email"];
            $arr["existe"] = "N";
                
            if( $intIdUsuario ){
                
                $arr["msn"] = lang["mismo_email"];
                $arr["existe"] = "Y";
                
            }
                   
            $objDBClass->db_close();
            
            print json_encode($arr);            
            die();
            
        }
        
        if( $strServicio == "setRegistroPConfirmacionSMS" ){
            
            include "core/function_servicio.php";
            
            $strLenguaje = isset($_POST["hidModalLogInIdioma"]) && $_POST["hidModalLogInIdioma"] == "es" ? "es" : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $intIdUsuario = isset($_POST["hidTokenRegistro"]) ? intval(fntCoreDecrypt($_POST["hidTokenRegistro"])) : 0;
            $strCodigoSMS = isset($_POST["txtCodigoSMS"]) ? fntCoreClearToQuery($_POST["txtCodigoSMS"]) : "";
            $strClave = isset($_POST["txtClaveRegristroConfirmacion"]) ? trim($_POST["txtClaveRegristroConfirmacion"]) : "";
            
            $arr["error"] = true;
            $arr["msn"] = lang["error_completa_los_datos"];
            
            if( $intIdUsuario && !empty($strCodigoSMS) && !empty($strClave) ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                
                $strQuery = "SELECT id_usuario
                             FROM   usuario_codigo_sms
                             WHERE  codigo = '{$strCodigoSMS}'
                             and    id_usuario = {$intIdUsuario}  " ;
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $intIdUsuario = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
                
                $arr["error"] = true;
                $arr["msn"] = lang["codigo_confirmacion_error"];
                
                if( $intIdUsuario ){
                    
                    $strClave = md5($strClave);
                    
                    $strQuery = "UPDATE usuario
                                 SET    estado = 'A',
                                        clave = '{$strClave}'
                                 WHERE  id_usuario = {$intIdUsuario}  ";
                    $objDBClass->db_consulta($strQuery);
                    
                    $strQuery = "DELETE FROM usuario_codigo_sms WHERE id_usuario = {$intIdUsuario} ";
                    $objDBClass->db_consulta($strQuery);
                
                    
                    $strQuery = "SELECT *
                                 FROM   usuario
                                 WHERE  id_usuario = {$intIdUsuario} ";
                    $qTMP = $objDBClass->db_consulta($strQuery);
                    $rTMP = $objDBClass->db_fetch_array($qTMP);
                    $objDBClass->db_free_result($qTMP);
                    
                    $arrQ["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                    $arrQ["inguate"]["nombre"] = $rTMP["nombre"];     
                    $arrQ["inguate"]["tipoSesion"] = $rTMP["tipo_registro"];     
                    $arrQ["inguate"]["tipoUsuario"] = $rTMP["tipo"];     
                    $arrQ["inguate"]["lenguaje"] = $rTMP["lenguaje"];     
                    $arrQ["inguate"]["ubicacion"] = $rTMP["ubicacion"];     
                    
                    $strCookie = implode(",", $arrQ["inguate"]);
                    setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                    
                    //fntEnviarCorreo("bryant@ritzycapital.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"])
                    //fntEnviarCorreo("bryant@ritzycapital.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"]);
                    fntEnviarCorreo("info@inguate.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"]);
                    
                    $arr["error"] = false;
                    $arr["msn"] = "";
                        
                }
                
                
            }
                            
            print json_encode($arr);            
            die();
                
        }
        
        if( $strServicio == "setRegistroConfirmacionEmail" ){
            
            include "core/function_servicio.php";
            
            $strLenguaje = isset($_POST["hidModalLogInIdioma"]) && $_POST["hidModalLogInIdioma"] == "es" ? "es" : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $intIdUsuario = isset($_POST["hidTokenRegistro"]) ? intval(fntCoreDecrypt($_POST["hidTokenRegistro"])) : 0;
            $strClaveRegistro = isset($_POST["txtClaveRegristroConfirmacionEmail"]) ? trim($_POST["txtClaveRegristroConfirmacionEmail"]) : "";
            
            $arr["error"] = true;
            $arr["msn"] = lang["error_completa_los_datos"];
            
            if( $intIdUsuario && !empty($strClaveRegistro) ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $strClaveRegistro = md5($strClaveRegistro);
                
                
                $strQuery = "SELECT *
                             FROM   usuario
                             WHERE  id_usuario = {$intIdUsuario} ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $strEstado =  $rTMP["estado"];
                
                $arrQ["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                $arrQ["inguate"]["nombre"] = $rTMP["nombre"];     
                $arrQ["inguate"]["tipoSesion"] = $rTMP["tipo_registro"];     
                $arrQ["inguate"]["tipoUsuario"] = $rTMP["tipo"];     
                $arrQ["inguate"]["lenguaje"] = $rTMP["lenguaje"];     
                $arrQ["inguate"]["ubicacion"] = $rTMP["ubicacion"];     
                
                $strCookie = implode(",", $arrQ["inguate"]);
                setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                
                $strQuery = "UPDATE usuario
                             SET    estado = 'A',
                                    clave = '{$strClaveRegistro}'
                             WHERE  id_usuario = {$intIdUsuario}  ";
                $objDBClass->db_consulta($strQuery);
                
                if( $strEstado != "A" )
                    fntEnviarCorreo("info@inguate.com", "Regristro INGUATE", "Regristro de Usuario: ".$rTMP["nombre"]." - telefono: ".$rTMP["telefono"]." - email: ".$rTMP["email"]);
                
                $arr["error"] = false;
                $arr["msn"] = "";
                
                
            }
                            
            print json_encode($arr);            
            die();
                
        }
        
        if( $strServicio == "setLogIn" ){
            
            include "core/function_servicio.php";
            
            $strLenguaje = isset($_POST["hidModalLogInIdioma"]) && $_POST["hidModalLogInIdioma"] == "es" ? "es" : "en";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $strEmail = isset($_POST["txtEmail"]) ? trim($_POST["txtEmail"]) : 0;
            $strClave = isset($_POST["txtClave"]) ? trim($_POST["txtClave"]) : 0;
            
            $arr["error"] = true;
            $arr["msn"] = lang["datos_error_1"];
            
            if( !empty($strEmail) && !empty($strClave) ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $strClave = md5($strClave);
                
                $strQuery = "SELECT *
                             FROM   usuario
                             WHERE  email = '{$strEmail}'
                             AND    clave = '{$strClave}' ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $intIdUsuario = intval($rTMP["id_usuario"]);
                
                $arr["error"] = true;
                $arr["msn"] = lang["usuario_clave_error"];
                
                if( $intIdUsuario ){
                    
                    $arrQ["inguate"]["webToken"] = fntCoreEncrypt($intIdUsuario);     
                    $arrQ["inguate"]["nombre"] = $rTMP["nombre"];     
                    $arrQ["inguate"]["tipoSesion"] = $rTMP["tipo_registro"];     
                    $arrQ["inguate"]["tipoUsuario"] = $rTMP["tipo"];     
                    $arrQ["inguate"]["lenguaje"] = $rTMP["lenguaje"];     
                    $arrQ["inguate"]["ubicacion"] = $rTMP["ubicacion"];     
                    
                    $strCookie = implode(",", $arrQ["inguate"]);
                    setcookie( "inguate", $strCookie, time()+(60*60*24*30) );
                    
                    $arr["error"] = false;
                    $arr["msn"] = "";
                    
                } 
                
            }
                            
            print json_encode($arr);            
            die();
                
        }
        
        if( $strServicio == "setLugar" ){
            
            include "core/function_servicio.php";
            
            $arr = json_decode(file_get_contents('php://input'));
              
            $strTextoBusqueda = isset($arr->textoBusqueda) ? addslashes($arr->textoBusqueda) : "";
            $intUbicacion = isset($arr->ubicacionLugar) ? addslashes($arr->ubicacionLugar) : "";
            
            $strUrlNegocio = isset($arr->lugar->urlNegocio) ? addslashes($arr->lugar->urlNegocio) : "";
            $strNombre = isset($arr->lugar->nombre) ? addslashes($arr->lugar->nombre) : "";
            $strNombre = str_replace('"',"", $strNombre);
            $strNombre = str_replace('&amp;',"", $strNombre);
            $strEstella = isset($arr->lugar->estrella) ? addslashes($arr->lugar->estrella) : "";
            $strDireccion = isset($arr->lugar->direccion) ? addslashes($arr->lugar->direccion) : "";
            $strTelefono = isset($arr->lugar->telefono) ? addslashes($arr->lugar->telefono) : "";
            $strPrecio = isset($arr->lugar->precio) ? addslashes($arr->lugar->precio) : "";
            $strLat = isset($arr->lugar->lat) ? addslashes($arr->lugar->lat) : "";
            $strLng = isset($arr->lugar->lng) ? addslashes($arr->lugar->lng) : "";
            $arrHorario = isset($arr->lugar->horario) ? $arr->lugar->horario : "";
            $arrGaleria = isset($arr->lugar->galeria) ? $arr->lugar->galeria : "";
                            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
            
            $strQuery = "SELECT id_texto,
                                texto
                         FROM   texto
                         WHERE  texto = '{$strTextoBusqueda}'";
            
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intIdTexto = isset($rTMP["id_texto"]) ? intval($rTMP["id_texto"]) : 0;
            
            if( !$intIdTexto ){
                
                $strQuery = "INSERT INTO texto(texto)
                                        VALUES('{$strTextoBusqueda}')";
                
                $objDBClass->db_consulta($strQuery);
                
                $intIdTexto = $objDBClass->db_last_id();
                
            }
            
            $strQuery = "SELECT id_negocio
                         FROM   negocio
                         WHERE  nombre = '{$strNombre}'
                         AND    id_ubicacion_lugar = {$intUbicacion} ";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intIdNegocio = isset($rTMP["id_negocio"]) ? intval($rTMP["id_negocio"]) : 0;
            
            if( !$intIdNegocio ){
                
                $intPrecio = substr_count($strPrecio, "$");
                
                
                $arrExplode = explode("/", $strUrlNegocio);

                if( isset($arrExplode[6]) ){
                    
                    $strTMP = $arrExplode[6];
                    $strTMP = str_replace("@", "", $strTMP);

                    $arrTMP = explode(",", $strTMP);

                    $strLat = isset($arrTMP[0]) ? $arrTMP[0] : "";
                    $strLng = isset($arrTMP[1]) ? $arrTMP[1] : "";
                    
                }
                    
                $strQuery = "INSERT INTO negocio(id_ubicacion_lugar, nombre, lat, lng, direccion, rango, telefono, precio)
                                        VALUES('{$intUbicacion}','{$strNombre}','{$strLat}','{$strLng}','{$strDireccion}','{$strEstella}', '{$strTelefono}', {$intPrecio} )";
                
                $objDBClass->db_consulta($strQuery);
                
                $intIdNegocio = $objDBClass->db_last_id();
                
                $strQuery = "INSERT INTO negocio_texto( id_negocio , id_texto)
                                                VALUES(  {$intIdNegocio}, {$intIdTexto} )";
                                                
                $objDBClass->db_consulta($strQuery);
            
                if( count($arrHorario) > 0 ){
                    
                    while( $rTMP = each($arrHorario) ){
                        
                        $strQuery = "INSERT INTO negocio_horario(id_negocio, dia, horario)
                                                        VALUES(  {$intIdNegocio}, '{$rTMP["value"]->dia_text}', '{$rTMP["value"]->horario}' )";
                                                        
                        $objDBClass->db_consulta($strQuery);
                    
                    }
                    
                }
                
                if( count($arrGaleria) > 0 ){
                    
                    $arrImgSubida = array();
                    
                    while( $rTMP = each($arrGaleria) ){
                        
                        $url = $rTMP["value"];

                        if( @parse_url($url) && !isset($arrImgSubida[$url]) ){
                            
                            $arrImgSubida[$url] = $url;
                            
                            $strQuery = "INSERT INTO negocio_galeria(id_negocio)
                                                            VALUES(  {$intIdNegocio})";
                                                            
                            $objDBClass->db_consulta($strQuery);
                            $intIdNegocioGaleria = $objDBClass->db_last_id();                            
                            
                            // Image path
                            $img = '../../file_inguate/negocio/'.( md5(decoct($intIdNegocioGaleria)) ).'.png';

                            // Save image
                            $ch = curl_init($url);
                            $fp = fopen($img, 'wb');
                            curl_setopt($ch, CURLOPT_FILE, $fp);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_exec($ch);
                            curl_close($ch);
                            fclose($fp);
                            
                            $strQuery = "UPDATE negocio_galeria
                                         SET    url = '{$img}'
                                         WHERE  id_negocio_galeria = {$intIdNegocioGaleria} ";
                                                            
                            $objDBClass->db_consulta($strQuery);
                            
                            if( !intval(filesize($img)) ){
                                
                                $strQuery = "DELETE FROM negocio_galeria
                                             WHERE  id_negocio_galeria = {$intIdNegocioGaleria} ";
                                                                
                                $objDBClass->db_consulta($strQuery);
                                
                                unlink($img);
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            else{
                
                $strQuery = "INSERT INTO negocio_texto( id_negocio , id_texto)
                                                VALUES(  {$intIdNegocio}, {$intIdTexto} )";
                                                
                $objDBClass->db_consulta($strQuery);
                
            }
            
            $objDBClass->db_close();
            
            $arrRespuesta["error"] = "N";
            print json_encode($arrRespuesta);
            
            die();                
            
        }
        
        if( $strServicio == "getIssetLugar" ){
            
            include "core/function_servicio.php";
            
            $arr = json_decode(file_get_contents('php://input'));
              
            $strTextoBusqueda = isset($arr->textoBusqueda) ? addslashes($arr->textoBusqueda) : "";
            $intUbicacion = isset($arr->ubicacionLugar) ? addslashes($arr->ubicacionLugar) : "";
            
            $strNombre = isset($arr->nombre) ? addslashes($arr->nombre) : "";
            $strNombre = str_replace('"',"", $strNombre);
            $strNombre = str_replace('&amp;',"", $strNombre);
                            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
            
            $strQuery = "SELECT id_texto,
                                texto
                         FROM   texto
                         WHERE  texto = '{$strTextoBusqueda}'";
            
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intIdTexto = isset($rTMP["id_texto"]) ? intval($rTMP["id_texto"]) : 0;
            
            if( !$intIdTexto ){
                
                $strQuery = "INSERT INTO texto(texto)
                                        VALUES('{$strTextoBusqueda}')";
                
                $objDBClass->db_consulta($strQuery);
                
                $intIdTexto = $objDBClass->db_last_id();
                
            }
            
            $strQuery = "SELECT id_negocio
                         FROM   negocio
                         WHERE  nombre = '{$strNombre}'
                         AND    id_ubicacion_lugar = {$intUbicacion} ";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intIdNegocio = isset($rTMP["id_negocio"]) ? intval($rTMP["id_negocio"]) : 0;
            
            $arrRespuesta["existe"] = "N"; 
            if( $intIdNegocio ){
                
                $strQuery = "INSERT INTO negocio_texto( id_negocio , id_texto)
                                                VALUES(  {$intIdNegocio}, {$intIdTexto} )";
                                                
                $objDBClass->db_consulta($strQuery);
                $arrRespuesta["existe"] = "Y";
                
            }
            
            $objDBClass->db_close();
            
            $arrRespuesta["error"] = "N";
            
            print json_encode($arrRespuesta);
            
            die();                
            
        }
        
        if( $strServicio == "setPaginaPublica" ){
            
            include "core/function_servicio.php";
            
            $strData = isset($_POST["data"]) ? $_POST["data"] : "";
            $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : "";
            
            $arr["error"] = "true";
            $arr["msn"] = "No Place";
            
            if( $intPlace ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
            
                $strQuery = "SELECT url_place
                             FROM   place
                             WHERE  id_place = {$intPlace} ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $strNombreArchivo = isset($rTMP["url_place"]) ? $rTMP["url_place"] : "";
            
                $objDBClass->db_close();
                
                if( !empty($strNombreArchivo) ){
                    
                    $fp = fopen("{$strNombreArchivo}.html","w+");
                    fwrite($fp,$strData);
                    fclose($fp);
                    
                }
                
                $arr["error"] = "false";
                $arr["msn"] = "";
                
                
            }
                
            print json_encode($arr);
            die();    
            
        }
        
        
        
        $arr["error"] = "true";
        
        print json_encode($arr);
        
    }

} catch (Exception $e) {
    
    //echo 'Caught exception: ',  $e->getMessage(), "\n";
    
    $arr["error_e"] = "true exception";
    $arr["error"] = $e->getMessage();
    
    print json_encode($arr);
    
}


?>