<?php
  
include "core/function_servicio.php";

$strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

fntDrawHeaderPublico($boolMovil, false, true);

?>
<div class="container-fluid m-0 p-0 content " style="" id="divContendedorPagina">

    <div class="row m-0 p-0">
    
        <div class="col-12 p-0 m-0 text-center pt-2 pt-lg-4" id="">
        
        <!--
            <img src="<?php print  $arrCatalogoFondo[1]["url_fondo"]?>" style="width: 100%; height: auto;">
                  -->
                   
                       
        <?php
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();

        $arrAlerta = fntGetNotificacionUsuario(true, $objDBClass, true);
    
        fntDrawHeaderPrincipal(2, true, $objDBClass, $arrAlerta);
        
        ?>      
            
        <div class="container-fluid p-0 " style="margin-top: 2% !important;">
                   
                   
            
            
            <?php
            
            fntDrawFooter();
            
            ?>
             
        </div>
                   
        </div>
    </div>
    
    <?php
    
    fntShowBarNavegacionFooter(true, "3E53AC" , $arrAlerta);
    
    ?>
    
</div> 

<script >
                
    $(document).ready(function() { 
        
        var xhr;
        $('input[name="txtAutocomplete"]').autoComplete({
            minChars: 2,
            source: function(term, suggest){
                
                try {
                    xhr.abort();
                }
                catch(error) {
                }

                xhr = $.ajax({
                    url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete").val()+"&len="+$("#slcLenguaje").val(), 
                    dataType : "json",
                    success: function(result){
                        suggest(result);
                    }
                });
                
            },
            renderItem: function (item, search){
                
                //console.log(item);
                
                return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-identificador_2="'+item["identificador_2"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
            
            },
            onSelect: function(e, term, item){
                
                if( item.data("tipo") == "5" ){
                    
                    str = "placeup.php?p="+item.data("identificador")+"&len="+$("#slcLenguaje").val();
                    location.href = str;
                    return false;
                    
                }
                else if( item.data("tipo") == "4" ){
                    
                    str = "placeup.php?p="+item.data("identificador_2")+"&pro="+item.data("identificador")+"&len="+$("#slcLenguaje").val();
                    location.href = str;
                    return false;
                    
                }
                else{
                    
                    str = "s.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                    location.href = str;
                    
                }
                
                return false;
                
            }
        }).change(function (){
            
            str = "s.php?o=1&key=0&t=4&q="+$("#txtAutocomplete").val()+"&ran=all";
            location.href = str;
                    
        });
        
        $('input[name="txtAutocomplete_2"]').autoComplete({
            minChars: 2,
            source: function(term, suggest){
                
                try {
                    xhr.abort();
                }
                catch(error) {
                }

                xhr = $.ajax({
                    url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete_2").val()+"&len="+$("#slcLenguaje_2").val(), 
                    dataType : "json",
                    success: function(result){
                        suggest(result);
                    }
                });
                
            },
            renderItem: function (item, search){
                
                //console.log(item);
                
                return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-identificador_2="'+item["identificador_2"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
            
            },
            onSelect: function(e, term, item){
                
                if( item.data("tipo") == "5" ){
                    
                    str = "placeup.php?p="+item.data("identificador")+"&len="+$("#slcLenguaje_2").val();
                    location.href = str;
                    return false;
                    
                }
                else if( item.data("tipo") == "4" ){
                    
                    str = "placeup.php?p="+item.data("identificador_2")+"&pro="+item.data("identificador")+"&len="+$("#slcLenguaje_2").val();
                    location.href = str;
                    return false;
                    
                }
                else{
                    
                    str = "s.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                    location.href = str;
                    
                }
                
                return false;
                
            }
        }).change(function (){
            
            str = "s.php?o=1&key=0&t=4&q="+$("#txtAutocomplete_2").val()+"&ran=all";
            location.href = str;
                    
        });
          
        var sinWindowsHeight = $(window).height();
        
        //$("#divContenidoPost").height(( sinWindowsHeight * ( 70 / 100 ) )+'px');
        
        fntShowContenido();
        
        $('#mlAutoComplete').on('shown.bs.modal', function (e) {
            
            $("#txtAutocomplete_2").blur();

            $("#txtAutocomplete_2").addClass('active').focus();
             
        });
        
        
    });
    
    function fntSetTipoUsuarioIndex(intTipo){
        
        $("#txtTipoRegistro").val(intTipo);
        $("#divFormRegistro").show();
        $("#divFormRegistroOpcion").hide();
        $("#exampleModalCenter").modal("show");
            
    }
    
    function fntLogInPublicoIndex(){
        
        $("#txtAction").val(window.location.href);
        
        var formData = new FormData(document.getElementById("frmModalLogInRegistroIndex"));
            
        $(".preloader").fadeIn();
        $.ajax({
            url: "servicio_core.php?servicio=setLogInRegistroPIndex", 
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function(result){
                
                $(".preloader").fadeOut();
                
                if( result["error"] == "true" ){
                    
                    swal({
                        title: "Error",
                        text: result["msn"],
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                        
                }
                else{
                    
                    $("#divFormRegistro").hide();
                    $("#divFormRegistroOpcion").show();
        
                    if( $("#txtTipoRegistro").val() == "2" ){
                        
                        swal({
                            title: "Registro Con Exito",
                            text: "Gracias por registrarte en INGUATE.com, en pocos dias sera el lanzamiento por que lo tienes que estar atento. Comparte con tus amigos",
                            type: "success",
                            confirmButtonClass: "#FF0000",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true,
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                 
                                $.ajax({
                                    url: "servicio_core.php?servicio=setUsuarioNegocio", 
                                    success: function(result){
                                        
                                        setLogInDashCookie();    
                                        
                                    }
                                    
                                });
                            
                            } 
                        });
                                
                            
                    }
                    else{
                        
                        swal({
                            title: "Registro Con Exito",
                            text: "Gracias por registrarte en INGUATE.com, en pocos dias sera el lanzamiento por que lo tienes que estar atento. Comparte con tus amigos",
                            type: "success",
                            confirmButtonClass: "#FF0000",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true,
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                 
                                location.href = "index.php"
                                
                            
                            } 
                        });    
                                                
                    }
                                                
                }
                
            }
                    
        });
        
        return false;
        
    }
    
    function fntShowContenido(){
        
        $.ajax({                                                                                                                                                    
            url: "search.php?drawCotenido=true&o=2&key="+$("#hidKey").val()+"&t="+$("#hidTipo").val()+"&q="+$("#hidTexto").val()+"&ran="+$("#hidRango").val()+"&fil_tipo="+$("#hidFiltroTipo").val()+"&fil_key="+$("#hidFiltroKey").val()+"&len="+$("#slcLenguaje").val(), 
            success: function(result){
                
                $("#divContendo").html(result);
            
            }
        });
            
    }
    
    function fntShowAutocomplete(){
        
        $("#mlAutoComplete").modal("show");       
        
    }
            
    /*
        $.ajax({
            url: "https://api.unsplash.com/search/photos?page="+1+"&query=car&client_id=7e27b730888b9daa33c2b405cefb31c10cb887e8bf931e79cbd97d145019c6c4", 
            success: function(result){

                $(".preloader").fadeOut();
                //
                
                $.each( result["results"], function( key, value ) {
                    
                    //console.log(value["user"]["name"]);                 
                    //console.log(value["urls"]["small"]);
                    
                    $("#photos").append('<div class=" card-img-wrap" style="">   '+
                                       '     <img src="'+value["urls"]["regular"]+'" alt="" class="img-fluid " style="border: 6px solid white; border-radius: 5px; width: 100% !important; height: auto !important;">    '+
                                       ' </div>');
                     
                  
                });
                
                return false;
                
            }
        });  
            */
</script>

<?php
fntDrawFooterPublico("3E53AC");
?>