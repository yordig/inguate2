<?php
session_start();

include "core/function_servicio.php";

$intPlaceProducto = isset($_GET["p"]) ? intval(fntCoreDecrypt($_GET["p"])) : 0;

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

$boolUserPanelLogIn = false;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$arrInfoDispositivoi = fntGetInformacionClienteNavegacion();
    
if( $arrInfoDispositivoi["plataforma"] == "iPhone" )
    $boolMovil = true;


$strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";
define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

$_SESSION["boolUserPanelLogIn"] = isset($_SESSION["boolUserPanelLogIn"]) ? $_SESSION["boolUserPanelLogIn"] : false;

if( isset($_GET["setAutoCompletarInfoPlace"]) ){
    
    $strPlaceId = isset($_GET["place"]) ? $_GET["place"] : "";
    
    
    $strApiKey = fntGetApiKeyPlace();

    $strUrlApi = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$strPlaceId."&language=".$strLenguaje."&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,utc_offset,vicinity,formatted_phone_number,international_phone_number,opening_hours,website,price_level,rating,review,user_ratings_total&key=".$strApiKey;
            
    $objJsonDatos = file_get_contents($strUrlApi);
    $objJsonDatos = json_decode($objJsonDatos);
    //drawdebug($objJsonDatos);

    $arrFotos = isset($objJsonDatos->result->photos) ? $objJsonDatos->result->photos : array();
    $arrDatos = array();

    $arrDatos["nombre"] = isset($objJsonDatos->result->name) ? $objJsonDatos->result->name : "";
    $arrDatos["direccion"] = isset($objJsonDatos->result->formatted_address) ? $objJsonDatos->result->formatted_address : "";
    $arrDatos["telefono"] = isset($objJsonDatos->result->international_phone_number) ? $objJsonDatos->result->international_phone_number : "";
    $arrDatos["lat"] = isset($objJsonDatos->result->geometry->location->lat) ? $objJsonDatos->result->geometry->location->lat : "14.5585707";
    $arrDatos["lng"] = isset($objJsonDatos->result->geometry->location->lng) ? $objJsonDatos->result->geometry->location->lng : "-90.72952299999997";
    $arrDatos["horario"] = isset($objJsonDatos->result->opening_hours) ? $objJsonDatos->result->opening_hours : array();
    $arrDatos["rating"] = isset($objJsonDatos->result->rating) ? intval($objJsonDatos->result->rating) : 0;

    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "UPDATE place
                 SET    direccion = '{$arrDatos["direccion"]}',
                        telefono = '{$arrDatos["telefono"]}',
                        ubicacion_lat = '{$arrDatos["lat"]}',
                        ubicacion_lng = '{$arrDatos["lng"]}',
                        place_pregunta = 'N'
                 WHERE  id_place = {$intPlace} ";
    $objDBClass->db_consulta($strQuery);
    
    if( is_array($arrFotos) && count($arrFotos) > 0 ){
        
            
        while( $rTMP = each($arrFotos) ){
            
            $strLinkMap = "https://maps.googleapis.com/maps/api/place/photo?&key=".$strApiKey."&maxwidth=400&photoreference=";
        
            $strLinkMap .= $rTMP["value"]->photo_reference;
            
            $strQuery = "INSERT INTO place_galeria(id_place, url_imagen)
                                    VALUES({$intPlace}, '{$strLinkMap}')";
            $objDBClass->db_consulta($strQuery);
            
        }
        
    }
    die();
}

if( isset($_GET["fntDrawSeccionForm"]) ){
    
    include "core/dbClass.php";
    
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $intCantidadImagenes = 2;
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intPlaceProducto = isset($_GET["pro"]) ? intval(fntCoreDecrypt($_GET["pro"])) : 0;
    
    
    if( $intSeccion == 1 ){
        
        $objDBClass = new dbClass();  
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrGetGaleriaPlace = fntGetPlaceProductoGaleria($intPlaceProducto, true, $objDBClass);
        $intCount = 1;
        
        ?>
        <div class="modal-dialog modal-lg" style="min-width: 95%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header" id="modalGeneralHeader">
                    
                    <h5 class="modal-title"><?php print lang["galeria"]?></h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body" id="modalGeneralBody">
                    <form name="frmGaleria" id="frmGaleria"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                    <input type="hidden" id="hidPlaceProducto" name="hidPlaceProducto" value="<?php print fntCoreEncrypt($intPlaceProducto)?>">
            
                    <div class="row justify-content-center">
                        
                        <?php
                        
                        if( is_array($arrGetGaleriaPlace) && count($arrGetGaleriaPlace) > 0 ){
                            
                            while( $rTMP = each($arrGetGaleriaPlace) ){
                                ?>
                                <input type="hidden" value="<?php print $rTMP["key"]?>" id="hidIdGaleria_<?php print $intCount?>" name="hidIdGaleria_<?php print $intCount?>">
                                <input type="hidden" value="N" id="hidDelete_<?php print $intCount?>" name="hidDelete_<?php print $intCount?>">
                            
                                <div class="col-12 col-lg-3 " id="divContentImageSeccion1_<?php print $intCount;?>">
                                    
                                    <input type="file" id="flFileImage_<?php print $intCount;?>" name="flFileImage_<?php print $intCount;?>" onchange="fntSetImgfile('<?php print $intCount;?>')" style="display: none;">
                                    <div class="card" style="width: 18rem;">
                                        
                                        <div class="card-img-wrap">
                                            
                                            <img id="IMGflFileImage_<?php print $intCount;?>" class="card-img-top imgZoom" src="imgInguate.php?t=up&src=<?php print $rTMP["value"]["url_imagen"]?>" alt="Card image cap">
                                        
                                        </div>                                
                                        
                                        <div class="p-1 text-right" style="position: absolute; top: 0px; right: 0px;">
                                            
                                            <button class="btn btn-light btn-sm" onclick="$('#flFileImage_<?php print $intCount;?>').click();">
                                                <i class="fa fa-pencil"></i>
                                            </button>                                                
                                        
                                            <button class="btn btn-light btn-sm" onclick="fntDeleteImageSeccion1('<?php print $intCount;?>');">
                                                <i class="fa fa-trash"></i>
                                            </button>                                                
                                        
                                        </div>
                                    </div>
                                    
                                </div>
                                <?php
                                $intCount++;
                            }
                            
                        }
                        
                        ?>
                        
                                            
                        
                        <div class="col-12 col-lg-3 align-middle" id="divAddMoreSeccion1">
                                   
                            <table style="height: 100%; width: 100%;">
                                <tbody>
                                    <tr>
                                        <td class="align-middle">

                                            <button class="btn btn-secondary btn-sm btn-block pt-5 pb-5" onclick="fntAddImageSeccion1();">
                                                <?php print lang["anadir_imagen"]?> <i class="fa fa-plus"></i>
                                            </button>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>                                       
                            
                        </div>            
                                   
                    
                    </div>            
                    </form>
                </div>
                
                <div class="modal-footer" id="modalGeneralFooter">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php print lang["cerrar"]?></button>
                    <button type="button" class="btn btn-success" onclick="fntSaveGaleria();"><?php print lang["guardar"]?></button>
                
                </div>
            </div>
        </div>
        <script> 
        
            var intCountImage = "<?php print $intCount;?>";       
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("80%");
                    
            }); 
            
            function fntSetImgfile(index){
                
                var filename = $('#flFileImage_'+index).val();
                
                arr = filename.split(".");
                
                if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                    
                    swal("Error!", "<?php print lang["alerta_solo_imagenes"]?>", "error");
                    fntDeleteImageSeccion1(index);
                    return false;                    
                    
                }
                
                
                if ( ! window.FileReader ) {
                    return alert( 'FileReader API is not supported by your browser.' );
                }
                var $i = $( '#flFileImage_'+index ), // Put file input ID here
                    input = $i[0]; // Getting the element from jQuery
                
                if ( input.files && input.files[0] ) {
                    
                    file = input.files[0]; // The file
                        
                    fr = new FileReader(); // FileReader instance
                    
                    fr.onload = function () {
                        
                        var image = new Image();

                        image.onload = function() {      

                            //if( this.naturalWidth >= 900 || this.naturalHeight >= 600 ){

                                document.getElementById("IMGflFileImage_"+index).src = fr.result;

                            //}
                            //else{

                            //    swal("Error!", "The minimum dimensions are 900 x 600", "error");
                            //    fntDeleteImageSeccion1(index);
                    
                            //}

                        }

                        // Load image.
                        image.src = fr.result;
                        
                    }; 
                    //fr.readAsText( file );
                    fr.readAsDataURL( file );
                    
                } 
                else {
                    // Handle errors here
                    alert( "File not selected or browser incompatible." )
                }
                                
            }
            
            function fntAddImageSeccion1(){
                
                intCount = 0;
                $(":input[id^=hidIdGaleria_]").each(function (){
                    
                    intCount++;                    
                    
                });
                
                if( intCount == "<?php print $arrDatosPlace["num_galeria"]?>" ){
                    
                    swal("Error!", "Have a total of <?php print $arrDatosPlace["num_galeria"]?> photos configured for your gallery", "error");
                    
                    return false;
                                        
                }
                
                $("#divAddMoreSeccion1").before("<input type=\"hidden\" value=\"0\" id=\"hidIdGaleria_"+intCountImage+"\" name=\"hidIdGaleria_"+intCountImage+"\">    "+
                                                "<input type=\"hidden\" value=\"N\" id=\"hidDelete_"+intCountImage+"\" name=\"hidDelete_"+intCountImage+"\">       "+
                                                "<div class=\"col-12 col-lg-3\" id=\"divContentImageSeccion1_"+intCountImage+"\">           "+
                                                "    <input type=\"file\" id=\"flFileImage_"+intCountImage+"\" name=\"flFileImage_"+intCountImage+"\" onchange=\"fntSetImgfile('"+intCountImage+"')\" style=\"display: none;\">   "+
                                                "    <div class=\"card\" style=\"width: 18rem;\">   "+
                                                "        <div class=\"card-img-wrap\">        "+
                                                "            <img id=\"IMGflFileImage_"+intCountImage+"\" class=\"card-img-top imgZoom\" src=\"images/Noimagee.jpg?1\" >    "+
                                                "        </div>      "+                          
                                                "        <div class=\"p-1 text-right\" style=\"position: absolute; top: 0px; right: 0px;\">  "+
                                                "            <button class=\"btn btn-light btn-sm\" onclick=\"$('#flFileImage_"+intCountImage+"').click();\">    "+
                                                "                <i class=\"fa fa-pencil\"></i>     "+
                                                "            </button>  "+                                              
                                                "            <button class=\"btn btn-light btn-sm\" onclick=\"fntDeleteImageSeccion1('"+intCountImage+"');\">   "+
                                                "                <i class=\"fa fa-trash\"></i>    "+
                                                "            </button>     "+                                           
                                                "        </div>   "+
                                                "    </div>    "+
                                                "</div>");
                
                $("#flFileImage_"+intCountImage).click();
                intCountImage++;
                
                
            }
            
            function fntDeleteImageSeccion1(intIndex){
                
                $("#hidDelete_"+intIndex).val("Y");
                $("#divContentImageSeccion1_"+intIndex).remove();
                
            }
            
            function fntSaveGaleria(){
                
                
                
                var formData = new FormData(document.getElementById("frmGaleria"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "proes.php?setGaleriaPlaceProducto=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        fntDrawSeccion(1);
                        $("#modalGeneral").modal("hide");
                
                    }
                            
                });
                
            }
            
        </script>        
        <?php   
                
    }
    
    if( $intSeccion == 2 ){
        
        $objDBClass = new dbClass();  
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrPlaceHorario = fntGetPlaceHorario($intPlace, true, $objDBClass);
        
        $arrDiasSemana = getSemana();
        $arrHorario = getHoras24Horas();
        
        ?>
        <div class="modal-dialog modal-lg" style="min-width: 60%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header" id="modalGeneralHeader">
                    
                    <h5 class="modal-title">Datos Principales</h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body" id="modalGeneralBody">
                    <form name="frmDatosPrincipales" id="frmDatosPrincipales"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                        
                        <div class="row">
                            <div class="col-12">
                                <label for="txtTitulo">Titulo</label>
                                <input value="<?php print $arrDatosPlace["titulo"]?>" type="text" class="form-control form-control-sm" id="txtTitulo" name="txtTitulo" placeholder="">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-1">
                                <label for="txtDescripcion">Descripcion</label>
                                <textarea class="form-control form-control-sm" id="txtDescripcion" name="txtDescripcion" placeholder="" rows="3"><?php print $arrDatosPlace["descripcion"]?></textarea>
                                
                            </div>
                        </div>
                              
                        <div class="row">
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label for="txtTelefono">Telefono</label>
                                <input value="<?php print $arrDatosPlace["telefono"]?>" type="tel" class="form-control form-control-sm" id="txtTelefono" name="txtTelefono" placeholder="">
                                
                            </div>
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label for="txtDireccion">Direccion</label>
                                <input value="<?php print $arrDatosPlace["direccion"]?>" type="text" class="form-control form-control-sm" id="txtDireccion" name="txtDireccion" placeholder="">
                                
                            </div>
                        </div>
                                   
                        
                        <div class="row">
                            <div class="col-12 mt-2">
                                
                                <div class="accordion" id="accFormHorario">
                                    <div class="card " style="border: 0px;">
                                        <div class="card-header p-0 m-0 bg-white" id="headingOne" data-toggle="collapse" data-target="#accFormHorarioOne" aria-expanded="true" aria-controls="accFormHorarioOne" style="cursor: pointer; border: 0px;">
                                            
                                            <div class="row">
                                                <div class="col-12  text-center">
                                                    Horario
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <i class="fa fa-angle-down text-color-theme"></i>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <div id="accFormHorarioOne" class="collapse " aria-labelledby="headingOne" data-parent="#accFormHorario">
                                            <div class="card-body p-0 m-0 pt-2">
                                                
                                                <?php 
                                                
                                                while(  $rTMP = each($arrDiasSemana)){
                                                    
                                                    $arrInfo = isset($arrPlaceHorario[$rTMP["key"]]) ? $arrPlaceHorario[$rTMP["key"]] : array();
                                                    
                                                    ?>
                                                    
                                                    <input type="hidden" value="<?php print isset($arrInfo["id_place_horario"]) ? $arrInfo["id_place_horario"] : 0?>" name="hidPlaceHorario_<?php print $rTMP["key"]?>" id="hidPlaceHorario_<?php print $rTMP["key"]?>">
                                                    <input type="hidden" value="<?php print $rTMP["key"]?>" name="hidKeyDia_<?php print $rTMP["key"]?>" id="hidKeyDia_<?php print $rTMP["key"]?>">
                
                                                    <div class="row justify-content-center">
                                                        <div class="col-3 p-0 p-lg-1 ">
                                                        
                                                            <h6 class="text-right" ><?php print $rTMP["value"]["nombre"]?></h6>                                        
                                                            
                                                        </div>
                                                        <div class="col-5 p-0 p-lg-1">
                                                            
                                                            <div class="input-group mb-3">
                                                                
                                                                <input value="<?php print isset($arrInfo["hora_inicio"]) ? $arrInfo["hora_inicio"] : "00:00"?>" <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "disabled" : ""?> id="slcHorarioInicio_<?php print $rTMP["key"]?>" name="slcHorarioInicio_<?php print $rTMP["key"]?>" type="text" class="form-control form-control-sm text-center clockpicker" data-placement="left" data-align="top" data-autoclose="true" >

                                                                <div class="input-group-append">
                                                                    <span class="input-group-text p-0 m-0">&nbsp;-&nbsp;</span>
                                                                </div>

                                                                <input value="<?php print isset($arrInfo["hora_fin"]) ? $arrInfo["hora_fin"] : "00:00"?>" <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "disabled" : ""?> id="slcHorarioFin_<?php print $rTMP["key"]?>" name="slcHorarioFin_<?php print $rTMP["key"]?>" type="text" class="form-control form-control-sm text-center clockpicker" data-placement="left" data-align="top" data-autoclose="true" >

                                                            </div>
                                                                                                        
                                                        </div>
                                                        
                                                        <div class="col-3 p-0 p-lg-1">
                                                            <div class="custom-control custom-checkbox ">
                                                                
                                                                <input onchange="fntDisableDia('<?php print $rTMP["key"]?>');" class="custom-control-input" type="checkbox" id="chkClose_<?php print $rTMP["key"]?>" name="chkClose_<?php print $rTMP["key"]?>" <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "checked" : ""?>>
                                                                <label class="custom-control-label" for="chkClose_<?php print $rTMP["key"]?>" >
                                                                    Cerrado
                                                                </label>
                                                            </div>  
                                                            
                                                                     
                                                                                                  
                                                        </div>
                                                    </div>
                                                    
                                                    <?php
                                                }
                                                
                                                ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                
                            </div>
                        </div>
                                    
                    </form>
                </div>
                
                <div class="modal-footer" id="modalGeneralFooter">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-success" onclick="fntSaveDatosPrincipales();">Guardar</button>
                
                </div>
            </div>
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $('.clockpicker').clockpicker({
                    placement: 'bottom',
                    align: 'left',
                    donetext: 'Done'
                });
                    
            });
            
            function fntDisableDia(index){
                
                check = document.getElementById("chkClose_"+index).checked;
                document.getElementById("slcHorarioFin_"+index).disabled = check;                
                document.getElementById("slcHorarioInicio_"+index).disabled = check;                
                
            }
            
            function fntSaveDatosPrincipales(){
                
                var formData = new FormData(document.getElementById("frmDatosPrincipales"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "proes.php?setSaveDatosPrincipales=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        fntDrawSeccion(2);
                        $("#modalGeneral").modal("hide");
                
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
    
    if( $intSeccion == 3 ){
        
        $objDBClass = new dbClass();  
        
        $arrPlaceProducto = fntGetPlaceProductoDatos($intPlaceProducto, true, $objDBClass);
        $arrPlaceProductoCategoriaEspecial = fntGetPlaceProductoCategoriaEspecial($intPlaceProducto, true, $objDBClass);
        $arrCategoriaEspecial = fntGetCatalogoCategoriaEspecial(true, $objDBClass);
        $arrCategoriaEspecialFrecuenciaPago = fntGetCatalogoCategoriaEspecialFrecuenciaPago(true, $objDBClass);
        
        $arrClasificacionPadreHijo = fntGetPlaceClasificacionPadreHijo($arrPlaceProducto["id_place"], $arrPlaceProducto["id_clasificacion_padre"], true, $objDBClass);
    
        
        $boolDisableSolicitud = false;
        
        if( isset($arrPlaceProductoCategoriaEspecial["id_sol_categoria_especial"]) && intval($arrPlaceProductoCategoriaEspecial["id_sol_categoria_especial"])
            && $arrPlaceProductoCategoriaEspecial["estado"] == "S" ){
                
            $boolDisableSolicitud = true;
                
        }
        
        ?>
        <div class="modal-dialog modal-lg" style="min-width: 70%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header" id="modalGeneralHeader">
                    
                    <h5 class="modal-title"><?php print lang["descripcion"]?></h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body pt-0" id="modalGeneralBody">
                    <form name="frmAmenidades" id="frmAmenidades"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                    <input type="hidden" id="hidPlaceProducto" name="hidPlaceProducto" value="<?php print fntCoreEncrypt($intPlaceProducto)?>">
                        
                        <?php
                        
                        ?>
                        <div class="row">
                            <div class="col-6 col-lg-5 ">
                                                                                                                                  
                                <label for="txtNombre">Nombre en español</label>
                                <input value="<?php print $arrPlaceProducto["nombre"]?>" type="text" class="form-control form-control-sm" id="txtNombre" name="txtNombre" placeholder="">
                                
                                
                            </div>
                            <div class="col-12 col-lg-6 ">
                                
                                <label for="txtPrecio"><?php print lang["precio"]?></label>
                                <input value="<?php print $arrPlaceProducto["precio"]?>" type="text" class="form-control form-control-sm text-right" id="txtPrecio" name="txtPrecio" onchange="fntMantenerDecimales(this)">
                                
                                
                            </div>
                        </div>
                        <?php
                        
                        if( count($arrClasificacionPadreHijo) > 0 ){
                            
                            ?>
                            <div class="row">
                                <div class="col-12 col-lg-12 ">
                                    
                                    <label for="txtPrecio"><?php print lang["clasificacion"]?></label>
                                    <select  id="slcClasificacionPadreProducto" name="slcClasificacionPadreProducto[]" class="form-control-sm select2-multiple col-2 " multiple>
                                                    
                                        <?php
                                        reset($arrClasificacionPadreHijo);                                                
                                        while( $arrTMP = each($arrClasificacionPadreHijo) ){
                                            
                                            $strSelected = isset($arrPlaceProducto["clasificacionPadreHijo"][$arrTMP["key"]]) ? "selected" : "";
                                            
                                            ?>
                                            <option <?php print $strSelected;?> value="<?php print $arrTMP["key"]?>"><?php print $strLenguaje == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?></option>
                                            <?php
                                            
                                        }                            
                                        
                                        ?>
                                        
                                    </select>
                                </div>  
                                  
                            </div>
                            <?php
                            
                        }
                        
                        ?>
                        
                        <div class="row">
                            <div class="col-12 mt-1 ">
                                
                                <label for="txtDescripcion">Descripción en español</label>
                                <label class="">
                                    <div id="divtoolbar" style="display: none;">
                                       
                                        <a class="badge badge-secondary" data-wysihtml-command="bold" title="CTRL+B" style="font-weight: 100 !important;">
                                            <i class="fa fa-bold" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="italic" title="CTRL+I" style="font-weight: 100 !important;">
                                            <i class="fa fa-italic" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="insertUnorderedList" style="font-weight: 100 !important;">
                                            <i class="fa fa-list-ul" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="insertOrderedList" style="font-weight: 100 !important;">
                                            <i class="fa fa-list-ol" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignLeftStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-left" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignCenterStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-center" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignRightStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-right" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="justifyFull" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-justify" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        
                                      </div>
                                </label>
                                  
                                <textarea class="form-control form-control-sm" rows="2" style="width: 100%; height: auto;" id="txtDescripcion" name="txtDescripcion" placeholder=""><?php print $arrPlaceProducto["descripcion"]?></textarea>
                                
                                
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-12 mt-1 text-center text-lg-left">
                                
                                <button class="btn btn-secondary btn-sm text-background-theme mt-1" onclick="fntTraducirNombreDescripcion();" > 
                                    Traducir / Translate
                                </button>                                
                                
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-6 col-lg-5 ">
                                                                                                                              
                                <label >Name in english</label>
                                <input value="<?php print $arrPlaceProducto["nombre_en"]?>" type="text" class="form-control form-control-sm" id="txtNombreEN" name="txtNombreEN" placeholder="">
                                
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-1 ">
                                
                                <label >Description in english</label>
                                <label class="">
                                    <div id="divtoolbarEN" style="display: none;">
                                       
                                        <a class="badge badge-secondary" data-wysihtml-command="bold" title="CTRL+B" style="font-weight: 100 !important;">
                                            <i class="fa fa-bold" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="italic" title="CTRL+I" style="font-weight: 100 !important;">
                                            <i class="fa fa-italic" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="insertUnorderedList" style="font-weight: 100 !important;">
                                            <i class="fa fa-list-ul" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="insertOrderedList" style="font-weight: 100 !important;">
                                            <i class="fa fa-list-ol" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignLeftStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-left" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignCenterStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-center" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignRightStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-right" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="justifyFull" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-justify" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        
                                      </div>
                                </label>
                                  
                                <textarea class="form-control form-control-sm" rows="2" style="width: 100%; height: auto;" id="txtDescripcionEN" name="txtDescripcionEN" placeholder=""><?php print $arrPlaceProducto["descripcion_en"]?></textarea>
                                
                                
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-12 mt-1 text-right">
                                
                                <button type="button" class="btn btn-secondary  text-background-theme mt-1" onclick="fntSaveAmenidad();"><?php print lang["guardar"]?></button>
                                
                            </div>                        
                        </div>
                        <?php
                        
                        if( false ){
                            ?>
                            <hr class="m-0 p-0">
                            <div class="row">
                                <div class="col-12 col-lg-12 ">
                                    
                                    <label for="txtPrecio">Categoria Especial</label>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-4 mt-lg-0 mt-2">
                                    <select  <?php print $boolDisableSolicitud ? "disabled" : ""?> class="custom-select custom-select-sm" id="slcCategoriaEspecial" name="slcCategoriaEspecial">
                                        <option>Categoria especial...</option>
                                        <?php
                                        
                                        while( $rTMP = each($arrCategoriaEspecial) ){
                                            
                                            $strSelected = isset($arrPlaceProductoCategoriaEspecial["id_categoria_especial"]) && $arrPlaceProductoCategoriaEspecial["id_categoria_especial"] == $rTMP["key"] ? "selected" : "";
                                            ?>
                                            
                                            <option <?php print $strSelected?>  value="<?php print $rTMP["key"]?>"><?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                                            
                                            <?php
                                        }
                                        
                                        ?>
                                        
                                    </select>
                                </div>
                                <div class="col-12 col-lg-4 mt-lg-0 mt-lg-0 mt-1">
                                    
                                    <select <?php print $boolDisableSolicitud ? "disabled" : ""?> class="custom-select custom-select-sm" id="slcFrecuenciaPago" name="slcFrecuenciaPago">
                                        <option>Frecuencia de Pago...</option>
                                        <?php
                                        
                                        while( $rTMP = each($arrCategoriaEspecialFrecuenciaPago) ){
                                            
                                            $strSelected = isset($arrPlaceProductoCategoriaEspecial["id_frecuencia"]) && $arrPlaceProductoCategoriaEspecial["id_frecuencia"] == $rTMP["key"] ? "selected" : "";
                                            ?>
                                            
                                            <option <?php print $strSelected?>  value="<?php print $rTMP["key"]?>"><?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                                            
                                            <?php
                                        }
                                        
                                        ?>
                                        
                                    </select>    
                                </div>
                                
                                
                                <div class="col-12 col-lg-4 p-0">
                                    
                                    <div class="input-group p-0 mt-lg-0 mt-2">
                                        <div class="input-group-prepend col-6 p-0 ">
                                            <input readonly <?php print $boolDisableSolicitud ? "disabled" : ""?> value="<?php print isset($arrPlaceProductoCategoriaEspecial["fecha_inicio"]) ? $arrPlaceProductoCategoriaEspecial["fecha_inicio"] : ""?>" type="text" class="form-control form-control-sm" id="txtFechaInicio" name="txtFechaInicio" placeholder="Ingresa tu Fecha Inicio">
                                            &nbsp;
                                            -
                                            &nbsp;
                                        </div>
                                        
                                        <div class="input-group-append col-6 p-0">
                                            <input readonly <?php print $boolDisableSolicitud ? "disabled" : ""?> value="<?php print isset($arrPlaceProductoCategoriaEspecial["fecha_fin"]) ? $arrPlaceProductoCategoriaEspecial["fecha_fin"] : ""?>" type="text" class="form-control form-control-sm" id="txtFechaFin" name="txtFechaFin" placeholder="Ingresa tu Fecha Fin">
                    
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>
                                
                                
                                
                                
                            </div> 
                            <div class="row justify-content-center mt-lg-1 mt-0">   
                                
                                <div class="col-12 col-lg-9 mt-lg-0 mt-2 ">
                                    <input <?php print $boolDisableSolicitud ? "disabled" : ""?> value="<?php print isset($arrPlaceProductoCategoriaEspecial["descripcion"]) ? $arrPlaceProductoCategoriaEspecial["descripcion"] : ""?>" type="text" class="form-control form-control-sm" id="txtDescripcionSolicitud" name="txtDescripcionSolicitud" placeholder="Motivo de solicitud">
                    
                                </div>
                                
                                <div class="col-12 col-lg-3 mt-lg-0 mt-2 text-center">
                                    <button id="divSolicitudenvio" class="btn btn-success btn-sm <?php print $boolDisableSolicitud ? "hide" : ""?>" onclick="fntSolicitudEnvio();">Enviar Solicitud</button>
                                    <div id="divSolicitudenviada" class="badge badge-info <?php print $boolDisableSolicitud ? "" : "hide"?>">Solicitud enviada</div>
                                </div>
                            </div>    
                            
                            <?php
                        }
                        
                        ?>
                                                    
                                        
                    </form>
                </div>
                
            </div>
        </div>                 
        <script src="dist/nueva_textarea/dist/wysihtml.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.all-commands.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.table_editing.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.toolbar.js"></script>

        <script src="dist/nueva_textarea/parser_rules/advanced_and_extended.js"></script>

        <link href="dist/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
        <script src="dist/datepicker/js/bootstrap-datepicker.min.js"></script>
        <script>
            
            var editorEN;
            $(document).ready(function() { 
                
                $('#tblAmenidades').DataTable({
                    "ordering": false
                });   
                
                var editor = new wysihtml.Editor("txtDescripcion", {
                    toolbar:      "divtoolbar",
                    parserRules:  wysihtmlParserRules,
                    pasteParserRulesets: wysihtmlParserPasteRulesets
                  });
                  
                $("#slcClasificacionPadreProducto").select2({
                    width: 'style',
                    placeholder: $(this).attr('placeholder'),
                    allowClear: Boolean($(this).data('allow-clear')),
                });
                
                
            });
                
            function fntSaveAmenidad(){
                
                var formData = new FormData(document.getElementById("frmAmenidades"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "proes.php?setPlaceProductoDescripcion=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        fntDrawSeccion(3);
                        $("#modalGeneral").modal("hide");
                
                    }
                            
                });
                
            }
            
            function fntSolicitudEnvio(){
                
                var formData = new FormData(document.getElementById("frmAmenidades"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "proes.php?setSolicitudEnvio=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $("#divSolicitudenviada").show();
                        $("#divSolicitudenvio").hide();
                
                        document.getElementById("slcCategoriaEspecial").disabled = true;
                        document.getElementById("slcFrecuenciaPago").disabled = true;
                        document.getElementById("txtDescripcionSolicitud").disabled = true;
                        document.getElementById("txtFechaInicio").disabled = true;
                        document.getElementById("txtFechaFin").disabled = true;
                
                    }
                            
                });
                
            }
            
            function fntTraducirNombreDescripcion(){
                
                var formData = new FormData();
                                                               
                formData.append('txtNombre', $("#txtNombre").val());
                formData.append('txtDescripcion', $("#txtDescripcion").val());
                
                $(".preloader").fadeIn();
                $.ajax({
                    url: "placeup.php?getTraduccion=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType : "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $("#txtNombreEN").val(result["nombre"]);
                        
                        $("#txtDescripcionEN").html(result["descripcion"], true);                                      
                        editorEN = new wysihtml.Editor("txtDescripcionEN", {
                            toolbar:      "divtoolbarEN",
                            parserRules:  wysihtmlParserRules,
                            pasteParserRulesets: wysihtmlParserPasteRulesets
                          });
                    }
                            
                });
                            
            }
            
        </script>
        <?php
        
    }
        
    if( $intSeccion == 4 ){
        
        $objDBClass = new dbClass();  
        
        $arrCatalogoFondo = fntCatalogoFondoPlacePublico();
        
        ?>
        <div class="modal-dialog modal-lg" style="min-width: 70%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header" id="modalGeneralHeader">
                    
                    <h5 class="modal-title">Fondo</h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body" id="modalGeneralBody">
                    <form name="frmFondo" id="frmFondo"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                    
                    <?php
                    
                    while( $rTMP = each($arrCatalogoFondo) ){
                        ?>
                        <div class="row justify-content-center">
                            <div class="col-lg-2 col-4 p-0 m-0 mt-3 text-right border-bottom">
                                        
                                <img style="width: 100%; height: auto;" src="<?php print $rTMP["value"]["url_fondo"]?>" class="img-fluid rounded">
                                
                                
                            </div>
                            <div class="col-lg-6 col-8 p-0 m-0 pl-3 mt-3 border-bottom pb-1">
                                
                                <div class="row">
                                    <div class="col-6">
                                        
                                        <h5 class="text-dark text-left lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                            <?php print $rTMP["value"]["nombre"]?>
                                        </h5>
                                    
                                    </div>
                                    <div class="col-6 ">
                                        
                                        <button class="btn btn-secondary" onclick="fntSetFondoPlace('<?php print $rTMP["key"]?>');">Select</button>
                                    
                                    </div>
                                </div>
                            
                            
                            </div>
                        </div>
                        <?php
                    }
                    
                    ?>
                            
                        
                    </form>
                </div>
                  
            </div>
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $('#tblAmenidades').DataTable({
                    "ordering": false
                });    
                    
                    
            });
            
            function fntSetFondoPlace(intKeyCatalogo){
                
                var formData = new FormData();
                formData.append("intFondoPlace", intKeyCatalogo);                    
                formData.append("hidPlace", "<?php print fntCoreEncrypt($intPlace)?>");
                                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "proes.php?setFondoPlace=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        location.href = "placeup.php?p=<?php print fntCoreEncrypt($intPlace)?>&len=<?php $strLenguaje?>";
                        
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
        
                      
    die();
}

if( isset($_GET["setFondoPlace"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceCatalogo = isset($_POST["intFondoPlace"]) ? intval(($_POST["intFondoPlace"])) : 0;
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "UPDATE place
                 SET    id_tema = {$intPlaceCatalogo}
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
    $objDBClass->db_close();
    
    die();
}

if( isset($_GET["getShowModalProductoClasificacionPadre"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intClasificacionPadre = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
    
    $arrClasificacionPadreHijo = fntGetPlaceClasificacionPadreHijo($intPlace, $intClasificacionPadre, true, $objDBClass);
    $arrPlaceProducto = fntGetPlaceProducto($intPlace, $intClasificacionPadre, true, $objDBClass);
    
    $intCount = 1;
    ?>
    <div class="modal-dialog modal-lg" style="min-width: 70%;" role="document">
        <div class="modal-content" id="modalGeneraContenido">
          
            <?php
            ?>
            <div class="modal-header" id="modalGeneralHeader">
                
                <h5 class="modal-title">Productos</h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
            </div>
            
            <div class="modal-body" id="modalGeneralBody">
                <form name="frmProducto" id="frmProducto"  onsubmit="return false;" method="POST">
                <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                <input type="hidden" id="hidClasificacionPadre" name="hidClasificacionPadre" value="<?php print fntCoreEncrypt($intClasificacionPadre)?>">
                
                <?php
                
                if( count($arrPlaceProducto) > 0 ){
                    
                    while( $rTMP = each($arrPlaceProducto) ){
                        ?>
                        
                        <input type="hidden" id="hidIdPlaceProducto_<?php print $intCount;?>" name="hidIdPlaceProducto_<?php print $intCount;?>" value="<?php print $rTMP["key"]?>">
                        <input type="hidden" id="hidProductoUpdate_<?php print $intCount;?>" name="hidProductoUpdate_<?php print $intCount;?>" value="N">
                        <input type="hidden" id="hidProductoDelete_<?php print $intCount;?>" name="hidProductoDelete_<?php print $intCount;?>" value="N">
                        
                        <div class="row p-0 m-0" id="divContenidoProducto_<?php print $intCount;?>">
                            <div class="col-lg-2 col-4 p-0 m-0 mt-3 text-right border-bottom">
                            
                                <input type="file" id="flImgProducto_<?php print $intCount;?>" name="flImgProducto_<?php print $intCount;?>" onchange="fntSetLogoPreview('<?php print $intCount;?>');" style="display: none;">
                                                         
                                <img id="imgProducto_<?php print $intCount;?>" style="width: 100px; height: 100px;" src="<?php print !empty($rTMP["value"]["logo_url"]) ? $rTMP["value"]["logo_url"] : "images/Noimagee.jpg?1"?>" class="img-fluid rounded">
                                
                                <div id="btnImgProducto_" class="btn-theme-circular" onclick="$('#flImgProducto_<?php print $intCount;?>').click();" style="padding: 2px 3px 2px 3px; font-size: 14px; display: none;">
                                    &nbsp;<i class="fa fa-pencil"></i>&nbsp;
                                </div> 
                                
                            </div>
                            <div class="col-lg-10 col-8 p-0 m-0 pl-3 mt-3 border-bottom pb-1">
                                                  
                                <div id="divVistaEdicion_<?php print $intCount?>" style="display: none;">
                                    <input value="<?php print $rTMP["value"]["nombre"];?>" type="text" class="form-control form-control-sm" id="txtNombre_<?php print $intCount;?>" name="txtNombre_<?php print $intCount;?>" placeholder="Nombre">
                                    
                                    <textarea class="form-control form-control-sm mt-1" id="txtDescripcion_<?php print $intCount;?>" name="txtDescripcion_<?php print $intCount;?>" rows="2" placeholder="Descripcion"><?php print $rTMP["value"]["descripcion"];?></textarea>
              
                                    <input value="<?php print $rTMP["value"]["precio"];?>" type="text" class="form-control form-control-sm mt-1 mb-1 col-12 col-lg-4 text-right" id="txtPrecio_<?php print $intCount;?>" name="txtPrecio_<?php print $intCount;?>" placeholder="Precio" onchange="fntMantenerDecimales(this)">
                                    
                                    <div class="row">
                                        <div class="col-12 col-lg-8">
                                            <label for="slcClasificacionPadreProducto_<?php print $intCount;?>"><small>Clasificacion</small></label>
                                            <select  id="slcClasificacionPadreProducto_<?php print $intCount;?>" name="slcClasificacionPadreProducto_<?php print $intCount;?>[]" class="form-control-sm select2-multiple col-2 " multiple>
                                                
                                                <?php
                                                reset($arrClasificacionPadreHijo);                                                
                                                while( $arrTMP = each($arrClasificacionPadreHijo) ){
                                                    
                                                    $strSelected = isset($rTMP["value"]["clasificacion_padre_hijo"][$arrTMP["key"]]) ? "selected" : "";
                                                    
                                                    ?>
                                                    <option <?php print $strSelected;?> value="<?php print $arrTMP["key"]?>"><?php print $strLenguaje == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?></option>
                                                    <?php
                                                    
                                                }                            
                                                
                                                ?>
                                                
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-4 pt-3 text-center text-lg-right">
                                            
                                            <button class="btn btn-secondary btn-sm mt-lg-3" onclick="fntDeleteProducto('<?php print $intCount;?>')">Eliminar</button>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="divVistaPrevia_<?php print $intCount?>" >
                                    
                                    <div class="row">
                                        <div class="col-8 col-lg-10">
                                            <h5 class="text-dark text-left lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                                <?php print $rTMP["value"]["nombre"];?>                   
                                    
                                            </h5>
                                        
                                        </div>
                                        <div class="col-4 col-lg-2 text-left">
                                            <button onclick="fntEditProducto('<?php print $intCount?>');" class="btn btn-secondary btn-sm text-background-theme"  >
                                                Editar                        
                                            </button>
                                        </div>
                                    </div>
                                        
                                                
                                        
                                    <h5 class="  text-left m-0 p-0" style="font-size: 14px;">
                                        <?php print $rTMP["value"]["descripcion"];?>               
                                    </h5>
                                    <h5 class="text-left m-0 p-0 text-color-theme" style="font-size: 14px;">
                                        Q <?php print $rTMP["value"]["precio"];?>             
                                    </h5>
                                    <h3 class="text-left  m-0 p-0" style="font-size: 16px;">
                                        
                                        <?php
                                        reset($arrClasificacionPadreHijo);                                                
                                        while( $arrTMP = each($arrClasificacionPadreHijo) ){
                                            
                                            if( isset($rTMP["value"]["clasificacion_padre_hijo"][$arrTMP["key"]]) ){
                                                
                                                ?>
                                                <span class="badge  text-white text-background-theme"  style="">
                                                    <?php print $strLenguaje == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?>                        
                                                </span>
                                                <?php
                                                
                                            }                                            
                                            
                                        }                            
                                        
                                        ?>
                                    </h3>
                                </div>
                                
                            </div>
                        </div>
                        <?php
                        $intCount++;
                    }
                    
                }
                
                ?>
                        
                
                <div class="row" id="divBotonAgregar">
                    <div class="col-6 mt-4">
                        <button class="btn btn-secondary btn-block" onclick="fntAddProducto();"> Agregar Producto <i class="fa fa-plus"></i> </button>
                    </div>
                    <div class="col-6 mt-4">
                        <button class="btn btn-secondary btn-block" onclick="fntAddProducto();"> Agregar Producto Especial <i class="fa fa-plus"></i> </button>
                    </div>
                </div>
                            
                </form>
            </div>
            
            <div class="modal-footer" id="modalGeneralFooter">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" onclick="fntSaveProducto();">Guardar</button>
            
            </div>
        </div>
    </div>
    <script> 
    
        var intCount = "<?php  print $intCount;?>";       
        
        $(document).ready(function() { 
                
            $(".select2-multiple").select2({
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });

        }); 
        
        function fntSetLogoPreview(index){
                                        
            var filename = $('#flImgProducto_'+index).val();
            arr = filename.split(".");
 
            if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                
                swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
                return false;                    
                
            }
            
            
            if ( ! window.FileReader ) {
                return alert( 'FileReader API is not supported by your browser.' );
            }
            var $i = $( '#flImgProducto_'+index ), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if ( input.files && input.files[0] ) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    //$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
                    //console.log(fr.result);
                    document.getElementById("imgProducto_"+index).src = fr.result;
                };
                //fr.readAsText( file );
                fr.readAsDataURL( file );
            } else {
                // Handle errors here
                alert( "File not selected or browser incompatible." )
            }
                
        }
        
        function fntAddProducto(){
            
            /*
            intCount = 0;
            $(":input[id^=hidIdGaleria_]").each(function (){
                
                intCount++;                    
                
            });
            
            if( intCount == "<?php print 2?>" ){
                
                swal("Error!", "Have a total of <?php print 2?> photos configured for your gallery", "error");
                
                return false;
                                    
            }
             */
            $("#divBotonAgregar").before("<input type=\"hidden\" id=\"hidIdPlaceProducto_"+intCount+"\" name=\"hidIdPlaceProducto_"+intCount+"\" value=\"0\">  "+
                                         "<input type=\"hidden\" id=\"hidProductoUpdate_"+intCount+"\" name=\"hidProductoUpdate_"+intCount+"\" value=\"Y\"> "+
                                         "<div class=\"row p-0 m-0\" id=\"divContenidoProducto_"+intCount+"\">   "+
                                         "   <div class=\"col-lg-2 col-4 p-0 m-0 mt-3 text-right border-bottom\">    "+
                                         "       <input type=\"file\" id=\"flImgProducto_"+intCount+"\" name=\"flImgProducto_"+intCount+"\" onchange=\"fntSetLogoPreview('"+intCount+"');\" style=\"display: none;\">   "+
                                         "       <img id=\"imgProducto_"+intCount+"\" style=\"width: 100px; height: 100px;\" src=\"images/Noimagee.jpg?1\" class=\"img-fluid rounded\">    "+
                                         "       <div id=\"btnImgProducto_\" class=\"btn-theme-circular\" onclick=\"$('#flImgProducto_"+intCount+"').click();\" style=\"padding: 2px 3px 2px 3px; font-size: 14px;\">   "+
                                         "           &nbsp;<i class=\"fa fa-pencil\"></i>&nbsp;  "+
                                         "       </div>            "+
                                         "   </div>        "+
                                         "   <div class=\"col-lg-10 col-8 p-0 m-0 pl-3 mt-3 border-bottom pb-1\">        "+
                                         "       <div id=\"divVistaEdicion_"+intCount+"\">     "+
                                         "           <input type=\"text\" class=\"form-control form-control-sm\" id=\"txtNombre_"+intCount+"\" name=\"txtNombre_"+intCount+"\" placeholder=\"Nombre\">        "+
                                         "           <textarea class=\"form-control form-control-sm mt-1\" id=\"txtDescripcion_"+intCount+"\" name=\"txtDescripcion_"+intCount+"\" rows=\"2\" placeholder=\"Descripcion\"></textarea>     "+
                                         "           <input value=\"\" type=\"text\" class=\"form-control form-control-sm mt-1 mb-1 col-12 col-lg-4 text-right\" id=\"txtPrecio_"+intCount+"\" name=\"txtPrecio_"+intCount+"\" placeholder=\"Precio\" onchange=\"fntMantenerDecimales(this);\">  "+
                                         "           <div class=\"row\">         "+
                                         "               <div class=\"col-12 col-lg-8\">      "+
                                         "                   <label for=\"slcClasificacionPadreProducto_"+intCount+"\"><small>Clasificacion</small></label>   "+
                                         "                   <select  id=\"slcClasificacionPadreProducto_"+intCount+"\" name=\"slcClasificacionPadreProducto_"+intCount+"[]\" class=\"form-control-sm select2-multiple col-2 \" multiple>    "+
                                                                <?php
                                                                reset($arrClasificacionPadreHijo);
                                                                while( $rTMP = each($arrClasificacionPadreHijo) ){
                                                                    ?>
                                         "                           <option value=\"<?php print $rTMP["key"]?>\"><?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>      "+
                                                                    <?php
                                                                }                            
                                                                ?>
                                         "                   </select>      "+
                                         "               </div>         "+
                                         "               <div class=\"col-12 col-lg-4 pt-3 text-center text-lg-right\">    "+
                                         "                   <button class=\"btn btn-secondary btn-sm mt-lg-3\" onclick=\"$('#divContenidoProducto_"+intCount+"').remove();\">Eliminar</button>  "+
                                         "               </div>    "+
                                         "           </div>     "+
                                         "       </div>     "+
                                         "   </div>  "+
                                         "</div>");
            $("#slcClasificacionPadreProducto_"+intCount).select2({
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
            intCount++;
            
            
        }
        
        function fntDeleteProducto(intIndex){
            
            $("#hidProductoDelete_"+intIndex).val('Y');
            $("#divContenidoProducto_"+intIndex).remove();
            
        }
        
        function fntEditProducto(intIndex){
                    
            $("#divVistaEdicion_"+intIndex).show();
            $("#divVistaPrevia_"+intIndex).hide();
            $("#hidProductoUpdate_"+intIndex).val('Y');
                        
        }
        
        function fntSaveProducto(){
            
            var formData = new FormData(document.getElementById("frmProducto"));
                
            $(".preloader").fadeIn();
            $.ajax({
                url: "proes.php?setProductoClasificacion=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    fntDrawSeccionProductoClasificacionPadre('<?php print ($intClasificacionPadre)?>');
                    $("#modalGeneral").modal("hide");
            
                }
                        
            });
            
        }
        
    </script>        
    <?php   
                       
    die();
}

if( isset($_GET["setProductoClasificacion"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceClasificacionPadre = isset($_POST["hidClasificacionPadre"]) ? intval(fntCoreDecrypt($_POST["hidClasificacionPadre"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "hidIdPlaceProducto" ){
                                                     
            $intPlaceProducto = isset($_POST["hidIdPlaceProducto_{$arrExplode[1]}"]) ? intval($_POST["hidIdPlaceProducto_{$arrExplode[1]}"]) : "";
            $strNombre = isset($_POST["txtNombre_{$arrExplode[1]}"]) ? addslashes($_POST["txtNombre_{$arrExplode[1]}"]) : "";
            $strPrecio = isset($_POST["txtPrecio_{$arrExplode[1]}"]) ? addslashes($_POST["txtPrecio_{$arrExplode[1]}"]) : "";
            $strDescripcion = isset($_POST["txtDescripcion_{$arrExplode[1]}"]) ? addslashes($_POST["txtDescripcion_{$arrExplode[1]}"]) : "";
            
            $boolProductoDelete = isset($_POST["hidProductoDelete_{$arrExplode[1]}"]) && trim($_POST["hidProductoDelete_{$arrExplode[1]}"]) == "Y" ? true : false;
            $boolProductoEdit = isset($_POST["hidProductoUpdate_{$arrExplode[1]}"]) && trim($_POST["hidProductoUpdate_{$arrExplode[1]}"]) == "Y" ? true : false;
            
            if( $boolProductoDelete ){
                                   
                $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo WHERE id_place_producto = {$intPlaceProducto}";
                $objDBClass->db_consulta($strQuery);
                                    
                $strUrlArchivo = "images/place/product/".fntCoreEncrypt($intPlaceProducto).".png";
                
                if( file_exists($strUrlArchivo) )
                    unlink($strUrlArchivo);
                
                $strQuery = "DELETE FROM place_producto WHERE id_place_producto = {$intPlaceProducto}";
                $objDBClass->db_consulta($strQuery);
                
                die();
            }
            elseif( $boolProductoEdit ){
                
                if( !$intPlaceProducto ){
                    
                    $strQuery = "INSERT INTO place_producto(id_place, id_clasificacion_padre, nombre, precio, descripcion)
                                                     VALUES( {$intPlace}, {$intPlaceClasificacionPadre}, '{$strNombre}', '{$strPrecio}', '{$strDescripcion}' )";
                    $objDBClass->db_consulta($strQuery);
                    $intPlaceProducto = $objDBClass->db_last_id();
                    
                }
                else{
                    
                    $strQuery = "UPDATE place_producto
                                 SET    nombre = '{$strNombre}',
                                        precio = '{$strPrecio}', 
                                        descripcion = '{$strDescripcion}' 
                                 WHERE  id_place_producto = {$intPlaceProducto} ";
                    $objDBClass->db_consulta($strQuery);
                    
                }
                
                if( $intPlaceProducto ){
                    
                    if( isset($_FILES["flImgProducto_{$arrExplode[1]}"]) && $_FILES["flImgProducto_{$arrExplode[1]}"]["size"] > 0 ){
                        
                        $arrNameFile = explode("_", $_FILES["flImgProducto_{$arrExplode[1]}"]["name"]); 
                        $strUrlArchivo = "images/place/product/".fntCoreEncrypt($intPlaceProducto).".webp";
                        rename($_FILES["flImgProducto_{$arrExplode[1]}"]["tmp_name"], $strUrlArchivo);
                        fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceProducto));
                
                        $strQuery = "UPDATE place_producto
                                     SET    logo_url = '{$strUrlArchivo}' 
                                     WHERE  id_place_producto = {$intPlaceProducto} ";
                        $objDBClass->db_consulta($strQuery);
                                
                    }
                    
                    $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo WHERE id_place_producto = {$intPlaceProducto}";
                    $objDBClass->db_consulta($strQuery);
                    
                    if( isset($_POST["slcClasificacionPadreProducto_{$arrExplode[1]}"]) && count($_POST["slcClasificacionPadreProducto_{$arrExplode[1]}"]) > 0 ){
                        
                        while( $arrTMP = each($_POST["slcClasificacionPadreProducto_{$arrExplode[1]}"]) ){
                            
                            $strQuery = "INSERT INTO place_producto_clasificacion_padre_hijo(id_place_producto, id_clasificacion_padre_hijo)
                                                                                    VALUES({$intPlaceProducto}, {$arrTMP["value"]})";
                            $objDBClass->db_consulta($strQuery);
                        }
                        
                    }
                    
                    
                }
                
            }
            
        }
        
    }    
    
    fntCalculoPrecioPlaceProducto($intPlace, $intPlaceClasificacionPadre, true, $objDBClass);
    
    die();
}

if( isset($_GET["setPlaceProductoDescripcion"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceProducto = isset($_POST["hidPlaceProducto"]) ? intval(fntCoreDecrypt($_POST["hidPlaceProducto"])) : 0;
    $strDescripcion = isset($_POST["txtDescripcion"]) ? addslashes(fntEliminarTildes($_POST["txtDescripcion"])) : "";
    $strNombre = isset($_POST["txtNombre"]) ? addslashes(fntCoreClearToQuery($_POST["txtNombre"])) : "";
    $strDescripcionEN = isset($_POST["txtDescripcionEN"]) ? addslashes(fntEliminarTildes($_POST["txtDescripcionEN"])) : "";
    $strNombreEN = isset($_POST["txtNombreEN"]) ? addslashes(fntCoreClearToQuery($_POST["txtNombreEN"])) : "";
    $sinPrecio = isset($_POST["txtPrecio"]) ? addslashes($_POST["txtPrecio"]) : "";
    $intCategoriaEspecial = isset($_POST["slcCategoriaEspecial"]) ? intval($_POST["slcCategoriaEspecial"]) : "";
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "UPDATE place_producto
                 SET    descripcion = '{$strDescripcion}',
                        nombre = '{$strNombre}',
                        descripcion_en = '{$strDescripcionEN}',
                        nombre_en = '{$strNombreEN}',
                        precio = '{$sinPrecio}',
                        id_categoria_especial = '0',
                        estado = 'A'
                 WHERE  id_place_producto = {$intPlaceProducto} ";
    $objDBClass->db_consulta($strQuery);
    
    $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo WHERE id_place_producto = {$intPlaceProducto} ";
    $objDBClass->db_consulta($strQuery);
    
    if( isset($_POST["slcClasificacionPadreProducto"]) && count($_POST["slcClasificacionPadreProducto"]) > 0 ){
                        
        while( $arrTMP = each($_POST["slcClasificacionPadreProducto"]) ){
            
            $strQuery = "INSERT INTO place_producto_clasificacion_padre_hijo(id_place_producto, id_clasificacion_padre_hijo)
                                                                    VALUES({$intPlaceProducto}, {$arrTMP["value"]})";
            $objDBClass->db_consulta($strQuery);
        }
        
    }
    
    
    die();    
    
}

if( isset($_GET["setSaveDatosPrincipales"]) ){
    
    include "core/dbClass.php";
    
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    
    $strTitulo = isset($_POST["txtTitulo"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtTitulo"])) : 0;
    $strDescripcion = isset($_POST["txtDescripcion"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtDescripcion"])) : 0;
    $strDescripcion = isset($_POST["txtDescripcion"]) ? addslashes(fntEliminarTildes($_POST["txtDescripcion"])) : 0;
    $strTelefono = isset($_POST["txtTelefono"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtTelefono"])) : 0;
    $strDireccion = isset($_POST["txtDireccion"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtDireccion"])) : 0;
    
    if( $intPlace ){
        
        $objDBClass = new dbClass();
        
        $strTituloBusqueda = fntGetNameEstablecimeintoFiltro($strTitulo, true, $objDBClass);
    
        $strQuery = "UPDATE place
                     SET    titulo = '{$strTitulo}',
                            descripcion = '{$strDescripcion}', 
                            telefono = '{$strTelefono}', 
                            direccion = '{$strDireccion}',
                            texto_autocomplete = '{$strTituloBusqueda}' 
                     WHERE  id_place = {$intPlace} ";
        $objDBClass->db_consulta($strQuery);
        
        
        while( $rTMP = each($_POST) ){
            
            $arrExplode = explode("_", $rTMP["key"]);
            
            if( $arrExplode[0] == "hidKeyDia" ){
                
                $intPlaceHorario = isset($_POST["hidPlaceHorario_{$arrExplode[1]}"]) ? intval($_POST["hidPlaceHorario_{$arrExplode[1]}"]) : 0;
                $intKeyDia = isset($_POST["hidKeyDia_{$arrExplode[1]}"]) ? intval($_POST["hidKeyDia_{$arrExplode[1]}"]) : 0;
                $strClose = isset($_POST["chkClose_{$arrExplode[1]}"]) ? "Y" : "N";
                $intHoraInicio = isset($_POST["slcHorarioInicio_{$arrExplode[1]}"]) ? trim($_POST["slcHorarioInicio_{$arrExplode[1]}"]) : 0;
                $intHoraFin = isset($_POST["slcHorarioFin_{$arrExplode[1]}"]) ? trim($_POST["slcHorarioFin_{$arrExplode[1]}"]) : 0;
                           
                $intHoraInicio = $intHoraInicio.":00";
                $intHoraFin = $intHoraFin.":00";
                
                if( !$intPlaceHorario ){
                    
                    $strQuery = "INSERT INTO place_horario (id_place, id_dia, hora_inicio, hora_fin, cerrado)
                                                    VALUES( {$intPlace}, {$intKeyDia}, '{$intHoraInicio}', '{$intHoraFin}', '{$strClose}')";
                    $objDBClass->db_consulta($strQuery);                
                }
                else{
                    
                    $strQuery = "UPDATE place_horario
                                 SET    id_dia = '{$intKeyDia}',
                                        hora_inicio = '{$intHoraInicio}',
                                        hora_fin = '{$intHoraFin}',
                                        cerrado = '{$strClose}'
                                 WHERE  id_place_horario = {$intPlaceHorario} ";
                    $objDBClass->db_consulta($strQuery);
                                    
                }
                
                
            }
            
        }
            
    
            
    }
    
    die();
}

if( isset($_GET["setUbicacionPlace"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
        
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $strLat = isset($_GET["lat"]) ? $_GET["lat"] : 0;
    $strLng = isset($_GET["lng"]) ? $_GET["lng"] : 0;
    
    $strQuery = "UPDATE place
                 SET    ubicacion_lat = '{$strLat}',
                        ubicacion_lng = '{$strLng}'
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
    
    
    die();    
}

if( isset($_GET["fntDrawSeccion"]) ){
    
    include "core/dbClass.php";
    
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $intDiaSemana = isset($_GET["diaS"]) ? intval($_GET["diaS"]) : 0;
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intPlaceProducto = isset($_GET["pro"]) ? intval(fntCoreDecrypt($_GET["pro"])) : 0;
    
    if( $intSeccion == 1 ){
        
        $objDBClass = new dbClass();  
        
        $arrGetGaleriaPlace = fntGetPlaceProductoGaleria($intPlaceProducto, true, $objDBClass);
        
        ?>
        <div class="slidePlace slider center" style="" >
            
            <?php
            
            if( is_array($arrGetGaleriaPlace) &&  count($arrGetGaleriaPlace) > 0 ){
                
                $i = 1;
                while( $rTMP = each($arrGetGaleriaPlace) ){
                    
                    ?>
                    <div class="p-0 imgGaleryDIV_<?php print $i;?>" style="" yordi="<?php print $i;?>" style="">
                        <img src="imgInguate.php?t=up&src=<?php print $rTMP["value"]["url_imagen"]?>" class="img-fluid p-3 imgSlideGallery imgGalery_<?php print $i;?>" style="border-radius: 8% !important; width: 100%; height: auto; object-fit: cover;"> 
                    </div>
                    
                    <?php
                    $i++;                        
                }                
                
            }
            else{
                
                for( $i = 1 ; $i <= 5; $i++ ){
                    
                    ?>
                    <div class="p-0  imgGaleryDIV_<?php print $i;?>" style="" yordi="<?php print $i;?>" style="">
                        <img src="images/Noimagee.jpg?1" class="img-fluid p-3 imgSlideGallery imgGalery_<?php print $i;?>" style="border-radius: 10% !important; "> 
                    </div>
                    <?php
                }
            }
            
                
            ?>
            
        </div>
        <script>
            
            $(document).ready(function() {
                
                $('.slidePlace').slick({
                    arrows: false,
                    centerMode: true,
                    infinite: true,
                    centerPadding: '60px',
                    slidesToShow: 4,
                    speed: 200,
                    autoplay: true,
                    variableWidth: false,
                    rtl:false,
                    responsive: [ {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1,
                            autoplay: false,
                            speed: 200
                        }
                    }]
                        
                });
                
                $('.slidePlace').on('afterChange', function(event, slick, currentSlide, nextSlide){
                    
                    var dataId = $('.slick-current').attr("data-slick-index");    
                    dataId++;
                    $(".imgSlideGallery").removeClass("slick-center-Border");
                    $(".imgGalery_"+dataId).addClass("slick-center-Border");
                
                });
                
                $('.slidePlace').slick('slickGoTo',"0");
                
                var sinWindowsHeight = $(window).height();
        
                $('.imgSlideGallery ').each(function (){
                    
                    $(this).height( ( sinWindowsHeight * ( 30 / 100 ) )+'px');
                    
                });  
                    
            });
            
        </script>
        <?php            
    }
    
    if( $intSeccion == 2 ){
        
        $objDBClass = new dbClass();  
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrPlaceHorario = fntGetPlaceHorario($intPlace, true, $objDBClass);
        
        $arrDiasSemana = getSemana();
        $arrHorario = getHoras24Horas();
        
        
        ?>
        <div class="row m-0 p-0">
            <div class="col-12 m-0 p-0">
                <h4 class="text-center text-lg-left m-0 p-0">
                    <?php print $arrDatosPlace["titulo"]?>
                </h4>
                <h4 class="m-0 p-0 text-center text-lg-left" style="line-height: 0.1;"> 
                    <?php
                    for( $i = 1; $i <= 5 ;  $i++ ){
                        
                        $strEstrella =  $i <=  $arrDatosPlace["estrellas"] ? "fa-star" : "fa-star-o";
                        ?>
                        <i class="fa  <?php print $strEstrella;?> text-color-theme m-0 p-0" style="font-size: 12px;"></i>
                        <?php
                    }
                    ?>
                    
                </h4>
                <p class="text-center text-lg-left text-secondary">
                    <?php print $arrDatosPlace["descripcion"]?>
                </p>
                <h6 class="text-center text-lg-left" style="line-height: 0.1;">
                    <a href="tel:3434343434" class="text-dark">
                        <i class="fa fa-phone pl-1 text-color-theme"></i>
                        <?php print $arrDatosPlace["telefono"]?>
                        
                    </a>
                </h6>
                <h6 class="text-center text-lg-left" style="line-height: 0.1;">
                    <a href="tel:3434343434" class="text-dark" style="line-height: 1;">
                        <i class="fa fa-map-marker pl-1 text-color-theme"></i>
                        <?php print $arrDatosPlace["direccion"]?>
                        
                    </a>
                </h6>
                <h6 class="text-center text-lg-left" style="line-height: 0.1;">
                    <a href="tel:3434343434" class="text-dark">
                        <i class="fa fa-clock-o pl-1 text-color-theme"></i>
                        Horario
                        
                    </a>
                </h6>
                <div class="accordion" id="accordionExample">
                    <div class="card " style="border: 0px;">
                        <div class="card-header p-0 m-0 bg-white" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; border: 0px;">
                            
                            <div class="row">
                                <div class="col-12 text-lg-left text-center">
                                    
                                    <?php
                                    
                                    if( isset($arrPlaceHorario[$intDiaSemana]) ){
                                        
                                        if( $arrPlaceHorario[$intDiaSemana]["cerrado"] == "Y" ){
                                            
                                            ?>
                                            &nbsp;
                                            &nbsp;
                                            <span class="text-danger font-weight-bold">Cerrado</span> 
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <i class="fa fa-angle-down text-color-theme"></i>
                                            <?php
                                            
                                        }
                                        else{
                                            
                                            ?>
                                            &nbsp;
                                            &nbsp;
                                            <span class="text-success font-weight-bold">Hoy Abierto</span> : 
                                            <?php print $arrPlaceHorario[$intDiaSemana]["hora_inicio_text"]?> - <?php print $arrPlaceHorario[$intDiaSemana]["hora_fin_text"]?>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <i class="fa fa-angle-down text-color-theme"></i>
                                            <?php
                                                    
                                        }
                                        
                                    }
                                        
                                    ?>
                                    
                                </div>
                            </div>
                            
                            
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body p-0 m-0">
                                <dl class="row mb-0 mt-4 lis-line-height-2 ">
                                    <?php
                                    
                                    if( count($arrPlaceHorario) > 0 ){
                                        
                                        while( $rTMP = each($arrPlaceHorario) ){
                                            ?>
                                            <dt class="col-6 text-left"><?php print $arrDiasSemana[$rTMP["key"]]["nombre"]?></dt>
                                            
                                            <?php
                                            if( $rTMP["value"]["cerrado"] == "Y" ){
                                                ?>
                                                <dd class="col-6 text-left text-danger">Cerrado</dd>
                                        
                                                <?php
                                            }
                                            else{
                                                ?>
                                                <dd class="col-6 text-left">
                                                    <?php print $rTMP["value"]["hora_inicio_text"];?> - <?php print $rTMP["value"]["hora_fin_text"];?>
                                                </dd>
                                        
                                                <?php
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    ?>
                                </dl>
                            </div>
                        </div>
                    </div>
                
                </div>
                <div class="btn-group btn-group-sm" style="display: none;">
                
                    <button type="button" class="btn btn-xs btn-light m-0" onclick="window.open('https://www.facebook.com/');" >
                        <i class="fa fa-facebook text-color-theme"></i>
                    </button>
                
                    <button type="button" class="btn btn-xs btn-light m-0" onclick="window.open('https://www.instagram.com/');">
                        <i class="fa fa-instagram text-color-theme"></i>
                    </button>
                
                    <button type="button" class="btn btn-xs btn-light m-0" onclick="window.open('https://www.twitter.com/');">
                        <i class="fa fa-twitter text-color-theme"></i>
                    </button>
                
                    <button type="button" class="btn btn-xs btn-light m-0" onclick="window.open('https://www.youtube.com/');">
                        <i class="fa fa-youtube text-color-theme"></i>
                    </button>
                
                </div>
            </div>
        </div>
        <?php
    }
    
    if( $intSeccion == 3 ){
        
        $objDBClass = new dbClass();
        $arrPlaceProducto = fntGetPlaceProductoDatos($intPlaceProducto, true, $objDBClass);
        
        ?>     
        <div class="card-body p-4">    
            <div class="row">
                            
                <div class="col-12 col-lg-6">
                    <h4 class=" text-color-theme"><?php print $strLenguaje == "es" ? $arrPlaceProducto["nombre"] : $arrPlaceProducto["nombre_en"]?></h4>
                    
                </div>
                            
                <div class="col-12 col-lg-6">
                
                    <h4 class="text-right text-color-theme">Q <?php print number_format($arrPlaceProducto["precio"], 2)?></h4>
                    
                </div>
                <div class="col-12">
                    
                    <?php print !empty($arrPlaceProducto["descripcion"]) ? ( $strLenguaje == "es" ? $arrPlaceProducto["descripcion"] : $arrPlaceProducto["descripcion_en"] ) : "Agrega tu descripcion"?>
                            
                </div>        
                    
                
            </div>
        <?php
    
    }
        
    die();
}

if( isset($_GET["setGaleriaPlaceProducto"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceProducto = isset($_POST["hidPlaceProducto"]) ? intval(fntCoreDecrypt($_POST["hidPlaceProducto"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "hidIdGaleria" ){
            
            $intPlaceGaleria = isset($_POST["hidIdGaleria_{$arrExplode[1]}"]) ? intval($_POST["hidIdGaleria_{$arrExplode[1]}"]) : 0;
            $boolUpdate = isset($_POST["hidUpdate_{$arrExplode[1]}"]) && $_POST["hidUpdate_{$arrExplode[1]}"] == "Y" ? true : false;
            $boolDelete = isset($_POST["hidDelete_{$arrExplode[1]}"]) && $_POST["hidDelete_{$arrExplode[1]}"] == "Y" ? true : false;
            $boolInsert = isset($_POST["hidInsert_{$arrExplode[1]}"]) && $_POST["hidInsert_{$arrExplode[1]}"] == "Y" ? true : false;
            
            if( $boolDelete ){
                
                $strQuery = "DELETE FROM place_producto_galeria WHERE id_place_producto_galeria = {$intPlaceGaleria}";
                $objDBClass->db_consulta($strQuery);
                $strUrlArchivo = "../../file_inguate/place_interno/producto/proes_".fntCoreEncrypt($intPlaceGaleria).".png";
                if( file_exists($strUrlArchivo) )
                    unlink($strUrlArchivo);
                
                                
            }
            else{
                
                if( isset($_FILES["flFileImage_{$arrExplode[1]}"]) && $_FILES["flFileImage_{$arrExplode[1]}"]["size"] > 0 ){
                    
                    if( !$intPlaceGaleria ){
                        
                        $strQuery = "INSERT INTO place_producto_galeria(id_place_producto)
                                                        VALUES({$intPlaceProducto})";
                        $objDBClass->db_consulta($strQuery);
                        $intPlaceGaleria = $objDBClass->db_last_id();
                        
                    }
                    
                    $strUrlArchivo = "../../file_inguate/place_interno/producto/proes_".fntCoreEncrypt($intPlaceGaleria).".webp";
                    rename($_FILES["flFileImage_{$arrExplode[1]}"]["tmp_name"], $strUrlArchivo);
                    
                    fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceGaleria));
                    
                    
                    $strQuery = "UPDATE place_producto_galeria
                                 SET    url_imagen = '{$strUrlArchivo}' 
                                 WHERE  id_place_producto_galeria = {$intPlaceGaleria}";
                    $objDBClass->db_consulta($strQuery);
                    
                }
                
            }
                        
        }
        
    }
    
    $strQuery = "UPDATE place_producto
                 SET    estado = 'Y'
                 WHERE  id_place_producto = {$intPlaceProducto}";
    $objDBClass->db_consulta($strQuery);
        
    die();
}

if( isset($_GET["getDrawSeccionProductoClasificacionPadre"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intClasificacionPadre = isset($_GET["key"]) ? intval(($_GET["key"])) : 0;
    
    $arrPlaceProducto = fntGetPlaceProducto($intPlace, $intClasificacionPadre, true, $objDBClass);
    
    if( count($arrPlaceProducto) > 0 ){
        
        $arrClasificacionPadreHijo = fntGetPlaceClasificacionPadreHijo($intPlace, $intClasificacionPadre, true, $objDBClass);
    
        while( $rTMP = each($arrPlaceProducto) ){
            ?>
            
            <div class="row p-0 m-0">
                <div class="col-lg-2 col-4 p-0 m-0 mt-3 text-right border-bottom">
                            
                    <img style="width: 100%; height: auto;" src="<?php print !empty($rTMP["value"]["logo_url"]) ? $rTMP["value"]["logo_url"] : "images/Noimagee.jpg?1"?>" class="img-fluid rounded">
                    
                    
                </div>
                <div class="col-lg-10 col-8 p-0 m-0 pl-3 mt-3 border-bottom pb-1">
                    
                    <div class="row m-0 p-0">
                        <div class="col-12 m-0 p-0">
                            <h5 class="text-dark text-left lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                <?php print $rTMP["value"]["nombre"];?>                   
                    
                            </h5>
                        
                        </div>
                    </div>
                        
                    <h5 class=" text-secondary text-left m-0 p-0" style="font-size: 14px;">
                        <?php print $rTMP["value"]["descripcion"];?>               
                    </h5>
                    <h5 class="text-left m-0 p-0 text-color-theme" style="font-size: 14px;">
                        Q <?php print $rTMP["value"]["precio"];?>             
                    </h5>
                    <h3 class="text-left  m-0 p-0" style="font-size: 16px;">
                        
                        <?php
                        reset($arrClasificacionPadreHijo);                                                
                        while( $arrTMP = each($arrClasificacionPadreHijo) ){
                            
                            if( isset($rTMP["value"]["clasificacion_padre_hijo"][$arrTMP["key"]]) ){
                                
                                ?>
                                <span class="badge  text-white text-background-theme" style="">
                                    <?php print $strLenguaje == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?>                        
                                </span>
                                <?php
                                
                            }                                            
                            
                        }                            
                        
                        ?>
                    </h3>
                
                </div>
            </div>
            
            <?php
        }
            
    }
        
    die();
}

if( isset($_GET["setSolicitudEnvio"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $intPlaceProducto = isset($_POST["hidPlaceProducto"]) ? intval(fntCoreDecrypt($_POST["hidPlaceProducto"])) : 0;
    $intCategoriaEspecial = isset($_POST["slcCategoriaEspecial"]) ? intval($_POST["slcCategoriaEspecial"]) : 0;
    $intFrecuenciaPago = isset($_POST["slcFrecuenciaPago"]) ? intval($_POST["slcFrecuenciaPago"]) : 0;
    $strDescripcion = isset($_POST["txtDescripcionSolicitud"]) ? fntCoreClearToQuery($_POST["txtDescripcionSolicitud"]) : 0;
    $strFechaInicio = isset($_POST["txtFechaInicio"]) ? ($_POST["txtFechaInicio"]) : 0;
    $strFechaFin = isset($_POST["txtFechaFin"]) ? ($_POST["txtFechaFin"]) : 0;
    
    $strQuery = "INSERT INTO sol_categoria_especial(id_place_producto, id_categoria_especial, id_frecuencia, estado, descripcion,
                                                    fecha_inicio, fecha_fin)
                                             VALUES({$intPlaceProducto}, {$intCategoriaEspecial}, {$intFrecuenciaPago}, 'S', '{$strDescripcion}',
                                                    '{$strFechaInicio}', '{$strFechaFin}')";
    $objDBClass->db_consulta($strQuery);
    
    
    die();    
    
}

if( $intPlaceProducto ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    
    $arrDatosPlaceProducto = fntGetPlaceProductoDatos($intPlaceProducto, true, $objDBClass);
    $intPlace = $arrDatosPlaceProducto["id_place"];
    $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
    $arrCatalogoFondo = fntCatalogoFondoPlacePublico();
    
    $strColorHex = isset($arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"]) ? $arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"] : $arrCatalogoFondo[1]["color_hex"];

    
    
}

fntDrawHeaderPublico($boolMovil);    

if( ( isset($_SESSION["_open_antigua"]["core"]["tipo"]) && $_SESSION["_open_antigua"]["core"]["tipo"] == 1  ) || ( isset($_SESSION["_open_antigua"]["core"]["login"]) && $_SESSION["_open_antigua"]["core"]["login"] 
    && $_SESSION["_open_antigua"]["core"]["id_usuario"] == $arrDatosPlace["id_usuario"] ) ){
    
    $_SESSION["boolUserPanelLogIn"] = true;
        
}
else{
    
    $_SESSION["boolUserPanelLogIn"] = false;
    
}


?>   
<link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
<link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> 
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>

<link rel="stylesheet" type="text/css" href="dist/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="dist/slick/slick-theme.css"/>

<link href="dist/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet"> 


<input type="hidden" id="hidOrigen" value="<?php print $intOrigen;?>">
<input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intKey);?>">
<input type="hidden" id="hidTipo" value="<?php print $intTipo;?>">
<input type="hidden" id="hidTexto" value="<?php print $strTexto;?>">
<input type="hidden" id="hidRango" value="<?php print $strRango;?>">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">


<style>
    
    .temalugar_border_back{
        border: 2px solid #<?php print $strColorHex?>; 
        color: #<?php print $strColorHex?> !important;    
        background: white;
    }    
    .text-background-theme {
        background-color: #<?php print $strColorHex?> !important;
        
    }
    .text-color-theme {
        color: #<?php print $strColorHex?> !important;
    
    }

    .btn-theme-circular {
        background-color: white;
        color: #<?php print $strColorHex?>;
        border: 2px solid #<?php print $strColorHex?>;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
        position:absolute;
        top:0%;
        right:2%;
        z-index: 99;

    }       
      
      <?php
      
      if( $boolMovil ){
          ?>
          
            
    .container__ {
      display: flex;
      justify-content: space-around;
      align-items: flex-start;
      height: 100%;
    }
          
          .police {
            position: -webkit-sticky;
              position: sticky;
              top: 0;
            }
          <?php
      }
      
      ?>  
         
                    
    .fondo {
     -webkit-box-sizing: content-box;
      -moz-box-sizing: content-box;
      box-sizing: content-box;
      padding: 54px 55px 55px;
      border: none;
      -webkit-border-radius: 0 0 0 2000px / 0 0 0 230px;
      border-radius: 0 0 0 2000px / 0 0 0 230px;
      font: normal 16px/1 "Times New Roman", Times, serif;
      color: black;
      -o-text-overflow: ellipsis;
      text-overflow: ellipsis;
     background: -moz-linear-gradient(top, rgba(46,57,117,0.83) 0%, rgba(46,57,117,0.83) 1%, rgba(41,52,119,0.93) 40%, rgba(39,50,119,0.95) 53%, rgba(39,50,119,1) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='red', endColorstr='blue',GradientType=0 ); 
    }          
    
</style>
<div class="container-fluid m-0 p-0" >
    
    <div class="row m-0 p-0">
    
        <div class="col-12 p-0 m-0 text-center" >
        
            <img src="<?php print isset($arrCatalogoFondo[$arrDatosPlace["id_tema"]]["url_fondo"]) ? $arrCatalogoFondo[$arrDatosPlace["id_tema"]]["url_fondo"] : $arrCatalogoFondo[1]["url_fondo"]?>" style="width: 100%; height: auto;">
                   
            <div class="m-0 p-0 pt-2 pt-lg-4  text-center" style="position: absolute; top: 0px; width: 100%;"> 
                
                <?php
                
                fntDrawHeaderPrincipal(1, true, $objDBClass);
                
                ?>        
                
                <!-- Contenido -->       
                <div class="row p-0 m-0 justify-content-center mt-3" style="">
                    <div class="col-12 p-0 m-0 ">
                        
                        <?php
                        
                        if( $_SESSION["boolUserPanelLogIn"] ){
                            ?>
                            <div class="btn-theme-circular" onclick="fntCargarModalSeccion('1')">
                                &nbsp;<i class="fa fa-pencil"></i>&nbsp;
                            </div>
                            <?php
                        }
                        
                        ?>
                             
                        
                        <div id="divContenidoSeccion_1">
                            <!--
                            <div class="slidePlace slider center" style="" >
                                
                                <?php
                                for( $i = 1 ; $i <= 5; $i++ ){
                                    
                                    ?>
                                    <div class="p-0  imgGaleryDIV_<?php print $i;?>" style="" yordi="<?php print $i;?>" style="">
                                        <img src="dist/images/gallery<?php print $i;?>.jpg" class="img-fluid p-3 imgSlideGallery imgGalery_<?php print $i;?>" style="border-radius: 10% !important; "> 
                                    </div>
                                    <?php
                                }
                                ?>
                                
                            </div>
                            -->
                        </div>
                                                
                    </div>
                </div>
                
                <div class="row p-0 m-0 pl-1 justify-content-center container__" style="">
                    
                    <div class="col-12 col-lg-4 p-1 m-0 bg-white police" style="" id="divContenidoSeccion_2">
                                  
                    </div>
                    
                    <div class="col-12 col-lg-8 p-0 m-0 pl-1 pr-1 bg-white" style="">
                        
                        
                        <div class="row p-0 m-0 ">
                            <div class="col-12 p-0 m-0">
                                
                                <?php
                            
                                if( $_SESSION["boolUserPanelLogIn"] ){
                                    ?>
                                    <div class="btn-theme-circular" onclick="fntCargarModalSeccion('3')">
                                        &nbsp;<i class="fa fa-pencil"></i>&nbsp;
                                    </div>
                                    <?php
                                }
                                
                                ?>
                                <div class="text-left" id="divContenidoSeccion_3"></div>
                                    
                                <div class="row p-0 m-0">
                                    <div class="col-12">    
                                        
                                        
                                        <button class="btn btn-sm btn-outline btn-outline-light " onclick="location.href = 'https://maps.google.com/?q=<?php print $arrDatosPlace["ubicacion_lat"]?>,<?php print $arrDatosPlace["ubicacion_lng"]?>' " style="border:  1px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>;" >
                                            Google Maps <i class="fa fa-paper-plane"></i>
                                         </button>
                                         
                                         <button class="btn btn-sm btn-outline btn-outline-light " onclick="location.href = 'https://waze.com/ul?ll=<?php print $arrDatosPlace["ubicacion_lat"]?>,<?php print $arrDatosPlace["ubicacion_lng"]?>&z=10' " style="border:  1px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>;" >
                                            Waze <i class="fa fa-rocket"></i>
                                         </button>
                                         <br>
                                         <br>
                                        
                                        <?php
                                        
                                        if( !empty($arrDatosPlace["direccion"]) ){
                                            $strApiKey = "AIzaSyABJ9worirb3vAoxbghUZj_rNUTrfTtRPY";

                                            ?>
                                            <iframe style="width: 100%; height: 450px; border: 0px;"  src="https://www.google.com/maps/embed/v1/place?key=<?php print $strApiKey?>&q=<?php print $arrDatosPlace["direccion"]?>" allowfullscreen></iframe>
                                         
                                            <?php
                                        }
                                        
                                        ?>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                
                    </div>
                    
                </div>
            
                <?php
                        
                fntDrawFooter($strColorHex);
                
                ?>
            
            </div>
                   
        </div>
    </div>
    
    <?php
        
    if( !$_SESSION["boolUserPanelLogIn"] )
        fntShowBarNavegacionFooter(true, isset($arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"]) ? $arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"] : $arrCatalogoFondo[1]["color_hex"]);
    
    ?>
    
</div>



<!-- Modal -->
<div class="modal fade" id="modalGeneral" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg" style="min-width: 90%;" role="document">
        <div class="modal-content" id="modalGeneraContenido">
        
            <div class="modal-header" id="modalGeneralHeader">
            
                <h5 class="modal-title">Modal title</h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
            </div>
            
            <div class="modal-body" id="modalGeneralBody">
                <br>
            </div>
            
            <div class="modal-footer" id="modalGeneralFooter">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            
            </div>
        
        </div>
    </div>
</div>
   

    
   
<script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
<script type="text/javascript" src="dist/slick/slick.min.js"></script>
<script src="dist/clockpicker/jquery-clockpicker.min.js"></script>
<script src="dist/js/jsAntigua.js"></script>
<script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
        
        
<style>
    
    .flotante_change_theme {
        
        position:absolute;
        top: 1%;
        left:1%;
    
    }

    .slick-center-Border {
        
        padding: 0px !important;
        
    }
    
    .flotante {
            
        display:none;
        position:fixed;
        top:90%;
        right:2%;
        background-color: #<?php print $strColorHex?>;
        color: white;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
    
    }
</style>
<script >
           
    var d = new Date();
    var intDiaSemana = d.getDay()
   
    $(document).ready(function() {
              
        fntDrawSeccion(1);
        fntDrawSeccion(2);
        fntDrawSeccion(3);
        
        $(".divContenidoClasificaionPadre").each(function (){
            
            arrExplode = this.id.split("_");
            
            fntDrawSeccionProductoClasificacionPadre(arrExplode[1]);
            
        });
        
        <?php
        
        if( $arrDatosPlace["place_pregunta"] == "Y" ){
            
            ?>
            
            swal({   
                title: "Aviso",   
                text: "Existe informacion de tu negocio la deseas autocompletar?.",
                type: "success",   
                showConfirmButton: true 
            },function(isConfirm){   
                
                $.ajax({
                    url: "proes.php?setAutoCompletarInfoPlace=true&p=<?php  print fntCoreEncrypt($intPlace)?>&place=<?php print $arrDatosPlace["place_id"]?>", 
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                
                        location.href = "placeup.php?p=<?php  print fntCoreEncrypt($intPlace)?>";
                    
                    }
                            
                });
                                    
            });
            
            <?php
                        
        }
        
        ?>
            
    });
    
    function initializeMap() {

        center = new google.maps.LatLng('<?php print $arrDatosPlace["ubicacion_lat"]?>', '<?php print $arrDatosPlace["ubicacion_lng"]?>');

        var mapOptions = {
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: center
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        
        var markerOptions = { 
            position: center, 
            draggable: false
        }
        
        markerInicial = new google.maps.Marker(markerOptions);
        markerInicial.setMap(map);
        
        map.setCenter(center);
        
    }
              
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
    }
    
    function fntDrawSeccionProductoClasificacionPadre(strKey){
        
        $.ajax({
            url: "proes.php?getDrawSeccionProductoClasificacionPadre=true&place=<?php print fntCoreEncrypt($intPlace);?>&key="+strKey, 
            success: function(result){
                
                $("#divContenidoClasificacionPadreo_"+strKey).html(result);
                
            }
        });
        
    }
    
    function fntDrawSeccion(intSeccion, strKey){
        
        strKey = !strKey ? "" : strKey;
                        
        $.ajax({
            url: "proes.php?fntDrawSeccion=true&place=<?php print fntCoreEncrypt($intPlace);?>&pro=<?php print fntCoreEncrypt($intPlaceProducto);?>&&seccion="+intSeccion+"&key="+strKey+"&diaS="+intDiaSemana, 
            success: function(result){
                
                $("#divContenidoSeccion_"+intSeccion).html(result);
                
            }
        });
        
    }
    
    <?php
    
    if( $_SESSION["boolUserPanelLogIn"] ){
        ?>
        
        function fntShowModalProductoClasificacionPadre(strKey){
            
            $.ajax({
                url: "proes.php?getShowModalProductoClasificacionPadre=true&place=<?php print fntCoreEncrypt($intPlace);?>&key="+strKey, 
                success: function(result){
                    
                    $("#modalGeneral").html(result);
                    $("#modalGeneral").modal("show");
                    
                }
            });
            
        }
        
        function fntCargarModalSeccion(intSeccion, strKey){
            
            strKey = !strKey ? "" : strKey;
                            
            $.ajax({
                url: "proes.php?fntDrawSeccionForm=true&place=<?php print fntCoreEncrypt($intPlace);?>&pro=<?php print fntCoreEncrypt($intPlaceProducto);?>&&seccion="+intSeccion+"&key="+strKey, 
                success: function(result){
                    
                    $("#modalGeneral").html(result);
                    $("#modalGeneral").modal("show");
                    
                }
            });
            
        }
        
        function fntSetUbicacionPlace(){
                
            $.ajax({
                url: "proes.php?setUbicacionPlace=true&place=<?php print fntCoreEncrypt($intPlace);?>&lat="+$("#hidLatUbicacion").val()+"&lng="+$("#hidLngUbicacion").val(), 
                success: function(result){
                    
                    swal({
                        title: "Data set",
                        //text: "Select An Option",
                        type: "success",
                        showConfirmButton: true,
                        //timer: 2000
                    });    
                
                }
            });
            
            
        }
        
        function fntSetFondoPlace(intKeyCatalogo){
            
            var formData = new FormData();
            formData.append("intFondoPlace", intKeyCatalogo);                    
            formData.append("hidPlace", "<?php print fntCoreEncrypt($intPlace)?>");
                                
            $(".preloader").fadeIn();
            $.ajax({
                url: "proes.php?setFondoPlace=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                    location.href = "placeup.php?p=<?php print fntCoreEncrypt($intPlace)?>&len=<?php $strLenguaje?>";
                    
                }
                        
            });
            
        }
        
        
        <?php
    }
    
    ?>
    
</script>
<?php
fntDrawFooterPublico($strColorHex); 
?>
