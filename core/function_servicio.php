<?php                               
date_default_timezone_set("Etc/UTC");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Twilio\Rest\Client;

define("sesion", fntGetCookieSesion());

function drawdebug($strContent = ""){
    if($strContent == "" || $strContent == null || $strContent == false){
        var_dump($strContent);
    }
    else{
        print_r("<pre style='text-align: left!important; direction:ltr;'>\r\r");
        print_r($strContent);
        print_r("\r\r</pre>");
    }
}

function fntEnviarCorreo($strCorreo, $strSubjet, $strBody, $intCuenta = 0, $boolDebug = false){

    
    $arr[0]["correo"] = "info@inguate.com";
    $arr[0]["clave"] = "intercorps8";
    
    $arr[1]["correo"] = "register@inguate.com";
    $arr[1]["clave"] = "intercorps8";
    
    require 'dist_interno/PHPMailer/src/Exception.php';
    require 'dist_interno/PHPMailer/src/PHPMailer.php';
    require 'dist_interno/PHPMailer/src/SMTP.php';


    $mail = new PHPMailer();                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = $boolDebug;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.yandex.com';  // Specify main and backup SMTP servers
        //$mail->Host = 'smtp.yandex.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $arr[$intCuenta]["correo"];                 // SMTP username
        //$mail->Username = 'support@intercorps.org';                 // SMTP username
        $mail->Password = $arr[$intCuenta]["clave"];                           // SMTP password
        //$mail->Password = 'xrplmupjhfhbpgvf';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to
        
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        ));
        
        //Recipients
        //$mail->setFrom('yordi@ritzycapital.com', 'PoweDialer');
        $mail->setFrom($arr[$intCuenta]["correo"], 'Inguate');
        $mail->addAddress($strCorreo);     // Add a recipient
        //$mail->addAddress("support@intercorps.org");     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('support@intercorps.org');
        //$mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $strSubjet." - ".$strCorreo;
        $mail->Body    = $strBody;

        //drawdebug("Adsfdsdsafds");
        
        $mail->send(); 
        
        //echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }
    
}

function fntEnviarCorreoSinEncabezado($strCorreo, $strSubjet, $strBody){

    

    $mail = new PHPMailer();                              // Passing `true` enables exceptions
    //try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        //$mail->Host = 'smtp.yandex.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'info@ritzycapital.com';                 // SMTP username
        //$mail->Username = 'support@intercorps.org';                 // SMTP username
        $mail->Password = 'intercorps';                           // SMTP password
        //$mail->Password = 'xrplmupjhfhbpgvf';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to
        
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        ));
        
        //Recipients
        //$mail->setFrom('yordi@ritzycapital.com', 'PoweDialer');
        $mail->setFrom('info@advansales.com', 'Advansales');
        $mail->addAddress($strCorreo);     // Add a recipient
        //$mail->addAddress("support@intercorps.org");     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('support@intercorps.org');
        //$mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $strSubjet." - ".$strCorreo;
        $mail->Body    = $strBody;

        //drawdebug("Adsfdsdsafds");
        
        $mail->send(); 
        
        //echo 'Message has been sent';
    //} catch (Exception $e) {
        //echo 'Message could not be sent.';
        //echo 'Mailer Error: ' . $mail->ErrorInfo;
    //}
    
}

function fntGetAmenidades($boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
        
    }    
    
    $arrAmenidad = array();
    
    $strQuery = "SELECT id_amenidad,
                        nombre,
                        tag_en,
                        tag_es
                 FROM   amenidad   
                 ";
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrAmenidad[$rTMP["id_amenidad"]]["id_amenidad"] = $rTMP["id_amenidad"];
        $arrAmenidad[$rTMP["id_amenidad"]]["nombre"] = $rTMP["nombre"];
        $arrAmenidad[$rTMP["id_amenidad"]]["tag_en"] = $rTMP["tag_en"];
        $arrAmenidad[$rTMP["id_amenidad"]]["tag_es"] = $rTMP["tag_es"];
                
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arrAmenidad;
    
}

function fntGetClasificacion($boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
        
    }    
    
    $strQuery = "SELECT id_clasificacion,
                        nombre, 
                        tag_en,
                        tag_es
                 FROM   clasificacion
                 ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrClasificacion[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
        $arrClasificacion[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
        $arrClasificacion[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
        $arrClasificacion[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
        
                    
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arrClasificacion;
    
}

function fntClasificacionPadre($boolInterno = false, $objDBClass = null, $strKey){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }    
    
    $strQuery = "SELECT clasificacion.nombre,
                        clasificacion.id_clasificacion,
                        clasificacion.tag_en,
                        clasificacion.tag_es,
                        clasificacion_padre.id_clasificacion_padre,
                        clasificacion_padre.nombre nombrePadre,
                        clasificacion_padre.tag_en tag_enPadre,
                        clasificacion_padre.tag_es tag_esPadre
                 FROM   clasificacion,
                        clasificacion_padre
                 WHERE  clasificacion.id_clasificacion IN ({$strKey})
                 AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                 ";
    $arrInfo = array();             
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrInfo[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
        $arrInfo[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
        $arrInfo[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
        $arrInfo[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
        $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["nombrePadre"] = $rTMP["nombrePadre"];
        $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_enPadre"] = $rTMP["tag_enPadre"];
        $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_esPadre"] = $rTMP["tag_esPadre"];
                    
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arrInfo;
    
}

function fntClasificacionPadreHijo($boolInterno = false, $objDBClass = null, $strKey = ""){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }    
    
    $strQuery = "SELECT clasificacion_padre.nombre,
                        clasificacion_padre.id_clasificacion_padre,
                        clasificacion_padre.tag_en,
                        clasificacion_padre.tag_es,
                        
                        clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        clasificacion_padre_hijo.nombre nombreHijo,
                        clasificacion_padre_hijo.tag_en tag_enHijo,
                        clasificacion_padre_hijo.tag_es tag_esHijo
                        
                 FROM   clasificacion_padre,
                        clasificacion_padre_hijo
                 WHERE  clasificacion_padre.id_clasificacion_padre IN ({$strKey})
                 AND    clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                 ";
    $arrInfo = array();             
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrInfo[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arrInfo[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
        $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
        $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
        $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
        $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["nombreHijo"] = $rTMP["nombreHijo"];
        $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_enHijo"] = $rTMP["tag_enHijo"];
        $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_esHijo"] = $rTMP["tag_esHijo"];
                    
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arrInfo;
    
}

function fntClasificacionPadreHijoDetalle($boolInterno = false, $objDBClass = null, $strKey = ""){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }    
    
    $strQuery = "SELECT clasificacion_padre_hijo.nombre,
                        clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        clasificacion_padre_hijo.tag_en,
                        clasificacion_padre_hijo.tag_es,
                        
                        clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle,
                        clasificacion_padre_hijo_detalle.nombre nombreDetalle,
                        clasificacion_padre_hijo_detalle.tag_en tag_enDetalle,
                        clasificacion_padre_hijo_detalle.tag_es tag_esDetalle
                        
                 FROM   clasificacion_padre_hijo,
                        clasificacion_padre_hijo_detalle
                 WHERE  clasificacion_padre_hijo.id_clasificacion_padre_hijo IN ({$strKey})
                 AND    clasificacion_padre_hijo.id_clasificacion_padre_hijo = clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo 
                 ";
    $arrInfo = array();             
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["tag_en"] = $rTMP["tag_en"];
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["tag_es"] = $rTMP["tag_es"];
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["id_clasificacion_padre_hijo_detalle"] = $rTMP["id_clasificacion_padre_hijo_detalle"];
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["nombreDetalle"] = $rTMP["nombreDetalle"];
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_enDetalle"] = $rTMP["tag_enDetalle"];
        $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_esDetalle"] = $rTMP["tag_esDetalle"];
                    
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arrInfo;
    
}

function getSemana(){
    
    $arr[0]["nombre"] = lang["domingo"];
    $arr[1]["nombre"] = lang["lunes"];
    $arr[2]["nombre"] = lang["martes"];
    $arr[3]["nombre"] = lang["miercoles"];
    $arr[4]["nombre"] = lang["jueves"];
    $arr[5]["nombre"] = lang["viernes"];
    $arr[6]["nombre"] = lang["sabado"];
    
    return $arr;
    
}

function getHoras24Horas(){
    
    $arr[1]["nombre"] = "1:00 am";
    $arr[2]["nombre"] = "2:00 am";
    $arr[3]["nombre"] = "3:00 am";
    $arr[4]["nombre"] = "4:00 am";
    $arr[5]["nombre"] = "5:00 am";
    $arr[6]["nombre"] = "6:00 am";
    $arr[7]["nombre"] = "7:00 am";
    $arr[8]["nombre"] = "8:00 am";
    $arr[9]["nombre"] = "9:00 am";
    $arr[10]["nombre"] = "10:00 am";
    $arr[11]["nombre"] = "11:00 am";
    $arr[12]["nombre"] = "12:00 am";
    $arr[13]["nombre"] = "13:00 am";
    $arr[14]["nombre"] = "14:00 am";
    $arr[15]["nombre"] = "15:00 am";
    $arr[16]["nombre"] = "16:00 am";
    $arr[17]["nombre"] = "17:00 am";
    $arr[18]["nombre"] = "18:00 am";
    $arr[19]["nombre"] = "19:00 am";
    $arr[20]["nombre"] = "20:00 am";
    $arr[21]["nombre"] = "21:00 am";
    $arr[22]["nombre"] = "22:00 am";
    $arr[23]["nombre"] = "23:00 am";
    
    return $arr;
}

function fntCoreEncrypt($string, $key = "Intercorps9PlaceAntigua") {

    
   $result = '';
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
   }
   return base64_encode($result);
}

function fntCoreDecrypt($string, $key = "Intercorps9PlaceAntigua") {   
    $string = str_replace(" ", "+", $string);
   $result = '';                                     
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
   }             
   return $result;
}

function fntCoreStringQueryLetra($strParametroBusqueda){
    
    return fntEliminarTildes($strParametroBusqueda);
    return  preg_replace('/[^A-Za-z0-9ñÑ\s]/', '', $strParametroBusqueda);
    
}

function fntGetPlaceClasificacionPadreHijo($intPlace, $intPlaceClasificacionPadre, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strQuery = "SELECT clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        clasificacion_padre_hijo.nombre,
                        clasificacion_padre_hijo.tag_en,
                        clasificacion_padre_hijo.tag_es
                 FROM   place_clasificacion_padre_hijo,
                        clasificacion_padre_hijo
                 WHERE  place_clasificacion_padre_hijo.id_place = {$intPlace}
                 AND    clasificacion_padre_hijo.id_clasificacion_padre = {$intPlaceClasificacionPadre} 
                 AND    place_clasificacion_padre_hijo.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo ";    
    
    
    $strQuery = "SELECT clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        clasificacion_padre_hijo.nombre,
                        clasificacion_padre_hijo.tag_en,
                        clasificacion_padre_hijo.tag_es
                 FROM   clasificacion_padre_hijo
                 WHERE  clasificacion_padre_hijo.id_clasificacion_padre = {$intPlaceClasificacionPadre} ";    
    
    $arr = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
        $arr[$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
        $arr[$rTMP["id_clasificacion_padre_hijo"]]["tag_en"] = $rTMP["tag_en"];
        $arr[$rTMP["id_clasificacion_padre_hijo"]]["tag_es"] = $rTMP["tag_es"];
                
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arr;
    
}

function fntGetPlaceClasificacionPadreHijoDetalle($intPlace, $intPlaceClasificacionPadreHijo, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strQuery = "SELECT place_clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle,
                        place_clasificacion_padre_hijo_detalle.nombre,
                        place_clasificacion_padre_hijo_detalle.tag_en,
                        place_clasificacion_padre_hijo_detalle.tag_es
                 FROM   place_clasificacion_padre_hijo_detalle,
                        clasificacion_padre_hijo_detalle
                 WHERE  place_clasificacion_padre_hijo_detalle.id_place = {$intPlace}
                 AND    clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = {$intPlaceClasificacionPadreHijo} 
                 AND    place_clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle = clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle ";    
    
    $arr = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_clasificacion_padre_hijo_detalle"]]["id_clasificacion_padre_hijo_detalle"] = $rTMP["id_clasificacion_padre_hijo_detalle"];
        $arr[$rTMP["id_clasificacion_padre_hijo_detalle"]]["nombre"] = $rTMP["nombre"];
        $arr[$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_en"] = $rTMP["tag_en"];
        $arr[$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_es"] = $rTMP["tag_es"];
                
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arr;
    
}

function fntGetPlaceProducto($intPlace, $intPlaceClasificacionPadre = 0, $boolInterno = false, $objDBClass = null, $intPlaceProducto = 0){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $intPlaceClasificacionPadre = intval($intPlaceClasificacionPadre);
    
    $strFiltro = $intPlaceClasificacionPadre ? "AND place_producto.id_clasificacion_padre = {$intPlaceClasificacionPadre} " : "";
    $strFiltro2 = $intPlaceProducto ? "AND place_producto.id_place_producto = {$intPlaceProducto} " : "";
    
    $strQuery = "SELECT place_producto.id_place_producto,
                        place_producto.id_clasificacion_padre,   
                        place_producto.nombre,   
                        place_producto.precio,   
                        place_producto.descripcion,   
                        place_producto.nombre_en,   
                        place_producto.descripcion_en,   
                        place_producto.logo_url,   
                        place_producto.destacado,   
                        place_producto.tipo,   
                        place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo,   
                        place_producto_galeria.id_place_producto_galeria, 
                        place_producto_galeria.url_imagen  
                        
                 FROM   place_producto
                            LEFT JOIN   place_producto_clasificacion_padre_hijo
                                ON  place_producto_clasificacion_padre_hijo.id_place_producto =  place_producto.id_place_producto
                                
                            LEFT JOIN   place_producto_galeria
                                ON  place_producto_galeria.id_place_producto =  place_producto.id_place_producto
                                
                 WHERE  place_producto.id_place = {$intPlace} 
                 {$strFiltro}
                 {$strFiltro2}
                         ";
    $arr = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
        $arr[$rTMP["id_place_producto"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arr[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
        $arr[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
        $arr[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
        $arr[$rTMP["id_place_producto"]]["destacado"] = $rTMP["destacado"];
        $arr[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
        $arr[$rTMP["id_place_producto"]]["tipo"] = $rTMP["tipo"];
        $arr[$rTMP["id_place_producto"]]["nombre_en"] = $rTMP["nombre_en"];
        $arr[$rTMP["id_place_producto"]]["descripcion_en"] = $rTMP["descripcion_en"];
        
        if( intval($rTMP["id_clasificacion_padre_hijo"]) )
            $arr[$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
        
        if( intval($rTMP["id_place_producto_galeria"]) ){
            
            $arr[$rTMP["id_place_producto"]]["galeria"][$rTMP["id_place_producto_galeria"]]["id_place_producto_galeria"] = $rTMP["id_place_producto_galeria"];
            $arr[$rTMP["id_place_producto"]]["galeria"][$rTMP["id_place_producto_galeria"]]["url_imagen"] = $rTMP["url_imagen"];
            
        }
                
    }
    
    $objDBClass->db_free_result($qTMP);
    
    return $arr;
    
}

function fntGetClasificacionPadre($intPlaceClasificacionPadre, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $intPlaceClasificacionPadre = intval($intPlaceClasificacionPadre);
    
    $strQuery = "SELECT *
                 FROM   clasificacion_padre
                 WHERE  id_clasificacion_padre = {$intPlaceClasificacionPadre} ";        
                 
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);                               
    $objDBClass->db_free_result($qTMP) ;
    return $rTMP;
    
}
                                         
function fntGetPlaceGaleria($intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $intPlace = intval($intPlace);
    
    $strQuery = "SELECT id_place_galeria,
                        url_imagen,
                        orden
                 FROM   place_galeria
                 WHERE  id_place = {$intPlace}
                 ORDER BY orden ASC
                 ";        
        
    $arr = array();         
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_place_galeria"]]["url_imagen"] = $rTMP["url_imagen"];  
        $arr[$rTMP["id_place_galeria"]]["orden"] = $rTMP["orden"];  
        
    }                               
    $objDBClass->db_free_result($qTMP) ;
    return $arr;
    
}

function fntGetPlace($intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strFiltroUsuario = "";
    $strFiltroUsuario2 = "";
    if( sesion["logIn"] ){
        
        $intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
        
        if( intval($intIdUsuario) ){
            
            $strFiltroUsuario = " , usuario_place_favorito.id_usuario_place_favorito ";
            $strFiltroUsuario2 = " LEFT JOIN usuario_place_favorito 
                                       ON   usuario_place_favorito.id_place = place.id_place
                                       AND  usuario_place_favorito.id_usuario = {$intIdUsuario}   ";
        
        }
            
            
    }
    
    $strQuery = "SELECT *
                 FROM   place
                 WHERE  place.id_place = {$intPlace} 
                  ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    return $rTMP;  
    
}

function fntGetPlaceHorario($intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    
    $strQuery = "SELECT id_place_horario,
                        id_dia,
                        DATE_FORMAT(hora_inicio, '%H:%i') hora_inicio,
                        DATE_FORMAT(hora_inicio, '%H:%i %p') hora_inicio_text,
                        DATE_FORMAT(hora_fin, '%H:%i') hora_fin,
                        DATE_FORMAT(hora_fin, '%H:%i %p') hora_fin_text,
                        cerrado    
                 FROM   place_horario
                 WHERE  id_place = {$intPlace} ";
    $arr = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_dia"]]["id_place_horario"] = $rTMP["id_place_horario"];
        $arr[$rTMP["id_dia"]]["id_dia"] = $rTMP["id_dia"];
        $arr[$rTMP["id_dia"]]["hora_inicio"] = $rTMP["hora_inicio"];
        $arr[$rTMP["id_dia"]]["hora_inicio_text"] = $rTMP["hora_inicio_text"];
        $arr[$rTMP["id_dia"]]["hora_fin"] = $rTMP["hora_fin"];
        $arr[$rTMP["id_dia"]]["hora_fin_text"] = $rTMP["hora_fin_text"];
        $arr[$rTMP["id_dia"]]["cerrado"] = $rTMP["cerrado"];
        
    }
    $objDBClass->db_free_result($qTMP);
    return $arr;
    
}

function fntGetPlaceAmenidad($intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    
    $strQuery = "SELECT place_amenidad.id_amenidad,
                        amenidad.tag_en,   
                        amenidad.tag_es   
                 FROM   place_amenidad,
                        amenidad
                 WHERE  place_amenidad.id_place = {$intPlace} 
                 AND    place_amenidad.id_amenidad = amenidad.id_amenidad
                 ";
    $arr = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_amenidad"]]["id_amenidad"] = $rTMP["id_amenidad"];
        $arr[$rTMP["id_amenidad"]]["tag_en"] = $rTMP["tag_en"];
        $arr[$rTMP["id_amenidad"]]["tag_es"] = $rTMP["tag_es"];
        
    }
    $objDBClass->db_free_result($qTMP);
    return $arr;
    
}

function fntCoreClearToQuery($cadena){
    $cadena = strip_tags($cadena);    
    $cadena = str_replace ("*", "", $cadena);
    $cadena = str_replace ("%", "", $cadena);
    $cadena = str_replace ("'", "", $cadena);
    $cadena = str_replace ("#", "", $cadena);
    $cadena = str_replace ("\\", "", $cadena);
    $cadena = str_replace("mysql","",$cadena);
    $cadena = str_replace("mssql","",$cadena);
    $cadena = str_replace("query","",$cadena);
    $cadena = str_replace("insert","",$cadena);
    $cadena = str_replace("into","",$cadena);
    $cadena = str_replace("update","",$cadena);
    $cadena = str_replace("delete","",$cadena);
    $cadena = str_replace("select","",$cadena);
    $cadena = str_replace("Character","",$cadena);
    $cadena = str_replace("MEMB_INFO","",$cadena);
    $cadena = str_replace("IN","",$cadena);
    $cadena = str_replace("OR","",$cadena);
    $cadena = str_replace (";", "", $cadena);
    //$cadena = str_replace (",", "", $cadena);
    $cadena = fntEliminarTildes($cadena);
    return $cadena;
}

function fntCalculoProducto($intProducto){
    
    
    
}

function fntCalculoPrecioPlaceProducto($intPlace, $intPlaceClasificacionPadre, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strQuery = "SELECT id_clasificacion
                 FROM   clasificacion_padre   
                 WHERE  id_clasificacion_padre = {$intPlaceClasificacionPadre} ";
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    $intClasificacion = $rTMP["id_clasificacion"];
    
    $strQuery = "SELECT clasificacion_padre.id_clasificacion,
                        clasificacion_padre.id_clasificacion_padre,
                        place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        place_producto.id_place,
                        place_producto.id_place_producto,
                        place_producto.nombre,
                        place_producto.precio,
                        place_producto.destacado
                 FROM   clasificacion_padre,
                        place_producto
                            LEFT JOIN   place_producto_clasificacion_padre_hijo
                                ON  place_producto.id_place_producto = place_producto_clasificacion_padre_hijo.id_place_producto                                
                   
                        
                 WHERE  clasificacion_padre.id_clasificacion = {$intClasificacion} 
                 AND    place_producto.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre 
                 
                 ";  
    $arrData = array();
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                
        if( $rTMP["destacado"] == "Y" ){
            
            ////////////////////////////////////////////////////////////////////////////////// 
            //Clasificacion       
            //////////////////////////////////////////////////////////////////////////////////
            if( !isset($arrData["clasificacion"][$rTMP["id_clasificacion"]]["min"]) )
                $arrData["clasificacion"][$rTMP["id_clasificacion"]]["min"] = $rTMP["precio"];
                
            if( $rTMP["precio"] <= $arrData["clasificacion"][$rTMP["id_clasificacion"]]["min"] )
                $arrData["clasificacion"][$rTMP["id_clasificacion"]]["min"] = $rTMP["precio"];
            
            if( !isset($arrData["clasificacion"][$rTMP["id_clasificacion"]]["max"]) )
                $arrData["clasificacion"][$rTMP["id_clasificacion"]]["max"] = $rTMP["precio"];
                
            if( $rTMP["precio"] >= $arrData["clasificacion"][$rTMP["id_clasificacion"]]["max"] )
                $arrData["clasificacion"][$rTMP["id_clasificacion"]]["max"] = $rTMP["precio"];
            
            //////////////////////////////////////////////////////////////////////////////////
            //Padre    
            //////////////////////////////////////////////////////////////////////////////////    
            if( !isset($arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["min"]) )
                $arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["min"] = $rTMP["precio"];
                
            if( $rTMP["precio"] <= $arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["min"] )
                $arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["min"] = $rTMP["precio"];
            
            if( !isset($arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["max"]) )
                $arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["max"] = $rTMP["precio"];
                
            if( $rTMP["precio"] >= $arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["max"] )
                $arrData["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["max"] = $rTMP["precio"];
            
            
        }
            
        //////////////////////////////////////////////////////////////////////////////////
        //Hijo    
        ////////////////////////////////////////////////////////////////////////////////// 
        if( intval($rTMP["id_clasificacion_padre_hijo"]) )   {
            
            if( !isset($arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["min"]) )
                $arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["min"] = $rTMP["precio"];
                
            if( $rTMP["precio"] <= $arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["min"] )
                $arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["min"] = $rTMP["precio"];
            
            if( !isset($arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["max"]) )
                $arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["max"] = $rTMP["precio"];
                
            if( $rTMP["precio"] >= $arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["max"] )
                $arrData["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["max"] = $rTMP["precio"];
            
            
        }
                    
    }  
    
    $objDBClass->db_free_result($qTMP);
    
    $arrRango = array();
    while( $rTMP = each($arrData) ) {
        
        while( $arrTMP = each($rTMP["value"]) ){
            
            $sinDiferencia = $arrTMP["value"]["max"] - $arrTMP["value"]["min"];
            
            
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["tipo"] = 1;                                
                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["bajo"]["min"] = $arrTMP["value"]["min"];                                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["bajo"]["max"] = $arrTMP["value"]["min"] + $intRango; 
                                               
                $arrData[$rTMP["key"]][$arrTMP["key"]]["medio"]["min"] = $arrTMP["value"]["min"] + $intRango + 1;                                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["medio"]["max"] = $arrTMP["value"]["min"] + ( $intRango * 2 );                                
                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["alto"]["min"] = $arrTMP["value"]["min"] + ( $intRango * 2 ) + 1;                                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["alto"]["max"] = $arrTMP["value"]["max"];                                
                    
                    
            }
            else{
                                                                                                 
                $arrData[$rTMP["key"]][$arrTMP["key"]]["tipo"] = $arrTMP["value"]["min"] != $arrTMP["value"]["max"] ? 2 : 3;    
                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["bajo"]["min"] = 0;                             
                $arrData[$rTMP["key"]][$arrTMP["key"]]["bajo"]["max"] = 0;
                                            
                $arrData[$rTMP["key"]][$arrTMP["key"]]["medio"]["min"] = $arrTMP["value"]["min"];                             
                $arrData[$rTMP["key"]][$arrTMP["key"]]["medio"]["max"] = $arrTMP["value"]["max"];
                
                $arrData[$rTMP["key"]][$arrTMP["key"]]["alto"]["min"] = 0;                             
                $arrData[$rTMP["key"]][$arrTMP["key"]]["alto"]["max"] = 0;
                
            }
                                            
        }
        
    }
    
    if( isset($arrData["clasificacion"]) ){
        
        reset($arrData["clasificacion"]);
        while( $rTMP = each($arrData["clasificacion"]) ){
            
            $strQuery = "UPDATE clasificacion
                         SET    tipo_rango_precio = '{$rTMP["value"]["tipo"]}',
                                bajo_min = '{$rTMP["value"]["bajo"]["min"]}',
                                bajo_max = '{$rTMP["value"]["bajo"]["max"]}',
                                medio_min = '{$rTMP["value"]["medio"]["min"]}',
                                medio_max = '{$rTMP["value"]["medio"]["max"]}',
                                alto_min = '{$rTMP["value"]["alto"]["min"]}',
                                alto_max = '{$rTMP["value"]["alto"]["max"]}'
                         WHERE  id_clasificacion = {$rTMP["key"]} ";
                         
            $objDBClass->db_consulta($strQuery);
            
        }
    }
    
    if( isset($arrData["clasificacion_padre"]) ){
        
        reset($arrData["clasificacion_padre"]);
        while( $rTMP = each($arrData["clasificacion_padre"]) ){
            
            $strQuery = "UPDATE clasificacion_padre
                         SET    tipo_rango_precio = '{$rTMP["value"]["tipo"]}',
                                bajo_min = '{$rTMP["value"]["bajo"]["min"]}',
                                bajo_max = '{$rTMP["value"]["bajo"]["max"]}',
                                medio_min = '{$rTMP["value"]["medio"]["min"]}',
                                medio_max = '{$rTMP["value"]["medio"]["max"]}',
                                alto_min = '{$rTMP["value"]["alto"]["min"]}',
                                alto_max = '{$rTMP["value"]["alto"]["max"]}'
                         WHERE  id_clasificacion_padre = {$rTMP["key"]} ";
                         
            $objDBClass->db_consulta($strQuery);
            
        }
    }
    
    if( isset($arrData["clasificacion_padre_hijo"]) ){
        
        reset($arrData["clasificacion_padre_hijo"]);
        while( $rTMP = each($arrData["clasificacion_padre_hijo"]) ){
            
            $strQuery = "UPDATE clasificacion_padre_hijo
                         SET    tipo_rango_precio = '{$rTMP["value"]["tipo"]}',
                                bajo_min = '{$rTMP["value"]["bajo"]["min"]}',
                                bajo_max = '{$rTMP["value"]["bajo"]["max"]}',
                                medio_min = '{$rTMP["value"]["medio"]["min"]}',
                                medio_max = '{$rTMP["value"]["medio"]["max"]}',
                                alto_min = '{$rTMP["value"]["alto"]["min"]}',
                                alto_max = '{$rTMP["value"]["alto"]["max"]}'
                         WHERE  id_clasificacion_padre_hijo = {$rTMP["key"]} ";
                         
            $objDBClass->db_consulta($strQuery);
            
        }
    }
    
}

function fntGetPlaceProductoDestacadoForm($intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    
    $strQuery = "SELECT place_producto.id_place_producto,
                        place_producto.nombre,
                        place_producto_clasificacion_padre_hijo.destacado,
                        clasificacion_padre_hijo.id_clasificacion_padre_hijo,                    
                        clasificacion_padre_hijo.nombre nombre_cla,
                        clasificacion_padre_hijo.tag_en
                 FROM   place_producto
                            LEFT JOIN place_producto_clasificacion_padre_hijo
                                    
                                ON  place_producto.id_place_producto = place_producto_clasificacion_padre_hijo.id_place_producto
                                
                                LEFT JOIN clasificacion_padre_hijo
                                    ON  clasificacion_padre_hijo.id_clasificacion_padre_hijo = place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo
                                
                 WHERE  place_producto.id_place = {$intPlace}       ";
    $arr = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        
        $arr["producto"][$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
        $arr["producto"][$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
        $arr["producto"][$rTMP["id_place_producto"]]["destacado"] = $rTMP["destacado"];
        
        if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
            
            $arr["producto"][$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
            $arr["producto"][$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["tag_en"]] = $rTMP["tag_en"];
            $arr["producto"][$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["destacado"]] = $rTMP["destacado"];
            
            
            if( !isset($arr["clasificacion_padre_hijo"][$rTMP["nombre_cla"]]["id_clasificacion_padre_hijo"]) )
                $arr["clasificacion_padre_hijo"][$rTMP["nombre_cla"]]["id_clasificacion_padre_hijo"] = "";
            
            $arr["clasificacion_padre_hijo"][$rTMP["nombre_cla"]]["id_clasificacion_padre_hijo"] .= ( $arr["clasificacion_padre_hijo"][$rTMP["nombre_cla"]]["id_clasificacion_padre_hijo"] == "" ? "" : "," ).$rTMP["id_clasificacion_padre_hijo"];
            $arr["clasificacion_padre_hijo"][$rTMP["nombre_cla"]]["tag_en"] = $rTMP["tag_en"];
            $arr["clasificacion_padre_hijo"][$rTMP["nombre_cla"]]["destacado"] = $rTMP["destacado"];
            
            
        }
                    
    }
    $objDBClass->db_free_result($qTMP);
    
    return $arr;    
    
}

function fntGetDiccionarioInternoIdioma($strLenguaje = "es"){
    
    include "core/lenguaje.php";
        
    return isset($arr[$strLenguaje]) ? $arr[$strLenguaje] : $arr["es"];
    
}

function fntDrawSearchTableProducto($arrProducto, $boolMovil){
               
    ?>
    <table class="p-0 m-0" id="myTable" style="width: 100%;">
        <thead class="" style="display: none;">
            <tr>
                <th >Product</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $intLetraDescripcion = 90;
            while( $rTMP = each($arrProducto) ){
                ?>
                <tr >
                    <td>
                    <!--
                    <td class=" text-right pr-2 " style="<?php print $boolMovil ? "width: 40%;" : "width: 30%;"?>">
                        <img onclick="window.open('pro.php?iden=<?php print fntCoreEncrypt($rTMP["key"])?>')" src="<?php print $rTMP["value"]["logo_url"]?>" style="<?php print $boolMovil ? "width: 160px; height: 120px;" : "width: 280px; height: 200px;"?> cursor: pointer;" class="img-fluid d-md-fle  border border-white lis-border-width-4 rounded m-0" alt="" />
                    </td>
                    <td style="vertical-align: top;">
                        <ul class="list-unstyled my-0 pr-0">
                            <li class="lis-font-weight-600 m-0  <?php print $boolMovil ? "h5" : "h2"?>">
                                <a href="#" class="lis-dark" onclick="window.open('pro.php?iden=<?php print fntCoreEncrypt($rTMP["key"])?>')">
                                    <?php print $rTMP["value"]["nombre"]?>
                                    
                                </a>
                                <?php
                                
                                if( sesion["logIn"] && isset($rTMP["value"]["favorito"]) ){
                                    
                                    ?>
                                    
                                    <i class="fa fa-heart-o" onclick="fntDisplayIconoFavorito('iconProFavoritoSi_<?php print $rTMP["key"]?>', 'iconProFavoritoNo_<?php print $rTMP["key"]?>', true, '<?php print fntCoreEncrypt($rTMP["key"])?>');" id="iconProFavoritoNo_<?php print $rTMP["key"]?>" style="font-size: 20px; cursor: pointer; color: red; <?php print $rTMP["value"]["favorito"] == "Y" ? "display: none;" : ""?>"></i>
                                    <i class="fa fa-heart"   onclick="fntDisplayIconoFavorito('iconProFavoritoSi_<?php print $rTMP["key"]?>', 'iconProFavoritoNo_<?php print $rTMP["key"]?>', false, '<?php print fntCoreEncrypt($rTMP["key"])?>');" id="iconProFavoritoSi_<?php print $rTMP["key"]?>" style="font-size: 20px; cursor: pointer; color: red; <?php print $rTMP["value"]["favorito"] == "Y" ? "" : "display: none;"?>"></i>
                                    
                                    <?php
                                }
                                
                                ?>
                                    
                            </li>
                            <li class="text-secondary mb-2" style="font-size: <?php print $boolMovil ? "13px" : "18px"?> ; line-height: 1.1;">
                                <?php print $rTMP["value"]["titulo"]?>
                            </li>
                            <li class=" " style="font-size: <?php print $boolMovil ? "14px" : "19px"?>; line-height: 1.2;">
                                <?php print strlen($rTMP["value"]["descripcion"]) > $intLetraDescripcion && $boolMovil ? substr($rTMP["value"]["descripcion"], 0, $intLetraDescripcion)."..." :$rTMP["value"]["descripcion"]?>
                            </li>
                            <li class=" <?php print $boolMovil ? "" : "h5"?>" style=" line-height: 1.2;">
                                Q<?php print number_format($rTMP["value"]["precio"], 2)?>
                            </li>
                        </ul>    
                    </td> -->
                        <div class="row p-0 m-0">
                            <div class="col-lg-3 col-4 p-0 m-0 mt-3 text-right ">
                                <img style="<?php print $boolMovil ? "width: 100%; height: auto;" : "width: 240px; height: 160px;"?>" src="<?php print $rTMP["value"]["logo_url"]?>" class="img-fluid rounded">
                            
                            </div>
                            <div class="col-lg-7 col-8 p-0 m-0 pl-3 mt-3">
                                <h5 class="text-dark text-left lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                    <?php print $rTMP["value"]["nombre"]?>                   
                                    
                                </h5>
                                <h5 class="  text-left m-0 p-0" style="font-size: 14px;">
                                    <?php print $rTMP["value"]["titulo"]?>
                                </h5>
                                <h5 class="  text-left m-0 p-0" style="font-size: 14px;">
                                    <?php print strlen($rTMP["value"]["descripcion"]) > $intLetraDescripcion && $boolMovil ? substr($rTMP["value"]["descripcion"], 0, $intLetraDescripcion)."..." :$rTMP["value"]["descripcion"]?>                
                                </h5>
                                <h5 class="text-left m-0 p-0 text-color-theme" style="font-size: 14px;">
                                    Q <?php print number_format($rTMP["value"]["precio"], 2)?>             
                                </h5>
                                
                            </div>
                        </div>
                    </td>
                </tr>
                
                <?php
            }                    
            
            ?>
    
        </tbody>
    </table>
    <script>    
        
        
        
        function fntDisplayIconoFavorito(idObjSi, idObjNo, boolFavorito, strKey){
            
            if( boolFavorito ){
                
                $("#"+idObjSi).show();
                $("#"+idObjNo).hide();
                
            }
            else{
                
                $("#"+idObjSi).hide();
                $("#"+idObjNo).show();
                
            }
            
            $.ajax({
                url: "servicio_core.php?servicio=setProductoFavorito&key="+strKey+"&fav="+( boolFavorito ? "Y" : "N" ), 
                dataType : "json",
                success: function(result){
                }
            });
            
                        
        }
        
    </script>    
    <?php    
    
    
}

function fntGetNameEstablecimeintoFiltro($strTitulo, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strTitulo = strtolower($strTitulo);
              
    $arrExplodeTitulo = explode(" ", $strTitulo);
    
    $strFiltro = "";
    if( count($arrExplodeTitulo) > 0 ){
        
        while( $rTMP = each($arrExplodeTitulo) ){
            
            if( strlen($rTMP["value"]) > 3 ){
                
                $strFiltro .= ( $strFiltro == "" ? "" : "OR" )."
                      LOWER(sinonimo) LIKE LOWER('%{$rTMP["value"]}%')
                ";
            
                
            }
                
        }
        
        
    }
    
    
    $strQuery = "SELECT tag_en,
                        tag_es,
                        sinonimo,
                        id_clasificacion identificador,
                        '1'tipo
                 FROM   clasificacion   
                 WHERE  {$strFiltro}
                 
                 UNION ALL 
                 
                 
                 SELECT tag_en,
                        tag_es,
                        sinonimo,
                        id_clasificacion_padre identificador,
                        '2' tipo
                 FROM   clasificacion_padre   
                 WHERE  {$strFiltro}  
                 
                 UNION ALL 
                 
                 
                 SELECT tag_en,
                        tag_es,
                        sinonimo,
                        id_clasificacion_padre_hijo identificador,
                        '3' tipo
                 FROM   clasificacion_padre_hijo   
                 WHERE  {$strFiltro}  
                 ";  
    $arrTag = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrExplode = explode(" ", $rTMP["sinonimo"]);
    
        if( count($arrExplode) > 0  ){
            
            while( $arrTMP = each($arrExplode) ){
                          
                reset($arrExplodeTitulo);
                while( $arrTMP2 = each($arrExplodeTitulo) ){
                    
                    if( strtolower($arrTMP["value"]) == strtolower($arrTMP2["value"]) ){
                        
                        $strTitulo = str_replace(strtolower($arrTMP["value"]), "", $strTitulo);
                        
                    }
                    
                    
                }
                    
            }
            
        }
        
            
    }
    $objDBClass->db_free_result($qTMP);
    
    return $strTitulo;
        
}

function fntGetNotificacionUsuario($boolInterno = false, $objDBClass = null, $boolVistoPublico = false){
    
    $arr = array();
    
    if( sesion["logIn"] ){
        
        $intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
        
        if( !$boolInterno ){
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();  
            
        }
        
        $strFiltro = "";
        if( $boolVistoPublico ){
            
            $strFiltro = "  AND usuario_alerta.visto = 'N' ";    
            
        }
        
        $intIdUsuario = intval($intIdUsuario);
        
        $strQuery = "SELECT tipo,
                            id_usuario_alerta,
                            identificador,
                            mensaje
                     FROM   usuario_alerta
                     WHERE  id_usuario = {$intIdUsuario}
                     {$strFiltro}
                     ORDER BY add_fecha "; 
                     
        $strQuery = "SELECT usuario_alerta.tipo,
                            usuario_alerta.id_usuario_alerta,
                            usuario_alerta.identificador,
                            usuario_alerta.titulo,
                            usuario_alerta.mensaje,
                            
                            usuario.tipo tipo_usuario,
                            usuario.lenguaje
                     FROM   usuario
                                LEFT JOIN   usuario_alerta
                                    ON  usuario.id_usuario = usuario_alerta.id_usuario
                                    {$strFiltro}
                     WHERE  usuario.id_usuario = {$intIdUsuario}
                     ORDER BY usuario_alerta.add_fecha 
                     ";  
        
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            if( intval($rTMP["id_usuario_alerta"]) )
                array_push($arr, $rTMP);       
        }
        
        $objDBClass->db_free_result($qTMP);
            
    }
    
        
    
    return $arr;
        
}

function fntDrawHeaderPublico($boolMovil = false, $boolChangeNavBarColor = false, $boolIndex = false,
                              $strTitle = "Inguate"  ){
    
    if( !sesion["logIn"] && !$boolIndex ){
        
        $strArrGet = "ar|".basename($_SERVER["PHP_SELF"]);
        if( isset($_GET) && count($_GET)  > 0 ){
            
            while( $rTMP = each($_GET) ){
                
                $strArrGet .= ";".( $rTMP["key"]."|".$rTMP["value"]  );
                        
            }
                        
        }
        
        header('Location: index.php?bref='.$strArrGet);
        
    }
    
    
    ?>
    <!DOCTYPE html>
    <html lang="en">
        
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title><?php print $strTitle;?></title>
            
            <link rel="shortcut icon" href="dist/images/favicon.ico">
            
            <link href="dist/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" type="text/css" href="dist/css/fonts.css"/>
               <meta name="yandex-verification" content="f41d07283a5dc6e5" />
            

            <script src="dist_interno/js/jquery-3.4.0.min.js" ></script>
            <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
            <script src="dist/bootstrap-4.3.1-dist/js/bootstrap.min.js" ></script>
            
            <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
            <script src="dist_interno/sweetalert/sweetalert.min.js"></script>

            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142437492-1"></script>
            <script>
             window.dataLayer = window.dataLayer || [];
             function gtag(){dataLayer.push(arguments);}
             gtag('js', new Date());

             gtag('config', 'UA-142437492-1');
            </script> 
            
        </head>
        <body style="height: 100% !important;" class="clBody ">
            <!-- The core Firebase JS SDK is always required and must be listed first -->
            <script src="https://www.gstatic.com/firebasejs/6.3.1/firebase-app.js"></script>

            <!-- Add Firebase products that you want to use -->
            <script src="https://www.gstatic.com/firebasejs/6.3.1/firebase-auth.js"></script>
            
            <div class="preloader">
                <div class="preloaderdetalle">
                    <img src="dist/images/30.gif" alt="NILA">
                </div>
            </div>
            <style>
                
                .preloader {
                    opacity: 0.5;
                    height: 100%;
                    width: 100%;
                    background: #FFF;
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 9999999;
                }
                
                .preloader .preloaderdetalle {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    -webkit-transform: translate(-50%, -50%);
                    transform: translate(-50%, -50%);
                    width: 120px;
                }
                
            </style>
            
            <!--
            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3&appId=2795767433798083&autoLogAppEvents=1"></script>
             -->

    <?php
        
}

function fntGetCookieSesion(){
    
    $arr["logIn"] = false;
    $arr["lenguaje"] = "es";
    $arr["ubicacion"] = "1";
    $arr["webToken"] = "0";
    $arr["tipoSesion"] = "0";
    $arr["ubicacion"] = "1";
    $arr["nombre"] = "";
    $arr["tipoUsuario"] = "3";
    
    if( isset($_COOKIE["inguate"]) ){
        
        $arr["logIn"] = true;
        
        $arrTMP = explode(",", $_COOKIE["inguate"]);
        $arr["webToken"] = $arrTMP[0];
        $arr["nombre"] = $arrTMP[1];
        $arr["tipoSesion"] = $arrTMP[2];
        $arr["tipoUsuario"] = $arrTMP[3];
        $arr["lenguaje"] = $arrTMP[4];
        $arr["ubicacion"] = $arrTMP[5];
    }
    
    return $arr;
    
}

function fntDrawNavbarPublico($boolShowMiNegocio = true, $boolInterno = false, $objDBClass = null, $boolMovil = false, $boolChangeNavBarColor = false){
    
    $arrCookieSesion = fntGetCookieSesion();
    
    if( $arrCookieSesion["logIn"] ){
        
        $arrAlerta = fntGetNotificacionUsuario(fntCoreDecrypt($arrCookieSesion["webToken"]), $boolInterno, $objDBClass, true);
        
    }
    
    ?>
    <style>
        
        .header nav.navbar-toggleable-md.navbar-light .navbar-nav .dropdown-menu {
            border-radius: 0px;
            padding: 0px;
            border: none;
            right: auto;
            left: -180px;
            margin-top: 0px;
            min-width: 200px;
            box-shadow: 0px 10px 20px 0px rgba(0, 0, 0, 0.18);
            top: 80%; 
        }
        
        .parpadea {
          
          animation-name: parpadeo;
          animation-duration: 1s;
          animation-timing-function: linear;
          animation-iteration-count: infinite;

          -webkit-animation-name:parpadeo;
          -webkit-animation-duration: 1s;
          -webkit-animation-timing-function: linear;
          -webkit-animation-iteration-count: infinite;
        }

        @-moz-keyframes parpadeo{  
          0% { opacity: 1.0; }
          50% { opacity: 0.0; }
          100% { opacity: 1.0; }
        }

        @-webkit-keyframes parpadeo {  
          0% { opacity: 1.0; }
          50% { opacity: 0.0; }
           100% { opacity: 1.0; }
        }

        @keyframes parpadeo {  
          0% { opacity: 1.0; }
           50% { opacity: 0.0; }
          100% { opacity: 1.0; }
        }
        
    </style>
    <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-1 mt-4" style="direction: ltr; ;">
        
        <a class="navbar-brand mr-4 mr-md-5 flo " href="index.php" style="<?php print $boolMovil ? "margin-left: 30%;" : "margin-left: 45%;"?> ">
            <img src=" <?php print $boolChangeNavBarColor ? "images/LOGO-_INGUATE_BLACK.png" : "images/LOGO-_INGUATE_WHITE.png"?>" style="width: 134px; height: 40px;" alt="">
        </a> 

        <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right text-right " style="direction: rtl;"  >
            <button class="btn btn-white float-right"  style="color: white; background: transparent; font-weight: bold; font-size: 20px;"><i class="fa fa-reorder "></i></button>
            <ul class="dl-menu col-12 col-6" style="direction: ltr;">      
            
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false" style="direction: ltr">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Favourite&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    <ul class="dl-submenu">
                        <li class="dl-back bg-dark"><a href="#">back</a></li>
                        <li><a href="fplace.php"> Places&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                        <li><a href="fproduct.php"> Product&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                        
                    </ul>
                </li>

                <?php
                
                if( $arrCookieSesion["logIn"] ){
                    ?>
                    <li >
                        <?php
                        
                        if( $boolShowMiNegocio ){
                            
                            if( $arrCookieSesion["tipoUsuario"] == "3" ){
                                ?>
                                <a href="javascript:void(0)" class="text-white login_form" onclick="fntDrawPreguntaNegocio();">
                                    Tienes un Negocio?
                                </a>
                                <?php                            
                            }
                            
                            if( $arrCookieSesion["tipoUsuario"] == "2" ){
                                ?>
                                <a href="javascript:void(0)" class="text-white login_form" onclick="setLogInDashCookie();">
                                    <i class="fa fa-external-link pr-2"></i> 
                                    &nbsp;
                                    My Negocio
                                    
                                </a>
                                <?php
                            }
                            
                        }
                            
                        
                        ?>
                            
                    </li>
                    <li>
                        <a href="#modal" class="text-white login_form">
                            <i class="fa fa-user-circle pr-2"></i> 
                            &nbsp;
                            <?php print $arrCookieSesion["nombre"];?>
                            
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="fntLogOut();" class="text-white">
                            <i class="fa fa-power-off pr-2"></i> 
                            &nbsp;
                            Sign Out
                            
                        </a>
                    </li>
                
                    <?php
                }
                else{
                    ?>
                    <li><a href="#modal" class="text-white login_form"><i class="fa fa-sign-in pr-2"></i> Sign In | Register</a></li>
                
                    <?php
                }
                
                ?>
                
            </ul>
        </div>

        <div class="collapse navbar-collapse d-none d-lg-block" id="navbarNav" style="height: 50px; ">
            
            <?php
                
            if( $arrCookieSesion["logIn"] ){
                ?>
                
                <ul class=" list-unstyled ml-auto my-2 my-lg-0">
                    <li class="<?php print $boolChangeNavBarColor ? "text-dark" : "text-white"?>   ">
                        
                        <?php
                        
                        if( $boolShowMiNegocio ){
                            
                            if( $arrCookieSesion["tipoUsuario"] == "3" ){
                                ?>
                                <span style="text-decoration: underline; cursor: pointer;" onclick="fntDrawPreguntaNegocio();">
                                    Tienes un Negocio?
                                </span>
                                <?php                            
                            }
                            
                            if( $arrCookieSesion["tipoUsuario"] == "2" ){
                                ?>
                                <span style="text-decoration: underline; cursor: pointer;" onclick="setLogInDashCookie();">
                                    My Negocio
                                    <i class="fa  fa-external-link "></i>
                                </span>
                                <?php
                            }
                            
                        }
                            
                        
                        ?>
                    
                    
                        
                        &nbsp;
                        &nbsp;
                        |
                        &nbsp;
                        &nbsp;
                        <?php print $arrCookieSesion["nombre"];?>
                    </li>
                </ul>
                <?php
                
                if( is_array($arrAlerta) && count($arrAlerta) > 0 ){
                    
                    ?>
                    <ul class="navbar-nav  ">
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link p-1 text-white" href="#" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-bell text-white parpadea m-1"></i>
                            </a>
                            <ul class="dropdown-menu " style="direction: rtl;">
                                <?php
                                
                                while( $rTMP = each($arrAlerta) ){
                                    ?>
                                    
                                    <li style="border-bottom: 2px solid white;">
                                    
                                        <a href="javascript:void(0)" class="bg-secondary text-white" onclick="setAlertaVista('<?php print fntCoreEncrypt($rTMP["value"]["id_usuario_alerta"])?>');">
                                            
                                            <?php print $rTMP["value"]["mensaje"]?>
                                        
                                        </a>
                                        
                                    </li>
                                
                                    <?php
                                }
                                
                                ?>
                            </ul>
                        </li>
                        
                    </ul>
                    <?php   
                                     
                } 
                
                ?>
                    
                <ul class="navbar-nav  ">
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link p-1 text-white " href="#" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-reorder <?php print $boolChangeNavBarColor ? "text-dark" : "text-white"?> "></i>
                        </a>
                        <ul class="dropdown-menu " style="direction: rtl;">
                            
                            
                            <li class="dropdown-submenu" ><a tabindex="-1" href="#" >Favourite</a>
                                <ul class="dropdown-menu">
                                    <li><a href="fplace.php"> Places</a></li>
                                    <li><a href="fproduct.php"> Product</a></li>
                                </ul>
                            </li>
                            <li class="text-dark p-1" style="cursor: pointer;" onclick="fntLogOut();">
                                Log Out
                                &nbsp;&nbsp;
                                <i  class="fa fa-power-off pl-2"></i> 
                            </li>
                        </ul>
                    </li>
                    
                </ul>
                
                
                <?php
            }
            else{
                ?>
                <ul class="list-unstyled my-2 ml-auto my-lg-0">
                    <li class="<?php print $boolChangeNavBarColor ? "text-dark" : "text-white"?>"><i class="fa fa-sign-in pl-2"></i> <a href="#modal" class="text-white login_form">Sign In | Register</a></li>
                </ul>
                
                <?php
            }
            
            ?>
            
        </div>
    </nav>
    
    <?php
    
}

function fntDrawHeaderPrincipal($intTipoBarra = 1, $boolInterno = false, $objDBClass = null, $arrAlerta = array()){
    
    ?>
    <!-- Header WEB -->       
    <div id="divHeaderPrincipaWeb">
        <div class="d-none d-md-block">
            
            <?php
            
            if( $intTipoBarra == 1 ){
                
                $strClassTextColor = "text-white";
                $strClassTextColorMovil = "text-white";
                $strUrlLogo = "images/LOGO-_INGUATE_WHITE.png";
                $strUrlMenu = "images/menu.png";
            
            }
            
            if( $intTipoBarra == 2 ){
                
                $strClassTextColor = "text-dark";
                $strClassTextColorMovil = "text-white";
                $strUrlLogo = "images/LogoInguate.png";
                $strUrlMenu = "images/menu_black.png";
                
            }
            
            
            ?> 
            
            <div class="row justify-content-center m-0">
                
                <div class="col-lg-2 col-md-3 col-sm-3" onclick="location.href = 'index.php';" style="z-index: 1049; cursor: pointer;">
                    <img src="<?php print $strUrlLogo;?>" class="img-fluid " style="width: 100%; height: auto;" >
                </div>
                
            </div>
            
            <div class="row justify-content-end m-0 p-0 pt-3 mt-sm-4" style="position: absolute; top: 0px; width: 100%;">
                <div class="col-11 m-0 p-0  text-center " >
                    
                    <div class="row">
                                    
                        <div class="col-12">
                                     
                            <div class="pos-f-t text-right"> 
                                <div class="collapse show" id="navbarToggleExternalContent"> 
                                    <div class=" p-1 nowrap" >
                                        
                                        <table style="width: 100%;" >
                                            <tr>
                                                <td  style="width: 100%;"  nowrap class="">
                                                
                                        <?php               
                                        
                                        if( sesion["logIn"] ){
                                            
                                            if( sesion["tipoUsuario"] == "1" ){
                                                ?>
                                                
                                                <button type="button" onclick="setLogInDashCookie();" class="btn btn-link <?php print $strClassTextColor;?>" style="cursor: pointer;">Panel <i class="fa fa-external-link"></i></button>
                                                
                                                <?php
                                            }
                                            elseif( sesion["tipoUsuario"] == "3" ){
                                                ?>
                                                
                                                <button type="button" onclick="fntDrawPreguntaNegocio();" class="btn btn-link <?php print $strClassTextColor;?>" style="cursor: pointer;"><?php print lang["tienes_un_negocio"]?> </button>
                                                
                                                <?php
                                            }
                                            elseif( sesion["tipoUsuario"] == "2" ){
                                                ?>
                                                
                                                <button type="button" onclick="setLogInDashCookie();" class="btn btn-link <?php print $strClassTextColor;?>" style="cursor: pointer;"><?php print lang["mi_negocio"]?> <i class="fa fa-external-link"></i> </button>
                                                
                                                <?php    
                                            }
                                            
                                            ?>
                                            
                                            <span class="dropdown">
                                                <button type="button" onclick="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link <?php print $strClassTextColor;?>" style="cursor: pointer;">
                                                    <?php print lang["hola"]." ".sesion["nombre"]?> <i class="fa fa-angle-down"></i>
                                                </button>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <!--<a class="dropdown-item" href="profile.php"><?php print lang["perfil"]?></a>  -->
                                                    <a class="dropdown-item" href="javascript:void(0)" onclick="fntCerrarSesion();"><?php print lang["cerrar_sesion"]?> </a>
                                                </div>
                                            </span>

                                            <?php
                                            
                                            
                                            if( count($arrAlerta) > 0 ){
                                                
                                                ?>
                                                <span class="dropdown">
                                                    <button type="button" id="dropdownAlertaCoreUsuario" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link <?php print $strClassTextColor;?>" style="cursor: pointer;">
                                                        
                                                        <i class="fa fa-bell">
                                                            
                                                            <div style="position: absolute; top: 0px; right: 0px;">
                                                                <div class="badge badge-danger" style="font-size: 10px;"><?php print count($arrAlerta)?></div>
                                                            </div>
                                                            
                                                        </i> 
                                                        
                                                    </button>
                                                    <div class="dropdown-menu border-0" aria-labelledby="dropdownAlertaCoreUsuario" style="background-color: transparent !important; ">
                                                        <?php
                                                        
                                                        while( $rTMP = each($arrAlerta) ){
                                                            
                                                            $strActionOnlcik = "setLogInDashCookie();";
                                                            
                                                            if( $rTMP["value"]["tipo"] == "SPE" || $rTMP["value"]["tipo"] == "SPP" || $rTMP["value"]["tipo"] == "SPR" )
                                                                $strActionOnlcik = "setAlertaVista('".fntCoreEncrypt($rTMP["value"]["id_usuario_alerta"])."', true); ";
                                                            
                                                            ?>
                                                            
                                                            <a class="dropdown-item p-0" href="javascript:void(0)" onclick="<?php print $strActionOnlcik?>" style="">
                                                                <div class="card text-white bg-warning mb-1" style="max-width: 100%;">
                                                                    
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">
                                                                            <?php print $rTMP["value"]["titulo"]?>
                                                                        </h5>
                                                                        <p class="card-text">
                                                                            <?php print $rTMP["value"]["mensaje"]?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        
                                                            <?php
                                                            
                                                        }
                                                        
                                                        ?>
                                                    
                                                    </div>
                                                    
                                                    
                                                    
                                                </span>
                                                <?php
                                                    
                                            }
                                            
                                            
                                        }   
                                        else{
                                            ?>
                                            
                                            <button type="button" onclick="fntOpenModalRegistroCore();" class="btn btn-link <?php print $strClassTextColor;?>" style="cursor: pointer;"><?php print lang["iniciar_sesion_registro"]?></button>
                                        
                                            <?php
                                        }                             
                                        
                                        ?>
                                        
                                        </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                                                                                                                                
                        </div>
                        
                    </div>
                </div>
                <div class="col-1 m-0 p-0 pr-4 text-right">
                             
                        <img src="<?php print $strUrlMenu;?>" style="cursor: pointer;" class="img-fluid" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                    
                    
                </div>
            </div>
        
        </div>
    </div>
    
    <?php    
}

function fntDrawRowContenidoModal_back($boolMovil = false){
    ?>
    <style>
                        
        .modal-backdrop.fade {
          opacity: 0.9;
        }
        
        .backLogiGradienate{
            
            background: rgb(0,4,50);
            background: linear-gradient(152deg, rgba(0,4,50,1) 0%, rgba(29,25,147,1) 75%, rgba(39,50,241,1) 100%);
        }
        
        .clasrInput {
            
            background-color: white !important; 
            color: black !important; 
            border-top: 0px solid black;
            border-bottom: 2px solid #3E53AC;
            border-left: 0px solid black;
            border-right: 0px solid black;
            border-radius: 0px;
            
        }
        .clasrInput::placeholder {
          color: #ACACAC;
          background-color: white;
        }
        
        
        .gradentaBotono{
            
            color: white;
            background: #3E53AC !important;
            border-radius: 10px;
        }
        
        .gradentaBotono2{
            
            color: #3E53AC !important;
            background:  white !important;
            border-radius: 10px;
        }
        
        .gradienteCuadro {
            
            background: rgb(44,38,145);
            background: linear-gradient(180deg, rgba(44,38,145,1) 0%, rgba(28,27,123,1) 22%, rgba(31,31,125,1) 28%, rgba(35,35,135,1) 36%, rgba(92,99,191,1) 92%);
            
        }
        
    </style>
    <div class="row m-0 p-0 gradienteCuadro" style="direction: rtl;">
                            
        <div id="divContenidoFormRegistroModal" class="col-12 col-lg-4 m-0 " style="padding: 4%; height: 100%; direction: ltr;">
            
            <h5 class="text-center" style="margin-top: <?php print $boolMovil ? "20%" : "0%"?>  ; margin-bottom: 40%;" >  
                <img src="images/LogoInguateWhite.png" class="img-fluid " style="width: 50%; height: auto;" >
                
            </h5>
            
            <div class=" " style="position: absolute; top: 50%; z-index: 999; width: 90%; left: 5%; ">
                
                <h4 class="text-center text-white">Registro</h4>
                
                <div class="p-4 bg-white" style="border-radius: 10px;">
                    <div id="divFormRegistro" >
                        <form name="frmModalLogInRegistro" id="frmModalLogInRegistro"  onsubmit="return false;" method="POST">
                            
                            <input type="hidden" name="txtTipoLogIn" id="txtTipoLogIn" value="E">
                            <input type="hidden" name="txtRegistro" id="txtRegistro" value="N">
                            <input type="hidden" name="txtIdentificador" id="txtIdentificador" value="">
                            <input type="hidden" name="txtPictureUrl" id="txtPictureUrl" value="">
                            <input type="hidden" name="txtAction" id="txtAction" value="">
                            <input type="hidden" name="txtDivTokenRecap" id="txtDivTokenRecap" value="">
                               
                               <!--         
                            <div class="row">
                                <div class="col-12 text-center" style="">
                                    <fb:login-button  
                                      scope="public_profile,email"
                                      onlogin="checkLoginState();"
                                      data-width="" data-size="large" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="true"
                                      >
                                    </fb:login-button>     
                                </div>
                            </div>
                            -->
                            <div id="divEmailClaveInput">
                                <div class="row">
                                    <div class="col-12">
                                    
                                                  
                                        <div class="input-group m-0 p-0">
                                            <div class="input-group-prepend m-0 p-0">
                                                
                                                <span id="sptxtEmail" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">@</span>
                                            </div>
                                            
                                            <input type="email" id="txtEmail" name="txtEmail" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Email" style="">
                                        
                                        </div>
                                                                        
                                        
                                    </div>
                                </div>
                                      
                                <div class="row">
                                
                                    <div class="col-12 mt-2">
                                        
                                        <div class="input-group m-0 p-0">
                                            <div class="input-group-prepend m-0 p-0">
                                                
                                                <span id="sptxtClave" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                                    <i class="fa fa-lock"></i>
                                                </span>
                                            </div>
                                                                 
                                            <input type="password" id="txtClave" name="txtClave" class="form-control text-left nofocusBorder clasrInput" placeholder="Contraseña">
                                        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div id="divContenidoLogIn" >     
                            
                                <div class="row">
                                
                                    <div class="col-12 mt-4 text-center">
                                        
                                        <center>
                                            <button id="btnInicionSesion" class="btn btn-block border-0 gradentaBotono " onclick="fntLogInPublico();"  style="width: 70%;">
                                                Iniciar Sesion
                                            </button>           
                                            <button id="btnRegistros1" class="btn btn-block border-0 gradentaBotono2 " onclick="fntSetFormRegristro(true);"  style="width: 70%;">Registrate</button>
                                        </center>                                    
                                    </div>
                                </div>
                                <div class="row">
                                    
                                    <div class="col-12 mt-2 text-right" style="direction: ltr;">
                                                 
                                        <img class="p-1" id="imEsModal" onclick="fntSetIdiomaModalLogIn('es');" src="images/es.png" style="width: 30px; height: 30px; border: 2px solid #3E53AC;">
                                        &nbsp;
                                        <img class="p-1" id="imEnModal" onclick="fntSetIdiomaModalLogIn('en');" src="images/en.png" style="width: 30px; height: 30px; ">
                                        
                                    </div>
                                </div>
                                 
                            </div>
                            <div id="divContenidoRegistro" style="display: none;">    
                            
                                <div class="row">
                            
                                    <div class="col-12 mt-2">
                                              
                                        <div class="input-group m-0 p-0">
                                            <div class="input-group-prepend m-0 p-0">
                                                
                                                <span class="input-group-text  text-center nofocusBorder clasrInput" style="color: #3E53AC !important;">
                                                    <i class="fa fa-id-badge"></i>
                                                </span>
                                            </div>
                                            
                                            <input type="text" id="txtNombre" name="txtNombre" class="form-control text-left form-control-sm  nofocusBorder clasrInput" placeholder="Nombre Completo">
                                        
                                        </div>                       
                                        
                                    </div>
                                </div>
                                
                                <div class="row">
                                
                                    <div class="col-12 mt-2">
                                    
                                        <div class="input-group m-0 p-0">
                                            <div class="input-group-prepend m-0 p-0">
                                                
                                                <span class="input-group-text  text-center nofocusBorder clasrInput" style="color: #3E53AC !important;">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                            </div>
                                            
                                            <input type="text" id="txtTelefono" name="txtTelefono" class="form-control text-left form-control-sm  nofocusBorder clasrInput" placeholder="Telefono">
                                        
                                        </div>
                                                                      
                                        
                                    </div>
                                </div>
                                
                                
                                
                                <div class="row">
                                
                                    <div class="col-12 mt-4 text-center" style="">
                                        
                                        
                                        <button id="btnRegistro" class="btn btn-block border-0 gradentaBotono hide" onclick="fntRegistro();"  style="border: 1px solid white;">Registro</button>
                                        
                                        <center>
                                            <div id="divRecap" style="width: 100%;  "></div>    
                                            <button id="btnRegresarModal" class="btn mt-2 btn-block border-0 gradentaBotono" onclick="fntSetFormRegristro();"  style="border: 1px solid white; width: 70%;">Regresar</button>
                                        
                                        </center>
                                        
                                        
                                    </div>
                                </div>
                                 
                            
                            </div> 
                                               
                        </form>
                    </div>                 
                    <div id="divFormConfirmacionUsuario" style="display: none;">
                        
                        <form name="frmModalLogInRegistroConfirmacion" id="frmModalLogInRegistroConfirmacion"  onsubmit="return false;" method="POST">
                            
                            <input type="hidden" name="hidKeyUsuario" id="hidKeyUsuario" value="">
                            <input type="hidden" name="hidCambioTelefono" id="hidCambioTelefono" value="N">
                            
                            
                            
                            <div class="row" id="divFormCodigoCambioTelefono" style="display: none;">
                                <div class="col-12 mb-2">
                                                                    
                                    <input type="text" id="txtTelefonoNuevoEnvio" name="txtTelefonoNuevoEnvio" class="form-control text-center form-control-sm nofocusBorder clasrInput" placeholder="Nuevo Email" style="">
                                    
                                </div>
                                <div class="col-12 mb-2">
                                                                    
                                    <button id="btnEnviarCodifoModal" class="btn btn-block border-0 gradentaBotono " onclick="fntLogInPublicoConfirmacion(true);"  style="">
                                        Enviar Codigo
                                    </button>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div> 
                            
                                                                      
                            <div class="row" id="divFormCodigo">
                            
                                
                                <div class="col-12">
                                    
                                    <h5 class="mt-2" style="color: white; font-size: 12px;">
                                         
                                        <span id="spnCodioEnviadoModal">Codigo Enviado a:</span>
                                        <span id="spnNumeroEnvioCodigo" style="text-decoration: underline; cursor: pointer; "  >
                                            34343434
                                        </span>
                                        &nbsp; 
                                        &nbsp; 
                                        <div id="divCambiarModal" class="badge badge-info" onclick="fntCambioTelefonoModalRegistro();" style="cursor: pointer;">Cambiar</div>
                                    </h5>
                                                                        
                                    <input type="text" id="txtCodigoConfirmacion" name="txtCodigoConfirmacion" class="form-control text-center form-control-sm nofocusBorder clasrInput" placeholder="Ingresa en Codigo Enviado" style="">
                                    
                                </div>
                                <div class="col-12 mt-2">
                                    
                                    <button id="btnConfirmarUsuarioModal" class="btn btn-block border-0 gradentaBotono " onclick="fntLogInPublicoConfirmacion(false);"  style="">
                                        Confirmar Usuario
                                    </button>
                                    <h5 class="mt-2" style="color: white; font-size: 12px;">
                                        
                                        <span id="spTieneProblemas">
                                            Tienes problemas? 
                                        
                                        </span>
                                        
                                        <span id="spReenvirCodigoEmail" style="text-decoration: underline; cursor: pointer; "  onclick="fntLogInPublicoConfirmacion(true);">
                                            Reenviar Codigo por Email
                                        </span> 
                                    </h5>
                                
                                </div>
                                
                            </div>
                                
                        </form>
                    
                    </div>
                    <br>
                </div>
            </div>
            
        </div>
        
        <div class="col-12 col-lg-8 m-0 p-0">
            
            <div id="crResgistro" class="carousel slide" data-ride="carousel" data-interval="3000">
              <div class="carousel-inner" style="">
              
                <?php
                
                for( $i = 1; $i <= 5; $i++ ){
                    
                    $strActive = $i == 1 ? "active" : "";
                    
                    ?>
                    <div class="carousel-item <?php print $strActive;?> ">
                      <img src="images/index/registro_<?php print $i;?>.jpg" class="img-fluid imgCrRegistro" style="width: 100%; height: 600px; object-fit: cover;" >

                    </div>
                    <?php
                }
                
                ?>
                
              
              </div>
              
            </div>

        </div>
    </div>
    <?php    
}

function fntDrawRowContenidoModal($boolMovil = false, $strLinkRef = "", $strCodigoConfirmacionUsuario = "", $intTrafico = 0){
    
    include "core/dbClass.php";
           /* 
    $arrParametos = explode(";", $strLinkRef);
    
    drawdebug($strLinkRef);
    
    $intPlaceRegistro = 0;
    if( isset($arrParametos[0]) ){
        
        if( isset($arrParametos[0]) ){
            
            $arrParametosDetalle = explode("|", $arrParametos[0]);
            
            //Place UP Registro Referencia
            if( $arrParametosDetalle[0] == "ar" && $arrParametosDetalle[1] == "placeup.php" ){
                
                if( isset($arrParametos[1]) ){
                    
                    $arrParametosDetalle = explode("|", $arrParametos[1]);
                    
                    if( isset($arrParametosDetalle[1]) ){
                        
                        $strUrlPlace = $arrParametosDetalle[1];
                        
                        $strLinkRef = $strUrlPlace;    
                    }
                    
                }                    
                
            }
            
        }
        
    }   
          */
          
    $objDBClass = new dbClass();
           
    $intPlaceRegistro = 0;
    if( !empty($strLinkRef) ){
        
        $strQuery = "SELECT id_place
                     FROM   place
                     WHERE  url_place = '{$strLinkRef}' ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        $intPlaceRegistro = isset($rTMP["id_place"]) ? intval($rTMP["id_place"]) : 0;
        
        
    }
            
    $intUsuarioConfirmacion = 0;
    if( !empty($strCodigoConfirmacionUsuario) ){
        
        $strCodigoConfirmacionUsuario = str_replace(" ", "+", $strCodigoConfirmacionUsuario);
        $arrExplode = explode(",", fntCoreDecrypt($strCodigoConfirmacionUsuario));
        $intUsuarioConfirmacion = intval($arrExplode[0]);
        
        if( $intUsuarioConfirmacion ){
            
            
            $strQuery = "SELECT id_usuario, nombre, lenguaje
                         FROM   usuario
                         WHERE  id_usuario = {$intUsuarioConfirmacion} ";
            //drawdebug($strQuery);
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            
            $intUsuarioConfirmacion = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
            $strNombreConfirmacion = isset($rTMP["nombre"]) ? $rTMP["nombre"] : 0;
            $strLenguaje = isset($rTMP["lenguaje"]) ? $rTMP["lenguaje"] : 0;
            
            $objDBClass->db_close();
                
            
        }
            
        
    } 
    
    $arrPlaceGaleria = array();
    if( $intPlaceRegistro ){
        
        $arrDatosPlace = fntGetPlace($intPlaceRegistro, true, $objDBClass);
        $arrPlaceGaleria = fntGetPlaceGaleria($intPlaceRegistro, true, $objDBClass);
        
        $objDBClass->db_close();
        
        
    }
    
    
    
    ?>
    <style>
                        
        .modal-backdrop.fade {
          opacity: 0.9;
        }
        
        .backLogiGradienate{
            
            background: rgb(0,4,50);
            background: linear-gradient(152deg, rgba(0,4,50,1) 0%, rgba(29,25,147,1) 75%, rgba(39,50,241,1) 100%);
        }
        
        .clasrInput {
            
            background-color: white !important; 
            color: black !important; 
            border-top: 0px solid black;
            border-bottom: 2px solid #3E53AC;
            border-left: 0px solid black;
            border-right: 0px solid black;
            border-radius: 0px;
            
        }
        .clasrInput::placeholder {
          color: #ACACAC;
          background-color: white;
        }
        
        
        .gradentaBotono{
            
            color: white;
            background: #3E53AC !important;
            border-radius: 10px;
        }
        
        .gradentaBotono2{
            
            color: #3E53AC !important;
            background:  white !important;
            border-radius: 10px;
        }
        
        .gradienteCuadro {
            
            background: rgb(44,38,145);
            background: linear-gradient(180deg, rgba(44,38,145,1) 0%, rgba(28,27,123,1) 22%, rgba(31,31,125,1) 28%, rgba(35,35,135,1) 36%, rgba(92,99,191,1) 92%);
            
        }
        
    </style>
    <div class="row m-0 p-0 gradienteCuadro" style="direction: rtl;">
        
        <div id="divContenidoFormRegistroModal" class="col-12 col-lg-4 m-0 " style="padding: 4%; height: 100%; padding-top: 50%; direction: ltr; ">
               
            <input type="hidden" id="hidModalLogInIdioma" name="hidModalLogInIdioma" value="es">
            
            <?php
            
            if( !$intUsuarioConfirmacion ){
                ?>
                <div id="divContenidoLogInFormulario" style="position: absolute; top: <?php print $boolMovil ? "25%" : "10%"?>; z-index: 999; width: 90%; left: 5%; ">
                    
                    <h5 class="text-center" style=" " >  
                        <img src="images/LogoInguateWhite.png" class="img-fluid " style="width: 40%; height: auto;" >
                        
                    </h5>
                    
                    <?php
                    
                    if( $intPlaceRegistro && isset($arrDatosPlace["titulo"]) && !empty($arrDatosPlace["titulo"]) ){
                        ?>
                        <h5 class="text-center text-white" style=" " >  
                            <?php print $arrDatosPlace["titulo"];?>
                        </h5>
                        <?php
                    }
                    
                    ?>
                    
                    <div class="p-4 bg-white" style="border-radius: 10px;">
                        <form name="frmModalLogIn" id="frmModalLogIn"  onsubmit="return false;" method="POST">
                        
                            <div class="row">
                                <div class="col-12">
                                
                                              
                                    <div class="input-group m-0 p-0">
                                        <div class="input-group-prepend m-0 p-0">
                                                               
                                            <span id="sptxtEmail" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">@</span>
                                        </div>
                                        
                                        <input type="email" id="txtEmail" name="txtEmail" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Email" style="">
                                    
                                    </div>
                                                                    
                                    
                                </div>
                            </div>
                                  
                            <div class="row">
                            
                                <div class="col-12 mt-2 text-center">
                                    
                                    <div class="input-group m-0 p-0">
                                        <div class="input-group-prepend m-0 p-0">
                                                               
                                            <span id="sptxtClave" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                                <i class="fa fa-lock"></i>
                                                &nbsp;
                                            </span>
                                        </div>
                                                                        
                                        <input type="password" id="txtClave" name="txtClave" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Contraseña">
                                    
                                    </div>
                                    
                                    <span id="spAlertaCamposLogIn" class="text-danger font-weight-bold " style="font-size: 12px; display: none;">
                                    
                                        Completa los campos obligatorios
                                    
                                    </span>
                                    
                                </div>
                            </div>
                        
                            <div class="row">
                            
                                <div class="col-12 mt-4 text-center">
                                    
                                    <center>        
                                        <button id="btnInicionSesion" class="btn btn-block border-0 gradentaBotono " onclick="fntLogIn__();"  style="width: 70%;">
                                            Iniciar sesión
                                        </button>           
                                        <button id="btnRegistros1" class="btn btn-block border-0 gradentaBotono2 " onclick="fntLogIn_showRegistro(true, false);"  style="width: 70%;">
                                            Registrate
                                        </button>         
                                        <button id="btnOlvidoClave" class="btn btn-block border-0 gradentaBotono2 " onclick="fntDrawFormRestablecerClave();"  style="width: 90%;">
                                            Se te olvidó tu contraseña?
                                        </button>
                                    </center>                                    
                                </div>
                            </div>
                            
                            <div class="row">
                                
                                <div class="col-12 mt-2 text-right" style="direction: ltr;">
                                             
                                    <img class="p-1" id="imEsModal" onclick="fntSetIdiomaModalLogIn('es');" src="images/es.png" style="width: 30px; height: 30px; border: 2px solid #3E53AC;">
                                    &nbsp;
                                    <img class="p-1" id="imEnModal" onclick="fntSetIdiomaModalLogIn('en');" src="images/en.png" style="width: 30px; height: 30px; ">
                                    
                                </div>
                                <div class="col-12 mt-1 text-right" style="direction: ltr;">
                                    
                                    <span class="text-secondary" style="font-size: 10px;">Lenguaje/Language</span>                                             
                                    
                                </div>
                            </div>
                                            
                        </form>
                    </div>
                    
                </div>
                               
                <div id="divContenidoLogInRegistroFormulario" style="position: absolute; top: <?php print $boolMovil ? "25%" : "10%"?>; z-index: 999; width: 90%; left: 5%; display: none; ">
                    
                    <form name="fmrRegistroModal" id="fmrRegistroModal"  onsubmit="return false;" method="POST">
                            
                    <input type="hidden" id="hidTokenRecap" name="hidTokenRecap">
                    <input type="hidden" id="hidReferenciaLugar" name="hidReferenciaLugar" value="<?php print $intPlaceRegistro ? fntCoreEncrypt($intPlaceRegistro) : ""?>">
                    <input type="hidden" id="hidTrafico" name="hidTrafico" value="<?php print $intTrafico ? fntCoreEncrypt($intTrafico) : ""?>">
                    <h3 id="h4Registro" class="text-white text-center" style="font-weight: 500; ">
                        Registro
                    </h3>
                    
                    <?php
                    
                    if( $intPlaceRegistro && isset($arrDatosPlace["titulo"]) && !empty($arrDatosPlace["titulo"]) ){
                        ?>
                        <h5 class="text-center text-white" style=" " >  
                            <?php print $arrDatosPlace["titulo"];?>
                        </h5>
                        <?php
                    }
                    
                    ?>
                       
                    <div class="p-4 bg-white mt-3" style="border-radius: 10px;">
                        
                        <div class="row">
                            
                            <div class="col-12">
                                          
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                          
                                        <span id="sptxtNombreRegistro" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-id-badge"></i>
                                        </span>
                                        
                                    </div>
                                    
                                    <input type="text" id="txtNombreRegistro" name="txtNombreRegistro" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Nombre completo" style="">
                                
                                </div>
                                
                            </div>
                            <div class="col-12 mt-2">
                                          
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                    
                                        <span id="sptxtEmailRegistro" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">@</span>
                                    </div>
                                    
                                    <input type="email" id="txtEmailRegistro" name="txtEmailRegistro" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Email" style="">
                                
                                </div>
                                
                            </div>
                            
                            <div class="col-12 mt-2 text-center">
                                          
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                  
                                        <span id="sptxtTelefonoRegistro" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-phone"></i>
                                            
                                        </span>                
                                                                 
                                        <input type="hidden" id="hidCodigoAreaRegistro" name="hidCodigoAreaRegistro" value="502">           
                                        <span id="sptxtCodigoArea" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important; cursor: pointer;" onclick="fntDrawModalCodigoArea();">
                                            <span id="sphidCodigoAreaRegistro">GT +502</span> &nbsp; <i class="fa fa-angle-down"></i>
                                        </span>
                                        
                                    </div>
                                                                
                                    <input type="tel" id="txtTelefonoRegistro" name="txtTelefonoRegistro" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Telefono" style="">
                                
                                </div>
                                <span id="spAlertaCampos" class="text-danger font-weight-bold " style="font-size: 12px; display: none;">
                                    
                                    Completa los campos obligatorios
                                
                                </span>
                            </div>
                            
                            <div class="col-12 mt-4 text-center">
                                <center>
                                    <div  id="recaptcha-container" ></div>
                                
                                </center>
                                
                                <center>
                                         
                                    <div id="divRecap" style="width: 100%;  "></div>    
                                    
                                    <button id="btnRegistroFormularioRegirstrate" class="btn btn-block border-0 gradentaBotono mt-1 " onclick="fntRecap();"  style="width: 70%; display: none;">
                                        Registrate
                                    </button>
                                    
                                    <button  class="btn btn-block border-0 gradentaBotono mt-1 " onclick="appVerifier.reset(window.recaptchaWidgetId);"  style="width: 70%; display: none;">
                                        ffffffffffff
                                    </button>
                                    
                                    <button id="btnTienesunacuenta" class="btn btn-block border-0 gradentaBotono2 " onclick="fntLogIn_showRegistro(false, false);"  style="width: 70%; color: #808080 !important;">
                                        ¿Ya tienes una cuenta?
                                    </button>
                                    
                                </center>                                    
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    </form>
                    
                </div>
                
                <div id="divContenidoLogInRegistroFormularioConfirmacionUsuario" style="position: absolute; top: <?php print $boolMovil ? "25%" : "10%"?>; z-index: 999; width: 90%; left: 5%; display: none; ">
                    
                    <form name="fmrRegistroModalConfirmacionSMS" id="fmrRegistroModalConfirmacionSMS"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidTokenRegistro" name="hidTokenRegistro">                        
                    <h4 id="h4Resitros2" class="text-white text-center" style="font-weight: 200;">
                        Registro
                    </h4>
                    
                    <br>
                    
                    <div class="p-4 bg-white" style="border-radius: 10px;">
                        
                        <div class="row">
                            
                            <div class="col-12">
                                          
                                <span  class="" style="font-size: 12px; ">
                                    
                                    <span id="spnCodigoenviadoporsmsms">Codigo enviado por SMS a:</span> <span id="spTelefonoenviado">+502 4343 4343</span> <div id="divTextoCambiar" class="badge text-white " style="background: #3E53AC; cursor: pointer;" onclick="location.reload()">Cambiar</div>
                                </span>
                                
                                <br>
                                <br>
                                
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                          
                                        <span id="sptxtCodigoSMS" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-mobile-phone"></i>
                                        </span>
                                        
                                    </div>                    
                                    
                                    <input type="text" id="txtCodigoSMS" name="txtCodigoSMS" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Codigo enviado" style="">
                                
                                </div>
                                
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                    
                                        <span id="sptxtClaveRegistro" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                        
                                    </div>
                                    
                                    <input type="password" id="txtClaveRegristroConfirmacion" name="txtClaveRegristroConfirmacion" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Tu nueva Contraseña" style="">
                                
                                </div>
                                <span id="spAlertaCamposConfirmacion" class="text-danger font-weight-bold " style="font-size: 12px; display: none;">
                                    
                                    Completa los campos obligatorios
                                
                                </span>
                            </div>
                                            
                            <div class="col-12 mt-4 text-center">
                                
                                
                                <center>
                                         
                                    <div id="divRecap" style="width: 100%;  "></div>    
                                    
                                    <button id="btnRegistroFormulario" class="btn btn-block border-0 gradentaBotono mt-1 " onclick="fntGetTokenFirebase();"  style="width: 70%; ">
                                        Confirmar Codigo
                                    </button>
                                    
                                </center>                                    
                            </div>
                            
                        </div>
                        
                        
                    </div>
                    
                    </form>
                    
                </div>
                
                <div id="divAlertaVerificacion" style="position: absolute; top: 10%; z-index: 999; width: 90%; left: 5%; display: none; ">
                    
                    <h5 class="text-center" style=" " >  
                        <img src="images/LogoInguateWhite.png" class="img-fluid " style="width: 40%; height: auto;" >
                    </h5>
                    <h4 id="h4GraciasporRegistro" class="text-white text-center" style="font-weight: 200;">
                        Gracias por registrarte
                    </h4>
                    <h6 id="h4GraciasporRegistro_msn" class="text-white text-center" style="font-weight: 200;">
                        Te hemos enviado un correo electrónico para verificar tu cuenta
                    </h6>
                    
                </div>
                
                <div id="divAlertaVerificacionClave" style="position: absolute; top: 10%; z-index: 999; width: 90%; left: 5%; display: none; ">
                    
                    <h5 class="text-center" style=" " >  
                        <img src="images/LogoInguateWhite.png" class="img-fluid " style="width: 40%; height: auto;" >
                    </h5>
                    <h6 id="h4GraciasporRegistro_msnClave" class="text-white text-center" style="font-weight: 200;">
                        Te hemos enviado un correo electrónico para verificar tu cuenta y restablecer tu contraseña.
                    </h6>
                    
                </div>
                
                <div id="divContenidoFormRestablecerClave" style="position: absolute; top: <?php print $boolMovil ? "25%" : "10%"?>; z-index: 999; width: 90%; left: 5%; display: none; ">
                    
                    <form name="fmrRestablecerClave" id="fmrRestablecerClave"  onsubmit="return false;" method="POST">
                            
                    <input type="hidden" id="hidTokenRecapClave" name="hidTokenRecapClave">
                    <h3 id="h4RestablecerClave" class="text-white text-center" style="font-weight: 500; ">
                        Restablecer contraseña
                    </h3>
                       
                    <div class="p-4 bg-white mt-3" style="border-radius: 10px;">
                        
                        <div class="row">
                                
                            <div class="col-12 mt-2">
                                          
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                    
                                        <span id="sptxtEmailRegistroClave" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">@</span>
                                    </div>
                                    
                                    <input type="email" id="txtEmailRegistroClave" name="txtEmailRegistroClave" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Email" style="">
                                
                                </div>
                                
                            </div>
                            
                            <div class="col-12 mt-2 text-center">
                                          
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                  
                                        <span id="sptxtTelefonoClave" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-phone"></i>
                                            
                                        </span>                
                                                                 
                                        <input type="hidden" id="hidCodigoAreaRegistroClave" name="hidCodigoAreaRegistroClave" value="502">           
                                        <span id="sptxtCodigoAreaClave" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important; cursor: pointer;" onclick="fntDrawModalCodigoAreaClave();">
                                            <span id="sphidCodigoAreaRegistroClave">GT +502</span> &nbsp; <i class="fa fa-angle-down"></i>
                                        </span>
                                        
                                    </div>
                                         
                                    <input type="tel" id="txtTelefonoClave" name="txtTelefonoClave" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Telefono" style="">
                                
                                </div>
                                <span id="spAlertaCamposClave" class="text-danger font-weight-bold " style="font-size: 12px; display: none;">
                                    
                                    Completa los campos obligatorios
                                
                                </span>
                            </div>
                            
                            <div class="col-12 mt-4 text-center">
                                <center>
                                    <div  id="recaptcha-containerClave" ></div>
                                
                                </center>
                                
                                <center>
                                         
                                    <div id="divRecap" style="width: 100%;  "></div>    
                                    
                                    <button id="btnRestablecerClave" class="btn btn-block border-0 gradentaBotono mt-1 " onclick="fntRecapClave();"  style="width: 70%;">
                                        Restablecer contraseña
                                    </button>
                                    
                                    <button id="btnTienesunacuentaRestablecerClave" class="btn btn-block border-0 gradentaBotono2 " onclick="fntLogIn_showRegistro(false, false);"  style="width: 70%; color: #808080 !important;">
                                        ¿Ya tienes una cuenta?
                                    </button>
                                    
                                </center>                                    
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    </form>
                    
                </div>
                
                <div id="divContenidoFormRestablecerClaveConfirmacion" style="position: absolute; top: <?php print $boolMovil ? "25%" : "10%"?>; z-index: 999; width: 90%; left: 5%; display: none; ">
                    
                    <form name="fmrRestablecerClaveConfirmacion" id="fmrRestablecerClaveConfirmacion"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidTokenRegistroClave" name="hidTokenRegistroClave">                        
                    <h4 id="h4Resitros2Clave" class="text-white text-center" style="font-weight: 200;">
                        Restablecer contraseña
                    </h4>
                    
                    <br>
                    
                    <div class="p-4 bg-white" style="border-radius: 10px;">
                        
                        <div class="row">
                            
                            <div class="col-12">
                                          
                                <span  class="" style="font-size: 12px; ">
                                    
                                    <span id="spnCodigoenviadoporsmsmsClave">Codigo enviado por SMS a:</span> <span id="spTelefonoenviadoClave">+502 4343 4343</span> <div id="divTextoCambiarClave" class="badge text-white " style="background: #3E53AC; cursor: pointer;" onclick="location.reload()">Cambiar</div>
                                </span>
                                
                                <br>
                                <br>
                                
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                 
                                        <span id="sptxtCodigoSMSClave" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-mobile-phone"></i>
                                        </span>
                                        
                                    </div>                    
                                    
                                    <input type="text" id="txtCodigoSMSClave" name="txtCodigoSMSClave" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Codigo enviado" style="">
                                
                                </div>
                                
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">
                                                                    
                                        <span id="sptxtClaveRegistroClave" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                        
                                    </div>
                                    
                                    <input type="password" id="txtClaveRegristroConfirmacionClave" name="txtClaveRegristroConfirmacionClave" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="Tu nueva Contraseña" style="">
                                
                                </div>
                                <span id="spAlertaCamposConfirmacionClave" class="text-danger font-weight-bold " style="font-size: 12px; display: none;">
                                    
                                    Completa los campos obligatorios
                                
                                </span>
                            </div>
                                            
                            <div class="col-12 mt-4 text-center">
                                
                                
                                <center>
                                         
                                    <button id="btnRegistroFormularioClave" class="btn btn-block border-0 gradentaBotono mt-1 " onclick="fntGetTokenFirebaseClave();"  style="width: 70%; ">
                                        Confirmar Codigo
                                    </button>
                                    
                                </center>                                    
                            </div>
                            
                        </div>
                        
                        
                    </div>
                    
                    </form>
                    
                </div>
                
                <?php    
            }
            else{
                ?>
                
                <div id="" style="position: absolute; top: 10%; z-index: 999; width: 90%; left: 5%; ">
                    
                    <form name="fmrRegistroModalConfirmacionEmail" id="fmrRegistroModalConfirmacionEmail"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidTokenRegistro" name="hidTokenRegistro" value="<?php print fntCoreEncrypt($intUsuarioConfirmacion)?>">                        
                    <h5 class="text-center" style=" " >  
                        <img src="images/LogoInguateWhite.png" class="img-fluid " style="width: 40%; height: auto;" >
                        
                    </h5>
                    <h4 id="" class="text-white text-center" style="font-weight: 200;">
                        <?php print ( $strLenguaje =="es" ? "Hola" : "Hi" )." ".$strNombreConfirmacion;?>
                    </h4>
                    <h6 id="" class="text-white text-center" style="font-weight: 200;">
                        <?php print ( $strLenguaje =="es" ? "Confirmación de registro" : "Confirmation of registration" );?>
                    </h6>
                    
                    <br>
                    
                    <div class="p-4 bg-white" style="border-radius: 10px;">
                        
                        <div class="row">
                            
                            <div class="col-12">
                                          
                                <div class="input-group m-0 p-0">
                                    <div class="input-group-prepend m-0 p-0">    
                                                                    
                                        <span id="sptxtClaveRegistroEmail" class="input-group-text  text-center nofocusBorder clasrInput"  style="color: #3E53AC !important;">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                        
                                    </div>
                                    
                                    <input type="password" id="txtClaveRegristroConfirmacionEmail" name="txtClaveRegristroConfirmacionEmail" class="form-control text-left nofocusBorder clasrInput m-0 p-0" placeholder="<?php print ( $strLenguaje =="es" ? "Tu nueva Contraseña" : "Your new password" );?>" style="">
                                
                                </div>
                                <span id="spAlertaCamposConfirmacionEmail" class="text-danger font-weight-bold " style="font-size: 12px; display: none;">
                                    
                                    
                                    <?php print ( $strLenguaje =="es" ? "Completa los campos obligatorios" : "Complete the required fields" );?>
                                
                                </span>
                            </div>
                                            
                            <div class="col-12 mt-4 text-center">
                                
                                
                                <center>
                                         
                                    <button id="btnRegistroFormulario" class="btn btn-block border-0 gradentaBotono mt-1 " onclick="fntSetClaveConfimracionUsuario();"  style="width: 70%; ">
                                        
                                        <?php print ( $strLenguaje =="es" ? "Iniciar sesión" : "Log in" );?>
                                    </button>
                                    
                                </center>                                    
                            </div>
                            
                        </div>
                        
                        
                    </div>
                    
                    </form>
                    
                </div>
                
                <?php    
                
            }
            
            ?>
            
                
            
        </div>
        
        <div class="col-12 col-lg-8 m-0 p-0">
            
            <div id="crResgistro" class="carousel slide" data-ride="carousel" data-interval="3000">
              <div class="carousel-inner" style="">
              
                <?php
                
                if( count($arrPlaceGaleria) > 0 ){
                    
                    $intCountGaleria = 1;
                    while( $rTMPGaleria = each($arrPlaceGaleria) ){
                        
                        $strActive = $intCountGaleria == 1 ? "active" : "";
                        
                        ?>
                        <div class="carousel-item <?php print $strActive;?> ">
                          <img src="imgInguate.php?t=up&src=<?php print $rTMPGaleria["value"]["url_imagen"]?>" class="img-fluid imgCrRegistro" style="width: 100%; height: 600px; object-fit: cover;" >

                        </div>
                        <?php
                        $intCountGaleria++;
                    }
                    
                }
                else{
                    
                    for( $i = 1; $i <= 5; $i++ ){
                        
                        $strActive = $i == 1 ? "active" : "";
                        
                        ?>
                        <div class="carousel-item <?php print $strActive;?> ">
                          <img src="images/index/registro_<?php print $i;?>.jpg" class="img-fluid imgCrRegistro" style="width: 100%; height: 600px; object-fit: cover;" >

                        </div>
                        <?php
                    }
                    
                }
                
                    
                
                ?>
                
              
              </div>
              
            </div>

        </div>
    </div>
               
               
    <!-- Button trigger modal -->


<!-- Modal -->
    <div class="modal fade" id="mlCodigoArea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Codigod de Area</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1" id="divmlCodigoAreaContenido">
                </div>
            </div>
        </div>
    </div>                      
    
    <script>
        
        var objgrecaptcha;
        var appVerifier;
        $(document).ready(function() {
            
            
            
                
                
        });
        
        
              
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyCc8-C6Tuc7vNHZ2WGVaDsuGgGsygmh3fs",
            authDomain: "inguate-7b85c.firebaseapp.com",
            databaseURL: "https://inguate-7b85c.firebaseio.com",
            projectId: "inguate-7b85c",
            storageBucket: "inguate-7b85c.appspot.com",
            messagingSenderId: "709370927652",
            appId: "1:709370927652:web:1e7f62f30eeeea0f"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        
        
        function onloadCallbackLog(){
            
            alert("asdf");
            
            
        }
        
        function fntLogIn_showRegistro(boolRegristro, boolReset){
            
            boolReset = !boolReset ? false : boolReset;
            
            $("#divContenidoFormRestablecerClave").hide();    
            $("#divContenidoLogInRegistroFormularioConfirmacionUsuario").hide();    
                            
            if( boolRegristro ){
                
                $("#divContenidoLogInRegistroFormulario").show();
                $("#divContenidoLogInFormulario").hide(); 
                $("#btnRegistroFormularioRegirstrate").show(); 
                
                if( boolReset ){
                    
                    //grecaptcha.reset(window.recaptchaWidgetId);
    
                }
                
            }
            else{
                
                $("#divContenidoLogInRegistroFormulario").hide();
                $("#divContenidoLogInFormulario").show();
                
            }
            
                    
        }
        
        function fntRegistroEnvio(){
            
            var boolError = false;
            
                  
            if( $("#txtNombreRegistro").val() == ""  ){
                
                $("#sptxtNombreRegistro").css("color", "red !important");
                $("#sptxtNombreRegistro").css("border-bottom", "2px solid red");
                $("#txtNombreRegistro").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtNombreRegistro").css("color", "#3E53AC !important");
                $("#sptxtNombreRegistro").css("border-bottom", "2px solid #3E53AC");
                $("#txtNombreRegistro").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( $("#txtEmailRegistro").val() == ""  ){
                
                $("#sptxtEmailRegistro").css("color", "red !important");
                $("#sptxtEmailRegistro").css("border-bottom", "2px solid red");
                $("#txtEmailRegistro").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtEmailRegistro").css("color", "#3E53AC !important");
                $("#sptxtEmailRegistro").css("border-bottom", "2px solid #3E53AC");
                $("#txtEmailRegistro").css("border-bottom", "2px solid #3E53AC");
                    
            }
              
            if( $("#txtTelefonoRegistro").val() == ""  ){
                
                $("#sptxtTelefonoRegistro").css("color", "red !important");
                $("#sptxtTelefonoRegistro").css("border-bottom", "2px solid red");
                $("#sptxtCodigoArea").css("color", "red !important");
                $("#sptxtCodigoArea").css("border-bottom", "2px solid red");
                $("#txtTelefonoRegistro").css("border-bottom", "2px solid red");
                
                boolError = true;
                      
            }
            else{
                
                $("#sptxtTelefonoRegistro").css("color", "#3E53AC !important");
                $("#sptxtTelefonoRegistro").css("border-bottom", "2px solid #3E53AC");
                $("#sptxtCodigoArea").css("color", "#3E53AC !important");
                $("#sptxtCodigoArea").css("border-bottom", "2px solid #3E53AC");
                $("#txtTelefonoRegistro").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( boolError ){
                
                $("#spAlertaCampos").show();
                
            }
            else{
                
                $("#spAlertaCampos").hide();
                                
                var formData = new FormData(document.getElementById("fmrRegistroModal"));
                formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
            
                $(".preloader").fadeIn();
                $.ajax({
                    url: "servicio_core.php?servicio=setRegistroPConfirmacion", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["error"] == true ){
                            
                            if( result["error_recap"] == true ){
                                
                                grecaptcha.reset(objgrecaptcha);    
                                    
                            }
                                
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            }); 
                                                            
                        }
                        else if( result["envio_sms"] == true ) { 
                        
                            $("#spTelefonoenviado").html($("#txtTelefonoRegistro").val());    
                            $("#hidTokenRegistro").val(result["token_registro"]);    
                            
                            grecaptcha.reset(objgrecaptcha);    
                                
                            $("#divContenidoLogInRegistroFormulario").hide();    
                            $("#divContenidoLogInRegistroFormularioConfirmacionUsuario").show();    
                                  
                                    
                        }
                        
                    }
                            
                });
                
            }
            
        }
        
        function fntRegistroEnvioConfirmarSMS(){
            
            var formData = new FormData(document.getElementById("fmrRegistroModalConfirmacionSMS"));
            
            formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
                
            $(".preloader").fadeIn();
            $.ajax({
                url: "servicio_core.php?servicio=setRegistroPConfirmacionSMS", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                    if( result["error"] == true ){
                        
                        swal({
                            title: "Error",
                            text: result["msn"],
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        }); 
                                                        
                    }
                    else { 
                        
                        location.reload(); 
                                
                    }
                    
                }
                        
            });    
            
        }
        
        function fntSetClaveConfimracionUsuario(){
            
            var boolError = false;
            
            if( $("#txtClaveRegristroConfirmacionEmail").val() == ""  ){
                
                $("#sptxtClaveRegistroEmail").css("color", "red !important");
                $("#sptxtClaveRegistroEmail").css("border-bottom", "2px solid red");
                $("#txtNombreRegistro").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtClaveRegistroEmail").css("color", "#3E53AC !important");
                $("#sptxtClaveRegistroEmail").css("border-bottom", "2px solid #3E53AC");
                $("#txtClaveRegristroConfirmacionEmail").css("border-bottom", "2px solid #3E53AC");
                    
            }
                           
            
            if( boolError ){
                
                $("#spAlertaCamposConfirmacionEmail").show();
                
            }
            else{
                
                $("#spAlertaCamposConfirmacionEmail").hide();
                
                var formData = new FormData(document.getElementById("fmrRegistroModalConfirmacionEmail"));
                
                formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "servicio_core.php?servicio=setRegistroConfirmacionEmail", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["error"] == true ){
                            
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            }); 
                                                            
                        }
                        else { 
                            
                            location.href = "index.php"; 
                                    
                        }
                        
                    }
                            
                });
                
            } 
              
              
                    
            
        }
        
        function fntLogIn__(){
            
            var boolError = false;
            
            if( $("#txtEmail").val() == ""  ){
                
                $("#sptxtEmail").css("color", "red !important");
                $("#sptxtEmail").css("border-bottom", "2px solid red");
                $("#txtEmail").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtEmail").css("color", "#3E53AC !important");
                $("#sptxtEmail").css("border-bottom", "2px solid #3E53AC");
                $("#txtEmail").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( $("#txtClave").val() == ""  ){
                
                $("#sptxtClave").css("color", "red !important");
                $("#sptxtClave").css("border-bottom", "2px solid red");
                $("#txtClave").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtClave").css("color", "#3E53AC !important");
                $("#sptxtClave").css("border-bottom", "2px solid #3E53AC");
                $("#txtClave").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( boolError ){
                
                $("#spAlertaCamposLogIn").show();
                
            }
            else{
                
                $("#spAlertaCamposLogIn").hide();
                                
                var formData = new FormData(document.getElementById("frmModalLogIn"));
                formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
            
                $(".preloader").fadeIn();
                $.ajax({
                    url: "servicio_core.php?servicio=setLogIn", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["error"] == true ){
                            
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            }); 
                                                            
                        }
                        else { 
                        
                            <?php
                            
                            if( !empty($strLinkRef) ){
                                
                                ?>
                                location.href = "<?php print $strLinkRef?>";
                            
                                <?php
                            }
                            else{
                                
                                ?>
                                location.reload();
                            
                                <?php
                            }
                            
                            ?>
                                    
                        }
                        
                    }
                            
                });
                
            }
                
        }
        
        function fntSetIdiomaModalLogIn(strKeyIdioma){
            
            if( strKeyIdioma == "es" ){
                
                   
                $("#txtEmail").attr("placeholder", "Email");
                $("#txtEmailRegistroClave").attr("placeholder", "Email");
                $("#txtClave").attr("placeholder", "Contraseña");
                $("#txtNombreRegistro").attr("placeholder", "Nombre completo");
                $("#txtEmailRegistro").attr("placeholder", "Email");
                $("#txtTelefonoRegistro").attr("placeholder", "Telefono");
                $("#txtTelefonoClave").attr("placeholder", "Telefono");
                $("#txtCodigoSMS").attr("placeholder", "Código enviado");
                $("#txtCodigoSMSClave").attr("placeholder", "Código enviado");
                $("#txtClaveRegristroConfirmacion").attr("placeholder", "Tu nueva contraseña");
                $("#txtClaveRegristroConfirmacionClave").attr("placeholder", "Tu nueva contraseña");
                
                
                $("#btnInicionSesion").html("Iniciar sesión");
                $("#btnOlvidoClave").html("Se te olvidó tu contraseña?");
                $("#btnRegistros1").html("Registrate");
                $("#h4RestablecerClave").html("Restablecer contraseña");
                $("#btnRestablecerClave").html("Restablecer contraseña");
                $("#h4Resitros2Clave").html("Restablecer contraseña");
                
                $("#h4GraciasporRegistro").html("Gracias por registrarte");
                $("#h4GraciasporRegistro_msn").html("Te hemos enviado un correo electrónico para verificar tu cuenta");
                $("#h4GraciasporRegistro_msnClave").html("Te hemos enviado un correo electrónico para verificar tu cuenta y restablecer tu contraseña.");
                
                $("#spAlertaCamposLogIn").html("Completa los campos obligatorios");
                $("#h4Registro").html("Registro");
                $("#spAlertaCampos").html("Completa los campos obligatorios");
                $("#spAlertaCamposClave").html("Completa los campos obligatorios");
                $("#spAlertaCamposConfirmacionClave").html("Completa los campos obligatorios");
                $("#btnRegistroFormularioRegirstrate").html("Registrate");
                $("#btnTienesunacuenta").html("¿Ya tienes una cuenta?");
                $("#btnTienesunacuentaRestablecerClave").html("¿Ya tienes una cuenta?");
                $("#h4Resitros2").html("Registro");
                $("#spnCodigoenviadoporsmsms").html("Código enviado por SMS a:");
                $("#spnCodigoenviadoporsmsmsClave").html("Código enviado por SMS a:");
                $("#divTextoCambiar").html("Cambiar");
                $("#divTextoCambiarClave").html("Cambiar");
                $("#btnRegistroFormulario").html("Confirmar Código");
                $("#btnRegistroFormularioClave").html("Confirmar Código");
                $("#hidModlLogInIdioma").val("es");
                
                $("#imEsModal").css("border", "2px solid #3E53AC");
                $("#imEnModal").css("border", "0px solid #3E53AC");
                    
            }
            else{
                
                $("#txtEmail").attr("placeholder", "Email");
                $("#txtEmailRegistroClave").attr("placeholder", "Email");
                $("#txtClave").attr("placeholder", "Password");
                $("#txtNombreRegistro").attr("placeholder", "Full name");
                $("#txtEmailRegistro").attr("placeholder", "Email");
                $("#txtTelefonoRegistro").attr("placeholder", "Phone");
                $("#txtTelefonoClave").attr("placeholder", "Phone");
                $("#txtCodigoSMS").attr("placeholder", "Code sent");
                $("#txtCodigoSMSClave").attr("placeholder", "Code sent");
                $("#txtClaveRegristroConfirmacion").attr("placeholder", "Your new password");
                $("#txtClaveRegristroConfirmacionClave").attr("placeholder", "Your new password");
                
                
                $("#btnInicionSesion").html("Log in");
                $("#btnOlvidoClave").html("Forgot password?");
                $("#btnRegistros1").html("Sign up");
                $("#h4RestablecerClave").html("Restore password");
                $("#btnRestablecerClave").html("Restore password");
                $("#h4Resitros2Clave").html("Restore password");
                
                $("#h4GraciasporRegistro").html("Thanks for signing up");
                $("#h4GraciasporRegistro_msn").html("We have sent you an email to verify your account");
                $("#h4GraciasporRegistro_msnClave").html("We have sent you an email to verify your account and reset your password.");
                
                
                $("#spAlertaCamposLogIn").html("Complete the required fields");
                $("#spAlertaCamposClave").html("Complete the required fields");
                $("#spAlertaCamposConfirmacionClave").html("Complete the required fields");
                $("#h4Registro").html("Registry");
                $("#spAlertaCampos").html("Complete the required fields");
                $("#btnRegistroFormularioRegirstrate").html("Registry");
                $("#btnTienesunacuenta").html("Do you already have an account?");
                $("#btnTienesunacuentaRestablecerClave").html("Do you already have an account?");
                $("#h4Resitros2").html("Registry");
                $("#spnCodigoenviadoporsmsms").html("Code sent by SMS to:");
                $("#spnCodigoenviadoporsmsmsClave").html("Code sent by SMS to:");
                $("#divTextoCambiar").html("Change");
                $("#divTextoCambiarClave").html("Change");
                $("#btnRegistroFormulario").html("Confirm Code");
                $("#btnRegistroFormularioClave").html("Confirm Code");
                
                $("#imEsModal").css("border", "0px solid #3E53AC");
                $("#imEnModal").css("border", "2px solid #3E53AC");
                
                $("#hidModalLogInIdioma").val("en");
                
                
            }
            
                
        }
        
        function fntDrawModalCodigoArea(){
            
            $.ajax({
                url: "index.php?drawCodigoAreaFormulario=true", 
                success: function(result){
                    
                    $("#mlCodigoArea").modal("show");
                    $("#divmlCodigoAreaContenido").html(result);
            
                }
                
            });
                
        }
        
        function fntDrawModalCodigoAreaClave(){
            
            $.ajax({
                url: "index.php?drawCodigoAreaFormulario=true", 
                success: function(result){
                    
                    $("#mlCodigoArea").modal("show");
                    $("#divmlCodigoAreaContenido").html(result);
            
                }
                
            });
                
        }
        
        function fntRecap(){
            
            var boolError = false;
            
                  
            if( $("#txtNombreRegistro").val() == ""  ){
                
                $("#sptxtNombreRegistro").css("color", "red !important");
                $("#sptxtNombreRegistro").css("border-bottom", "2px solid red");
                $("#txtNombreRegistro").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtNombreRegistro").css("color", "#3E53AC !important");
                $("#sptxtNombreRegistro").css("border-bottom", "2px solid #3E53AC");
                $("#txtNombreRegistro").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( $("#txtEmailRegistro").val() == ""  ){
                
                $("#sptxtEmailRegistro").css("color", "red !important");
                $("#sptxtEmailRegistro").css("border-bottom", "2px solid red");
                $("#txtEmailRegistro").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtEmailRegistro").css("color", "#3E53AC !important");
                $("#sptxtEmailRegistro").css("border-bottom", "2px solid #3E53AC");
                $("#txtEmailRegistro").css("border-bottom", "2px solid #3E53AC");
                    
            }
              
            if( $("#txtTelefonoRegistro").val() == ""  ){
                
                $("#sptxtTelefonoRegistro").css("color", "red !important");
                $("#sptxtTelefonoRegistro").css("border-bottom", "2px solid red");
                $("#sptxtCodigoArea").css("color", "red !important");
                $("#sptxtCodigoArea").css("border-bottom", "2px solid red");
                $("#txtTelefonoRegistro").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtTelefonoRegistro").css("color", "#3E53AC !important");
                $("#sptxtTelefonoRegistro").css("border-bottom", "2px solid #3E53AC");
                $("#sptxtCodigoArea").css("color", "#3E53AC !important");
                $("#sptxtCodigoArea").css("border-bottom", "2px solid #3E53AC");
                $("#txtTelefonoRegistro").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( boolError ){
                
                $("#spAlertaCampos").show();
                
            }
            else{
                     
                     
                    
                $.ajax({
                    url: "servicio_core.php?servicio=getCorreoExiste&email="+$("#txtEmailRegistro").val()+"&hidModalLogInIdioma="+$("#hidModalLogInIdioma").val(), 
                    dataType: "json",
                    success: function(result){
                        
                        if( result["existe"] == "Y" ){
                            
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            }); 
                                
                        }
                        else{
                            
                            $("#btnRegistroFormularioRegirstrate").hide(); 
                            
                            if( $("#hidCodigoAreaRegistro").val() == "502" ){
                                
                                    
                                var phoneNumber = '+'+$("#hidCodigoAreaRegistro").val()+$("#txtTelefonoRegistro").val();
                                appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
                                
                                firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                                    .then(function (confirmationResult) {
                                        // SMS sent. Prompt user to type the code from the message, then sign the
                                        // user in with confirmationResult.confirm(code).
                                        window.confirmationResult = confirmationResult;
                                        
                                        $("#spTelefonoenviado").html(phoneNumber);    
                                        $("#divContenidoLogInRegistroFormulario").hide();    
                                        $("#divContenidoLogInRegistroFormularioConfirmacionUsuario").show();    
                                                
                                        

                                    }).catch(function (error) {
                                        // Error; SMS not sent
                                        // ...
                                        console.log(error);
                                        
                                    });
                                    
                            }
                            else{
                                
                                window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
                                    'size': 'normal',
                                    'callback': function(response) {
                                        // reCAPTCHA solved, allow signInWithPhoneNumber.
                                        // ...
                                        
                                        var formData = new FormData(document.getElementById("fmrRegistroModal"));
                                        formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
                                        formData.append("txtNombreRegistro", $("#txtNombreRegistro").val());
                                        formData.append("txtEmailRegistro", $("#txtEmailRegistro").val());
                                        formData.append("txtTelefonoRegistro", $("#txtTelefonoRegistro").val());
                                        formData.append("hidReferenciaLugar", $("#hidReferenciaLugar").val());
                                        formData.append("hidCodigoAreaRegistro", $("#hidCodigoAreaRegistro").val());
                                        formData.append("txtClaveRegristroConfirmacion", $("#txtClaveRegristroConfirmacion").val());
                                        formData.append("txtTokenRecap", response);
                                        
                                        $(".preloader").fadeIn();
                                        $.ajax({
                                            url: "servicio_core.php?servicio=setRegistroEmailConfirmacion", 
                                            type: "POST",
                                            data: formData,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            dataType: "json",
                                            success: function(result){
                                                
                                                $(".preloader").fadeOut();
                                                
                                                if( result["error"] == true ){
                                                    
                                                    swal({
                                                        title: "Error",
                                                        text: result["msn"],
                                                        type: "error",
                                                        confirmButtonClass: "btn-danger",
                                                        confirmButtonText: "Ok",
                                                        closeOnConfirm: true
                                                    }); 
                                                    
                                                    window.recaptchaVerifier.render().then(function(widgetId) {
                                                      grecaptcha.reset(widgetId);
                                                    });
                                                                                    
                                                }
                                                else { 
                                                      
                                                    $("#divAlertaVerificacion").show();
                                                    $("#divContenidoLogInRegistroFormulario").hide();
                                                                                                
                                                }
                                                
                                            }
                                                    
                                        });
                                        
                                        
                                    },
                                    'expired-callback': function() {
                                        // Response expired. Ask user to solve reCAPTCHA again.
                                        // ...
                                        window.recaptchaVerifier.render().then(function(widgetId) {
                                          grecaptcha.reset(widgetId);
                                        });
                                    }
                                });

                                recaptchaVerifier.render().then(function(widgetId) {
                                    window.recaptchaWidgetId = widgetId;
                                });
                                
                                
                                return false;
                                
                                    
                            }
                            
                        }
                                        
                    }
                    
                });
                
            }
            
        }
        
        function fntRecapClave(){
            
            var boolError = false;         
            
            if( $("#txtEmailRegistroClave").val() == ""  ){
                
                $("#sptxtEmailRegistroClave").css("color", "red !important");
                $("#sptxtEmailRegistroClave").css("border-bottom", "2px solid red");
                $("#txtEmailRegistroClave").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtEmailRegistroClave").css("color", "#3E53AC !important");
                $("#sptxtEmailRegistroClave").css("border-bottom", "2px solid #3E53AC");
                $("#txtEmailRegistroClave").css("border-bottom", "2px solid #3E53AC");
                    
            }
              
            if( $("#txtTelefonoClave").val() == ""  ){
                
                $("#sptxtTelefonoClave").css("color", "red !important");
                $("#sptxtTelefonoClave").css("border-bottom", "2px solid red");
                $("#sptxtCodigoAreaClave").css("color", "red !important");
                $("#sptxtCodigoAreaClave").css("border-bottom", "2px solid red");
                $("#txtTelefonoClave").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtTelefonoClave").css("color", "#3E53AC !important");
                $("#sptxtTelefonoClave").css("border-bottom", "2px solid #3E53AC");
                $("#sptxtCodigoAreaClave").css("color", "#3E53AC !important");
                $("#sptxtCodigoAreaClave").css("border-bottom", "2px solid #3E53AC");
                $("#txtTelefonoClave").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( boolError ){
                
                $("#spAlertaCamposClave").show();
                
                
            }
            else{
                     
                $.ajax({
                    url: "servicio_core.php?servicio=getCorreoExiste&clave=Y&email="+$("#txtEmailRegistroClave").val()+"&hidModalLogInIdioma="+$("#hidModalLogInIdioma").val(), 
                    dataType: "json",
                    success: function(result){
                        
                        if( result["existe"] == "N" ){
                            
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            }); 
                                
                        }
                        else{   
                            
                            $("#btnRestablecerClave").hide(); 
                            
                            if( $("#hidCodigoAreaRegistroClave").val() == "502" ){
                                
                                    
                                var phoneNumber = '+'+$("#hidCodigoAreaRegistroClave").val()+$("#txtTelefonoClave").val();
                                        
                                appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-containerClave');
                                
                                firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                                    .then(function (confirmationResult) {
                                        // SMS sent. Prompt user to type the code from the message, then sign the
                                        // user in with confirmationResult.confirm(code).
                                        window.confirmationResult = confirmationResult;
                                        
                                        $("#spTelefonoenviadoClave").html(phoneNumber);    
                                        $("#divContenidoFormRestablecerClave").hide();    
                                        $("#divContenidoFormRestablecerClaveConfirmacion").show();    
                                                
                                        

                                    }).catch(function (error) {
                                        // Error; SMS not sent
                                        // ...
                                        console.log(error);
                                        
                                    });
                                    
                            }
                            else{
                                
                                window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-containerClave', {
                                    'size': 'normal',
                                    'callback': function(response) {
                                        // reCAPTCHA solved, allow signInWithPhoneNumber.
                                        // ...
                                        
                                        var formData = new FormData(document.getElementById("fmrRestablecerClaveConfirmacion"));
                                        
                                        formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
                                        formData.append("txtEmailRegistroClave", $("#txtEmailRegistroClave").val());
                                        formData.append("txtTelefonoClave", $("#txtTelefonoClave").val());
                                        formData.append("hidCodigoAreaRegistroClave", $("#hidCodigoAreaRegistroClave").val());
                                        formData.append("txtTokenRecap", response);
                                        
                                        $(".preloader").fadeIn();
                                        $.ajax({
                                            url: "servicio_core.php?servicio=setRegistroEmailConfirmacionClave", 
                                            type: "POST",
                                            data: formData,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            dataType: "json",
                                            success: function(result){
                                                
                                                $(".preloader").fadeOut();
                                                
                                                if( result["error"] == true ){
                                                    
                                                    swal({
                                                        title: "Error",
                                                        text: result["msn"],
                                                        type: "error",
                                                        confirmButtonClass: "btn-danger",
                                                        confirmButtonText: "Ok",
                                                        closeOnConfirm: true
                                                    }); 
                                                    
                                                    window.recaptchaVerifier.render().then(function(widgetId) {
                                                      grecaptcha.reset(widgetId);
                                                    });
                                                                                    
                                                }
                                                else { 
                                                      
                                                    $("#divAlertaVerificacionClave").show();
                                                    $("#divContenidoFormRestablecerClave").hide();
                                                                                                
                                                }
                                                
                                            }
                                                    
                                        });
                                        
                                        
                                    },
                                    'expired-callback': function() {
                                        // Response expired. Ask user to solve reCAPTCHA again.
                                        // ...
                                        window.recaptchaVerifier.render().then(function(widgetId) {
                                          grecaptcha.reset(widgetId);
                                        });
                                    }
                                });

                                recaptchaVerifier.render().then(function(widgetId) {
                                    window.recaptchaWidgetId = widgetId;
                                });
                                
                                
                                return false;
                                
                                    
                            }
                            
                        }
                                        
                    }
                    
                });
                
            }
            
        }
        
        function fntGetTokenFirebase(){
            
            var boolError = false;
            
            if( $("#txtCodigoSMS").val() == ""  ){
                
                $("#sptxtCodigoSMS").css("color", "red !important");
                $("#sptxtCodigoSMS").css("border-bottom", "2px solid red");
                $("#txtCodigoSMS").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtCodigoSMS").css("color", "#3E53AC !important");
                $("#sptxtCodigoSMS").css("border-bottom", "2px solid #3E53AC");
                $("#txtCodigoSMS").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( $("#txtClaveRegristroConfirmacion").val() == ""  ){
                
                $("#sptxtClaveRegistro").css("color", "red !important");
                $("#sptxtClaveRegistro").css("border-bottom", "2px solid red");
                $("#txtClaveRegristroConfirmacion").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtClaveRegistro").css("color", "#3E53AC !important");
                $("#sptxtClaveRegistro").css("border-bottom", "2px solid #3E53AC");
                $("#txtClaveRegristroConfirmacion").css("border-bottom", "2px solid #3E53AC");
                    
            }
                           
            if( boolError ){
                
                $("#spAlertaCamposConfirmacion").show();
                
            }else{
                
                var code = $("#txtCodigoSMS").val();
                confirmationResult.confirm(code).then(function (result) {
                    // User signed in successfully.
                    var user = result.user;

                    var formData = new FormData(document.getElementById("fmrRegistroModal"));
                    formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
                    formData.append("txtNombreRegistro", $("#txtNombreRegistro").val());
                    formData.append("txtEmailRegistro", $("#txtEmailRegistro").val());
                    formData.append("txtTelefonoRegistro", $("#txtTelefonoRegistro").val());
                    formData.append("hidReferenciaLugar", $("#hidReferenciaLugar").val());
                    formData.append("hidCodigoAreaRegistro", $("#hidCodigoAreaRegistro").val());
                    formData.append("txtClaveRegristroConfirmacion", $("#txtClaveRegristroConfirmacion").val());
                    formData.append("hidTrafico", $("#hidTrafico").val());
                    formData.append("uid", result.user.uid);
                    formData.append("token", result.user.ra);
                
                    $(".preloader").fadeIn();
                    $.ajax({
                        url: "servicio_core.php?servicio=setRegistroPConfirmacion", 
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        success: function(result){
                            
                            $(".preloader").fadeOut();
                            
                            if( result["error"] == true ){
                                
                                swal({
                                    title: "Error",
                                    text: result["msn"],
                                    type: "error",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                }); 
                                                                
                            }
                            else  { 
                                
                                <?php
                                
                                if( !empty($strLinkRef) ){
                                    
                                    ?>
                                    location.href = "<?php print $strLinkRef?>";
                                
                                    <?php
                                }
                                else{
                                    
                                    ?>
                                    location.reload();
                                
                                    <?php
                                }
                                
                                ?>
                                        
                            }
                            
                        }
                                
                    });
                    





                    // ...
                }).catch(function (error) {
                  // User couldn't sign in (bad verification code?)
                  // ...
                  swal({
                        title: "Error",
                        text: ( $("#hidModalLogInIdioma").val() == "es" ? "Código incorrecto" : "Incorrect code" ),
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                });
            }
                                         
                
            
        }
        
        function fntGetTokenFirebaseClave(){
            
            var boolError = false;
                     
            if( $("#txtCodigoSMSClave").val() == ""  ){
                
                $("#sptxtCodigoSMSClave").css("color", "red !important");
                $("#sptxtCodigoSMSClave").css("border-bottom", "2px solid red");
                $("#txtCodigoSMSClave").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtCodigoSMSClave").css("color", "#3E53AC !important");
                $("#sptxtCodigoSMSClave").css("border-bottom", "2px solid #3E53AC");
                $("#txtCodigoSMSClave").css("border-bottom", "2px solid #3E53AC");
                    
            }
            
            if( $("#txtClaveRegristroConfirmacionClave").val() == ""  ){
                
                $("#sptxtClaveRegistroClave").css("color", "red !important");
                $("#sptxtClaveRegistroClave").css("border-bottom", "2px solid red");
                $("#txtClaveRegristroConfirmacionClave").css("border-bottom", "2px solid red");
                
                boolError = true;
            
            }
            else{
                
                $("#sptxtClaveRegistroClave").css("color", "#3E53AC !important");
                $("#sptxtClaveRegistroClave").css("border-bottom", "2px solid #3E53AC");
                $("#txtClaveRegristroConfirmacionClave").css("border-bottom", "2px solid #3E53AC");
                    
            }
                           
            if( boolError ){
                
                $("#spAlertaCamposConfirmacionClave").show();
                
            }else{
                
                var code = $("#txtCodigoSMSClave").val();
                
                confirmationResult.confirm(code).then(function (result) {
                    // User signed in successfully.
                    var user = result.user;
                                                   
                    var formData = new FormData(document.getElementById("fmrRestablecerClaveConfirmacion"));
                    formData.append("hidModalLogInIdioma", $("#hidModalLogInIdioma").val());
                    formData.append("txtEmailRegistroClave", $("#txtEmailRegistroClave").val());
                    formData.append("txtTelefonoClave", $("#txtTelefonoClave").val());
                    formData.append("hidCodigoAreaRegistroClave", $("#hidCodigoAreaRegistroClave").val());
                    formData.append("txtClaveRegristroConfirmacionClave", $("#txtClaveRegristroConfirmacionClave").val());
                    formData.append("uid", result.user.uid);
                    formData.append("token", result.user.ra);
                
                    $(".preloader").fadeIn();
                    $.ajax({
                        url: "servicio_core.php?servicio=setRegistroPConfirmacionClave", 
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        success: function(result){
                            
                            $(".preloader").fadeOut();
                            
                            if( result["error"] == true ){
                                
                                swal({
                                    title: "Error",
                                    text: result["msn"],
                                    type: "error",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                }); 
                                                                
                            }
                            else { 
                                
                                location.reload();
                                        
                            }
                            
                        }
                                
                    });
                    
                }).catch(function (error) {
                  // User couldn't sign in (bad verification code?)
                  // ...
                  swal({
                        title: "Error",
                        text: ( $("#hidModalLogInIdioma").val() == "es" ? "Código incorrecto" : "Incorrect code" ),
                        type: "error",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                });
            }
                                         
                
            
        }
        
        function fntDrawFormRestablecerClave(){
            
            $("#divContenidoLogInFormulario").hide(); 
            $("#divContenidoFormRestablecerClave").show(); 
                
                
        }
        
    </script>
    <?php    
}

function fntDrawContenidoCodigoArea(){
    
    $arrCodigo = fntCodigoArea();
    
    ?>
    
    <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
    <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
     
    <div class="table-responsive p-1">
    
       
    <table class="table table-secondary table-hover " id="tblCodigoArea">
        <thead class="thead-dark">
            <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col">Nombre</th>
                <th scope="col">Prefijo</th>
                <th scope="col">Codigo</th>
            </tr>
        </thead>
        <tbody>
            <?php
            
            while( $rTMP = each($arrCodigo) ){
                ?>
                <tr style="cursor: pointer;" onclick="fntSetcodigoArea('<?php print $rTMP["value"]["codigo_area"]?>', '<?php print $rTMP["value"]["iso_2"]?>');">
                    <td>
                        <img src="images/bandera/<?php print $rTMP["value"]["iso_3"]?>.png" style="width: 50px; height: 30px;">
                    </td>
                    <td><?php print $rTMP["value"]["nombre"]?></td>
                    <td><?php print $rTMP["value"]["iso_2"]?></td>
                    <td>+<?php print $rTMP["value"]["codigo_area"]?></td>
                </tr>
                <?php                                        
            }
            
            ?>
        </tbody>
    </table>
    
    </div>
    <script>
        
        $('#tblCodigoArea').DataTable({
            "ordering": false                    
        });    
        
        function fntSetcodigoArea(strCodigo, strPrefijo){
            
            $("#hidCodigoAreaRegistro").val(strCodigo);
            $("#hidCodigoAreaRegistroClave").val(strCodigo);
            $("#sphidCodigoAreaRegistro").html(strPrefijo+" +"+strCodigo);
            $("#sphidCodigoAreaRegistroClave").html(strPrefijo+" +"+strCodigo);
            
            $("#hidCodigoAreaRegistro").val(strCodigo);
            $("#sphidCodigoAreaRegistro").html(strPrefijo+" +"+strCodigo);
            
            $("#mlCodigoArea").modal("hide");
            $("#divmlCodigoAreaContenido").html("");
                    
            
        }
                 
        
    </script>
    
    <?php    
    
}

function fntDrawFooterPublico($strColorHex = "3E53AC", $boolModalLogi = true){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
    $boolMovil = $pos === false ? false : true;

    if( !$boolMovil ){
        
        $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

        $boolMovil = $pos === false ? false : true;

    }
    
    if( $boolModalLogi ){
        ?>
        <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog"  aria-hidden="true"  style="">
            <div class="modal-dialog modal-lg modal-dialog-centered" style="min-width: 90%;" role="document" style="">
                <div class="modal-content" style="min-height: 400px;">
                    
                    
                    <div class="modal-body p-0" id="modalLoginContenido">
                    
                        <?php
                        
                        //fntDrawRowContenidoModal();
                        
                        ?>    
                    
                                
                    </div>
                    
                </div>
            </div>
        </div>
        <?php
    }
    
    ?>
    
    <!-- Modal -->
              
              <!--
    
    <div class="d-none d-sm-block flotanteCentroArriba " onclick="location.href = 'index.php';">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br>
        <div class="col-3 p-0 m-0 d-none d-md-block" >
            <br>
        </div>
        
    </div>  -->
    
    <div class="flotante" onclick="fntTopScroll();">
        &nbsp;<i class="fa fa-arrow-up"></i>&nbsp;
    </div>
    
    
    
    <style>
            
        .classMenuAutocomplete {
            padding: 10px;
        }
        
        #customBtn {
          display: inline-block;
          background: white;
          color: #444;
          width: 190px;
          border-radius: 5px;
          border: thin solid #888;
          box-shadow: 1px 1px 1px grey;
          white-space: nowrap;
        }
        
        .text-tema{
            color: #<?php print $strColorHex?>;
        }  
        
        .flotante {
            
            display:none;
            position:fixed;
            top:80%;
            right:2%;
            background-color: white;
            border: 2px solid #<?php print $strColorHex;?>;
            color: #<?php print $strColorHex;?>;
            padding: 10px;
            border-radius: 50%;
            cursor: pointer;
            
        
        }
        .flotanteCentroArriba {
            
            position:fixed;
            top:0%;
            right:45%;
            background-color: transparent;
            color: white;
            padding: 10px;
            cursor: pointer; 
        
        }
        
        .card-img-wrap { 
          overflow: hidden;
          position: relative;
        }
        .card-img-wrap:after {
          content: '';
          position: absolute;
          top: 0; left: 0; right: 0; bottom: 0;
          background: rgba(255,255,255,0.3);
          opacity: 0;
          transition: opacity .25s;
        }
        .card-img-wrap img {
          transition: transform .25s;
          width: 100%;
        }
        .card-img-wrap:hover img {
          transform: scale(1.1);
        }
        /*
        .card-img-wrap:hover:after {
          opacity: 1;
        }
        */
        
        .text-hover-underline:hover{
            text-decoration: underline;
            cursor: pointer;
        }
        
        .hide {
            display: none;
        }
        
        .nofocusBorder:focus {
            
            color: #495057;
            background-color: #fff;
            border-color: #80bdff;
            outline: 0;
            box-shadow: 0 0 0 0.2rem transparent;
        
        }
        
        .btn-inguate{
            color: #<?php print $strColorHex?>;
            border: 1px solid #<?php print $strColorHex?>;
        }
        
        .btn-inguate:focus{
            color: #<?php print $strColorHex?>;
            border: 1px solid #<?php print $strColorHex?>;
        }
        
    </style>
        
        <script>
              
            function fntLogOut(){
                
                location.href = "index.php?setlogOut=true";
                    
                
            }     
                 /*
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '2795767433798083',
                    cookie     : false,
                    xfbml      : false,
                    version    : 'v3.3'
                });

                FB.AppEvents.logPageView();   

            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
                     
            function checkLoginState() {
              FB.getLoginStatus(function(response) {
                  
                  if( response.status == "connected" ){
                      
                      FB.api('/me?fields=email,first_name,last_name,picture', function(response) {
                                                
                          $("#txtEmail").val(response.email);
                          $("#txtNombre").val(response.first_name+" "+response.last_name);
                          $("#txtApellido").val(response.last_name);
                          $("#txtIdentificador").val(response.id);
                          $("#txtTipoLogIn").val("F");
                          $("#txtPictureUrl").val(response.picture.data.url);
                      
                          fntLogInPublico();
                          
                      });
                      
                  }
                
              }, {scope: 'public_profile,email'});
            }
                */
            function fntCambioTelefonoModalRegistro(){
                           
                $("#divFormCodigo").hide();    
                $("#divFormCodigoCambioTelefono").show();    
                $("#hidCambioTelefono").val("Y");    
                
            }
            
            function fntLogInPublicoConfirmacion(boolReenvio){
                
                boolReenvio = !boolReenvio ? false : boolReenvio;
                
                var formData = new FormData(document.getElementById("frmModalLogInRegistroConfirmacion"));
                
                formData.append("reenvio", boolReenvio);
                
                $(".preloader").fadeIn();
                $.ajax({
                    url: "servicio_core.php?servicio=setLogInRegistroPConfirmacion", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["error"] == true ){
                            
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            });
                                                            
                        }
                        else {
                            
                            if( result["reenvio"] == true ){
                                
                                if( result["reenvio_telefono_nuevo"] == true ){
                                    
                                    $("#divFormCodigo").show();    
                                    $("#divFormCodigoCambioTelefono").hide();    
                                    $("#hidCambioTelefono").val("N");    
                                    $("#spnNumeroEnvioCodigo").html(result["reenvio_telefono_nuevo_telefono"]);    
                                    
                                    
                                    
                                }
                                
                                swal({
                                    title: "Listo",
                                    text: result["msn"],
                                    type: "success",
                                    confirmButtonClass: "btn-info",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                });
                                    
                            }
                            else{
                                
                                location.reload();
                            
                            }
                                    
                        }
                            
                        
                    }
                            
                });
                
                return false;
                
            }
            
            function fntLogInPublico(){
                
                $("#txtAction").val(window.location.href);
                
                var formData = new FormData(document.getElementById("frmModalLogInRegistro"));
                formData.append("hidKeyRegistroCore", $("#hidKeyRegistroCore").val());    
                formData.append("hidLogInFb", $("#hidKeyRegistroCore").val());    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "servicio_core.php?servicio=setLogInRegistroP", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["confirmacion_sms"] == true ){
                            
                            $("#divFormConfirmacionUsuario").show();
                            $("#divFormRegistro").hide();
                            $("#hidKeyUsuario").val(result["u_key"]);
                            //$("#spnNumeroEnvioCodigo").html(result["telefonoEnvio"]); 
                            $("#spnNumeroEnvioCodigo").html(result["emailEnvio"]); 
                            
                            //if( result["telefonoEnvio"] == "" ){
                            if( result["emailEnvio"] == "" ){
                                            
                                fntCambioTelefonoModalRegistro(); 
                                    
                            }   
                                                                
                        }
                        else {
                            
                            if( result["error"] == "true" ){
                                
                                if( result["error_telefono_registro"] == "true" ){
                                    
                                    fntSetFormRegristro(true);    
                                        
                                }
                                else{
                                    
                                    swal({
                                        title: "Error",
                                        text: result["msn"],
                                        type: "error",
                                        confirmButtonClass: "btn-danger",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                    
                                    
                                    grecaptcha.reset(objgrecaptcha);
                                    
                                    if( result["sms_pendiente"] == "true" ){
                                        
                                        $("#divFormConfirmacionUsuario").show();
                                        $("#divFormRegistro").hide();
                                        $("#hidKeyUsuario").val(result["u_key"]);
                                        //$("#spnNumeroEnvioCodigo").html(result["telefonoEnvio"]);    
                                        $("#spnNumeroEnvioCodigo").html(result["emailEnvio"]);    
                                            
                                        //if( result["telefonoEnvio"] == "" ){
                                        if( result["emailEnvio"] == "" ){
                                            
                                            fntCambioTelefonoModalRegistro(); 
                                                
                                        }
                                                
                                        
                                    }
                                    else{
                                        
                                        fntSetFormRegristro();
                                        
                                    }
                                    
                                }
                                
                                    
                                    
                            }
                            else{
                                
                                location.href = result["href"];
                                                            
                            }
                            
                        }
                            
                        
                    }
                            
                });
                
                return false;
                
            }
            
            
                 
            function fntRegistro(){
                $("#dicContenidRecap").show();    
                $("#btnRegistro").hide();    
                
            }
            
            
            function fntDrawPreguntaNegocio(){
                
                //location.href = "pn.php";
                
                //return false;
                
                swal({
                    title: "",
                    text: "<?php print lang["alerta_registro_negocio_1"]?>",
                    type: "",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "servicio_core.php?servicio=setUsuarioNegocio", 
                            success: function(result){
                                
                                setLogInDashCookie();    
                                
                            }
                            
                        });
                        
                    
                    } 
                });
            }
            
            function setLogInDashCookie(boolHref){
                
                boolHref = !boolHref ? false : boolHref;
                
                $.ajax({
                    url: "login.php?login=true&cookie=Y", 
                    success: function(result){
                        
                        //if( boolHref ){
                            
                            location.href = "home.php";
                        //}
                        //else{
                            
                        //    window.open("home.php");
                            
                        //}
                        
                    }
                    
                });
                
            }
            
            function setAlertaVista(intKeyAlerta, boolHref){
                
                boolHref = !boolHref ? false : boolHref;
                
                $.ajax({
                    url: "servicio_core.php?servicio=setAlertaVista&key="+intKeyAlerta,
                    dataType: "json", 
                    success: function(result){
                        
                        setLogInDashCookie(boolHref);    
                        
                    }
                    
                });
                
                
            }
            
            function fntTopScroll(){
                
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;    
                
            }
            
            $(document).ready(function() {       
            
                $(window).scroll(function() {
                    var scroll = $(window).scrollTop();
                    if (scroll >= 50) {
                    $(".flotante").show();

                    } else {
                    $(".flotante").hide();
                    }
                });
                
                $('#crResgistro').carousel()
                
                $('#modalLogin').on('hidden.bs.modal', function (e) {
                    
                    $.ajax({
                        url: "servicio_core.php?servicio=getSession",
                        dataType: "json", 
                        success: function(result){
                            
                            if( result["session"] == "N" ){
                                
                                $("#modalLogin").modal("show");
                                    
                            }
                            
                        }
                        
                    });
                    
                });
                
                <?php
                
                if( !sesion["logIn"] || isset($_GET["cc"]) ){
                    
                    ?>
                    fntOpenModalRegistroCore();
                    <?php
                    
                }
                
                ?>
                 
                $(".preloader").fadeOut();
                   
            });
            
            function fntOpenModalRegistroCore(){
                
                $.ajax({
                        
                    url: "index.php?drawRegistroIndexPagina=true&bref=<?php print isset($_GET["bref"]) ? $_GET["bref"] : "";?>&cc=<?php print isset($_GET["cc"]) ? $_GET["cc"] : "";?>&tt=<?php print isset($_GET["tt"]) ? $_GET["tt"] : "";?>", 
                    success: function(result){
                        
                        <?php
                        
                        if( true ){
                            
                            ?>
                            $("#divContendedorPagina").html(result);
                            
                            var sinWindowsHeight2 = $("#divContenidoFormRegistroModal").height();
                            var sinWindowsHeight = $(window).height();
                    
                            sinWindowsHeight = sinWindowsHeight - sinWindowsHeight2;
                            
                            $(".imgCrRegistro").each(function (){
                                
                                $(this).height( sinWindowsHeight+'px');    
                                
                            });
                            
                            $('html, body').css({
                                overflow: 'hidden',
                                height: '100%'
                            });
                            <?php
                        }
                        else{
                            ?>
                            
                            //$("#modalLogin").modal("show");
                            //$("#modalLoginContenido").html(result);
                    
                            <?php    
                        }
                        
                        ?>
                        
                    }
                    
                });
                                
            }
            
            function fntSetFormRegristro(boolRegistro){
                
                       
                if( boolRegistro ){
                    
                    $("#txtEmail").css("border-bottom", "2px solid #3E53AC");
                    $("#txtClave").css("border-bottom", "2px solid #3E53AC");
                    
                    $("#sptxtEmail").css("border-bottom", "2px solid #3E53AC");
                    $("#sptxtClave").css("border-bottom", "2px solid #3E53AC");
                
                    var boolError = false;
                    
                    if( $("#txtEmail").val() == ""  ){
                        
                        
                        $("#sptxtEmail").css("color", "red !important");
                        $("#sptxtEmail").css("border-bottom", "2px solid red");
                        $("#txtEmail").css("border-bottom", "2px solid red");
                        
                        boolError = true;
                    
                    }
                    
                    if( $("#txtClave").val() == "" ){
                        
                        
                        $("#sptxtClave").css("color", "red !important");
                        $("#sptxtClave").css("border-bottom", "2px solid red");
                        $("#txtClave").css("border-bottom", "2px solid red");
                        
                        boolError = true;
                    }
                    
                    if( boolError )
                        return false;
                    
                    $("#divEmailClaveInput").hide();
                    
                    $("#divContenidoRegistro").show();
                    $("#divContenidoLogIn").hide();
                    $("#txtRegistro").val('Y');
                    
                }
                else{
                    
                    $("#divContenidoRegistro").hide();
                    $("#divContenidoLogIn").show();
                    $("#txtRegistro").val('N');
                    
                    $("#divEmailClaveInput").show();
                    
                    
                }                
            }
                
            function fntCerrarSesion(){
                
                $.ajax({
                    
                    url: "index.php?setlogOut=true", 
                    success: function(result){
                        
                        location.href = "index.php";
                        
                    }
                    
                });    
                
            }
            
            function fntUbicacionCookie(intUbicacion){
                
                $.ajax({
                    
                    url: "servicio_core.php?servicio=setUbicacionCookie&ubicacion="+intUbicacion, 
                    dataType : "json",
                    success: function(result){
                        location.reload();    
                    }
                    
                });     
                
            }
            
            function fntSetLenguajeCookie(strLenguaje){
                
                $.ajax({
                    
                    url: "servicio_core.php?servicio=setLenguajeCookie&lenguaje="+strLenguaje, 
                    dataType : "json",
                    success: function(result){
                        location.reload();    
                    }
                    
                });
                
            }
            
        </script>
         
        </body>

    </html>
    
    <?php
}

function fntGetProductoFavoritoUsuario($intIdUsuario, $boolInterno = false, $objDBClass = null, $strLenguaje = "es"){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strQuery = "SELECT place.id_place,
                        place.titulo,
                        place.url_logo,
                        place_producto.id_place_producto,
                        place_producto.nombre,
                        place_producto.descripcion,
                        place_producto.precio,
                        place_producto.logo_url,
                        place_producto.id_clasificacion_padre,
                        usuario_place_producto_favorito.id_usuario_producto_favorito,
                        
                        clasificacion_padre.id_clasificacion_padre,
                        clasificacion_padre.tag_en padre_tang_en,
                        clasificacion_padre.tag_es padre_tang_es,
                        
                        clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        clasificacion_padre_hijo.tag_en padre_hijo_tang_en,
                        clasificacion_padre_hijo.tag_es padre_hijo_tang_es
                 
                 FROM   usuario_place_producto_favorito,
                        place_producto
                            LEFT JOIN place_producto_clasificacion_padre_hijo
                                INNER JOIN clasificacion_padre_hijo
                                    ON  clasificacion_padre_hijo.id_clasificacion_padre_hijo = place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo
                                ON  place_producto.id_place_producto = place_producto_clasificacion_padre_hijo.id_place_producto,
                        place,
                        clasificacion_padre
                 WHERE  usuario_place_producto_favorito.id_usuario = {$intIdUsuario}
                 AND    usuario_place_producto_favorito.id_place_producto = place_producto.id_place_producto
                 AND    place_producto.id_place = place.id_place
                 AND    place_producto.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre
                 
                  ";
    $arrProducto = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrProducto[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
        $arrProducto[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
        $arrProducto[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["precio"];
        $arrProducto[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
        $arrProducto[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
        $arrProducto[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
        $arrProducto[$rTMP["id_place_producto"]]["favorito"] = intval($rTMP["id_usuario_producto_favorito"]) ? "Y" : "N";
        $arrProducto[$rTMP["id_place_producto"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arrProducto[$rTMP["id_place_producto"]]["id_place"] = $rTMP["id_place"];
        $arrProducto[$rTMP["id_place_producto"]]["titulo"] = $rTMP["titulo"];
        $arrProducto[$rTMP["id_place_producto"]]["url_logo"] = $rTMP["url_logo"];
        $arrProducto[$rTMP["id_place_producto"]]["padre_tang_en"] = $strLenguaje == "es" ? $rTMP["padre_tang_es"] : $rTMP["padre_tang_en"];
        $arrProducto[$rTMP["id_place_producto"]]["padre_tang_es"] = $rTMP["padre_tang_es"];
        
        if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
            
            $arrProducto[$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrProducto[$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["padre_hijo_tang_en"] = $rTMP["padre_hijo_tang_en"];
            $arrProducto[$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["padre_hijo_tang_es"] = $rTMP["padre_hijo_tang_es"];
            
        }
                    
    }
    
    $objDBClass->db_free_result($qTMP);
    return $arrProducto;    
}

function fntGetPlaceFavoritoUsuario($intIdUsuario, $boolInterno = false, $objDBClass = null, $strLenguaje = "es"){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strQuery = "SELECT place.id_place,
                        place.titulo,
                        place.subTitulo,
                        place.descripcion,
                        place.url_logo
                 FROM   usuario_place_favorito,
                        place
                 WHERE  usuario_place_favorito.id_usuario = {$intIdUsuario}
                 AND    usuario_place_favorito.id_place = place.id_place
                  ";
    $arrPlace = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPlace[$rTMP["id_place"]]["id_place"] = $rTMP["id_place"];
        $arrPlace[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
        $arrPlace[$rTMP["id_place"]]["subTitulo"] = $rTMP["subTitulo"];
        $arrPlace[$rTMP["id_place"]]["descripcion"] = $rTMP["descripcion"];
        $arrPlace[$rTMP["id_place"]]["url_logo"] = $rTMP["url_logo"];
                    
    }
    
    $objDBClass->db_free_result($qTMP);
    return $arrPlace;    
}

function fntGetCatalogoEstadoPlace(){
    
    $arr["C"]["nombre"] = lang["creado"];
    $arr["A"]["nombre"] = lang["activo"];
    $arr["S"]["nombre"] = lang["verificando"];
    $arr["R"]["nombre"] = lang["rechazada"];
    $arr["P"]["nombre"] = lang["pendiente_de_pago"];
                
    return $arr;
}

function fntGetCatalogoSolicitudPlace(){
    
    $arr["A"]["nombre"] = "Activo";
    $arr["C"]["nombre"] = "Creado";
    $arr["S"]["nombre"] = "Solicitud Enviada";
    $arr["R"]["nombre"] = "Solicitud Rechazada";
    $arr["P"]["nombre"] = "Solicitud Pendiente de Pago";
                
    return $arr;
}

function fntGetCatalogoTipoCategoriaComercio(){
    
    $arr["1"]["nombre"] = "Comercio Pequeño";
    $arr["2"]["nombre"] = "Comercio Mediano";
    $arr["3"]["nombre"] = "Comercio Grande";
    
    return $arr;    
}

function fntGetCatalogoPaqueteProducto(){
    
    $arr["1"]["nombre"] = "NORMAL 5 PRODUCTOS";
    $arr["1"]["costo"]["mensual"] = "100.00";
    $arr["1"]["costo"]["semestral"] = "300.00";
    $arr["1"]["costo"]["anual"] = "500.00";
    
    $arr["2"]["nombre"] = "FULL";
    $arr["2"]["costo"]["mensual"] = "140.00";
    $arr["2"]["costo"]["semestral"] = "420.00";
    $arr["2"]["costo"]["anual"] = "700.00";
    
    return $arr;    
}

function fntCatalogoFondoPlacePublico(){
    
    $arr[1]["nombre"] = "Fondo Azul";
    $arr[1]["url_fondo"] = "images/fondo.png";
    $arr[1]["color_hex"] = "3E53AC";
    
    $arr[2]["nombre"] = "Fondo Amarillo";
    $arr[2]["url_fondo"] = "images/fondo_amarillo.png";
    $arr[2]["color_hex"] = "808000";
    
    $arr[3]["nombre"] = "Fondo Rojo";
    $arr[3]["url_fondo"] = "images/fondo_rojo.png";
    $arr[3]["color_hex"] = "FF0000";
    
    $arr[4]["nombre"] = "Fondo Verde";
    $arr[4]["url_fondo"] = "images/fondo_verde.png";
    $arr[4]["color_hex"] = "008000";
    
    return $arr;
    
}

function fntUbicacion($boolInterno = false, $objDBClass = null){
    /**************   Antigua Guatemala   *******************/    
             
    $intKey = 1;
    $arr[$intKey]["nombre"] = "antigua_guatemala";
    $arr[$intKey]["tag_es"] = "Antigua Guatemala";
    $arr[$intKey]["tag_en"] = "Antigua Guatemala";
    $arr[$intKey]["lat"] = "14.5572969";
    $arr[$intKey]["lng"] = "-90.73322329999996";
    $arr[$intKey]["radio"] = "5000";
                 
    /**************   Ciudad Guatemala   *******************/    
    /*
    $intKey = 2;
    $arr[$intKey]["nombre"] = "ciudad_guatemala";
    $arr[$intKey]["tag_es"] = "Ciudad de Guatemala";
    $arr[$intKey]["tag_en"] = "Guatemala City";
    $arr[$intKey]["lat"] = "14.6349149";
    $arr[$intKey]["lng"] = "-90.5068824";
    $arr[$intKey]["radio"] = "5000";
      */           
             
    /**************   Carretera al Salvador   *******************/
    /*    
    $intKey = 3;
    $arr[$intKey]["nombre"] = "carretera_salvador";
    $arr[$intKey]["tag_es"] = "Carretera al Salvador";
    $arr[$intKey]["tag_en"] = "Carretera al Salvador";
    $arr[$intKey]["lat"] = "14.5521096";
    $arr[$intKey]["lng"] = "-90.45302579999998";
    $arr[$intKey]["radio"] = "5000";
        */    
    return $arr;
        
}

function fntUbicacionLugar(){
    
    /**************   Antigua Guatemala   *******************/    
             
    $intKey = 1;
    $intKeyLugar = 1;
    $arr[$intKey]["lugar"][$intKeyLugar]["nombre"] = "antigua_guatemala";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_es"] = "Antigua Guatemala";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_en"] = "Antigua Guatemala";
    $arr[$intKey]["lugar"][$intKeyLugar]["lat"] = "14.5572969";
    $arr[$intKey]["lugar"][$intKeyLugar]["lng"] = "-90.73322329999996";
    $arr[$intKey]["lugar"][$intKeyLugar]["radio"] = "2000";
    
    /**************   Ciudad Guatemala   *******************/    
                 
    for( $i = 1 ; $i <= 25 ; $i++ ){
        
        $intKey = 2;
        $intKeyLugar++;
        $arr[$intKey]["lugar"][$intKeyLugar]["nombre"] = "zona_{$i}";
        $arr[$intKey]["lugar"][$intKeyLugar]["tag_es"] = "Zona {$i}";
        $arr[$intKey]["lugar"][$intKeyLugar]["tag_en"] = "Zone {$i}";
        $arr[$intKey]["lugar"][$intKeyLugar]["lat"] = "14.5572969";
        $arr[$intKey]["lugar"][$intKeyLugar]["lng"] = "-90.73322329999996";
        $arr[$intKey]["lugar"][$intKeyLugar]["radio"] = "1000";
        
    }
             
             
    /**************   Carretera al Salvador   *******************/    
    $intKey = 3;
    $intKeyLugar++;
    $arr[$intKey]["lugar"][$intKeyLugar]["nombre"] = "san_jose_pinula";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_es"] = "San Jose Pinula";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_en"] = "San Jose Pinula";
    $arr[$intKey]["lugar"][$intKeyLugar]["lat"] = "14.5447785";
    $arr[$intKey]["lugar"][$intKeyLugar]["lng"] = "-90.41016189999999";
    $arr[$intKey]["lugar"][$intKeyLugar]["radio"] = "1000";
         
    $intKey = 3;
    $intKeyLugar++;
    $arr[$intKey]["lugar"][$intKeyLugar]["nombre"] = "fraijanes";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_es"] = "Fraijanes";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_en"] = "Fraijanes";
    $arr[$intKey]["lugar"][$intKeyLugar]["lat"] = "14.4631813";
    $arr[$intKey]["lugar"][$intKeyLugar]["lng"] = "-90.44316179999998";
    $arr[$intKey]["lugar"][$intKeyLugar]["radio"] = "1000";
            
    $intKey = 3;
    $intKeyLugar++;
    $arr[$intKey]["lugar"][$intKeyLugar]["nombre"] = "muxbal";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_es"] = "Muxbal";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_en"] = "Muxbal";
    $arr[$intKey]["lugar"][$intKeyLugar]["lat"] = "14.4631813";
    $arr[$intKey]["lugar"][$intKeyLugar]["lng"] = "-90.44316179999998";
    $arr[$intKey]["lugar"][$intKeyLugar]["radio"] = "1000";
             
    $intKey = 3;
    $intKeyLugar++;
    $arr[$intKey]["lugar"][$intKeyLugar]["nombre"] = "condado_concepcion";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_es"] = "Condado Concepción";
    $arr[$intKey]["lugar"][$intKeyLugar]["tag_en"] = "Condado Concepción";
    $arr[$intKey]["lugar"][$intKeyLugar]["lat"] = "14.4631813";
    $arr[$intKey]["lugar"][$intKeyLugar]["lng"] = "-90.44316179999998";
    $arr[$intKey]["lugar"][$intKeyLugar]["radio"] = "1000";
               
                             
    
    return $arr;
    
}

function fntGetApiKeyPlace(){
    
    return "AIzaSyBfVjj9Zei1MUXxszfkRwPdsGXC7ddOk7o";
    
}

function fntGetPlaceProductoGaleria($intPlaceProducto, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $intPlaceProducto = intval($intPlaceProducto);
    
    $strQuery = "SELECT id_place_producto_galeria,
                        url_imagen
                 FROM   place_producto_galeria
                 WHERE  id_place_producto = {$intPlaceProducto}
                 ORDER BY id_place_producto_galeria";        
        
    $arr = array();         
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_place_producto_galeria"]]["url_imagen"] = $rTMP["url_imagen"];  
        
    }                               
    $objDBClass->db_free_result($qTMP) ;
    return $arr;
    
}

function fntGetPlaceProductoDatos($intPlaceProducto, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strFiltroUsuario = "";
    $strFiltroUsuario2 = "";
    
    $strQuery = "SELECT *
                 FROM   place_producto
                 WHERE  id_place_producto = {$intPlaceProducto} 
                  ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    $rTMP["clasificacionPadreHijo"] = array();
    
    $strQuery = "SELECT id_clasificacion_padre_hijo
                 FROM   place_producto_clasificacion_padre_hijo
                 WHERE  id_place_producto = {$intPlaceProducto} ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $arrTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $rTMP["clasificacionPadreHijo"][$arrTMP["id_clasificacion_padre_hijo"]] = $arrTMP["id_clasificacion_padre_hijo"];
            
    }
    $objDBClass->db_free_result($qTMP);
    
    return $rTMP;  
    
}

function fntGetCatalogoCategoriaEspecial($boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strFiltroUsuario = "";
    $strFiltroUsuario2 = "";
    
    $strQuery = "SELECT *
                 FROM   categoria_especial
                 WHERE  estado = 'A'
                  ";
    $arr = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arr[$rTMP["id_categoria_especial"]] = $rTMP;    
    }
    $objDBClass->db_free_result($qTMP);
    return $arr;  
    
}

function fntDrawTrProducto($rTMP, $strHeightPX = "height: 190px;", $strColorHex = "3E53AC", $intCount = 1){
    
    $rTMP["key"] = str_replace("_", "", $rTMP["key"]);
    $rTMP["key"] = str_replace("1", "", $rTMP["key"]);
    
    $rTMP["key"] = intval($rTMP["key"]);
    
    $intIdentificador = dechex($rTMP["key"]*100);
    
    ?>
    
    <div class="col-12 col-lg-3 p-lg-4 p-0  m-0 mb-3 mb-lg-0  "  onclick="window.open('<?php print $rTMP["value"]["url_place"]?>?pro=<?php print fntCoreEncrypt($rTMP["value"]["id_place_producto"])?>')">
        
        <div class="row p-0 m-0" >
            <div class="col-lg-12  col-12 p-0 m-0  pl-lg-1 pr-lg-1 mt-1">
                
                <?php
                //logIn     webToken
                if( false && sesion["logIn"] ){
                    
                    ?>
                    <div style="position: absolute; top: 0px; left: 0px; z-index: 997;"> 
                        <i class="fa fa-heart" onclick="fntDisplayIconoFavorito('iconProFavoritoSi_<?php print $rTMP["key"]?>', 'iconProFavoritoNo_<?php print $rTMP["key"]?>', true, '<?php print fntCoreEncrypt($rTMP["key"])?>');" id="iconProFavoritoNo_<?php print $rTMP["key"]?>" style="cursor: pointer; color: black; <?php print $rTMP["value"]["favorito"] == "Y" ? "display: none;" : ""?>"></i>
                        <i class="fa fa-heart"   onclick="fntDisplayIconoFavorito('iconProFavoritoSi_<?php print $rTMP["key"]?>', 'iconProFavoritoNo_<?php print $rTMP["key"]?>', false, '<?php print fntCoreEncrypt($rTMP["key"])?>');" id="iconProFavoritoSi_<?php print $rTMP["key"]?>" style="cursor: pointer; color: red; <?php print $rTMP["value"]["favorito"] == "Y" ? "" : "display: none;"?>"></i>
                    </div>
                    <?php
                    
                }
                
                if( $rTMP["value"]["tipo"] == 1 ){
                    
                    ?>
                    
                    <img style="width: 100%; <?php print $strHeightPX?> object-fit: cover; border-radius: 15px;" src="<?php print !empty($rTMP["value"]["logo_url"]) ? "imgInguate.php?t=up&src=".$rTMP["value"]["logo_url"] : "images/Noimagee.jpg?1"?>" class="img-fluid rounded">
                            
                    <?php    
                    
                }
                
                if( $rTMP["value"]["tipo"] == 2 ){
                    
                    ?> 
                    <div id="carouselExampleIndicators_<?php print $intCount?>" class="carousel slide" data-ride="carousel" data-interval="<?php print 800 + ( 300 * $intCount )?>">
                        
                        <div class="carousel-inner">
                            
                            <?php
                            
                            reset($rTMP["value"]["galeria"]);
                            $intCountSlide = 0;
                            while( $arrTMP = each($rTMP["value"]["galeria"]) ){
                                ?>
                                <div class="carousel-item <?php print $intCountSlide == 0 ? "active" : "" ?>">
                                
                                    <img style="width: 100%; <?php print $strHeightPX?> object-fit: cover; border-radius: 15px;" src="imgInguate.php?t=up&src=<?php print $arrTMP["value"]["url_imagen"]?>" class="img-fluid rounded">
                            
                                </div>
                                <?php
                                $intCountSlide++;
                            }
                            
                            ?>
                            
                        </div> 
                        
                                                    
                    </div>
                    <?php
                }
                
                ?>            
            
                        
                  
            </div>
            <div class="col-lg-12 col-7 p-0 m-0  pl-0 pl-lg-1 pr-lg-1 mt-1 text-left" >
        
                <h5  class="text-dark text-left text-hover-underline lis-line-height-1 m-0 p-0" style="font-size: 18px; margin-top: 5px;">
                    
                    <?php print $rTMP["value"]["nombre"];?>                   
                
                </h5>
                    
                <h5 class=" text-secondary text-left m-0 p-0" style="font-size: 14px; margin-top: 5px;">
                    
                    <span class="badge  text-white text-background-theme " onclick="window.open('placeup.php?p=<?php print fntCoreEncrypt($rTMP["value"]["id_place"])?>')" style="cursor: pointer;"><?php print $rTMP["value"]["titulo"] ?></span>
                
                </h5>    
                <span class="text-left m-0 p-0 text-color-theme" style="font-size: 15px; margin-top: 5px;">
                    
                    Q <?php print number_format($rTMP["value"]["precio"], 2);?>             
                
                </span>
                
                <?php
                
                if( false && sesion["logIn"] ){
                    ?>
                    <ul class="list-group list-group-horizontal" >
                        
                        <li onclick="fntLikeProducto('<?php print fntCoreEncrypt($rTMP["key"])?>', '<?php print $intIdentificador?>');" style="cursor: pointer;" class="list-group-item p-1 mr-2 border-0">
                            
                            <span like="<?php print $rTMP["value"]["mi_like"] == "Y" ? "Y" : "N"?>" countLike="<?php print $rTMP["value"]["like"]?>" id="spCountLikeProducto_<?php print $intIdentificador?>">
                                <?php print number_format($rTMP["value"]["like"], 0)?>
                            </span>
                            <i id="iIconoLikeProducto_<?php print $intIdentificador?>" style="<?php print $rTMP["value"]["mi_like"] == "Y" ? " color: #3E53AC; " : ""?>" class="fa fa-thumbs-up "></i>
                        
                        </li>
                        <li onclick="fntNoLikeProducto('<?php print fntCoreEncrypt($rTMP["key"])?>', '<?php print $intIdentificador?>');" style="cursor: pointer;" class="list-group-item p-1 ml-2 border-0">
                            
                            <span nolike="<?php print $rTMP["value"]["no_mi_like"] == "Y" ? "Y" : "N"?>" countLike="<?php print $rTMP["value"]["no_like"]?>" id="spCountNoLikeProducto_<?php print $intIdentificador?>">
                                <?php print number_format($rTMP["value"]["no_like"], 0)?>
                            </span>
                            <i id="iIconoNoLikeProducto_<?php print $intIdentificador?>" style="<?php print $rTMP["value"]["no_mi_like"] == "Y" ? " color: #3E53AC; " : ""?>" class="fa fa-thumbs-down " ></i>
                            
                        </li>
                    </ul>
                    <?php
                }                    
                
                ?>
                   
            </div>
        </div>
    </div>
    <?php
        
}

function fntDrawTrProductoPlace($rTMP, $strHeightPX = "height: 130px;", $strColorHex = "3E53AC", $intCount = 1, $arrFiltroClasificacionPadre = array()){
    
    ?>
    
    <div class="col-12 col-lg-3 p-lg-4 p-0  m-0 mb-3 mb-lg-0" onclick="window.open('/<?php print $rTMP["value"]["url_place"]?>')">
        <div class="row p-0 m-0">
            <div class="col-lg-12  col-12 p-0 m-0  pl-lg-1 pr-lg-1 mt-1 ">
                <?php
                
                if( false && sesion["logIn"] ){
                    
                    ?>
                    <div style="position: absolute; top: 0px; left: 0px; z-index: 1049;"> 
                        <i class="fa fa-heart" onclick="fntDisplayIconoFavoritoPlace('iconPlaceProFavoritoSi_<?php print $rTMP["key"]?>', 'iconPlaceProFavoritoNo_<?php print $rTMP["key"]?>', true, '<?php print fntCoreEncrypt($rTMP["value"]["id_place"])?>');" id="iconPlaceProFavoritoNo_<?php print $rTMP["key"]?>" style="cursor: pointer; color: black; <?php print $rTMP["value"]["favorito"] == "Y" ? "display: none;" : ""?>"></i>
                        <i class="fa fa-heart" onclick="fntDisplayIconoFavoritoPlace('iconPlaceProFavoritoSi_<?php print $rTMP["key"]?>', 'iconPlaceProFavoritoNo_<?php print $rTMP["key"]?>', false, '<?php print fntCoreEncrypt($rTMP["value"]["id_place"])?>');" id="iconPlaceProFavoritoSi_<?php print $rTMP["key"]?>" style="cursor: pointer; color: red; <?php print $rTMP["value"]["favorito"] == "Y" ? "" : "display: none;"?>"></i>
                    
                    </div>
                    <?php
                    
                }
                
                ?>
                    
                <div id="carouselExampleIndicators_<?php print $intCount?>" class="carousel slide" data-ride="carousel" data-interval="<?php print 800 + ( 300 * $intCount )?>">
                    
                    <div class="carousel-inner ">
                        
                        <?php
                        reset($rTMP["value"]["galeria"]);
                        $intCountSlide = 0;
                        while( $arrTMP = each($rTMP["value"]["galeria"]) ){
                            ?>
                            <div class="carousel-item  <?php print $intCountSlide == 0 ? "active" : "" ?>">
                                
                                <img style="width: 100%; <?php print $strHeightPX?> object-fit: cover; border-radius: 15px;" src="imgInguate.php?t=up&src=<?php print $arrTMP["value"]["url_imagen"]?>" class="img-fluid rounded">
                            
                            </div>
                            <?php
                            $intCountSlide++; 
                            
                        }
                        ?>
                        
                    </div> 
                                               
                </div>    
                  
            </div>
            
            <div class="col-12 m-0 p-0" style="margin-top: 5px;" >    
                <h5  class="text-dark text-left text-hover-underline lis-line-height-1 m-0 p-0" style="font-size: 18px;">   
                    <?php print $rTMP["value"]["titulo"];?>            
                </h5>       
                <!--
                <p  class="text-dark text-left text-hover-underline lis-line-height-1 m-0 p-0" style="font-size: 15px;">   
                    <?php print "aaaaaa"//$rTMP["value"]["direccion"];?>               
                </p>     -->  
            </div>       
            <div class="col-6 m-0 p-0" style="margin-top: 5px;" >    
                <h5  class="text-left text-hover-underline lis-line-height-1 m-0 p-0" style="font-size: 15px;">   
                    <?php
                                
                    for( $i = 1; $i <= intval($rTMP["value"]["estrellas"]) ; $i++ ){
                        ?>
                        <i class="fa fa-star text-tema"></i>
                        <?php
                    }
                    
                    ?>
                </h5>           
            </div>       
            <div class="col-6 m-0 p-0" style="margin-top: 5px;" >    
                <h5  class="text-right text-hover-underline lis-line-height-1 m-0 p-0 " style="font-size: 15px;">   
                    <span class="badge  text-white text-background-theme" > 
                        Q <?php print number_format($rTMP["value"]["precio_mediana_bajo"], 2) ." - ".number_format($rTMP["value"]["precio_mediana_alto"], 2);?>
                    </span>
                </h5>           
            </div>  
            
            <div class="col-12 p-0 m-0" style="margin-top: 5px;">
            
                <h5 class=" text-secondary text-left tm-0 p-0" style="font-size: 14px; display: none;">
                    
                    <?php
                    
                    reset($rTMP["value"]["clasificacion_padre"]);
                    while( $arrTMP = each($rTMP["value"]["clasificacion_padre"]) ){
                        
                        if( isset($arrFiltroClasificacionPadre[$arrTMP["key"]]) ){
                            
                            ?>
                            
                            <span class="badge  text-white text-background-theme " ><?php print $arrFiltroClasificacionPadre[$arrTMP["key"]]["texto"] ?></span>
                        
                            <?php
                        }
                            
                    }
                    
                    ?>
                                  
                </h5>
                
            </div>    
            
            
        </div>
    </div>
    <?php
    
}

function fntGetCatalogoCategoriaEspecialFrecuenciaPago(){
    
    $arr["1"]["nombre"] = "semanal";
    $arr["1"]["tag_es"] = "Semanal";
    $arr["1"]["tag_en"] = "Weekly";
    $arr["1"]["precio"] = "300";
    
    $arr["2"]["nombre"] = "mensual";
    $arr["2"]["tag_es"] = "Mensual";
    $arr["2"]["tag_en"] = "Monthly";
    $arr["2"]["precio"] = "700";
    
    return $arr;
}

function fntGetCatalogoCategoriaEspecialSolicitudEstado(){
    
    $arr["A"]["nombre"] = "Solicitud Activa";
    $arr["A"]["tag_es"] = "Solicitud Activa";
    $arr["A"]["tag_en"] = "Active Request";
    
    $arr["S"]["nombre"] = "Solicitud Enviada";
    $arr["S"]["tag_es"] = "Solicitud Enviada";
    $arr["S"]["tag_en"] = "Request sent";
    
    $arr["R"]["nombre"] = "Solicitud Rechazada";
    $arr["R"]["tag_es"] = "Solicitud Enviada";
    $arr["R"]["tag_en"] = "Rejected request";
    
    return $arr;
}

function fntGetPlaceProductoCategoriaEspecial($intPlaceProducto, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strQuery = "SELECT *
                 FROM   sol_categoria_especial
                 WHERE  id_place_producto = {$intPlaceProducto} 
                  ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    return $rTMP;  
    
}

function fntDrawPost($arrInfo, $strHeight = "450px"){
    
    $intIdentificador = dechex($arrInfo["id_post"]*100);
    
    //drawdebug($arrInfo);
    ?>
    <div class="card m-0 p-0 mt-4" style="border: 0px;">
                                        
        <div class="card-header p-2 m-0 border-0 text-left">
            
            <div class="row m-0 p-0">
                <div class="col-8">
                    <img src="imgInguate.php?t=up&src=../../file_inguate/usuario_perfil/<?php print fntCoreEncrypt($arrInfo["id_usuario_propietario"])?>.png" style="border-radius: 50%; object-fit: cover; width: 40px; height: 40px;" >
                               
                    <span onclick="window.open('profile.php?p=<?php print fntCoreEncrypt($arrInfo["id_usuario_propietario"])?>')" class="text-hover-underline">
                        <?php print $arrInfo["nombre_propietario"]?> 
                    </span>
                </div>
                <div class="col-4 text-right p-1">
                    
                    <span valueCount="<?php print $arrInfo["like"]?>" id="spnLikePost_<?php print $intIdentificador?>">
                        <?php print $arrInfo["like"] > 0 ? number_format($arrInfo["like"], 0) : ""?>
                    </span>
                    
                    <i class="fa fa-thumbs-up " id="spnIconoPost_<?php print $intIdentificador?>" onclick="<?php print $arrInfo["mi_like"] == "N" ? " fntSetLikePost('".fntCoreEncrypt($arrInfo["id_post"])."', 'spnLikePost_".$intIdentificador."', 'spnIconoPost_".$intIdentificador."');  " : ""?>"   style=" <?php print $arrInfo["mi_like"] == "Y" ? " color: #3E53AC;" : "cursor: pointer;"?>"></i>
                
                </div>
            </div>
            
                    
        </div>
        
        <?php
           
        if( isset($arrInfo["galeria"]) && count($arrInfo["galeria"]) > 0 ){
            
            ?>
            <div id="crsPost_<?php print $intIdentificador?>" class="carousel slide" data-ride="">
                <div class="carousel-inner">
                    <?php
                    
                    $intCount = 1;
                    while( $rTMP = each($arrInfo["galeria"]) ){
                        
                        ?>       
                        <div class="carousel-item <?php print $intCount == 1 ? "active" : ""?>">
                            
                            <img src="imgInguate.php?src=<?php print $rTMP["value"]["imagen_url"]?>" class="card-img-top img-fluid" style="object-fit: cover; width: 100%; height: <?php print $strHeight?>;">
                
                        </div>
                        <?php
                        $intCount++;
                    }
                    
                    ?>
                </div>
                <a class="carousel-control-prev" href="#crsPost_<?php print $intIdentificador?>" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#crsPost_<?php print $intIdentificador?>" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <?php
            
                        
        }
        else{
            ?>
            <div class="card-body p-2">
            <p class="card-text text-left">
                <?php print $arrInfo["contenido"]?>
            </p>
        </div>
            <?php
        }
        
        if( ( isset($arrInfo["galeria"]) && count($arrInfo["galeria"]) > 0 ) ) {
            
            ?>
            <div class="card-body p-2">
                <p class="card-text text-left">
                    <?php print $arrInfo["contenido"]?>
                </p>
            </div>    
            <?php
        }
        
        if( ( isset($arrInfo["respuesta"]) && count($arrInfo["respuesta"]) > 0 ) ) {
            
            ?>
            <div class="card-body p-2">
                <ul class="list-group list-group-horizontal ">
                    <?php
                    
                    while( $rTMP = each($arrInfo["respuesta"]) ){
                        
                        ?>
                        
                        <li onclick="fntSetPostRespuestaUsuario('<?php print fntCoreEncrypt($rTMP["value"]["id_post_respuesta"])?>');" class="flex-fill list-group-item list-group-item-light list-group-item-action p-1" style="cursor: pointer; min-height: 100%;">
                            
                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td class="align-middle" >
                                        
                                        <?php print $rTMP["value"]["respuesta"]?>
                        
                                    </td>
                                </tr>
                            </table>
                            
                        </li>
                            
                        <?php
                    }
                    
                    ?>
                </ul>
            </div>    
            <?php
        }
        
        ?> 
        
        <div class="card-body p-2">
            <div class="row" id="divPostComentario_<?php print $intIdentificador?>">
                
                <?php
                
                if( isset($arrInfo["comentario"]) && count($arrInfo["comentario"]) > 0 ){
                    
                    while( $rTMP = each($arrInfo["comentario"]) ){
                        
                        ?>
                        <div class="col-12 text-left">
                            <span style="font-weight: bold;"><?php print $rTMP["value"]["nombre_usuario"]?></span>
                            <?php print $rTMP["value"]["comentario"]?>
                        </div>
                        <?php
                        
                    }
                    
                }                
                
                ?>
                        
                
            </div>
            <div class="row">
                <div class="col-12 text-left mt-2">

                    <div class="input-group mb-3">
                        
                        <input id="txtComentarioPost_<?php print $intIdentificador;?>" name="txtComentarioPost_<?php print $intIdentificador;?>" type="text" class="form-control form-control-sm border-0" placeholder="Agrega tu comentario..." aria-label="Recipient's username" aria-describedby="button-addon2">
                        
                        <div class="input-group-append">
                            
                            <button class="btn btn-outline-secondary btn-sm border-0" type="button" onclick="fntSetComentarioPost('<?php print $intIdentificador;?>', '<?php print fntCoreEncrypt($arrInfo["id_post"])?>');">Comentar</button>
                        
                        </div>
                    
                    </div> 

                </div>
            </div>
        </div>
        
    </div>
    <?php
        
}

function fntGetScriptFuncionPost(){
    ?>
    <script>
        
        function fntSetLikePost(strKey, strObjLike, strObjIcono){
            
            intCount = $("#"+strObjLike).attr( "valueCount" );
            intCount++;
            
            $("#"+strObjLike).attr("valueCount", intCount);
            $("#"+strObjLike).html(intCount);
            
            $("#"+strObjIcono).attr("onclick", "");
            $("#"+strObjIcono).css("color", "#3E53AC");
            
            $.ajax({
                url: "servicio_core.php?servicio=setLikePost&key="+strKey, 
                success: function(result){
                    
                }
                
            });
            
            
        }
        
        function fntSetComentarioPost(strIdentificador, strKey){
            
            strComentario = $("#txtComentarioPost_"+strIdentificador).val();
            $("#txtComentarioPost_"+strIdentificador).val("");
            
            $("#divPostComentario_"+strIdentificador).append("<div class=\"col-12 text-left\">   "+
                                           "     <span style=\"font-weight: bold;\"><?php print sesion["nombre"]?></span>    "+
                                           "     "+strComentario+"  "+
                                           " </div>")
            
            var formData = new FormData();
            formData.append("txtComentario", strComentario);
                
            $(".preloader").fadeIn();
            $.ajax({
                url: "servicio_core.php?servicio=setComentarioPost&key="+strKey, 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                        
                }
                        
            });
        }
        
        function fntSetPostRespuestaUsuario(strRespuesta){
            
            alert(strRespuesta);
                        
        }
        
    </script>
    <?php
}

function fntGetUsuarioAmigo($intIdUsuario, $objDBClass){

    $arr = array();
    $strQuery = "SELECT id_usuario_amigo
                 FROM   usuario_amigo
                 WHERE  id_usuario = {$intIdUsuario}   ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){

        $arr[$rTMP["id_usuario_amigo"]] = $rTMP["id_usuario_amigo"];
        
    }
    $objDBClass->db_free_result($qTMP);

    return $arr;    
}

function fntGetPostUsuario($intIdUsuario, $arrUsuario, $objDBClass){
    
    $strIdUsuario = implode(',', $arrUsuario);
    
    $strQuery = "SELECT post.id_post,
                        post.contenido,
                        post.add_fecha,
                        post.id_usuario,
                        
                        post.propietario,
                        post.id_post_original,
                        post.id_usuario_propietario,
                        
                        usuario.nombre nombre_propietario,
                        usuario.apellido apellido_propietario
                 FROM   post,
                        usuario
                 WHERE  post.id_usuario IN ({$strIdUsuario}) 
                 AND    post.id_usuario = usuario.id_usuario
                 ORDER BY add_fecha DESC       
                 ";
                 
    $arrPost = array();
    $arrPostVerificarPropietario = array();
    $strPost = "";
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
        
        $arrPost[$rTMP["id_post"]]["id_post"] = $rTMP["id_post"];
        $arrPost[$rTMP["id_post"]]["contenido"] = $rTMP["contenido"];
        $arrPost[$rTMP["id_post"]]["add_fecha"] = $rTMP["add_fecha"];
        $arrPost[$rTMP["id_post"]]["propietario"] = $rTMP["propietario"];
        $arrPost[$rTMP["id_post"]]["id_usuario_propietario"] = $rTMP["id_usuario"];
        $arrPost[$rTMP["id_post"]]["nombre_propietario"] = $rTMP["nombre_propietario"];
        $arrPost[$rTMP["id_post"]]["like"] = 0;
        $arrPost[$rTMP["id_post"]]["mi_like"] = 'N';
        
        if( $rTMP["propietario"] == "N" ){
            
            $arrPostVerificarPropietario[$rTMP["id_usuario_propietario"]][$rTMP["id_post"]]["id_post"] = $rTMP["id_post"];
            
        }
        
            
    }
    $objDBClass->db_free_result($qTMP);
    
    if( count($arrPost) > 0 ){
        
        //Verificar Propietarios
        if( count($arrPostVerificarPropietario) > 0 ){
            
            $strUsuarioPropietario = "";
            while( $rTMP = each($arrPostVerificarPropietario) ){
            
                $strUsuarioPropietario .= ( $strUsuarioPropietario == "" ? "" : "," ).$rTMP["key"];
                
            }
            
            $strQuery = "SELECT id_usuario_amigo
                         FROM   usuario_amigo
                         WHERE  id_usuario = {$intIdUsuario}
                         AND    id_usuario_amigo IN({$strUsuarioPropietario}) ";
            $qTMP = $objDBClass->db_consulta($strQuery);
            while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
                
                
                if( isset($arrPostVerificarPropietario[$rTMP["id_usuario_amigo"]]) ){
                    
                    reset($arrPostVerificarPropietario);
                    while( $arrTMP = each($arrPostVerificarPropietario[$rTMP["id_usuario_amigo"]]) ){
                        
                        if( isset($arrPost[$arrTMP["value"]["id_post"]]) )
                            unset($arrPost[$arrTMP["value"]["id_post"]]);
                            
                                
                    }
                    
                }
                    
            }
            $objDBClass->db_free_result($qTMP);
            
                
        }
        
        while( $rTMP = each($arrPost) ){
            
            $strPost .= ( $strPost == "" ? "" : "," ).$rTMP["key"];
            
        }
        reset($arrPost);
        
               /*
        //Galeria
        $strQuery = "SELECT id_post_galeria, 
                            id_post,
                            imagen_url
                     FROM   post_galeria
                     WHERE  id_post IN({$strPost})     
                     ORDER BY id_post_galeria ASC";
                     
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
            
            $arrPost[$rTMP["id_post"]]["galeria"][$rTMP["id_post_galeria"]]["imagen_url"] = $rTMP["imagen_url"];
            
        }
        $objDBClass->db_free_result($qTMP);

              */
        //Likes
        $strQuery = "SELECT id_post,
                            COUNT(*) contador_like,
                            MAX(CASE WHEN id_usuario = {$intIdUsuario} THEN 'Y' ELSE 'N' END) mi_like
                     FROM   post_like
                     WHERE  id_post IN ({$strPost})
                     GROUP BY id_post";
        
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
            
            $arrPost[$rTMP["id_post"]]["like"] = $rTMP["contador_like"];
            $arrPost[$rTMP["id_post"]]["mi_like"] = $rTMP["mi_like"];
            
        }
        $objDBClass->db_free_result($qTMP);
        
        // Respuestas
        $strQuery = "SELECT id_post_respuesta, 
                            id_post,
                            orden,
                            respuesta
                     FROM   post_respuesta
                     WHERE  id_post IN({$strPost})     
                     ORDER BY orden ASC";
                     
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
            
            $arrPost[$rTMP["id_post"]]["respuesta"][$rTMP["id_post_respuesta"]]["id_post_respuesta"] = $rTMP["id_post_respuesta"];
            $arrPost[$rTMP["id_post"]]["respuesta"][$rTMP["id_post_respuesta"]]["orden"] = $rTMP["orden"];
            $arrPost[$rTMP["id_post"]]["respuesta"][$rTMP["id_post_respuesta"]]["respuesta"] = $rTMP["respuesta"];
            
        }
        $objDBClass->db_free_result($qTMP);

        
        //Comentarios
        $strQuery = "SELECT post_comentario.id_post_comentario,
                            post_comentario.id_post,
                            post_comentario.id_usuario,
                            post_comentario.comentario,
                            post_comentario.add_fecha,
                            usuario.nombre nombre_usuario
                     FROM   post_comentario,
                            usuario
                     WHERE  post_comentario.id_post IN({$strPost})
                     AND    post_comentario.id_usuario = usuario.id_usuario
                     ORDER BY id_post_comentario ASC";
        
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
            
            $arrPost[$rTMP["id_post"]]["comentario"][$rTMP["id_post_comentario"]]["id_post_comentario"] = $rTMP["id_post_comentario"];
            $arrPost[$rTMP["id_post"]]["comentario"][$rTMP["id_post_comentario"]]["id_usuario"] = $rTMP["id_usuario"];
            $arrPost[$rTMP["id_post"]]["comentario"][$rTMP["id_post_comentario"]]["nombre_usuario"] = $rTMP["nombre_usuario"];
            $arrPost[$rTMP["id_post"]]["comentario"][$rTMP["id_post_comentario"]]["comentario"] = $rTMP["comentario"];
            
        }
        $objDBClass->db_free_result($qTMP);
         
        
    }
    
    return $arrPost;        
    
}

function fntCatalogoSexo(){
    
    $arr["1"]["tag_es"] = "Femenino";
    $arr["1"]["tag_en"] = "Female";
    
    $arr["2"]["tag_es"] = "Masculino";
    $arr["2"]["tag_en"] = "Male";
    
    return $arr;
}

function fntGetDatosUsuario($intIdUsuario, $objDBClass){
    
    $strQuery = "SELECT *
                 FROM   usuario
                 WHERE  id_usuario = {$intIdUsuario}   ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);

    return $rTMP;
    
}

function fntOptimizarImagen($strUrl, $strNombre, $strExtension = "webp"){
    
    $strArchivoOrigen = $strUrl;
    
    shell_exec(" sudo su ");
    shell_exec(" cwebp {$strArchivoOrigen} -q 30 -alpha_q 100 -m 6 -o ../../{$strNombre}.webp ");
    shell_exec(" rm {$strArchivoOrigen} ");
    shell_exec(" mv ../../{$strNombre}.webp  {$strArchivoOrigen} ");
    
}

function fntShowBarNavegacionFooter($boolJsAutoComplete = false, $strBorderHex = "3E53AC", $arrAlerta = array()){
    
    $arrInfoDispositivoi = fntGetInformacionClienteNavegacion();
    
    ?>
    
    <div id="divBarraNavegacionFooter">
        <?php
        
        if( $boolJsAutoComplete  ){
            ?>
            <script src="dist/autocomplete/jquery.auto-complete.min.js"></script>

            <?php
        }
        ?>
        
        <div class="d-block d-md-none" style="position: fixed; bottom: 0%; width: 100%; z-index: 998">
            <div class="row p-0 m-0">
                
                <div onclick="location.href = 'index.php'" class="col-3 mt-2 pb-1 pr-0 bg-white text-center " style="border-top: 10px solid #<?php print $strBorderHex?>;">
                    
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td class="text-right pr-2" style="vertical-align: middle;">
                                <img src="images/LogoInguateAZUL.png" class="img-fluidd " style="width: 100%; height: auto;" >
                    
                            </td>
                        </tr>
                    </table>
                    
                </div>
                
                <div class="col-6 bg-white pb-1 p-0" onclick="" style="border-radius:10px 10px 0px 0px; border-top: 18px solid #<?php print $strBorderHex?>;">
                    
                    <div class="bg-white m-1" style="position: absolute; top: -15px; left: 0px; border-radius:10px 10px 0px 0px; width: 96%;">
                    
                        <table style="width: 100%; height: 100%;">
                            <tr>
                                <td class="text-right" style="vertical-align: middle; ">
                                    
                                    <i class="fa fa-search" style="color: #3E53AC;  font-size: 18px;"></i>
                                    
                                </td>
                                <td class="text-center" nowrap style="vertical-align: middle; width: 78%;">
                                    
                                    <input type="text" id="txtcoreQueBuscar" value="<?php print lang["textoPlaceHolderAutocomplete_movil"]?>" class="form-control form-con border-0 " onfocus="fntShowAutocomplete();" style="border-radius:10px 10px 0px 0px; font-size: 18px;"> 
                                                        
                                </td>
                            </tr>
                        </table>
                        
                    </div>
                    <br>
                    <br>    
                </div>
                
                <div class="col-3 bg-white mt-2 pb-1  p-0" style="border-top: 10px solid #<?php print $strBorderHex?>;">
                    
                    <table style="width: 100%; height: 100%;">
                        <tr>     
                            <td class="text-center" style="vertical-align: middle; width: 50%;">
                                <i class="fa fa-users hide" style="color: #3E53AC; font-size: 18px;"></i> 
                                
                                
                                <!--
                                <div style="position: absolute; top: 0px; right: 50%;">
                                    <div class="badge badge-danger" style="font-size: 10px;">1</div>
                                </div> 
                                -->
                                
                            </td>
                            <td class="text-center " style="vertical-align: middle; width: 50%;">
                                
                                <i class="fa fa-bars" id="sidebarCollapse" style="color: #3E53AC; font-size: 18px;"></i>
                                
                                <?php
                                
                                if( count($arrAlerta) > 0 ){
                                    ?>
                                    <div style="position: absolute; top: 0px; right: 0px;">
                                        <div class="badge badge-danger" style="font-size: 10px;"><?php print count($arrAlerta)?></div>
                                    </div>
                                    <?php
                                }
                                
                                ?>
                                    
                                
                            </td>
                        </tr>
                    </table>
                    
                </div>
                
            </div>
        </div>
        
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="dist/bootstrap-4.3.1-dist/jquery.mCustomScrollbar.min.css">
      
        <!-- jQuery Custom Scroller CDN -->
        <script src="dist/bootstrap-4.3.1-dist//jquery.mCustomScrollbar.concat.min.js"></script>
           
        <style>
            
            #sidebar {
                width: 250px;
                position: fixed;
                top: 0;
                right: -250px;
                height: 100%;
                z-index: 999;
                background: #3E53AC;
                color: #fff;
                transition: all 0.3s;
                overflow-y: scroll;
                box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.2);
            }

            #sidebar.active {
                right: 0;
            }

            #dismiss {
                width: 100%;
                height: 35px;
                line-height: 35px;
                text-align: center;
                background: #7386D5;
                position: absolute;
                bottom: 20%;
                left: 0%;
                cursor: pointer;
                -webkit-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }

            #dismiss:hover {
                background: #fff;
                color: #7386D5;
            }

            .overlay {
                display: none;
                position: fixed;
                width: 100vw;
                height: 100vh;
                background: rgba(0, 0, 0, 0.7);
                z-index: 998;
                opacity: 0;
                transition: all 0.5s ease-in-out;
                top: 0px
            }
            .overlay.active {
                display: block;
                opacity: 1;
            }

            #sidebar .sidebar-header {
                padding: 20px;
                background: #3E53AC;
            }

            #sidebar ul.components {
                padding: 20px 0;
                border-bottom: 1px solid #47748b;
                height: 10%;
                background: 100%;
            }

            #sidebar ul p {
                color: #fff;
                padding: 10px;
            }

            #sidebar ul li a {
                padding: 10px;
                font-size: 1.1em;
                display: block;
            }

            #sidebar ul li a:hover {
                color: #7386D5;
                background: #fff;
            }

            #sidebar ul li.active>a,
            a[aria-expanded="true"] {
                color: white;
                background: #6d7fcc;
            }

            a[data-toggle="collapse"] {
                position: relative;
            }

            .dropdown-toggle::after {
                display: block;
                position: absolute;
                top: 50%;
                right: 20px;
                transform: translateY(-50%);
            }

            ul ul a {
                font-size: 0.9em !important;
                padding-left: 30px !important;
                background: #6d7fcc;
            }
            
            .mCSB_container {
                overflow: hidden;
                width: auto;
                height: 100%;
            }
                              
        </style>  
        
        <!-- Sidebar  -->
        <nav id="sidebar">
            

            <div class="sidebar-header pt-4 pl-4 pr-4 pb-0">
                <h3 class="">
                    
                    <img src="images/LOGO-_INGUATE_WHITE.png" class="img-fluidd " style="width: 70%; height: auto;" >
                    
                
                </h3>
            </div>

            <ul class="list-unstyled components" style="height: 60%;">
            
                <?php
                                        
                if( sesion["logIn"] ){
                    
                    if( sesion["tipoUsuario"] == "3" ){
                        ?>
                        <li class="mt-1">
                            <a href="javascript:void(0)" onclick="fntDrawPreguntaNegocio();" class="text-white p-1">
                                <?php print lang["tienes_un_negocio"]?>
                            </a>
                        </li>
                        
                        <?php
                    }
                    elseif( sesion["tipoUsuario"] == "2" ){
                        ?>
                        <li class="mt-1">
                            <a href="javascript:void(0)" onclick="setLogInDashCookie();" class="text-white p-1">
                                <?php print lang["mi_negocio"]?> <i class="fa fa-external-link"></i>
                            </a>
                        </li>
                                                                
                        <?php    
                    }
                    
                    ?>
                    
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="p-1">
                            <?php print lang["hola"]." ".sesion["nombre"]?> <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <!--
                            <li>
                                <a href="javascript:void(0)" onclick="location.href = 'profile.php'" class="text-white">
                                    <?php print lang["perfil"]?> <i class="fa fa-user-circle"></i>
                                </a>
                            </li>  -->
                            <li>
                                <a href="javascript:void(0)" onclick="fntCerrarSesion();" class="text-white">
                                    <?php print lang["cerrar_sesion"]?> <i class="fa fa-power-off"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    
                    <?php
                }   
                else{
                    ?>
                    
                    <li class="mt-1">
                        <a href="javascript:void(0)" onclick="$('#navbarToggleExternalContent2').collapse('hide'); fntOpenModalRegistroCore();" class="text-white p-1">
                            <?php print lang["iniciar_sesion_registro"]?>
                        </a>
                    </li>
                    
                
                    <?php
                }                             
                
                ?>
                
                <li class="active mt-1 ">
                    <a href="#liUbicacionDrawer" class="text-white p-1" data-toggle="collapse" aria-expanded="false">
                        <?php print lang["ubicacion"]?> <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="liUbicacionDrawer">
                        
                        <?php
                        
                        $arrUbicacion = fntUbicacion();
                        while( $rTMP = each($arrUbicacion) ){
                                                
                            $strSelected = sesion["ubicacion"] == $rTMP["key"] ? "text-white" : "text-dark";
                            ?>
                            
                            <li class="">
                                <a href="javascript:void(0)" onclick="fntUbicacionCookie('<?php print $rTMP["key"]?>');" class="<?php print $strSelected;?>">
                                    <?php print sesion["lenguaje"] == "en" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?>
                                </a>
                            </li>    
                            <?php
                            
                        }
                        
                        ?>
                    
                    </ul>
                </li>
                
                <li class="active mt-1">
                    <a href="#liIdiomaDrawer" class="text-white p-1" data-toggle="collapse" aria-expanded="false">
                        <?php print lang["idioma"]?> <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="liIdiomaDrawer">
                        <li>
                            <a href="javascript:void(0)" onclick="fntSetLenguajeCookie('es');" class="<?php print sesion["lenguaje"] == "es" ? "text-white" : "text-dark"?>">
                                <?php print lang["espaniol"]?>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" onclick="fntSetLenguajeCookie('en');" class="<?php print sesion["lenguaje"] == "en" ? "text-white" : "text-dark"?>">
                                <?php print lang["ingles"]?>
                            </a>
                        </li>
                    </ul>
                </li>
                
                <?php
                
                if( count($arrAlerta) > 0 ){
                    
                    ?>
                    <li class="active mt-1">
                        <a href="#liAlertaDrawer" class="text-white p-1" data-toggle="collapse" aria-expanded="false">
                            <?php print lang["alertas"]?> 
                            <div class="badge badge-danger" style="font-size: 10px;"><?php print count($arrAlerta)?></div>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="collapse list-unstyled show" id="liAlertaDrawer">
                            
                            <?php
                            
                            while( $rTMP = each($arrAlerta) ){
                                
                                $strActionOnlcik = "setLogInDashCookie();";
                                                            
                                if( $rTMP["value"]["tipo"] == "SPE" )
                                    $strActionOnlcik = "setAlertaVista('".fntCoreEncrypt($rTMP["value"]["id_usuario_alerta"])."', true); ";
                                
                                ?>
                                <li class="">
                                    <a href="javascript:void(0)" onclick="<?php print $strActionOnlcik?>" class="p-1">
                                        <div class="bg-warning text-white p-1">
                                            <?php print $rTMP["value"]["titulo"]?>
                                            <div class="badge badge-dark" >Ver</div>
                                        </div>
                                    </a>
                                </li>
                                <?php
                            }
                            
                            ?>
                                
                        
                        </ul>
                    </li>
                    <?php
                    
                }
                
                ?>
                
                <li class="active mt-1">
                    <a href="javascript:void(0)" id="dismissDrawer" class="text-white text-center p-1" >
                        <i class="fa fa-arrow-left"></i>
                    </a>
                </li>
                <li class="active mt-1 p-2">
                    
                </li>
            </ul>

            <div class="p-3">
                <i class="fa fa-map-marker"></i> Colonia las victorias F-29, Jocotenango
                <br>
                <i class="fa fa-phone"></i> +502 4229 7744
                <br>
                <span onclick="window.open('<?php print sesion["lenguaje"] == "es" ? " https://wa.me/14242378844?text=Estoy%20interesado%20en%20Inguate" : "https://wa.me/14242378844?text=Im%20interested%20in%20inguate"?>');" style="cursor: pointer"><img src="images/whatsapp.png"  style="width: 20px; height: 20px;">  Whatsapp chat</span>

            </div>
            
               
        </nav>
        
        <div class="overlay"></div>        
        
        <div class="modal fade" id="mlAutoComplete" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content " style="margin-top: <?php print $arrInfoDispositivoi["plataforma"] == "iPhone" ? "90%" : "10%"?> ;">

                      <input type="text" value=""  id="txtAutocomplete_2" name="txtAutocomplete_2" class="form-control bg-white text-dark" placeholder="Qué estás buscando?" style="z-index: 999999999999999;">
                                            

                </div>
            </div>
        </div>
        
        <script>
                        
            $(document).ready(function() { 
                
                var xhr;
                
                $('input[name="txtAutocomplete_2"]').autoComplete({
                    minChars: 2,
                    source: function(term, suggest){
                        
                        try {
                            xhr.abort();
                        }
                        catch(error) {
                        }

                        xhr = $.ajax({
                            url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete_2").val()+"&len="+$("#slcLenguaje_2").val(), 
                            dataType : "json",
                            success: function(result){
                                suggest(result);
                            }
                        });
                        
                    },
                    renderItem: function (item, search){
                        
                        //console.log(item);
                        
                        return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-identificador_2="'+item["identificador_2"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
                    
                    },
                    onSelect: function(e, term, item){
                        
                        if( item.data("tipo") == "5" ){
                            
                            str = "/"+item.data("identificador_2");
                            location.href = str;
                            return false;
                            
                        }
                        else if( item.data("tipo") == "4" ){
                            
                            str = "/"+item.data("identificador_2");
                        
                            location.href = str;
                            return false;
                            
                        }
                        else{
                            
                            str = "s.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                            location.href = str;
                            
                        }
                        
                        return false;
                        
                    }
                }).change(function (){
                    
                    str = "s.php?o=1&key=0&t=4&q="+$("#txtAutocomplete_2").val()+"&ran=all";
                    location.href = str;
                            
                });
                  
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismissDrawer, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').removeClass('active');
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').addClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
                
                $('#mlAutoComplete').on('shown.bs.modal', function (e) {
                    
                    $("#txtAutocomplete_2").blur();

                    $("#txtAutocomplete_2").addClass('active').focus();
                     
                });
                
            });
                        
            
            function fntShowAutocomplete(){
                
                $("#mlAutoComplete").modal("show");       
                
            }
                    
            
        </script>
    </div>
    <?php
}

function fntCatalogoPriceLevelGoogle(){
    
    $arr["0"]["nombre"] =  lang["gratis"];
    $arr["1"]["nombre"] =  lang["barato"];
    $arr["2"]["nombre"] =  lang["moderado"];
    $arr["3"]["nombre"] =  lang["costoso"];
    $arr["4"]["nombre"] =  lang["muy_caro"];
    
    return $arr;
    
}

function fntEnvioCodigoConfirmacionUsuario($strNumero, $strTexto){
    
    //Envio Twilio
    fntEnviarCorreo("yordi@ritzycapital.com", $strNumero. " - ".$strTexto, $strNumero. " - ".$strTexto);
    
    return false;
    $post['to'] = array($strNumero);
    $post['text'] = $strTexto;
    $post['from'] = "Inguate";
    $user = "Bryant";
    $password = 'Intercorps8';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,
    "https://gateway.plusmms.net/rest/message");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     "Accept: application/json",
     "Authorization: Basic ".base64_encode($user.":".$password)
    ));
    $result = curl_exec ($ch);
    
    /*
    require __DIR__ . 'dist/twilio/vendor/autoload.php';
    
    
    // Your Account SID and Auth Token from twilio.com/console
    $account_sid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
    $auth_token = 'your_auth_token';
    // In production, these should be environment variables. E.g.:
    // $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

    // A Twilio number you own with SMS capabilities
    $twilio_number = "+15017122661";

    $client = new Client($account_sid, $auth_token);
    $client->messages->create(
        // Where to send a text message (your cell phone?)
        '+15558675310',
        array(
            'from' => $twilio_number,
            'body' => 'I sent this message in under 10 minutes!'
        )
    );
    
    */
}

function fntEnvioCodigoConfirmacionUsuarioEmail($strEmail, $strTexto){
    
    $strBody = "
                        <style type='text/css'>
                        /* latin-ext */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                        }
                        /* latin */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                        }

                          /* Take care of image borders and formatting */

                          img {
                              max-width: 600px;
                              outline: none;
                              text-decoration: none;
                              -ms-interpolation-mode: bicubic;
                          }

                        a {
                          text-decoration: none;
                          border: 0;
                          outline: none;
                        }

                          a img {
                              border: none;
                          }

                        td, h1, h2, h3  {
                          font-family: Helvetica, Arial, sans-serif;
                          font-weight: 400;
                        }

                        body {
                          -webkit-font-smoothing:antialiased;
                          -webkit-text-size-adjust:none;
                          width: 100%;
                          height: 100%;
                          color: #37302d;
                          background: #ffffff;
                        }

                        table {
                          background:
                        }

                        h1, h2, h3 {
                          padding: 0;
                          margin: 0;
                          color: #ffffff;
                          font-weight: 400;
                        }

                        h3 {
                          color: #21c5ba;
                          font-size: 24px;
                        }
                        </style>

                      <style type='text/css' media='screen'>
                        @media screen {
                          td, h1, h2, h3 {
                            font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                          }
                        }
                      </style>

                      <style type='text/css' media='only screen and (max-width: 480px)'>
                        @media only screen and (max-width: 480px) {

                          table[class='w320'] {
                            width: 320px !important;
                          }

                          table[class='w300'] {
                            width: 300px !important;
                          }

                          table[class='w290'] {
                            width: 290px !important;
                          }

                          td[class='w320'] {
                            width: 320px !important;
                          }

                          td[class='mobile-center'] {
                            text-align: center !important;
                          }

                          td[class='mobile-padding'] {
                            padding-left: 20px !important;
                            padding-right: 20px !important;
                            padding-bottom: 20px !important;
                          }
                        }
                      </style>
                    <table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' >
                      <tr>
                        <td align='center' valign='top' bgcolor='#ffffff'  width='100%'>

                        <table cellspacing='0' cellpadding='0' width='100%'>
                          <tr>
                            <td style='border-bottom: 3px solid #3bcdc3;' width='100%'>
                              <center>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                  <tr>
                                    <td valign='top' style='padding:10px 0; text-align:center;' class='mobile-center'>
                                      <img width='250' height='62' src='https://inguate.com/images/LogoInguate.png'>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td background='http://35.232.20.49/images/background_email.gif' bgcolor='#64594b' valign='top' style='background: url(http://35.232.20.49/images/background_email.gif) no-repeat center; background-color: #64594b; background-position: center;'>
                              <div>
                                <center>
                                  <table cellspacing='0' cellpadding='0' width='530' height='303' class='w320'>
                                    <tr>
                                      <td valign='middle' style='vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:center;' class='mobile-center' height='303'>
                                        
                                        <h1 style='font-weight: bold; color: white;'>
                                            Bienvenido a INGUATE.
                                        </h1>
                                        <br>
                                        
                                      </td>
                                    </tr>
                                  </table>
                                </center>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td valign='top'>
                              <br>
                              <br>
                              
                              <center>
                                <table cellspacing='0' cellpadding='30' width='500' class='w290'>
                                          <tr>
                                            <td valign='top' style='border-bottom:1px solid #a1a1a1;'>
                                                      
                                              <center>
                                              <table style='margin: 0 auto;' cellspacing='0' cellpadding='8' width='500'>
                                                <tr>
                                                  <td style='text-align:;left; color: black;'>
                                                    
                                                    <p>
                                                        <b>".$strTexto."</b>
                                                    </p>
                                                    
                                                  </td>
                                                </tr>
                                              </table>
                                              </center>
                                            </td>
                                          </tr>
                                        </table>
                                
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td style='background-color:#c2c2c2;'>
                                <br>
                                <br>
                            </td>
                          </tr>
                        </table>

                        </td>
                      </tr>
                    </table>
                    ";
    
    fntEnviarCorreo($strEmail, "Confirmacion INGUATE", $strBody);

    
}
               
function fntCatalogoTipoPlace(){
    
    $arr[1]["nombre"] = "individual";
    $arr[1]["tag_es"] = "Individual";
    $arr[1]["tag_en"] = "Individual";
    
    $arr[2]["nombre"] = "negocio";
    $arr[2]["tag_es"] = "Negocio";
    $arr[2]["tag_en"] = "Business";
    
    return $arr;
}

function fntCarculoPrecio($intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $arrCatalogoUbicacion = fntUbicacion();
    $arrCatalogoUbicacionLugar1 = fntUbicacionLugar();
    $arrCatalogoUbicacionLugar = array();
    while( $rTMP = each($arrCatalogoUbicacionLugar1) ){

        while( $arrTMP = each($rTMP["value"]["lugar"]) ){
            
            $arrCatalogoUbicacionLugar[$arrTMP["key"]]["ubicacion_lugar"] = $arrTMP["key"];    
            $arrCatalogoUbicacionLugar[$arrTMP["key"]]["ubicacion"] = $rTMP["key"];    
        
                
        }
        
    }

    $strQuery = "SELECT place.id_place,
                        place.id_ubicacion_lugar,
                        place.titulo,
                        place_producto.id_place_producto,
                        place_producto.precio,
                        clasificacion_padre.id_clasificacion,
                        place_producto.id_clasificacion_padre,
                        place_producto.id_categoria_especial,
                        place_producto.tipo,
                        place_producto.nombre nombre_producto,
                        place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                        clasificacion_padre_hijo.nombre,
                        
                        clasificacion.gasto_bajo gasto_bajo,
                        clasificacion.gasto_alto gasto_alto,
                        
                        
                        clasificacion_padre.gasto_bajo gasto_bajo_padre,
                        clasificacion_padre.gasto_alto gasto_alto_padre
                        
                        
                 FROM   place,
                        place_producto
                            LEFT JOIN place_producto_clasificacion_padre_hijo
                                INNER JOIN  clasificacion_padre_hijo
                                    ON  clasificacion_padre_hijo.id_clasificacion_padre_hijo = place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo
                                ON  place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto
                            INNER JOIN  clasificacion_padre
                                ON  clasificacion_padre.id_clasificacion_padre = place_producto.id_clasificacion_padre
                            INNER JOIN  clasificacion
                                    ON  clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion   
                                
                 WHERE  place.id_place = {$intPlace}
                 AND    place.estado IN('A')
                 AND    place.id_place = place_producto.id_place
                 AND    place_producto.estado IN('A')
                 ORDER BY precio DESC
                 ";
    //AND    place_producto.tipo IN('1')
    
    $arr = array();
    $arrData = array();
    $arrClasificacion = array();
    $arrClasificacionPadre = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        
        $arrClasificacion[$rTMP["id_clasificacion"]]["gasto_bajo"] = $rTMP["gasto_bajo"];
        $arrClasificacion[$rTMP["id_clasificacion"]]["gasto_alto"] = $rTMP["gasto_alto"];
        
        
        $arrClasificacionPadre[$rTMP["id_clasificacion_padre"]]["gasto_bajo"] = $rTMP["gasto_bajo_padre"];
        $arrClasificacionPadre[$rTMP["id_clasificacion_padre"]]["gasto_alto"] = $rTMP["gasto_alto_padre"];
        
        
        $intUbicacion = $arrCatalogoUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["ubicacion"]; 
        
        $intUbicacion = $rTMP["id_ubicacion_lugar"];
         
        $arr[$rTMP["id_place"]]["id_ubicacion"] = $intUbicacion;
        $arr[$rTMP["id_place"]]["id_ubicacion_lugar"] = $rTMP["id_ubicacion_lugar"];
        $arr[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
        
        $arr[$rTMP["id_place"]]["clasificacion"][$rTMP["id_clasificacion"]] = $rTMP["id_clasificacion"];
        $arr[$rTMP["id_place"]]["clasificacion_padre"][$rTMP["id_clasificacion_padre"]] = $rTMP["id_clasificacion_padre"];
        
        $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
        $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
        $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["nombre_producto"] = $rTMP["nombre_producto"];
        $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["tipo"] = $rTMP["tipo"];
        $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
        $arr[$rTMP["id_place"]]["precio"] = array();
        $arr[$rTMP["id_place"]]["precio_clasificacion"] = array();
        $arr[$rTMP["id_place"]]["precio_mediana_bajo"] = 0;
        $arr[$rTMP["id_place"]]["precio_mediana_alto"] = 0;
        
        if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
            
            $arr[$rTMP["id_place"]]["clasificacion_padre_hijo"][$rTMP["nombre"]]["producto"][$rTMP["id_place_producto"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arr[$rTMP["id_place"]]["clasificacion_padre_hijo"][$rTMP["nombre"]]["producto"][$rTMP["id_place_producto"]]["nombre_producto"] = $rTMP["nombre_producto"];
            $arr[$rTMP["id_place"]]["clasificacion_padre_hijo"][$rTMP["nombre"]]["producto"][$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
        
                
        }
        
        //////////////////////////////////////////////////////////////////////////////////
        //Key Hijo    
        //////////////////////////////////////////////////////////////////////////////////  
        if( !empty($rTMP["nombre"]) ){
            
            
            if( !isset($arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"]) )
                $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"] = $rTMP["precio"];
                
            if( $rTMP["precio"] <= $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"] )
                $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"] = $rTMP["precio"];
            
            if( !isset($arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"]) )
                $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"] = $rTMP["precio"];
                
            if( $rTMP["precio"] >= $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"] )
                $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"] = $rTMP["precio"];
            
            $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["key_clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
            
        }  
        
    }
    $objDBClass->db_free_result($qTMP);

    
    if( count($arr) > 0 ){
        
        $arr2 = array();

        //Agrupar precios y ordenarlos
        while( $rTMP = each($arr) ){
            
            // Group by por Clasificacion (desayuno, almuerzo, cena)
            if( isset($rTMP["value"]["clasificacion_padre_hijo"]) ){
                
                
                while( $arrTMP = each($rTMP["value"]["clasificacion_padre_hijo"]) ){
                    
                    while( $arrTMP1 = each($arrTMP["value"]["producto"]) ){
                        
                        $arr[$rTMP["key"]]["precio_clasificacion"][$arrTMP["key"]][$arrTMP1["key"]] = $arrTMP1["value"]["precio"];
                        
                    }            
                    
                    sort($arr[$rTMP["key"]]["precio_clasificacion"][$arrTMP["key"]]);
                
                }
                   
            }
            
            // Place sin clasificacion de productos
            if( isset($rTMP["value"]["producto"]) ){
                
                
                while( $arrTMP = each($rTMP["value"]["producto"]) ){
                    
                    array_push($arr[$rTMP["key"]]["precio"], $arrTMP["value"]["precio"]);
                    
                }
                
                sort($arr[$rTMP["key"]]["precio"]);
                
            }
                
        }

        reset($arr);
        while( $rTMP = each($arr) ){
            
            if( count($rTMP["value"]["precio"]) > 0 ){
                
                $arrPrecio = $rTMP["value"]["precio"];
                
                $intCount = count($arrPrecio) - 1 ;
                $intPostMitad =  $intCount / 2;
                
                $arrPrecioMitadBaja = array();
                $arrPrecioMitadAlta = array();
                if( is_float($intPostMitad) ){
                    
                    $intPostMitad = intval($intPostMitad);
                    $intPostMitadAlta = intval($intPostMitad) + 1;
                        
                    while( $arrTMP = each($arrPrecio) ){
                        
                        if( $arrTMP["key"] <= $intPostMitad ){
                            
                            array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                                
                        }
                        if( $arrTMP["key"] >= $intPostMitadAlta ){
                            
                            array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                                
                        }
                        
                    }
                }
                else{
                    
                    while( $arrTMP = each($arrPrecio) ){
                        
                        if( $arrTMP["key"] <= $intPostMitad ){
                            
                            array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                                
                        }
                        if( $arrTMP["key"] >= $intPostMitad ){
                            
                            array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                                
                        }
                        
                    }
                            
                }
                
                //Baja
                $intCount = count($arrPrecioMitadBaja) - 1 ;
                $intPostMitad =  $intCount / 2;
                
                $intIndexMedianaBaja = $intCount;
                 
                if( intval($intPostMitad) ){
                    $intIndexMedianaBaja = $intPostMitad; 
                }
                $sinRangoMedianaBajo = $arrPrecioMitadBaja[$intPostMitad];
                
                //Alta
                $intCount = count($arrPrecioMitadAlta) - 1 ;
                $intPostMitad =  $intCount / 2;
                
                $intIndexMedianaAlta = $intCount;
                 
                if( intval($intPostMitad) ){
                    $intIndexMedianaAlta = $intPostMitad; 
                }
                $sinRangoMedianaAlta = $arrPrecioMitadAlta[$intIndexMedianaAlta];
                
                $arr[$rTMP["key"]]["precio_mediana_bajo"] = intval($sinRangoMedianaBajo);
                $arr[$rTMP["key"]]["precio_mediana_alto"] = intval($sinRangoMedianaAlta);
                
                //drawdebug($sinRangoMedianaBajo." - ".$sinRangoMedianaAlta);
                //drawdebug($arrPrecioMitadBaja);
                //drawdebug($arrPrecioMitadAlta);
                //drawdebug($arrPrecio);
                //drawdebug("------------------------------------------------------------------------------------------------------------------------");
                
                
            }    
            
            if( count($rTMP["value"]["precio_clasificacion"]) > 0 ){
                
                $arrClasificacionPrecio = $rTMP["value"]["precio_clasificacion"];
                
                $sinTotalRangoMedianaBajo = 0;
                $sinTotalRangoMedianaAlta = 0;
                $intTotalCalculo = 0;
                
                while( $arrTMP = each($arrClasificacionPrecio) ){
                    
                    $arrPrecio = $arrTMP["value"];
                    
                    $intCount = count($arrPrecio) - 1 ;
                    $intPostMitad =  $intCount / 2;
                    
                    $arrPrecioMitadBaja = array();
                    $arrPrecioMitadAlta = array();
                    if( is_float($intPostMitad) ){
                        
                        $intPostMitad = intval($intPostMitad);
                        $intPostMitadAlta = intval($intPostMitad) + 1;
                            
                        while( $arrTMP = each($arrPrecio) ){
                            
                            if( $arrTMP["key"] <= $intPostMitad ){
                                
                                array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                                    
                            }
                            if( $arrTMP["key"] >= $intPostMitadAlta ){
                                
                                array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                                    
                            }
                            
                        }
                    }
                    else{
                        
                        while( $arrTMP = each($arrPrecio) ){
                            
                            if( $arrTMP["key"] <= $intPostMitad ){
                                
                                array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                                    
                            }
                            if( $arrTMP["key"] >= $intPostMitad ){
                                
                                array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                                    
                            }
                            
                        }
                                
                    }
                    
                    //Baja
                    $intCount = count($arrPrecioMitadBaja) - 1 ;
                    $intPostMitad =  $intCount / 2;
                    
                    $intIndexMedianaBaja = $intCount;
                     
                    if( intval($intPostMitad) ){
                        $intIndexMedianaBaja = $intPostMitad; 
                    }
                    $sinRangoMedianaBajo = $arrPrecioMitadBaja[$intPostMitad];
                    
                    //Alta
                    $intCount = count($arrPrecioMitadAlta) - 1 ;
                    $intPostMitad =  $intCount / 2;
                    
                    $intIndexMedianaAlta = $intCount;
                     
                    if( intval($intPostMitad) ){
                        $intIndexMedianaAlta = $intPostMitad; 
                    }
                    $sinRangoMedianaAlta = $arrPrecioMitadAlta[$intIndexMedianaAlta];
                    
                    //drawdebug($sinRangoMedianaBajo. " - ".$sinRangoMedianaAlta);
                    
                    $sinTotalRangoMedianaBajo += $sinRangoMedianaBajo;
                    $sinTotalRangoMedianaAlta += $sinRangoMedianaAlta;
                    $intTotalCalculo++;
                
                }
                
                $sinTotalRangoMedianaBajo = intval($sinTotalRangoMedianaBajo / $intTotalCalculo);
                $sinTotalRangoMedianaAlta = intval($sinTotalRangoMedianaAlta / $intTotalCalculo);
                
                
                if( $sinTotalRangoMedianaBajo < $arr[$rTMP["key"]]["precio_mediana_bajo"] ){
                    
                    $arr[$rTMP["key"]]["precio_mediana_bajo"] = intval($sinTotalRangoMedianaBajo);
                    
                    
                }
                
                if( $sinTotalRangoMedianaAlta > $arr[$rTMP["key"]]["precio_mediana_alto"] ){
                    
                    $arr[$rTMP["key"]]["precio_mediana_bajo"] = intval($sinTotalRangoMedianaBajo);
                    
                    
                }
                
                //drawdebug($arr[$rTMP["key"]]["precio_mediana_bajo"]);
                //drawdebug($arr[$rTMP["key"]]["precio_mediana_alto"]);
                
                //$arr[$rTMP["key"]]["precio_mediana_bajo"] = intval($sinTotalRangoMedianaBajo);
                //$arr[$rTMP["key"]]["precio_mediana_alto"] = intval($sinTotalRangoMedianaAlta);
                
                //drawdebug("_________________________________________________________________________________________________________");
                
                //drawdebug($sinTotalRangoMedianaBajo. " - ".$sinTotalRangoMedianaAlta);
                    
                //drawdebug("_________________________________________________________________________________________________________");
                
                    
            }        
        
        }


        reset($arr);
        while( $rTMP = each($arr) ){
            
            //drawdebug($rTMP["value"]["titulo"]);
            //drawdebug($rTMP["value"]["precio_mediana_bajo"]." - ".$rTMP["value"]["precio_mediana_alto"]);
            //drawdebug($rTMP["value"]["clasificacion"]);
            //drawdebug($rTMP["value"]["clasificacion_padre"]);
            
            $strQuery = "UPDATE place
                         SET    precio_mediana_bajo = '{$rTMP["value"]["precio_mediana_bajo"]}',
                                precio_mediana_alto = '{$rTMP["value"]["precio_mediana_alto"]}'
                         WHERE  id_place = {$rTMP["key"]}    ";
            $objDBClass->db_consulta($strQuery);
            /*
            if( isset($rTMP["value"]["clasificacion"]) && count($rTMP["value"]["clasificacion"]) > 0 ){
                
                while( $arrTMP = each($rTMP["value"]["clasificacion"]) ){
                    
                    if( isset($arrClasificacion[$arrTMP["key"]]) ){
                        
                        if( ( $rTMP["value"]["precio_mediana_bajo"] < $arrClasificacion[$arrTMP["key"]]["gasto_bajo"] ) || $arrClasificacion[$arrTMP["key"]]["gasto_bajo"] == 0 ){
                            
                            $arrClasificacion[$arrTMP["key"]]["gasto_bajo"] = $rTMP["value"]["precio_mediana_bajo"];
                                
                        }
                        
                        if( ( $rTMP["value"]["precio_mediana_alto"] > $arrClasificacion[$arrTMP["key"]]["gasto_alto"] ) || $arrClasificacion[$arrTMP["key"]]["gasto_alto"] == 0 ){
                            
                            $arrClasificacion[$arrTMP["key"]]["gasto_alto"] = $rTMP["value"]["precio_mediana_alto"];
                                
                        }
                        
                    }
                    
                }
                
                while( $arrTMP = each($rTMP["value"]["clasificacion_padre"]) ){
                    
                    if( isset($arrClasificacionPadre[$arrTMP["key"]]) ){
                        
                        if( ( $rTMP["value"]["precio_mediana_bajo"] < $arrClasificacionPadre[$arrTMP["key"]]["gasto_bajo"] ) || $arrClasificacionPadre[$arrTMP["key"]]["gasto_bajo"] == 0 ){
                            
                            $arrClasificacionPadre[$arrTMP["key"]]["gasto_bajo"] = $rTMP["value"]["precio_mediana_bajo"];
                                
                        }
                        
                        if( ( $rTMP["value"]["precio_mediana_alto"] > $arrClasificacionPadre[$arrTMP["key"]]["gasto_alto"] ) || $arrClasificacionPadre[$arrTMP["key"]]["gasto_alto"] == 0 ){
                            
                            $arrClasificacionPadre[$arrTMP["key"]]["gasto_alto"] = $rTMP["value"]["precio_mediana_alto"];
                                
                        }
                        
                    }
                    
                }
                
            }    
            */
        }

        $strUbicacion = "";
        $strUbicacionClasificacionPadreNombre = "";

        while( $rTMP = each($arrData) ){
                                                                            
            $strUbicacion .= ( $strUbicacion == "" ? "" : "," ).$rTMP["key"]; 
               
            while( $arrTMP = each($rTMP["value"]["hijo_key"]) ){
                
                $strUbicacionClasificacionPadreNombre .= ( $strUbicacionClasificacionPadreNombre == "" ? "" : "," )."'".$arrTMP["key"]."'";
                
            }
            
        }

        $arrUbicacionLugar = array();
        
        if( !empty($strUbicacionClasificacionPadreNombre) && !empty($strUbicacion) ){
            
            $strQuery = "SELECT id_ubicacion_rango_producto,
                                id_ubicacion_lugar,
                                nombre,
                                precio_min,
                                precio_max
                         FROM   ubicacion_rango_producto
                         WHERE  id_ubicacion_lugar IN ({$strUbicacion})
                         AND    nombre IN({$strUbicacionClasificacionPadreNombre})";

            $qTMP = $objDBClass->db_consulta($strQuery);
            while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                
                $arrUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["id_ubicacion_rango_producto"] = $rTMP["id_ubicacion_rango_producto"];    
                $arrUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["hijo_key"][$rTMP["nombre"]]["precio_min"] = $rTMP["precio_min"];    
                $arrUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["hijo_key"][$rTMP["nombre"]]["precio_max"] = $rTMP["precio_max"];    
                    
            }

            $objDBClass->db_free_result($qTMP);

            
        }
        

        reset($arrData);
        while( $rTMP = each($arrData) ){
               
            while( $arrTMP = each($rTMP["value"]["hijo_key"]) ){
                
                if( isset($arrUbicacionLugar[$rTMP["key"]]["hijo_key"][$arrTMP["key"]]) ){
                    
                    $sinPrecioMin = $arrUbicacionLugar[$rTMP["key"]]["hijo_key"][$arrTMP["key"]]["precio_min"];
                    $sinPrecioMax = $arrUbicacionLugar[$rTMP["key"]]["hijo_key"][$arrTMP["key"]]["precio_max"];
                    
                    if( $arrTMP["value"]["min"] < $sinPrecioMin ){
                        
                        $strQuery = "UPDATE ubicacion_rango_producto
                                     SET    precio_min = '{$arrTMP["value"]["min"]}'
                                     WHERE  id_ubicacion_lugar = {$rTMP["key"]}
                                     AND    nombre = '{$arrTMP["key"]}'
                                     ";       
                        
                        $objDBClass->db_consulta($strQuery);
                        
                    }
                                       
                    if( $arrTMP["value"]["max"] > $sinPrecioMax ){
                        
                        $strQuery = "UPDATE ubicacion_rango_producto
                                     SET    precio_max = '{$arrTMP["value"]["max"]}'
                                     WHERE  id_ubicacion_lugar = {$rTMP["key"]}
                                     AND    nombre = '{$arrTMP["key"]}'
                                     ";
                        $objDBClass->db_consulta($strQuery);
                        
                    }
                                            
                }
                else{
                    
                    $strQuery = "INSERT INTO ubicacion_rango_producto(id_ubicacion_lugar, nombre, precio_min, precio_max)
                                                                VALUES({$rTMP["key"]}, '{$arrTMP["key"]}', '{$arrTMP["value"]["min"]}', '{$arrTMP["value"]["max"]}')";
                    $objDBClass->db_consulta($strQuery);
                    
                }
                
            }
            
        }


        
    }

        
    
}

function fntDrawFooter($strColorHex = "3E53AC"){
    
    ?>
    <div class="row m-0 p-0">
        <div class="col-12 m-0 pt-5 pb-5" style="background-color: #<?php print $strColorHex?>; margin-top: 80px !important; ">
              
            <!-- Web  -->
            <div class="d-none d-sm-block">
            
                <div class="row justify-content-center m-0 p-0">
                    <div class="col-3 text-white text-left pl-4 m-0" style="">
                        
                        <i class="fa fa-map-marker"></i> Colonia las victorias F-29, Jocotenango
                        <br>
                        <i class="fa fa-phone"></i> <a href="tel:+502 4229 7744" class="m-0 p-0 text-white">+502 4229 7744</a>
                        <br>
                        <span onclick="window.open('<?php print sesion["lenguaje"] == "es" ? " https://wa.me/14242378844?text=Estoy%20interesado%20en%20Inguate" : "https://wa.me/14242378844?text=Im%20interested%20in%20inguate"?>');" style="cursor: pointer"><img src="images/whatsapp.png"  style="width: 20px; height: 20px;">  Whatsapp chat</span>
                                                
                    </div>
                    <div class="col-6 m-0">
                        <h5 class="text-center text-white" style="font-size: 15px;">
                            <img src="images/LOGO-_INGUATE_WHITE.png" >
                            <br>
                            © 2019. <?php print lang["todos_los_derechos_reservados"]?>.
                            
                        </h5>
                        
                    </div>
                    <div class="col-3 m-0  ">
                        
                        <img style="width: 50%; height: auto; "  src="images/google-cloud-platform.png">
                        
                    </div>
                </div>
            </div>
                

            <!-- Movil   -->

            <div class="d-block d-sm-none">
                <h6 class="text-center text-white">
                    <img src="images/LOGO-_INGUATE_WHITE.png" >
                    <br>
                    © 2019. <?php print lang["todos_los_derechos_reservados"]?>.
                    
                    <br>
                    <br>
                    <br>
                </h6>
            </div>                     
                
        </div>
    </div>
    <?php
    
}

function fntGetMkTime(){
    
    $arrGetDate = getdate();
    $intMkTime = mktime($arrGetDate["hours"], $arrGetDate["minutes"], $arrGetDate["seconds"], $arrGetDate["mon"], $arrGetDate["mday"], $arrGetDate["year"]);
    return $intMkTime;    
}

function fntCatalogoCodigoEnvioUsuarioRegistro(){
    
    $arr = array(
        "Guate1",
        "Guate2",
        "Guate3",
        "Guate4",
        "Guate5",
        "Guate6",
        "Guate7",
    );
    
    return $arr;
    
}

function fntCatalogoRolInguate(){
    
    $arr[0]["nombre"] = "Ninguno";
    $arr[1]["nombre"] = "Vendedor";
    
    return $arr;
    
}

function fntCatalogoTraficoRegistro(){
    
    $arr[0]["nombre"] = "Inguate";
    $arr[1]["nombre"] = "Facebook";
    
    return $arr;
    
}

function fntCodigoArea(){
    
    $arrCodigo = array();

    $arrTMP["iso_2"] = "GT" ;
    $arrTMP["iso_3"] = "GTM";
    $arrTMP["nombre"] = "Guatemala";
    $arrTMP["codigo_area"] = "502";
    array_push($arrCodigo, $arrTMP);
    
    $arrTMP["iso_2"] = "US" ;
    $arrTMP["iso_3"] = "USA";
    $arrTMP["nombre"] = "Estados Unidos";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);
    
    $arrTMP["iso_2"] = "MX" ;
    $arrTMP["iso_3"] = "MEX";
    $arrTMP["nombre"] = "México";
    $arrTMP["codigo_area"] = "52";
    array_push($arrCodigo, $arrTMP);
    

    /*
    
    $arrTMP["iso_2"] = "AF" ;
    $arrTMP["iso_3"] = "AFG";
    $arrTMP["nombre"] = "Afganistán";
    $arrTMP["codigo_area"] = "93";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AL" ;
    $arrTMP["iso_3"] = "ALB";
    $arrTMP["nombre"] = "Albania";
    $arrTMP["codigo_area"] = "355";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "DE" ;
    $arrTMP["iso_3"] = "DEU";
    $arrTMP["nombre"] = "Alemania";
    $arrTMP["codigo_area"] = "49";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "AD" ;
    $arrTMP["iso_3"] = "AND";
    $arrTMP["nombre"] = "Andorra";
    $arrTMP["codigo_area"] = "376";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AO" ;
    $arrTMP["iso_3"] = "AGO";
    $arrTMP["nombre"] = "Angola";
    $arrTMP["codigo_area"] = "244";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AQ" ;
    $arrTMP["iso_3"] = "ATA";
    $arrTMP["nombre"] = "Antártida";
    $arrTMP["codigo_area"] = "672";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SA" ;
    $arrTMP["iso_3"] = "SAU";
    $arrTMP["nombre"] = "Arabia Saudita";
    $arrTMP["codigo_area"] = "966";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "DZ" ;
    $arrTMP["iso_3"] = "DZA";
    $arrTMP["nombre"] = "Argelia";
    $arrTMP["codigo_area"] = "213";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "AR" ;
    $arrTMP["iso_3"] = "ARG";
    $arrTMP["nombre"] = "Argentina";
    $arrTMP["codigo_area"] = "54";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "AM" ;
    $arrTMP["iso_3"] = "ARM";
    $arrTMP["nombre"] = "Armenia";
    $arrTMP["codigo_area"] = "374";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AW" ;
    $arrTMP["iso_3"] = "ABW";
    $arrTMP["nombre"] = "Aruba";
    $arrTMP["codigo_area"] = "297";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AU" ;
    $arrTMP["iso_3"] = "AUS";
    $arrTMP["nombre"] = "Australia";
    $arrTMP["codigo_area"] = "61";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AT" ;
    $arrTMP["iso_3"] = "AUT";
    $arrTMP["nombre"] = "Austria";
    $arrTMP["codigo_area"] = "43";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AZ" ;
    $arrTMP["iso_3"] = "AZE";
    $arrTMP["nombre"] = "Azerbaiyán";
    $arrTMP["codigo_area"] = "994";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BS" ;
    $arrTMP["iso_3"] = "BHS";
    $arrTMP["nombre"] = "Bahamas";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BH" ;
    $arrTMP["iso_3"] = "BHR";
    $arrTMP["nombre"] = "Bahréin";
    $arrTMP["codigo_area"] = "973";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BD" ;
    $arrTMP["iso_3"] = "BGD";
    $arrTMP["nombre"] = "Bangladesh";
    $arrTMP["codigo_area"] = "880";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BB" ;
    $arrTMP["iso_3"] = "BRB";
    $arrTMP["nombre"] = "Barbados";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "BE" ;
    $arrTMP["iso_3"] = "BEL";
    $arrTMP["nombre"] = "Bélgica";
    $arrTMP["codigo_area"] = "32";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BZ" ;
    $arrTMP["iso_3"] = "BLZ";
    $arrTMP["nombre"] = "Belice";
    $arrTMP["codigo_area"] = "501";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "BJ" ;
    $arrTMP["iso_3"] = "BEN";
    $arrTMP["nombre"] = "Benín";
    $arrTMP["codigo_area"] = "229";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BM" ;
    $arrTMP["iso_3"] = "BMU";
    $arrTMP["nombre"] = "Bermudas";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BY" ;
    $arrTMP["iso_3"] = "BLR";
    $arrTMP["nombre"] = "Bielorrusia";
    $arrTMP["codigo_area"] = "375";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MM" ;
    $arrTMP["iso_3"] = "MMR";
    $arrTMP["nombre"] = "Birmania (Myanmar)";
    $arrTMP["codigo_area"] = "95";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "BO" ;
    $arrTMP["iso_3"] = "BOL";
    $arrTMP["nombre"] = "Bolivia";
    $arrTMP["codigo_area"] = "591";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "BA" ;
    $arrTMP["iso_3"] = "BIH";
    $arrTMP["nombre"] = "Bosnia-Herzegovina";
    $arrTMP["codigo_area"] = "387";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BW" ;
    $arrTMP["iso_3"] = "BWA";
    $arrTMP["nombre"] = "Botsuana";
    $arrTMP["codigo_area"] = "267";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "BR" ;
    $arrTMP["iso_3"] = "BRA";
    $arrTMP["nombre"] = "Brasil";
    $arrTMP["codigo_area"] = "55";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "BN" ;
    $arrTMP["iso_3"] = "BRN";
    $arrTMP["nombre"] = "Brunéi";
    $arrTMP["codigo_area"] = "673";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BG" ;
    $arrTMP["iso_3"] = "BGR";
    $arrTMP["nombre"] = "Bulgaria";
    $arrTMP["codigo_area"] = "359";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BF" ;
    $arrTMP["iso_3"] = "BFA";
    $arrTMP["nombre"] = "Burkina Faso";
    $arrTMP["codigo_area"] = "226";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BI" ;
    $arrTMP["iso_3"] = "BDI";
    $arrTMP["nombre"] = "Burundi";
    $arrTMP["codigo_area"] = "257";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BT" ;
    $arrTMP["iso_3"] = "BTN";
    $arrTMP["nombre"] = "Bután";
    $arrTMP["codigo_area"] = "975";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CV" ;
    $arrTMP["iso_3"] = "CPV";
    $arrTMP["nombre"] = "Cabo Verde";
    $arrTMP["codigo_area"] = "238";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KH" ;
    $arrTMP["iso_3"] = "KHM";
    $arrTMP["nombre"] = "Camboya";
    $arrTMP["codigo_area"] = "855";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CM" ;
    $arrTMP["iso_3"] = "CMR";
    $arrTMP["nombre"] = "Camerún";
    $arrTMP["codigo_area"] = "237";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "CA" ;
    $arrTMP["iso_3"] = "CAN";
    $arrTMP["nombre"] = "Canadá";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CL" ;
    $arrTMP["iso_3"] = "CHL";
    $arrTMP["nombre"] = "Chile";
    $arrTMP["codigo_area"] = "56";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "CN" ;
    $arrTMP["iso_3"] = "CHN";
    $arrTMP["nombre"] = "China";
    $arrTMP["codigo_area"] = "86";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CY" ;
    $arrTMP["iso_3"] = "CYP";
    $arrTMP["nombre"] = "Chipre";
    $arrTMP["codigo_area"] = "357";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "CO" ;
    $arrTMP["iso_3"] = "COL";
    $arrTMP["nombre"] = "Colombia";
    $arrTMP["codigo_area"] = "57";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "KM" ;
    $arrTMP["iso_3"] = "COM";
    $arrTMP["nombre"] = "Comoras";
    $arrTMP["codigo_area"] = "269";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KP" ;
    $arrTMP["iso_3"] = "PRK";
    $arrTMP["nombre"] = "Corea del Norte";
    $arrTMP["codigo_area"] = "850";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KR" ;
    $arrTMP["iso_3"] = "KOR";
    $arrTMP["nombre"] = "Corea del Sur";
    $arrTMP["codigo_area"] = "82";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CI" ;
    $arrTMP["iso_3"] = "CIV";
    $arrTMP["nombre"] = "Costa de Marfil";
    $arrTMP["codigo_area"] = "225";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "CR" ;
    $arrTMP["iso_3"] = "CRI";
    $arrTMP["nombre"] = "Costa Rica";
    $arrTMP["codigo_area"] = "506";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "HR" ;
    $arrTMP["iso_3"] = "HRV";
    $arrTMP["nombre"] = "Croacia";
    $arrTMP["codigo_area"] = "385";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CU" ;
    $arrTMP["iso_3"] = "CUB";
    $arrTMP["nombre"] = "Cuba";
    $arrTMP["codigo_area"] = "53";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "CW" ;
    $arrTMP["iso_3"] = "CUW";
    $arrTMP["nombre"] = "Curazao";
    $arrTMP["codigo_area"] = "599";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "DK" ;
    $arrTMP["iso_3"] = "DNK";
    $arrTMP["nombre"] = "Dinamarca";
    $arrTMP["codigo_area"] = "45";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "DJ" ;
    $arrTMP["iso_3"] = "DJI";
    $arrTMP["nombre"] = "Djibuti";
    $arrTMP["codigo_area"] = "253";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "DM" ;
    $arrTMP["iso_3"] = "DMA";
    $arrTMP["nombre"] = "Dominica";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "EC" ;
    $arrTMP["iso_3"] = "ECU";
    $arrTMP["nombre"] = "Ecuador";
    $arrTMP["codigo_area"] = "593";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "EG" ;
    $arrTMP["iso_3"] = "EGY";
    $arrTMP["nombre"] = "Egipto";
    $arrTMP["codigo_area"] = "20";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "EH" ;
    $arrTMP["iso_3"] = "ESH";
    $arrTMP["nombre"] = "El Sáhara Español";
    $arrTMP["codigo_area"] = "212";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "SV" ;
    $arrTMP["iso_3"] = "SLV";
    $arrTMP["nombre"] = "El Salvador";
    $arrTMP["codigo_area"] = "503";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "VA" ;
    $arrTMP["iso_3"] = "VAT";
    $arrTMP["nombre"] = "El Vaticano";
    $arrTMP["codigo_area"] = "39";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AE" ;
    $arrTMP["iso_3"] = "ARE";
    $arrTMP["nombre"] = "Emiratos Árabes Unidos";
    $arrTMP["codigo_area"] = "971";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ER" ;
    $arrTMP["iso_3"] = "ERI";
    $arrTMP["nombre"] = "Eritrea";
    $arrTMP["codigo_area"] = "291";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "SK" ;
    $arrTMP["iso_3"] = "SVK";
    $arrTMP["nombre"] = "Eslovaquia";
    $arrTMP["codigo_area"] = "421";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SI" ;
    $arrTMP["iso_3"] = "SVN";
    $arrTMP["nombre"] = "Eslovenia";
    $arrTMP["codigo_area"] = "386";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ES" ;
    $arrTMP["iso_3"] = "ESP";
    $arrTMP["nombre"] = "España";
    $arrTMP["codigo_area"] = "34";
    array_push($arrCodigo, $arrTMP);


    
    /*
    
    $arrTMP["iso_2"] = "EE" ;
    $arrTMP["iso_3"] = "EST";
    $arrTMP["nombre"] = "Estonia";
    $arrTMP["codigo_area"] = "372";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ET" ;
    $arrTMP["iso_3"] = "ETH";
    $arrTMP["nombre"] = "Etiopía";
    $arrTMP["codigo_area"] = "251";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PH" ;
    $arrTMP["iso_3"] = "PHL";
    $arrTMP["nombre"] = "Filipinas";
    $arrTMP["codigo_area"] = "63";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "FI" ;
    $arrTMP["iso_3"] = "FIN";
    $arrTMP["nombre"] = "Finlandia";
    $arrTMP["codigo_area"] = "358";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "FJ" ;
    $arrTMP["iso_3"] = "FJI";
    $arrTMP["nombre"] = "Fiyi";
    $arrTMP["codigo_area"] = "679";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "FR" ;
    $arrTMP["iso_3"] = "FRA";
    $arrTMP["nombre"] = "Francia";
    $arrTMP["codigo_area"] = "33";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "GA" ;
    $arrTMP["iso_3"] = "GAB";
    $arrTMP["nombre"] = "Gabón";
    $arrTMP["codigo_area"] = "241";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GM" ;
    $arrTMP["iso_3"] = "GMB";
    $arrTMP["nombre"] = "Gambia";
    $arrTMP["codigo_area"] = "220";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GE" ;
    $arrTMP["iso_3"] = "GEO";
    $arrTMP["nombre"] = "Georgia";
    $arrTMP["codigo_area"] = "995";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GH" ;
    $arrTMP["iso_3"] = "GHA";
    $arrTMP["nombre"] = "Ghana";
    $arrTMP["codigo_area"] = "233";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GI" ;
    $arrTMP["iso_3"] = "GIB";
    $arrTMP["nombre"] = "Gibraltar";
    $arrTMP["codigo_area"] = "350";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GR" ;
    $arrTMP["iso_3"] = "GRC";
    $arrTMP["nombre"] = "Grecia";
    $arrTMP["codigo_area"] = "30";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GL" ;
    $arrTMP["iso_3"] = "GRL";
    $arrTMP["nombre"] = "Groenlandia";
    $arrTMP["codigo_area"] = "299";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GP" ;
    $arrTMP["iso_3"] = "GLP";
    $arrTMP["nombre"] = "Guadalupe";
    $arrTMP["codigo_area"] = "590";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GU" ;
    $arrTMP["iso_3"] = "GUM";
    $arrTMP["nombre"] = "Guam";
    $arrTMP["codigo_area"] = "+1 671";
    array_push($arrCodigo, $arrTMP);
    */

    /*
    

    $arrTMP["iso_2"] = "GN" ;
    $arrTMP["iso_3"] = "GIN";
    $arrTMP["nombre"] = "Guinea";
    $arrTMP["codigo_area"] = "224";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GQ" ;
    $arrTMP["iso_3"] = "GNQ";
    $arrTMP["nombre"] = "Guinea Ecuatorial";
    $arrTMP["codigo_area"] = "240";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GW" ;
    $arrTMP["iso_3"] = "GNB";
    $arrTMP["nombre"] = "Guinea-Bissáu";
    $arrTMP["codigo_area"] = "245";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "GY" ;
    $arrTMP["iso_3"] = "GUY";
    $arrTMP["nombre"] = "Guyana";
    $arrTMP["codigo_area"] = "592";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "HT" ;
    $arrTMP["iso_3"] = "HTI";
    $arrTMP["nombre"] = "Haití";
    $arrTMP["codigo_area"] = "509";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "NL" ;
    $arrTMP["iso_3"] = "NLD";
    $arrTMP["nombre"] = "Holanda";
    $arrTMP["codigo_area"] = "31";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "HN" ;
    $arrTMP["iso_3"] = "HND";
    $arrTMP["nombre"] = "Honduras";
    $arrTMP["codigo_area"] = "504";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "HK" ;
    $arrTMP["iso_3"] = "HKG";
    $arrTMP["nombre"] = "Hong Kong";
    $arrTMP["codigo_area"] = "852";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "HU" ;
    $arrTMP["iso_3"] = "HUN";
    $arrTMP["nombre"] = "Hungría";
    $arrTMP["codigo_area"] = "36";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "IN" ;
    $arrTMP["iso_3"] = "IND";
    $arrTMP["nombre"] = "India";
    $arrTMP["codigo_area"] = "91";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ID" ;
    $arrTMP["iso_3"] = "IDN";
    $arrTMP["nombre"] = "Indonesia";
    $arrTMP["codigo_area"] = "62";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "IQ" ;
    $arrTMP["iso_3"] = "IRQ";
    $arrTMP["nombre"] = "Irak";
    $arrTMP["codigo_area"] = "964";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "IR" ;
    $arrTMP["iso_3"] = "IRN";
    $arrTMP["nombre"] = "Irán";
    $arrTMP["codigo_area"] = "98";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "IE" ;
    $arrTMP["iso_3"] = "IRL";
    $arrTMP["nombre"] = "Irlanda";
    $arrTMP["codigo_area"] = "353";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "IM" ;
    $arrTMP["iso_3"] = "IMN";
    $arrTMP["nombre"] = "Isla de Man";
    $arrTMP["codigo_area"] = "44";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "NF" ;
    $arrTMP["iso_3"] = "NFK";
    $arrTMP["nombre"] = "Isla Norfolk";
    $arrTMP["codigo_area"] = "672";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "IS" ;
    $arrTMP["iso_3"] = "ISL";
    $arrTMP["nombre"] = "Islandia";
    $arrTMP["codigo_area"] = "354";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "KY" ;
    $arrTMP["iso_3"] = "CYM";
    $arrTMP["nombre"] = "Islas Caimán";
    $arrTMP["codigo_area"] = "-344";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CK" ;
    $arrTMP["iso_3"] = "COK";
    $arrTMP["nombre"] = "Islas Cook";
    $arrTMP["codigo_area"] = "682";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "FO" ;
    $arrTMP["iso_3"] = "FRO";
    $arrTMP["nombre"] = "Islas Feroe";
    $arrTMP["codigo_area"] = "298";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "FK" ;
    $arrTMP["iso_3"] = "FLK";
    $arrTMP["nombre"] = "Islas Malvinas";
    $arrTMP["codigo_area"] = "500";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MP" ;
    $arrTMP["iso_3"] = "MNP";
    $arrTMP["nombre"] = "Islas Marianas del Norte";
    $arrTMP["codigo_area"] = "+1 670";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MH" ;
    $arrTMP["iso_3"] = "MHL";
    $arrTMP["nombre"] = "Islas Marshall";
    $arrTMP["codigo_area"] = "692";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PN" ;
    $arrTMP["iso_3"] = "PCN";
    $arrTMP["nombre"] = "Islas Pitcairn";
    $arrTMP["codigo_area"] = "870";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SB" ;
    $arrTMP["iso_3"] = "SLB";
    $arrTMP["nombre"] = "Islas Salomón";
    $arrTMP["codigo_area"] = "677";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "VG" ;
    $arrTMP["iso_3"] = "VGB";
    $arrTMP["nombre"] = "Islas Vírgenes Británicas";
    $arrTMP["codigo_area"] = "+1 284";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "IL" ;
    $arrTMP["iso_3"] = "ISR";
    $arrTMP["nombre"] = "Israel";
    $arrTMP["codigo_area"] = "972";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "IT" ;
    $arrTMP["iso_3"] = "ITA";
    $arrTMP["nombre"] = "Italia";
    $arrTMP["codigo_area"] = "39";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "JM" ;
    $arrTMP["iso_3"] = "JAM";
    $arrTMP["nombre"] = "Jamaica";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "JP" ;
    $arrTMP["iso_3"] = "JPN";
    $arrTMP["nombre"] = "Japón";
    $arrTMP["codigo_area"] = "81";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "JO" ;
    $arrTMP["iso_3"] = "JOR";
    $arrTMP["nombre"] = "Jordania";
    $arrTMP["codigo_area"] = "962";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KZ" ;
    $arrTMP["iso_3"] = "KAZ";
    $arrTMP["nombre"] = "Kazajistán";
    $arrTMP["codigo_area"] = "7";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KE" ;
    $arrTMP["iso_3"] = "KEN";
    $arrTMP["nombre"] = "Kenia";
    $arrTMP["codigo_area"] = "254";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KG" ;
    $arrTMP["iso_3"] = "KGZ";
    $arrTMP["nombre"] = "Kirgizistán";
    $arrTMP["codigo_area"] = "996";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KI" ;
    $arrTMP["iso_3"] = "KIR";
    $arrTMP["nombre"] = "Kiribati";
    $arrTMP["codigo_area"] = "686";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "XK" ;
    $arrTMP["iso_3"] = "XKX";
    $arrTMP["nombre"] = "Kosovo";
    $arrTMP["codigo_area"] = "381";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KW" ;
    $arrTMP["iso_3"] = "KWT";
    $arrTMP["nombre"] = "Kuwait";
    $arrTMP["codigo_area"] = "965";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LA" ;
    $arrTMP["iso_3"] = "LAO";
    $arrTMP["nombre"] = "Laos";
    $arrTMP["codigo_area"] = "856";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LS" ;
    $arrTMP["iso_3"] = "LSO";
    $arrTMP["nombre"] = "Lesoto";
    $arrTMP["codigo_area"] = "266";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LV" ;
    $arrTMP["iso_3"] = "LVA";
    $arrTMP["nombre"] = "Letonia";
    $arrTMP["codigo_area"] = "371";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LB" ;
    $arrTMP["iso_3"] = "LBN";
    $arrTMP["nombre"] = "Líbano";
    $arrTMP["codigo_area"] = "961";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LR" ;
    $arrTMP["iso_3"] = "LBR";
    $arrTMP["nombre"] = "Liberia";
    $arrTMP["codigo_area"] = "231";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LY" ;
    $arrTMP["iso_3"] = "LBY";
    $arrTMP["nombre"] = "Libia";
    $arrTMP["codigo_area"] = "218";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LI" ;
    $arrTMP["iso_3"] = "LIE";
    $arrTMP["nombre"] = "Liechtenstein";
    $arrTMP["codigo_area"] = "423";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LT" ;
    $arrTMP["iso_3"] = "LTU";
    $arrTMP["nombre"] = "Lituania";
    $arrTMP["codigo_area"] = "370";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LU" ;
    $arrTMP["iso_3"] = "LUX";
    $arrTMP["nombre"] = "Luxemburgo";
    $arrTMP["codigo_area"] = "352";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MO" ;
    $arrTMP["iso_3"] = "MAC";
    $arrTMP["nombre"] = "Macao";
    $arrTMP["codigo_area"] = "853";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MK" ;
    $arrTMP["iso_3"] = "MKD";
    $arrTMP["nombre"] = "Macedonia";
    $arrTMP["codigo_area"] = "389";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MG" ;
    $arrTMP["iso_3"] = "MDG";
    $arrTMP["nombre"] = "Madagascar";
    $arrTMP["codigo_area"] = "261";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MY" ;
    $arrTMP["iso_3"] = "MYS";
    $arrTMP["nombre"] = "Malasia";
    $arrTMP["codigo_area"] = "60";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MW" ;
    $arrTMP["iso_3"] = "MWI";
    $arrTMP["nombre"] = "Malaui";
    $arrTMP["codigo_area"] = "265";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MV" ;
    $arrTMP["iso_3"] = "MDV";
    $arrTMP["nombre"] = "Maldivas";
    $arrTMP["codigo_area"] = "960";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ML" ;
    $arrTMP["iso_3"] = "MLI";
    $arrTMP["nombre"] = "Mali";
    $arrTMP["codigo_area"] = "223";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MT" ;
    $arrTMP["iso_3"] = "MLT";
    $arrTMP["nombre"] = "Malta";
    $arrTMP["codigo_area"] = "356";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MA" ;
    $arrTMP["iso_3"] = "MAR";
    $arrTMP["nombre"] = "Marruecos";
    $arrTMP["codigo_area"] = "212";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MU" ;
    $arrTMP["iso_3"] = "MUS";
    $arrTMP["nombre"] = "Mauricio";
    $arrTMP["codigo_area"] = "230";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MR" ;
    $arrTMP["iso_3"] = "MRT";
    $arrTMP["nombre"] = "Mauritania";
    $arrTMP["codigo_area"] = "222";
    array_push($arrCodigo, $arrTMP);
    */

    /*
    

    $arrTMP["iso_2"] = "FM" ;
    $arrTMP["iso_3"] = "FSM";
    $arrTMP["nombre"] = "Micronesia";
    $arrTMP["codigo_area"] = "691";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MD" ;
    $arrTMP["iso_3"] = "MDA";
    $arrTMP["nombre"] = "Moldavia";
    $arrTMP["codigo_area"] = "373";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "MC" ;
    $arrTMP["iso_3"] = "MCO";
    $arrTMP["nombre"] = "Mónaco";
    $arrTMP["codigo_area"] = "377";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "MN" ;
    $arrTMP["iso_3"] = "MNG";
    $arrTMP["nombre"] = "Mongolia";
    $arrTMP["codigo_area"] = "976";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ME" ;
    $arrTMP["iso_3"] = "MNE";
    $arrTMP["nombre"] = "Montenegro";
    $arrTMP["codigo_area"] = "382";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MS" ;
    $arrTMP["iso_3"] = "MSR";
    $arrTMP["nombre"] = "Montserrat";
    $arrTMP["codigo_area"] = "+1 664";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MZ" ;
    $arrTMP["iso_3"] = "MOZ";
    $arrTMP["nombre"] = "Mozambique";
    $arrTMP["codigo_area"] = "258";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "NA" ;
    $arrTMP["iso_3"] = "NAM";
    $arrTMP["nombre"] = "Namibia";
    $arrTMP["codigo_area"] = "264";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "NR" ;
    $arrTMP["iso_3"] = "NRU";
    $arrTMP["nombre"] = "Nauru";
    $arrTMP["codigo_area"] = "674";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "NP" ;
    $arrTMP["iso_3"] = "NPL";
    $arrTMP["nombre"] = "Nepal";
    $arrTMP["codigo_area"] = "977";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "NI" ;
    $arrTMP["iso_3"] = "NIC";
    $arrTMP["nombre"] = "Nicaragua";
    $arrTMP["codigo_area"] = "505";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "NE" ;
    $arrTMP["iso_3"] = "NER";
    $arrTMP["nombre"] = "Níger";
    $arrTMP["codigo_area"] = "227";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "NG" ;
    $arrTMP["iso_3"] = "NGA";
    $arrTMP["nombre"] = "Nigeria";
    $arrTMP["codigo_area"] = "234";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "NU" ;
    $arrTMP["iso_3"] = "NIU";
    $arrTMP["nombre"] = "Niue";
    $arrTMP["codigo_area"] = "683";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "NO" ;
    $arrTMP["iso_3"] = "NOR";
    $arrTMP["nombre"] = "Noruega";
    $arrTMP["codigo_area"] = "47";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "NC" ;
    $arrTMP["iso_3"] = "NCL";
    $arrTMP["nombre"] = "Nueva Caledonia";
    $arrTMP["codigo_area"] = "687";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "NZ" ;
    $arrTMP["iso_3"] = "NZL";
    $arrTMP["nombre"] = "Nueva Zelanda";
    $arrTMP["codigo_area"] = "64";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "OM" ;
    $arrTMP["iso_3"] = "OMN";
    $arrTMP["nombre"] = "Omán";
    $arrTMP["codigo_area"] = "968";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PK" ;
    $arrTMP["iso_3"] = "PAK";
    $arrTMP["nombre"] = "Pakistán";
    $arrTMP["codigo_area"] = "92";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PW" ;
    $arrTMP["iso_3"] = "PLW";
    $arrTMP["nombre"] = "Palau";
    $arrTMP["codigo_area"] = "680";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "PA" ;
    $arrTMP["iso_3"] = "PAN";
    $arrTMP["nombre"] = "Panamá";
    $arrTMP["codigo_area"] = "507";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "PG" ;
    $arrTMP["iso_3"] = "PNG";
    $arrTMP["nombre"] = "Papúa Nueva Guinea";
    $arrTMP["codigo_area"] = "675";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "PY" ;
    $arrTMP["iso_3"] = "PRY";
    $arrTMP["nombre"] = "Paraguay";
    $arrTMP["codigo_area"] = "595";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PE" ;
    $arrTMP["iso_3"] = "PER";
    $arrTMP["nombre"] = "Perú";
    $arrTMP["codigo_area"] = "51";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "PF" ;
    $arrTMP["iso_3"] = "PYF";
    $arrTMP["nombre"] = "Polinesia Francesa";
    $arrTMP["codigo_area"] = "689";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "PL" ;
    $arrTMP["iso_3"] = "POL";
    $arrTMP["nombre"] = "Polonia";
    $arrTMP["codigo_area"] = "48";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PT" ;
    $arrTMP["iso_3"] = "PRT";
    $arrTMP["nombre"] = "Portugal";
    $arrTMP["codigo_area"] = "351";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PR" ;
    $arrTMP["iso_3"] = "PRI";
    $arrTMP["nombre"] = "Puerto Rico";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "QA" ;
    $arrTMP["iso_3"] = "QAT";
    $arrTMP["nombre"] = "Qatar";
    $arrTMP["codigo_area"] = "974";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "GB" ;
    $arrTMP["iso_3"] = "GBR";
    $arrTMP["nombre"] = "Reino Unido";
    $arrTMP["codigo_area"] = "44";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "CF" ;
    $arrTMP["iso_3"] = "CAF";
    $arrTMP["nombre"] = "República Centroafricana";
    $arrTMP["codigo_area"] = "236";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "CZ" ;
    $arrTMP["iso_3"] = "CZE";
    $arrTMP["nombre"] = "República Checa";
    $arrTMP["codigo_area"] = "420";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "SS" ;
    $arrTMP["iso_3"] = "SSD";
    $arrTMP["nombre"] = "República de Sudán del Sur";
    $arrTMP["codigo_area"] = "211";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CG" ;
    $arrTMP["iso_3"] = "COG";
    $arrTMP["nombre"] = "República del Congo";
    $arrTMP["codigo_area"] = "242";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CD" ;
    $arrTMP["iso_3"] = "COD";
    $arrTMP["nombre"] = "República Democrática del Congo";
    $arrTMP["codigo_area"] = "243";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "DO" ;
    $arrTMP["iso_3"] = "DOM";
    $arrTMP["nombre"] = "República Dominicana";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);

    /*
    

    $arrTMP["iso_2"] = "RE" ;
    $arrTMP["iso_3"] = "REU";
    $arrTMP["nombre"] = "Reunión";
    $arrTMP["codigo_area"] = "262";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "RW" ;
    $arrTMP["iso_3"] = "RWA";
    $arrTMP["nombre"] = "Ruanda";
    $arrTMP["codigo_area"] = "250";
    array_push($arrCodigo, $arrTMP);
    */
    

    $arrTMP["iso_2"] = "RO" ;
    $arrTMP["iso_3"] = "ROU";
    $arrTMP["nombre"] = "Rumanía";
    $arrTMP["codigo_area"] = "40";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "RU" ;
    $arrTMP["iso_3"] = "RUS";
    $arrTMP["nombre"] = "Rusia";
    $arrTMP["codigo_area"] = "7";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "WS" ;
    $arrTMP["iso_3"] = "WSM";
    $arrTMP["nombre"] = "Samoa";
    $arrTMP["codigo_area"] = "685";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "AS" ;
    $arrTMP["iso_3"] = "ASM";
    $arrTMP["nombre"] = "Samoa Americana";
    $arrTMP["codigo_area"] = "+1 684";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "BL" ;
    $arrTMP["iso_3"] = "BLM";
    $arrTMP["nombre"] = "San Bartolomé";
    $arrTMP["codigo_area"] = "590";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "KN" ;
    $arrTMP["iso_3"] = "KNA";
    $arrTMP["nombre"] = "San Cristóbal y Nevis";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SM" ;
    $arrTMP["iso_3"] = "SMR";
    $arrTMP["nombre"] = "San Marino";
    $arrTMP["codigo_area"] = "378";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "MF" ;
    $arrTMP["iso_3"] = "MAF";
    $arrTMP["nombre"] = "San Martín";
    $arrTMP["codigo_area"] = "+1 599";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "PM" ;
    $arrTMP["iso_3"] = "SPM";
    $arrTMP["nombre"] = "San Pedro y Miquelón";
    $arrTMP["codigo_area"] = "508";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "VC" ;
    $arrTMP["iso_3"] = "VCT";
    $arrTMP["nombre"] = "San Vicente y las Granadinas";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);

    /*
    

    $arrTMP["iso_2"] = "SH" ;
    $arrTMP["iso_3"] = "SHN";
    $arrTMP["nombre"] = "Santa Elena";
    $arrTMP["codigo_area"] = "290";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LC" ;
    $arrTMP["iso_3"] = "LCA";
    $arrTMP["nombre"] = "Santa Lucía";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ST" ;
    $arrTMP["iso_3"] = "STP";
    $arrTMP["nombre"] = "Santo Tomé y Príncipe";
    $arrTMP["codigo_area"] = "239";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SN" ;
    $arrTMP["iso_3"] = "SEN";
    $arrTMP["nombre"] = "Senegal";
    $arrTMP["codigo_area"] = "221";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "RS" ;
    $arrTMP["iso_3"] = "SRB";
    $arrTMP["nombre"] = "Serbia";
    $arrTMP["codigo_area"] = "381";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SC" ;
    $arrTMP["iso_3"] = "SYC";
    $arrTMP["nombre"] = "Seychelles";
    $arrTMP["codigo_area"] = "248";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SL" ;
    $arrTMP["iso_3"] = "SLE";
    $arrTMP["nombre"] = "Sierra Leona";
    $arrTMP["codigo_area"] = "232";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SG" ;
    $arrTMP["iso_3"] = "SGP";
    $arrTMP["nombre"] = "Singapur";
    $arrTMP["codigo_area"] = "65";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SY" ;
    $arrTMP["iso_3"] = "SYR";
    $arrTMP["nombre"] = "Siria";
    $arrTMP["codigo_area"] = "963";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SO" ;
    $arrTMP["iso_3"] = "SOM";
    $arrTMP["nombre"] = "Somalia";
    $arrTMP["codigo_area"] = "252";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "LK" ;
    $arrTMP["iso_3"] = "LKA";
    $arrTMP["nombre"] = "Sri Lanka";
    $arrTMP["codigo_area"] = "94";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ZA" ;
    $arrTMP["iso_3"] = "ZAF";
    $arrTMP["nombre"] = "Sudáfrica";
    $arrTMP["codigo_area"] = "27";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SD" ;
    $arrTMP["iso_3"] = "SDN";
    $arrTMP["nombre"] = "Sudán";
    $arrTMP["codigo_area"] = "249";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "SE" ;
    $arrTMP["iso_3"] = "SWE";
    $arrTMP["nombre"] = "Suecia";
    $arrTMP["codigo_area"] = "46";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "CH" ;
    $arrTMP["iso_3"] = "CHE";
    $arrTMP["nombre"] = "Suiza";
    $arrTMP["codigo_area"] = "41";
    array_push($arrCodigo, $arrTMP);

    /*
    

    $arrTMP["iso_2"] = "SR" ;
    $arrTMP["iso_3"] = "SUR";
    $arrTMP["nombre"] = "Surinam";
    $arrTMP["codigo_area"] = "597";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "SZ" ;
    $arrTMP["iso_3"] = "SWZ";
    $arrTMP["nombre"] = "Swazilandia";
    $arrTMP["codigo_area"] = "268";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TH" ;
    $arrTMP["iso_3"] = "THA";
    $arrTMP["nombre"] = "Tailandia";
    $arrTMP["codigo_area"] = "66";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TW" ;
    $arrTMP["iso_3"] = "TWN";
    $arrTMP["nombre"] = "Taiwán";
    $arrTMP["codigo_area"] = "886";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TZ" ;
    $arrTMP["iso_3"] = "TZA";
    $arrTMP["nombre"] = "Tanzania";
    $arrTMP["codigo_area"] = "255";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TJ" ;
    $arrTMP["iso_3"] = "TJK";
    $arrTMP["nombre"] = "Tayikistán";
    $arrTMP["codigo_area"] = "992";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TL" ;
    $arrTMP["iso_3"] = "TLS";
    $arrTMP["nombre"] = "Timor Oriental";
    $arrTMP["codigo_area"] = "670";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TG" ;
    $arrTMP["iso_3"] = "TGO";
    $arrTMP["nombre"] = "Togo";
    $arrTMP["codigo_area"] = "228";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TK" ;
    $arrTMP["iso_3"] = "TKL";
    $arrTMP["nombre"] = "Tokelau";
    $arrTMP["codigo_area"] = "690";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "TT" ;
    $arrTMP["iso_3"] = "TTO";
    $arrTMP["nombre"] = "Trinidad y Tobago";
    $arrTMP["codigo_area"] = "1";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "TN" ;
    $arrTMP["iso_3"] = "TUN";
    $arrTMP["nombre"] = "Túnez";
    $arrTMP["codigo_area"] = "216";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "TM" ;
    $arrTMP["iso_3"] = "TKM";
    $arrTMP["nombre"] = "Turkmenistán";
    $arrTMP["codigo_area"] = "993";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "TR" ;
    $arrTMP["iso_3"] = "TUR";
    $arrTMP["nombre"] = "Turquía";
    $arrTMP["codigo_area"] = "90";
    array_push($arrCodigo, $arrTMP);
    /*
    

    $arrTMP["iso_2"] = "TV" ;
    $arrTMP["iso_3"] = "TUV";
    $arrTMP["nombre"] = "Tuvalu";
    $arrTMP["codigo_area"] = "688";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "UA" ;
    $arrTMP["iso_3"] = "UKR";
    $arrTMP["nombre"] = "Ucrania";
    $arrTMP["codigo_area"] = "380";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "UG" ;
    $arrTMP["iso_3"] = "UGA";
    $arrTMP["nombre"] = "Uganda";
    $arrTMP["codigo_area"] = "256";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "UY" ;
    $arrTMP["iso_3"] = "URY";
    $arrTMP["nombre"] = "Uruguay";
    $arrTMP["codigo_area"] = "598";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "UZ" ;
    $arrTMP["iso_3"] = "UZB";
    $arrTMP["nombre"] = "Uzbekistán";
    $arrTMP["codigo_area"] = "998";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "VU" ;
    $arrTMP["iso_3"] = "VUT";
    $arrTMP["nombre"] = "Vanuatu";
    $arrTMP["codigo_area"] = "678";
    array_push($arrCodigo, $arrTMP);
    */

    $arrTMP["iso_2"] = "VE" ;
    $arrTMP["iso_3"] = "VEN";
    $arrTMP["nombre"] = "Venezuela";
    $arrTMP["codigo_area"] = "58";
    array_push($arrCodigo, $arrTMP);

    /*
    
    $arrTMP["iso_2"] = "VN" ;
    $arrTMP["iso_3"] = "VNM";
    $arrTMP["nombre"] = "Vietnam";
    $arrTMP["codigo_area"] = "84";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "YE" ;
    $arrTMP["iso_3"] = "YEM";
    $arrTMP["nombre"] = "Yemen";
    $arrTMP["codigo_area"] = "967";
    array_push($arrCodigo, $arrTMP);


    $arrTMP["iso_2"] = "ZM" ;
    $arrTMP["iso_3"] = "ZMB";
    $arrTMP["nombre"] = "Zambia";
    $arrTMP["codigo_area"] = "260";
    array_push($arrCodigo, $arrTMP);

    $arrTMP["iso_2"] = "ZW" ;
    $arrTMP["iso_3"] = "ZWE";
    $arrTMP["nombre"] = "Zimbabue";
    $arrTMP["codigo_area"] = "263";
    array_push($arrCodigo, $arrTMP);
    */
    
    return $arrCodigo;
}

function app_fntGetLugarCategoriaEspecial($intCategoriEspecia, $intUbicacion, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $intUbicacion = sesion["ubicacion"];
    $arrCatalogoUbicacionLugar1 = fntUbicacionLugar();
    $strKeyUbicaionLugar = "";
    while( $rTMP = each($arrCatalogoUbicacionLugar1[$intUbicacion]["lugar"]) ){
        
        $strKeyUbicaionLugar .= ( $strKeyUbicaionLugar == "" ? "" : "," ).$rTMP["key"];
        
    }  
    
    $arrCatalogoPrecioGoogle = fntCatalogoPriceLevelGoogle();
    
    
    $strQuery = "SELECT texto_google_place
                 FROM   categoria_especial
                 WHERE  id_categoria_especial = {$intCategoriEspecia} ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    $strTexto = $rTMP["texto_google_place"];
    
    $strQuery = "SELECT negocio.id_negocio
                 FROM   texto,
                        negocio_texto,
                        negocio
                 WHERE  texto.texto LIKE '%{$strTexto}%'
                 AND    negocio_texto.id_texto = texto.id_texto                 
                 AND    negocio.id_negocio = negocio_texto.id_negocio                 
                 AND    negocio.id_ubicacion_lugar IN({$strKeyUbicaionLugar})
                 
                 ";
                 
    $arrKeyNegocio = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrKeyNegocio[$rTMP["id_negocio"]] = $rTMP["id_negocio"];
    
    }
    $objDBClass->db_free_result($qTMP);
    
    $strKeyNegocio = implode(",", $arrKeyNegocio);
    
    $strQuery = "SELECT negocio.id_negocio,
                        negocio.nombre,
                        negocio.lat,
                        negocio.lng,
                        negocio.direccion,
                        negocio.rango,
                        negocio.precio,
                        negocio.telefono,
                        negocio.estado,
                        negocio_galeria.id_negocio_galeria,
                        negocio_galeria.url
                 FROM   negocio
                            LEFT JOIN negocio_galeria
                                ON  negocio_galeria.id_negocio = negocio.id_negocio
                          
                 WHERE  negocio.id_negocio IN({$strKeyNegocio})                
                 ORDER BY rango DESC, id_negocio_galeria DESC 
                 ";        
    
    $arrPlace = array();
    $arrDatos = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrDatos[$rTMP["id_negocio"]]["id_negocio"] = $rTMP["id_negocio"];
        $arrDatos[$rTMP["id_negocio"]]["nombre"] = $rTMP["nombre"];
        $arrDatos[$rTMP["id_negocio"]]["lat"] = $rTMP["lat"];
        $arrDatos[$rTMP["id_negocio"]]["lng"] = $rTMP["lng"];
        $arrDatos[$rTMP["id_negocio"]]["direccion"] = $rTMP["direccion"];
        $arrDatos[$rTMP["id_negocio"]]["rango"] = $rTMP["rango"];
        $arrDatos[$rTMP["id_negocio"]]["telefono"] = $rTMP["telefono"];
        $arrDatos[$rTMP["id_negocio"]]["estado"] = $rTMP["estado"];
        $arrDatos[$rTMP["id_negocio"]]["precio"] = $rTMP["precio"];
        
        
        if( intval($rTMP["id_negocio_galeria"]) ){
            
            $arrDatos[$rTMP["id_negocio"]]["galeria"][$rTMP["id_negocio_galeria"]]["url"] = $rTMP["url"];
            
        } 
            
    }
    
    $objDBClass->db_free_result($qTMP);
                 
    
    if( count($arrDatos)  > 0 ) {
        
        while( $rTMP = each($arrDatos)){
            
            $arrTMP["tipo_lugar"] = "NC";
            $arrTMP["direccion"] = $rTMP["value"]["direccion"];
            $arrTMP["nombre"] = $rTMP["value"]["nombre"];
            $arrTMP["identificador"] = md5($rTMP["value"]["id_negocio"]);
            $arrTMP["rating"] = $rTMP["value"]["rango"];
            $arrTMP["precio"] = $rTMP["value"]["precio"];
            $arrTMP["opening_hours"] = "ssssssss";
            $arrTMP["price_level"] = isset($arrCatalogoPrecioGoogle[$rTMP["value"]["precio"]]) ?  $arrCatalogoPrecioGoogle[$rTMP["value"]["precio"]]["nombre"] : "";
            
            
            $arrTMP["foto"] = "images/Noimagee.jpg?1";
                
            if( isset($rTMP["value"]["galeria"]) ){
                
                while( $arrTMP1 = each($rTMP["value"]["galeria"]) ){
                    
                    $arrTMP["foto"] = "http://192.168.1.54/inguate/imgInguate.php?t=up&src=".$arrTMP1["value"]["url"];
                                   
                }
                
            }
                                         
            array_push($arrPlace, $arrTMP);    
            
        }
        
    }            
    
    
    $arr["next_page_token"] = 10;
    $arr["resultados"] = $arrPlace;
    
    return $arr;
}
  
function app_fntGetLugarNoConfirmado($strPlaceId, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strQuery = "SELECT negocio.id_negocio,
                        negocio.nombre,
                        negocio.lat,
                        negocio.lng,
                        negocio.direccion,
                        negocio.rango,
                        negocio.precio,
                        negocio.telefono,
                        negocio.estado,
                        negocio_galeria.id_negocio_galeria,
                        negocio_galeria.url,
                        negocio_horario.id_negocio_horario,
                        negocio_horario.dia,
                        negocio_horario.horario
                 FROM   negocio
                            LEFT JOIN negocio_galeria
                                ON  negocio_galeria.id_negocio = negocio.id_negocio
                          
                            LEFT JOIN negocio_horario
                                ON  negocio_horario.id_negocio = negocio.id_negocio
                          
                 WHERE  md5(negocio.id_negocio) = '{$strPlaceId}'
                 "; 
    $arrCatalogoPrecioGoogle = fntCatalogoPriceLevelGoogle();

    $arrPlace = array();
    $arrDatos = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrDatos["id_negocio"] = $rTMP["id_negocio"];
        $arrDatos["nombre"] = $rTMP["nombre"];
        $arrDatos["lat"] = $rTMP["lat"];
        $arrDatos["lng"] = $rTMP["lng"];
        $arrDatos["direccion"] = $rTMP["direccion"];
        $arrDatos["rating"] = $rTMP["rango"];
        $arrDatos["telefono"] = $rTMP["telefono"];
        $arrDatos["estado"] = $rTMP["estado"];
        $arrDatos["price_level"] =  $arrCatalogoPrecioGoogle[$rTMP["precio"]]["nombre"];
        
        
        if( intval($rTMP["id_negocio_galeria"]) ){
            
            $arrDatos["galeria"][$rTMP["id_negocio_galeria"]]["url"] = "http://192.168.1.54/inguate/imgInguate.php?t=up&src=".$rTMP["url"];
            
        } 
            
        if( intval($rTMP["id_negocio_horario"]) ){
            
            $arrDatos["horario"][$rTMP["id_negocio_horario"]]["dia"] = $rTMP["dia"];
            $arrDatos["horario"][$rTMP["id_negocio_horario"]]["horario"] = $rTMP["horario"];
        
        } 
            
    }

    $objDBClass->db_free_result($qTMP);

    return $arrDatos;
}

function fntSendEmailVerificacionUsuario($strCorreo, $strNombre, $strCodigoEnvio){
    
    $strSaludo = lang["hola"];
    $strBoton = lang["verifica_tu_cuenta"];
    $strTitulo = lang["verifica_tu_cuenta_inguate"];
    
    $strBody = "
                        <style type='text/css'>
                        /* latin-ext */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                        }
                        /* latin */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                        }

                          /* Take care of image borders and formatting */

                          img {
                              max-width: 600px;
                              outline: none;
                              text-decoration: none;
                              -ms-interpolation-mode: bicubic;
                          }

                        a {
                          text-decoration: none;
                          border: 0;
                          outline: none;
                        }

                          a img {
                              border: none;
                          }

                        td, h1, h2, h3  {
                          font-family: Helvetica, Arial, sans-serif;
                          font-weight: 400;
                        }

                        body {
                          -webkit-font-smoothing:antialiased;
                          -webkit-text-size-adjust:none;
                          width: 100%;
                          height: 100%;
                          color: #37302d;
                          background: #ffffff;
                        }

                        table {
                          background:
                        }

                        h1, h2, h3 {
                          padding: 0;
                          margin: 0;
                          color: #ffffff;
                          font-weight: 400;
                        }

                        h3 {
                          color: #21c5ba;
                          font-size: 24px;
                        }
                        </style>

                      <style type='text/css' media='screen'>
                        @media screen {
                          td, h1, h2, h3 {
                            font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                          }
                        }
                      </style>

                      <style type='text/css' media='only screen and (max-width: 480px)'>
                        @media only screen and (max-width: 480px) {

                          table[class='w320'] {
                            width: 320px !important;
                          }

                          table[class='w300'] {
                            width: 300px !important;
                          }

                          table[class='w290'] {
                            width: 290px !important;
                          }

                          td[class='w320'] {
                            width: 320px !important;
                          }

                          td[class='mobile-center'] {
                            text-align: center !important;
                          }

                          td[class='mobile-padding'] {
                            padding-left: 20px !important;
                            padding-right: 20px !important;
                            padding-bottom: 20px !important;
                          }
                        }
                      </style>
                    <table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' >
                      <tr>
                        <td align='center' valign='top' bgcolor='#ffffff'  width='100%'>

                        <table cellspacing='0' cellpadding='0' width='100%'>
                          <tr>
                            <td style='border-bottom: 3px solid #3E53AC;' width='100%'>
                              <center>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                  <tr>
                                    <td valign='top' style='padding:10px 0; text-align:center;' class='mobile-center'>
                                      <img width='250' height='62' src='https://inguate.com/images/LogoInguate.png'>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          
                          <tr>
                            <td valign='top'>
                              
                              <center>
                                <table cellspacing='0' cellpadding='30' width='500' class='w290'>
                                          <tr>
                                            <td valign='top' style='border-bottom:1px solid #a1a1a1;'>
                                                      
                                              <center>
                                              <table style='margin: 0 auto;' cellspacing='0' cellpadding='8' width='500'>
                                                <tr>
                                                  <td style='text-align:;left; color: black; text-align:center; font-weight:bold;font-size:10px;'>
                                                    
                                                    {$strSaludo} {$strNombre}
                                                    
                                                  </td>
                                                </tr>
                                              </table>
                                              </center>
                                            </td>
                                          </tr>
                                        </table>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                     
                                  <tr>
                                    <td class='mobile-padding' style='text-align: center;'>
                                      <table cellspacing='0' cellpadding='0' width='100%'>
                                        <tr>
                                          <td style='width:100%; text-align: center; background-color: #3E53AC;'>
                                            <div>
                                            <table cellspacing='0' cellpadding='0' width='100%'>
                                                <tr>
                                                    <td style='text-align: center;background-color:#3E53AC;border-radius:0px;color:#ffffff;display:inline-block;font-family:'Lato', Helvetica, Arial, sans-serif;font-weight:bold;font-size:13px;line-height:33px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;mso-hide:all;'>
                                                        <span style='color:#ffffff'><br>
                                                        
                                                        <a href='https://inguate.com/index.php?cc={$strCodigoEnvio}' style='color:white;'>
                                                            <h1 >
                                                                {$strBoton}
                                                            </h1>
                                                        </a>
                                                        
                                                        <br>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                            </div>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  
                                </table>
                              </center>
                            </td>
                          </tr>
                          
                        </table>

                        </td>
                      </tr>
                    </table>
                    ";

    fntEnviarCorreo($strCorreo, $strTitulo, $strBody);
    
}

function fntGetInformacionClienteNavegacion(){
    
    // obteniendo el 'user_agent':
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $os_array =  array(
                    '/windows nt 10/i'      =>  'Windows 10',
                    '/windows nt 6.3/i'     =>  'Windows 8.1',
                    '/windows nt 6.2/i'     =>  'Windows 8',
                    '/windows nt 6.1/i'     =>  'Windows 7',
                    '/windows nt 6.0/i'     =>  'Windows Vista',
                    '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                    '/windows nt 5.1/i'     =>  'Windows XP',
                    '/windows xp/i'         =>  'Windows XP',
                    '/windows nt 5.0/i'     =>  'Windows 2000',
                    '/windows me/i'         =>  'Windows ME',
                    '/win98/i'              =>  'Windows 98',
                    '/win95/i'              =>  'Windows 95',
                    '/win16/i'              =>  'Windows 3.11',
                    '/macintosh|mac os x/i' =>  'Mac OS X',
                    '/mac_powerpc/i'        =>  'Mac OS 9',
                    '/linux/i'              =>  'Linux',
                    '/ubuntu/i'             =>  'Ubuntu',
                    '/iphone/i'             =>  'iPhone',
                    '/ipod/i'               =>  'iPod',
                    '/ipad/i'               =>  'iPad',
                    '/android/i'            =>  'Android',
                    '/blackberry/i'         =>  'BlackBerry',
                    '/webos/i'              =>  'Mobile'
                  );
    //
    $os_platform = "Unknown OS Platform";
    foreach ($os_array as $regex => $value) { 
        if (preg_match($regex, $user_agent)) {
            $os_platform = $value;
        }
    }
    $browser_array = array(
                        '/msie/i'       =>  'Internet Explorer',
                        '/firefox/i'    =>  'Firefox',
                        '/safari/i'     =>  'Safari',
                        '/chrome/i'     =>  'Chrome',
                        '/edge/i'       =>  'Edge',
                        '/opera/i'      =>  'Opera',
                        '/netscape/i'   =>  'Netscape',
                        '/maxthon/i'    =>  'Maxthon',
                        '/konqueror/i'  =>  'Konqueror',
                        '/mobile/i'     =>  'Handheld Browser'
                      );
    $browser = "Unknown Browser";
    foreach ($browser_array as $regex => $value) { 
        if (preg_match($regex, $user_agent)) {
            $browser = $value;
            
        }
    }
    
    $arr["navegador"] = $browser;
    $arr["plataforma"] = $os_platform;
    $arr["detalle_completo"] = $user_agent;
    $arr["ip"] = $_SERVER['REMOTE_ADDR']; ;
    
    return $arr;
        
}

function fntSetRastreoBusquedaUsuario( $intIdUsuario, $intOrigen, $intCategoriaBusqueda,
                                       $strDatoNoConfirmado, $intLLave, $strTexto, 
                                       $strRango, $intUbicacionLugar, $intCantidadResultado, $strLenguaje, 
                                       $strFiltroTipo, $strFiltroKey, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
        
    $boolMovil = $pos === false ? false : true;

    if( !$boolMovil ){
        
        $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

        $boolMovil = $pos === false ? false : true;

    }

    $strFiltroKey = intval($strFiltroKey);
    
    $arrInformacionCliente = fntGetInformacionClienteNavegacion();
    
    $strQuery = "INSERT INTO usuario_busqueda(id_usuario, origen,  dispositivo, categoria_busqueda, 
                                              dato_no_confirmado, id_llave, texto,
                                              rango_precio, id_ubicacion_lugar, cantidad_resultado,
                                              navegador, ip, lenguaje,
                                              filtro_tipo, filtro_key )
                                       VALUES({$intIdUsuario}, {$intOrigen}, '{$arrInformacionCliente["plataforma"]}', '{$intCategoriaBusqueda}',
                                              '{$strDatoNoConfirmado}', '{$intLLave}', '{$strTexto}', 
                                              '{$strRango}', {$intUbicacionLugar}, {$intCantidadResultado},                
                                              '{$arrInformacionCliente["navegador"]}', '{$arrInformacionCliente["ip"]}', '{$strLenguaje}',
                                              '{$strFiltroTipo}', '{$strFiltroKey}'  )      ";
    $objDBClass->db_consulta($strQuery);
        
}

function fntGetPlaceComentario($intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $arrComentario = array();
    
    $strQuery = "SELECT place_comentario.comentario,
                        usuario.nombre,
                        place_comentario.add_fecha fechaori,
                        DATE_FORMAT(DATE_SUB(place_comentario.add_fecha, INTERVAL 6 HOUR), '%d/%m/%Y' ) fecha
                 FROM   place_comentario,
                        usuario
                 WHERE  place_comentario.id_place = {$intPlace} 
                 AND    place_comentario.id_usuario = usuario.id_usuario
                 ORDER by place_comentario.add_fecha DESC
                 LIMIT  5
                 ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        array_push($arrComentario, $rTMP);
            
    }
    
    
    $objDBClass->db_free_result($qTMP);
    
    return $arrComentario;  
    
}

function fntEliminarTildes($cadena){
 
    //Codificamos la cadena en formato utf8 en caso de que nos de errores
    //$cadena = utf8_encode($cadena);
 
    //Ahora reemplazamos las letras
    //Ahora reemplazamos las letras
    $cadena = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $cadena
    );

    $cadena = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $cadena );

    $cadena = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $cadena );

    $cadena = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $cadena );

    $cadena = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $cadena );

    $cadena = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C'),
        $cadena
    );
 
    $cadena = str_replace(
        array('?', '?', '?', '?', '"', "&"),
        array('n', 'N', 'c', 'C', " ", "&"),
        $cadena
    );
 
    return $cadena;
}

function fntGetNombrePaginaPublica($strTitulo, $intPlace, $boolInterno = false, $objDBClass = null){
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strTitulo = fntEliminarTildes($strTitulo);
    $strTitulo = strtolower($strTitulo);
    $strTitulo = str_replace(" ", "-", $strTitulo);
    
    $strQuery = "SELECT id_place
                 FROM   place
                 WHERE  url_place = '{$strTitulo}' 
                 AND    estado = 'A'
                 AND    id_place != {$intPlace} ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    $intPlace = isset($rTMP["id_place"]) ? intval($rTMP["id_place"]) : 0;
    
    $arr["error"] = false;
    $arr["url_place"] = $strTitulo;
    
    if( $intPlace ){
        
        $arr["error"] = true;
        $arr["url_place"] = "";
            
    }
    
    return $arr;
    
}
    
function app_fntGetAutoComplete($intIdUsuario, $intUbicacion, $strParametroBusqueda, $strLenguaje = "es", $boolRetornoArray = false, $boolMovil = false, $boolInterno = false, $objDBClass = null){
         
    
    if( !$boolInterno ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
    }
    
    $strParametroBusqueda = fntCoreStringQueryLetra($strParametroBusqueda);
    $strParametroBusqueda = strtolower($strParametroBusqueda);
    
    $arrCatalogoUbicacionLugar1 = fntUbicacionLugar();
    $strKeyUbicaionLugar = "";
    while( $rTMP = each($arrCatalogoUbicacionLugar1[$intUbicacion]["lugar"]) ){
        
        $strKeyUbicaionLugar .= ( $strKeyUbicaionLugar == "" ? "" : "," ).$rTMP["key"];
        
    }
    
    $strTextoBusquedaPlace =  "";
    
    $arrSeparacionString = explode(" ", $strParametroBusqueda);
    
    while( $rTMP = each($arrSeparacionString) ){
        
        $strTextoBusquedaPlace .= ( $strTextoBusquedaPlace == "" ? "" : " OR " )." ( texto_autocomplete LIKE '%{$rTMP["value"]}%' )  ";
            
    }    
    
    $strQuery = "SELECT id_clasificacion identificador,
                        '' identificador_2,
                        nombre,
                        tag_en,
                        tag_es,
                        '1' tipo
                 FROM   clasificacion
                 WHERE  sinonimo LIKE '%{$strParametroBusqueda}%' 
                 
                 UNION ALL 
                         
                 SELECT clasificacion_padre.id_clasificacion_padre identificador,
                        '' identificador_2,
                        clasificacion_padre.nombre,
                        CONCAT(clasificacion.tag_en, ' / ', clasificacion_padre.tag_en)  tag_en,
                        CONCAT(clasificacion.tag_es, ' / ', clasificacion_padre.tag_es)  tag_es,
                        '2' tipo
                 FROM   clasificacion_padre,
                        clasificacion
                 WHERE  clasificacion_padre.sinonimo LIKE '%{$strParametroBusqueda}%' 
                 AND    clasificacion_padre.id_clasificacion = clasificacion.id_clasificacion
                 
                 UNION ALL
                 
                 SELECT id_clasificacion_padre_hijo identificador,
                        '' identificador_2,
                        nombre,
                        tag_en,
                        tag_es,
                        '3' tipo
                 FROM   clasificacion_padre_hijo
                 WHERE  clasificacion_padre_hijo.sinonimo LIKE '%{$strParametroBusqueda}%' 
                 
                 UNION ALL 
                       
                 SELECT id_place identificador,
                        url_place identificador_2,
                        titulo nombre,
                        titulo tag_en,
                        titulo tag_es,
                        '5' tipo
                 FROM   place
                 WHERE  ( {$strTextoBusquedaPlace} ) 
                 AND    estado IN('A')
                 
                 UNION ALL
                 
                 SELECT id_categoria_especial identificador,
                        '' identificador_2,
                        nombre nombre,
                        tag_en tag_en,
                        tag_es tag_es,
                        '7' tipo
                 FROM   categoria_especial
                 WHERE  sinonimo LIKE '%{$strParametroBusqueda}%'
                 AND    estado IN('A')
                 
                 ORDER BY nombre
                ";
    
    $arrRetorno = array();
    
    $strKeyClasificacion = "";
    $strKeyClasificacionPadre = "";
    
    $arrClasificacion = array();
    $arrClasificacionPadre = array();
    $arrClasificacionPadreHijo = array();
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        //Key Para buscar Place
        if( $rTMP["tipo"] == "1" ){
            
            $arrClasificacion[$rTMP["identificador"]] = $rTMP["identificador"];
            
            $strKey = $rTMP["tipo"]."||".$rTMP["identificador"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["identificador"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            //$arrRetorno[$strKey]["texto_corto"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
                                            
        }
        
        if( $rTMP["tipo"] == "2" ){
            
            $arrClasificacionPadre[$rTMP["identificador"]] = $rTMP["identificador"];
            $strKey = $rTMP["tipo"]."||".$rTMP["identificador"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["identificador"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            //$arrRetorno[$strKey]["texto_corto"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
            
        }
        
        if( $rTMP["tipo"] == "3" ){
            
            $arrClasificacionPadreHijo[$rTMP["nombre"]][$rTMP["identificador"]] = $rTMP["identificador"];
            
            $strKey = $rTMP["tipo"]."||".$rTMP["nombre"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            //$arrRetorno[$strKey]["texto_corto"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
            
        }
               
        if( $rTMP["tipo"] == "4" || $rTMP["tipo"] == "5" || $rTMP["tipo"] == "7" ){
            
            $strKey = $rTMP["tipo"]."||".$rTMP["identificador"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["identificador"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
                                            
        }
        
    }
    $objDBClass->db_free_result($qTMP);
    
    //drawdebug($arrRetorno);
    
    $arrDatosClasificacionRango = array();
    $arrDatosClasificacionPadreRango = array();
    if( count($arrClasificacion) > 0 || count($arrClasificacionPadre) > 0 ){
        
        $strKeyClasificacion = implode(",", $arrClasificacion);
        $strKeyClasificacionPadre = implode(",", $arrClasificacionPadre);
        
        $strFiltroKeyClasificacion = !empty($strKeyClasificacion) ? "AND    place_clasificacion.id_clasificacion IN({$strKeyClasificacion})" : "";
        $strFiltroKeyClasificacionPadre = !empty($strKeyClasificacionPadre) ? "AND    place_clasificacion_padre.id_clasificacion_padre IN({$strKeyClasificacionPadre})" : "";
        
        $strQuery = "SELECT place.id_place,
                            place.titulo,
                            place.precio_mediana_bajo,
                            place.precio_mediana_alto,
                            place_clasificacion.id_clasificacion,
                            place_clasificacion_padre.id_clasificacion_padre
                     FROM   place,
                            place_clasificacion,
                            place_clasificacion_padre
                     WHERE  place.id_ubicacion_lugar IN({$strKeyUbicaionLugar})
                     AND    place.estado = 'A'
                     AND    place.id_place = place_clasificacion.id_place
                     AND    place.id_place = place_clasificacion_padre.id_place
                     {$strFiltroKeyClasificacion}
                     {$strFiltroKeyClasificacionPadre}
                     ";
        $arrDatosClasificacion = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrDatosClasificacion[$rTMP["id_clasificacion"]][$rTMP["id_place"]]["precio_mediana_bajo"] = $rTMP["precio_mediana_bajo"];
            $arrDatosClasificacion[$rTMP["id_clasificacion"]][$rTMP["id_place"]]["precio_mediana_alto"] = $rTMP["precio_mediana_alto"];
            
            $arrDatosClasificacionPadre[$rTMP["id_clasificacion_padre"]][$rTMP["id_place"]]["precio_mediana_bajo"] = $rTMP["precio_mediana_bajo"];
            $arrDatosClasificacionPadre[$rTMP["id_clasificacion_padre"]][$rTMP["id_place"]]["precio_mediana_alto"] = $rTMP["precio_mediana_alto"];
            
            //////////////////////////////////////////////////////////////////////////////////
            //Key Clasificacion    
            //////////////////////////////////////////////////////////////////////////////////  
            if( !isset($arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"]) )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"] = $rTMP["precio_mediana_bajo"];
                
            if( $rTMP["precio_mediana_bajo"] <= $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"] )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"] = $rTMP["precio_mediana_bajo"];
            
            if( !isset($arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"]) )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"] = $rTMP["precio_mediana_alto"];
                
            if( $rTMP["precio_mediana_alto"] >= $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"] )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"] = $rTMP["precio_mediana_alto"];
            
            
            //////////////////////////////////////////////////////////////////////////////////
            //Key Clasificacion    
            //////////////////////////////////////////////////////////////////////////////////  
            if( !isset($arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"]) )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"] = $rTMP["precio_mediana_bajo"];
                
            if( $rTMP["precio_mediana_bajo"] <= $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"] )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"] = $rTMP["precio_mediana_bajo"];
            
            if( !isset($arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"]) )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"] = $rTMP["precio_mediana_alto"];
                
            if( $rTMP["precio_mediana_alto"] >= $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"] )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"] = $rTMP["precio_mediana_alto"];
            
                
            
        }
        $objDBClass->db_free_result($qTMP);
        
        while( $rTMP = each($arrDatosClasificacionRango) ){
            
            $sinMin = $rTMP["value"]["min"];
            $sinMax = $rTMP["value"]["max"];
            
            $arrDatosClasificacionRango[$rTMP["key"]]["precio_min"] = $sinMin; 
            $arrDatosClasificacionRango[$rTMP["key"]]["precio_max"] = $sinMax; 
                
            $sinDiferencia = $sinMax - $sinMin;
                
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrDatosClasificacionRango[$rTMP["key"]]["tipo"] = 1; 
                                                                                   
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["min"] = $sinMin; 
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["max"] = $sinMin + $intRango; 
                           
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["min"] = $sinMin + $intRango + 1; 
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                                
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["max"] = $sinMax; 
                                
            }   
            else{
                
                $arrDatosClasificacionRango[$rTMP["key"]]["tipo"] = $sinMin != $sinMax ? 2 : 3;
                
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["min"] = 0;
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["max"] = 0;
                           
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["min"] = $sinMin;
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["max"] = $sinMax;
                                
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["min"] = 0;
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["max"] = 0;
                
            }
            
        }
        
        while( $rTMP = each($arrDatosClasificacionPadreRango) ){
            
            $sinMin = $rTMP["value"]["min"];
            $sinMax = $rTMP["value"]["max"];
            
            $arrDatosClasificacionPadreRango[$rTMP["key"]]["precio_min"] = $sinMin; 
            $arrDatosClasificacionPadreRango[$rTMP["key"]]["precio_max"] = $sinMax; 
                
            $sinDiferencia = $sinMax - $sinMin;
                
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["tipo"] = 1; 
                                                                                   
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["min"] = $sinMin; 
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["max"] = $sinMin + $intRango; 
                           
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["min"] = $sinMin + $intRango + 1; 
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["max"] = $sinMax; 
                                
            }   
            else{
                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["tipo"] = $sinMin != $sinMax ? 2 : 3;
                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["min"] = 0;
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["max"] = 0;
                           
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["min"] = $sinMin;
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["max"] = $sinMax;
                                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["min"] = 0;
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["max"] = 0;
                
            }
            
        }
        
    }
    
    $arrClasificacionPadreHijoRango = array();
    if( count($arrClasificacionPadreHijo) > 0 ){
        
        $strNombreClasificacionPadreHijo = "";
        while( $rTMP = each($arrClasificacionPadreHijo) ){
            
            $strNombreClasificacionPadreHijo .= ( $strNombreClasificacionPadreHijo == "" ? "" : "," )."'".$rTMP["key"]."'";    
            
        }
        
        $strQuery = "SELECT id_ubicacion_rango_producto,
                            id_ubicacion_lugar,
                            nombre,
                            precio_min,
                            precio_max
                     FROM   ubicacion_rango_producto
                     WHERE  nombre IN ({$strNombreClasificacionPadreHijo})
                     AND    id_ubicacion_lugar IN({$strKeyUbicaionLugar})";
        
        $arrClasificacionPadreHijoRango = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $sinMin = $rTMP["precio_min"];
            $sinMax = $rTMP["precio_max"];
            
            $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["precio_min"] = $sinMin; 
            $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["precio_max"] = $sinMax; 
                
            $sinDiferencia = $sinMax - $sinMin;
                
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["tipo"] = 1; 
                                                                                   
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["min"] = $sinMin; 
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["max"] = $sinMin + $intRango; 
                           
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["min"] = $sinMin + $intRango + 1; 
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["max"] = $sinMax; 
                                
            }   
            else{
                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["tipo"] = $sinMin != $sinMax ? 2 : 3;
                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["min"] = 0;
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["max"] = 0;
                           
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["min"] = $sinMin;
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["max"] = $sinMax;
                                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["min"] = 0;
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["max"] = 0;
                
            }
            
              
                    
        }    
        $objDBClass->db_free_result($qTMP);
        
        
    }
    
    
    $arrResultado = array();
    if( count($arrRetorno) > 0 ){
        
        while( $rTMP = each($arrRetorno) ){
            
            if( $rTMP["value"]["tipo"] == "1" ){
                
                if( isset($arrDatosClasificacion[$rTMP["value"]["identificador"]]) ){
                    
                    $arrRetorno[$rTMP["key"]]["rango"] = $arrDatosClasificacionRango[$rTMP["value"]["identificador"]]; 
                                    
                }
                
            }
            
            if( $rTMP["value"]["tipo"] == "2" ){
                
                if( isset($arrDatosClasificacionPadreRango[$rTMP["value"]["identificador"]]) ){
                    
                    $arrRetorno[$rTMP["key"]]["rango"] = $arrDatosClasificacionPadreRango[$rTMP["value"]["identificador"]]; 
                                    
                }
                
            }
            
            if( $rTMP["value"]["tipo"] == "3" ){
                
                $arrKey = explode("||", $rTMP["key"]);
                $strNombreClasificacionPadreHijo = $arrKey[1];
                
                if( isset($arrClasificacionPadreHijoRango[$strNombreClasificacionPadreHijo]) ){
                    
                    $arrRetorno[$rTMP["key"]]["rango"] = $arrClasificacionPadreHijoRango[$strNombreClasificacionPadreHijo];
                   
                }
                
            }
            
            
        }
        
        
        reset($arrRetorno);
        while( $rTMP = each($arrRetorno) ){
            
            $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
            $arrTMP["identificador_2"] = $rTMP["value"]["tipo"] == "5" || $rTMP["value"]["tipo"] == "4" ? $rTMP["value"]["identificador_2"] : fntCoreEncrypt($rTMP["value"]["identificador_2"]);
            $arrTMP["texto"] = $rTMP["value"]["texto"];
            $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
            $arrTMP["tipo"] = $rTMP["value"]["tipo"];
            $arrTMP["rango"] = "all";
            
            array_push($arrResultado, $arrTMP);
            
            if( isset($rTMP["value"]["rango"]) ){
                
                if( $rTMP["value"]["rango"]["tipo"] == 1 ){
                    
                    
                    //...........Alto
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    
                    if( $boolMovil ){
                        
                        $arrTMP["texto"] = lang["alto"]." Q ".number_format($rTMP["value"]["rango"]["alto"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["alto"]["max"], 2);
                    
                    }
                    else{
                        
                        $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["alto"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["alto"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["alto"]["max"], 2);
                        
                    }
                    
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["rango"] = $rTMP["value"]["rango"]["tipo"]."_A";
                    
                    if( $boolRetornoArray ){
                        
                        $arrTMP["min"] = $rTMP["value"]["rango"]["alto"]["min"];
                        $arrTMP["max"] = $rTMP["value"]["rango"]["alto"]["max"];
                        
                    }
                    
                    array_push($arrResultado, $arrTMP);
                
                
                    //...........Medio
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    if( $boolMovil ){
                        
                        $arrTMP["texto"] = lang["medio"]." Q ".number_format($rTMP["value"]["rango"]["medio"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["medio"]["max"], 2);
                        
                    }
                    else{
                        
                        $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["medio"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["medio"]["max"], 2);
                        
                    }
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["rango"] = $rTMP["value"]["rango"]["tipo"]."_M";
                    
                    if( $boolRetornoArray ){
                        
                        $arrTMP["min"] = $rTMP["value"]["rango"]["medio"]["min"];
                        $arrTMP["max"] = $rTMP["value"]["rango"]["medio"]["max"];
                        
                    }
                    
                    array_push($arrResultado, $arrTMP);
                
                    
                    //...........BAJO
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    if( $boolMovil ){
                        
                        $arrTMP["texto"] = lang["bajo"]." Q ".number_format($rTMP["value"]["rango"]["bajo"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["bajo"]["max"], 2);
                        
                    }
                    else{
                        
                        $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["bajo"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["bajo"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["bajo"]["max"], 2);
                        
                    }
                    
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["rango"] = $rTMP["value"]["rango"]["tipo"]."_B";
                    
                    if( $boolRetornoArray ){
                        
                        $arrTMP["min"] = $rTMP["value"]["rango"]["bajo"]["min"];
                        $arrTMP["max"] = $rTMP["value"]["rango"]["bajo"]["max"];
                        
                    }
                    
                    array_push($arrResultado, $arrTMP);
                                            
                }
                else{
                    
                    //...........Medio
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    if( $boolMovil ){
                        
                        $arrTMP["texto"] = lang["medio"]." Q ".number_format($rTMP["value"]["rango"]["medio"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["medio"]["max"], 2);
                        
                    }
                    else{
                        
                        $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["medio"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["medio"]["max"], 2);
                        
                    }
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["tipo"] = $rTMP["value"]["tipo"];
                    
                    array_push($arrResultado, $arrTMP);
                    
                }
                
            }
        
        }
        
    }
    else{
        
        $arrTMP["identificador"] = "";
        $arrTMP["identificador_2"] = "";
        $arrTMP["texto"] = lang["ver_todos_los_resultados_para"]." {$strParametroBusqueda}...";
        $arrTMP["texto_corto"] = $strParametroBusqueda;
        $arrTMP["tipo"] = "6";
        $arrTMP["rango"] = "all";
        
        array_push($arrResultado, $arrTMP);
        
            
    } 
    
    return $arrResultado;
    
}
               
?>