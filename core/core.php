<?php
header("Content-type: text/html; charset=utf-8");
session_start();
include "core/function_servicio.php";
include "dbClass.php";
define("lang", fntGetDiccionarioInternoIdioma(sesion["lenguaje"]) );

                
//define("lang", fntGetLang( $_SESSION["power_dialer"]["core"]["lenguaje"] ? $_SESSION["power_dialer"]["core"]["lenguaje"] : "en"  ));
                                                        
if( !isset($_SESSION["_open_antigua"]["core"]["login"]) && !isset($_GET["vcard"]) ){
    
     header('Location: login.php');
     ?>
     <script>
        location.href = 'login.php';
     </script>
     <?php
    
}
elseif( isset($_GET["vcard"]) ){

}
                                
function fntDrawHeaderPage(){
    
    
    ?>
    <!doctype html>
    <html lang="en">
        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="shortcut icon" href="dist/images/favicon.ico">
                
            <!-- Material Design for Bootstrap fonts and icons -->
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

            <!-- Material Design for Bootstrap CSS -->
            <link rel="stylesheet" href="dist_interno/bootstrap-material-design-dist/css/bootstrap-material-design.css" >
            

            <title>Inguate</title>
            <style>
                
                body, html {
                    height: 100%;
                }

                .flex-grow {
                    flex: 1 0 auto;
                }

                .bmd-layout-canvas {
                  flex-grow: 1;
                }
                
                .preloader {
                    opacity: 0.5;
                    height: 100%;
                    width: 100%;
                    background: #FFF;
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 9999999;
                }
                 
                .preloader .preloaderdetalle {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    -webkit-transform: translate(-50%, -50%);
                    transform: translate(-50%, -50%);
                    width: 120px;
                }
                
            </style>
            
            
            <script src="dist_interno/js/jquery-3.4.0.min.js" ></script>
            <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
            
            <script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>

            <script src="dist_interno/bootstrap-material-design-dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
            <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
            <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
            
            
            <link href="dist_interno/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" type="text/css">
            <script type="text/javascript" src="https://momentjs.com/downloads/moment.min.js"></script>
            
            
        </head>
        <body id="bodyContenedor" >    

            
    <?php
        //fntDrawHeaderContenido();
    
}

function fntDrawDrawerCore(){
    
    
    ?>  
    
                
    <script src="dist_interno/js/jquery-3.4.0.min.js" ></script>
    <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
    <script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>
    <script src="dist_interno/bootstrap-material-design-dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
    <script src="dist/js/jsAntigua.js" ></script>
    
    <header class="bmd-layout-header" style="background-color: #3f51b5">
        <div class="row ">
            
            <div class="col col-12 text-left ">
            
                <table >
                    <tr>
                        <td style="width: 1%; vertical-align: top;" >
                            <button class="navbar-toggler text-secondary m-0 p-2" style="" type="button" data-toggle="drawer" data-target="#dw-p1">
                                <span class="sr-only">Toggle drawer</span>
                                <i class="material-icons text-white">menu</i>
                            </button>
                            
                        </td> 
                        <td class="text-left" style="width: 1%; vertical-align: middle;" nowrap>
                            
                            <img onclick="location.href = 'index.php' " src="images/LOGO-_INGUATE_WHITE.png" class="img-fluid " style="width: 40%; height: auto; cursor: pointer;" > 
                        
                            <span class="align-middle text-white" onclick="fntCargarPaginaActiva();" style="cursor: pointer">
                                <?php print $_SESSION["_open_antigua"]["core"]["pagina_nombre"];?>
                            </span>    
                        </td> 
                        <td class="text-center" style="width: 97%; vertical-align: middle;" id="tdCoreBarraMedio">
                            
                                                         
                                    
                            
                        </td>
                        <td style="vertical-align: middle; width: 1%;">
                            <div class="dropdown pull-xs-right m-1">
                              <button class="btn bmd-btn-icon " type="button" id="lr1" onclick="fntCerrarSesion();" >
                                <i class="material-icons text-white">power_settings_new</i>
                              </button>
                            </div>
                        </td>
                    </tr>
                </table>
                            
            </div>  
        </div>
        <div id="tdCoreBarraMedio_movil">
            
        </div>            
    </header>
    
    <div id="dw-p1" class="bmd-layout-drawer bg-faded">
        <header>
            <a class="navbar-brand">
                
                <img src="images/LogoInguateAZUL.png" class="img-fluid " style="" >
        
            
            </a>
            
        </header>
        <ul class="list-group">
            <?php
            
            if( $_SESSION["_open_antigua"]["core"]["tipo"] == 1 ){
                
                ?>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "registro" ? "active" : "";?> " onclick="fntCargarContenidoPagina('registro', 'Usuarios');">Usuarios</a>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "categoria" ? "active" : "";?> " onclick="fntCargarContenidoPagina('categoria', 'Categorias');">Categorias</a>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "amenidad" ? "active" : "";?> " onclick="fntCargarContenidoPagina('amenidad', 'Amenidades');">Amenidades</a>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "sol_place" ? "active" : "";?> " onclick="fntCargarContenidoPagina('sol_place', 'Solicitud Place');">Solicitud Place</a>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "sol_categoria_especial" ? "active" : "";?> " onclick="fntCargarContenidoPagina('sol_categoria_especial', 'Solicitud Categoria Especial');">Solicitud Categoria Especial</a>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "categoria_especial" ? "active" : "";?> " onclick="fntCargarContenidoPagina('categoria_especial', 'Categoria Especial');">Categoria Especial</a>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "lugar_dato" ? "active" : "";?> " onclick="fntCargarContenidoPagina('lugar_dato', 'Lugar Datos');">Lugar Datos</a>
            
                <?php
                
            }
            else{
                
                ?>
                <a href="javascript:void(0)" class="list-group-item <?php print $_SESSION["_open_antigua"]["core"]["pagina"] == "place" ? "active" : "";?> " onclick="fntCargarContenidoPagina('place', '<?php print lang["tu_negocio"]?>');"><?php print lang["tu_negocio"]?></a>
            
                <?php
                
            }
            
            ?>
                
        </ul>
    </div>
    <script>
        $(document).ready(function() { 
            $('body').bootstrapMaterialDesign(); 
        });
        
        function fntCerrarSesion(){
                
            $.ajax({
                
                url: "login.php?logout=true", 
                success: function(result){
                    
                    location.href = "index.php";
                    
                }
                
            });    
            
        }
    </script>
    <?php
}

function fntDrawHeaderContenido(){
    
    ?>
    
    <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
        <?php 
        fntDrawDrawerCore();
        ?>    
        
        <!-- Contenidi por Pagina  -->
        <main class="bmd-layout-content bg-dark" >
            <div class="container-fluid bg-danger " >
                
            <div class="row justify-content-md-center">
                <div class="col col-lg-2">
                    <button type="button" class="btn btn-raised btn-dark" data-toggle="modal" data-target="#exampleModal">
                      Launch demo modal
                    </button>
                    <button type="button" class="btn btn-secondary" data-timeout="1000" data-toggle="snackbar" data-content="Free fried chicken here! <a href='https://example.org' class='btn btn-info'>Check it out</a>" data-html-allowed="true" data-timeout="0">
                        Snackbar
                    </button>
                </div>
                <div class="col-md-auto">
                    Variable width content
                </div>
                <div class="col col-lg-2">
                    3 of 3
                </div>
                </div>
                <div class="row">
                <div class="col">
                1 of 3
                </div>
                <div class="col-md-auto">
                Variable width content
                </div>
                <div class="col col-lg-2">
                3 of 3
                </div>
            </div>
            
              
              
              
            </div> 
        </main>
      
        <!-- Espacio para Modal  -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
      
    </div>    
    <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
            
    <?php
    
}

function fntDrawFooterPage(){
    
    ?>
            <div class="preloader">
                <div class="preloaderdetalle">
                    <img src="dist/images/30.gif" alt="NILA">
                </div>
            </div> 
            <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
            
            <script>
                $(".preloader").fadeOut();       
                var PaginaActiva;   
                var PaginaActivaNombre;
                    
                function fntCargarContenidoPagina(strPagina, strNombrePagina, boolNoContenido){
                    
                    var boolNoContenidoo = !boolNoContenido ? false : boolNoContenido;
                    
                    $("#coreA_"+PaginaActiva).removeClass("active");
                    
                    PaginaActiva = strPagina;   
                    PaginaActivaNombre = strNombrePagina;
                    
                    $("#coreA_"+strPagina).addClass("active");
                    
                    
                    $(".preloader").fadeIn();
                    $.ajax({
                        url: "servicio_core.php?servicio=drawContenidoPaginaCore&pagina="+strPagina+"&nombre="+strNombrePagina+"&boolContenido="+!boolNoContenidoo, 
                        success: function(result){
                            
                            $(".preloader").fadeOut();
                            
                            if( !boolNoContenidoo ){
                                
                                //$("#h3CoreNamePagina").html(strNombrePagina);
                                
                                $("#bodyContenedor").html(result);
                                
                            }
                                
                        }
                    });
                    
                }
                
                function fntCargarPaginaActiva(){
            
                    fntCargarContenidoPagina(PaginaActiva, PaginaActivaNombre);
            
                    
                }
            </script>
            
        </body>
    </html>
    <?php
        
}

?>