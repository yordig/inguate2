<?php

$arr["en"]["bajo"] = "low";
$arr["es"]["bajo"] = "Bajo";

$arr["en"]["medio"] = "Medium";
$arr["es"]["medio"] = "Medio";

$arr["en"]["alto"] = "High";
$arr["es"]["alto"] = "Alto";

$arr["en"]["gratis"] = "Free";
$arr["es"]["gratis"] = "Gratis";

$arr["en"]["barato"] = "Inexpensive";
$arr["es"]["barato"] = "Barato";

$arr["en"]["moderado"] = "Moderate";
$arr["es"]["moderado"] = "Moderado";

$arr["en"]["costoso"] = "Expensive";
$arr["es"]["costoso"] = "Costoso";

$arr["en"]["muy_caro"] = "Very Expensive";
$arr["es"]["muy_caro"] = "Muy caro";

$arr["en"]["costo"] = "Cost";
$arr["es"]["costo"] = "Costo";

$arr["en"]["textoPlaceHolderAutocomplete"] = "What are you looking for?";
$arr["es"]["textoPlaceHolderAutocomplete"] = "¿Qué estás buscando?";
         
$arr["en"]["textoPlaceHolderAutocomplete_movil"] = "What are you looking for?";
$arr["es"]["textoPlaceHolderAutocomplete_movil"] = "¿Qué buscas?";
         
$arr["en"]["diviertete"] = "Have fun";
$arr["es"]["diviertete"] = "Diviertete";
         
$arr["en"]["te_ayudamos_descubrir"] = "We help you discover";
$arr["es"]["te_ayudamos_descubrir"] = "Te ayudamos a descubrir";
         
$arr["en"]["eventos_proximos"] = "Upcoming Events";
$arr["es"]["eventos_proximos"] = "Eventos Próximos";
         
$arr["en"]["los_mejores_eventos"] = "The Best Events";
$arr["es"]["los_mejores_eventos"] = "Los mejores eventos";
         
$arr["en"]["escoge_tu_rango_precio"] = "choose your price range";
$arr["es"]["escoge_tu_rango_precio"] = "escoge tu rango de precio";
         
$arr["en"]["experiencias_riquisimas"] = "Rich Experiences";
$arr["es"]["experiencias_riquisimas"] = "Experiencias Riquisimas";
         
$arr["en"]["deliciosos"] = "Delicious";
$arr["es"]["deliciosos"] = "Deliciosos";
         
$arr["en"]["sabroso"] = "Tasty";
$arr["es"]["sabroso"] = "Sabroso";
         
$arr["en"]["rico"] = "Rich";
$arr["es"]["rico"] = "Rico";
         
$arr["en"]["descuentos_ofertas"] = "Discounts and Offers";
$arr["es"]["descuentos_ofertas"] = "Descuentos y Ofertas";
         
$arr["en"]["descubre_guatemala"] = "Discover Guatemala";
$arr["es"]["descubre_guatemala"] = "Descubre Guatemala";
         
$arr["en"]["lugares_actividades_emocionantes"] = "Exciting places and activities";
$arr["es"]["lugares_actividades_emocionantes"] = "Lugares y actividades emocionantes";
         
$arr["en"]["escoge_tu_rango_precio"] = "choose your price range";
$arr["es"]["escoge_tu_rango_precio"] = "escoge tu rango de precio";
         
$arr["en"]["quedate_conmigo"] = "Stay with me";
$arr["es"]["quedate_conmigo"] = "Quedate conmigo";
         
$arr["en"]["para_vestirse_bien"] = "To dress well";
$arr["es"]["para_vestirse_bien"] = "Para vestirse bien";
         
$arr["en"]["todos_los_derechos_reservados"] = "All rights reserved";
$arr["es"]["todos_los_derechos_reservados"] = "Todos los derechos reservados";
         
$arr["en"]["tienes_un_negocio"] = "Do you have a Business?";
$arr["es"]["tienes_un_negocio"] = "Tiene un Negocio?";

$arr["en"]["iniciar_sesion_registro"] = "Login | Registry";
$arr["es"]["iniciar_sesion_registro"] = "Iniciar Sesión | Registro";

$arr["en"]["perfil"] = "Profile";
$arr["es"]["perfil"] = "Perfil";

$arr["en"]["cerrar_sesion"] = "Sign off";
$arr["es"]["cerrar_sesion"] = "Cerrar Sesión";

$arr["en"]["ubicacion"] = "Location";
$arr["es"]["ubicacion"] = "Ubicación";

$arr["en"]["idioma"] = "Language";
$arr["es"]["idioma"] = "Idioma";

$arr["en"]["mi_negocio"] = "My business";
$arr["es"]["mi_negocio"] = "Mi Negocio";

$arr["en"]["hola"] = "Hi";
$arr["es"]["hola"] = "Hola";

$arr["en"]["espaniol"] = "Spanish";
$arr["es"]["espaniol"] = "Español";

$arr["en"]["ingles"] = "English";
$arr["es"]["ingles"] = "Ingles";

$arr["en"]["alertas"] = "Alerts";
$arr["es"]["alertas"] = "Alertas";

$arr["en"]["todos"] = "All";
$arr["es"]["todos"] = "Todos";

$arr["en"]["abierto_ahora"] = "Open Now";
$arr["es"]["abierto_ahora"] = "Abierto Ahora";

$arr["en"]["cerrado"] = "Closed";
$arr["es"]["cerrado"] = "Cerrado";

$arr["en"]["error_completa_los_datos"] = "Error Complete the data";
$arr["es"]["error_completa_los_datos"] = "Error Completa los datos";


$arr["en"]["codigo_confirmacion_error"] = "Incorrect Confirmation Code.";
$arr["es"]["codigo_confirmacion_error"] = "Código de Confirmación Incorrecto.";


$arr["en"]["error_captcha"] = "Incorrect Data, validation CAPTCHA invalidates.";
$arr["es"]["error_captcha"] = "Datos Incorrectos, validacion CAPTCHA invalida.";


$arr["en"]["datos_error_1"] = "Incorrect Data, complete the data.";
$arr["es"]["datos_error_1"] = "Datos Incorrectos, completa los datos.";


$arr["en"]["mismo_email"] = "Email registered previously.";
$arr["es"]["mismo_email"] = "Email registrado anteriormente.";


$arr["en"]["hola"] = "Hi";
$arr["es"]["hola"] = "Hola";


$arr["en"]["codigo_confirmacion_msn_1"] = "Your confirmation code is";
$arr["es"]["codigo_confirmacion_msn_1"] = "tu codigo de confirmacion es";


$arr["en"]["usuario_clave_error"] = "Incorrect username or password";
$arr["es"]["usuario_clave_error"] = "Usuario o contraseña incorrectas";

$arr["en"]["verifica_tu_cuenta"] = "Verify your account";
$arr["es"]["verifica_tu_cuenta"] = "Verifica tu cuenta";

$arr["en"]["verifica_tu_cuenta_inguate"] = "Verify your Inguate account";
$arr["es"]["verifica_tu_cuenta_inguate"] = "Verifica tu cuenta Inguate";

$arr["en"]["localiza_tu_negocio"] = "Locate your business";
$arr["es"]["localiza_tu_negocio"] = "Localiza tu negocio";

$arr["en"]["nombre_tu_negocio"] = "Name of your business";
$arr["es"]["nombre_tu_negocio"] = "Nombre de tu negocio";

$arr["en"]["informacion_de_tu_negocio"] = "Information of your business";
$arr["es"]["informacion_de_tu_negocio"] = "Información de tu negocio";

$arr["en"]["nombre_de_tu_negocio"] = "Name of your Business";
$arr["es"]["nombre_de_tu_negocio"] = "Nombre de tu Negocio";

$arr["en"]["que_hace_tu_negocio"] = "What does your business do?";
$arr["es"]["que_hace_tu_negocio"] = "¿Qué hace tu negocio?";

$arr["en"]["seleccione_una_opcion"] = "Select an option ...";
$arr["es"]["seleccione_una_opcion"] = "Seleccione una opción...";

$arr["en"]["clasificacion_principal"] = "Main Classification";
$arr["es"]["clasificacion_principal"] = "Clasificación Principal";

$arr["en"]["regresar"] = "Back";
$arr["es"]["regresar"] = "Regresar";

$arr["en"]["siguiente"] = "Next";
$arr["es"]["siguiente"] = "Siguiente";

$arr["en"]["region"] = "Region";
$arr["es"]["region"] = "Región";

$arr["en"]["clasificacion_secundaria"] = "Secondary Classification";
$arr["es"]["clasificacion_secundaria"] = "Clasificación secundaria";

$arr["en"]["tu_negocio"] = "Your business";
$arr["es"]["tu_negocio"] = "Tu Negocio";

$arr["en"]["completa_los_datos_obligatorios"] = "Complete the required information";
$arr["es"]["completa_los_datos_obligatorios"] = "Completa los datos Obligatorios";

$arr["en"]["advertencia"] = "Warning";
$arr["es"]["advertencia"] = "Advertencia";

$arr["en"]["alerta_negocio_publicado"] = "Continue completing the information and submit the request.";
$arr["es"]["alerta_negocio_publicado"] = "Continúa completando la información para el registro y envia la solicitud.";

$arr["en"]["alerta_negocio_rechazo"] = "   Your business has been rejected, reason:";
$arr["es"]["alerta_negocio_rechazo"] = "Tu negocio ha sido rechazado, motivo:";

$arr["en"]["galeria"] = "Gallery";
$arr["es"]["galeria"] = "Galería";

$arr["en"]["anadir_imagen"] = "Añadir imagen";
$arr["es"]["anadir_imagen"] = "Add Image";

$arr["en"]["cerrar"] = "Close";
$arr["es"]["cerrar"] = "Cerrar";

$arr["en"]["guardar"] = "Save";
$arr["es"]["guardar"] = "Guardar";

$arr["en"]["alerta_solo_imagenes"] = "The only accepted formats are: .png .jpg .jpeg";
$arr["es"]["alerta_solo_imagenes"] = "Los únicos formatos aceptados son: .png .jpg .jpeg";

$arr["en"]["img_minimo_dimesion"] = "The minimum dimensions are 900 x 600";
$arr["es"]["img_minimo_dimesion"] = "Las dimensiones mínimas son 900 x 600.";

$arr["en"]["alerta_maximo_imagenes_1"] = "Have a total of";
$arr["es"]["alerta_maximo_imagenes_1"] = "Tienes un total de";

$arr["en"]["alerta_maximo_imagenes_2"] = "photos configured for your gallery";
$arr["es"]["alerta_maximo_imagenes_2"] = "fotos configuradas para tu galería";

$arr["en"]["datos_principales"] = "Main data";
$arr["es"]["datos_principales"] = "Datos principales";

$arr["en"]["titulo"] = "Title";
$arr["es"]["titulo"] = "Titulo";

$arr["en"]["descripcion"] = "Description";
$arr["es"]["descripcion"] = "Descripción";

$arr["en"]["telefono"] = "Phone";
$arr["es"]["telefono"] = "Teléfono";

$arr["en"]["direccion"] = "Direction";
$arr["es"]["direccion"] = "Dirección";

$arr["en"]["horario"] = "Schedule";
$arr["es"]["horario"] = "Horario";

$arr["en"]["domingo"] = "Sunday";
$arr["es"]["domingo"] = "Domingo";

$arr["en"]["lunes"] = "Monday";
$arr["es"]["lunes"] = "Lunes";

$arr["en"]["martes"] = "Tuesday";
$arr["es"]["martes"] = "Martes";

$arr["en"]["miercoles"] = "Wednesday";
$arr["es"]["miercoles"] = "Miércoles";

$arr["en"]["jueves"] = "Thursday";
$arr["es"]["jueves"] = "Jueves";

$arr["en"]["viernes"] = "Friday";
$arr["es"]["viernes"] = "Viernes";

$arr["en"]["sabado"] = "Saturday";
$arr["es"]["sabado"] = "Sábado";

$arr["en"]["amenidades"] = "Amenities";
$arr["es"]["amenidades"] = "Amenidades";

$arr["en"]["solicitud"] = "Request";
$arr["es"]["solicitud"] = "Solicitud";

$arr["en"]["motivo_rechazo"] = "Reason for rejection:";
$arr["es"]["motivo_rechazo"] = "Motivo de Rechazo:";

$arr["en"]["codigo_vendedor"] = "Seller Code";
$arr["es"]["codigo_vendedor"] = "Código Vendedor";

$arr["en"]["nombre_completo_propietario"] = "Full Name Owner";
$arr["es"]["nombre_completo_propietario"] = "Nombre completo propietario";

$arr["en"]["telefono_propietario"] = "Owner phone";
$arr["es"]["telefono_propietario"] = "Teléfono propietario";

$arr["en"]["email_propietario"] = "Email Owner";
$arr["es"]["email_propietario"] = "Email propietario";

$arr["en"]["tipo_de_negocio"] = "Business Type";
$arr["es"]["tipo_de_negocio"] = "Tipo de negocio";

$arr["en"]["foto_patente"] = "Photo or PDF of Commercial Patent";
$arr["es"]["foto_patente"] = "Foto o PDF de Patente de Comercio";

$arr["en"]["foto_sa"] = "Photo or PDF of S.A.";
$arr["es"]["foto_sa"] = "Foto o PDF de S.A.";

$arr["en"]["enviar_solicitud"] = "Send request";
$arr["es"]["enviar_solicitud"] = "Enviar solicitud";

$arr["en"]["aviso_envio_sol_place"] = "Your request has been submitted and is being processed.";
$arr["es"]["aviso_envio_sol_place"] = "Tu solicitud ha sido enviada y está siendo procesada.";

$arr["en"]["listo"] = "Ready";
$arr["es"]["listo"] = "Listo";

$arr["en"]["codigo_venta_no_valido"] = "Invalid Code (non mandatory field)";
$arr["es"]["codigo_venta_no_valido"] = "Codigo Invalido (campo no obligatorio)";

$arr["en"]["asesor_de_ventas"] = "Sales Advisor:";
$arr["es"]["asesor_de_ventas"] = "Asesor de ventas:";

$arr["en"]["productos"] = "Products";
$arr["es"]["productos"] = "Productos";

$arr["en"]["nombre"] = "Name";
$arr["es"]["nombre"] = "Nombre";

$arr["en"]["precio"] = "Price";
$arr["es"]["precio"] = "Precio";

$arr["en"]["clasificacion"] = "Classification";
$arr["es"]["clasificacion"] = "Clasificación";

$arr["en"]["eliminar"] = "Remove";
$arr["es"]["eliminar"] = "Eliminar";

$arr["en"]["editar"] = "Edit";
$arr["es"]["editar"] = "Editar";

$arr["en"]["agregar_producto"] = "Add product";
$arr["es"]["agregar_producto"] = "Agregar producto";

$arr["en"]["agregar_producto_especial"] = "Add Special Product";
$arr["es"]["agregar_producto_especial"] = "Agregar Producto Especial";

$arr["en"]["alerta_pedirContacto_misUsuario"] = "Get in touch to see the information of your users";
$arr["es"]["alerta_pedirContacto_misUsuario"] = "Ponte en contacto para ver la informacion de tus usuarios";

$arr["en"]["mis_usuario"] = "My Users";
$arr["es"]["mis_usuario"] = "Mis Usuarios";

$arr["en"]["visitas_usuarios"] = "User visits";
$arr["es"]["visitas_usuarios"] = "Visitas de usuarios";

$arr["en"]["estado"] = "Status";
$arr["es"]["estado"] = "Estado";

$arr["en"]["sol_categoria_especial"] = "Special Category Application";
$arr["es"]["sol_categoria_especial"] = "Solicitud categoría especial";

$arr["en"]["categoria_especial"] = "Special Category";
$arr["es"]["categoria_especial"] = "Categoría Especial";

$arr["en"]["fecha_inicio"] = "Start date";
$arr["es"]["fecha_inicio"] = "Fecha Inicio";

$arr["en"]["fecha_fin"] = "End date";
$arr["es"]["fecha_fin"] = "Fecha Fin";

$arr["en"]["selecciona_tu_negocio"] = "Select your Business ...";
$arr["es"]["selecciona_tu_negocio"] = "Selecciona tu Negocio...";

$arr["en"]["selecciona_una_clasificacion"] = "Select a Classification ...";
$arr["es"]["selecciona_una_clasificacion"] = "Selecciona una Clasificacion...";

$arr["en"]["selecciona_tu_frecuencia"] = "Select your frequency ...";
$arr["es"]["selecciona_tu_frecuencia"] = "Selecciona tu frecuencia...";

$arr["en"]["motivo"] = "Reason";
$arr["es"]["motivo"] = "Motivo";

$arr["en"]["motivo_rechazo"] = "Reason for rejection";
$arr["es"]["motivo_rechazo"] = "Motivo de Rechazo";

$arr["en"]["enviar_cambios_por_rechazo"] = "Send Changes by Rejection";
$arr["es"]["enviar_cambios_por_rechazo"] = "Enviar Cambios por Rechazo";

$arr["en"]["enviar_solicitud"] = "Send request";
$arr["es"]["enviar_solicitud"] = "Enviar Solicitud";

$arr["en"]["alerta_registro_negocio_1"] = "Do you want to register your business so they can meet in Inguate?";
$arr["es"]["alerta_registro_negocio_1"] = "Deseas registrar tu negocio para que puedan encontrarse en Inguate?";

$arr["en"]["administrador_de_negocio"] = "Business manager";
$arr["es"]["administrador_de_negocio"] = "Administrador de negocio";

$arr["en"]["datos_guardados"] = "Saved data";
$arr["es"]["datos_guardados"] = "Datos guardados";

$arr["en"]["informacion_general"] = "General information";
$arr["es"]["informacion_general"] = "información general";

$arr["en"]["edicion_de_pagina"] = "Page Edit";
$arr["es"]["edicion_de_pagina"] = "Edición de página";

$arr["en"]["verificacion_de_negocio"] = "Business verification";
$arr["es"]["verificacion_de_negocio"] = "Verificación de negocio";

$arr["en"]["editar_pagina"] = "Edit page";
$arr["es"]["editar_pagina"] = "Editar página";

$arr["en"]["visitas"] = "visits";
$arr["es"]["visitas"] = "Visitas";

$arr["en"]["creado"] = "Created";
$arr["es"]["creado"] = "Creado";

$arr["en"]["activo"] = "Active";
$arr["es"]["activo"] = "Activo";

$arr["en"]["verificando"] = "Verifying";
$arr["es"]["verificando"] = "Verificando";

$arr["en"]["rechazada"] = "Rejected";
$arr["es"]["rechazada"] = "Rechazada";

$arr["en"]["pendiente_de_pago"] = "Outstanding";
$arr["es"]["pendiente_de_pago"] = "Pendiente de Pago";

$arr["en"]["error_codigo_sms"] = "Incorrect code";
$arr["es"]["error_codigo_sms"] = "Código incorrecto";

$arr["en"]["ver_mas_detalles"] = "See more details";
$arr["es"]["ver_mas_detalles"] = "Ver más detalles";

$arr["en"]["producto_especial"] = "Special product";
$arr["es"]["producto_especial"] = "Producto especial";

$arr["en"]["todas_la_categorias"] = "All Categories";
$arr["es"]["todas_la_categorias"] = "Todas la categorías";

$arr["en"]["categorias"] = "Categories";
$arr["es"]["categorias"] = "Categorías";

$arr["en"]["add_your_comment"] = "Add your comment";
$arr["es"]["add_your_comment"] = "Añade tu comentario";

$arr["en"]["gracias_por_comentario"] = "Thank you for giving us your comment.";
$arr["es"]["gracias_por_comentario"] = "Gracias por darnos tu comentario";

$arr["en"]["nuevo_negocio"] = "New business";
$arr["es"]["nuevo_negocio"] = "Nuevo negocio";

$arr["en"]["opiniones"] = "Opinions";
$arr["es"]["opiniones"] = "Opiniones";

$arr["en"]["comentar"] = "Comment";
$arr["es"]["comentar"] = "Comentar";

$arr["en"]["agregar_producto_con_pagina"] = "Add Product with page";
$arr["es"]["agregar_producto_con_pagina"] = "Agregar Producto con página";

$arr["en"]["negocio"] = "Business";
$arr["es"]["negocio"] = "Negocio";

$arr["en"]["email_no_registrado"] = "Email not registered";
$arr["es"]["email_no_registrado"] = "Email no registrado";

$arr["en"]["contactanos"] = "Contact us";
$arr["es"]["contactanos"] = "Contactanos";

$arr["en"]["error_titulo_negocio_no"] = "You cannot use this title for your business change it and try again";
$arr["es"]["error_titulo_negocio_no"] = "No puedes utilizar este título para tu negocio cambialo e intenta nuevamente";

$arr["en"]["pregunta_eliminar_producto"] = "Are you sure you want to remove this product?";
$arr["es"]["pregunta_eliminar_producto"] = "Estas seguro de eliminar este producto?";

$arr["en"]["ver_pagina_publica"] = "See public page";
$arr["es"]["ver_pagina_publica"] = "Ver página pública";

$arr["en"]["intente nuevamente"] = "Try again";
$arr["es"]["intente_nuevamente"] = "Intente nuevamente";

$arr["en"]["ver_todos_los_resultados_para"] = "See all results for";
$arr["es"]["ver_todos_los_resultados_para"] = "Ver todos los resultados para";

$arr["en"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";
$arr["es"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";

$arr["en"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";
$arr["es"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";

$arr["en"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";
$arr["es"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";

$arr["en"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";
$arr["es"]["FFFFFFFFFFFFFF"] = "FFFFFFFFFFFFFF";






?>