<?php
  

class dbClass{
    
    var $conexion;
                    
    public function __construct(){
        
        //include "config/config.php";
        
        $this->conexion = new mysqli('localhost', 'root', '', 'inguate_produccion');
        //$this->conexion = new mysqli('35.192.81.156', 'root', 'powerdialer_abc123', 'inguate'); //Produccion
        //$this->conexion = new mysqli('35.226.90.3', 'power', '123', 'intercorps-1208:us-central1:powerdialer');
        //$this->conexion = new mysqli('localhost', 'intehoho_power', 'power_123', 'intehoho_power');
	
        if (!$this->conexion->set_charset("utf8")) {
            //printf("Error cargando el conjunto de caracteres utf8: %s\n", $this->conexion->error);
            //exit();
        } else {
            //printf("Conjunto de caracteres actual: %s\n", $this->conexion->character_set_name());
        }
        if ($this->conexion->connect_error) {

            die('Connect Error (' . $this->conexion->connect_errno . ') '
                    . $this->conexion->connect_error);
        }
        
    }
    
    public function db_consulta($strQuery){
                        
        //$strQuery = utf8_encode($strQuery);
        $resultado = $this->conexion->query($strQuery);
        if(!$resultado){
            
            $strError = "<pre>LOCALHOST Ha ocurrido un error intente nuevamente:  <br> Query:  <br>".$strQuery." <br> Error: <br>".$this->conexion->error."</pre>";
            
            $str = "";
                
            $arrTraza = debug_backtrace();
            
            if( count($arrTraza) > 0 ){
                
                while( $rTMP = each($arrTraza) ){
                
                    $str .= "Archivo: {$rTMP["value"]["file"]} [{$rTMP["value"]["line"]}], function: {$rTMP["value"]["function"]}, class: {$rTMP["value"]["class"]} <br><br>".json_encode($rTMP["value"]["object"])."<br>________________________________________<br>";
                    
                }
                    
            }
            
            
            //fntEnviarCorreo("yordi@inguate.com", "Error Inguate DB LOCALHOST", $strError."<br><br><br>".$str);            
            
            print $strError."<pre>".debug_print_backtrace()."</pre>";         
            
            //$strTrace = debug_backtrace();
            
            
            
            return null;
            
        }
        else
        {                 
            return $resultado;
        }
        
    }
    
    public function db_fetch_array($qTMP){
    
        if( $qTMP != null )
            return $qTMP->fetch_assoc();
        else
            return null;
    }
    
    public function db_free_result($qTMP){
    
        if( $qTMP != null )
        return $qTMP->free();

    }
    
    public function db_close(){
    
        return $this->conexion->close();

    }
    
    public function getConexion(){
    
        return $this->conexion;

    }
    
    public function db_last_id(){
        
        $strQuery = "SELECT LAST_INSERT_ID() id";
        
        $qTMP = $this->db_fetch_array($this->db_consulta($strQuery));
        
        return intval($qTMP["id"]);
        
    }
    
    
    
}

  
?>
