<?php
session_start();

include "core/function_servicio.php";

$intUsuarioVisita = isset($_GET["p"]) ? intval(fntCoreDecrypt($_GET["p"])) : 0;

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$strLenguaje = isset(sesion["lenguaje"]) ? sesion["lenguaje"] : "es";

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
 
if( isset($_GET["getShowPregunta"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();    
    
    $boolGlobalNextPregunta = true;
    $intCountPreguntas = 3;
    
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
    $strQuery = "SELECT MAX(pregunta.orden) orden
                 FROM   usuario_pregunta_respuesta,
                        pregunta_respuesta,
                        pregunta
                 WHERE  usuario_pregunta_respuesta.id_usuario = {$intIdUsuario}
                 AND    usuario_pregunta_respuesta.id_pregunta_respuesta = pregunta_respuesta.id_pregunta_respuesta 
                 AND    pregunta_respuesta.id_pregunta = pregunta.id_pregunta
                 ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    $intOrdenPregunta = isset($rTMP["orden"]) && intval($rTMP["orden"]) ? $rTMP["orden"] : 0;
    
    $arr["tipo"] = 0;
    $arr["count_preguntas"] = 0;
    $arr["preguntar"] = false;
        
    if( !$intOrdenPregunta ){
        
        $arr["tipo"] = 1;
        $arr["count_preguntas"] = 2;
        $arr["preguntar"] = true;
    
    }
    elseif( $boolGlobalNextPregunta ){
        
        $intOrdenPregunta++;
    
        $strQuery = "SELECT pregunta.id_pregunta
                     FROM   pregunta
                     WHERE  pregunta.orden = {$intOrdenPregunta}
                     
                     ";
        $arrPregunta = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);    
        
        $intPregunta = isset($rTMP["id_pregunta"]) ? intval($rTMP["id_pregunta"]) : 0;
        
        $arr["tipo"] = 2;
        $arr["count_preguntas"] = $intCountPreguntas;
        $arr["preguntar"] = $intPregunta ? true : false;
        
                
    }
    
    $objDBClass->db_close();
    
    print json_encode($arr); 
    
    die();
}

if( isset($_GET["getIssetNextPreguntar"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();    
    
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
    $intOrdenPregunta = 1;
    
    $strQuery = "SELECT MAX(pregunta.orden) orden
                 FROM   usuario_pregunta_respuesta,
                        pregunta_respuesta,
                        pregunta
                 WHERE  usuario_pregunta_respuesta.id_usuario = {$intIdUsuario}
                 AND    usuario_pregunta_respuesta.id_pregunta_respuesta = pregunta_respuesta.id_pregunta_respuesta 
                 AND    pregunta_respuesta.id_pregunta = pregunta.id_pregunta
                 ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    $intOrdenPregunta = isset($rTMP["orden"]) && intval($rTMP["orden"]) ? $rTMP["orden"] : 0;
    
    $intOrdenPregunta++;
    
    $strQuery = "SELECT pregunta.id_pregunta
                 FROM   pregunta
                 WHERE  pregunta.orden = {$intOrdenPregunta}
                 
                 ";
    $arrPregunta = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);    
    
    $intPregunta = isset($rTMP["id_pregunta"]) ? intval($rTMP["id_pregunta"]) : 0;
    
    $arr["showPregunta"] = $intPregunta ? true : false;
    $arr["pregunta"] = fntCoreEncrypt($intPregunta);
    
    print json_encode($arr); 
    
    die();
}
  
if( isset($_GET["setRespuesta"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $intRespuesta = isset($_GET["res"]) ? intval(fntCoreDecrypt($_GET["res"])) : 0;
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
    $sinPorcentajeMinimoMatch = 50;        
    
    $strQuery = "INSERT INTO usuario_pregunta_respuesta(id_usuario, id_pregunta_respuesta)
                                                VALUES({$intIdUsuario}, {$intRespuesta})";    

    $objDBClass->db_consulta($strQuery);
    
    //$intIdUsuario = 63;
    $strQuery = "SELECT id_pregunta_respuesta
                 FROM   usuario_pregunta_respuesta   
                 WHERE  id_usuario = {$intIdUsuario} ";
    
    $arrRespuestaUsuario = array();
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrRespuestaUsuario[$rTMP["id_pregunta_respuesta"]] = $rTMP["id_pregunta_respuesta"];
                
    }
    $objDBClass->db_free_result($qTMP);
    
    if( count($arrRespuestaUsuario) > 0 ){
        
        $strRespuestaUsuario = implode(',', $arrRespuestaUsuario);
        
        $strQuery = "SELECT id_usuario, 
                            id_pregunta_respuesta
                     FROM   usuario_pregunta_respuesta   
                     WHERE  id_pregunta_respuesta IN({$strRespuestaUsuario})
                     AND    id_usuario != {$intIdUsuario}
                     ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrRespuestaUsuarioOtros[$rTMP["id_usuario"]]["id_usuario"] = $rTMP["id_usuario"];
            $arrRespuestaUsuarioOtros[$rTMP["id_usuario"]]["respuesta"][$rTMP["id_pregunta_respuesta"]] = $rTMP["id_pregunta_respuesta"];
                    
        }
        $objDBClass->db_free_result($qTMP);
        
        if( count($arrRespuestaUsuarioOtros) > 0 ){
            
            
            $arrConexionUsuario = array();
            while( $rTMP = each($arrRespuestaUsuarioOtros) ){
                
                $intCountMatch = 0;
                
                $intCantidaRespuestaUsuario = count($arrRespuestaUsuario);
                
                if( count($rTMP["value"]["respuesta"]) > $intCantidaRespuestaUsuario ){
                    
                    $intCantidaRespuestaUsuario = count($rTMP["value"]["respuesta"]);
                
                        
                }    
                
                while( $arrTMP = each($rTMP["value"]["respuesta"]) ){
                    
                    if( isset($arrRespuestaUsuario[$arrTMP["key"]])   ){
                        
                        $intCountMatch++;
                    }
                    
                    
                }
                
                $sinPorcentaje = ( $intCountMatch / $intCantidaRespuestaUsuario ) * 100;
                
                //drawdebug($rTMP["key"]. " -> March:".$intCountMatch." de:".$intCantidaRespuestaUsuario." = ".$sinPorcentaje);
                
                $strConexion = "N";
                if( $sinPorcentaje >= $sinPorcentajeMinimoMatch ){
                    
                    $strConexion = "Y";
                                        
                }
                
                $arrConexionUsuario[$rTMP["key"]]["conexion"] = $strConexion;                    
                    
                
            }
            
            if( count($arrConexionUsuario)  > 0 ){
                
                $strQuery = "SELECT id_usuario_amigo
                             FROM   usuario_amigo
                             WHERE  id_usuario = {$intIdUsuario}";
                $arrListaAmigo = array();
                $qTMP = $objDBClass->db_consulta($strQuery);
                while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                    
                    $arrListaAmigo[$rTMP["id_usuario_amigo"]] = $rTMP["id_usuario_amigo"];
                    
                }
                $objDBClass->db_free_result($qTMP);
                
                
                while( $rTMP = each($arrConexionUsuario) ){
                    
                    if( $rTMP["value"]["conexion"] == "Y" ){
                        
                        if( !isset($arrListaAmigo[$rTMP["key"]]) ){
                            
                            $strQuery = "INSERT INTO usuario_amigo(id_usuario, id_usuario_amigo)
                                                            VALUES({$intIdUsuario}, {$rTMP["key"]})";
                            $objDBClass->db_consulta($strQuery);
                            
                            $strQuery = "INSERT INTO usuario_amigo(id_usuario, id_usuario_amigo)
                                                            VALUES({$rTMP["key"]}, {$intIdUsuario})";
                            $objDBClass->db_consulta($strQuery);
                            
                        }
                            
                    }                    
                        
                    if( $rTMP["value"]["conexion"] == "N" ){
                        
                        if( isset($arrListaAmigo[$rTMP["key"]]) ){
                            
                            $strQuery = "DELETE FROM usuario_amigo WHERE id_usuario = {$intIdUsuario} AND id_usuario_amigo = {$rTMP["key"]}";
                            $objDBClass->db_consulta($strQuery);
                            
                            $strQuery = "DELETE FROM usuario_amigo WHERE id_usuario = {$rTMP["key"]} AND id_usuario_amigo = {$intIdUsuario}";
                            $objDBClass->db_consulta($strQuery);
                            
                        }
                            
                    }                    
                                        
                }                     
                
            }
            
        }    
        
    }
    
    $objDBClass->db_close();
    die();
    
}    
      
if( isset($_GET["getDrawBodaPregunta"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();    
    
    //drawdebug(sesion);
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
    $intOrdenPregunta = 1;
    
    $strQuery = "SELECT MAX(pregunta.orden) orden
                 FROM   usuario_pregunta_respuesta,
                        pregunta_respuesta,
                        pregunta
                 WHERE  usuario_pregunta_respuesta.id_usuario = {$intIdUsuario}
                 AND    usuario_pregunta_respuesta.id_pregunta_respuesta = pregunta_respuesta.id_pregunta_respuesta 
                 AND    pregunta_respuesta.id_pregunta = pregunta.id_pregunta
                 ";
    $qTMP = $objDBClass->db_consulta($strQuery);
    $rTMP = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    $intOrdenPregunta = isset($rTMP["orden"]) && intval($rTMP["orden"]) ? $rTMP["orden"] : 0;
    
    $intOrdenPregunta++;
    
    
    $strQuery = "SELECT pregunta.id_pregunta,
                        pregunta.pregunta_en,
                        pregunta.pregunta_es,
                        pregunta_respuesta.id_pregunta_respuesta,
                        pregunta_respuesta.valor,
                        pregunta_respuesta.respuesta_en,
                        pregunta_respuesta.respuesta_es,
                        pregunta_respuesta.orden
                 FROM   pregunta,
                        pregunta_respuesta
                 WHERE  pregunta.orden = {$intOrdenPregunta}
                 AND    pregunta.id_pregunta = pregunta_respuesta.id_pregunta
                 ORDER BY   pregunta_respuesta.orden ASC 
                 
                 ";
    $arrPregunta = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPregunta["id_pregunta"] = $rTMP["id_pregunta"];    
        $arrPregunta["pregunta_texto"] = $strLenguaje == "es" ? $rTMP["pregunta_es"] : $rTMP["pregunta_en"];    
        $arrPregunta["respuesta"][$rTMP["id_pregunta_respuesta"]]["id_pregunta_respuesta"] = $rTMP["id_pregunta_respuesta"];    
        $arrPregunta["respuesta"][$rTMP["id_pregunta_respuesta"]]["valor"] = $rTMP["valor"];    
        $arrPregunta["respuesta"][$rTMP["id_pregunta_respuesta"]]["respuesta_texto"] = $strLenguaje == "es" ? $rTMP["respuesta_es"] : $rTMP["respuesta_en"];    
        $arrPregunta["respuesta"][$rTMP["id_pregunta_respuesta"]]["orden"] = $rTMP["orden"];    
                
    }
    $objDBClass->db_free_result($qTMP);
    $objDBClass->db_close();
    
    if( count($arrPregunta) > 0 ){
        
        ?>
        <div class="row">
            <div class="col-12 text-center">    
                <h5 class="text-center"><?php print $arrPregunta["pregunta_texto"]?></h5>
                <br>   
                <div class="d-none d-sm-block">
                    <ul class="list-group list-group-horizontal ">
                        <?php
                        
                        while( $rTMP = each($arrPregunta["respuesta"]) ){
                            ?>
                            
                            <li onclick="fntSetRespuesta('<?php print fntCoreEncrypt($rTMP["value"]["id_pregunta_respuesta"])?>')" class="list-group-item flex-fill list-group-item-action list-group-item-light" style="cursor: pointer;">
                                <?php print $rTMP["value"]["respuesta_texto"]?>
                            </li>
                                
                            <?php
                        }
                        
                        ?>
                    </ul>
                </div> 
                
                <div class="d-block d-sm-none">
                
                    <ul class="list-group   ">
                        <?php
                        
                        reset($arrPregunta["respuesta"]);
                        while( $rTMP = each($arrPregunta["respuesta"]) ){
                            ?>
                            
                            <li onclick="fntSetRespuesta('<?php print fntCoreEncrypt($rTMP["value"]["id_pregunta_respuesta"])?>')" class="list-group-item flex-fill list-group-item-action list-group-item-light" style="cursor: pointer;">
                                <?php print $rTMP["value"]["respuesta_texto"]?>
                            </li>
                                
                            <?php
                        }
                        
                        ?>
                    </ul>
                
                </div>
                <br>
            </div>
        </div>
        <?php    
    }
    else{
        ?>
        <div class="alert alert-info text-center">Fin del Cuestionario</div>
        <center>
            <button class="btn btn-info" onclick="$('#mlPregunta').modal('hide')">Listo</button>
        </center>
        <?php
    }
    
    
    die();
}

if( isset($_GET["setPost"]) ){
    
    $strPost = isset($_POST["txtPost"]) ? addslashes($_POST["txtPost"]) : "";
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
    if( !empty($strPost) && $intIdUsuario ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();    
        
        $strQuery = "INSERT INTO post(id_usuario, contenido)
                                VALUES({$intIdUsuario}, '{$strPost}')";
        
        $objDBClass->db_consulta($strQuery);
        $intPost = intval($objDBClass->db_last_id());
        
        if( $intPost && count($_FILES) > 0 ){
            
            while( $rTMP = each($_FILES) ){
                
                if( $rTMP["value"]["size"] > 0 ){
                    
                    $strQuery = "INSERT INTO post_galeria(id_post, imagen_url)
                                                    VALUES({$intPost}, '')";
                    $objDBClass->db_consulta($strQuery);
                    $intPostGaleria = intval($objDBClass->db_last_id());
                    
                    if( $intPostGaleria ){
                        
                        $strUrlArchivo = "../../file_inguate/post/".fntCoreEncrypt($intPostGaleria).".png";
                        rename($rTMP["value"]["tmp_name"], $strUrlArchivo);
                        
                        $strQuery = "UPDATE post_galeria
                                     SET    imagen_url = '{$strUrlArchivo}' 
                                     WHERE  id_post_galeria = {$intPostGaleria}";
                        $objDBClass->db_consulta($strQuery);
                        
                        fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPostGaleria));
        
                    }
                    
                }
                
            }            
            
        }
        
        if( $intPost ){
            
            while( $rTMP = each($_POST) ){
                
                $arrExplode = explode("_", $rTMP["key"]);
                
                if( $arrExplode[0] == "txtPostRespuesta" ){
                       
                    $intOrden = isset($_POST["txtPostRespuestaOrden_{$arrExplode[1]}"]) ? intval($_POST["txtPostRespuestaOrden_{$arrExplode[1]}"]) : 0;
                    $strRespuestaTexto = isset($_POST["txtPostRespuesta_{$arrExplode[1]}"]) ? addslashes($_POST["txtPostRespuesta_{$arrExplode[1]}"]) : 0;
                    
                    $strQuery = "INSERT INTO post_respuesta(id_post,  orden, respuesta)
                                                    VALUES( {$intPost}, {$intOrden}, '{$strRespuestaTexto}' )";
                    $objDBClass->db_consulta($strQuery);
                    
                }
                
            }
            
        }
            
        
        print fntCoreEncrypt($intPost);
        
        $objDBClass->db_close();        
        
    }
    
    die();
}
  
if( isset($_GET["getShowPostIndividual"]) ){
    
    $intPost = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
    
    if( $intPost ){
    
        include "core/dbClass.php";
        $objDBClass = new dbClass();

        $strQuery = "SELECT post.id_post,
                            post.contenido,
                            post.add_fecha,
                            post_galeria.id_post_galeria,
                            post_galeria.imagen_url,
                            usuario.nombre nombre_propietario,
                            usuario.apellido apellido_propietario
                     FROM   post
                                LEFT JOIN post_galeria
                                    ON  post_galeria.id_post = post.id_post,
                            usuario
                     WHERE  post.id_post = {$intPost} 
                     AND    post.id_usuario = usuario.id_usuario
                     ORDER BY id_post_galeria ASC       
                     ";
        $arrPost = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
            
            $arrPost["id_post"] = $rTMP["id_post"];
            $arrPost["contenido"] = $rTMP["contenido"];
            $arrPost["add_fecha"] = $rTMP["add_fecha"];
            $arrPost["like"] = 0;
            $arrPost["mi_like"] = 'N';
            $arrPost["nombre_propietario"] = $rTMP["nombre_propietario"]." ".$rTMP["apellido_propietario"];
            
            if( intval($rTMP["id_post_galeria"]) )
                $arrPost["galeria"][$rTMP["id_post_galeria"]]["imagen_url"] = $rTMP["imagen_url"];
                
        }
        $objDBClass->db_free_result($qTMP);
                
        
        $objDBClass->db_close();
        
        if( count($arrPost) > 0 ){
            
            fntDrawPost($arrPost);
            
        }
            
    }
    
    die();    
}

if( isset($_GET["setDatosPerfil"]) ){
    
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
    if( $intIdUsuario ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();    
        
        $strNombre = isset($_POST["txtNombre"]) ? fntCoreClearToQuery($_POST["txtNombre"]) : "";
        $intSexo = isset($_POST["slcCatalogoSexo"]) ? intval($_POST["slcCatalogoSexo"]) : "";
        $strFechaNacimiento = isset($_POST["txtFechaNacimiento"]) ? trim($_POST["txtFechaNacimiento"]) : "";
        
        $strQuery = "UPDATE usuario
                     SET    nombre = '{$strNombre}',
                            sexo = '{$intSexo}',
                            fecha_nacimiento = '{$strFechaNacimiento}'
                     WHERE  id_usuario = {$intIdUsuario} ";
        
        $objDBClass->db_consulta($strQuery);
        $objDBClass->db_close();
        
        if( isset($_FILES["flImagePerfil"]) && $_FILES["flImagePerfil"]["size"] > 0 ){
            
            $strUrlArchivo = "../../file_inguate/usuario_perfil/".fntCoreEncrypt($intIdUsuario).".png";
            rename($_FILES["flImagePerfil"]["tmp_name"], $strUrlArchivo);
            
            fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intIdUsuario));
        
        }
    }
            
    
    die();
}

$intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
include "core/dbClass.php";
$objDBClass = new dbClass();

$boolVisita = false;
if( intval($intUsuarioVisita) && $intIdUsuario != $intUsuarioVisita ){
    
    $intIdUsuario = $intUsuarioVisita;
    $boolVisita = true;    
    
}

$arrUsuarioDatos = fntGetDatosUsuario($intIdUsuario, $objDBClass);

$arrUsuarioAmigo = fntGetUsuarioAmigo($intIdUsuario, $objDBClass);
$arrUsuarioAmigo[$intIdUsuario] = $intIdUsuario;

$arrPostMuro = fntGetPostUsuario($intIdUsuario, $arrUsuarioAmigo, $objDBClass);
 
$arrCatalogoFondo = fntCatalogoFondoPlacePublico();
    
$strColorHex = $arrCatalogoFondo[1]["color_hex"];

fntDrawHeaderPublico($boolMovil);

?>   
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 

<script src="dist/wysihtml5/dist_back/wysihtml5-0.3.0.min.js"></script>
                                
<input type="hidden" id="hidOrigen" value="<?php print $intOrigen;?>">
<input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intKey);?>">
<input type="hidden" id="hidTipo" value="<?php print $intTipo;?>">
<input type="hidden" id="hidTexto" value="<?php print $strTexto;?>">
<input type="hidden" id="hidRango" value="<?php print $strRango;?>">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">


<style>
    
        
    .text-background-theme {
        background-color: #<?php print $strColorHex?> !important;
        
    }
    .text-color-theme {
        color: #<?php print $strColorHex?> !important;
    
    }

    .btn-theme-circular {
        background-color: #<?php print $strColorHex?>;
        color: white;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
        position:absolute;
        top:0%;
        right:2%;
        z-index: 99;

    }
            
    .container__ {
      display: flex;
      justify-content: space-around;
      align-items: flex-start;
      height: 100%;
    }
        
    .police {
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    } 
                    
    
</style>
<div class="container-fluid m-0 p-0" >
    
    <div class="row m-0 p-0">
    
        <div class="col-12 p-0 m-0 text-center" >
        
            <img src="<?php print  $arrCatalogoFondo[1]["url_fondo"]?>" style="width: 100%; height: auto;">
                   
            <div class="m-0 p-0 pt-2 pt-lg-4  text-center" style="position: absolute; top: 0px; width: 100%;"> 
                
                <?php
                
                fntDrawHeaderPrincipal(1, true, $objDBClass);
                
                $arrCatalogoSexo = fntCatalogoSexo();
                                            
                $strSexo = "";
                if( isset($arrCatalogoSexo[$arrUsuarioDatos["sexo"]]) ){
                    
                    $strSexo  = $strLenguaje == "es" ? $arrCatalogoSexo[$arrUsuarioDatos["sexo"]]["tag_es"] : $arrCatalogoSexo[$arrUsuarioDatos["sexo"]]["tag_en`"];
                    
                    
                }
                
                ?>        
                
                <!-- Contenido -->       
                <div class="row p-0 m-0 justify-content-center mt-4 " style="">
                    <div class="col-12 col-lg-3 p-0 m-0  ">
                        
                        <div class="row ">
                            <div class="col-12 ">
                                
                                <form name="frmPerfil" id="frmPerfil"  onsubmit="return false;" method="POST">
                                
                                <?php
                                
                                if( !$boolVisita ){
                                    ?>
                                    <input onchange="fntSetImgfileProfile('flImagePerfil', 'imgPerfil');" type="file" id="flImagePerfil" name="flImagePerfil" style="display: none;">
                                    
                                    <?php                                    
                                }
                                
                                ?>
                                <img onclick="$('#flImagePerfil').click()" id="imgPerfil" src="imgInguate.php?t=up&src=../../file_inguate/usuario_perfil/<?php print fntCoreEncrypt($intIdUsuario)?>.png" style="border-radius: 50%; width: 100px; height: 100px; object-fit: cover; cursor: pointer;" >
                                    
                                    
                                <div id="divContenidoVista">
                                                        
                                    <h5>
                                        <span id="spnNombrePerfil"><?php print $arrUsuarioDatos["nombre"]?></span> 
                                        <?php
                                        
                                        if( !$boolVisita ){
                                            ?>
                                            <i onclick="fntShowFormPerfil();" class="fa fa-pencil " style="color: #3E53AC; font-size: 15px; cursor: pointer;"></i>
                                    
                                            <?php
                                        }
                                        
                                        ?>
                                        
                                    </h5>
                                    <h5 class="text-center text-secondary pl-2" style="font-size: 15px;"><span id="spnSexoPerfil"><?php print  $strSexo?></span> &nbsp;&nbsp;&nbsp;&nbsp; <span id="spnFechaNacimiendoPerfil"><?php print $arrUsuarioDatos["fecha_nacimiento"]?></span></h5>
                                    <h5 class="text-center text-secondary pl-2" style="font-size: 15px;"></h5>
                                </div>
                                <div id="divContenidoForm" style="display: none;">
                                    
                                    <div id="" class="row ">
                                        <div class="col-12">
                                            
                                            <input type="text" value="<?php print $arrUsuarioDatos["nombre"]?>" name="txtNombre" id="txtNombre" class="form-control form-control-sm text-center" value="Yordi Guevara">
                                                                        
                                        </div>             
                                        <div class="col-6 mt-1">
                                            
                                            <select name="slcCatalogoSexo" id="slcCatalogoSexo" class="custom-select custom-select-sm">
                                                <?php
                                                
                                                while( $rTMP = each($arrCatalogoSexo) ){
                                                    
                                                    $strSelected = $arrUsuarioDatos["sexo"] == $rTMP["key"] ? "selected" : "";
                                                    ?>
                                                    
                                                    <option <?php print $strSelected;?> value="<?php print $rTMP["key"]?>"><?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                                                
                                                    <?php
                                                }
                                                
                                                ?>
                                            </select>
                                                                        
                                        </div>
                                        <div class="col-6 mt-1">
                                            
                                            <input value="<?php print $arrUsuarioDatos["fecha_nacimiento"]?>" type="text" id="txtFechaNacimiento" name="txtFechaNacimiento" class="form-control form-control-sm text-center" value="">
                                            
                                                                        
                                        </div>
                                        <div class="col-12 mt-1">
                                            
                                            <button onclick="fntGuardarPerfil();" class="btn btn-success btn-sm" style="background-color: #3E53AC;">Guardar</button>
                                                             
                                        </div>
                                    </div>
                                </div>
                                
                                </form>
                            </div>                        
                        </div> 
                        <div class="row">
                            <div class="col-12 pl-4 pr-4">
                                <br>
                                <h5 class="text-left">Cuestionario</h5>
                            
                                <div class="progress">
                                    <div class="progress-bar " role="progressbar" style="width: 25%; background-color: #3E53AC;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                        25%
                                    </div>
                                </div>
                                
                                <button class="btn btn-success btn-sm mt-3 hide" style="background-color: #3E53AC;">Responder</button>
                                                            
                            </div>                        
                        </div>                        
                                                
                    </div>
                    
                    
                    
                    <div class="col-12 col-lg-9  p-0 m-0 ">
                        
                        <?php
                        
                        if( !$boolVisita ){
                            ?>
                            <div class="row p-0 m-0 justify-content-center">
                                <div class="col-12 col-lg-8 p-0 m-0 bg-white text-right" style="border-radius: 5px; height: auto;">
                                    
                                    <hr class="d-block d-sm-none">
                                    <form name="frmPost" id="frmPost"  onsubmit="return false;" method="POST">
                        
                                    <textarea id="txtPost" name="txtPost" rows="4" class="form-control form-control-sm" style="width: 100%; resize:vertical; border: 0px; border-radius: 5px 5px 0px 0px;" placeholder="Realiza un POST..."></textarea>
                                    
                                    <div class="row m-0 p-0">
                                        <div id="divContenidoPostRespuesta" class="col-12 col-lg-12 m-0 p-1 text-center ">
                                                  
                                            
                                                                                        
                                        
                                        </div>
                                        <div class="col-12 align-bottom mb-1">
                                            <div class="row ">
                                                <div class="col-6 text-left">
                                                    <span class="text-hover-underline" id="spnAddFotoPost" onclick="fntAddPostRespuesta();">
                                                        
                                                        <i class="fa fa-plus" style="color: #3E53AC;"></i>
                                                        Agregar Respuesta
                                                        
                                                    </span>
                                                </div>
                                                <div class="col-6 text-right">
                                                    
                                                    <button onclick="fntSavePost();" class="ml-2 btn btn-primary btn-sm" style="background-color: #3E53AC;">Publicar</button>
                                            
                                                </div>
                                            </div>
                                              
                                        </div>
                                        
                                    </div>
                                    
                                    <?php
                                    
                                    // Queda comentado el agregar fotos
                                    if( false ){
                                        ?>
                                        <div class="row m-0 p-0">
                                            <div class="col-12 col-lg-12 m-0 p-0 text-center ">
                                                
                                                <div class="card-columns" id="dicCards">
                                                                 
                                                </div>
                                            
                                            </div>
                                            <div class="col-12 align-bottom">
                                                <p class="p-1 m-0 ">
                                                    
                                                    <span class="text-hover-underline" id="spnAddFotoPost" onclick="fntAddFoto();">
                                                        Agregar Foto
                                                        <i class="fa fa-camera" style="color: #3E53AC;"></i>
                                                    </span>
                                                    
                                                    <button onclick="fntSavePost();" class="ml-2 btn btn-primary btn-sm" style="background-color: #3E53AC;">Publicar</button>
                                                
                                                </p>
                                            </div>
                                            
                                        </div>
                                        <?php
                                    }
                                    
                                    ?>
                                    
                                    
                                    </form>            
                                </div>                        
                            </div>
                            <?php
                        }
                        
                        ?>
                             
                                                
                        <div class="row p-0 m-0 justify-content-center">
                            <div class="col-12 col-lg-8 text-center  p-0 m-0 " id="divContenidoPOST">
                               
                                <?php
                                
                                if( count($arrPostMuro) > 0 ){
                                    
                                    while( $rTMP = each($arrPostMuro) ){
                                        
                                        fntDrawPost($rTMP["value"]);
                                        
                                    }
                                    
                                }
                                
                                /*
                                for( $i = 1; $i <= 5; $i++ ){
                                    ?>
                                    <div class="card m-0 p-0 mt-4">
                                        
                                        <div class="card-header p-2 m-0 border-0 text-left">
                                            <img src="dist/images/author6.jpg" style="border-radius: 50%; object-fit: cover; width: 40px; height: 40px;" alt="Cinque Terre">
                                            Yordi Guevara
                                        </div>
                                        
                                        <img src="dist/images/slide10.jpg " class="card-img-top" style="object-fit: cover;">

                                        
                                        
                                        <ul class="list-group list-group-horizontal">
                                            <li class="list-group-item p-2">
                                                <i class="fa fa-heart"></i>
                                            </li> 
                                            <li class="list-group-item p-2">
                                                <i class="fa fa-share"></i>
                                            </li>
                                        </ul>
                                        
                                        <div class="card-body p-0">
                                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                        </div>
                                    </div>
                                    <div class="card m-0 p-0 mt-4">
                                        
                                        <div class="card-header p-2 m-0 border-0 text-left">
                                            <img src="dist/images/author6.jpg" style="border-radius: 50%; object-fit: cover; width: 40px; height: 40px;" alt="Cinque Terre">
                                            Yordi Guevara
                                        </div>
                                        
                                        
                                        
                                        <div class="card-body p-0">
                                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                        </div>
                                        <ul class="list-group list-group-horizontal">
                                            <li class="list-group-item p-2">
                                                <i class="fa fa-heart"></i>
                                            </li> 
                                            <li class="list-group-item p-2">
                                                <i class="fa fa-share"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <?php
                                }
                                 */
                                ?>
                                                                
                            </div>                        
                        </div> 
                        
                        <style>
                            
                        </style>
                                                
                    </div>
                </div>
                
                
            
            </div>
                   
        </div>
    </div>
    
</div>

<div class="modal fade" id="mlPregunta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Responde</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body " id="mlPreguntaBody">
                
                
                
                
                
                
            </div>
        </div>
    </div>
</div>
   
<script src="dist/js/jsAntigua.js"></script>
<link href="dist/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<script src="dist/datepicker/js/bootstrap-datepicker.min.js"></script>
                
<style>
    
    .flotante_change_theme {
        
        position:absolute;
        top: 1%;
        left:1%;
    
    }

    .slick-center-Border {
        
        padding: 0px !important;
        
    }
    
    .flotante {
            
        display:none;
        position:fixed;
        top:90%;
        right:2%;
        background-color: #<?php print $strColorHex?>;
        color: white;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
    
    }
    
    .slick-slide img{
        width: 100%;
        height: 500px;
    }
</style>
<script src="dist/wysihtml5/dist_back/parser_rules/advanced.js"></script>
        
<script >                                 
    
    var intCountPostRespuesta = 1;       
    var intCountPreguntaShow = 0;       
    var intCountPregunta = 0;       
    var boolPreguntar = false;       

    var intCountFotoPost = 1;
    var objEditor;
    
    $(document).ready(function() {
        
        objEditor = new wysihtml5.Editor("txtPost", {
            parserRules:  wysihtml5ParserRules
        });
        
        fntNextPregunta(true);
        
        $('#txtFechaNacimiento').datepicker({
            format: "yyyy-mm-dd",
            language: "es",
            autoclose: true
        });
        
        fntAddPostRespuesta();
        
    });   
    
    function fntNextPregunta(boolInicio){
        
        boolInicio = !boolInicio ? false : boolInicio;
                
        $.ajax({
            url: "profile.php?getShowPregunta=true", 
            dataType: "json",
            success: function(result){
                
                if( result["preguntar"] == true ){
                    
                    intCountPregunta = result["count_preguntas"];       
                    boolPreguntar = true;       
                
                    getIssetNextPreguntar();
                    
                }
                else{
                    
                    $("#mlPreguntaBody").html("");
                    $("#mlPregunta").modal("hide");
                    
                    if( !boolInicio )    
                        location.reload();        
                }    
                
            }
        });
        
    }
    
    function getIssetNextPreguntar(){
        
        if( intCountPreguntaShow <= intCountPregunta ){
            
            $.ajax({
                url: "profile.php?getIssetNextPreguntar=true", 
                dataType: "json",
                success: function(result){
                    
                    if( result["showPregunta"] == true ){
                        
                        intCountPreguntaShow++;    
                        fntDrawPregunta();
                        
                    }
                    else{
                        
                        $("#mlPreguntaBody").html("");
                        $("#mlPregunta").modal("hide");    
                        
                    }
                    
                        
                }
            });
            
        }
            
            
    }
    
    function fntDrawPregunta(){
        
        $.ajax({
            url: "profile.php?getDrawBodaPregunta=true", 
            success: function(result){
                
                $("#mlPreguntaBody").html(result);
                $("#mlPregunta").modal("show");
            
            }
        });
            
    }
    
    function fntSetRespuesta(strRespuesta){
        
        $.ajax({
            url: "profile.php?setRespuesta=true&res="+strRespuesta, 
            success: function(result){
                
                fntNextPregunta();        
            
            }
        });
                
    }
    
    function fntAddFoto(){
                                            
        $("#dicCards").append("<div class=\"card \" id=\"divCardImage_"+intCountFotoPost+"\">      "+
                              "      <img id=\"imgImagePost_"+intCountFotoPost+"\" src=\"\" class=\"card-img-top\" > "+
                              "      <input type=\"file\" id=\"flPostImage_"+intCountFotoPost+"\" name=\"flPostImage_"+intCountFotoPost+"\" style=\"display: none;\" onchange=\"fntSetImgfile('"+intCountFotoPost+"');\">  "+
                              "      <div class=\"p-1 text-right\" style=\"position: absolute; top: 0px; right: 0px;\">    "+
                              "          <button class=\"btn btn-light btn-sm\" onclick=\"$('#flPostImage_"+intCountFotoPost+"').click();\">   "+
                              "              <i class=\"fa fa-pencil\"></i>   "+
                              "          </button>        "+                                        
                              "          <button class=\"btn btn-light btn-sm\" onclick=\"fntDeleteImagePost('"+intCountFotoPost+"');\">  "+
                              "              <i class=\"fa fa-trash\"></i>       "+
                              "          </button>  "+                                              
                              "      </div>    "+
                              "  </div>");    
        $('#flPostImage_'+intCountFotoPost).click();
        intCountFotoPost++;
    
    }
    
    function fntSetImgfile(strIndex){
        
        var filename = $('#flPostImage_'+strIndex).val();
        
        arr = filename.split(".");
        
        if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
            
            swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
            fntDeleteImageSeccion1(index);
            return false;                    
            
        }
        
        
        if ( ! window.FileReader ) {
            return alert( 'FileReader API is not supported by your browser.' );
        }
        var $i = $( '#flPostImage_'+strIndex ), // Put file input ID here
            input = $i[0]; // Getting the element from jQuery
        
        if ( input.files && input.files[0] ) {
            
            file = input.files[0]; // The file
                
            fr = new FileReader(); // FileReader instance
            
            fr.onload = function () {
                
                var image = new Image();

                image.onload = function() {      

                    document.getElementById("imgImagePost_"+strIndex).src = fr.result;

                }

                // Load image.
                image.src = fr.result;
                
            }; 
            //fr.readAsText( file );
            fr.readAsDataURL( file );
            
        } 
        else {
            // Handle errors here
            alert( "File not selected or browser incompatible." )
        }
                        
    }
    
    function fntDeleteImagePost(strIndex){
        
        $("#divCardImage_"+strIndex).remove();    
        
        
    }
    
    function fntSavePost(){
                
        var formData = new FormData(document.getElementById("frmPost"));
            
        $(".preloader").fadeIn();
        $.ajax({
            url: "profile.php?setPost=true", 
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result){
                
                $(".preloader").fadeOut();
                $("#dicCards").html("");
                objEditor.setValue("");
                intCountFotoPost = 1;  
                
                fntShowPostIndividual(result);
        
            }
                    
        });
        
    }
    
    function fntShowPostIndividual(strKey){
        
        $.ajax({
            url: "profile.php?getShowPostIndividual=true&key="+strKey, 
            success: function(result){
                
                $("#divContenidoPOST").prepend(result);
            
            }
        });
        
    }

    function fntSetImgfileProfile(strFile, strImg){
        
        var filename = $('#'+strFile).val();
        
        arr = filename.split(".");
        
        if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
            
            swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
            fntDeleteImageSeccion1(index);
            return false;                    
            
        }
        
        
        if ( ! window.FileReader ) {
            return alert( 'FileReader API is not supported by your browser.' );
        }
        var $i = $( '#'+strFile ), // Put file input ID here
            input = $i[0]; // Getting the element from jQuery
        
        if ( input.files && input.files[0] ) {
            
            file = input.files[0]; // The file
                
            fr = new FileReader(); // FileReader instance
            
            fr.onload = function () {
                
                var image = new Image();

                image.onload = function() {      

                    document.getElementById(strImg).src = fr.result;

                }

                // Load image.
                image.src = fr.result;
                
            }; 
            //fr.readAsText( file );
            fr.readAsDataURL( file );
            
        } 
        else {
            // Handle errors here
            alert( "File not selected or browser incompatible." )
        }
                        
    }
    
    function fntGuardarPerfil(){
        
        var formData = new FormData(document.getElementById("frmPerfil"));
            
        $(".preloader").fadeIn();
        $.ajax({
            url: "profile.php?setDatosPerfil=true", 
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result){
                
                $(".preloader").fadeOut();
                $("#divContenidoForm").hide();
                $("#divContenidoVista").show();
                                   
                $("#spnNombrePerfil").html($("#txtNombre").val());
                $("#spnFechaNacimiendoPerfil").html($("#txtFechaNacimiento").val());
                $("#spnSexoPerfil").html($("#slcCatalogoSexo option:selected").text());
        
            }
                    
        });    
        
    }
    
    function fntShowFormPerfil(){
        
        $("#divContenidoForm").show();
        $("#divContenidoVista").hide();
        
    }
    
    function fntRemoveRespuesta(strKey){
        
        $("#divRowPostRespuesta_"+strKey).remove();    
        fntSetOrdenPostRespuesta();                                      
            
    }
    
    function fntAddPostRespuesta(strKey){                   
        
        $("#divContenidoPostRespuesta").append("<div class=\"row mb-1\" id=\"divRowPostRespuesta_"+intCountPostRespuesta+"\">  "+
                                               "      <div class=\"col-1 text-right\">      "+
                                               "          <span id=\"spnaOrdenRespuesta_"+intCountPostRespuesta+"\">1</span>.   "+
                                               "      </div>         "+
                                               "      <div class=\"col-10 pl-0 pr-0\">         "+
                                               "          <input id=\"txtPostRespuestaOrden_"+intCountPostRespuesta+"\" name=\"txtPostRespuestaOrden_"+intCountPostRespuesta+"\" type=\"hidden\">    "+
                                               "          <input id=\"txtPostRespuesta_"+intCountPostRespuesta+"\" name=\"txtPostRespuesta_"+intCountPostRespuesta+"\" class=\"form-control rowPostRespuesta form-control-sm \" type=\"text\" placeholder=\"Escribe la posible respuesta\">    "+
                                               "      </div>          "+
                                               "      <div class=\"col-1 text-left\">         "+
                                               "          <i onclick=\"fntRemoveRespuesta('"+intCountPostRespuesta+"');\" style=\"cursor: pointer;\" class=\"fa fa-close text-danger\"></i>    "+
                                               "      </div>    "+
                                               "  </div>");   
                                               
        fntSetOrdenPostRespuesta();                                      
        intCountPostRespuesta++;
        
    }
    
    function fntSetOrdenPostRespuesta(){
        
        intCount = 0;
        $(".rowPostRespuesta").each(function (){
            
            arrExplode = this.id.split("_");
            
            intCount++;
            
            $("#spnaOrdenRespuesta_"+arrExplode[1]).html(intCount);
            $("#txtPostRespuestaOrden_"+arrExplode[1]).val(intCount);
            
        });                                       
        
    }
    
</script>
<?php
fntGetScriptFuncionPost(); 
fntDrawFooterPublico($strColorHex, false); 
?>
