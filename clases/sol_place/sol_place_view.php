<?php

require_once "sol_place_model.php";

class sol_place_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new sol_place_model(); 
        
    }
    
    public function fntContentPage($strAction){
        
        //drawdebug(md5("intercorps8"));
    
        $arrSolPlace = $this->objModel->getSolPlace($_SESSION["_open_antigua"]["core"]["id_usuario"]);
        
        $arrCatalogo = fntGetCatalogoSolicitudPlace();
        
        $arrCatalogoTipoComercio = fntGetCatalogoTipoCategoriaComercio();
        $arrCatalogoPaqueteProducto = fntGetCatalogoPaqueteProducto();
        ?>
         
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
            <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
       
            <link rel="stylesheet" media="screen" type="text/css" href="dist/colorpicker/css/colorpicker.css" />
            <script type="text/javascript" src="dist/colorpicker/js/colorpicker.js"></script>
 
       
            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-2  " id="divContenidoBody"  >
                    
                    <div class="row justify-content-center">
                        <div class="col-10 col-lg-6">
                            
                            <br>
                            <select id="slCategoriaNegocio" name="slCategoriaNegocio" class="custom-select">
                                <option>Estado Negocio</option>
                                <?php
                                
                                while( $rTMP = each($arrCatalogo) ){
                                    ?>
                                    <option value="<?php print $rTMP["key"]?>"><?php print $rTMP["value"]["nombre"]?></option>
                                    <?php
                                }
                                
                                ?>
                                
                            </select>
                            <br>
                            
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-11 table-responsive">
                            
                            <table class="table table-secondary table-secondary table-striped   " id="myTable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Titulo</th>
                                        <th scope="col">Usuario</th>
                                        <th scope="col">Telefono</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col" style="width: 20%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    while( $rTMP = each($arrSolPlace) ){
                                        ?>
                                        <tr>
                                            <td><?php print $rTMP["value"]["titulo"]?></td>
                                            <td><?php print $rTMP["value"]["nombre"]?></td>
                                            <td><?php print $rTMP["value"]["telefonoUsuario"]?></td>
                                            <td><?php print $rTMP["value"]["email"]?></td>
                                            <td><?php print $rTMP["value"]["estado_text"]?></td>
                                            <td class="text-right nowrap" nowrap>
                                                <button onclick="fntDrawConfiguracion('<?php print $rTMP["value"]["id_place"]?>');" class="btn btn-primary btn-raised btn-sm">Configuracion</button>
                                                <button onclick="fntDrawSolicitud('<?php print $rTMP["value"]["id_sol_place"]?>');" class="btn btn-primary btn-raised btn-sm">Solicitud</button>
                                                <button onclick="window.open('placeup.php?p=<?php print fntCoreEncrypt($rTMP["value"]["id_place"])?>');" class="btn btn-primary btn-raised btn-sm">Editar Pagina</button>
                                                <button onclick="window.open('<?php print $rTMP["value"]["url_place"]?>');" class="btn btn-primary btn-raised btn-sm">Ver Pagina</button>
                                                
                                                <?php
                                                
                                                if( $rTMP["value"]["estado"] == "S" ){
                                                    ?>
                                                    <button onclick="fntAceptarSol('<?php print $rTMP["value"]["id_sol_place"]?>', '<?php print $rTMP["value"]["id_place"]?>');" class="btn btn-success btn-raised btn-sm">Aprobacion Pago</button>
                                                    <button onclick="fntRechazarSol('<?php print $rTMP["value"]["id_sol_place"]?>');" class="btn btn-danger btn-raised btn-sm">Rechazar</button>
                                                
                                                    <?php
                                                }
                                                
                                                ?>
                                                    
                                            </td>
                                        </tr>
                                        <?php                                        
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>
                            
                        </div>    
                    </div>    
                                        
                </div> 
            </main>
            
            <div class="modal fade" id="modalRechazo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            Rechazar                    
                        </div>
                        <div class="modal-body">
                            <form name="frmRechazo" id="frmRechazo" onsubmit="return false;" method="POST">
        
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="hidden" id="hidKeyRechazo" name="hidKeyRechazo" value="0">
                                        <label for="txtMotivoModal"><small>Motivo</small></label>
                                        <textarea class="form-control" id="txtMotivoModal" name="txtMotivoModal" placeholder="Motivo" rows="5"></textarea>
                                    
                                    </div>
                                </div>      
                            </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-raised m-1" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn-raised m-1" onclick="fntRechazoSolitud();">Rechazar</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="modalAprobar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-body" id="divModalAprobacionPlace">
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-raised m-1" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn-raised m-1" onclick="fntAprobacionPago();">Aprobacion de Pago</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="modalSolicitudPlace" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            Datos                    
                        </div>
                        <div class="modal-body" id="modalSolicitudPlaceContenido">
                        </div>
                        <div class="modal-footer" id="">
                            <button type="button" class="btn btn-secondary btn-raised m-1" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>    
          
        </div>    
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable({
                    "ordering": false                    
                });    
                    
            }); 
            
            function fntAceptarSol(intSolPlace, intPlace){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawAprobacionSolicitud=true&place="+intPlace+"&sol_place="+intSolPlace, 
                    success: function(result){
                        
                        $("#divModalAprobacionPlace").html(result);
                        $("#modalAprobar").modal("show");
                
                    
                    }
                });
                
            }
            
            function fntAprobacionPago(intPlace){
                
                var formData = new FormData(document.getElementById("frmAprobar"));
                
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setAprobadoPagoSolicitud=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });                
                
            }   
            
            function fntRechazarSol(intPlace){
                
                $('#hidKeyRechazo').val(intPlace);                
                $('#modalRechazo').modal('show');                
                
            }
            
            function fntRechazoSolitud(){
                
                var formData = new FormData(document.getElementById("frmRechazo"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setRezhazoSolicitud=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });
                
            }
            
            function fntChangeSugerirPrecio(){
                
                <?php
                
                reset($arrCatalogoPaqueteProducto);
                while( $rTMP = each($arrCatalogoPaqueteProducto) ){
                    ?>
                        
                    if( $("#slPaqueteProducto option:selected").val() == "<?php print $rTMP["key"]?>" ){
                        
                        $("#txtMensual").val("<?php print $rTMP["value"]["costo"]["mensual"]?>");                            
                        $("#txtSemestral").val("<?php print $rTMP["value"]["costo"]["semestral"]?>");                            
                        $("#txtAnual").val("<?php print $rTMP["value"]["costo"]["anual"]?>");                            
                        
                    }
                    
                    <?php
                }
                
                
                ?>                
                
            }
            
            function fntDrawSolicitud(intSolicitudPlace){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawSolicitud=true&sol_place="+intSolicitudPlace, 
                    success: function(result){
                        
                        $("#modalSolicitudPlaceContenido").html(result);
                        $("#modalSolicitudPlace").modal("show");
                
                    
                    }
                });
                    
            }
            
            function fntDrawConfiguracion(intSolicitudPlace){
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawFormNewPlace=true&key="+intSolicitudPlace+"&paso="+1, 
                    success: function(result){
                        
                        $("#modalSolicitudPlaceContenido").html(result);
                        $("#modalSolicitudPlace").modal("show");
                    
                    }
                });
                    
            }
            
               
        </script>        
        <?php 
        
    }
    
    public function drawSolicitud($intSolPlace){
        
        $arrSolicitud = $this->objModel->getSolicitudDatos($intSolPlace);
        
        ?>
        
        <div class="row">
            
            <div class="col-6">
                <label><small class="font-weight-bold">Descripcion</small></label><br>
                <?php print $arrSolicitud["descripcion"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">add_fecha</small></label><br>
                <?php print $arrSolicitud["add_fecha"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">nombre</small></label><br>
                <?php print $arrSolicitud["nombre"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">telefono</small></label><br>
                <?php print $arrSolicitud["telefono"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">Vendedor</small></label><br>
                <?php print $arrSolicitud["codigo"]?>
            </div>
                        
            <div class="col-6">
                <label><small class="font-weight-bold">place_id</small></label><br>
                <?php print $arrSolicitud["place_id"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">telefono_propietario</small></label><br>
                <?php print $arrSolicitud["telefono_propietario"]?>
            </div>
            <div class="col-6">
                <label><small class="font-weight-bold">email_propietario</small></label><br>
                <?php print $arrSolicitud["email_propietario"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">tipo_negocio</small></label><br>
                <?php print $arrSolicitud["tipo_negocio"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">Patente</small></label><br>
                <button onclick="window.open('imgInguate.php?t=up&src=../../file_inguate/place/<?php print $arrSolicitud["url_patente"]?>')" class="btn btn-primary btn-sm btn-raised" >Ver</button>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">DPI</small></label><br>
                <button onclick="window.open('imgInguate.php?t=up&src=../../file_inguate/place/<?php print $arrSolicitud["url_dpi"]?>')" class="btn btn-primary btn-sm btn-raised" >Ver</button>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">SA</small></label><br>
                <button onclick="window.open('imgInguate.php?t=up&src=../../file_inguate/place/<?php print $arrSolicitud["url_sa"]?>')" class="btn btn-primary btn-sm btn-raised" >Ver</button>
            </div>
            
        </div>
        
        <?php
        
    }
    
    public function drawAprobacionSolicitud($strAction, $intPlace, $intSolPlace){
        
        $arrCatalogoTipoComercio = fntGetCatalogoTipoCategoriaComercio();
        $arrCatalogoPaqueteProducto = fntGetCatalogoPaqueteProducto();
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $this->objModel->getobjDBClass());
    
        ?>
        <form name="frmAprobar" id="frmAprobar" onsubmit="return false;" method="POST">
        <input type="hidden" id="hidKeyAprobar" name="hidKeyAprobar" value="<?php print $intSolPlace?>">
        <input type="hidden" id="hidKeyAprobarSol" name="hidKeyAprobarSol" value="<?php print $intSolPlace?>">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    
                    <select id="slCategoriaNegocio" name="slCategoriaNegocio" class="custom-select">
                        <option>Categoria Negocio</option>
                        <?php
                        
                        while( $rTMP = each($arrCatalogoTipoComercio) ){
                            ?>
                            <option value="<?php print $rTMP["key"]?>"><?php print $rTMP["value"]["nombre"]?></option>
                            <?php
                        }
                        
                        ?>
                        
                    </select>                                        
                </div>
            </div>      
        </div>
        <div class="row">
            <div class="col-12">
                
                <select onchange="fntChangeSugerirPrecio();" id="slPaqueteProducto" name="slPaqueteProducto" class="custom-select">
                    <option>Paquete Producto</option>
                    <?php
                    
                    while( $rTMP = each($arrCatalogoPaqueteProducto) ){
                        ?>
                        <option value="<?php print $rTMP["key"]?>"><?php print $rTMP["value"]["nombre"]?></option>
                        <?php
                    }
                    
                    ?>
                    
                </select>    
                
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                
                <label for="txtMensual"><small>Mensual</small></label>
                <input type="text" value="0.00" class="form-control" id="txtMensual" name="txtMensual" placeholder="Mensual" required>

                                                    
            </div>
            <div class="col-4">
                
                <label for="txtSemestral"><small>Semestral</small></label>
                <input type="text" value="0.00" class="form-control" id="txtSemestral" name="txtSemestral" placeholder="Semestral" required>

                                                    
            </div>
            <div class="col-4">
                
                <label for="txtAnual"><small>Anual</small></label>
                <input type="text" value="0.00" class="form-control" id="txtAnual" name="txtAnual" placeholder="Anual" required>

                                                    
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                
                <label><small>Descripcion </small><b>ES</b>
                
                    <button onclick="fntSetTraduccionIdioma('txtDescripcionEN', 'en', 'es');" class="btn btn-raised btn-sm btn-primary">Traducir a ES</button>
                
                </label>
                
                <label class="">
                    <div id="divtoolbarES" style="display: none;">
                       
                        <a class="badge badge-secondary" data-wysihtml-command="bold" title="CTRL+B" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_bold</i>
                        </a> 
                        <a class="badge badge-secondary" data-wysihtml-command="italic" title="CTRL+I" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_italic</i>
                        </a> 
                        <a class="badge badge-secondary" data-wysihtml-command="insertUnorderedList" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_list_bulleted</i>
                        </a> 
                        <a class="badge badge-secondary" data-wysihtml-command="insertOrderedList" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_list_numbered</i>
                            
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="alignLeftStyle" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_left</i>
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="alignCenterStyle" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_center</i>
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="alignRightStyle" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_right</i>
                            
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="justifyFull" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_justify</i>
                        </a> 
                        
                        
                      </div>
                </label>
                <textarea class="form-control form-control-sm border" id="txtDescripcionES" name="txtDescripcionES" placeholder="" rows="10"><?php print $arrDatosPlace["descripcion"]?></textarea>
                
                                                    
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                
                <label><small>Descripcion </small><b>EN</b>
                    <button onclick="fntSetTraduccionIdioma('txtDescripcionEN', 'es', 'en');" class="btn btn-raised btn-sm btn-primary">Traducir a EN</button>
                
                </label>
                <label class="">
                    <div id="divtoolbarEN" style="display: none;">
                       
                        <a class="badge badge-secondary" data-wysihtml-command="bold" title="CTRL+B" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_bold</i>
                        </a> 
                        <a class="badge badge-secondary" data-wysihtml-command="italic" title="CTRL+I" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_italic</i>
                        </a> 
                        <a class="badge badge-secondary" data-wysihtml-command="insertUnorderedList" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_list_bulleted</i>
                        </a> 
                        <a class="badge badge-secondary" data-wysihtml-command="insertOrderedList" style="font-weight: 100 !important;">
                            <i class="material-icons " style="font-weight: 100 !important;">format_list_numbered</i>
                            
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="alignLeftStyle" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_left</i>
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="alignCenterStyle" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_center</i>
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="alignRightStyle" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_right</i>
                            
                            
                        </a> 
                        
                        <a class="badge badge-secondary" data-wysihtml-command="justifyFull" style="font-weight: 100 !important;">
                            
                            <i class="material-icons " style="font-weight: 100 !important;">format_align_justify</i>
                        </a> 
                        
                        
                      </div>
                </label>
                <textarea class="form-control form-control-sm border" id="txtDescripcionEN" name="txtDescripcionEN" placeholder="" rows="10"><?php print $arrDatosPlace["descripcion"]?></textarea>
                
                                                    
            </div>
        </div>
        </form>
        <script src="dist/nueva_textarea/dist/wysihtml.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.all-commands.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.table_editing.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.toolbar.js"></script>

        <script src="dist/nueva_textarea/parser_rules/advanced_and_extended.js"></script>
        <script>
        
            var editorES; 
            var editorEN; 
            
            $(document).ready(function() { 
                
                editorES = new wysihtml.Editor("txtDescripcionES", {
                    toolbar:      "divtoolbarES",
                    parserRules:  wysihtmlParserRules,
                    pasteParserRulesets: wysihtmlParserPasteRulesets
                  });
                
                editorEN = new wysihtml.Editor("txtDescripcionEN", {
                    toolbar:      "divtoolbarEN",
                    parserRules:  wysihtmlParserRules,
                    pasteParserRulesets: wysihtmlParserPasteRulesets
                  });
                
                //$('#txtDescripcionEN').val('new content', true);

                    
            });
            
            function fntSetTraduccionIdioma(idObjetoTexarea, strIdiomaOrigen, strIdiomaTraduccion){
                
                editorES.val("ssssssssssss", true);
                editorEN.val("ssssssssssss", true);
                
                return false;
                        
                var formData = new FormData(document.getElementById("frmAprobar"));
                
                formData.append("strIdiomaOrigen", strIdiomaOrigen);
                formData.append("strIdiomaTraduccion", strIdiomaTraduccion);
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?getTraduccionIdioma=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $("#"+idObjetoTexarea).val("sdddddddddd");
                        
                        
                    }
                            
                });                
                
                                
            }
            
        </script>
        <?php
        
    }
    
    public function drawConfiguracion($strAction, $intSolPlace){
        
        $arrSolicitud = $this->objModel->getSolicitudDatos($intSolPlace);
        
        ?>
        
        <div class="row">
            
            <div class="col-6">
                <label><small class="font-weight-bold">Descripcion</small></label><br>
                <?php print $arrSolicitud["descripcion"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">add_fecha</small></label><br>
                <?php print $arrSolicitud["add_fecha"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">nombre</small></label><br>
                <?php print $arrSolicitud["nombre"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">telefono</small></label><br>
                <?php print $arrSolicitud["telefono"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">Vendedor</small></label><br>
                <?php print $arrSolicitud["codigo"]?>
            </div>
                        
            <div class="col-6">
                <label><small class="font-weight-bold">place_id</small></label><br>
                <?php print $arrSolicitud["place_id"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">telefono_propietario</small></label><br>
                <?php print $arrSolicitud["telefono_propietario"]?>
            </div>
            <div class="col-6">
                <label><small class="font-weight-bold">email_propietario</small></label><br>
                <?php print $arrSolicitud["email_propietario"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">tipo_negocio</small></label><br>
                <?php print $arrSolicitud["tipo_negocio"]?>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">Patente</small></label><br>
                <button onclick="window.open('imgInguate.php?t=up&src=../../file_inguate/place/<?php print $arrSolicitud["url_patente"]?>')" class="btn btn-primary btn-sm btn-raised" >Ver</button>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">DPI</small></label><br>
                <button onclick="window.open('imgInguate.php?t=up&src=../../file_inguate/place/<?php print $arrSolicitud["url_dpi"]?>')" class="btn btn-primary btn-sm btn-raised" >Ver</button>
            </div>
            
            <div class="col-6">
                <label><small class="font-weight-bold">SA</small></label><br>
                <button onclick="window.open('imgInguate.php?t=up&src=../../file_inguate/place/<?php print $arrSolicitud["url_sa"]?>')" class="btn btn-primary btn-sm btn-raised" >Ver</button>
            </div>
            
        </div>
        
        <?php
        
    }
    
    public function fntDrawFormNewPlace($strAction, $intPlace = 0, $intPaso = 1){
        
        $intUsuario = $_SESSION["_open_antigua"]["core"]["id_usuario"];
        
        $arrClasificacion = $this->objModel->getClasificacion();
        $arrInfoPlace = $this->objModel->getUsuarioPlaceaa($intUsuario, $intPlace);
        $arrInfoPlace = isset($arrInfoPlace[$intPlace]) ? $arrInfoPlace[$intPlace] : array();
        
        $arrPlaceClasificacion = $this->objModel->getPlaceClasificacion($intPlace);
        $arrPlaceClasificacionPadre = $this->objModel->getPlaceClasificacionPadre($intPlace);
        $arrRestriccionUsuario = $this->objModel->getRestriccionUsuario($_SESSION["_open_antigua"]["core"]["id_usuario"]);
        
        $arrCatalogoUbicacion = fntUbicacion();
        $arrCatalogoUbicacionLugar = fntUbicacionLugar();
        
        $arrCatalogoTipoPlace = fntCatalogoTipoPlace(); 
        
        $arrSolPlace = $this->objModel->fntGetSolPlace($intPlace);
        
        $arrCatalogoRestrinccionPlace = $this->objModel->getPlaceRestrinccionUsuario(); 
        $arrCatalogoPlaceCombo = $this->objModel->getPlaceCombo(); 
        
        ?>
          
        <link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 
        <script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
        <link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
            <link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> <!-- for live demo page -->
            <script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
            
       
        <div class="row justify-content-center">
            <div class="col-12 " style="">
                       
                <form onsubmit="return false;" id="frmPlace" method="POST">
                    <input type="hidden" name="hidIdUsuario" value="<?php print $arrInfoPlace["id_usuario"]?>">
                    <input type="hidden" name="hidIdPlace" value="<?php print $intPlace?>">
                    
                    
                    <div class="row p-0 m-0 justify-content-center" id="">
                                                                                       
                        <div class="col-12 " id="divDatos">    
                            
                            <div class="row">
                                <div class="col-12 mt-2">
                                    <label for="txtTitulo"  id="lbtxtTitulo"><small><?php print lang["nombre_de_tu_negocio"]?></small></label>
                                    <input type="text" value="<?php print isset($arrInfoPlace["titulo"]) ? $arrInfoPlace["titulo"] : ""?>" class="form-control form-control-sm   " id="txtTitulo" name="txtTitulo" placeholder="">
                                    <input type="hidden" id="hidNegocio" name="hidNegocio" value="0">    
                          
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-4">
                                    <label for="txtDescripcion" id="lbtxtDescripcion"><small><?php print lang["que_hace_tu_negocio"]?></small></label>
                                    <textarea class="form-control form-control-sm" id="txtDescripcion" name="txtDescripcion" placeholder="Descripción" rows="2"><?php print isset($arrSolPlace["descripcion"]) ? $arrSolPlace["descripcion"] : ""?></textarea>
                            
                                </div>
                            </div>
                            
                            <div class="row">
                            
                                <div class="col-6 mt-4">
                                    <label for="slcRestriccion" id="lbslcRestriccion"><small>Restrinccion</small></label>
                                    <select class="custom-select custom-select-sm " name="slcRestriccion" id="slcRestriccion">
                                        <option value=""><?php print lang["seleccione_una_opcion"]?></option>
                                                    
                                        <?php
                                
                                        while( $arrTMP = each($arrCatalogoRestrinccionPlace) ){
                                            
                                            $strSelected = $arrInfoPlace["id_usuario_place_restrinccion"] == $arrTMP["key"] ? "selected" : "";
                                            
                                            ?>
                                            
                                            <option <?php print $strSelected?> value="<?php print $arrTMP["key"]?>"><?php print $arrTMP["value"]["max_place"] ?></option>
                                            
                                            <?php
                                            
                                        }
                                                
                                        
                                        ?>
                                            
                                    </select>         
                                </div>
                            
                                <div class="col-6 mt-4">
                                    <label for="slcUbicacionLugar" id="lbslcUbicacionLugar"><small><?php print lang["region"]?></small></label>
                                    <select class="custom-select custom-select-sm " name="slcUbicacionLugar" id="slcUbicacionLugar">
                                        <option value=""><?php print lang["seleccione_una_opcion"]?></option>
                                                    
                                        <?php
                                        
                                        while( $rTMP = each($arrCatalogoUbicacion) ){
                                            ?>
                                            <optgroup label="<?php print $rTMP["value"]["tag_es"]?>">
                                                <?php
                                                
                                                while( $arrTMP = each($arrCatalogoUbicacionLugar[$rTMP["key"]]["lugar"]) ){
                                                    
                                                    ?>
                                                    
                                                    <option selected="" value="<?php print $arrTMP["key"]?>"><?php print sesion["lenguaje"] == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?></option>
                                                    
                                                    <?php
                                                    
                                                }
                                                
                                                ?>
                                            </optgroup>
                                            <?php
                                        }
                                        
                                        ?>
                                            
                                    </select>         
                                </div>
                            </div>
                            
                            <div class="row">
                            
                                <div class="col-6 mt-4">
                                    <label for="slcComboPlace" id="lbslcComboPlace"><small>Combo Place</small></label>
                                    <select class="custom-select custom-select-sm " name="slcComboPlace" id="slcComboPlace">
                                        <option value=""><?php print lang["seleccione_una_opcion"]?></option>
                                                    
                                        <?php
                                
                                        while( $arrTMP = each($arrCatalogoPlaceCombo) ){
                                            
                                            $strSelected = $arrInfoPlace["id_place_combo"] == $arrTMP["key"] ? "selected" : "";
                                            
                                            ?>
                                            
                                            <option <?php print $strSelected?> value="<?php print $arrTMP["key"]?>"><?php print $arrTMP["value"]["nombre"] ?></option>
                                            
                                            <?php
                                            
                                        }
                                                
                                        
                                        ?>
                                            
                                    </select>         
                                </div>
                            
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-4">
                                
                                    <div class="form-group">
                                        <label for="slcClasificacionPrincipal" id="lbslcClasificacionPrincipal"><small><?php print lang["clasificacion_principal"]?></small></label>
                                        <div class="input-group">
                                            <select id="slcClasificacionPrincipal" name="slcClasificacionPrincipal[]" class="form-control form-control-sm select2-multiple" multiple onchange="fntDrawClas2();">
                                                <?php
                                            
                                                while( $rTMP = each($arrClasificacion) ){
                                                    
                                                    $strSelected = isset($arrPlaceClasificacion[$rTMP["key"]]) ? " selected " : "";                                        
                                                    ?>
                                                    <option <?php print $strSelected;?> value="<?php print $rTMP["value"]["id_clasificacion"]?>"><?php print sesion["lenguaje"] == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                                                    <?php
                                                    
                                                }
                                                
                                                ?>
                                            </select>
                                            <button class="btn btn-raised btn-sm btn-success ml-1 " id="" onclick="fntDrawClas2();" style="display: none;">Next</button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <div id="divClasificacionSecundaria"></div>
                            
                            <div class="row justify-content-center justify-content-lg-end" style="display: none;">
                                
                                <div class="col-6 col-lg-4">
                                
                                    <button class="btn btn-primary btn-raised btn-block" onclick="fntCrearPlace();"><?php print lang["siguiente"]?></button>
                                
                                </div>
                                
                            </div>
                            
                            
                        </div>
                        
                    </div>     
                    
                    <div class="row">
                        <div class="col-12 text-right">
                            
                            <button class="btn btn-raised btn-primary" onclick="fntCrearPlace();">Guardar</button>
                            
                        </div>             
                    </div>             
                         
                </form>
                
            </div>    
        </div>
        
        <script>
        
            $( document ).ready(function() {
                
                $("#hidIdPlaceGeneral").val("<?php print fntCoreEncrypt($intPlace)?>") ;
                      
                $("#slcClasificacionPrincipal").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#slcClasificacionPrincipal").change(function (){
                    
                    $("#btnNextClasificacionPrincipal").show();
                    $("#divClasificacionSecundaria").html("");
                    $("#divClasificacionDetalle").html("");
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                var xhr;
                $('input[name="txtTitulo"]').autoComplete({
                    minChars: 2,
                    source: function(term, suggest){
                        
                        try {
                            xhr.abort();
                        }
                        catch(error) {
                        }

                        xhr = $.ajax({
                            url: "<?php print $strAction?>?getNegocioAutoComplete=true&q="+$("#txtTitulo").val(), 
                            dataType : "json",
                            success: function(result){
                                suggest(result);
                            }
                        });
                        
                    },
                    renderItem: function (item, search){
                        
                        return '<div class="autocomplete-suggestion" data-nombre="'+item["nombre"]+'" data-lat="'+item["lat"]+'" data-lng="'+item["lng"]+'"  data-identificador="'+item["identificador"]+'"  style="direction: ltr;" >'+item["nombre"]+'</div>';
                    
                    },
                    onSelect: function(e, term, item){
                        
                        $("#txtTitulo").val(item.data("nombre"));
                        $("#hidNegocio").val(item.data("identificador"));
                        $("#txtTitulo").val(item.data("nombre"));
                  
                        
                        console.log(item.data("lat"));
                        console.log(item.data("lng"));
                        //markerInicial.setPosition(new google.maps.LatLng(item.data("lat"), item.data("lng")))
                        
                        
                        return false;
                        
                    }
                    
                });
                
                
                
                <?php
                
                if( $intPlace ){
                    ?>
                    fntDrawClas2();
                    <?php
                }
                
                ?>
                
                   
                
                
            });
            
            function fntEditPlace(){
                
                <?php
                
                if( count($arrPlaceClasificacionPadre) > 0 ){
                    
                    while( $rTMP = each($arrPlaceClasificacionPadre) ){
                        ?>
                        
                        arr = $("#slcClasificacionPadre").val();
                        
                        boolExiste = false;
                        
                        $.each(arr, function( index, value ) {
                          
                            if( value == "<?php print $rTMP["key"]?>" ){
                                boolExiste = true;    
                            }
                            
                        });
                        
                        if( !boolExiste ){
                            
                            swal({
                                title: "Are you sure?",
                                text: "The change of classification affects the products that exist",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "#FF0000",
                                confirmButtonText: "Si",
                                cancelButtonText: "No",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    
                                    fntSavePlace(true);
                                
                                } 
                            });
                            
                            return false;
                            
                        }
                        
                        <?php
                    }                
                }
                
                ?>
                
                fntSavePlace(false);
            
            }
            
            function fntSavePlace(boolChangeProduct){
                
                
                var boolError = false;
                    
                if( $("#txtTelefonoNegocio").val() == "" ){
                    
                    $("#lbtxtTelefonoNegocio").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTelefonoNegocio").removeClass("text-danger");
                    
                }
                
                if( $("#txtNombreCompletoDuenio").val() == "" ){
                    
                    $("#lbtxtNombreCompletoDuenio").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtNombreCompletoDuenio").removeClass("text-danger");
                    
                }
                
                
                if( $("#txtTelefonoDuenio").val() == "" ){
                    
                    $("#lbtxtTelefonoDuenio").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTelefonoDuenio").removeClass("text-danger");
                    
                }
                
                if( $("#txtEmailPropietario").val() == "" ){
                    
                    $("#lbtxtEmailPropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtEmailPropietario").removeClass("text-danger");
                    
                }
                
                var intSizeFile = $("#flDPI")[0].files.length;
                if( intSizeFile === 0){
                               
                    $("#lbflDPI").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbflDPI").removeClass("text-danger");
                        
                }
                
                var intSizeFile = $("#flPatente")[0].files.length;
                if( intSizeFile === 0){
                               
                    $("#lbflPatente").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbflPatente").removeClass("text-danger");
                        
                }
                
                if( $("#slcTipoNegocio").val() == 2 ){
                    
                    var intSizeFile = $("#flSA")[0].files.length;
                    if( intSizeFile === 0){
                                   
                        $("#lbflSA").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbflSA").removeClass("text-danger");
                            
                    }
                    
                }
                
                if( boolError ){
                        
                    $.snackbar({
                        content: "<?php print lang["completa_los_datos_obligatorios"]?>",
                        timeout: 1500
                    });
                    
                    return false;

                }                 
                
                var formData = new FormData(document.getElementById("frmPlace"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setSavePlace=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType : 'json',
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["action"] == "1" ){
                            
                            alert("Datos Guardados")
                            
                            //setPasoCreacionPlace(true, true, false);
                
                            //fntDrawFormPlacePagina(result["place"]);
                            
                                
                        }
                        else{
                            
                            fntCargarPaginaActiva();
                            
                        }
                        
                            
                    }
                            
                });
                
            }
            
            function fntDrawClas2(){
                             
                intCountSelec = 0;
                strClass1 = "";
                $("#slcClasificacionPrincipal option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    intCountSelec++;    
                });          
                
                if( intCountSelec > "<?php print $intPlace ? count($arrClasificacion) : $arrRestriccionUsuario["max_place"]?>" ){
                    
                    $.snackbar({
                        content: "Select Only 1 option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                }
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                }    
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas2=true&place=<?php print $intPlace;?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionSecundaria").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            function fntShowDatos(strDivActual, strDivSiguiente, intPantallaValidacion){
                      
                intPantallaValidacion = !intPantallaValidacion ? 0 : intPantallaValidacion;
                
                if( intPantallaValidacion == 2 ){
                    
                    var boolError = false;
                    
                    if( $("#txtTitulo").val() == "" ){
                        
                        $("#lbtxtTitulo").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbtxtTitulo").removeClass("text-danger");
                        
                    }
                    
                    if( $("#txtDescripcion").val() == "" ){
                        
                        $("#lbtxtDescripcion").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbtxtDescripcion").removeClass("text-danger");
                        
                    }
                    
                    
                    if( $("#slcUbicacionLugar").val() == "" ){
                        
                        $("#lbslcUbicacionLugar").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbslcUbicacionLugar").removeClass("text-danger");
                        
                    }
                    
                    
                    intCountSelec = 0;
                    $("#slcClasificacionPrincipal option:selected").each(function(){
                        
                        intCountSelec++;    
                    });          
                    
                    if( intCountSelec == 0 ){
                        
                        $("#lbslcClasificacionPrincipal").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbslcClasificacionPrincipal").removeClass("text-danger");
                        
                    }
                    
                    intCountSelec = 0; 
                    $("#slcClasificacionPadre option:selected").each(function(){
                        
                        intCountSelec++;    
                    });          
                    
                    if( intCountSelec == 0 ){
                        
                        $("#lbslcClasificacionPadre").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbslcClasificacionPadre").removeClass("text-danger");
                        
                    }
                    
                    if( boolError ){
                        
                        $.snackbar({
                            content: "<?php print lang["completa_los_datos_obligatorios"]?>",
                            timeout: 1500
                        });
                        
                        return false;

                    } 
                        
                    
                    
                }
                
                
                
                $("#"+strDivSiguiente).show();
                $("#"+strDivActual).hide();
            }
            
            function fntTipoPlace(){
                
                if( $("#slcTipoNegocio").val() == 2 ){
                    
                    $("#divFileSA").show();
                    
                }   
                else{
                    
                    $("#divFileSA").hide();
                    
                } 
                
            }
            
            function fntCrearPlace(){
                
                var boolError = false;
                
                if( $("#txtTitulo").val() == "" ){
                    
                    $("#lbtxtTitulo").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTitulo").removeClass("text-danger");
                    
                }
                
                if( $("#txtDescripcion").val() == "" ){
                    
                    $("#lbtxtDescripcion").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtDescripcion").removeClass("text-danger");
                    
                }
                
                if( $("#slcUbicacionLugar").val() == "" ){
                    
                    $("#lbslcUbicacionLugar").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbslcUbicacionLugar").removeClass("text-danger");
                    
                }
                
                intCountSelec = 0;
                $("#slcClasificacionPrincipal option:selected").each(function(){
                    
                    intCountSelec++;    
                });          
                
                if( intCountSelec == 0 ){
                    
                    $("#lbslcClasificacionPrincipal").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbslcClasificacionPrincipal").removeClass("text-danger");
                    
                }
                
                intCountSelec = 0; 
                $("#slcClasificacionPadre option:selected").each(function(){
                    
                    intCountSelec++;    
                });          
                
                if( intCountSelec == 0 ){
                    
                    $("#lbslcClasificacionPadre").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbslcClasificacionPadre").removeClass("text-danger");
                    
                }
                
                if( boolError ){
                    
                    $.snackbar({
                        content: "<?php print lang["completa_los_datos_obligatorios"]?>",
                        timeout: 1500
                    });
                    
                    return false;

                }
                else{
                    
                    var formData = new FormData(document.getElementById("frmPlace"));
                        
                    $(".preloader").fadeIn();
                    $.ajax({
                        url: "<?php print $strAction?>?setCrearPlace=true", 
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType : 'json',
                        success: function(result){
                            
                            $(".preloader").fadeOut();
                            $("#modalSolicitudPlaceContenido").html("");
                            $("#modalSolicitudPlace").modal("hide");
                    
                            
                                
                        }
                                
                    });
                    
                    
                }
                
            }
            
        </script>
        <?php
                
    }
    
    public function fntDrawClas2($strAction, $strKey, $intPlace = 0){
        
        $arrInfo = $this->objModel->getClasificacionPadre($strKey);
        $arrPlaceClasificacionPadre = $this->objModel->getPlaceClasificacionPadre($intPlace);
        
        ?>
        
        <div class="row">
            <div class="col-12 mt-4">
            
                <div class="form-group">
                    <label for="slcClasificacionPadre" id="lbslcClasificacionPadre"><small><?php print lang["clasificacion_secundaria"]?></small></label>
                    <div class="input-group">
                        <select id="slcClasificacionPadre" name="slcClasificacionPadre[]" class="form-control-sm select2-multiple" multiple>
                            <?php
                        
                            while( $rTMP = each($arrInfo) ){
                                
                                ?>
                                <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                    <?php
                                    
                                    while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                        
                                        $strSelected = isset($arrPlaceClasificacionPadre[$arrTMP["key"]]) ? " selected " : "";                                        
                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre"]?>"><?php print sesion["lenguaje"] == "es" ?  $arrTMP["value"]["tag_esPadre"] : $arrTMP["value"]["tag_enPadre"]?></option>
                                
                                        <?php
                                    }
                                    
                                    ?>
                                </optgroup>
                                <?php
                                
                            }
                            
                            ?>
                        </select>
                        <button class="btn btn-raised btn-sm btn-success ml-1 hide" id="btnNextClasificacionPrincipalPadred" style="display: none" onclick="fntDrawClas3();">Next</button>
                    </div>
                </div>
                
            </div>
            
        </div>
        <script>
            
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPadre").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#btnNextClasificacionPrincipal").hide();
                
                $("#slcClasificacionPadre").change(function (){
                    
                    //$("#btnNextClasificacionPrincipalPadre").show();
                    $("#divClasificacionDetalle").html("");
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                <?php
                
                if( $intPlace ){
                    ?>
                    //fntDrawClas3();
                    <?php
                }
                
                ?>
                
            });
            
            function fntDrawClas3(){
                             
                strClass1 = "";
                $("#slcClasificacionPadre option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    
                });          
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                } 
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas3=true&place=<?php print $intPlace?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionDetalle").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            
        </script>
        
        <?php        
        
    }
    
    
    
}

?>
