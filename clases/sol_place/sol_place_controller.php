<?php
use \Statickidz\GoogleTranslate;
  
require_once "sol_place_model.php";
require_once "sol_place_view.php";

class sol_place_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new sol_place_model();
        $this->objView = new sol_place_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["setRezhazoSolicitud"]) ){
            
            $intSolPlace = isset($_POST["hidKeyRechazo"]) ? intval($_POST["hidKeyRechazo"]) : 0;
            $strMotivo = isset($_POST["txtMotivoModal"]) ? addslashes($_POST["txtMotivoModal"]) : 0;
            
            $this->objModel->setRezhazoSolicitud($intSolPlace, $strMotivo);
                        
            die();
        }
        
        if( isset($_GET["setAprobadoPagoSolicitud"]) ){
            
            $intSolPlace = isset($_POST["hidKeyAprobar"]) ? intval($_POST["hidKeyAprobar"]) : 0;
            $intCategoriaNegocio = isset($_POST["slCategoriaNegocio"]) ? intval($_POST["slCategoriaNegocio"]) : 0;
            $intPaqueteProducto = isset($_POST["slPaqueteProducto"]) ? intval($_POST["slPaqueteProducto"]) : 0;
            $sinCostoMensual = isset($_POST["txtMensual"]) ? floatval($_POST["txtMensual"]) : 0;
            $sinCostoSemestral = isset($_POST["txtSemestral"]) ? floatval($_POST["txtSemestral"]) : 0;
            $sinCostoAnual = isset($_POST["txtAnual"]) ? floatval($_POST["txtAnual"]) : 0;
            
            $this->objModel->setAprobacionPagoSolicitud($intSolPlace, $intCategoriaNegocio, $intPaqueteProducto, 
                                                        $sinCostoMensual, $sinCostoSemestral, $sinCostoAnual);
                        
            die();
        }
        
        if( isset($_GET["drawSolicitud"]) ){
            
            $intSolPlace = isset($_GET["sol_place"]) ? intval($_GET["sol_place"]) : 0;
            
            $this->objView->drawSolicitud($intSolPlace);
                        
            die();
        }
        
        if( isset($_GET["drawAprobacionSolicitud"]) ){
            
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : 0;
            $intSolPlace = isset($_GET["sol_place"]) ? intval($_GET["sol_place"]) : 0;
            
            $this->objView->drawAprobacionSolicitud($strAction, $intPlace, $intSolPlace);
            
            die();
            
        }
        
        if( isset($_GET["getTraduccionIdioma"]) ){
            
            $strTextoES = isset($_POST["txtDescripcionES"]) ? $_POST["txtDescripcionES"] : "";
            $strTextoEN = isset($_POST["txtDescripcionEN"]) ? $_POST["txtDescripcionEN"] : "";
            $strIdiomaOrigen = isset($_POST["strIdiomaOrigen"]) ? $_POST["strIdiomaOrigen"] : "";
            $strIdiomaTraduccion = isset($_POST["strIdiomaTraduccion"]) ? $_POST["strIdiomaTraduccion"] : "";
            
            require_once ('dist_interno/test_trans/vendor/autoload.php');
            
            $source = 'es';
            $target = 'en';
            $text = 'buenos días, como estas';

            $trans = new GoogleTranslate();
            $result = $trans->translate($strIdiomaOrigen, $strIdiomaTraduccion,  ( $strIdiomaTraduccion == "es" ? $strTextoES : $strTextoEN ) );

            print $result;
            
            die();
            
        }
        
        if( isset($_GET["drawConfiguracion"]) ){
            
            $intSolPlace = isset($_GET["sol_place"]) ? intval($_GET["sol_place"]) : 0;
            
            $this->objView->drawConfiguracion($strAction, $intSolPlace);
                        
            die();
        }
        
        if( isset($_GET["fntDrawFormNewPlace"]) ){
            
            $intPlace = isset($_GET["key"]) ? trim(($_GET["key"])) : "";
            $intPaso = isset($_GET["paso"]) ? intval(($_GET["paso"])) : "";
            
            $this->objView->fntDrawFormNewPlace($strAction, $intPlace, $intPaso);
            
            die();
            
        }
        
        if( isset($_GET["getDrawClas2"]) ){
            
            $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : "";
            
            $this->objView->fntDrawClas2($strAction, $strKey, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["getNegocioAutoComplete"]) ){
            
            $strTexto = isset($_GET["q"]) ? addslashes($_GET["q"]) : "";
            
            $arr = $this->objModel->getNegocioAutocomplete($strTexto);
            
            print json_encode($arr);            
            
            die();
        }
        
        if( isset($_GET["setCrearPlace"]) ){
            
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : 0;
            $intNegocio = isset($_POST["hidNegocio"]) ? intval(fntCoreDecrypt($_POST["hidNegocio"])) : 0;
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : 0;
            $intPlace = isset($_POST["hidIdPlace"]) ? intval($_POST["hidIdPlace"]) : 0;
            $strTitulo = isset($_POST["txtTitulo"]) ? addslashes(fntCoreClearToQuery($_POST["txtTitulo"])) : "";
            $strNombreCompletoDuenio = isset($_POST["txtNombreCompletoDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtNombreCompletoDuenio"])) : "";
            $strTelefono = isset($_POST["txtTelefonoNegocio"]) ? addslashes(fntCoreClearToQuery($_POST["txtTelefonoNegocio"])) : "";
            $strEmail = isset($_POST["txtEmailDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtEmailDuenio"])) : "";
            $strLat = isset($_POST["hidLatUbicacion"]) ? addslashes($_POST["hidLatUbicacion"]) : "";
            $strLng = isset($_POST["hidLngUbicacion"]) ? addslashes($_POST["hidLngUbicacion"]) : "";
            $strPlaceId = isset($_POST["hidPlaceId"]) ? addslashes($_POST["hidPlaceId"]) : "";
            $strDescripcionNegocio = isset($_POST["txtDescripcion"]) ? addslashes(fntCoreClearToQuery($_POST["txtDescripcion"])) : "";
            $intCantidadProducto = isset($_POST["txtCantidadProductos"]) ? intval($_POST["txtCantidadProductos"]) : "";
            $intCantidadGaleria = isset($_POST["txtCantidadGaleria"]) ? intval($_POST["txtCantidadGaleria"]) : "";
            $intUbicacionLugar = isset($_POST["slcUbicacionLugar"]) ? intval($_POST["slcUbicacionLugar"]) : "";
            $intRestrinccion = isset($_POST["slcRestriccion"]) ? intval($_POST["slcRestriccion"]) : "";
            $intPlaceCombo = isset($_POST["slcComboPlace"]) ? intval($_POST["slcComboPlace"]) : "";
            
            $intUbicacionLugar = isset($_POST["slcUbicacionLugar"]) ? intval($_POST["slcUbicacionLugar"]) : "";
            
            $strTelefonoDuenio = isset($_POST["txtTelefonoDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtTelefonoDuenio"])) : "";
            $strEmailDuenio = isset($_POST["txtEmailPropietario"]) ? addslashes(fntCoreClearToQuery($_POST["txtEmailPropietario"])) : "";
            $intTipoNegocio = isset($_POST["slcTipoNegocio"]) ? intval($_POST["slcTipoNegocio"]) : "";
                  
            if( !$intPlace ){
                
                $intPlace = $this->objModel->setPlace($intUsuario, $strTitulo, $intCantidadProducto, $intCantidadGaleria, $strDescripcionNegocio,
                                                      $strNombreCompletoDuenio, $strTelefono, $strEmail, $strLat, $strLng, $strPlaceId, $intUbicacionLugar,
                                                      $strTelefonoDuenio, $strEmailDuenio, $intTipoNegocio, $intNegocio);
                $arr["action"] = "1";
                $arr["place"] = fntCoreEncrypt($intPlace);
                  /*
                if( isset($_FILES["flPatente"]) && $_FILES["flPatente"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flPatente"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PC_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flPatente"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlace($intPlace, $strUrlArchivo);
                            
                }
            
                if( isset($_FILES["flDPI"]) && $_FILES["flDPI"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flDPI"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCdpi_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flDPI"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlaceDPI($intPlace, $strUrlArchivo);
                            
                }
            
                if( isset($_FILES["flSA"]) && $_FILES["flSA"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flSA"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCsa_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flSA"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlaceSA($intPlace, $strUrlArchivo);
                            
                }
                 */
            }
            else{
                
                $this->objModel->setEditPlace($intPlace, $strTitulo, $strDescripcionNegocio, $intRestrinccion, $intUsuario, $intPlaceCombo);
                $arr["action"] = "1";
                $arr["place"] = fntCoreEncrypt($intPlace);
            }
            
            $this->objModel->setDeletePlaceClasificacion($intPlace);
            if( isset($_POST["slcClasificacionPrincipal"]) && is_array($_POST["slcClasificacionPrincipal"]) && count($_POST["slcClasificacionPrincipal"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPrincipal"]) ){
                    
                    $this->objModel->setPlaceClasificacion($intPlace, $rTMP["value"]);
                    
                }
                
            }
            
            $this->objModel->setDeletePlaceClasificacionPadre($intPlace);
            if( isset($_POST["slcClasificacionPadre"]) && is_array($_POST["slcClasificacionPadre"]) && count($_POST["slcClasificacionPadre"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPadre"]) ){
                    
                    $this->objModel->setPlaceClasificacionPadre($intPlace, $rTMP["value"]);
                    
                }
                
            }      
            
            print json_encode($arr);
            
            die();
        }
                     
    }
    
}

  
?>
