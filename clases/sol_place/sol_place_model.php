<?php
 
class sol_place_model{
      
    var $objDBClass;
    
    public function __construct(){
        
        $this->objDBClass = new dbClass(); 
            
    }
    
    public function getobjDBClass(){
        return $this->objDBClass;
    }
    
    public function setCloseDB(){
        
        $this->objDBClass->db_close();
    
    }
    
    public function getSolPlace($intIdUsuario){
        
        $strQuery = "SELECT sol_place.id_sol_place,
                            sol_place.id_usuario,
                            sol_place.id_place,
                            sol_place.descripcion,
                            sol_place.estado,
                            
                            sol_place.nombre nombre_propietario,
                            sol_place.telefono telefono_propietario,
                            sol_place.email_propietario,
                            sol_place.tipo_negocio,
                            
                            usuario.nombre,
                            usuario.email,
                            usuario.telefono telefonoUsuario,
                            place.titulo,
                            place.telefono,
                            place.url_place
                     FROM   sol_place,
                            usuario,
                            place
                     WHERE  sol_place.id_usuario = usuario.id_usuario
                     AND    place.id_place = sol_place.id_place
                     ";
        $arr = array();
        $arrCatalogo = fntGetCatalogoSolicitudPlace();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arr[$rTMP["id_sol_place"]]["id_sol_place"] = $rTMP["id_sol_place"];
            $arr[$rTMP["id_sol_place"]]["id_usuario"] = $rTMP["id_usuario"];
            $arr[$rTMP["id_sol_place"]]["id_place"] = $rTMP["id_place"];
            $arr[$rTMP["id_sol_place"]]["descripcion"] = $rTMP["descripcion"];
            $arr[$rTMP["id_sol_place"]]["estado"] = $rTMP["estado"];
            $arr[$rTMP["id_sol_place"]]["estado_text"] = $arrCatalogo[$rTMP["estado"]]["nombre"];
            $arr[$rTMP["id_sol_place"]]["nombre"] = $rTMP["nombre"];
            $arr[$rTMP["id_sol_place"]]["email"] = $rTMP["email"];
            $arr[$rTMP["id_sol_place"]]["telefonoUsuario"] = $rTMP["telefonoUsuario"];
            $arr[$rTMP["id_sol_place"]]["titulo"] = $rTMP["titulo"];
            $arr[$rTMP["id_sol_place"]]["nombre_propietario"] = $rTMP["nombre_propietario"];
            $arr[$rTMP["id_sol_place"]]["telefono_propietario"] = $rTMP["telefono_propietario"];
            $arr[$rTMP["id_sol_place"]]["email_propietario"] = $rTMP["email_propietario"];
            $arr[$rTMP["id_sol_place"]]["tipo_negocio"] = $rTMP["tipo_negocio"];
            $arr[$rTMP["id_sol_place"]]["url_place"] = $rTMP["url_place"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arr;
                
    }
    
    public function setRezhazoSolicitud($intSolPlace, $strMotivo){
        
        $strQuery = "SELECT sol_place.id_sol_place,
                            sol_place.id_usuario,
                            sol_place.id_place,
                            sol_place.descripcion,
                            sol_place.estado,
                            usuario.nombre,
                            usuario.email,
                            place.titulo
                     FROM   sol_place,
                            usuario,
                            place
                     WHERE  sol_place.id_sol_place = {$intSolPlace}
                     AND    sol_place.id_usuario = usuario.id_usuario
                     AND    place.id_place = sol_place.id_place";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        $strQuery = "UPDATE sol_place
                     SET    estado = 'R'
                     WHERE  id_sol_place = {$intSolPlace}";
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "UPDATE place
                     SET    estado = 'R'
                     WHERE  id_place = {$intSolPlace}";
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "INSERT INTO sol_place_motivo (id_sol_place, motivo, estado)
                                        VALUES({$intSolPlace}, '{$strMotivo}', 'R')";                   
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "INSERT INTO usuario_alerta (id_usuario, tipo, identificador, mensaje)
                                          VALUES({$rTMP["id_usuario"]}, 'SPR', '{$rTMP["id_place"]}', 'Solicitud de Negocio Rechaza, ".substr($strMotivo, 0, 50)." ')";                   
        $this->objDBClass->db_consulta($strQuery);
        
        $strBody = "
                        <style type='text/css'>
                        /* latin-ext */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                        }
                        /* latin */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                        }

                          /* Take care of image borders and formatting */

                          img {
                              max-width: 600px;
                              outline: none;
                              text-decoration: none;
                              -ms-interpolation-mode: bicubic;
                          }

                        a {
                          text-decoration: none;
                          border: 0;
                          outline: none;
                        }

                          a img {
                              border: none;
                          }

                        td, h1, h2, h3  {
                          font-family: Helvetica, Arial, sans-serif;
                          font-weight: 400;
                        }

                        body {
                          -webkit-font-smoothing:antialiased;
                          -webkit-text-size-adjust:none;
                          width: 100%;
                          height: 100%;
                          color: #37302d;
                          background: #ffffff;
                        }

                        table {
                          background:
                        }

                        h1, h2, h3 {
                          padding: 0;
                          margin: 0;
                          color: #ffffff;
                          font-weight: 400;
                        }

                        h3 {
                          color: #21c5ba;
                          font-size: 24px;
                        }
                        </style>

                      <style type='text/css' media='screen'>
                        @media screen {
                          td, h1, h2, h3 {
                            font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                          }
                        }
                      </style>

                      <style type='text/css' media='only screen and (max-width: 480px)'>
                        @media only screen and (max-width: 480px) {

                          table[class='w320'] {
                            width: 320px !important;
                          }

                          table[class='w300'] {
                            width: 300px !important;
                          }

                          table[class='w290'] {
                            width: 290px !important;
                          }

                          td[class='w320'] {
                            width: 320px !important;
                          }

                          td[class='mobile-center'] {
                            text-align: center !important;
                          }

                          td[class='mobile-padding'] {
                            padding-left: 20px !important;
                            padding-right: 20px !important;
                            padding-bottom: 20px !important;
                          }
                        }
                      </style>
                    <table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' >
                      <tr>
                        <td align='center' valign='top' bgcolor='#ffffff'  width='100%'>

                        <table cellspacing='0' cellpadding='0' width='100%'>
                          <tr>
                            <td style='border-bottom: 3px solid #3bcdc3;' width='100%'>
                              <center>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                  <tr>
                                    <td valign='top' style='padding:10px 0; text-align:center;' class='mobile-center'>
                                      <img width='250' height='62' src='https://inguate.com/images/LogoInguate.png'>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td background='http://35.232.20.49/images/background_email.gif' bgcolor='#64594b' valign='top' style='background: url(http://35.232.20.49/images/background_email.gif) no-repeat center; background-color: #64594b; background-position: center;'>
                              <div>
                                <center>
                                  <table cellspacing='0' cellpadding='0' width='530' height='303' class='w320'>
                                    <tr>
                                      <td valign='middle' style='vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:center;' class='mobile-center' height='303'>
                                        
                                        <h1 style='font-weight: bold; color: white;'>
                                            ".$rTMP["nombre"]." tu solicitud de registro ha sido rechazada.
                                        </h1>
                                        <br>
                                        
                                      </td>
                                    </tr>
                                  </table>
                                </center>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td valign='top'>
                              <br>
                              <br>
                              
                              <center>
                                <table cellspacing='0' cellpadding='30' width='500' class='w290'>
                                          <tr>
                                            <td valign='top' style='border-bottom:1px solid #a1a1a1;'>
                                                      
                                              <center>
                                              <table style='margin: 0 auto;' cellspacing='0' cellpadding='8' width='500'>
                                                <tr>
                                                  <td style='text-align:;left; color: black;'>
                                                    
                                                    <p>
                                                        <b>".$rTMP["titulo"]."</b>
                                                        <br>
                                                        <br>
                                                        El motivo de tu rechazo es: <br>
                                                        
                                                        ".$strMotivo."
                                                    </p>
                                                    
                                                  </td>
                                                </tr>
                                              </table>
                                              </center>
                                            </td>
                                          </tr>
                                        </table>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                     
                                  <tr>
                                    <td class='mobile-padding' style='text-align: center;'>
                                      <table cellspacing='0' cellpadding='0' width='100%'>
                                        <tr>
                                          <td style='width:100%; text-align: center; background-color: #3bcdc3;'>
                                            <div>
                                            <table cellspacing='0' cellpadding='0' width='100%'><tr><td style='text-align: center;background-color:#3bcdc3;border-radius:0px;color:#ffffff;display:inline-block;font-family:'Lato', Helvetica, Arial, sans-serif;font-weight:bold;font-size:13px;line-height:33px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;mso-hide:all;'>
                                            <span style='color:#ffffff'><br>
                                            
                                            <a href='inguate.com' style='color:white;'><h1 >INGUATE</h1></a>
                                            
                                            <br></span></td></tr></table>
                                            
                                            </div>
                                          </td>
                                          <td>
                                            &nbsp;
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table cellspacing='0' cellpadding='25' width='100%'>
                                        <tr>
                                          <td>
                                            &nbsp;
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td style='background-color:#c2c2c2;'>
                                <br>
                                <br>
                            </td>
                          </tr>
                        </table>

                        </td>
                      </tr>
                    </table>
                    ";
        fntEnviarCorreo($rTMP["email"], "Solicitud de Registro Rechazada", $strBody);
                
    }
    
    public function setAceptarSolicitud($intSolPlace){
        
        $strQuery = "SELECT sol_place.id_sol_place,
                            sol_place.id_usuario,
                            sol_place.id_place,
                            sol_place.descripcion,
                            sol_place.estado,
                            usuario.nombre,
                            usuario.email,
                            place.titulo
                     FROM   sol_place,
                            usuario,
                            place
                     WHERE  sol_place.id_sol_place = {$intSolPlace}
                     AND    sol_place.id_usuario = usuario.id_usuario
                     AND    place.id_place = sol_place.id_place";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        $strQuery = "UPDATE sol_place
                     SET    estado = 'A'
                     WHERE  id_sol_place = {$intSolPlace}";
        $this->objDBClass->db_consulta($strQuery);    
        
        $strQuery = "UPDATE place
                     SET    estado = 'A'
                     WHERE  id_place = {$rTMP["id_place"]}";
        $this->objDBClass->db_consulta($strQuery);    
        
        $strQuery = "INSERT INTO usuario_alerta (id_usuario, tipo, identificador, mensaje)
                                          VALUES({$rTMP["id_usuario"]}, 'SPA', '{$rTMP["id_place"]}', 'Solicitud para ".$rTMP["titulo"]." ha sido Aprobada')";                   
        $this->objDBClass->db_consulta($strQuery);
        
        $strBody = "
                        <style type='text/css'>
                        /* latin-ext */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                        }
                        /* latin */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                        }

                          /* Take care of image borders and formatting */

                          img {
                              max-width: 600px;
                              outline: none;
                              text-decoration: none;
                              -ms-interpolation-mode: bicubic;
                          }

                        a {
                          text-decoration: none;
                          border: 0;
                          outline: none;
                        }

                          a img {
                              border: none;
                          }

                        td, h1, h2, h3  {
                          font-family: Helvetica, Arial, sans-serif;
                          font-weight: 400;
                        }

                        body {
                          -webkit-font-smoothing:antialiased;
                          -webkit-text-size-adjust:none;
                          width: 100%;
                          height: 100%;
                          color: #37302d;
                          background: #ffffff;
                        }

                        table {
                          background:
                        }

                        h1, h2, h3 {
                          padding: 0;
                          margin: 0;
                          color: #ffffff;
                          font-weight: 400;
                        }

                        h3 {
                          color: #21c5ba;
                          font-size: 24px;
                        }
                        </style>

                      <style type='text/css' media='screen'>
                        @media screen {
                          td, h1, h2, h3 {
                            font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                          }
                        }
                      </style>

                      <style type='text/css' media='only screen and (max-width: 480px)'>
                        @media only screen and (max-width: 480px) {

                          table[class='w320'] {
                            width: 320px !important;
                          }

                          table[class='w300'] {
                            width: 300px !important;
                          }

                          table[class='w290'] {
                            width: 290px !important;
                          }

                          td[class='w320'] {
                            width: 320px !important;
                          }

                          td[class='mobile-center'] {
                            text-align: center !important;
                          }

                          td[class='mobile-padding'] {
                            padding-left: 20px !important;
                            padding-right: 20px !important;
                            padding-bottom: 20px !important;
                          }
                        }
                      </style>
                    <table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' >
                      <tr>
                        <td align='center' valign='top' bgcolor='#ffffff'  width='100%'>

                        <table cellspacing='0' cellpadding='0' width='100%'>
                          <tr>
                            <td style='border-bottom: 3px solid #3bcdc3;' width='100%'>
                              <center>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                  <tr>
                                    <td valign='top' style='padding:10px 0; text-align:center;' class='mobile-center'>
                                      <img width='250' height='62' src='https://inguate.com/images/LogoInguate.png'>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td background='http://35.232.20.49/images/background_email.gif' bgcolor='#64594b' valign='top' style='background: url(http://35.232.20.49/images/background_email.gif) no-repeat center; background-color: #64594b; background-position: center;'>
                              <div>
                                <center>
                                  <table cellspacing='0' cellpadding='0' width='530' height='303' class='w320'>
                                    <tr>
                                      <td valign='middle' style='vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:center;' class='mobile-center' height='303'>
                                        
                                        <h1 style='font-weight: bold; color: white;'>
                                            ".$rTMP["nombre"]." tu solicitud de registro ha sido Aprobada.
                                        </h1>
                                        <br>
                                        
                                      </td>
                                    </tr>
                                  </table>
                                </center>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td valign='top'>
                              <br>
                              <br>
                              
                              <center>
                                <table cellspacing='0' cellpadding='30' width='500' class='w290'>
                                          <tr>
                                            <td valign='top' style='border-bottom:1px solid #a1a1a1;'>
                                                      
                                              <center>
                                              <table style='margin: 0 auto;' cellspacing='0' cellpadding='8' width='500'>
                                                <tr>
                                                  <td style='text-align:;left; color: black;'>
                                                    
                                                    <p>
                                                        <b>".$rTMP["titulo"]."</b>
                                                        <br>
                                                        <br>
                                                        Se encuentra activo en INGUATE
                                                    </p>
                                                    
                                                  </td>
                                                </tr>
                                              </table>
                                              </center>
                                            </td>
                                          </tr>
                                        </table>
                                
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td style='background-color:#c2c2c2;'>
                                <br>
                                <br>
                            </td>
                          </tr>
                        </table>

                        </td>
                      </tr>
                    </table>
                    ";
        fntEnviarCorreo($rTMP["email"], "Solicitud de Registro Rechazada", $strBody);
        
        
    }
    
    public function setAprobacionPagoSolicitud($intSolPlace, $intCategoriaNegocio, $intPaqueteProducto, 
                                                $sinCostoMensual, $sinCostoSemestral, $sinCostoAnual){
        
        $intSolPlace = intval($intSolPlace);
        $intCategoriaNegocio = intval($intCategoriaNegocio);
        $intPaqueteProducto = intval($intPaqueteProducto);
        $sinCostoMensual = floatval($sinCostoMensual);
        $sinCostoSemestral = floatval($sinCostoSemestral);
        $sinCostoAnual = floatval($sinCostoAnual);
        
        $strQuery = "SELECT sol_place.id_sol_place,
                            sol_place.id_usuario,
                            sol_place.id_place,
                            sol_place.descripcion,
                            sol_place.estado,
                            sol_place.id_negocio,
                            usuario.nombre,
                            usuario.email,
                            place.titulo
                     FROM   sol_place,
                            usuario,
                            place
                     WHERE  sol_place.id_sol_place = {$intSolPlace}
                     AND    sol_place.id_usuario = usuario.id_usuario
                     AND    place.id_place = sol_place.id_place";
             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        $strQuery = "UPDATE sol_place
                     SET    estado = 'A'
                     WHERE  id_sol_place = {$intSolPlace}";
        $this->objDBClass->db_consulta($strQuery);    
        
        $strQuery = "UPDATE negocio
                     SET    estado = 'U'
                     WHERE  id_negocio = {$intSolPlace}";
        $this->objDBClass->db_consulta($strQuery);    
        
        $strTituloBusqueda = fntGetNameEstablecimeintoFiltro($rTMP["titulo"], true, $this->getobjDBClass());
                            
        $strQuery = "UPDATE place
                     SET    estado = 'A',
                            texto_autocomplete = '{$strTituloBusqueda}'
                     WHERE  id_place = {$rTMP["id_place"]}";
        $this->objDBClass->db_consulta($strQuery);
        
        fntCarculoPrecio($rTMP["id_place"], true, $this->getobjDBClass());
        
                                                
        $strQuery = "INSERT INTO place_cobro(id_place, tipo_place, paquete_cobro, 
                                             cobro_mensual, cobro_semestral, cobro_anual, estado)
                                       VALUES({$rTMP["id_place"]}, {$intCategoriaNegocio}, {$intPaqueteProducto},
                                              {$sinCostoMensual}, {$sinCostoSemestral}, {$sinCostoAnual}, 'P')";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "INSERT INTO usuario_alerta (id_usuario, tipo, identificador, titulo, mensaje)
                                          VALUES({$rTMP["id_usuario"]}, 'SPP', '{$rTMP["id_place"]}', 'Solicitud de Negocio Aprobada','Pendiente de Pago')";                   
        $this->objDBClass->db_consulta($strQuery);
        
        
        $strBody = "
                        <style type='text/css'>
                        /* latin-ext */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                        }
                        /* latin */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                        }

                          /* Take care of image borders and formatting */

                          img {
                              max-width: 600px;
                              outline: none;
                              text-decoration: none;
                              -ms-interpolation-mode: bicubic;
                          }

                        a {
                          text-decoration: none;
                          border: 0;
                          outline: none;
                        }

                          a img {
                              border: none;
                          }

                        td, h1, h2, h3  {
                          font-family: Helvetica, Arial, sans-serif;
                          font-weight: 400;
                        }

                        body {
                          -webkit-font-smoothing:antialiased;
                          -webkit-text-size-adjust:none;
                          width: 100%;
                          height: 100%;
                          color: #37302d;
                          background: #ffffff;
                        }

                        table {
                          background:
                        }

                        h1, h2, h3 {
                          padding: 0;
                          margin: 0;
                          color: #ffffff;
                          font-weight: 400;
                        }

                        h3 {
                          color: #21c5ba;
                          font-size: 24px;
                        }
                        </style>

                      <style type='text/css' media='screen'>
                        @media screen {
                          td, h1, h2, h3 {
                            font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                          }
                        }
                      </style>

                      <style type='text/css' media='only screen and (max-width: 480px)'>
                        @media only screen and (max-width: 480px) {

                          table[class='w320'] {
                            width: 320px !important;
                          }

                          table[class='w300'] {
                            width: 300px !important;
                          }

                          table[class='w290'] {
                            width: 290px !important;
                          }

                          td[class='w320'] {
                            width: 320px !important;
                          }

                          td[class='mobile-center'] {
                            text-align: center !important;
                          }

                          td[class='mobile-padding'] {
                            padding-left: 20px !important;
                            padding-right: 20px !important;
                            padding-bottom: 20px !important;
                          }
                        }
                      </style>
                    <table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' >
                      <tr>
                        <td align='center' valign='top' bgcolor='#ffffff'  width='100%'>

                        <table cellspacing='0' cellpadding='0' width='100%'>
                          <tr>
                            <td style='border-bottom: 3px solid #3bcdc3;' width='100%'>
                              <center>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                  <tr>
                                    <td valign='top' style='padding:10px 0; text-align:center;' class='mobile-center'>
                                      <img width='250' height='62' src='https://inguate.com/images/LogoInguate.png'>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td background='http://35.232.20.49/images/background_email.gif' bgcolor='#64594b' valign='top' style='background: url(http://35.232.20.49/images/background_email.gif) no-repeat center; background-color: #64594b; background-position: center;'>
                              <div>
                                <center>
                                  <table cellspacing='0' cellpadding='0' width='530' height='303' class='w320'>
                                    <tr>
                                      <td valign='middle' style='vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:center;' class='mobile-center' height='303'>
                                        
                                        <h1 style='font-weight: bold; color: white;'>
                                            ".$rTMP["nombre"]." tu solicitud de registro ha sido Aprobada.
                                        </h1>
                                        <br>
                                        
                                      </td>
                                    </tr>
                                  </table>
                                </center>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td valign='top'>
                              <br>
                              <br>
                              
                              <center>
                                <table cellspacing='0' cellpadding='30' width='500' class='w290'>
                                          <tr>
                                            <td valign='top' style='border-bottom:1px solid #a1a1a1;'>
                                                      
                                              <center>
                                              <table style='margin: 0 auto;' cellspacing='0' cellpadding='8' width='500'>
                                                <tr>
                                                  <td style='text-align:;left; color: black;'>
                                                    
                                                    <p>
                                                        <b>".$rTMP["titulo"]."</b>
                                                        <br>
                                                        <br>
                                                        Se encuentra Pendiente de Pago, encontraras las intrucciones el pdf adjunto
                                                    </p>
                                                    
                                                  </td>
                                                </tr>
                                              </table>
                                              </center>
                                            </td>
                                          </tr>
                                        </table>
                                
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td style='background-color:#c2c2c2;'>
                                <br>
                                <br>
                            </td>
                          </tr>
                        </table>

                        </td>
                      </tr>
                    </table>
                    ";
                    
        fntEnviarCorreo($rTMP["email"], "Solicitud de Registro Aprobada", $strBody);
        
        
    }
    
    public function getSolicitudDatos($intSolPlace){
        
        $intSolPlace = intval($intSolPlace);
        
        $arr = array();
        
        $strQuery = "SELECT *
                     FROM   sol_place
                     WHERE  id_sol_place = {$intSolPlace} ";
        
        $strQuery = "SELECT *
                     FROM   sol_place
                                LEFT JOIN   usuario_vendedor 
                                    ON    usuario_vendedor.id_usuario = sol_place.id_usuario_vendedor
                     WHERE  sol_place.id_sol_place = {$intSolPlace}";
        
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        return $rTMP;
        
                
    }
    
    public function getClasificacion(){
        
        $strQuery = "SELECT id_clasificacion,
                            nombre, 
                            tag_en,
                            tag_es
                     FROM   clasificacion
                     ";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrClasificacion[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
            
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrClasificacion;
        
    }
    
    public function getClasificacionPadre($strKey){
        
        $strQuery = "SELECT clasificacion.nombre,
                            clasificacion.id_clasificacion,
                            clasificacion.tag_en,
                            clasificacion.tag_es,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre.nombre nombrePadre,
                            clasificacion_padre.tag_en tag_enPadre,
                            clasificacion_padre.tag_es tag_esPadre
                     FROM   clasificacion,
                            clasificacion_padre
                     WHERE  clasificacion.id_clasificacion IN ({$strKey})
                     AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["nombrePadre"] = $rTMP["nombrePadre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_enPadre"] = $rTMP["tag_enPadre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_esPadre"] = $rTMP["tag_esPadre"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getClasificacionPadreHijo($strKey){
        
        $strQuery = "SELECT clasificacion_padre.nombre,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es,
                            
                            clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                            clasificacion_padre_hijo.nombre nombreHijo,
                            clasificacion_padre_hijo.tag_en tag_enHijo,
                            clasificacion_padre_hijo.tag_es tag_esHijo
                            
                     FROM   clasificacion_padre,
                            clasificacion_padre_hijo
                     WHERE  clasificacion_padre.id_clasificacion_padre IN ({$strKey})
                     AND    clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["nombreHijo"] = $rTMP["nombreHijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_enHijo"] = $rTMP["tag_enHijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_esHijo"] = $rTMP["tag_esHijo"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getUsuarioPlaceaa($intUsuario, $intPlace = 0){
        
        $intUsuario = intval($intUsuario);
        $intPlace = intval($intPlace);
                               
        $strQuery = "SELECT place.id_place,
                            place.titulo,
                            place.num_producto,
                            place.num_galeria,
                            place.estado,
                            place.id_usuario,
                            place.id_place_combo,
                            usuario.id_usuario_place_restrinccion
                     FROM   place,
                            usuario   
                     WHERE  place.id_place = {$intPlace} 
                     AND    place.id_usuario = usuario.id_usuario";    
        
        $arrPlace = array();
        
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrPlace[$rTMP["id_place"]]["id_place"] = $rTMP["id_place"];
            $arrPlace[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
            $arrPlace[$rTMP["id_place"]]["num_producto"] = $rTMP["num_producto"];
            $arrPlace[$rTMP["id_place"]]["num_galeria"] = $rTMP["num_galeria"];
            $arrPlace[$rTMP["id_place"]]["estado"] = $rTMP["estado"];
            $arrPlace[$rTMP["id_place"]]["id_usuario"] = $rTMP["id_usuario"];
            $arrPlace[$rTMP["id_place"]]["id_usuario_place_restrinccion"] = $rTMP["id_usuario_place_restrinccion"];
            $arrPlace[$rTMP["id_place"]]["id_place_combo"] = $rTMP["id_place_combo"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        return $arrPlace;
                        
    }
    
    public function getPlaceClasificacion($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion
                     FROM   place_clasificacion
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion"]] = $rTMP["id_clasificacion"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadre($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT place_clasificacion_padre.id_clasificacion_padre,
                            place_clasificacion_padre.producto,
                            clasificacion_padre.nombre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es
                     FROM   place_clasificacion_padre,
                            clasificacion_padre
                     WHERE  place_clasificacion_padre.id_place = {$intPlace}
                     AND    place_clasificacion_padre.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["producto"] = $rTMP["producto"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadreHijo($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion_padre_hijo
                     FROM   place_clasificacion_padre_hijo
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadreHijoDetalle($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion_padre_hijo_detalle
                     FROM   place_clasificacion_padre_hijo_detalle
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo_detalle"]] = $rTMP["id_clasificacion_padre_hijo_detalle"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function setPlace($intUsuario, $strTitulo, $intCantidadProducto, $intCantidadGaleria, $strDescripcionNegocio,
                             $strNombreCompletoDuenio, $strTelefono, $strEmail, $strLat, $strLng, $strPlaceId, $intUbicacionLugar,
                             $strTelefonoDuenio, $strEmailDuenio, $intTipoNegocio, $intNegocio){
        
        $intUsuario = intval($intUsuario);
        $intCantidadProducto = intval($intCantidadProducto);
        $intCantidadGaleria = intval($intCantidadGaleria);
        $strTitulo = trim($strTitulo);
        
        
        $strQuery = "SELECT galeria,
                            producto_categoria
                     FROM   usuario,
                            usuario_place_restrinccion
                     WHERE  usuario.id_usuario = {$intUsuario}
                     AND    usuario.id_usuario_place_restrinccion = usuario_place_restrinccion.id_usuario_place_restrinccion ";    
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        $intCantidadProducto = $rTMP["producto_categoria"];
        $intCantidadGaleria = $rTMP["galeria"];
        
        $strPreguntaPlace = empty($strPlaceId) ? "N" : "Y";
        
        
        
        
        
        /*********************************************************************************/
        
        $strDireccion = "";    
        $strLat = "";    
        $strLng = "";    
        $strEstrellas = "0";    
        $arrGaleriaNegocio = array();
        
        if( $intNegocio ){
            
            $strQuery = "SELECT negocio.id_negocio,
                                negocio.id_ubicacion_lugar,
                                negocio.nombre,
                                negocio.lat,
                                negocio.lng,
                                negocio.direccion,
                                negocio.rango,
                                negocio.telefono,
                                negocio.precio,
                                negocio_galeria.id_negocio_galeria,
                                negocio_galeria.url
                                
                         FROM   negocio,
                                negocio_galeria
                         WHERE  negocio.id_negocio = {$intNegocio}
                         AND    negocio.id_negocio = negocio_galeria.id_negocio
                         ";
            
            
            $qTMP = $this->objDBClass->db_consulta($strQuery);
            while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
                
                
                $arrGaleriaNegocio[$rTMP["id_negocio_galeria"]]["url"] = $rTMP["url"];    
                
                $strTelefono = $rTMP["telefono"];    
                $strDireccion = $rTMP["direccion"];    
                $strLat = $rTMP["lat"];    
                $strLng = $rTMP["lng"];    
                $strEstrellas = $rTMP["rango"];    
                
            }
            
            $this->objDBClass->db_free_result($qTMP);
            
        }
        
        /*********************************************************************************/
        
        $strEstrellas = intval($strEstrellas);
        $strDireccion = fntCoreStringQueryLetra($strDireccion);
        
        $strQuery = "INSERT INTO place(id_usuario, titulo, num_producto, num_galeria, estado, place_id, place_pregunta, id_ubicacion_lugar, 
                                       telefono, direccion, ubicacion_lat, ubicacion_lng, estrellas)
                                VALUES({$intUsuario}, '{$strTitulo}', {$intCantidadProducto}, {$intCantidadGaleria}, 'C' , '{$strPlaceId}', '{$strPreguntaPlace}', {$intUbicacionLugar}, 
                                        '{$strTelefono}', '{$strDireccion}', '{$strLat}', '{$strLng}', '{$strEstrellas}' )";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $intIdPlace = $this->objDBClass->db_last_id();
        
        $strQuery = "INSERT INTO sol_place(id_usuario, id_place, descripcion, estado,
                                           nombre, telefono, email, place_id, lat, lng,
                                           telefono_propietario, email_propietario, tipo_negocio, id_negocio )
                                   VALUES({$intUsuario}, '{$intIdPlace}', '{$strDescripcionNegocio}', 'C',
                                          '{$strNombreCompletoDuenio}', '{$strTelefono}', '{$strEmail}', '{$strPlaceId}', '{$strLat}', '{$strLng}',
                                          '{$strTelefonoDuenio}', '{$strEmailDuenio}', '1', {$intNegocio}   )";
                                          
        
        
        $this->objDBClass->db_consulta($strQuery);
        
        if( count($arrGaleriaNegocio) > 0  ){
            
            $intCount = 1;
            while( $rTMP = each($arrGaleriaNegocio)  ){
                
                if( file_exists($rTMP["value"]["url"]) ){
                    
                    $strQuery = "INSERT INTO place_galeria(id_place)
                                                    VALUES({$intIdPlace})";
                    $this->objDBClass->db_consulta($strQuery);
                    $intPlaceGaleria = $this->objDBClass->db_last_id();
                    
                    $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".webp";
                    $strUrlArchivo = "../../file_inguate/place_interno/galeria/".fntCoreEncrypt($intPlaceGaleria).".webp";
                    copy($rTMP["value"]["url"], $strUrlArchivo);
                    
                    fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceGaleria));
                    
                    $strQuery = "UPDATE place_galeria
                                 SET    url_imagen = '{$strUrlArchivo}',
                                        orden = '{$intCount}' 
                                 WHERE  id_place_galeria = {$intPlaceGaleria}";
                    $this->objDBClass->db_consulta($strQuery);
                    $intCount++;
                                        
                }
                                
            }
            
        }
        
        return $intIdPlace;        
    }
    
    public function setEditPlace($intPlace, $strTitulo, $strDescripcionNegocio = "", $intRestrinccion = 1, $intUsuario= 0, $intPlaceCombo = 0){
        
        $intPlace = intval($intPlace); 
        $strTitulo = trim($strTitulo); 
        
        $strQuery = "UPDATE usuario
                     SET    id_usuario_place_restrinccion = '{$intRestrinccion}'  
                     WHERE  id_usuario = {$intUsuario} ";
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "UPDATE place
                     SET    titulo = '{$strTitulo}',
                            id_place_combo = {$intPlaceCombo}  
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
                
        $strQuery = "UPDATE sol_place
                     SET    descripcion = '{$strDescripcionNegocio}'  
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setDeletePlaceClasificacionPadre($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion_padre WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlaceClasificacion($intPlace, $intPlaceClasificacion){
        
        $strQuery = "INSERT INTO place_clasificacion (id_place, id_clasificacion)
                                                VALUES( {$intPlace}, {$intPlaceClasificacion} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
        
    public function setDeletePlaceClasificacion($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlaceClasificacionPadre($intPlace, $intPlaceClasificacionPadre){
        
        $strQuery = "INSERT INTO place_clasificacion_padre (id_place, id_clasificacion_padre)
                                                VALUES( {$intPlace}, {$intPlaceClasificacionPadre} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
       
    public function getRestriccionUsuario($intUsuario){
        
        $strQuery = "SELECT usuario_place_restrinccion.id_usuario_place_restrinccion,
                            usuario_place_restrinccion.max_place
                     FROM   usuario,
                            usuario_place_restrinccion
                     WHERE  usuario.id_usuario = {$intUsuario}
                     AND    usuario.id_usuario_place_restrinccion = usuario_place_restrinccion.id_usuario_place_restrinccion     ";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        return $rTMP;
        
    }
    
    public function fntGetSolPlace($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT *
                     FROM   sol_place   
                     WHERE  id_place = {$intPlace}";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        return $rTMP;
                
    }
    
    public function getNegocioAutocomplete($strTexto){
        
        
        $strQuery = "SELECT id_negocio,
                            nombre,
                            lat,
                            lng
                     FROM   negocio
                     WHERE  nombre LIKE '%{$strTexto}%'
                     AND    estado = 'A'
                     LIMIT  30";
        $arrResultado = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrTMP["identificador"] = fntCoreEncrypt($rTMP["id_negocio"]);
            $arrTMP["nombre"] = $rTMP["nombre"];
            $arrTMP["lat"] = $rTMP["lat"];
            $arrTMP["lng"] = $rTMP["lng"];
            
            array_push($arrResultado, $arrTMP);
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrResultado;
        
        
    }
    
    public function getPlaceRestrinccionUsuario(){
        
        $arr = array() ;
        $strQuery = "SELECT *
                     FROM   usuario_place_restrinccion";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arr[$rTMP["id_usuario_place_restrinccion"]]["id_usuario_place_restrinccion"] = $rTMP["id_usuario_place_restrinccion"];
            $arr[$rTMP["id_usuario_place_restrinccion"]]["max_place"] = $rTMP["max_place"];
            $arr[$rTMP["id_usuario_place_restrinccion"]]["galeria"] = $rTMP["galeria"];
            $arr[$rTMP["id_usuario_place_restrinccion"]]["producto_categoria"] = $rTMP["producto_categoria"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arr;
    }
        
    public function getPlaceCombo(){
        
        $arr = array() ;
        $strQuery = "SELECT *
                     FROM   place_combo";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arr[$rTMP["id_place_combo"]]["id_place_combo"] = $rTMP["id_place_combo"];
            $arr[$rTMP["id_place_combo"]]["nombre"] = $rTMP["nombre"];
            $arr[$rTMP["id_place_combo"]]["tipo_dato"] = $rTMP["tipo_dato"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arr;
    }
                
}

?>
