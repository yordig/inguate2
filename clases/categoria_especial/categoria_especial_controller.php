<?php
  
require_once "categoria_especial_model.php";
require_once "categoria_especial_view.php";

class categoria_especial_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new categoria_especial_model();
        $this->objView = new categoria_especial_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["drawFormNewClassification"]) ){
            
            $intClasificacion = isset($_GET["clasificacion"]) ? trim($_GET["clasificacion"]) : "";
            
            $this->objView->fntDrawFormNewClassification($strAction, $intClasificacion);
            
            die();
            
        }
        
        if( isset($_GET["setClasificacion"]) ){
            
            $intClasificacion = isset($_POST["hidIdClassification"]) ? intval($_POST["hidIdClassification"]) : 0;
            $strNombre = isset($_POST["txtNombre"]) ? addslashes($_POST["txtNombre"]) : "";
            $strTagEN = isset($_POST["txtTag_en"]) ? addslashes($_POST["txtTag_en"]) : "";
            $strTagES = isset($_POST["txtTag_es"]) ? addslashes($_POST["txtTag_es"]) : "";
            $strOrden = isset($_POST["txtOrden"]) ? addslashes($_POST["txtOrden"]) : 0;
            $strColor = isset($_POST["txtColor"]) ? addslashes($_POST["txtColor"]) : 0;
            $strSinonimo = isset($_POST["txtSinonimo"]) ? addslashes($_POST["txtSinonimo"]) : 0;
            $strGratuito = isset($_POST["slcGratuito"]) ? addslashes($_POST["slcGratuito"]) : "N";
            
            if( $intClasificacion ){
                
                $this->objModel->setEditClasificacion($intClasificacion, $strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strGratuito);
                    
            }
            else{
                $intClasificacion = $this->objModel->setClasificacion( $strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strGratuito);    
            }
            
            if( isset($_FILES["fllogo"]) && $_FILES["fllogo"]["size"] > 0 ){
                
                $strUrlArchivo = "images/icon/clas/CE_".fntCoreEncrypt($intClasificacion).".png";
                rename($_FILES["fllogo"]["tmp_name"], $strUrlArchivo);
                
                $this->objModel->setEditLogo($intClasificacion, $strUrlArchivo);
                
            }
            
            if( isset($_FILES["flImagenFondo"]) && $_FILES["flImagenFondo"]["size"] > 0 ){
                
                $strUrlArchivo = "images/icon/clas/CEI_".fntCoreEncrypt($intClasificacion).".png";
                rename($_FILES["flImagenFondo"]["tmp_name"], $strUrlArchivo);
                
                $this->objModel->setEditImagen($intClasificacion, $strUrlArchivo);
                
            }
            
            if( isset($_FILES["flImagenFondoWeb"]) && $_FILES["flImagenFondoWeb"]["size"] > 0 ){
                
                $strUrlArchivo = "images/icon/clas/CEIM_".fntCoreEncrypt($intClasificacion).".png";
                rename($_FILES["flImagenFondoWeb"]["tmp_name"], $strUrlArchivo);
                
                $this->objModel->setEditImagenWeb($intClasificacion, $strUrlArchivo);
                
            }
            
            die();
        }
        
        if( isset($_GET["setEliminarClasificacion"]) ){
            
            $intClasificacion = isset($_GET["key"]) ? intval($_GET["key"]) : 0;
            
            $this->objModel->setEliminarClasificacion($intClasificacion);
            
            die();
        }         
                     
    }
    
}

  
?>
