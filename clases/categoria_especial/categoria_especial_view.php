<?php

require_once "categoria_especial_model.php";

class categoria_especial_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new categoria_especial_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        $arrUsuario = $this->objModel->getClasificacion();
        
        ?>
        
         
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
            <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
       
            <link rel="stylesheet" media="screen" type="text/css" href="dist/colorpicker/css/colorpicker.css" />
            <script type="text/javascript" src="dist/colorpicker/js/colorpicker.js"></script>
 
       
            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-2  " id="divContenidoBody"  >
                    
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewClassification();">New Classification</button>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-12">
                            
                            <table class="table table-secondary table-hover" id="myTable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Tag EN</th>
                                        <th scope="col">Tag ES</th>
                                        <th scope="col">Orden</th>
                                        <th scope="col">Icon</th>
                                        <th scope="col">Image Movil</th>
                                        <th scope="col">Image Web</th>
                                        <th scope="col">Gratuito</th>
                                        <th scope="col">Synonyms</th>
                                        <th scope="col" style="width: 20%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    while( $rTMP = each($arrUsuario) ){
                                        ?>
                                        <tr>
                                            <td><?php print $rTMP["value"]["nombre"]?></td>
                                            <td><?php print $rTMP["value"]["tag_en"]?></td>
                                            <td><?php print $rTMP["value"]["tag_es"]?></td>
                                            <td><?php print $rTMP["value"]["orden"]?></td>
                                            <td>
                                                
                                                <div style="background-color: <?php print "#".$rTMP["value"]["color_hex"]?>; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                                    <img src="<?php print $rTMP["value"]["url_imagen"]?>">
                                                </div>
                                                
                                            </td>
                                            <td>
                                                
                                                <a href="<?php print $rTMP["value"]["url_imagen_principal"];?>" class="btn btn-raised btn-sm  btn-success" target="_blank">Ver <i class="fa fa-external-link"></i></a>
                                
                                            
                                            </td>
                                            <td>
                                                
                                                <a href="<?php print $rTMP["value"]["url_imagen_principal_web"];?>" class="btn btn-raised btn-sm  btn-success" target="_blank">Ver <i class="fa fa-external-link"></i></a>
                                
                                            
                                            </td>
                                            <td><?php print $rTMP["value"]["gratuita"] == "Y" ? "Si" : "No"?></td>
                                            
                                            <td><?php print $rTMP["value"]["sinonimo"]?></td>
                                            <td class="text-right nowrap">
                                                <button onclick="fntDrawFormNewClassification('<?php print $rTMP["value"]["id_categoria_especial"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                                <button onclick="fntDrawAdminClasificacionPadre('<?php print $rTMP["value"]["id_categoria_especial"]?>');" class="btn btn-success btn-raised btn-sm">Admin</button>
                                                <button onclick="fntDEliminarClasificacionPadre('<?php print $rTMP["value"]["id_categoria_especial"]?>');" class="btn btn-danger btn-raised btn-sm">Eliminar</button>
                                            </td>
                                        </tr>
                                        <?php                                        
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>

                            
                            
                            
                        </div>    
                    </div>    
                    
                    
                                        
                </div> 
            </main>
               
          
        </div>    
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable({
                    "order": [[ 3, "asc" ]]
                });
                    
            }); 
            
            function fntDrawFormNewClassification(intClassification){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawFormNewClassification=true&clasificacion="+intClassification, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }
             
            function fntDrawAdminClasificacionPadre(intClassification){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawAdminClasificacionPadre=true&clasificacion="+intClassification, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }  
            
            function fntDrawAdminClasificacionPadreHijo(intClassificationPadre){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawAdminClasificacionPadreHijo=true&clasificacionPadre="+intClassificationPadre, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }
            
            function fntDrawAdminClasificacionPadreHijoDetalle(intClassificationPadreHijo){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawAdminClasificacionPadreHijoDetalle=true&clasificacionPadreHijo="+intClassificationPadreHijo, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }  
            
            function fntDEliminarClasificacionPadre(intClasificacion){
                
                swal({
                    title: "Estas Seguro",
                    text: "Se Eliminaran todos los datos",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "<?php print $strAction?>?setEliminarClasificacion=true&key="+intClasificacion, 
                            success: function(result){
                                
                                fntCargarPaginaActiva();
                                
                            }
                            
                        });
                        
                    
                    } 
                });
                
                
            }
                
        </script>        
        <?php 
        
    }
    
    public function fntDrawFormNewClassification($strAction, $intClasificacion){
        
        $arrClassificacion = $this->objModel->getClasificacion($intClasificacion);
        $arrClassificacion = isset($arrClassificacion[$intClasificacion]) ? $arrClassificacion[$intClasificacion] : array();
        
        ?>
        <style>
            
            .colorpicker {
                width: 356px;
                height: 176px;
                overflow: hidden;
                position: absolute;
                background: url(dist/colorpicker/images/colorpicker_background.png);
                font-family: Arial, Helvetica, sans-serif;
                display: none;
                z-index: 999;
            }
            
        </style>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntCargarPaginaActiva();">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveClasificacion();">Save</button>
            </div>    
        </div>
        
        <div class="row">
            <div class="col-6 offset-3 ">
                
                <form onsubmit="return false;" id="frmClasificacion" method="POST">
                    <input type="hidden" name="hidIdClassification" value="<?php print $intClasificacion?>">
                    <div class="form-group">
                        <label for="txtNombre"><small>Nombre</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["nombre"]) ? $arrClassificacion["nombre"] : ""?>" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                    
                    </div>
                    
                    <div class="form-group">
                        <label for="txtTag_en"><small>Tag EN</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["tag_en"]) ? $arrClassificacion["tag_en"] : ""?>" class="form-control" id="txtTag_en" name="txtTag_en" placeholder="Tag EN" required>
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Tag ES</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["tag_es"]) ? $arrClassificacion["tag_es"] : ""?>" class="form-control" id="txtTag_es" name="txtTag_es" placeholder="Tag ES" required>
                    </div>
                    <div class="form-group">
                        <label for="slcGratuito"><small>Gratuito</small></label>
                        <select class="custom-select " name="slcGratuito">
                            <option <?php print isset($arrClassificacion["gratuita"]) && $arrClassificacion["gratuita"] == "N" ? "selected" : ""?> value="N">No</option>
                            <option <?php print isset($arrClassificacion["gratuita"]) && $arrClassificacion["gratuita"] == "Y" ? "selected" : ""?> value="Y">Yes</option>
                            
                        </select>                    
                    </div>
                    <div class="form-group">
                        <label for="txtOrden"><small>Orden</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["orden"]) ? $arrClassificacion["orden"] : ""?>" class="form-control" id="txtOrden" name="txtOrden" placeholder="Orden" required>
                    
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Synonyms</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["sinonimo"]) ? $arrClassificacion["sinonimo"] : ""?>" class="form-control" id="txtSinonimo" name="txtSinonimo" placeholder="Synonyms" required>
                    </div>
                    <div class="form-group">
                        <label for="txtOrden">
                            <small>Imagen Pagina Movil
                            
                                <?php
                            
                            if( isset($arrClassificacion["url_imagen_principal"]) ){
                                
                                ?>
                                <a href="<?php print $arrClassificacion["url_imagen_principal"];?>" class="btn btn-raised btn-sm  btn-success" target="_blank">Ver <i class="fa fa-external-link"></i></a>
                                <?php
                                
                            }
                            
                            ?>
                            </small>
                        </label>                    
                        <input type="file" class="form-control" id="flImagenFondo" name="flImagenFondo" >
                        
                                         
                    </div>
                    <div class="form-group">
                        <label for="txtOrden">
                            <small>Imagen Pagina Web
                            
                                <?php
                            
                            if( isset($arrClassificacion["url_imagen_principal_web"]) ){
                                
                                ?>
                                <a href="<?php print $arrClassificacion["url_imagen_principal_web"];?>" class="btn btn-raised btn-sm  btn-success" target="_blank">Ver <i class="fa fa-external-link"></i></a>
                                <?php
                                
                            }
                            
                            ?>
                            </small>
                        </label>                    
                        <input type="file" class="form-control" id="flImagenFondoWeb" name="flImagenFondoWeb" >
                        
                                         
                    </div>
                    <div class="form-group">
                        <label for="txtOrden"><small>Logo</small></label>                    
                        <input type="file" onchange="fntSetImgfile();" class="form-control" id="fllogo" name="fllogo" >
                                         
                        <div id="divIcono"  onclick="$('#fllogo').click();" class="" style="background-color: <?php print isset($arrClassificacion["color_hex"]) ? "#".$arrClassificacion["color_hex"] : "red"?>; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                            <img id="IMGdivIcono" src="<?php print isset($arrClassificacion["url_imagen"]) ? $arrClassificacion["url_imagen"] : ""?>">
                        </div>
                        
                        <input type="text" value="<?php print isset($arrClassificacion["color_hex"]) ? $arrClassificacion["color_hex"] : ""?>" class="form-controld" id="txtColor" name="txtColor" >
                    
                        
                    </div>
                    
                    <div class="form-group">
                        
                        
                    </div>
                  
                </form>     
                
            </div>    
        </div>
        <script>
             
            $( document ).ready(function() {
                
                
                $('#txtColor').ColorPicker({
                    onSubmit: function(hsb, hex, rgb, el) {
                        $(el).val(hex);
                        $(el).ColorPickerHide();
                        $("#divIcono").css("background-color", "#"+hex); 
                    },
                    onBeforeShow: function () {
                        $(this).ColorPickerSetColor(this.value);
                    }
                })
                .bind('keyup', function(){
                    $(this).ColorPickerSetColor(this.value);
                });
                    
            });
            
            function fntSaveClasificacion(){
                
                var formData = new FormData(document.getElementById("frmClasificacion"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setClasificacion=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });
                
            }
            
            function fntSetImgfile(){
                
                    
                var filename = $('#fllogo').val();
                
                arr = filename.split(".");
                
                if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                    
                    swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
                    fntDeleteImageSeccion1(index);
                    return false;                    
                    
                }
                
                
                if ( ! window.FileReader ) {
                    return alert( 'FileReader API is not supported by your browser.' );
                }
                var $i = $( '#fllogo'), // Put file input ID here
                    input = $i[0]; // Getting the element from jQuery
                if ( input.files && input.files[0] ) {
                    file = input.files[0]; // The file
                    fr = new FileReader(); // FileReader instance
                    fr.onload = function () {
                        // Do stuff on onload, use fr.result for contents of file
                        //$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
                        //console.log(fr.result);
                        document.getElementById("IMGdivIcono").src = fr.result;
                    };
                    //fr.readAsText( file );
                    fr.readAsDataURL( file );
                } else {
                    // Handle errors here
                    alert( "File not selected or browser incompatible." )
                }
                                
            }
            
                            
        </script>
        
        <?php
        
    }
                                                
                                        
}

?>
