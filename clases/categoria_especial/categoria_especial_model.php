<?php
 
class categoria_especial_model{
      
    var $objDBClass;
    
    public function __construct(){
        
        $this->objDBClass = new dbClass(); 
            
    }
    
    public function getobjDBClass(){
        return $this->objDBClass;
    }
    
    public function setCloseDB(){
        
        $this->objDBClass->db_close();
    
    }
    
    public function setClasificacion($strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strGratuito){
        
        $strQuery = "INSERT INTO categoria_especial(nombre, tag_en, tag_es, estado, orden, color_hex, sinonimo, gratuita)
                                            VALUES( '{$strNombre}', '{$strTagEN}', '{$strTagES}', 'A', '{$strOrden}', '{$strColor}', '{$strSinonimo}', '{$strGratuito}' )";
        $this->objDBClass->db_consulta($strQuery);
        
        return $this->objDBClass->db_last_id();
    }
    
    public function setEditClasificacion($intClasification, $strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strGratuito){
        
        
        $strQuery = "UPDATE categoria_especial
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            color_hex = '{$strColor}',
                            sinonimo = '{$strSinonimo}',
                            gratuita = '{$strGratuito}'
                     WHERE  id_categoria_especial = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setEditLogo($intClasification, $strUrlArchivo){
        
        
        $strQuery = "UPDATE categoria_especial
                     SET    url_imagen = '{$strUrlArchivo}'
                     WHERE  id_categoria_especial = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setEditImagen($intClasification, $strUrlArchivo){
        
        
        $strQuery = "UPDATE categoria_especial
                     SET    url_imagen_principal = '{$strUrlArchivo}'
                     WHERE  id_categoria_especial = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setEditImagenWeb($intClasification, $strUrlArchivo){
        
        
        $strQuery = "UPDATE categoria_especial
                     SET    url_imagen_principal_web = '{$strUrlArchivo}'
                     WHERE  id_categoria_especial = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getClasificacion($intClasificacion = 0){
        
        $intClasificacion = intval($intClasificacion);
        
        $strFiltro = $intClasificacion ? "WHERE id_categoria_especial = {$intClasificacion}" : "";
        
        $strQuery = "SELECT id_categoria_especial,
                            nombre,
                            tag_en,
                            tag_es,
                            url_imagen,
                            estado,
                            sinonimo,
                            color_hex, 
                            orden, 
                            url_imagen_principal,
                            url_imagen_principal_web,
                            gratuita 
                     FROM   categoria_especial
                     {$strFiltro} 
                     ORDER BY orden DESC";
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_categoria_especial"]]["id_categoria_especial"] = $rTMP["id_categoria_especial"];
            $arrInfo[$rTMP["id_categoria_especial"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_categoria_especial"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_categoria_especial"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_categoria_especial"]]["url_imagen"] = $rTMP["url_imagen"];
            $arrInfo[$rTMP["id_categoria_especial"]]["estado"] = $rTMP["estado"];
            $arrInfo[$rTMP["id_categoria_especial"]]["sinonimo"] = $rTMP["sinonimo"];
            $arrInfo[$rTMP["id_categoria_especial"]]["color_hex"] = $rTMP["color_hex"];
            $arrInfo[$rTMP["id_categoria_especial"]]["orden"] = $rTMP["orden"];
            $arrInfo[$rTMP["id_categoria_especial"]]["url_imagen_principal"] = $rTMP["url_imagen_principal"];
            $arrInfo[$rTMP["id_categoria_especial"]]["url_imagen_principal_web"] = $rTMP["url_imagen_principal_web"];
            $arrInfo[$rTMP["id_categoria_especial"]]["gratuita"] = $rTMP["gratuita"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getClasificacionPadre($intClasificacion, $intClasificacionPadre = 0){
        
        $intClasificacion = intval($intClasificacion);
        
        $strFiltro = $intClasificacionPadre ? "AND clasificacion_padre.id_clasificacion_padre = {$intClasificacionPadre}" : "";
        
        $strQuery = "SELECT clasificacion_padre.id_clasificacion_padre, 
                            clasificacion_padre.nombre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es,
                            clasificacion_padre.orden,
                            clasificacion_padre.sinonimo,
                            clasificacion.nombre nombreCla
                     FROM   clasificacion   
                                LEFT JOIN clasificacion_padre
                                    ON  clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion
                     WHERE  clasificacion.id_clasificacion = {$intClasificacion} 
                     {$strFiltro} ";
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo["nombreCla"] = $rTMP["nombreCla"];
            
            if( intval($rTMP["id_clasificacion_padre"]) ){
                
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["orden"] = $rTMP["orden"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["sinonimo"] = $rTMP["sinonimo"];
                
                
            }
                    
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setClasificacionPadre($intClasificacionPadre, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        $strQuery = "INSERT INTO clasificacion_padre(id_clasificacion, nombre, tag_en, tag_es, orden, sinonimo)
                                  VALUES( {$intClasificacionPadre}, '{$strNombre}', '{$strTagEN}', '{$strTagES}', '{$strOrden}', '{$strSinonimo}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditClasificacionPadre($intClasification, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        
        $strQuery = "UPDATE clasificacion_padre
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            sinonimo = '{$strSinonimo}'
                     WHERE  id_clasificacion_padre = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getClasificacionPadreHijo($intClasificacionPadre, $intClasificacionPadreHijo = 0){
        
        $intClasificacionPadre = intval($intClasificacionPadre);
        $intClasificacionPadreHijo = intval($intClasificacionPadreHijo);
        
        $strFiltro = $intClasificacionPadreHijo ? "AND clasificacion_padre_hijo.id_clasificacion_padre_hijo = {$intClasificacionPadreHijo}" : "";
        
        $strQuery = "SELECT clasificacion_padre_hijo.id_clasificacion_padre_hijo, 
                            clasificacion_padre_hijo.nombre,
                            clasificacion_padre_hijo.tag_en,
                            clasificacion_padre_hijo.tag_es,
                            clasificacion_padre_hijo.orden,
                            clasificacion_padre_hijo.sinonimo,
                            
                            clasificacion.nombre nombreCla,
                            clasificacion.id_clasificacion,
                            clasificacion_padre.nombre nombreClaPadre,
                            clasificacion_padre.id_clasificacion_padre
                            
                     FROM   clasificacion,
                            clasificacion_padre
                                LEFT JOIN   clasificacion_padre_hijo
                                    ON  clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                                    
                     WHERE  clasificacion_padre.id_clasificacion_padre = {$intClasificacionPadre}
                     AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                     {$strFiltro} 
                     ";   
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo["nombreCla"] = $rTMP["nombreCla"];
            $arrInfo["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo["nombreClaPadre"] = $rTMP["nombreClaPadre"];
            $arrInfo["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            
            if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
                
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["tag_en"] = $rTMP["tag_en"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["tag_es"] = $rTMP["tag_es"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["orden"] = $rTMP["orden"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["sinonimo"] = $rTMP["sinonimo"];
                
                
            }
                    
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setClasificacionPadreHijo($intClasificacionPadre, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        $strQuery = "INSERT INTO clasificacion_padre_hijo(id_clasificacion_padre, nombre, tag_en, tag_es, orden, sinonimo)
                                  VALUES( {$intClasificacionPadre}, '{$strNombre}', '{$strTagEN}', '{$strTagES}', '{$strOrden}', '{$strSinonimo}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditClasificacionPadreHijo($intClasificacionPadreHijo, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        
        $strQuery = "UPDATE clasificacion_padre_hijo
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            sinonimo = '{$strSinonimo}'
                     WHERE  id_clasificacion_padre_hijo = {$intClasificacionPadreHijo} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getClasificacionPadreHijoDetalle($intClasificacionPadreHijo, $intClasificacionPadreHijoDetalle = 0){
        
        $intClasificacionPadreHijo = intval($intClasificacionPadreHijo);
        $intClasificacionPadreHijoDetalle = intval($intClasificacionPadreHijoDetalle);
        
        $strFiltro = $intClasificacionPadreHijoDetalle ? "AND clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle = {$intClasificacionPadreHijoDetalle}" : "";
        
        $strQuery = "SELECT clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle, 
                            clasificacion_padre_hijo_detalle.nombre,
                            clasificacion_padre_hijo_detalle.tag_en,
                            clasificacion_padre_hijo_detalle.tag_es,
                            clasificacion_padre_hijo_detalle.orden,
                            clasificacion_padre_hijo_detalle.sinonimo,
                            
                            clasificacion.nombre nombreCla,
                            clasificacion.id_clasificacion,
                            clasificacion_padre.nombre nombreClaPadre,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre_hijo.nombre nombreClaPadreHijo,
                            clasificacion_padre_hijo.id_clasificacion_padre_hijo
                            
                     FROM   clasificacion,
                            clasificacion_padre,
                            clasificacion_padre_hijo
                                LEFT JOIN   clasificacion_padre_hijo_detalle
                                    ON  clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo 
                                    
                     WHERE  clasificacion_padre_hijo.id_clasificacion_padre_hijo = {$intClasificacionPadreHijo}
                     AND    clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                     AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                     {$strFiltro} 
                     ";   
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo["nombreCla"] = $rTMP["nombreCla"];
            $arrInfo["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo["nombreClaPadre"] = $rTMP["nombreClaPadre"];
            $arrInfo["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo["nombreClaPadreHijo"] = $rTMP["nombreClaPadreHijo"];
            $arrInfo["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            
            if( intval($rTMP["id_clasificacion_padre_hijo_detalle"]) ){
                
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["id_clasificacion_padre_hijo_detalle"] = $rTMP["id_clasificacion_padre_hijo_detalle"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["nombre"] = $rTMP["nombre"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_en"] = $rTMP["tag_en"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_es"] = $rTMP["tag_es"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["orden"] = $rTMP["orden"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["sinonimo"] = $rTMP["sinonimo"];
                
                
            }
                    
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setClasificacionPadreHijoDetalle($intClasificacionPadreHijo, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        $strQuery = "INSERT INTO clasificacion_padre_hijo_detalle(id_clasificacion_padre_hijo, nombre, tag_en, tag_es, orden, sinonimo)
                                  VALUES( {$intClasificacionPadreHijo}, '{$strNombre}', '{$strTagEN}', '{$strTagES}', '{$strOrden}', '{$strSinonimo}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditClasificacionPadreHijoDetalle($intClasificacionPadreHijoDetalle, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        
        $strQuery = "UPDATE clasificacion_padre_hijo_detalle
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            sinonimo = '{$strSinonimo}'
                     WHERE  id_clasificacion_padre_hijo_detalle = {$intClasificacionPadreHijoDetalle} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setEliminarClasificacion($intClasificacion){
        
        $intClasificacion = intval($intClasificacion);
        
        $strQuery = "DELETE FROM categoria_especial WHERE id_categoria_especial = {$intClasificacion}";
        
        $this->objDBClass->db_consulta($strQuery);
    
    }
    
    function setEliminarClasificacionPadreHijoDetalle($intClasificacionPadreHijoDetalle){
        
        $intClasificacionPadreHijoDetalle = intval($intClasificacionPadreHijoDetalle);
        
        $strQuery = "DELETE FROM clasificacion_padre_hijo_detalle WHERE id_clasificacion_padre_hijo_detalle = {$intClasificacionPadreHijoDetalle} ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    function setEliminarClasificacionPadreHijo($intClasificacionPadreHijo){
        
        $intClasificacionPadreHijo = intval($intClasificacionPadreHijo);
        
        $strQuery = "DELETE clasificacion_padre_hijo_detalle
                     FROM   clasificacion_padre_hijo_detalle
                                
                                INNER JOIN  clasificacion_padre_hijo
                                
                                ON  clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo    
                                AND clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = {$intClasificacionPadreHijo}
                                 ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        
        $strQuery = "DELETE FROM clasificacion_padre_hijo WHERE id_clasificacion_padre_hijo = {$intClasificacionPadreHijo} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEliminarClasificacionPadre($intClasificacionPadre){
        
        $strQuery = "DELETE clasificacion_padre_hijo_detalle
                     FROM   clasificacion_padre_hijo_detalle
                                
                                INNER JOIN  clasificacion_padre_hijo
                                
                                ON  clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo    
                                AND clasificacion_padre_hijo.id_clasificacion_padre = {$intClasificacionPadre}
                                 ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM  clasificacion_padre_hijo WHERE id_clasificacion_padre = {$intClasificacionPadre}   ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM  clasificacion_padre WHERE id_clasificacion_padre = {$intClasificacionPadre}   ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        
        
    }
                              
}

?>
