<?php
 
class place_model{
      
    var $objDBClass;
    
    public function __construct(){
        
        $this->objDBClass = new dbClass(); 
            
    }
    
    public function getobjDBClass(){
        return $this->objDBClass;
    }
    
    public function setCloseDB(){
        
        $this->objDBClass->db_close();
    
    }
    
    public function setUsuario($strEmail, $strPassWord, $strNombre, $strApellido, $strTipo){
        
        $strPassWord = md5($strPassWord);
        $strQuery = "INSERT INTO usuario(email, clave, nombre, apellido, estado, tipo, lenguaje)
                                  VALUES( '{$strEmail}', '{$strPassWord}', '{$strNombre}', '{$strApellido}', 'A', '{$strTipo}', 'es'  )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditUsuario($intUsuario, $strEmail, $strPassWord, $strNombre, $strApellido, $strTipo){
        
        $strUpdateClave = !empty($strPassWord) ? " ,clave = '{$strPassWord}' " : "";
        
        $strQuery = "UPDATE usuario
                     SET    nombre = '{$strNombre}', 
                            apellido = '{$strApellido}',
                            tipo = '{$strTipo}',
                            email = '{$strEmail}'
                            {$strUpdateClave}
                     WHERE  id_usuario = {$intUsuario} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getUsuario($intUsuario = 0){
        
        $intUsuario = intval($intUsuario);
        
        $strFiltro = $intUsuario ? "WHERE id_usuario = {$intUsuario}" : "";
        
        $strQuery = "SELECT id_usuario, email, clave, nombre, apellido, estado, tipo, lenguaje
                     FROM   usuario
                     {$strFiltro} ";
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_usuario"]]["id_usuario"] = $rTMP["id_usuario"];
            $arrInfo[$rTMP["id_usuario"]]["email"] = $rTMP["email"];
            $arrInfo[$rTMP["id_usuario"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_usuario"]]["apellido"] = $rTMP["apellido"];
            $arrInfo[$rTMP["id_usuario"]]["estado"] = $rTMP["estado"];
            $arrInfo[$rTMP["id_usuario"]]["tipo"] = $rTMP["tipo"];
            $arrInfo[$rTMP["id_usuario"]]["lenguaje"] = $rTMP["lenguaje"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getTipoUsuario(){
        
        $arr[1]["nombre"] = "Admin";
        $arr[2]["nombre"] = "User Place Admin";
        
        return $arr;
    }
    
    public function getClasificacion(){
        
        $strQuery = "SELECT id_clasificacion,
                            nombre, 
                            tag_en,
                            tag_es
                     FROM   clasificacion
                     ";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrClasificacion[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
            
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrClasificacion;
        
    }
    
    public function getClasificacionPadre($strKey){
        
        $strQuery = "SELECT clasificacion.nombre,
                            clasificacion.id_clasificacion,
                            clasificacion.tag_en,
                            clasificacion.tag_es,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre.nombre nombrePadre,
                            clasificacion_padre.tag_en tag_enPadre,
                            clasificacion_padre.tag_es tag_esPadre
                     FROM   clasificacion,
                            clasificacion_padre
                     WHERE  clasificacion.id_clasificacion IN ({$strKey})
                     AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["nombrePadre"] = $rTMP["nombrePadre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_enPadre"] = $rTMP["tag_enPadre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_esPadre"] = $rTMP["tag_esPadre"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getClasificacionPadreHijo($strKey){
        
        $strQuery = "SELECT clasificacion_padre.nombre,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es,
                            
                            clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                            clasificacion_padre_hijo.nombre nombreHijo,
                            clasificacion_padre_hijo.tag_en tag_enHijo,
                            clasificacion_padre_hijo.tag_es tag_esHijo
                            
                     FROM   clasificacion_padre,
                            clasificacion_padre_hijo
                     WHERE  clasificacion_padre.id_clasificacion_padre IN ({$strKey})
                     AND    clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["nombreHijo"] = $rTMP["nombreHijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_enHijo"] = $rTMP["tag_enHijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_esHijo"] = $rTMP["tag_esHijo"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getClasificacionPadreHijoDetalle($strKey){
        
        $strQuery = "SELECT clasificacion_padre_hijo.nombre,
                            clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                            clasificacion_padre_hijo.tag_en,
                            clasificacion_padre_hijo.tag_es,
                            
                            clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle,
                            clasificacion_padre_hijo_detalle.nombre nombreDetalle,
                            clasificacion_padre_hijo_detalle.tag_en tag_enDetalle,
                            clasificacion_padre_hijo_detalle.tag_es tag_esDetalle
                            
                     FROM   clasificacion_padre_hijo,
                            clasificacion_padre_hijo_detalle
                     WHERE  clasificacion_padre_hijo.id_clasificacion_padre_hijo IN ({$strKey})
                     AND    clasificacion_padre_hijo.id_clasificacion_padre_hijo = clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["id_clasificacion_padre_hijo_detalle"] = $rTMP["id_clasificacion_padre_hijo_detalle"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["nombreDetalle"] = $rTMP["nombreDetalle"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_enDetalle"] = $rTMP["tag_enDetalle"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_esDetalle"] = $rTMP["tag_esDetalle"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getPlaceUsuario($intUsuario){
        
        $intUsuario = intval($intUsuario);
        $arrCatalogoPlace = fntGetCatalogoEstadoPlace();
        
        $strQuery = "SELECT place.id_place,
                            place.url_place,
                            place.titulo,
                            place.estado,
                            clasificacion.id_clasificacion,
                            clasificacion.tag_en,
                            clasificacion.tag_es,
                            usuario_alerta.id_usuario_alerta,
                            usuario_alerta.tipo,
                            usuario_alerta.identificador,
                            usuario_alerta.mensaje,
                            place_visita.id_place_visita,
                            place_visita.publico
                     FROM   place
                                LEFT JOIN usuario_alerta
                                    ON  usuario_alerta.identificador = place.id_place
                                    AND usuario_alerta.tipo IN ('SPR', 'SPP')
                                LEFT JOIN place_visita
                                    ON  place_visita.id_place = place.id_place
                                ,
                            place_clasificacion,
                            clasificacion
                     WHERE  place.id_usuario = {$intUsuario} 
                     AND    place.id_place = place_clasificacion.id_place
                     AND    place_clasificacion.id_clasificacion = clasificacion.id_clasificacion
                     ORDER BY id_usuario_alerta ASC";
                     
        $arr = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arr[$rTMP["id_place"]]["titulo"]  = $rTMP["titulo"];
            $arr[$rTMP["id_place"]]["estado"]  = $rTMP["estado"];
            $arr[$rTMP["id_place"]]["estado_text"]  = $arrCatalogoPlace[$rTMP["estado"]]["nombre"];
            $arr[$rTMP["id_place"]]["id_usuario_alerta"]  = $rTMP["id_usuario_alerta"];
            $arr[$rTMP["id_place"]]["identificador"]  = $rTMP["identificador"];
            $arr[$rTMP["id_place"]]["mensaje"]  = $rTMP["mensaje"];
            $arr[$rTMP["id_place"]]["tipo"]  = $rTMP["tipo"];
            $arr[$rTMP["id_place"]]["url_place"]  = $rTMP["url_place"];
            
            if( !isset($arr[$rTMP["id_place"]]["visita_publica"]) )
                $arr[$rTMP["id_place"]]["visita_publica"] = array();
            
            if( !isset($arr[$rTMP["id_place"]]["visita_usuario"]) )
                $arr[$rTMP["id_place"]]["visita_usuario"] = array();
            
            if( intval($rTMP["id_place_visita"]) ){
                
                if( $rTMP["publico"] == "Y" ){
                    
                    $arr[$rTMP["id_place"]]["visita_publica"][$rTMP["id_place_visita"]]  = $rTMP["id_place_visita"];
                    
                }
                else{
                    
                    $arr[$rTMP["id_place"]]["visita_usuario"][$rTMP["id_place_visita"]]  = $rTMP["id_place_visita"];
                    
                }
                
            }
                
            if( !isset($arr[$rTMP["id_place"]]["clasificacion"]) )
                $arr[$rTMP["id_place"]]["clasificacion"] = "";
            
            if( !isset($arr[$rTMP["id_place"]]["clasificacionArray"][$rTMP["id_clasificacion"]]) ){
                
                $arr[$rTMP["id_place"]]["clasificacionArray"][$rTMP["id_clasificacion"]] = $rTMP["id_clasificacion"];
                $arr[$rTMP["id_place"]]["clasificacion"]  .= ( $arr[$rTMP["id_place"]]["clasificacion"] == "" ? "" : "," )." ".$rTMP["tag_es"];
                
            }    
                            
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arr;
        
                
    }
    
    public function getRestriccionUsuario($intUsuario){
        
        $strQuery = "SELECT usuario_place_restrinccion.id_usuario_place_restrinccion,
                            usuario_place_restrinccion.max_place
                     FROM   usuario,
                            usuario_place_restrinccion
                     WHERE  usuario.id_usuario = {$intUsuario}
                     AND    usuario.id_usuario_place_restrinccion = usuario_place_restrinccion.id_usuario_place_restrinccion     ";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        return $rTMP;
        
    }
                                   
    public function getUsuarioPlace($intUsuario, $intPlace = 0){
        
        $intUsuario = intval($intUsuario);
        $intPlace = intval($intPlace);
        
        $strFiltro = $intPlace ? "AND   id_place = {$intPlace} " : "";
        
        $strQuery = "SELECT id_place,
                            titulo,
                            num_producto,
                            num_galeria,
                            estado
                     FROM   place   
                     WHERE  id_usuario = {$intUsuario}
                     {$strFiltro} ";    
        
        $arrPlace = array();
        
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrPlace[$rTMP["id_place"]]["id_place"] = $rTMP["id_place"];
            $arrPlace[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
            $arrPlace[$rTMP["id_place"]]["num_producto"] = $rTMP["num_producto"];
            $arrPlace[$rTMP["id_place"]]["num_galeria"] = $rTMP["num_galeria"];
            $arrPlace[$rTMP["id_place"]]["estado"] = $rTMP["estado"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        return $arrPlace;
                        
    }                     
    public function getUsuarioPlaceaa($intUsuario, $intPlace = 0){
        
        $intUsuario = intval($intUsuario);
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_place,
                            titulo,
                            num_producto,
                            num_galeria,
                            estado
                     FROM   place   
                     WHERE  id_usuario = {$intUsuario}
                     AND   id_place = {$intPlace} ";    
        
        $arrPlace = array();
        
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrPlace[$rTMP["id_place"]]["id_place"] = $rTMP["id_place"];
            $arrPlace[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
            $arrPlace[$rTMP["id_place"]]["num_producto"] = $rTMP["num_producto"];
            $arrPlace[$rTMP["id_place"]]["num_galeria"] = $rTMP["num_galeria"];
            $arrPlace[$rTMP["id_place"]]["estado"] = $rTMP["estado"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        return $arrPlace;
                        
    }
    
    public function getPlaceClasificacion($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion
                     FROM   place_clasificacion
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion"]] = $rTMP["id_clasificacion"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadre($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT place_clasificacion_padre.id_clasificacion_padre,
                            place_clasificacion_padre.producto,
                            clasificacion_padre.nombre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es
                     FROM   place_clasificacion_padre,
                            clasificacion_padre
                     WHERE  place_clasificacion_padre.id_place = {$intPlace}
                     AND    place_clasificacion_padre.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["producto"] = $rTMP["producto"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadreHijo($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion_padre_hijo
                     FROM   place_clasificacion_padre_hijo
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadreHijoDetalle($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion_padre_hijo_detalle
                     FROM   place_clasificacion_padre_hijo_detalle
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo_detalle"]] = $rTMP["id_clasificacion_padre_hijo_detalle"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function setPlace($intUsuario, $strTitulo, $intCantidadProducto, $intCantidadGaleria, $strDescripcionNegocio,
                             $strNombreCompletoDuenio, $strTelefono, $strEmail, $strLat, $strLng, $strPlaceId, $intUbicacionLugar,
                             $strTelefonoDuenio, $strEmailDuenio, $intTipoNegocio, $intNegocio){
        
        $intUsuario = intval($intUsuario);
        $intCantidadProducto = intval($intCantidadProducto);
        $intCantidadGaleria = intval($intCantidadGaleria);
        $strTitulo = trim($strTitulo);
        
        
        $strQuery = "SELECT galeria,
                            producto_categoria
                     FROM   usuario,
                            usuario_place_restrinccion
                     WHERE  usuario.id_usuario = {$intUsuario}
                     AND    usuario.id_usuario_place_restrinccion = usuario_place_restrinccion.id_usuario_place_restrinccion ";    
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        $intCantidadProducto = $rTMP["producto_categoria"];
        $intCantidadGaleria = $rTMP["galeria"];
        
        $strPreguntaPlace = empty($strPlaceId) ? "N" : "Y";
        
        
        
        
        
        /*********************************************************************************/
        
        $strDireccion = "";    
        $strLat = "";    
        $strLng = "";    
        $strEstrellas = "0";    
        $arrGaleriaNegocio = array();
        
        if( $intNegocio ){
            
            $strQuery = "SELECT negocio.id_negocio,
                                negocio.id_ubicacion_lugar,
                                negocio.nombre,
                                negocio.lat,
                                negocio.lng,
                                negocio.direccion,
                                negocio.rango,
                                negocio.telefono,
                                negocio.precio,
                                negocio_galeria.id_negocio_galeria,
                                negocio_galeria.url
                                
                         FROM   negocio,
                                negocio_galeria
                         WHERE  negocio.id_negocio = {$intNegocio}
                         AND    negocio.id_negocio = negocio_galeria.id_negocio
                         ";
            
            
            $qTMP = $this->objDBClass->db_consulta($strQuery);
            while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
                
                
                $arrGaleriaNegocio[$rTMP["id_negocio_galeria"]]["url"] = $rTMP["url"];    
                
                $strTelefono = $rTMP["telefono"];    
                $strDireccion = $rTMP["direccion"];    
                $strLat = $rTMP["lat"];    
                $strLng = $rTMP["lng"];    
                $strEstrellas = $rTMP["rango"];    
                
            }
            
            $this->objDBClass->db_free_result($qTMP);
            
        }
        
        /*********************************************************************************/
        
        $strEstrellas = intval($strEstrellas);
        $strDireccion = fntCoreStringQueryLetra($strDireccion);
        
        $strQuery = "INSERT INTO place(id_usuario, titulo, num_producto, num_galeria, estado, place_id, place_pregunta, id_ubicacion_lugar, 
                                       telefono, direccion, ubicacion_lat, ubicacion_lng, estrellas)
                                VALUES({$intUsuario}, '{$strTitulo}', {$intCantidadProducto}, {$intCantidadGaleria}, 'C' , '{$strPlaceId}', '{$strPreguntaPlace}', {$intUbicacionLugar}, 
                                        '{$strTelefono}', '{$strDireccion}', '{$strLat}', '{$strLng}', '{$strEstrellas}' )";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $intIdPlace = $this->objDBClass->db_last_id();
        
        $strQuery = "INSERT INTO sol_place(id_usuario, id_place, descripcion, estado,
                                           nombre, telefono, email, place_id, lat, lng,
                                           telefono_propietario, email_propietario, tipo_negocio, id_negocio )
                                   VALUES({$intUsuario}, '{$intIdPlace}', '{$strDescripcionNegocio}', 'C',
                                          '{$strNombreCompletoDuenio}', '{$strTelefono}', '{$strEmail}', '{$strPlaceId}', '{$strLat}', '{$strLng}',
                                          '{$strTelefonoDuenio}', '{$strEmailDuenio}', '1', {$intNegocio}   )";
                                          
        
        
        $this->objDBClass->db_consulta($strQuery);
        
        if( count($arrGaleriaNegocio) > 0  ){
            
            $intCount = 1;
            while( $rTMP = each($arrGaleriaNegocio)  ){
                
                if( file_exists($rTMP["value"]["url"]) ){
                    
                    $strQuery = "INSERT INTO place_galeria(id_place)
                                                    VALUES({$intIdPlace})";
                    $this->objDBClass->db_consulta($strQuery);
                    $intPlaceGaleria = $this->objDBClass->db_last_id();
                    
                    $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".webp";
                    $strUrlArchivo = "../../file_inguate/place_interno/galeria/".fntCoreEncrypt($intPlaceGaleria).".webp";
                    copy($rTMP["value"]["url"], $strUrlArchivo);
                    
                    fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceGaleria));
                    
                    $strQuery = "UPDATE place_galeria
                                 SET    url_imagen = '{$strUrlArchivo}',
                                        orden = '{$intCount}' 
                                 WHERE  id_place_galeria = {$intPlaceGaleria}";
                    $this->objDBClass->db_consulta($strQuery);
                    $intCount++;
                                        
                }
                                
            }
            
        }
        
        return $intIdPlace;        
    }
    
    public function setEditPlace($intPlace, $strTitulo, $strDescripcionNegocio = ""){
        
        $intPlace = intval($intPlace); 
        $strTitulo = trim($strTitulo); 
        
        $strQuery = "UPDATE place
                     SET    titulo = '{$strTitulo}'  
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
                
        $strQuery = "UPDATE sol_place
                     SET    descripcion = '{$strDescripcionNegocio}'  
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setDeletePlaceClasificacionPadre($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion_padre WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlaceClasificacion($intPlace, $intPlaceClasificacion){
        
        $strQuery = "INSERT INTO place_clasificacion (id_place, id_clasificacion)
                                                VALUES( {$intPlace}, {$intPlaceClasificacion} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
        
    public function setDeletePlaceClasificacion($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlaceClasificacionPadre($intPlace, $intPlaceClasificacionPadre){
        
        $strQuery = "INSERT INTO place_clasificacion_padre (id_place, id_clasificacion_padre)
                                                VALUES( {$intPlace}, {$intPlaceClasificacionPadre} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
       
    public function setEnvioPagoPlace($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "UPDATE place
                     SET    estado = 'A'
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM usuario_alerta WHERE identificador = {$intPlace}";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setUrlArchivoSolPlace($intPlace, $strUrlArchivo){
        
        $strQuery = "UPDATE sol_place
                     SET    url_patente = '{$strUrlArchivo}' 
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
                
    }
    
    public function setUrlArchivoSolPlaceDPI($intPlace, $strUrlArchivo){
        
        $strQuery = "UPDATE sol_place
                     SET    url_dpi = '{$strUrlArchivo}' 
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
                
    }
    
    public function setUrlArchivoSolPlaceSA($intPlace, $strUrlArchivo){
        
        $strQuery = "UPDATE sol_place
                     SET     url_sa = '{$strUrlArchivo}' 
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
                
    }
    
    public function getPlaceVisitaUsuario($intPlace){
        
        $strQuery = "SELECT place_visita.id_place_visita,
                            MAX(place_visita.add_fecha) add_fecha,
                            usuario.id_usuario,
                            usuario.email,
                            usuario.telefono,
                            usuario.nombre,
                            usuario.apellido
                     FROM   place_visita,
                            usuario
                     WHERE  place_visita.id_place = {$intPlace}
                     AND    place_visita.publico = 'N'
                     AND    place_visita.id_usuario = usuario.id_usuario
                     GROUP BY id_usuario 
                     ORDER BY place_visita.add_fecha DESC
                     ";
        $arrResultado = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrResultado[$rTMP["id_place_visita"]]["id_place_visita"] = $rTMP["id_place_visita"];
            $arrResultado[$rTMP["id_place_visita"]]["add_fecha"] = $rTMP["add_fecha"];
            $arrResultado[$rTMP["id_place_visita"]]["id_usuario"] = $rTMP["id_usuario"];
            $arrResultado[$rTMP["id_place_visita"]]["email"] = $rTMP["email"];
            $arrResultado[$rTMP["id_place_visita"]]["telefono"] = $rTMP["telefono"];
            $arrResultado[$rTMP["id_place_visita"]]["nombre"] = $rTMP["nombre"];
            $arrResultado[$rTMP["id_place_visita"]]["apellido"] = $rTMP["apellido"];
                
        }
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrResultado;
                
    }
    
    public function getPlaceProducto($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_place_producto,
                            nombre,
                            precio
                     FROM   place_producto
                     WHERE  id_place = {$intPlace}
                     ORDER BY id_place_producto              
                     ";
        $arrPlaceProducto = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrPlaceProducto[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
            $arrPlaceProducto[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
            $arrPlaceProducto[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrPlaceProducto;        
    }
    
    public function setSolicitudCategoriaEspecial($intPlace, $intPlaceProducto, $intCategoriaEspecial, $intFrecuenciaPago, $strDescripcion,
                                                   $strFechaInicio, $strFechaFin){
        
    
        
        $strQuery = "INSERT INTO sol_categoria_especial(id_place_producto, id_categoria_especial, id_frecuencia, estado, descripcion,
                                                        fecha_inicio, fecha_fin)
                                                 VALUES({$intPlaceProducto}, {$intCategoriaEspecial}, {$intFrecuenciaPago}, 'S', '{$strDescripcion}',
                                                        '{$strFechaInicio}', '{$strFechaFin}')";
        $this->objDBClass->db_consulta($strQuery);
                                                       
    }
    
    public function setSolicitudCategoriaEspecialUpdate($intSolicitudCategoriaEspecial, $intPlace, $intPlaceProducto, $intCategoriaEspecial, $intFrecuenciaPago, $strDescripcion,
                                                        $strFechaInicio, $strFechaFin){
        
    
        
        $strQuery = "UPDATE sol_categoria_especial 
                     SET    id_place_producto = {$intPlaceProducto},
                            id_categoria_especial = {$intCategoriaEspecial},
                            id_frecuencia = {$intFrecuenciaPago},
                            estado = 'S',
                            descripcion = '{$strDescripcion}',
                            fecha_inicio = '{$strFechaInicio}',
                            fecha_fin = '{$strFechaFin}'
                            
                     WHERE  id_sol_categoria_especial = {$intSolicitudCategoriaEspecial} ";
        
        $this->objDBClass->db_consulta($strQuery);
                                                       
    }
    
    public function getSolicitudCategoriaEspecial($arrPlace, $intSolicitudCategoriaEspecial = 0){
        
        $arr = array();
        $arrRechazo = array();
        if( is_array($arrPlace) &&  count($arrPlace) > 0 ){
            
            $strKeyPlace = "";
            while( $rTMP = each($arrPlace) ) {
                
                $strKeyPlace .= ( $strKeyPlace == "" ? "" : "," ).$rTMP["key"] ;   
                
            }
            
            $arrCategoriaEspecial = fntGetCatalogoCategoriaEspecial(true, $this->getobjDBClass());
            $arrCategoriaEspecialFrecuenciaPago = fntGetCatalogoCategoriaEspecialFrecuenciaPago();
            $arrEstadoSolCategoriaEspecial = fntGetCatalogoCategoriaEspecialSolicitudEstado();
            
            $intSolicitudCategoriaEspecial = intval($intSolicitudCategoriaEspecial);
            $strFiltroSolicitudCategoriaEspecial = "";
            if( $intSolicitudCategoriaEspecial )            
                $strFiltroSolicitudCategoriaEspecial = " AND    sol_categoria_especial.id_sol_categoria_especial = {$intSolicitudCategoriaEspecial}  ";
                
                
            $strQuery = "SELECT sol_categoria_especial.id_sol_categoria_especial,
                                sol_categoria_especial.id_categoria_especial,
                                sol_categoria_especial.id_frecuencia,
                                sol_categoria_especial.fecha_inicio,
                                sol_categoria_especial.fecha_fin,
                                sol_categoria_especial.estado,
                                sol_categoria_especial.descripcion,
                                place_producto.id_place_producto,
                                place_producto.nombre,
                                place.id_place,
                                place.titulo
                         FROM   sol_categoria_especial,
                                place_producto,
                                place   
                         WHERE  sol_categoria_especial.estado IN('S', 'A', 'R')
                         AND    sol_categoria_especial.id_place_producto = place_producto.id_place_producto
                         AND    place_producto.id_place = place.id_place
                         AND    place_producto.id_place IN ({$strKeyPlace})
                         {$strFiltroSolicitudCategoriaEspecial}
                         ";
            $qTMP = $this->objDBClass->db_consulta($strQuery);
            while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
                
                $arr[$rTMP["id_sol_categoria_especial"]]["id_sol_categoria_especial"] = $rTMP["id_sol_categoria_especial"];
                $arr[$rTMP["id_sol_categoria_especial"]]["id_categoria_especial"] = $rTMP["id_categoria_especial"];
                $arr[$rTMP["id_sol_categoria_especial"]]["categoria_especial_nombre"] =  $arrCategoriaEspecial[$rTMP["id_categoria_especial"]]["nombre"];
                $arr[$rTMP["id_sol_categoria_especial"]]["id_frecuencia"] = $rTMP["id_frecuencia"];
                $arr[$rTMP["id_sol_categoria_especial"]]["frecuencia_nombre"] = $arrCategoriaEspecialFrecuenciaPago[$rTMP["id_frecuencia"]]["nombre"];
                $arr[$rTMP["id_sol_categoria_especial"]]["fecha_inicio"] = $rTMP["fecha_inicio"];
                $arr[$rTMP["id_sol_categoria_especial"]]["fecha_fin"] = $rTMP["fecha_fin"];
                $arr[$rTMP["id_sol_categoria_especial"]]["estado"] = $rTMP["estado"];
                $arr[$rTMP["id_sol_categoria_especial"]]["estado_text"] = $arrEstadoSolCategoriaEspecial[$rTMP["estado"]]["nombre"];
                $arr[$rTMP["id_sol_categoria_especial"]]["descripcion"] = $rTMP["descripcion"];
                $arr[$rTMP["id_sol_categoria_especial"]]["id_place_producto"] = $rTMP["id_place_producto"];
                $arr[$rTMP["id_sol_categoria_especial"]]["nombre"] = $rTMP["nombre"];
                $arr[$rTMP["id_sol_categoria_especial"]]["id_place"] = $rTMP["id_place"];
                $arr[$rTMP["id_sol_categoria_especial"]]["titulo"] = $rTMP["titulo"];
                $arr[$rTMP["id_sol_categoria_especial"]]["motivo_rechazo"] = "";
                $arr[$rTMP["id_sol_categoria_especial"]]["fecha_rechazo"] = "";
                $arr[$rTMP["id_sol_categoria_especial"]]["id_usuario_alerta_rechazo"] = "0";
                
                if( $rTMP["estado"] == "R" ){
                    
                    $arrRechazo[$rTMP["id_sol_categoria_especial"]] = $rTMP["id_sol_categoria_especial"];
                    
                }
                            
            }                  
            
            $this->objDBClass->db_free_result($qTMP);
                        
            if( count($arrRechazo) > 0  ){
                
                $strSolictudRechazo = implode(",", $arrRechazo);
                
                $strQuery = "SELECT id_usuario_alerta,
                                    identificador,
                                    mensaje,
                                    add_fecha
                             FROM   usuario_alerta
                             WHERE  tipo = 'SPE'
                             AND    identificador IN({$strSolictudRechazo})
                             
                             ";
                $qTMP = $this->objDBClass->db_consulta($strQuery);
                while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
                    
                    if( isset($arr[$rTMP["identificador"]]) ){
                                                                
                        $arr[$rTMP["identificador"]]["motivo_rechazo"] = $rTMP["mensaje"];
                        $arr[$rTMP["identificador"]]["fecha_rechazo"] = $rTMP["add_fecha"];
                        $arr[$rTMP["identificador"]]["id_usuario_alerta_rechazo"] = $rTMP["id_usuario_alerta"];
                        
                    } 
                                        
                }   
                
                $this->objDBClass->db_free_result($qTMP);             
                                
            }                        
            
            
        }
        
        
        return $arr;
    }
    
    public function getNegocioAutocomplete($strTexto){
        
        
        $strQuery = "SELECT id_negocio,
                            nombre,
                            lat,
                            lng
                     FROM   negocio
                     WHERE  nombre LIKE '%{$strTexto}%'
                     AND    estado = 'A'
                     LIMIT  30";
        $arrResultado = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrTMP["identificador"] = fntCoreEncrypt($rTMP["id_negocio"]);
            $arrTMP["nombre"] = $rTMP["nombre"];
            $arrTMP["lat"] = $rTMP["lat"];
            $arrTMP["lng"] = $rTMP["lng"];
            
            array_push($arrResultado, $arrTMP);
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrResultado;
        
        
    }
    
    public function fntGetSolPlace($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT *
                     FROM   sol_place   
                     WHERE  id_place = {$intPlace}";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        return $rTMP;
                
    }
    
    public function fntGetCodigoVendedor($intIdUsuario){
        
        $intIdUsuario = intval($intIdUsuario);
        
        $strCodigo = "";
        if( $intIdUsuario ){
            
            $strQuery = "SELECT codigo
                         FROM   usuario_vendedor
                         WHERE  id_usuario = {$intIdUsuario}";
            $qTMP = $this->objDBClass->db_consulta($strQuery);
            $rTMP = $this->objDBClass->db_fetch_array($qTMP);
            $this->objDBClass->db_free_result($qTMP);
            
            $strCodigo = isset($rTMP["codigo"]) ? $rTMP["codigo"] : "";
            
                        
        }
        
        return $strCodigo;
        
    }   
    
}

?>
