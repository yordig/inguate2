<?php
  
require_once "place_model.php";
require_once "place_view.php";

class place_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new place_model();
        $this->objView = new place_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["fntDrawFormNewPlace"]) ){
            
            $intPlace = isset($_GET["key"]) ? trim(fntCoreDecrypt($_GET["key"])) : "";
            $intPaso = isset($_GET["paso"]) ? intval(($_GET["paso"])) : "";
            
            $this->objView->fntDrawFormNewPlace($strAction, $intPlace, $intPaso);
            
            die();
            
        }
        
        if( isset($_GET["getDrawClas2"]) ){
            
            $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : "";
            
            $this->objView->fntDrawClas2($strAction, $strKey, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["getDrawClas3"]) ){
            
            $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : "";
            
            $this->objView->fntDrawClas3($strAction, $strKey, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["getDrawClas4"]) ){
            
            $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : "";
            
            $this->objView->fntDrawClas4($strAction, $strKey, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["setSavePlace"]) ){
            
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : 0;
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : 0;
            $intPlace = isset($_POST["hidIdPlace"]) ? intval($_POST["hidIdPlace"]) : 0;
            $strTitulo = isset($_POST["txtTitulo"]) ? addslashes(fntCoreClearToQuery($_POST["txtTitulo"])) : "";
            $strNombreCompletoDuenio = isset($_POST["txtNombreCompletoDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtNombreCompletoDuenio"])) : "";
            $strTelefono = isset($_POST["txtTelefonoNegocio"]) ? addslashes(fntCoreClearToQuery($_POST["txtTelefonoNegocio"])) : "";
            $strEmail = isset($_POST["txtEmailDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtEmailDuenio"])) : "";
            $strLat = isset($_POST["hidLatUbicacion"]) ? addslashes($_POST["hidLatUbicacion"]) : "";
            $strLng = isset($_POST["hidLngUbicacion"]) ? addslashes($_POST["hidLngUbicacion"]) : "";
            $strPlaceId = isset($_POST["hidPlaceId"]) ? addslashes($_POST["hidPlaceId"]) : "";
            $strDescripcionNegocio = isset($_POST["txtDescripcion"]) ? addslashes(fntCoreClearToQuery($_POST["txtDescripcion"])) : "";
            $intCantidadProducto = isset($_POST["txtCantidadProductos"]) ? intval($_POST["txtCantidadProductos"]) : "";
            $intCantidadGaleria = isset($_POST["txtCantidadGaleria"]) ? intval($_POST["txtCantidadGaleria"]) : "";
            $intUbicacionLugar = isset($_POST["slcUbicacionLugar"]) ? intval($_POST["slcUbicacionLugar"]) : "";
            
            $intUbicacionLugar = isset($_POST["slcUbicacionLugar"]) ? intval($_POST["slcUbicacionLugar"]) : "";
            
            $strTelefonoDuenio = isset($_POST["txtTelefonoDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtTelefonoDuenio"])) : "";
            $strEmailDuenio = isset($_POST["txtEmailPropietario"]) ? addslashes(fntCoreClearToQuery($_POST["txtEmailPropietario"])) : "";
            $intTipoNegocio = isset($_POST["slcTipoNegocio"]) ? intval($_POST["slcTipoNegocio"]) : "";
                  
            if( !$intPlace ){
                
                $intPlace = $this->objModel->setPlace($intUsuario, $strTitulo, $intCantidadProducto, $intCantidadGaleria, $strDescripcionNegocio,
                                                      $strNombreCompletoDuenio, $strTelefono, $strEmail, $strLat, $strLng, $strPlaceId, $intUbicacionLugar,
                                                      $strTelefonoDuenio, $strEmailDuenio, $intTipoNegocio);
                $arr["action"] = "1";
                $arr["place"] = fntCoreEncrypt($intPlace);
                
                if( isset($_FILES["flPatente"]) && $_FILES["flPatente"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flPatente"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PC_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flPatente"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlace($intPlace, $strUrlArchivo);
                            
                }
            
                if( isset($_FILES["flDPI"]) && $_FILES["flDPI"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flDPI"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCdpi_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flDPI"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlaceDPI($intPlace, $strUrlArchivo);
                            
                }
            
                if( isset($_FILES["flSA"]) && $_FILES["flSA"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flSA"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCsa_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flSA"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlaceSA($intPlace, $strUrlArchivo);
                            
                }
            
            }
            else{
                
                $this->objModel->setEditPlace($intPlace, $strTitulo, $strDescripcionNegocio);
                $arr["action"] = "1";
                $arr["place"] = fntCoreEncrypt($intPlace);
                
            }
            
            $this->objModel->setDeletePlaceClasificacion($intPlace);
            if( isset($_POST["slcClasificacionPrincipal"]) && is_array($_POST["slcClasificacionPrincipal"]) && count($_POST["slcClasificacionPrincipal"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPrincipal"]) ){
                    
                    $this->objModel->setPlaceClasificacion($intPlace, $rTMP["value"]);
                    
                }
                
            }
            
            $this->objModel->setDeletePlaceClasificacionPadre($intPlace);
            if( isset($_POST["slcClasificacionPadre"]) && is_array($_POST["slcClasificacionPadre"]) && count($_POST["slcClasificacionPadre"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPadre"]) ){
                    
                    $this->objModel->setPlaceClasificacionPadre($intPlace, $rTMP["value"]);
                    
                }
                
            }      
            
            print json_encode($arr);
            
            die();
        }
        
        if( isset($_GET["setEnviarPago"]) ){
            
            $intPlace = isset($_POST["hidPlace"])  ? fntCoreDecrypt($_POST["hidPlace"]) : 0;
            
            $this->objModel->setEnvioPagoPlace($intPlace);
            
            die();
        }
        
        if( isset($_GET["getDrawMisUsuarios"]) ){
            
            $intPlace = isset($_GET["place"])  ? fntCoreDecrypt($_GET["place"]) : 0;
            
            $this->objView->getDrawMisUsuarios($strAction, $intPlace);
            
            die();
        }
        
        if( isset($_GET["drawShowModalContenidoSolicitud"]) ){
            
            $intSolicitudCategoriaEspecial = isset($_GET["solicitud"]) ? trim(fntCoreDecrypt($_GET["solicitud"])) : "";
            
            $this->objView->drawShowModalContenidoSolicitud($strAction, $intSolicitudCategoriaEspecial);
            
            die();
            
        }
        
        if( isset($_GET["drawGetProductoPlace"]) ){
            
            $intPlace = isset($_GET["place"]) ? trim(fntCoreDecrypt($_GET["place"])) : "";
            $intPlaceProducto = isset($_GET["placeProducto"]) ? trim(fntCoreDecrypt($_GET["placeProducto"])) : "";
            
            $this->objView->drawGetProductoPlace($strAction, $intPlace, $intPlaceProducto);
            
            die();
            
        }
        
        if( isset($_GET["setSolicitudCategoriaEspecial"]) ){
            
            $intSolicitudCategoriaEspecial = isset($_POST["hidSolicitud"]) ? intval(fntCoreDecrypt($_POST["hidSolicitud"])) : 0;
            $intPlace = isset($_POST["slcModalPlace"]) ? intval(fntCoreDecrypt($_POST["slcModalPlace"])) : 0;
            $intPlaceProducto = isset($_POST["slcModalPlaceProducto"]) ? intval(fntCoreDecrypt($_POST["slcModalPlaceProducto"])) : 0;
            $intCategoriaEspecial = isset($_POST["slcModalCategoriaEspecial"]) ? intval(fntCoreDecrypt($_POST["slcModalCategoriaEspecial"])) : 0;
            $intFrecuenciaPago = isset($_POST["slcModalFrecuencia"]) ? intval(fntCoreDecrypt($_POST["slcModalFrecuencia"])) : 0;
            $strDescripcion = isset($_POST["txtMotalMotivo"]) ? fntCoreClearToQuery($_POST["txtMotalMotivo"]) : 0;
            $strFechaInicio = isset($_POST["txtFechaInicio"]) ? ($_POST["txtFechaInicio"]) : 0;
            $strFechaFin = isset($_POST["txtFechaFin"]) ? ($_POST["txtFechaFin"]) : 0;
            
            
            if( intval($intSolicitudCategoriaEspecial) ){
                
                $this->objModel->setSolicitudCategoriaEspecialUpdate($intSolicitudCategoriaEspecial, $intPlace, $intPlaceProducto, $intCategoriaEspecial, $intFrecuenciaPago, $strDescripcion,
                                                               $strFechaInicio, $strFechaFin );
                
            }
            else{
                
                $this->objModel->setSolicitudCategoriaEspecial($intPlace, $intPlaceProducto, $intCategoriaEspecial, $intFrecuenciaPago, $strDescripcion,
                                                               $strFechaInicio, $strFechaFin );
                
            }
                                    
            die();
            
        }
        
        if( isset($_GET["getNegocioAutoComplete"]) ){
            
            $strTexto = isset($_GET["q"]) ? addslashes($_GET["q"]) : "";
            
            $arr = $this->objModel->getNegocioAutocomplete($strTexto);
            
            print json_encode($arr);            
            
            die();
        }
        
        if( isset($_GET["setCrearPlace"]) ){
            
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : 0;
            $intNegocio = isset($_POST["hidNegocio"]) ? intval(fntCoreDecrypt($_POST["hidNegocio"])) : 0;
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : 0;
            $intPlace = isset($_POST["hidIdPlace"]) ? intval($_POST["hidIdPlace"]) : 0;
            $strTitulo = isset($_POST["txtTitulo"]) ? addslashes(fntCoreClearToQuery($_POST["txtTitulo"])) : "";
            $strNombreCompletoDuenio = isset($_POST["txtNombreCompletoDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtNombreCompletoDuenio"])) : "";
            $strTelefono = isset($_POST["txtTelefonoNegocio"]) ? addslashes(fntCoreClearToQuery($_POST["txtTelefonoNegocio"])) : "";
            $strEmail = isset($_POST["txtEmailDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtEmailDuenio"])) : "";
            $strLat = isset($_POST["hidLatUbicacion"]) ? addslashes($_POST["hidLatUbicacion"]) : "";
            $strLng = isset($_POST["hidLngUbicacion"]) ? addslashes($_POST["hidLngUbicacion"]) : "";
            $strPlaceId = isset($_POST["hidPlaceId"]) ? addslashes($_POST["hidPlaceId"]) : "";
            $strDescripcionNegocio = isset($_POST["txtDescripcion"]) ? addslashes(fntCoreClearToQuery($_POST["txtDescripcion"])) : "";
            $intCantidadProducto = isset($_POST["txtCantidadProductos"]) ? intval($_POST["txtCantidadProductos"]) : "";
            $intCantidadGaleria = isset($_POST["txtCantidadGaleria"]) ? intval($_POST["txtCantidadGaleria"]) : "";
            $intUbicacionLugar = isset($_POST["slcUbicacionLugar"]) ? intval($_POST["slcUbicacionLugar"]) : "";
            
            $intUbicacionLugar = isset($_POST["slcUbicacionLugar"]) ? intval($_POST["slcUbicacionLugar"]) : "";
            
            $strTelefonoDuenio = isset($_POST["txtTelefonoDuenio"]) ? addslashes(fntCoreClearToQuery($_POST["txtTelefonoDuenio"])) : "";
            $strEmailDuenio = isset($_POST["txtEmailPropietario"]) ? addslashes(fntCoreClearToQuery($_POST["txtEmailPropietario"])) : "";
            $intTipoNegocio = isset($_POST["slcTipoNegocio"]) ? intval($_POST["slcTipoNegocio"]) : "";
                  
            if( !$intPlace ){
                
                $intPlace = $this->objModel->setPlace($intUsuario, $strTitulo, $intCantidadProducto, $intCantidadGaleria, $strDescripcionNegocio,
                                                      $strNombreCompletoDuenio, $strTelefono, $strEmail, $strLat, $strLng, $strPlaceId, $intUbicacionLugar,
                                                      $strTelefonoDuenio, $strEmailDuenio, $intTipoNegocio, $intNegocio);
                $arr["action"] = "1";
                $arr["place"] = fntCoreEncrypt($intPlace);
                  /*
                if( isset($_FILES["flPatente"]) && $_FILES["flPatente"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flPatente"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PC_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flPatente"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlace($intPlace, $strUrlArchivo);
                            
                }
            
                if( isset($_FILES["flDPI"]) && $_FILES["flDPI"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flDPI"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCdpi_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flDPI"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlaceDPI($intPlace, $strUrlArchivo);
                            
                }
            
                if( isset($_FILES["flSA"]) && $_FILES["flSA"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flSA"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCsa_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flSA"]["tmp_name"], $strUrlArchivo);
                    
                    $this->objModel->setUrlArchivoSolPlaceSA($intPlace, $strUrlArchivo);
                            
                }
                 */
            }
            else{
                
                $this->objModel->setEditPlace($intPlace, $strTitulo, $strDescripcionNegocio);
                $arr["action"] = "1";
                $arr["place"] = fntCoreEncrypt($intPlace);
            }
            
            $this->objModel->setDeletePlaceClasificacion($intPlace);
            if( isset($_POST["slcClasificacionPrincipal"]) && is_array($_POST["slcClasificacionPrincipal"]) && count($_POST["slcClasificacionPrincipal"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPrincipal"]) ){
                    
                    $this->objModel->setPlaceClasificacion($intPlace, $rTMP["value"]);
                    
                }
                
            }
            
            $this->objModel->setDeletePlaceClasificacionPadre($intPlace);
            if( isset($_POST["slcClasificacionPadre"]) && is_array($_POST["slcClasificacionPadre"]) && count($_POST["slcClasificacionPadre"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPadre"]) ){
                    
                    $this->objModel->setPlaceClasificacionPadre($intPlace, $rTMP["value"]);
                    
                }
                
            }      
            
            print json_encode($arr);
            
            die();
        }
        
        if( isset($_GET["fntDrawDetalleLugar"]) ){
            
            $strKey = isset($_GET["key"]) ? $_GET["key"] : "";
            $strAltoPx = isset($_GET["algopx"]) ? $_GET["algopx"] : "";
            
            $this->objView->drawPlaceDetalle($strAction, $strKey, $strAltoPx);
            
            die();
        }
        
        if( isset($_GET["drawShowVerificacion"]) ){
            
            $intPlace = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : "";
            
            $this->objView->drawShowVerificacion($strAction,  $intPlace);
            
            die();
        }
        
        if( isset($_GET["getDrawPasosRegistro"]) ){
            
            $this->objView->getDrawPasosRegistro();
            
            die();
        }
        
        if( isset($_GET["setEnvioSolicitud"]) ){
            
            $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
            $strCodigoVendedor = isset($_POST["txtCodigoVendedor"]) ? addslashes($_POST["txtCodigoVendedor"]) : "";
            $strNombrePropietario = isset($_POST["txtNombrePropietario"]) ? addslashes($_POST["txtNombrePropietario"]) : "";
            $strTelefonoPropietario = isset($_POST["txtTelefonoPropietario"]) ? addslashes($_POST["txtTelefonoPropietario"]) : "";
            $strEmailPropietario = isset($_POST["txtEmailPropietario"]) ? addslashes($_POST["txtEmailPropietario"]) : "";
            $intTipoNegocio = isset($_POST["slcTipoNegocio"]) ? intval($_POST["slcTipoNegocio"]) : "";
            $strNombreNegocio = isset($_POST["hidNombreNegocio"]) ? trim($_POST["hidNombreNegocio"]) : "";
                       
            $arr["error"] = "false"; 
            $arr["msn"] = lang["error_completa_los_datos"]; 
            
            if( $intPlace ){
                
                $strQuery = "SELECT usuario.id_usuario,
                                    usuario.nombre
                             FROM   usuario_vendedor,
                                    usuario
                             WHERE  usuario_vendedor.codigo = '{$strCodigoVendedor}'
                             AND    usuario_vendedor.activo = 'A'
                             AND    usuario_vendedor.id_usuario = usuario.id_usuario
                              ";
                $qTMP = $this->objModel->getobjDBClass()->db_consulta($strQuery);
                $rTMP = $this->objModel->getobjDBClass()->db_fetch_array($qTMP);
                $this->objModel->getobjDBClass()->db_free_result($qTMP);
                
                $intIdUsuarioVendedor = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
                
                $arrSolPlace = $this->objModel->fntGetSolPlace($intPlace);   
        
                $strEstado = ", estado = 'S' ";
                
                if( isset($arrSolPlace["estado"]) && $arrSolPlace["estado"] == 'A'  ){
                    
                    $strEstado = " ";
                    
                    $arr["error"] = "false"; 
                    $arr["msn"] = lang["datos_guardados"]; 
                    
                }
                else{
                    
                    $strQuery = "UPDATE place
                                 SET    estado = 'S'
                                 WHERE  id_place = {$intPlace} ";
                                 
                    $this->objModel->getobjDBClass()->db_consulta($strQuery);
                    
                    fntEnviarCorreo("info@inguate.com", "Solicitud de Negocio INGUATE", "Solicitud de Negocio: {$strNombreNegocio}");
                    
                    $arr["error"] = "false"; 
                    $arr["msn"] = lang["aviso_envio_sol_place"]; 
                    
                }
                
                $strQuery = "UPDATE sol_place
                             SET    nombre = '{$strNombrePropietario}',
                                    telefono_propietario = '{$strTelefonoPropietario}',
                                    email_propietario = '{$strEmailPropietario}',
                                    tipo_negocio = '{$intTipoNegocio}',
                                    id_usuario_vendedor = '{$intIdUsuarioVendedor}'
                                    {$strEstado}
                             WHERE  id_place = {$intPlace} ";
                             
                $this->objModel->getobjDBClass()->db_consulta($strQuery);
                
                if( isset($_FILES["flPatente"]) && $_FILES["flPatente"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flPatente"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PC_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flPatente"]["tmp_name"], $strUrlArchivo);
                    
                    $strQuery = "UPDATE sol_place
                                 SET    url_patente = '{$strUrlArchivo}'
                                 WHERE  id_place = {$intPlace} ";
                                 
                    $this->objModel->getobjDBClass()->db_consulta($strQuery);
                    
                            
                }

                if( isset($_FILES["flDPI"]) && $_FILES["flDPI"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flDPI"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCdpi_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flDPI"]["tmp_name"], $strUrlArchivo);
                    
                    $strQuery = "UPDATE sol_place
                                 SET    url_dpi = '{$strUrlArchivo}'
                                 WHERE  id_place = {$intPlace} ";
                                 
                    $this->objModel->getobjDBClass()->db_consulta($strQuery);
                            
                }

                if( isset($_FILES["flSA"]) && $_FILES["flSA"]["size"] > 0 ){
                        
                    $arrNameFile = explode(".", $_FILES["flSA"]["name"]); 
                    $strUrlArchivo = "../../file_inguate/place/PCsa_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
                    rename($_FILES["flSA"]["tmp_name"], $strUrlArchivo);
                    
                    $strQuery = "UPDATE sol_place
                                 SET    url_sa = '{$strUrlArchivo}'
                                 WHERE  id_place = {$intPlace} ";
                                 
                    $this->objModel->getobjDBClass()->db_consulta($strQuery);
                            
                }
                
            }
            
            print json_encode($arr);            
            die();
        }
        
        if( isset($_GET["drawBotonPaso1"]) ){
            
            $this->objView->drawBotonPaso1();
            
            die();
        }
                             
        if( isset($_GET["drawBotonPaso2"]) ){
            
            $strPlace = isset($_GET["place"]) ? $_GET["place"] : "";
            
            $this->objView->drawBotonPaso2($strPlace);
            
            die();
        }
                             
        if( isset($_GET["drawBotonPaso3"]) ){
            
            $strPlace = isset($_GET["place"]) ? $_GET["place"] : "";
            
            $this->objView->drawBotonPaso3($strPlace);
            
            die();
        }  
                     
        if( isset($_GET["setPaginaPlace"]) ){
            
            $strData = isset($_POST["data"]) ? $_POST["data"] : "";
            
            $fp = fopen("posada-san-carlos.html","w+");
            fwrite($fp,$strData);
            fclose($fp);
            
            die();
        }
                             
    }
    
}

  
?>
