<?php

require_once "place_model.php";

class place_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new place_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        $arrPlace = $this->objModel->getPlaceUsuario($_SESSION["_open_antigua"]["core"]["id_usuario"]);
        $arrRestriccionUsuario = $this->objModel->getRestriccionUsuario($_SESSION["_open_antigua"]["core"]["id_usuario"]);
        
        $arrSolicitudCategoriaEspecial = $this->objModel->getSolicitudCategoriaEspecial($arrPlace);        
        
        ?>
        
        <div class="preloader">
            <div class="preloaderdetalle">
                <img src="dist/images/30.gif" alt="NILA">
            </div>
        </div> 
        <style>
            
            .preloader {
                opacity: 0.5;
                height: 100%;
                width: 100%;
                background: #FFF;
                position: fixed;
                top: 0;
                left: 0;
                z-index: 9999999;
            }
            
            .preloader .preloaderdetalle {
                position: absolute;
                top: 50%;
                left: 50%;
                -webkit-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                width: 120px;
            }
        </style>
        
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore(true);
            ?>    
            
            <link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
            <link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> <!-- for live demo page -->
            <script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
            
            <script src="dist_interno/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
            
            <input type="hidden" id="hidIdPlaceGeneral">
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-0  " id="divContenidoBody"   >
                      
                      
                    <div class="row">
                            
                        <div class="col-12 mt-1">
                        
                            <?php
                            
                            if( count($arrPlace) < $arrRestriccionUsuario["max_place"] ){
                                ?>
                                <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewPlace('0', '1');"><?php print lang["nuevo_negocio"]?></button>
                                <?php
                            }
                            
                            ?>
                                
                        </div>    
                    </div>
                    
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-11">    
                            <br>
                            
                            <div class="row">
                                <div class="col-4 col-lg-3 border-bottom mb-2">
                                    <?php print lang["mi_negocio"]?>
                                </div>
                                <div class="col-4 col-lg-2 border-bottom mb-2">
                                    <?php print lang["visitas"]?>
                                </div>
                                <div class="col-4 col-lg-2 border-bottom mb-2">
                                    <?php print lang["estado"]?>
                                </div>
                            </div>
                            
                            <?php 
                            
                            while( $rTMP = each($arrPlace) ){
                                
                                ?>
                                <div class="row">
                                    <div class="col-4 col-lg-3 text-secondary">
                                        <?php print $rTMP["value"]["titulo"]?>
                                    </div>
                                    <div class="col-4 col-lg-2 text-secondary">
                                        <?php print count($rTMP["value"]["visita_usuario"])?>
                                    </div>
                                    <div class="col-4 col-lg-2 text-secondary">
                                        <?php
                                        print $rTMP["value"]["estado_text"];
                                        ?>
                                    </div>
                                    <div class="col-12 col-lg-5 text-center mt-2 mt-lg-0">
                                        
                                        <button onclick="location.href = '/<?php print $rTMP["value"]["url_place"]?>'" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 130px; border-radius: 5px;"><?php print lang["ver_pagina_publica"]?></button>
                                        <button onclick="fntDrawFormNewPlace('<?php print fntCoreEncrypt($rTMP["key"])?>', '1');" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px;"><?php print lang["editar"]?> info</button>
                                        <button onclick="fntDrawFormPlacePagina('<?php print fntCoreEncrypt($rTMP["key"])?>', false);" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px;"><?php print lang["editar_pagina"]?></button>
                                        <button onclick="fntDrawMisUsuarios('<?php print fntCoreEncrypt($rTMP["key"])?>');" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px;"><?php print lang["mis_usuario"]?></button>
                                                    
                                        
                                    </div>
                                </div>
                                <?php
                                
                            }
                            
                            ?>
                            
                        </div>    
                    </div>
                    
                    <?php
                    
                    if( false ){
                        ?>
                        <div class="row">
                            <div class="col-12">
                                <table class="table  table-hover table-responsive-sm">
                                    <tbody>
                                        <tr>
                                            <td scope="col" class="border-0" colspan="4">
                                                <?php print lang["sol_categoria_especial"]?>
                                                <span class="btn-group-sm">
                                                    <button onclick="fntShowModalContenidoSolicitud('0');" type="button" class="btn btn-primary btn-raised btn bmd-btn-icon-sm bmd-btn-icon" style="width: 1.1rem !important; min-width: 1.2rem !important; height: 1.3rem" >
                                                        <i class="material-icons" style="font-size: 12px;">add</i>
                                                    </button>                            
                                                </span>
                                            </td>
                                        </tr>
                                        <?php 
                                        
                                        if( count($arrSolicitudCategoriaEspecial) > 0 ){
                                            ?>
                                            <tr>
                                                <td scope="col" class="border-0"><?php print lang["nombre"]?></td>
                                                <td scope="col" class="border-0"><?php print lang["mi_negocio"]?></td>
                                                <td scope="col" class="border-0"><?php print lang["categoria_especial"]?></td>
                                                <td scope="col" class="border-0"><?php print lang["descripcion"]?></td>
                                                <td scope="col" class="border-0"><?php print lang["fecha_inicio"]?></td>
                                                <td scope="col" class="border-0"><?php print lang["fecha_fin"]?></td>
                                                <td scope="col" class="border-0"><?php print lang["estado"]?></td>
                                                <td scope="col" class="border-0" style="width: 10%">&nbsp;</td>
                                            </tr>
                                            <?php
                                            
                                            while( $rTMP = each($arrSolicitudCategoriaEspecial) ){
                                                ?>
                                                <tr style="font-size: 14px;">
                                                    <td class="border-0 text-secondary"><?php print $rTMP["value"]["nombre"]?></td>
                                                    <td class="border-0 text-secondary"><?php print $rTMP["value"]["titulo"]?></td>
                                                    <td class="border-0 text-secondary"><?php print $rTMP["value"]["categoria_especial_nombre"]?></td>
                                                    <td class="border-0 text-secondary"><?php print $rTMP["value"]["descripcion"]?></td>
                                                    <td class="border-0 text-secondary"><?php print $rTMP["value"]["fecha_inicio"]?></td>
                                                    <td class="border-0 text-secondary"><?php print $rTMP["value"]["fecha_fin"]?></td>
                                                    <td class="border-0 text-secondary">
                                                        
                                                        <?php 
                                                        print $rTMP["value"]["estado_text"];
                                                        
                                                        if( $rTMP["value"]["estado"] == "R" ){
                                                            
                                                            ?>
                                                            <button class="btn btn-danger btn-sm btn-raised" type="button" data-toggle="collapse" data-target="#collapseMotivoRechazo" aria-expanded="false" aria-controls="collapseMotivoRechazo">
                                                                Ver Motivo
                                                            </button>
                                                            <div class="collapse" id="collapseMotivoRechazo">
                                                                <div class="card card-body">
                                                                    <?php print print $rTMP["value"]["motivo_rechazo"];?>
                                                                </div>
                                                            </div>
                                                            
                                                                                                                        
                                                            <?php
                                                            
                                                                
                                                        }
                                                        
                                                        ?>
                                                        
                                                    </td>
                                                    <td class="text-right nowrap border-0" nowrap>
                                                        
                                                        <button onclick="fntShowModalContenidoSolicitud('<?php print fntCoreEncrypt($rTMP["key"])?>');" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 80px; border-radius: 5px;"><?php print lang["edit"]?></button>
                                                            
                                                    </td>
                                                </tr>
                                                <?php                                        
                                            }
                                            
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                    
                            </div>    
                        </div>
                        
                        <?php
                    }
                    
                    ?>
                        
                
                </div>
            </main>
        
            <div class="modal fade" id="mlMisUsuarios" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg" style="min-width: 80%;" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?php print lang["mis_usuario"]?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="divBodyModalMisUsuarios">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <!-- Modal -->
            <div class="modal fade" id="mlSolicitudCategoriaEspecial" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><?php print lang["solicitud"]?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mb-0 pb-0" id="mlDivContenidoSolicitudCategoriaEspecial">
                            Contenido
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
             
        <script>
        
            $(document).ready(function() { 
                
                <?php 
                
                if( count($arrPlace) == 0 ){
                    ?>
                    fntDrawFormNewPlace('1');
                    <?php
                }
                
                ?>
                
                $(".preloader").fadeOut();
                
                
            });
            
            function fntDrawFormNewPlace(intPlace){
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawFormNewPlace=true&usuario=<?php print $_SESSION["_open_antigua"]["core"]["id_usuario"]?>&place="+intPlace, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }   
            
            function fntDrawEnviarPago(strPlace, strTitulo){
                
                var formData = new FormData();
                formData.append("hidPlace", strPlace);
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setEnviarPago=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        swal({   
                            title: "Listo",   
                            text: "Tu pago ha sido realizado, "+strTitulo+" ahora se encuentra activo.",
                            type: "success",   
                            showConfirmButton: true 
                        },function(isConfirm){   
                            
                            fntCargarPaginaActiva();
                        
                        });
                            
                    }
                            
                });
                
            }
            
            function fntDrawMisUsuarios(strPlace){
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawMisUsuarios=true&place="+strPlace, 
                    success: function(result){
                        
                        $("#divBodyModalMisUsuarios").html(result);
                        $("#mlMisUsuarios").modal("show");
                
                    
                    }
                });
                                
            }
            
            function fntShowModalContenidoSolicitud(intKey){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawShowModalContenidoSolicitud=true&usuario=<?php print $_SESSION["_open_antigua"]["core"]["id_usuario"]?>&solicitud="+intKey, 
                    success: function(result){
                        
                        $("#mlDivContenidoSolicitudCategoriaEspecial").html(result);
                        $('#mlSolicitudCategoriaEspecial').modal("show");
    
                    }
                });
                               
                
                
            }
            
            function fntDrawFormNewPlace(strKey, strPaso){
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawFormNewPlace=true&key="+strKey+"&paso="+strPaso, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }
            
            function fntDrawFormPlacePagina(strKey){
                
                sinWindowsHeight = ( $(window).height() -  $(".bmd-layout-header").height() - 10 )+'px';
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawDetalleLugar=true&key="+strKey+"&algopx="+sinWindowsHeight, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                        fntDrawPasosRegistro(true, true, true, false);
                    
                    }
                });
                        
            }
            
            function fntDrawPasosRegistro(boolSetPaso, boolPaso1, boolPaso2, boolPaso3){
                
                boolSetPaso = !boolSetPaso ? false : boolSetPaso;
                boolPaso1 = !boolPaso1 ? false : boolPaso1;
                boolPaso2 = !boolPaso2 ? false : boolPaso2;
                boolPaso3 = !boolPaso3 ? false : boolPaso3;
                
                console.log("Draw - Funcion "+boolPaso1+", "+boolPaso2+". "+boolPaso3+" ")
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawPasosRegistro=true", 
                    success: function(result){
                        
                        <?php
                        
                        $pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
                        $boolMovil = $pos === false ? false : true;

                        $boolUserPanelLogIn = false;

                        if( !$boolMovil ){
                            
                            $arrInfoDispositivoi = fntGetInformacionClienteNavegacion();
    
                            if( $arrInfoDispositivoi["plataforma"] == "iPhone" )
                                $boolMovil = true;

                        }

                        
                        if( $boolMovil ){
                            ?>
                            
                            $("#tdCoreBarraMedio_movil").html(result);
                        
                            <?php
                        }
                        else{
                            ?>
                            
                            $("#tdCoreBarraMedio").html(result);
                        
                            <?php
                        }
                        
                        ?>
                        
                        if( boolSetPaso ){
                            
                            setPasoCreacionPlace(boolPaso1, boolPaso2, boolPaso3);
                
                                
                        }
                        
                        
                    }
                });
                
            } 
            
            function setPasoCreacionPlace(boolPaso1, boolPaso2, boolPaso3){
                
                console.log("Funcion "+boolPaso1+", "+boolPaso2+". "+boolPaso3+" ")
                
                $("#spPaso1_web").removeClass("spPasoActivo");
                $("#spPaso2_web").removeClass("spPasoActivo");
                $("#spPaso3_web").removeClass("spPasoActivo");
                
                $("#spPaso1_web").removeClass("spPasoNoActivo");
                $("#spPaso2_web").removeClass("spPasoNoActivo");
                $("#spPaso3_web").removeClass("spPasoNoActivo");
                
                if( boolPaso1 && !boolPaso2 && !boolPaso3 ){
                    
                    $("#spPaso1_web").addClass("spPasoActivo");
                    
                    
                    $.ajax({
                        url: "<?php print $strAction?>?drawBotonPaso1=true", 
                        success: function(result){
                            
                            $("#divBotoneBarra").html(result);
                        
                        }
                    });
                    
                }
                else{
                    
                    $("#spPaso1_web").addClass("spPasoNoActivo");
                    
                }
                   
                if( boolPaso1 && boolPaso2 && !boolPaso3 ){
                    
                    $("#spPaso1_web").removeClass("spPasoNoActivo");
                    $("#spPaso1_web").addClass("spPasoActivo");
                    $("#spPaso2_web").addClass("spPasoActivo");
                    $("#spPaso3_web").addClass("spPasoNoActivo");
                    
                    $.ajax({
                        url: "<?php print $strAction?>?drawBotonPaso2=true&place="+$("#hidIdPlaceGeneral").val(), 
                        success: function(result){
                            
                            $("#divBotoneBarra").html(result);
                        
                        }
                    });
                        
                }
                else{
                    
                    $("#spPaso2_web").addClass("spPasoNoActivo");
                    
                }
                   
                if( boolPaso1 && boolPaso2 && boolPaso3 ){
                    
                    $("#spPaso1_web").removeClass("spPasoNoActivo");
                    $("#spPaso2_web").removeClass("spPasoNoActivo");
                    
                    $("#spPaso1_web").addClass("spPasoActivo");
                    $("#spPaso2_web").addClass("spPasoActivo");
                    
                    $("#spPaso3_web").addClass("spPasoActivo");
                    
                    $.ajax({
                            url: "<?php print $strAction?>?drawBotonPaso3=true&place="+$("#hidIdPlaceGeneral").val(), 
                        success: function(result){
                            
                            $("#divBotoneBarra").html(result);
                        
                        }
                    });
                }
                else{
                    
                    $("#spPaso3_web").addClass("spPasoNoActivo");
                    
                }
                                                
            }  
            
            
            
        </script>        
        <?php 
        
    }
    
    public function fntDrawClas2($strAction, $strKey, $intPlace = 0){
        
        $arrInfo = $this->objModel->getClasificacionPadre($strKey);
        $arrPlaceClasificacionPadre = $this->objModel->getPlaceClasificacionPadre($intPlace);
        
        ?>
        
        <div class="row">
            <div class="col-12 mt-4">
            
                <div class="form-group">
                    <label for="slcClasificacionPadre" id="lbslcClasificacionPadre"><small><?php print lang["clasificacion_secundaria"]?></small></label>
                    <div class="input-group">
                        <select id="slcClasificacionPadre" name="slcClasificacionPadre[]" class="form-control-sm select2-multiple" multiple>
                            <?php
                        
                            while( $rTMP = each($arrInfo) ){
                                
                                ?>
                                <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                    <?php
                                    
                                    while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                        
                                        $strSelected = isset($arrPlaceClasificacionPadre[$arrTMP["key"]]) ? " selected " : "";                                        
                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre"]?>"><?php print sesion["lenguaje"] == "es" ?  $arrTMP["value"]["tag_esPadre"] : $arrTMP["value"]["tag_enPadre"]?></option>
                                
                                        <?php
                                    }
                                    
                                    ?>
                                </optgroup>
                                <?php
                                
                            }
                            
                            ?>
                        </select>
                        <button class="btn btn-raised btn-sm btn-success ml-1 hide" id="btnNextClasificacionPrincipalPadred" style="display: none" onclick="fntDrawClas3();">Next</button>
                    </div>
                </div>
                
            </div>
            
        </div>
        <script>
            
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPadre").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#btnNextClasificacionPrincipal").hide();
                
                $("#slcClasificacionPadre").change(function (){
                    
                    //$("#btnNextClasificacionPrincipalPadre").show();
                    $("#divClasificacionDetalle").html("");
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                <?php
                
                if( $intPlace ){
                    ?>
                    //fntDrawClas3();
                    <?php
                }
                
                ?>
                
            });
            
            function fntDrawClas3(){
                             
                strClass1 = "";
                $("#slcClasificacionPadre option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    
                });          
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                } 
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas3=true&place=<?php print $intPlace?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionDetalle").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            
        </script>
        
        <?php        
        
    }
    
    public function fntDrawClas3($strAction, $strKey, $intPlace = 0){
        
        $arrInfo = $this->objModel->getClasificacionPadreHijo($strKey);
        $arrPlaceClasificacionPadreHijo = $this->objModel->getPlaceClasificacionPadreHijo($intPlace);
        
        ?>
        
        <div class="row">
            <div class="col-12">
            
                <div class="form-group">
                    <label for="slcClasificacionPadreHijo"><small>Detail Classification</small></label>
                    <div class="input-group">
                        <select id="slcClasificacionPadreHijo" name="slcClasificacionPadreHijo[]" class="form-control-sm select2-multiple" multiple>
                            <?php
                        
                            while( $rTMP = each($arrInfo) ){
                                
                                ?>
                                <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                    <?php
                                    
                                    while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                        $strSelected = isset($arrPlaceClasificacionPadreHijo[$arrTMP["key"]]) ? " selected " : "";                                        
                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo"]?>"><?php print $arrTMP["value"]["tag_enHijo"]?></option>
                                
                                        <?php
                                    }
                                    
                                    ?>
                                </optgroup>
                                <?php
                                
                            }
                            
                            ?>
                        </select>
                        <button class="btn btn-raised btn-sm btn-success ml-1" id="btnNextClasificacionPrincipalPadreHijo" onclick="fntDrawClas4();">Next</button>
                    </div>
                </div>
                
            </div>
            
        </div>
        <script>
            
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPadreHijo").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#btnNextClasificacionPrincipalPadre").hide();
                
                $("#slcClasificacionPadreHijo").change(function (){
                    
                    $("#btnNextClasificacionPrincipalPadreHijo").show();
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                <?php
                
                if( $intPlace ){
                    ?>
                    //fntDrawClas4();
                    <?php
                }
                
                ?>
                
            });
            
            function fntDrawClas4(){
                             
                strClass1 = "";
                $("#slcClasificacionPadreHijo option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    
                });          
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                } 
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas4=true&place=<?php print $intPlace?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionDetalleOtros").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            
        </script>
        
        <?php        
        
    }
    
    public function fntDrawClas4($strAction, $strKey, $intPlace = 0){
        
        $arrInfo = $this->objModel->getClasificacionPadreHijoDetalle($strKey);
        $arrPlaceClasificacionPadreHijoDetalle = $this->objModel->getPlaceClasificacionPadreHijoDetalle($intPlace);
        
        
        ?>
        
        <div class="row">
            <div class="col-12">
            
                <div class="form-group">
                    <label for="slcClasificacionPadreHijo"><small>Other Classification</small></label>
                    <div class="input-group">
                        <select id="slcClasificacionPadreHijoDetalle" name="slcClasificacionPadreHijoDetalle[]" class="form-control-sm select2-multiple" multiple>
                            <?php
                        
                            while( $rTMP = each($arrInfo) ){
                                
                                ?>
                                <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                    <?php
                                    
                                    while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                        $strSelected = isset($arrPlaceClasificacionPadreHijoDetalle[$arrTMP["key"]]) ? " selected " : "";                                        
                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo_detalle"]?>"><?php print $arrTMP["value"]["tag_enDetalle"]?></option>
                                
                                        <?php
                                    }
                                    
                                    ?>
                                </optgroup>
                                <?php
                                
                            }
                            
                            ?>
                        </select>
                    </div>
                </div>
                
            </div>
            
        </div>
        <script>
            
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPadreHijoDetalle").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#btnNextClasificacionPrincipalPadreHijo").hide();
                
                
            });
            
            
        </script>
        
        <?php        
        
    }
    
    public function fntDrawFormNewPlace($strAction, $intPlace = 0, $intPaso = 1){
        
        $intUsuario = $_SESSION["_open_antigua"]["core"]["id_usuario"];
        
        $arrClasificacion = $this->objModel->getClasificacion();
        $arrInfoPlace = $this->objModel->getUsuarioPlaceaa($intUsuario, $intPlace);
        $arrInfoPlace = isset($arrInfoPlace[$intPlace]) ? $arrInfoPlace[$intPlace] : array();
        
        $arrPlaceClasificacion = $this->objModel->getPlaceClasificacion($intPlace);
        $arrPlaceClasificacionPadre = $this->objModel->getPlaceClasificacionPadre($intPlace);
        $arrRestriccionUsuario = $this->objModel->getRestriccionUsuario($_SESSION["_open_antigua"]["core"]["id_usuario"]);
        
        $arrCatalogoUbicacion = fntUbicacion();
        $arrCatalogoUbicacionLugar = fntUbicacionLugar();
        
        $arrCatalogoTipoPlace = fntCatalogoTipoPlace(); 
        
        $arrSolPlace = $this->objModel->fntGetSolPlace($intPlace); 
        
        //drawdebug($arrSolPlace);
        ?>
          
        <link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 
        <script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
       
        
        <div class="row justify-content-center">
            <div class="col-12 " style="">
                       
                <form onsubmit="return false;" id="frmPlace" method="POST">
                    <input type="hidden" name="hidIdUsuario" value="<?php print $intUsuario?>">
                    <input type="hidden" name="hidIdPlace" value="<?php print $intPlace?>">
                    
                    
                    <div class="row p-0 m-0 justify-content-center" id="">
                                                                                       
                        <div class="col-12 col-lg-6 mt-4" id="divDatos">    
                            
                    
                            <h5 class="text-center"><?php print lang["informacion_de_tu_negocio"]?></h5>
                            
                            <div class="row">
                                <div class="col-12 mt-2">
                                    <label for="txtTitulo"  id="lbtxtTitulo"><small><?php print lang["nombre_de_tu_negocio"]?></small></label>
                                    <input type="text" value="<?php print isset($arrInfoPlace["titulo"]) ? $arrInfoPlace["titulo"] : ""?>" class="form-control form-control-sm   " id="txtTitulo" name="txtTitulo" placeholder="">
                                    <input type="hidden" id="hidNegocio" name="hidNegocio" value="0">    
                          
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-4">
                                    <label for="txtDescripcion" id="lbtxtDescripcion"><small><?php print lang["que_hace_tu_negocio"]?></small></label>
                                    <textarea class="form-control form-control-sm" id="txtDescripcion" name="txtDescripcion" placeholder="Descripción" rows="2"><?php print isset($arrSolPlace["descripcion"]) ? $arrSolPlace["descripcion"] : ""?></textarea>
                            
                                </div>
                            </div>
                            
                            <div class="row">
                            
                                <div class="col-12 mt-4">
                                    <label for="slcUbicacionLugar" id="lbslcUbicacionLugar"><small><?php print lang["region"]?></small></label>
                                    <select class="custom-select custom-select-sm " name="slcUbicacionLugar" id="slcUbicacionLugar">
                                        <option value=""><?php print lang["seleccione_una_opcion"]?></option>
                                                    
                                        <?php
                                        
                                        while( $rTMP = each($arrCatalogoUbicacion) ){
                                            ?>
                                            <optgroup label="<?php print $rTMP["value"]["tag_es"]?>">
                                                <?php
                                                
                                                while( $arrTMP = each($arrCatalogoUbicacionLugar[$rTMP["key"]]["lugar"]) ){
                                                    
                                                    ?>
                                                    
                                                    <option selected="" value="<?php print $arrTMP["key"]?>"><?php print sesion["lenguaje"] == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?></option>
                                                    
                                                    <?php
                                                    
                                                }
                                                
                                                ?>
                                            </optgroup>
                                            <?php
                                        }
                                        
                                        ?>
                                            
                                    </select>         
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-4">
                                
                                    <div class="form-group">
                                        <label for="slcClasificacionPrincipal" id="lbslcClasificacionPrincipal"><small><?php print lang["clasificacion_principal"]?></small></label>
                                        <div class="input-group">
                                            <select id="slcClasificacionPrincipal" name="slcClasificacionPrincipal[]" class="form-control form-control-sm select2-multiple" multiple onchange="fntDrawClas2();">
                                                <?php
                                            
                                                while( $rTMP = each($arrClasificacion) ){
                                                    
                                                    $strSelected = isset($arrPlaceClasificacion[$rTMP["key"]]) ? " selected " : "";                                        
                                                    ?>
                                                    <option <?php print $strSelected;?> value="<?php print $rTMP["value"]["id_clasificacion"]?>"><?php print sesion["lenguaje"] == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                                                    <?php
                                                    
                                                }
                                                
                                                ?>
                                            </select>
                                            <button class="btn btn-raised btn-sm btn-success ml-1 " id="" onclick="fntDrawClas2();" style="display: none;">Next</button>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        
                            <div id="divClasificacionSecundaria"></div>
                            
                            <div class="row justify-content-center justify-content-lg-end" style="display: none;">
                                
                                <div class="col-6 col-lg-4">
                                
                                    <button class="btn btn-primary btn-raised btn-block" onclick="fntCrearPlace();"><?php print lang["siguiente"]?></button>
                                
                                </div>
                                
                            </div>
                            
                            
                        </div>
                        <div class="col-12 col-lg-6" id="divDatos">    
                            <img src="images/formulario1.png" style="width: 100%; height: auto; object-fit: contain;">                        
                        </div>
                        
                    </div>                  
                         
                </form>
                
            </div>    
        </div>
        
        <script>
        
            $( document ).ready(function() {
                
                $("#hidIdPlaceGeneral").val("<?php print fntCoreEncrypt($intPlace)?>") ;
                      
                $("#slcClasificacionPrincipal").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#slcClasificacionPrincipal").change(function (){
                    
                    $("#btnNextClasificacionPrincipal").show();
                    $("#divClasificacionSecundaria").html("");
                    $("#divClasificacionDetalle").html("");
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                var xhr;
                $('input[name="txtTitulo"]').autoComplete({
                    minChars: 2,
                    source: function(term, suggest){
                        
                        try {
                            xhr.abort();
                        }
                        catch(error) {
                        }

                        xhr = $.ajax({
                            url: "<?php print $strAction?>?getNegocioAutoComplete=true&q="+$("#txtTitulo").val(), 
                            dataType : "json",
                            success: function(result){
                                suggest(result);
                            }
                        });
                        
                    },
                    renderItem: function (item, search){
                        
                        return '<div class="autocomplete-suggestion" data-nombre="'+item["nombre"]+'" data-lat="'+item["lat"]+'" data-lng="'+item["lng"]+'"  data-identificador="'+item["identificador"]+'"  style="direction: ltr;" >'+item["nombre"]+'</div>';
                    
                    },
                    onSelect: function(e, term, item){
                        
                        $("#txtTitulo").val(item.data("nombre"));
                        $("#hidNegocio").val(item.data("identificador"));
                        $("#txtTitulo").val(item.data("nombre"));
                  
                        
                        console.log(item.data("lat"));
                        console.log(item.data("lng"));
                        //markerInicial.setPosition(new google.maps.LatLng(item.data("lat"), item.data("lng")))
                        
                        
                        return false;
                        
                    }
                    
                });
                
                
                
                fntDrawPasosRegistro(true, true, false, false);
                
                <?php
                
                if( $intPlace ){
                    ?>
                    fntDrawClas2();
                    <?php
                }
                
                ?>
                
                   
                
                
            });
            
            function fntEditPlace(){
                
                <?php
                
                if( count($arrPlaceClasificacionPadre) > 0 ){
                    
                    while( $rTMP = each($arrPlaceClasificacionPadre) ){
                        ?>
                        
                        arr = $("#slcClasificacionPadre").val();
                        
                        boolExiste = false;
                        
                        $.each(arr, function( index, value ) {
                          
                            if( value == "<?php print $rTMP["key"]?>" ){
                                boolExiste = true;    
                            }
                            
                        });
                        
                        if( !boolExiste ){
                            
                            swal({
                                title: "Are you sure?",
                                text: "The change of classification affects the products that exist",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "#FF0000",
                                confirmButtonText: "Si",
                                cancelButtonText: "No",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    
                                    fntSavePlace(true);
                                
                                } 
                            });
                            
                            return false;
                            
                        }
                        
                        <?php
                    }                
                }
                
                ?>
                
                fntSavePlace(false);
            
            }
            
            function fntSavePlace(boolChangeProduct){
                
                
                var boolError = false;
                    
                if( $("#txtTelefonoNegocio").val() == "" ){
                    
                    $("#lbtxtTelefonoNegocio").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTelefonoNegocio").removeClass("text-danger");
                    
                }
                
                if( $("#txtNombreCompletoDuenio").val() == "" ){
                    
                    $("#lbtxtNombreCompletoDuenio").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtNombreCompletoDuenio").removeClass("text-danger");
                    
                }
                
                
                if( $("#txtTelefonoDuenio").val() == "" ){
                    
                    $("#lbtxtTelefonoDuenio").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTelefonoDuenio").removeClass("text-danger");
                    
                }
                
                if( $("#txtEmailPropietario").val() == "" ){
                    
                    $("#lbtxtEmailPropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtEmailPropietario").removeClass("text-danger");
                    
                }
                
                var intSizeFile = $("#flDPI")[0].files.length;
                if( intSizeFile === 0){
                               
                    $("#lbflDPI").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbflDPI").removeClass("text-danger");
                        
                }
                
                var intSizeFile = $("#flPatente")[0].files.length;
                if( intSizeFile === 0){
                               
                    $("#lbflPatente").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbflPatente").removeClass("text-danger");
                        
                }
                
                if( $("#slcTipoNegocio").val() == 2 ){
                    
                    var intSizeFile = $("#flSA")[0].files.length;
                    if( intSizeFile === 0){
                                   
                        $("#lbflSA").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbflSA").removeClass("text-danger");
                            
                    }
                    
                }
                
                if( boolError ){
                        
                    $.snackbar({
                        content: "<?php print lang["completa_los_datos_obligatorios"]?>",
                        timeout: 1500
                    });
                    
                    return false;

                }                 
                
                var formData = new FormData(document.getElementById("frmPlace"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setSavePlace=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType : 'json',
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["action"] == "1" ){
                            
                            alert("Datos Guardados")
                            
                            //setPasoCreacionPlace(true, true, false);
                
                            //fntDrawFormPlacePagina(result["place"]);
                            
                                
                        }
                        else{
                            
                            fntCargarPaginaActiva();
                            
                        }
                        
                            
                    }
                            
                });
                
            }
            
            function fntDrawClas2(){
                             
                intCountSelec = 0;
                strClass1 = "";
                $("#slcClasificacionPrincipal option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    intCountSelec++;    
                });          
                
                if( intCountSelec > "<?php print $intPlace ? count($arrClasificacion) : $arrRestriccionUsuario["max_place"]?>" ){
                    
                    $.snackbar({
                        content: "Select Only 1 option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                }
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                }    
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas2=true&place=<?php print $intPlace;?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionSecundaria").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            function fntShowDatos(strDivActual, strDivSiguiente, intPantallaValidacion){
                      
                intPantallaValidacion = !intPantallaValidacion ? 0 : intPantallaValidacion;
                
                if( intPantallaValidacion == 2 ){
                    
                    var boolError = false;
                    
                    if( $("#txtTitulo").val() == "" ){
                        
                        $("#lbtxtTitulo").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbtxtTitulo").removeClass("text-danger");
                        
                    }
                    
                    if( $("#txtDescripcion").val() == "" ){
                        
                        $("#lbtxtDescripcion").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbtxtDescripcion").removeClass("text-danger");
                        
                    }
                    
                    
                    if( $("#slcUbicacionLugar").val() == "" ){
                        
                        $("#lbslcUbicacionLugar").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbslcUbicacionLugar").removeClass("text-danger");
                        
                    }
                    
                    
                    intCountSelec = 0;
                    $("#slcClasificacionPrincipal option:selected").each(function(){
                        
                        intCountSelec++;    
                    });          
                    
                    if( intCountSelec == 0 ){
                        
                        $("#lbslcClasificacionPrincipal").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbslcClasificacionPrincipal").removeClass("text-danger");
                        
                    }
                    
                    intCountSelec = 0; 
                    $("#slcClasificacionPadre option:selected").each(function(){
                        
                        intCountSelec++;    
                    });          
                    
                    if( intCountSelec == 0 ){
                        
                        $("#lbslcClasificacionPadre").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbslcClasificacionPadre").removeClass("text-danger");
                        
                    }
                    
                    if( boolError ){
                        
                        $.snackbar({
                            content: "<?php print lang["completa_los_datos_obligatorios"]?>",
                            timeout: 1500
                        });
                        
                        return false;

                    } 
                        
                    
                    
                }
                
                
                
                $("#"+strDivSiguiente).show();
                $("#"+strDivActual).hide();
            }
            
            function fntTipoPlace(){
                
                if( $("#slcTipoNegocio").val() == 2 ){
                    
                    $("#divFileSA").show();
                    
                }   
                else{
                    
                    $("#divFileSA").hide();
                    
                } 
                
            }
            
            function fntCrearPlace(){
                
                var boolError = false;
                
                if( $("#txtTitulo").val() == "" ){
                    
                    $("#lbtxtTitulo").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTitulo").removeClass("text-danger");
                    
                }
                
                if( $("#txtDescripcion").val() == "" ){
                    
                    $("#lbtxtDescripcion").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtDescripcion").removeClass("text-danger");
                    
                }
                
                if( $("#slcUbicacionLugar").val() == "" ){
                    
                    $("#lbslcUbicacionLugar").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbslcUbicacionLugar").removeClass("text-danger");
                    
                }
                
                intCountSelec = 0;
                $("#slcClasificacionPrincipal option:selected").each(function(){
                    
                    intCountSelec++;    
                });          
                
                if( intCountSelec == 0 ){
                    
                    $("#lbslcClasificacionPrincipal").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbslcClasificacionPrincipal").removeClass("text-danger");
                    
                }
                
                intCountSelec = 0; 
                $("#slcClasificacionPadre option:selected").each(function(){
                    
                    intCountSelec++;    
                });          
                
                if( intCountSelec == 0 ){
                    
                    $("#lbslcClasificacionPadre").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbslcClasificacionPadre").removeClass("text-danger");
                    
                }
                
                if( boolError ){
                    
                    $.snackbar({
                        content: "<?php print lang["completa_los_datos_obligatorios"]?>",
                        timeout: 1500
                    });
                    
                    return false;

                }
                else{
                    
                    var formData = new FormData(document.getElementById("frmPlace"));
                        
                    $(".preloader").fadeIn();
                    $.ajax({
                        url: "<?php print $strAction?>?setCrearPlace=true", 
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType : 'json',
                        success: function(result){
                            
                            $(".preloader").fadeOut();
                            fntDrawFormPlacePagina(result["place"]);
                            
                                
                        }
                                
                    });
                    
                    
                }
                
            }
            
        </script>
        <?php
                
    }
    
    public function getDrawMisUsuarios($strAction, $intPlace){
    
        $arrVisitaUsuario = $this->objModel->getPlaceVisitaUsuario($intPlace);
        $arrDatosPlace = fntGetPlace($intPlace, true, $this->objModel->getobjDBClass());
        
        $boolShowDatos = $arrDatosPlace["id_place_combo"] == 2 ? true : false;
     
        if( !$boolShowDatos ){
            
            ?>
            <div class="row">
                <div class="col-12 text-center">
                    <div class="alert alert-warning text-center"><?php print lang["alerta_pedirContacto_misUsuario"]?> ( <?php print count($arrVisitaUsuario)?> )</div>
                </div>    
            </div>
            
            <?php    
        }
        ?>
        <div class="row">
            <div class="col-12">
                <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
                <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>

                <table class="table table-secondary table-hover" id="tblUsuario">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Email</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Ultima Fecha Visita</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        while( $rTMP = each($arrVisitaUsuario) ){
                            ?>
                            <tr>
                                <td><?php print $boolShowDatos ? $rTMP["value"]["nombre"] : substr($rTMP["value"]["nombre"], 0, 4)."*******"?></td>
                                <td><?php print $boolShowDatos ? $rTMP["value"]["email"] : substr($rTMP["value"]["email"], 0, 2)."*******"?></td>
                                <td><?php print $boolShowDatos ? $rTMP["value"]["telefono"] : substr($rTMP["value"]["telefono"], 0, 2)."*******"?></td>
                                <td><?php print $rTMP["value"]["add_fecha"]?></td>
                            </tr>
                            <?php                                        
                        }
                        
                        ?>
                    </tbody>
                </table>
                <script>
                    
                    $('#tblUsuario').DataTable({
                        "ordering": false,
                        "info":     false
                    });
                    
                </script>
            </div>    
        </div>
        <?php
                    
    }
   
    public function drawShowModalContenidoSolicitud($strAction, $intSolicitudCategoriaEspecial){
        
        $arrPlace = $this->objModel->getPlaceUsuario($_SESSION["_open_antigua"]["core"]["id_usuario"]);
        
        $arrCategoriaEspecial = fntGetCatalogoCategoriaEspecial(true, $this->objModel->objDBClass);
        $arrCategoriaEspecialFrecuenciaPago = fntGetCatalogoCategoriaEspecialFrecuenciaPago(true, $this->objModel->objDBClass);
        
        $arrSolicitudCategoriaEspecial = $this->objModel->getSolicitudCategoriaEspecial($arrPlace, $intSolicitudCategoriaEspecial);
        $arrSolicitudCategoriaEspecial = isset($arrSolicitudCategoriaEspecial[$intSolicitudCategoriaEspecial]) ? $arrSolicitudCategoriaEspecial[$intSolicitudCategoriaEspecial] : array();
        
        ?>
        
        <form onsubmit="return false;" id="frmPlace" method="POST">
        <input type="hidden" name="hidSolicitud" id="hidSolicitud" value="<?php print isset($arrSolicitudCategoriaEspecial["id_sol_categoria_especial"]) ? fntCoreEncrypt($arrSolicitudCategoriaEspecial["id_sol_categoria_especial"]) : "0"?>">
        <div class="row">
            <div class="col-12 col-lg-6 ">
                
                <select id="slcModalPlace" name="slcModalPlace" class="custom-select custom-select-sm" onchange="fntGetProductoPlace();">
                    
                    <option><?php print lang["selecciona_tu_negocio"]?></option>
                    <?php
                    
                    $intCount = 1;
                    while( $rTMP = each($arrPlace) ){
                        
                        if( isset($arrSolicitudCategoriaEspecial["id_place"]) ){
                            
                            $strSelected =  $arrSolicitudCategoriaEspecial["id_place"] ==  $rTMP["key"] ? "selected" : "";
                            
                        }
                        else{
                            
                            $strSelected =  $intCount == 1 ? "selected" : "";
                            
                        }
                        
                        ?>
                        
                        <option <?php print $strSelected?> value="<?php print fntCoreEncrypt($rTMP["key"])?>"><?php print $rTMP["value"]["titulo"]?></option>
                    
                        <?php
                        $intCount++;
                    }
                    
                    ?>
                    
                </select>                    
                
            </div>
            <div class="col-12 col-lg-6" id="divColProductoPlace">
                
                
            </div>
        </div>
        <div class="row">
            
            <div class="col-12 col-lg-6 mt-1">
                
                <select id="slcModalCategoriaEspecial" name="slcModalCategoriaEspecial" class="custom-select custom-select-sm" >
                    
                    <option><?php print lang["selecciona_una_clasificacion"]?></option>
                    <?php
                    
                    while( $rTMP = each($arrCategoriaEspecial) ){
                        
                        $strSelected = "";
                        if( isset($arrSolicitudCategoriaEspecial["id_categoria_especial"]) ){
                            
                            $strSelected =  $arrSolicitudCategoriaEspecial["id_categoria_especial"] ==  $rTMP["key"] ? "selected" : "";
                            
                        }
                        
                        ?>
                        
                        <option <?php print $strSelected;?> value="<?php print fntCoreEncrypt($rTMP["key"])?>"><?php print sesion["lenguaje"] == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                    
                        <?php
                    }
                    
                    ?>
                    
                </select>                    
                
            </div>
            
            <div class="col-12 col-lg-6 mt-1">
                
                <select id="slcModalFrecuencia" name="slcModalFrecuencia" class="custom-select custom-select-sm" >
                    
                    <option><?php print lang["selecciona_tu_frecuencia"]?></option>
                    <?php
                    
                    while( $rTMP = each($arrCategoriaEspecialFrecuenciaPago) ){
                        
                        $strSelected = "";
                        if( isset($arrSolicitudCategoriaEspecial["id_frecuencia"]) ){
                            
                            $strSelected =  $arrSolicitudCategoriaEspecial["id_frecuencia"] ==  $rTMP["key"] ? "selected" : "";
                            
                        }
                        
                        ?>
                        
                        <option <?php print $strSelected?> value="<?php print fntCoreEncrypt($rTMP["key"])?>"><?php print ( sesion["lenguaje"] == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"] )." (Q".number_format($rTMP["value"]["precio"], 2).") "?></option>
                    
                        <?php
                    }
                    
                    ?>
                    
                </select>                    
                
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-12 col-lg-6 mt-1">
                <label><small><?php print lang["fecha_inicio"]?></small></label>
                <input value="<?php print isset($arrSolicitudCategoriaEspecial["fecha_inicio"]) ? $arrSolicitudCategoriaEspecial["fecha_inicio"] : ""?>" type="text" id="txtFechaInicio" name="txtFechaInicio" class="form-control form-control-sm text-center" placeholder="">
            </div>
            
            <div class="col-12 col-lg-6 mt-1">
                <label><small><?php print lang["fecha_fin"]?></small></label>
                <input value="<?php print isset($arrSolicitudCategoriaEspecial["fecha_fin"]) ? $arrSolicitudCategoriaEspecial["fecha_fin"] : ""?>" type="text" id="txtFechaFin" name="txtFechaFin" class="form-control form-control-sm text-center" placeholder="">
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-12 col-lg-12 mt-1">
                <label><small><?php print lang["motivo"]?></small></label>
                
                <textarea class="form-control" id="txtMotalMotivo" name="txtMotalMotivo" rows="2"><?php print isset($arrSolicitudCategoriaEspecial["descripcion"]) ? $arrSolicitudCategoriaEspecial["descripcion"] : ""?></textarea>

            </div>
            
            
        </div>
        
        <?php
        
        if( isset($arrSolicitudCategoriaEspecial["estado"]) && $arrSolicitudCategoriaEspecial["estado"] == "R" ){
            
            ?>
            <div class="row">
                
                <div class="col-12 col-lg-12 mt-1">
                    <label><small><?php print lang["motivo_rechazo"]?></small></label>
                    
                    <br>
                    <?php print isset($arrSolicitudCategoriaEspecial["motivo_rechazo"]) ? $arrSolicitudCategoriaEspecial["motivo_rechazo"] : ""?>

                </div>
                
                
            </div>
            <?php
            
        }
        
        
        ?>
        <div class="row">
            
            <div class="col-12 text-right mt-4">
                
                <button type="button" class="btn btn-secondary btn-raised" data-dismiss="modal">Close</button>
                
                <?php
                
                if( isset($arrSolicitudCategoriaEspecial["estado"]) && $arrSolicitudCategoriaEspecial["estado"] == "R" ){
                    
                    ?>
                    
                    <button type="button" class="btn btn-warning btn-raised" onclick="fntSetSolicitudCategoriaEspecial()"><?php print lang["enviar_cambios_por_rechazo"]?></button>
            
                    <?php
                                        
                }
                else{
                    
                    ?>
                    
                    <button type="button" class="btn btn-primary btn-raised" onclick="fntSetSolicitudCategoriaEspecial();"><?php print lang["enviar_solicitud"]?></button>
            
                    <?php
                    
                }
                
                ?>
                
                
                
            </div>
        </div>
        
        
        
        
        </form>
        <script>        
            
            $(document).ready(function() { 
                
                $('#txtFechaInicio').bootstrapMaterialDatePicker({
                    time: false,
                    clearButton: true,
                    switchOnClick : true
                });       
                      
                $('#txtFechaFin').bootstrapMaterialDatePicker({
                    time: false,
                    clearButton: true,
                    switchOnClick : true
                });       
                       
                          
                fntGetProductoPlace();

            });
            
            
            function fntGetProductoPlace(){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawGetProductoPlace=true&placeProducto=<?php print  ( isset($arrSolicitudCategoriaEspecial["id_place_producto"]) ? fntCoreEncrypt($arrSolicitudCategoriaEspecial["id_place_producto"]) : 0  )?>&place="+$("#slcModalPlace").val(), 
                    success: function(result){
                        
                        $("#divColProductoPlace").html(result);
    
                    }
                });    
                
            }
            
            function fntSetSolicitudCategoriaEspecial(){
                
                var formData = new FormData(document.getElementById("frmPlace"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setSolicitudCategoriaEspecial=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $('#mlSolicitudCategoriaEspecial').modal("hide");
                            
                        swal({   
                            title: "<?php print lang["listo"]?>",   
                            text: "<?php print lang["aviso_envio_sol_place"]?>",
                            type: "success",   
                            showConfirmButton: true 
                        },function(isConfirm){   
                            
                            fntCargarPaginaActiva();
                        
                        });
                        
                            
                    }
                            
                });
                
            }
            
        </script>        
        <?php
                
    }
    
    public function drawGetProductoPlace($strAction, $intPlace, $intPlaceProducto){
        
        $arrProductoPlace = $this->objModel->getPlaceProducto($intPlace);
        ?>
        <select id="slcModalPlaceProducto" name="slcModalPlaceProducto" class="custom-select custom-select-sm" >
                    
            <option>Selecciona tu Producto...</option>
            <?php
            
            while( $rTMP = each($arrProductoPlace) ){
                
                $strSelected = $intPlaceProducto == $rTMP["key"] ? "selected" : ""; 
                
                ?>
                        
                <option  <?php print $strSelected;?> value="<?php print fntCoreEncrypt($rTMP["key"])?>"><?php print $rTMP["value"]["nombre"]." ( Q".number_format($rTMP["value"]["precio"], 2)." ) "?></option>
            
                <?php
            }
            
            ?>
            
        </select>
        <?php
        
                
    }
    
    public function drawPlaceDetalle($strAction, $strKeyPlace, $strAltoPx){
        
        $pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");

        $boolMovil = $pos === false ? false : true;

        if( !$boolMovil ){
            
            $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

            $boolMovil = $pos === false ? false : true;

        }
        ?>
        
        <div class="row">
            <div class="col-12 p-0 mt-0">
                <iframe src="placeup.php?p=<?php print $strKeyPlace?>" style="width: 100%; height: <?php print $strAltoPx?>; border: 0px;"></iframe>
                
                <div style="display: none;  position: absolute; bottom: <?php print $boolMovil ? "20%" : "5%"?>; right: <?php print $boolMovil ? "30%" : "40%"?>; width: <?php print $boolMovil ? "30%" : "20%"?>; z-index: 999;">
                    
                    <button class="btn btn-primary btn-raised btn-block" onclick="fntShowVerificacion();"><?php print lang["siguiente"]?></button>
                    
                </div>
                
            </div>
        </div>
        <script>
            
            function fntShowVerificacion(){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawShowVerificacion=true&key=<?php print $strKeyPlace?>", 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                        fntDrawPasosRegistro(true, true, true, true); 
                
                    }
                });
                
                
            }
            
        </script>
        <?php
    }
    
    public function getDrawPasosRegistro(){
        
        ?>
        <style>
            
            .spPasoActivo{
                border-radius: 5px; 
                font-size: 9px; 
                width: 15px; 
                border: 1px solid white;
                background-color: #3E53AC !important;
                color: white;
            }
            
            .spPasoNoActivo{
                border-radius: 5px; 
                font-size: 9px; 
                width: 15px; 
                color: black;
            }
            
        </style>
        <div class="row p-0 m-0 justify-content-center">
            
            <div class="col-4 col-lg-2 p-0 m-0 text-center ">
                
                <table style="width: 100%;" class="">
                    <tr>
                        <td class="p-0 m-0" style="text-align: center;">
                           <span id="spPaso1_web" class="bg-white pl-4 pr-4 text-center spPasoNoActivo" style="">
                                 1
                            </span>
                        </td>
                    </tr> 
                    <tr>
                        <td class="p-0 m-0">
                            <h1 class="text-white text-center" style="font-size: 10px;"><?php print lang["informacion_general"]?></h1>
                        </td>
                    </tr>
                </table>
                
            </div>
            <div class="col-4 col-lg-2  p-0 m-0 text-center ">
                
                <table style="width: 100%;" class="">
                    <tr>
                        <td class="p-0 m-0" style="text-align: center;">
                            <span id="spPaso2_web" class="bg-white pl-4 pr-4 text-center spPasoNoActivo" style="">
                                2
                            </span>
                        </td>
                    </tr> 
                    <tr>
                        <td class="p-0 m-0">
                            <h1 class="text-white text-center" style="font-size: 10px;"><?php print lang["edicion_de_pagina"]?></h1>
                
                        </td>
                    </tr>
                </table>
                        
                
                
            </div>
            <div class="col-4 col-lg-2  p-0 m-0 text-center ">
                
                <table style="width: 100%;" class="">
                    <tr>
                        <td class="p-0 m-0" style="text-align: center;">
                            <span id="spPaso3_web" class="bg-white pl-4 pr-4 text-center spPasoNoActivo" style="">
                                3
                            </span>
                        </td>
                    </tr> 
                    <tr>
                        <td class="p-0 m-0">
                            <h1 class="text-white text-center" style="font-size: 10px;"><?php print lang["verificacion_de_negocio"]?></h1>
                
                        </td>
                    </tr>
                </table>
                        
            </div>
            <div class="col-12 col-lg-2  pt-1 m-0 text-center " id="divBotoneBarra">
                
                        
            </div>
            
            
        </div>                         
        <?php
        
    }
    
    public function drawShowVerificacion($strAction, $intPlace){
        
        $arrPlace = $this->objModel->getUsuarioPlaceaa($_SESSION["_open_antigua"]["core"]["id_usuario"], $intPlace);
        $arrPlace = isset($arrPlace[$intPlace]) ? $arrPlace[$intPlace] : array();
        
        $arrSolPlace = $this->objModel->fntGetSolPlace($intPlace);   
        $arrCatalogoTipoPlace = fntCatalogoTipoPlace();
        $strCodigoVentas = $this->objModel->fntGetCodigoVendedor( isset($arrSolPlace["id_usuario_vendedor"]) ? intval($arrSolPlace["id_usuario_vendedor"]) : 0 );
        
        ?>
        
        <div class="row justify-content-center">
            <div class="col-12 " style="">
                       
                <form onsubmit="return false;" id="frmDatosSolicitudEnvio" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                    <input type="hidden" id="hidSolPlace" name="hidSolPlace" value="<?php print fntCoreEncrypt($arrSolPlace["id_sol_place"])?>">
                    <input type="hidden" id="hidNombreNegocio" name="hidNombreNegocio" value="<?php print $arrPlace["titulo"]?>">
                    
                    
                    <div class="row p-0 m-0 justify-content-center" id="">
                                                                                       
                        <div class="col-12 col-lg-6 mt-4" id="divDatos">    
                            
                            <h5 class="text-center"><?php print lang["verificacion_de_negocio"]?></h5>
                            
                            <?php
                            
                            if( isset($arrSolPlace["estado"]) && $arrSolPlace["estado"] == "R" ){
                                
                                ?>
                                <div class="row">
                                    <div class="col-12 mt-1 text-danger text-center">
                                        
                                        
                                        <?php
                                        
                                        print lang["motivo_rechazo"];
                                        
                                        print isset($arrSolPlace["motivo"]) ? $arrSolPlace["motivo"] : "";
                                        
                                        ?>                                    
                                                                        
                                    </div>
                                </div>
                                <?php
                                
                            }
                            
                            ?>
                            
                            <div class="row">
                                <div class="col-12 mt-2">
                                    
                                    <label id="lbtxtCodigoVendedor"><small><?php print lang["codigo_vendedor"]?></small></label>
                                    <input type="text" value="<?php print $strCodigoVentas?>" class="form-control form-control-sm   " id="txtCodigoVendedor" name="txtCodigoVendedor" placeholder="">
                                    
                                    <label id="lbspEstadoCodigo" style="display: none;">
                                        <span id="spEstadoCodigo" class="" style="font-size: 12px;"></span>
                                    </label>
                                        
                          
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-2">
                                    
                                    <label id="lbtxtNombrePropietario"><small><?php print lang["nombre_completo_propietario"]?></small></label>
                                    <input type="text" value="<?php print isset($arrSolPlace["nombre"]) ? $arrSolPlace["nombre"] : ""?>" class="form-control form-control-sm   " id="txtNombrePropietario" name="txtNombrePropietario" placeholder="">
                          
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-2">
                                    
                                    <label id="lbtxtTelefonoPropietario"><small><?php print lang["telefono_propietario"]?></small></label>
                                    <input type="text" value="<?php print isset($arrSolPlace["telefono_propietario"]) ? $arrSolPlace["telefono_propietario"] : ""?>" class="form-control form-control-sm   " id="txtTelefonoPropietario" name="txtTelefonoPropietario" placeholder="">
                          
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-2">
                                    
                                    <label id="lbtxtEmailPropietario"><small><?php print lang["email_propietario"]?></small></label>
                                    <input type="text" value="<?php print isset($arrSolPlace["email_propietario"]) ? $arrSolPlace["email_propietario"] : ""?>" class="form-control form-control-sm   " id="txtEmailPropietario" name="txtEmailPropietario" placeholder="">
                          
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 mt-2">
                                    
                                    <label id="lbslcTipoNegocio"><small><?php print lang["tipo_de_negocio"]?></small></label>
                                    <select class="custom-select custom-select-sm " name="slcTipoNegocio" id="slcTipoNegocio">
                                        
                                        <?php
                                        
                                        while( $rTMP = each($arrCatalogoTipoPlace) ){
                                            
                                            $strSelected = $rTMP["key"] == (  isset($arrSolPlace["tipo_negocio"]) ? $arrSolPlace["tipo_negocio"] : "" ) ? "selected" : "";
                                            
                                            ?>
                                            
                                            <option <?php print $strSelected;?> value="<?php print $rTMP["key"]?>"><?php print  sesion["lenguaje"] == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"] ?></option>
                                            
                                            <?php
                                        }
                                        
                                        ?>
                                        
                                    </select>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-12 col-lg-6 mt-2">
                                    
                                    <label id="lbflDPI"><small>DPI</small></label>
                                    <input type="file" id="flDPI" name="flDPI" class="form-control-file" style="cursor: pointer;">
                                </div>
                                <div class="col-12 col-lg-6 mt-2">
                                    
                                    <label id="lbflPatente"><small><?php print lang["foto_patente"]?></small></label>
                                    <input type="file" id="flPatente" name="flPatente" class="form-control-file" style="cursor: pointer;">
                                </div>
                            </div>
                            
                            <div class="row justify-content-center justify-content-lg-end" style="display: none;">
                                
                                <div class="col-6 col-lg-4 mt-3 mt-0">
                                
                                    <button class="btn btn-primary btn-raised btn-block" onclick="fntSetVerificacion();"><?php print lang["enviar_solicitud"]?></button>
                                
                                </div>
                                
                            </div>
                            
                            
                        </div>
                        <div class="col-12 col-lg-6" id="divDatos">    
                            <img src="images/formulario1.png" style="width: 100%; height: auto; object-fit: contain;">                        
                        </div>
                        
                    </div>                  
                         
                </form>
                
            </div>    
        </div>
        <script>
            
            
            $(document).ready(function() { 
                
                                
                            
            });
            
            function fntSetVerificacion(){
                
                
                var boolError = false;
                    
                if( $("#txtNombrePropietario").val() == "" ){
                    
                    $("#lbtxtNombrePropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtNombrePropietario").removeClass("text-danger");
                    
                }
                
                
                if( $("#txtTelefonoPropietario").val() == "" ){
                    
                    $("#lbtxtTelefonoPropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTelefonoPropietario").removeClass("text-danger");
                    
                }
                
                if( $("#txtEmailPropietario").val() == "" ){
                    
                    $("#lbtxtEmailPropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtEmailPropietario").removeClass("text-danger");
                    
                }
                
                if( false && $("#txtCodigoVendedor").val() == "" ){
                    
                    if( $("#slcTipoNegocio").val() == 1 ){
                        
                        var intSizeFile = $("#flDPI")[0].files.length;
                        if( intSizeFile === 0){
                                       
                            $("#lbflDPI").addClass("text-danger");
                                
                            boolError = true;
                        }
                        else{
                            
                            $("#lbflDPI").removeClass("text-danger");
                                
                        }
                            
                    }
                    
                    if( $("#slcTipoNegocio").val() == 2 ){
                        
                        var intSizeFile = $("#flPatente")[0].files.length;
                        if( intSizeFile === 0){
                                       
                            $("#lbflPatente").addClass("text-danger");
                                
                            boolError = true;
                        }
                        else{
                            
                            $("#lbflPatente").removeClass("text-danger");
                                
                        }
                    }
                    
                    
                }
                
                    
                if( boolError ){
                        
                    swal("<?php print lang["advertencia"]?>", "<?php print lang["completa_los_datos_obligatorios"]?>", "");
                    
                    return false;

                }
                
                var formData = new FormData(document.getElementById("frmDatosSolicitudEnvio"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setEnvioSolicitud=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType : "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        swal({   
                            title: "",   
                            text: result["msn"],
                            type: "success",   
                            showConfirmButton: true 
                        },function(isConfirm){   
                            
                            fntCargarPaginaActiva();
                        
                        });
                                        
                
                    }
                            
                });
                
            }
            
            
            
        </script>
        <?php
        
    }
    
    public function drawBotonPaso1(){
        
        ?>
        <table style="width: 100%;" class="">
            <tr>
                <td class="p-1" style="text-align: center; width: 50%;">
                    
                    <button onclick="fntCargarPaginaActiva();" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px; color: #3E53AC; background: white;"><?php print lang["regresar"]?></button>
                        
                </td>
                <td class="p-1" style="text-align: center; width: 50%;">
                    
                    <button onclick="fntCrearPlace();" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px; color: #3E53AC; background: white;"><?php print lang["siguiente"]?></button>
                        
                </td>
            </tr> 
        </table>
        
        <?php
        
    }
                        
    public function drawBotonPaso2($strPlace){
        
        
        ?>
        <table style="width: 100%;" class="">
            <tr>
                <td class="p-1" style="text-align: center; width: 50%;">
                    
                    <?php
                    
                    if( !empty($strPlace) ){
                        ?>
                        <button onclick="fntDrawFormNewPlace('<?php print $strPlace?>', '1');" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px; color: #3E53AC; background: white;"><?php print lang["regresar"]?></button>
                    
                        <?php
                    }
                    else{
                        ?>
                        <button onclick="fntCargarPaginaActiva();" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px; color: #3E53AC; background: white;"><?php print lang["regresar"]?></button>
                    
                        <?php
                    }
                    ?>
                    
                            
                </td>
                <td class="p-1" style="text-align: center; width: 50%;">
                    
                    <button onclick="fntShowVerificacion();" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px; color: #3E53AC; background: white;"><?php print lang["siguiente"]?></button>
                        
                </td>
            </tr> 
        </table>
        
        <?php
        
    }
                   
    public function drawBotonPaso3($strPlace){
        
        
        ?>
        <table style="width: 100%;" class="">
            <tr>
                <td class="p-1" style="text-align: center; width: 50%;">
                    
                    <button onclick="fntDrawFormPlacePagina('<?php print $strPlace?>');" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 90px; border-radius: 5px; color: #3E53AC; background: white;"><?php print lang["regresar"]?></button>
                            
                </td>
                <td class="p-1" style="text-align: center; width: 50%;">
                                                                                    
                    <button onclick="fntSetVerificacion();" class="btn btn-primary btn-raised btn-sm " style="font-size: 11px; width: 120px; border-radius: 5px; color: #3E53AC; background: white;"><?php print lang["enviar_solicitud"]?></button>
                        
                </td>
            </tr> 
        </table>
        
        <?php
        
    }
                                        
}

?>
