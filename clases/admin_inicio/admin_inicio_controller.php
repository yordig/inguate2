<?php
  
require_once "admin_inicio_model.php";
require_once "admin_inicio_view.php";

class admin_inicio_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new admin_inicio_model();
        $this->objView = new admin_inicio_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
                     
    }
    
}

  
?>
