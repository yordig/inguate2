<?php

require_once "admin_inicio_model.php";

class admin_inicio_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new admin_inicio_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        ?>
        
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content " style="background-color: #F0F0F0;" >
                <div class="container-fluid  " >
                    
                    <button type="button" class="btn btn-raised btn-dark" data-toggle="modal" data-target="#exampleModal">
                          Launch demo modal
                        </button>
                        <button type="button" class="btn btn-secondary" data-timeout="1000" data-toggle="snackbar" data-content="Free fried chicken here! <a href='https://example.org' class='btn btn-info'>Check it out</a>" data-html-allowed="true" data-timeout="0">
                            Snackbar
                        </button>
                
                  
                  
                  
                </div> 
            </main>
          
            <!-- Espacio para Modal  -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>    
        <script>        
        </script>        
        <?php
        
    }
                                        
}

?>
