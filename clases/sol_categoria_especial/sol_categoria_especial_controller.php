<?php
  
require_once "sol_categoria_especial_model.php";
require_once "sol_categoria_especial_view.php";

class sol_categoria_especial_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new sol_categoria_especial_model();
        $this->objView = new sol_categoria_especial_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["setAprobarSolProduto"]) ){
            
            $intSolProducto = isset($_POST["intSol"]) ? intval($_POST["intSol"]) : 0;
            
            $this->objModel->setAprobarSolProduto($intSolProducto);
                        
            die();
        }
        
        if( isset($_GET["setRechazarSolProduto"]) ){
            
            $intSolProducto = isset($_POST["intSol"]) ? intval($_POST["intSol"]) : 0;
            $strMotivo = isset($_POST["txtMotivoRechazo"]) ? addslashes($_POST["txtMotivoRechazo"]) : 0;
            
            $this->objModel->setRechazarSolProduto($intSolProducto, $strMotivo);                        
            die();
        }
        
                     
    }
    
}

  
?>
