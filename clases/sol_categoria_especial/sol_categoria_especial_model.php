<?php
 
class sol_categoria_especial_model{
      
    var $objDBClass;
    
    public function __construct(){
        
        $this->objDBClass = new dbClass(); 
            
    }
    
    public function getobjDBClass(){
        return $this->objDBClass;
    }
    
    public function setCloseDB(){
        
        $this->objDBClass->db_close();
    
    }
    
    public function getSolProducoEspecial(){
        
        $arrCategoriaEspecial = fntGetCatalogoCategoriaEspecial(true, $this->getobjDBClass());
        $arrCategoriaEspecialFrecuenciaPago = fntGetCatalogoCategoriaEspecialFrecuenciaPago();
        $arrEstadoSolCategoriaEspecial = fntGetCatalogoCategoriaEspecialSolicitudEstado();
        
        $strQuery = "SELECT sol_categoria_especial.id_sol_categoria_especial,
                            sol_categoria_especial.id_categoria_especial,
                            sol_categoria_especial.id_frecuencia,
                            sol_categoria_especial.fecha_inicio,
                            sol_categoria_especial.fecha_fin,
                            sol_categoria_especial.estado,
                            sol_categoria_especial.descripcion,
                            place_producto.id_place_producto,
                            place_producto.nombre,
                            place.id_place,
                            place.titulo
                     FROM   sol_categoria_especial,
                            place_producto,
                            place   
                     WHERE  sol_categoria_especial.estado IN('S')
                     AND    sol_categoria_especial.id_place_producto = place_producto.id_place_producto
                     AND    place_producto.id_place = place.id_place
                     
                     ";
        $arr = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arr[$rTMP["id_sol_categoria_especial"]]["id_sol_categoria_especial"] = $rTMP["id_sol_categoria_especial"];
            $arr[$rTMP["id_sol_categoria_especial"]]["id_categoria_especial"] = $rTMP["id_categoria_especial"];
            $arr[$rTMP["id_sol_categoria_especial"]]["categoria_especial_nombre"] =  $arrCategoriaEspecial[$rTMP["id_categoria_especial"]]["nombre"];
            $arr[$rTMP["id_sol_categoria_especial"]]["id_frecuencia"] = $rTMP["id_frecuencia"];
            $arr[$rTMP["id_sol_categoria_especial"]]["frecuencia_nombre"] = $arrCategoriaEspecialFrecuenciaPago[$rTMP["id_frecuencia"]]["nombre"];
            $arr[$rTMP["id_sol_categoria_especial"]]["fecha_inicio"] = $rTMP["fecha_inicio"];
            $arr[$rTMP["id_sol_categoria_especial"]]["fecha_fin"] = $rTMP["fecha_fin"];
            $arr[$rTMP["id_sol_categoria_especial"]]["estado"] = $rTMP["estado"];
            $arr[$rTMP["id_sol_categoria_especial"]]["estado_text"] = $arrEstadoSolCategoriaEspecial[$rTMP["estado"]]["nombre"];
            $arr[$rTMP["id_sol_categoria_especial"]]["descripcion"] = $rTMP["descripcion"];
            $arr[$rTMP["id_sol_categoria_especial"]]["id_place_producto"] = $rTMP["id_place_producto"];
            $arr[$rTMP["id_sol_categoria_especial"]]["nombre"] = $rTMP["nombre"];
            $arr[$rTMP["id_sol_categoria_especial"]]["id_place"] = $rTMP["id_place"];
            $arr[$rTMP["id_sol_categoria_especial"]]["titulo"] = $rTMP["titulo"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arr;
        
        
        
    }
    
    public function setAprobarSolProduto($intSolProducto){
        
        $strQuery = "UPDATE sol_categoria_especial
                     SET    estado = 'A'
                     WHERE  id_sol_categoria_especial = {$intSolProducto} ";        
        
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setEliminarSolProduto($intSolProducto){
        
        $strQuery = "DELETE FROM  sol_categoria_especial 
                     WHERE  id_sol_categoria_especial = {$intSolProducto} ";        
        
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setRechazarSolProduto($intSolProducto, $strMotivo){
        
        $strQuery = "UPDATE sol_categoria_especial
                     SET    estado = 'R'
                     WHERE  id_sol_categoria_especial = {$intSolProducto} ";
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "SELECT place_producto.nombre,
                            usuario.id_usuario,
                            usuario.email
                     FROM   sol_categoria_especial,
                            place_producto,
                            place,
                            usuario
                     WHERE  sol_categoria_especial.id_sol_categoria_especial = {$intSolProducto}
                     AND    place_producto.id_place_producto = sol_categoria_especial.id_place_producto  
                     AND    place_producto.id_place = place.id_place
                     AND    place.id_usuario = usuario.id_usuario
                     
                        ";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        $rTMP = $this->objDBClass->db_fetch_array($qTMP);
        $this->objDBClass->db_free_result($qTMP);
        
        $strTituloCorreo = "Solicitud para {$rTMP["nombre"]} ha sido rechazada ";
        $strInsertMotivo = "Motivo: {$strMotivo}  ";
        
        $strQuery = "INSERT INTO usuario_alerta (id_usuario, tipo, identificador, titulo, mensaje)
                                            values({$rTMP["id_usuario"]}, 'SPE', {$intSolProducto}, '{$strTituloCorreo}', '{$strInsertMotivo}')        ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strBody = "
                        <style type='text/css'>
                        /* latin-ext */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
                        }
                        /* latin */
                        @font-face {
                          font-family: 'Lato';
                          font-style: normal;
                          font-weight: 400;
                          unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
                        }

                          /* Take care of image borders and formatting */

                          img {
                              max-width: 600px;
                              outline: none;
                              text-decoration: none;
                              -ms-interpolation-mode: bicubic;
                          }

                        a {
                          text-decoration: none;
                          border: 0;
                          outline: none;
                        }

                          a img {
                              border: none;
                          }

                        td, h1, h2, h3  {
                          font-family: Helvetica, Arial, sans-serif;
                          font-weight: 400;
                        }

                        body {
                          -webkit-font-smoothing:antialiased;
                          -webkit-text-size-adjust:none;
                          width: 100%;
                          height: 100%;
                          color: #37302d;
                          background: #ffffff;
                        }

                        table {
                          background:
                        }

                        h1, h2, h3 {
                          padding: 0;
                          margin: 0;
                          color: #ffffff;
                          font-weight: 400;
                        }

                        h3 {
                          color: #21c5ba;
                          font-size: 24px;
                        }
                        </style>

                      <style type='text/css' media='screen'>
                        @media screen {
                          td, h1, h2, h3 {
                            font-family: 'Lato', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                          }
                        }
                      </style>

                      <style type='text/css' media='only screen and (max-width: 480px)'>
                        @media only screen and (max-width: 480px) {

                          table[class='w320'] {
                            width: 320px !important;
                          }

                          table[class='w300'] {
                            width: 300px !important;
                          }

                          table[class='w290'] {
                            width: 290px !important;
                          }

                          td[class='w320'] {
                            width: 320px !important;
                          }

                          td[class='mobile-center'] {
                            text-align: center !important;
                          }

                          td[class='mobile-padding'] {
                            padding-left: 20px !important;
                            padding-right: 20px !important;
                            padding-bottom: 20px !important;
                          }
                        }
                      </style>
                    <table align='center' cellpadding='0' cellspacing='0' width='100%' height='100%' >
                      <tr>
                        <td align='center' valign='top' bgcolor='#ffffff'  width='100%'>

                        <table cellspacing='0' cellpadding='0' width='100%'>
                          <tr>
                            <td style='border-bottom: 3px solid #3bcdc3;' width='100%'>
                              <center>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                  <tr>
                                    <td valign='top' style='padding:10px 0; text-align:center;' class='mobile-center'>
                                      <img width='250' height='62' src='https://inguate.com/images/LogoInguateAZUL.png'>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td background='http://35.232.20.49/images/background_email.gif' bgcolor='#64594b' valign='top' style='background: url(http://35.232.20.49/images/background_email.gif) no-repeat center; background-color: #64594b; background-position: center;'>
                              <div>
                                <center>
                                  <table cellspacing='0' cellpadding='0' width='530' height='303' class='w320'>
                                    <tr>
                                      <td valign='middle' style='vertical-align:middle; padding-right: 15px; padding-left: 15px; text-align:center;' class='mobile-center' height='303'>
                                        
                                        <h1 style='font-weight: bold; color: white;'>
                                            {$strTituloCorreo}
                                        </h1>
                                        <br>
                                        
                                      </td>
                                    </tr>
                                  </table>
                                </center>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td valign='top'>
                              <br>
                              <br>
                              
                              <center>
                                <table cellspacing='0' cellpadding='30' width='500' class='w290'>
                                          <tr>
                                            <td valign='top' style='border-bottom:1px solid #a1a1a1;'>
                                                      
                                              <center>
                                              <table style='margin: 0 auto;' cellspacing='0' cellpadding='8' width='500'>
                                                <tr>
                                                  <td style='text-align:;left; color: black;'>
                                                    
                                                    <p>
                                                        <b>{$strTituloCorreo}</b>
                                                        <br>
                                                        <br>
                                                        El motivo de tu rechazo es: <br>
                                                        
                                                        {$strInsertMotivo}
                                                    </p>
                                                    
                                                  </td>
                                                </tr>
                                              </table>
                                              </center>
                                            </td>
                                          </tr>
                                        </table>
                                <table cellspacing='0' cellpadding='0' width='500' class='w320'>
                                     
                                  <tr>
                                    <td class='mobile-padding' style='text-align: center;'>
                                      <table cellspacing='0' cellpadding='0' width='100%'>
                                        <tr>
                                          <td style='width:100%; text-align: center; background-color: #3bcdc3;'>
                                            <div>
                                            <table cellspacing='0' cellpadding='0' width='100%'><tr><td style='text-align: center;background-color:#3bcdc3;border-radius:0px;color:#ffffff;display:inline-block;font-family:'Lato', Helvetica, Arial, sans-serif;font-weight:bold;font-size:13px;line-height:33px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;mso-hide:all;'>
                                            <span style='color:#ffffff'><br>
                                            
                                            <a href='inguate.com' style='color:white;'><h1 >INGUATE</h1></a>
                                            
                                            <br></span></td></tr></table>
                                            
                                            </div>
                                          </td>
                                          <td>
                                            &nbsp;
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table cellspacing='0' cellpadding='25' width='100%'>
                                        <tr>
                                          <td>
                                            &nbsp;
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </center>
                            </td>
                          </tr>
                          <tr>
                            <td style='background-color:#c2c2c2;'>
                                <br>
                                <br>
                            </td>
                          </tr>
                        </table>

                        </td>
                      </tr>
                    </table>
                    ";
        fntEnviarCorreo($rTMP["email"], $strTituloCorreo, $strBody);
                
    }
    
}

?>
