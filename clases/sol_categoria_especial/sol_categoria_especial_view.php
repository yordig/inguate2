<?php

require_once "sol_categoria_especial_model.php";

class sol_categoria_especial_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new sol_categoria_especial_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        $arrSolProductoCategoriaEspecial = $this->objModel->getSolProducoEspecial();
        
        ?>
         
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
            <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
       
            <link rel="stylesheet" media="screen" type="text/css" href="dist/colorpicker/css/colorpicker.css" />
            <script type="text/javascript" src="dist/colorpicker/js/colorpicker.js"></script>
 
       
            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-2  " id="divContenidoBody"  >
                    
                    <div class="row">
                        <div class="col-12">
                            
                            <table class="table table-secondary table-hover" id="myTable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Establecimiento</th>
                                        <th scope="col">Categoria Especial</th>
                                        <th scope="col">Descripion Solicitud</th>
                                        <th scope="col">Fecha Inicio</th>
                                        <th scope="col">Fecha Fin</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col" style="width: 20%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    while( $rTMP = each($arrSolProductoCategoriaEspecial) ){
                                        ?>
                                        <tr>
                                            <td><?php print $rTMP["value"]["nombre"]?></td>
                                            <td><?php print $rTMP["value"]["titulo"]?></td>
                                            <td><?php print $rTMP["value"]["categoria_especial_nombre"]?></td>
                                            <td><?php print $rTMP["value"]["descripcion"]?></td>
                                            <td><?php print $rTMP["value"]["fecha_inicio"]?></td>
                                            <td><?php print $rTMP["value"]["fecha_fin"]?></td>
                                            <td><?php print $rTMP["value"]["estado_text"]?></td>
                                            <td class="text-right nowrap" nowrap>
                                                
                                                <button onclick="window.open('proes.php?p=<?php print fntCoreEncrypt($rTMP["value"]["id_place_producto"])?>');" class="btn btn-primary btn-raised btn-sm">Ver</button>
                                                <button onclick="fntAprobarSolProduto('<?php print $rTMP["key"]?>');" class="btn btn-success btn-raised btn-sm">Aprobar</button>
                                                <button onclick="fntEliminarSolProduto('<?php print $rTMP["key"]?>');" class="btn btn-danger btn-raised btn-sm">Eliminar</button>
                                                <button onclick="fntDrawRechazarSolProduto('<?php print $rTMP["key"]?>');" class="btn btn-danger btn-raised btn-sm">Rechazar</button>
                                                
                                                    
                                            </td>
                                        </tr>
                                        <?php                                        
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>
                            
                        </div>    
                    </div>    
                                        
                </div> 
            </main>
            
            <div class="modal fade" id="mlSolicitudRechazo" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" style="" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Rechazo</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="divBodyModalMisUsuarios">

                            <input type="hidden" id="hidSolicitudModal" name="hidSolicitudModal" value="0">
                            <label for="txtMotivoRechazo">Motivo</label>
                            <textarea class="form-control" name="txtMotivoRechazo" id="txtMotivoRechazo" rows="3" placeholder="Escrbe el motivo de rechazo"></textarea>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-raised btn-sm mr-2" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger btn-raised btn-sm" onclick="fntRechazarSolProduto();" >Enviar Rechazo</button>
                        </div>
                    </div>
                </div>
            </div>          
          
        </div>    
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            }); 
                     
            function fntAprobarSolProduto(intSol){
                
                var formData = new FormData();
                formData.append("intSol", intSol);
                
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setAprobarSolProduto=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });                
                
            }
              
            function fntEliminarSolProduto(intSol){
                
                var formData = new FormData();
                formData.append("intSol", intSol);
                
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setEliminarSolProduto=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });                
                
            }   
              
            function fntDrawRechazarSolProduto(intSol){
                
                $("#mlSolicitudRechazo").modal("show");
                $("#hidSolicitudModal").val(intSol);
                    
            }
            
            function fntRechazarSolProduto(){
                
                var formData = new FormData();
                formData.append("txtMotivoRechazo", $("#txtMotivoRechazo").val());
                formData.append("intSol", $("#hidSolicitudModal").val());
                
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setRechazarSolProduto=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });                
                
            }   
                
        </script>        
        <?php 
        
    }
    
}

?>
