<?php
 
class categoria_model{
      
    var $objDBClass;
    
    public function __construct(){
        
        $this->objDBClass = new dbClass(); 
            
    }
    
    public function getobjDBClass(){
        return $this->objDBClass;
    }
    
    public function setCloseDB(){
        
        $this->objDBClass->db_close();
    
    }
    
    public function setClasificacion($strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strProduct){
        
        $strQuery = "INSERT INTO clasificacion(nombre, tag_en, tag_es, orden, color_hex, sinonimo, producto)
                                  VALUES( '{$strNombre}', '{$strTagEN}', '{$strTagES}', '{$strOrden}', '{$strColor}', '{$strSinonimo}', '{$strProduct}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditClasificacion($intClasification, $strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strProduct){
        
        
        $strQuery = "UPDATE clasificacion
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            color_hex = '{$strColor}',
                            sinonimo = '{$strSinonimo}',
                            producto = '{$strProduct}'
                     WHERE  id_clasificacion = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setEditLogo($intClasification, $strUrlArchivo){
        
        
        $strQuery = "UPDATE clasificacion
                     SET    url_img = '{$strUrlArchivo}'
                     WHERE  id_clasificacion = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getClasificacion($intClasificacion = 0){
        
        $intClasificacion = intval($intClasificacion);
        
        $strFiltro = $intClasificacion ? "WHERE id_clasificacion = {$intClasificacion}" : "";
        
        $strQuery = "SELECT id_clasificacion, 
                            nombre,
                            tag_en,
                            tag_es,
                            orden,
                            url_img,
                            color_hex,
                            sinonimo,
                            producto
                     FROM   clasificacion
                     {$strFiltro} ";
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion"]]["orden"] = $rTMP["orden"];
            $arrInfo[$rTMP["id_clasificacion"]]["url_img"] = $rTMP["url_img"];
            $arrInfo[$rTMP["id_clasificacion"]]["color_hex"] = $rTMP["color_hex"];
            $arrInfo[$rTMP["id_clasificacion"]]["sinonimo"] = $rTMP["sinonimo"];
            $arrInfo[$rTMP["id_clasificacion"]]["producto"] = $rTMP["producto"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getClasificacionPadre($intClasificacion, $intClasificacionPadre = 0){
        
        $intClasificacion = intval($intClasificacion);
        
        $strFiltro = $intClasificacionPadre ? "AND clasificacion_padre.id_clasificacion_padre = {$intClasificacionPadre}" : "";
        
        $strQuery = "SELECT clasificacion_padre.id_clasificacion_padre, 
                            clasificacion_padre.nombre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es,
                            clasificacion_padre.orden,
                            clasificacion_padre.sinonimo,
                            clasificacion.nombre nombreCla
                     FROM   clasificacion   
                                LEFT JOIN clasificacion_padre
                                    ON  clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion
                     WHERE  clasificacion.id_clasificacion = {$intClasificacion} 
                     {$strFiltro} ";
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo["nombreCla"] = $rTMP["nombreCla"];
            
            if( intval($rTMP["id_clasificacion_padre"]) ){
                
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["orden"] = $rTMP["orden"];
                $arrInfo["ClaPadre"][$rTMP["id_clasificacion_padre"]]["sinonimo"] = $rTMP["sinonimo"];
                
                
            }
                    
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setClasificacionPadre($intClasificacionPadre, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        $strQuery = "INSERT INTO clasificacion_padre(id_clasificacion, nombre, tag_en, tag_es, orden, sinonimo)
                                  VALUES( {$intClasificacionPadre}, '{$strNombre}', '{$strTagEN}', '{$strTagES}', '{$strOrden}', '{$strSinonimo}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditClasificacionPadre($intClasification, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        
        $strQuery = "UPDATE clasificacion_padre
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            sinonimo = '{$strSinonimo}'
                     WHERE  id_clasificacion_padre = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getClasificacionPadreHijo($intClasificacionPadre, $intClasificacionPadreHijo = 0){
        
        $intClasificacionPadre = intval($intClasificacionPadre);
        $intClasificacionPadreHijo = intval($intClasificacionPadreHijo);
        
        $strFiltro = $intClasificacionPadreHijo ? "AND clasificacion_padre_hijo.id_clasificacion_padre_hijo = {$intClasificacionPadreHijo}" : "";
        
        $strQuery = "SELECT clasificacion_padre_hijo.id_clasificacion_padre_hijo, 
                            clasificacion_padre_hijo.nombre,
                            clasificacion_padre_hijo.tag_en,
                            clasificacion_padre_hijo.tag_es,
                            clasificacion_padre_hijo.orden,
                            clasificacion_padre_hijo.sinonimo,
                            
                            clasificacion.nombre nombreCla,
                            clasificacion.id_clasificacion,
                            clasificacion_padre.nombre nombreClaPadre,
                            clasificacion_padre.id_clasificacion_padre
                            
                     FROM   clasificacion,
                            clasificacion_padre
                                LEFT JOIN   clasificacion_padre_hijo
                                    ON  clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                                    
                     WHERE  clasificacion_padre.id_clasificacion_padre = {$intClasificacionPadre}
                     AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                     {$strFiltro} 
                     ";   
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo["nombreCla"] = $rTMP["nombreCla"];
            $arrInfo["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo["nombreClaPadre"] = $rTMP["nombreClaPadre"];
            $arrInfo["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            
            if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
                
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["tag_en"] = $rTMP["tag_en"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["tag_es"] = $rTMP["tag_es"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["orden"] = $rTMP["orden"];
                $arrInfo["ClaPadreHijo"][$rTMP["id_clasificacion_padre_hijo"]]["sinonimo"] = $rTMP["sinonimo"];
                
                
            }
                    
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setClasificacionPadreHijo($intClasificacionPadre, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        $strQuery = "INSERT INTO clasificacion_padre_hijo(id_clasificacion_padre, nombre, tag_en, tag_es, orden, sinonimo)
                                  VALUES( {$intClasificacionPadre}, '{$strNombre}', '{$strTagEN}', '{$strTagES}', '{$strOrden}', '{$strSinonimo}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditClasificacionPadreHijo($intClasificacionPadreHijo, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        
        $strQuery = "UPDATE clasificacion_padre_hijo
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            sinonimo = '{$strSinonimo}'
                     WHERE  id_clasificacion_padre_hijo = {$intClasificacionPadreHijo} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getClasificacionPadreHijoDetalle($intClasificacionPadreHijo, $intClasificacionPadreHijoDetalle = 0){
        
        $intClasificacionPadreHijo = intval($intClasificacionPadreHijo);
        $intClasificacionPadreHijoDetalle = intval($intClasificacionPadreHijoDetalle);
        
        $strFiltro = $intClasificacionPadreHijoDetalle ? "AND clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle = {$intClasificacionPadreHijoDetalle}" : "";
        
        $strQuery = "SELECT clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle, 
                            clasificacion_padre_hijo_detalle.nombre,
                            clasificacion_padre_hijo_detalle.tag_en,
                            clasificacion_padre_hijo_detalle.tag_es,
                            clasificacion_padre_hijo_detalle.orden,
                            clasificacion_padre_hijo_detalle.sinonimo,
                            
                            clasificacion.nombre nombreCla,
                            clasificacion.id_clasificacion,
                            clasificacion_padre.nombre nombreClaPadre,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre_hijo.nombre nombreClaPadreHijo,
                            clasificacion_padre_hijo.id_clasificacion_padre_hijo
                            
                     FROM   clasificacion,
                            clasificacion_padre,
                            clasificacion_padre_hijo
                                LEFT JOIN   clasificacion_padre_hijo_detalle
                                    ON  clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo 
                                    
                     WHERE  clasificacion_padre_hijo.id_clasificacion_padre_hijo = {$intClasificacionPadreHijo}
                     AND    clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                     AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                     {$strFiltro} 
                     ";   
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo["nombreCla"] = $rTMP["nombreCla"];
            $arrInfo["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo["nombreClaPadre"] = $rTMP["nombreClaPadre"];
            $arrInfo["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo["nombreClaPadreHijo"] = $rTMP["nombreClaPadreHijo"];
            $arrInfo["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            
            if( intval($rTMP["id_clasificacion_padre_hijo_detalle"]) ){
                
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["id_clasificacion_padre_hijo_detalle"] = $rTMP["id_clasificacion_padre_hijo_detalle"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["nombre"] = $rTMP["nombre"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_en"] = $rTMP["tag_en"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_es"] = $rTMP["tag_es"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["orden"] = $rTMP["orden"];
                $arrInfo["ClaPadreHijoDetalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["sinonimo"] = $rTMP["sinonimo"];
                
                
            }
                    
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setClasificacionPadreHijoDetalle($intClasificacionPadreHijo, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        $strQuery = "INSERT INTO clasificacion_padre_hijo_detalle(id_clasificacion_padre_hijo, nombre, tag_en, tag_es, orden, sinonimo)
                                  VALUES( {$intClasificacionPadreHijo}, '{$strNombre}', '{$strTagEN}', '{$strTagES}', '{$strOrden}', '{$strSinonimo}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditClasificacionPadreHijoDetalle($intClasificacionPadreHijoDetalle, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo){
        
        
        $strQuery = "UPDATE clasificacion_padre_hijo_detalle
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}',
                            orden = '{$strOrden}',
                            sinonimo = '{$strSinonimo}'
                     WHERE  id_clasificacion_padre_hijo_detalle = {$intClasificacionPadreHijoDetalle} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function setEliminarClasificacion($intClasificacion){
        
        $intClasificacion = intval($intClasificacion);
        
        $strQuery = "DELETE clasificacion_padre_hijo_detalle
                     FROM   clasificacion_padre_hijo_detalle
                                
                                INNER JOIN  clasificacion_padre_hijo
                                
                                ON  clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo    
                                    INNER JOIN  clasificacion_padre
                                    
                                    ON  clasificacion_padre_hijo.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre
                                    AND clasificacion_padre.id_clasificacion = {$intClasificacion}
                                    
                                 ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE clasificacion_padre_hijo 
                     FROM   clasificacion_padre_hijo
                     
                                INNER JOIN  clasificacion_padre
                                    
                                ON  clasificacion_padre_hijo.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre
                                AND clasificacion_padre.id_clasificacion = {$intClasificacion}
                            
                     ";
    
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM clasificacion_padre WHERE id_clasificacion = {$intClasificacion}";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM clasificacion WHERE id_clasificacion = {$intClasificacion}";
        
        $this->objDBClass->db_consulta($strQuery);
    
    }
    
    function setEliminarClasificacionPadreHijoDetalle($intClasificacionPadreHijoDetalle){
        
        $intClasificacionPadreHijoDetalle = intval($intClasificacionPadreHijoDetalle);
        
        $strQuery = "DELETE FROM clasificacion_padre_hijo_detalle WHERE id_clasificacion_padre_hijo_detalle = {$intClasificacionPadreHijoDetalle} ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    function setEliminarClasificacionPadreHijo($intClasificacionPadreHijo){
        
        $intClasificacionPadreHijo = intval($intClasificacionPadreHijo);
        
        $strQuery = "DELETE clasificacion_padre_hijo_detalle
                     FROM   clasificacion_padre_hijo_detalle
                                
                                INNER JOIN  clasificacion_padre_hijo
                                
                                ON  clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo    
                                AND clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = {$intClasificacionPadreHijo}
                                 ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        
        $strQuery = "DELETE FROM clasificacion_padre_hijo WHERE id_clasificacion_padre_hijo = {$intClasificacionPadreHijo} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEliminarClasificacionPadre($intClasificacionPadre){
        
        $strQuery = "DELETE clasificacion_padre_hijo_detalle
                     FROM   clasificacion_padre_hijo_detalle
                                
                                INNER JOIN  clasificacion_padre_hijo
                                
                                ON  clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo = clasificacion_padre_hijo.id_clasificacion_padre_hijo    
                                AND clasificacion_padre_hijo.id_clasificacion_padre = {$intClasificacionPadre}
                                 ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM  clasificacion_padre_hijo WHERE id_clasificacion_padre = {$intClasificacionPadre}   ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM  clasificacion_padre WHERE id_clasificacion_padre = {$intClasificacionPadre}   ";
        
        $this->objDBClass->db_consulta($strQuery);
        
        
        
    }
                              
}

?>
