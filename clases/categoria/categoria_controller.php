<?php
  
require_once "categoria_model.php";
require_once "categoria_view.php";

class categoria_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new categoria_model();
        $this->objView = new categoria_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["drawFormNewClassification"]) ){
            
            $intClasificacion = isset($_GET["clasificacion"]) ? trim($_GET["clasificacion"]) : "";
            
            $this->objView->fntDrawFormNewClassification($strAction, $intClasificacion);
            
            die();
            
        }
        
        if( isset($_GET["drawAdminClasificacionPadre"]) ){
            
            $intClasificacion = isset($_GET["clasificacion"]) ? trim($_GET["clasificacion"]) : "";
            
            $this->objView->fntDrawAdminClasificacionPadre($strAction, $intClasificacion);
            
            die();
            
        }
        
        if( isset($_GET["drawAdminClasificacionPadreHijo"]) ){
            
            $intClasificacionPadre = isset($_GET["clasificacionPadre"]) ? trim($_GET["clasificacionPadre"]) : "";
            $intClasificacionPadreHijo = isset($_GET["clasificacionPadreHijo"]) ? trim($_GET["clasificacionPadreHijo"]) : "";
            
            $this->objView->fntDrawAdminClasificacionPadreHijo($strAction, $intClasificacionPadre, $intClasificacionPadreHijo);
            
            die();
            
        }
        
        if( isset($_GET["drawAdminClasificacionPadreHijoDetalle"]) ){
            
            $intClasificacionPadreHijo = isset($_GET["clasificacionPadreHijo"]) ? trim($_GET["clasificacionPadreHijo"]) : "";
            
            $this->objView->fntDrawAdminClasificacionPadreHijoDetalle($strAction, $intClasificacionPadreHijo);
            
            die();
            
        }
        
        if( isset($_GET["drawFormNewClassificationPadre"]) ){
            
            $intClasificacion = isset($_GET["clasificacion"]) ? intval($_GET["clasificacion"]) : "";
            $intClasificacionPadre = isset($_GET["clasificacionPadre"]) ? intval($_GET["clasificacionPadre"]) : "";
            
            $this->objView->fntDrawFormNewClassificacionPadre($strAction, $intClasificacion, $intClasificacionPadre);
            
            die();
            
        }
        
        if( isset($_GET["drawFormNewClassificationPadreHijo"]) ){
            
            $intClasificacionPadre = isset($_GET["clasificacionPadre"]) ? intval($_GET["clasificacionPadre"]) : "";
            $intClasificacionPadreHijo = isset($_GET["clasificacionPadreHijo"]) ? intval($_GET["clasificacionPadreHijo"]) : "";
            
            $this->objView->fntDrawFormNewClassificacionPadreHijo($strAction, $intClasificacionPadre, $intClasificacionPadreHijo);
            
            die();
            
        }
        
        if( isset($_GET["drawFormNewClassificationPadreHijoDetalle"]) ){
            
            $intClasificacionPadreHijo = isset($_GET["clasificacionPadreHijo"]) ? intval($_GET["clasificacionPadreHijo"]) : "";
            $intClasificacionPadreHijoDetalle = isset($_GET["clasificacionPadreHijoDetalle"]) ? intval($_GET["clasificacionPadreHijoDetalle"]) : "";
            
            $this->objView->fntDrawFormNewClassificacionPadreHijoDetalle($strAction, $intClasificacionPadreHijo, $intClasificacionPadreHijoDetalle);
            
            die();
            
        }
        
        if( isset($_GET["setClasificacion"]) ){
            
            $intClasificacion = isset($_POST["hidIdClassification"]) ? intval($_POST["hidIdClassification"]) : 0;
            $strNombre = isset($_POST["txtNombre"]) ? addslashes($_POST["txtNombre"]) : "";
            $strTagEN = isset($_POST["txtTag_en"]) ? addslashes($_POST["txtTag_en"]) : "";
            $strTagES = isset($_POST["txtTag_es"]) ? addslashes($_POST["txtTag_es"]) : "";
            $strOrden = isset($_POST["txtOrden"]) ? addslashes($_POST["txtOrden"]) : 0;
            $strColor = isset($_POST["txtColor"]) ? addslashes($_POST["txtColor"]) : 0;
            $strSinonimo = isset($_POST["txtSinonimo"]) ? addslashes($_POST["txtSinonimo"]) : 0;
            $strProduct = isset($_POST["slcProduct"]) ? addslashes($_POST["slcProduct"]) : 0;
            
            if( $intClasificacion ){
                
                $this->objModel->setEditClasificacion($intClasificacion, $strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strProduct);
                    
            }
            else{
                $this->objModel->setClasificacion( $strNombre, $strTagEN, $strTagES, $strOrden, $strColor, $strSinonimo, $strProduct);    
            }
            
            if( isset($_FILES["fllogo"]) && $_FILES["fllogo"]["size"] > 0 ){
                
                $strUrlArchivo = "images/icon/clas/".fntCoreEncrypt($intClasificacion).".png";
                rename($_FILES["fllogo"]["tmp_name"], $strUrlArchivo);
                
                $this->objModel->setEditLogo($intClasificacion, $strUrlArchivo);
                
            }
            
            die();
        }
        
        if( isset($_GET["setClasificacionPadre"]) ){
            
            $intClasificacion = isset($_POST["hidIdClassification"]) ? intval($_POST["hidIdClassification"]) : 0;
            $intClasificacionPadre = isset($_POST["hidIdClassificationPadre"]) ? intval($_POST["hidIdClassificationPadre"]) : 0;
            $strNombre = isset($_POST["txtNombre"]) ? addslashes($_POST["txtNombre"]) : "";
            $strTagEN = isset($_POST["txtTag_en"]) ? addslashes($_POST["txtTag_en"]) : "";
            $strTagES = isset($_POST["txtTag_es"]) ? addslashes($_POST["txtTag_es"]) : "";
            $strOrden = isset($_POST["txtOrden"]) ? addslashes($_POST["txtOrden"]) : 0;
            $strSinonimo = isset($_POST["txtSinonimo"]) ? addslashes($_POST["txtSinonimo"]) : 0;
            
            if( $intClasificacionPadre ){
                
                $this->objModel->setEditClasificacionPadre($intClasificacionPadre, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo);
                    
            }
            else{
                $this->objModel->setClasificacionPadre($intClasificacion, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo);    
            }
            
            die();
        }
        
        if( isset($_GET["setClasificacionPadreHijo"]) ){
            
            $intClasificacionPadre = isset($_POST["hidIdClassificationPadre"]) ? intval($_POST["hidIdClassificationPadre"]) : 0;
            $intClasificacionPadreHijo = isset($_POST["hidIdClassificationPadreHijo"]) ? intval($_POST["hidIdClassificationPadreHijo"]) : 0;
            $strNombre = isset($_POST["txtNombre"]) ? addslashes($_POST["txtNombre"]) : "";
            $strTagEN = isset($_POST["txtTag_en"]) ? addslashes($_POST["txtTag_en"]) : "";
            $strTagES = isset($_POST["txtTag_es"]) ? addslashes($_POST["txtTag_es"]) : "";
            $strOrden = isset($_POST["txtOrden"]) ? addslashes($_POST["txtOrden"]) : 0;
            $strSinonimo = isset($_POST["txtSinonimo"]) ? addslashes($_POST["txtSinonimo"]) : 0;
            
            if( $intClasificacionPadreHijo ){
                
                $this->objModel->setEditClasificacionPadreHijo($intClasificacionPadreHijo, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo);
                    
            }
            else{
                $this->objModel->setClasificacionPadreHijo($intClasificacionPadre, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo);    
            }
            
            die();
        }
        
        if( isset($_GET["setClasificacionPadreHijoDetalle"]) ){
            
            $intClasificacionPadreHijo = isset($_POST["hidIdClassificationPadreHijo"]) ? intval($_POST["hidIdClassificationPadreHijo"]) : 0;
            $intClasificacionPadreHijoDetalle = isset($_POST["hidIdClassificationPadreHijoDetalle"]) ? intval($_POST["hidIdClassificationPadreHijoDetalle"]) : 0;
            $strNombre = isset($_POST["txtNombre"]) ? addslashes($_POST["txtNombre"]) : "";
            $strTagEN = isset($_POST["txtTag_en"]) ? addslashes($_POST["txtTag_en"]) : "";
            $strTagES = isset($_POST["txtTag_es"]) ? addslashes($_POST["txtTag_es"]) : "";
            $strOrden = isset($_POST["txtOrden"]) ? addslashes($_POST["txtOrden"]) : 0;
            $strSinonimo = isset($_POST["txtSinonimo"]) ? addslashes($_POST["txtSinonimo"]) : 0;
            
            if( $intClasificacionPadreHijoDetalle ){
                
                $this->objModel->setEditClasificacionPadreHijoDetalle($intClasificacionPadreHijoDetalle, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo);
                    
            }
            else{
                $this->objModel->setClasificacionPadreHijoDetalle($intClasificacionPadreHijo, $strNombre, $strTagEN, $strTagES, $strOrden, $strSinonimo);    
            }
            
            die();
        }
        
        if( isset($_GET["setEliminarClasificacion"]) ){
            
            $intClasificacion = isset($_GET["key"]) ? intval($_GET["key"]) : 0;
            
            $this->objModel->setEliminarClasificacion($intClasificacion);
            
            die();
        }
        
        if( isset($_GET["setEliminarClasificacionPadreHijoDetalle"]) ){
            
            $intClasificacionPadreHijoDetalle = isset($_GET["key"]) ? intval($_GET["key"]) : 0;
            
            $this->objModel->setEliminarClasificacionPadreHijoDetalle($intClasificacionPadreHijoDetalle);
            
            die();
        }
        
        if( isset($_GET["setEliminarClasificacionPadreHijo"]) ){
            
            $intClasificacionPadreHijo = isset($_GET["key"]) ? intval($_GET["key"]) : 0;
            
            $this->objModel->setEliminarClasificacionPadreHijo($intClasificacionPadreHijo);
            
            die();
        }
        
        if( isset($_GET["setEliminarClasificacionPadre"]) ){
            
            $intClasificacionPadre = isset($_GET["key"]) ? intval($_GET["key"]) : 0;
            
            $this->objModel->setEliminarClasificacionPadre($intClasificacionPadre);
            
            die();
        }
        
                     
    }
    
}

  
?>
