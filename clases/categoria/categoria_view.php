<?php

require_once "categoria_model.php";

class categoria_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new categoria_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        $arrUsuario = $this->objModel->getClasificacion();
        
        ?>
        
         
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
            <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
       
            <link rel="stylesheet" media="screen" type="text/css" href="dist/colorpicker/css/colorpicker.css" />
            <script type="text/javascript" src="dist/colorpicker/js/colorpicker.js"></script>
 
       
            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-2  " id="divContenidoBody"  >
                    
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewClassification();">New Classification</button>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-12">
                            
                            <table class="table table-secondary table-hover" id="myTable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Tag EN</th>
                                        <th scope="col">Tag ES</th>
                                        <th scope="col">Orden</th>
                                        <th scope="col">Icon</th>
                                        <th scope="col">Synonyms</th>
                                        <th scope="col" style="width: 20%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    while( $rTMP = each($arrUsuario) ){
                                        ?>
                                        <tr>
                                            <td><?php print $rTMP["value"]["nombre"]?></td>
                                            <td><?php print $rTMP["value"]["tag_en"]?></td>
                                            <td><?php print $rTMP["value"]["tag_es"]?></td>
                                            <td><?php print $rTMP["value"]["orden"]?></td>
                                            <td>
                                                
                                                <div style="background-color: <?php print "#".$rTMP["value"]["color_hex"]?>; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                                    <img src="<?php print $rTMP["value"]["url_img"]?>">
                                                </div>
                                                
                                            </td>
                                            <td><?php print $rTMP["value"]["sinonimo"]?></td>
                                            <td class="text-right nowrap">
                                                <button onclick="fntDrawFormNewClassification('<?php print $rTMP["value"]["id_clasificacion"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                                <button onclick="fntDrawAdminClasificacionPadre('<?php print $rTMP["value"]["id_clasificacion"]?>');" class="btn btn-success btn-raised btn-sm">Admin</button>
                                                <button onclick="fntDEliminarClasificacionPadre('<?php print $rTMP["value"]["id_clasificacion"]?>');" class="btn btn-danger btn-raised btn-sm">Eliminar</button>
                                            </td>
                                        </tr>
                                        <?php                                        
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>

                            
                            
                            
                        </div>    
                    </div>    
                    
                    
                                        
                </div> 
            </main>
               
          
        </div>    
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            }); 
            
            function fntDrawFormNewClassification(intClassification){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawFormNewClassification=true&clasificacion="+intClassification, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }
             
            function fntDrawAdminClasificacionPadre(intClassification){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawAdminClasificacionPadre=true&clasificacion="+intClassification, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }  
            
            function fntDrawAdminClasificacionPadreHijo(intClassificationPadre){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawAdminClasificacionPadreHijo=true&clasificacionPadre="+intClassificationPadre, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }
            
            function fntDrawAdminClasificacionPadreHijoDetalle(intClassificationPadreHijo){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawAdminClasificacionPadreHijoDetalle=true&clasificacionPadreHijo="+intClassificationPadreHijo, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }  
            
            function fntDEliminarClasificacionPadre(intClasificacion){
                
                swal({
                    title: "Estas Seguro",
                    text: "Se Eliminaran todos los datos",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "<?php print $strAction?>?setEliminarClasificacion=true&key="+intClasificacion, 
                            success: function(result){
                                
                                fntCargarPaginaActiva();
                                
                            }
                            
                        });
                        
                    
                    } 
                });
                
                
            }
                
        </script>        
        <?php 
        
    }
    
    public function fntDrawFormNewClassification($strAction, $intClasificacion){
        
        $arrClassificacion = $this->objModel->getClasificacion($intClasificacion);
        $arrClassificacion = isset($arrClassificacion[$intClasificacion]) ? $arrClassificacion[$intClasificacion] : array();
        
        ?>
        <style>
            
            .colorpicker {
                width: 356px;
                height: 176px;
                overflow: hidden;
                position: absolute;
                background: url(dist/colorpicker/images/colorpicker_background.png);
                font-family: Arial, Helvetica, sans-serif;
                display: none;
                z-index: 999;
            }
            
        </style>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntCargarPaginaActiva();">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveClasificacion();">Save</button>
            </div>    
        </div>
        
        <div class="row">
            <div class="col-6 offset-3 ">
                
                <form onsubmit="return false;" id="frmClasificacion" method="POST">
                    <input type="hidden" name="hidIdClassification" value="<?php print $intClasificacion?>">
                    <div class="form-group">
                        <label for="txtNombre"><small>Nombre</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["nombre"]) ? $arrClassificacion["nombre"] : ""?>" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                    
                    </div>
                    
                    <div class="form-group">
                        <label for="txtTag_en"><small>Tag EN</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["tag_en"]) ? $arrClassificacion["tag_en"] : ""?>" class="form-control" id="txtTag_en" name="txtTag_en" placeholder="Tag EN" required>
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Tag ES</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["tag_es"]) ? $arrClassificacion["tag_es"] : ""?>" class="form-control" id="txtTag_es" name="txtTag_es" placeholder="Tag ES" required>
                    </div>
                    <div class="form-group">
                        <label for="txtOrden"><small>Orden</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["orden"]) ? $arrClassificacion["orden"] : ""?>" class="form-control" id="txtOrden" name="txtOrden" placeholder="Orden" required>
                    
                    </div>
                    <div class="form-group">
                        <label for="slcProduct"><small>Product</small></label>
                        <select class="custom-select " name="slcProduct">
                            <option <?php print isset($arrClassificacion["producto"]) && $arrClassificacion["producto"] == "Y" ? "selected" : ""?> value="Y">Yes</option>
                            <option <?php print isset($arrClassificacion["producto"]) && $arrClassificacion["producto"] == "N" ? "selected" : ""?> value="N">No</option>
                        </select>                    
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Synonyms</small></label>
                        <input type="text" value="<?php print isset($arrClassificacion["sinonimo"]) ? $arrClassificacion["sinonimo"] : ""?>" class="form-control" id="txtSinonimo" name="txtSinonimo" placeholder="Synonyms" required>
                    </div>
                    <div class="form-group">
                        <label for="txtOrden"><small>Logo</small></label>                    
                        <input type="file" onchange="fntSetImgfile();" class="form-control" id="fllogo" name="fllogo" >
                                         
                        <div id="divIcono"  onclick="$('#fllogo').click();" class="" style="background-color: <?php print isset($arrClassificacion["color_hex"]) ? "#".$arrClassificacion["color_hex"] : "red"?>; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                            <img id="IMGdivIcono" src="<?php print isset($arrClassificacion["url_img"]) ? $arrClassificacion["url_img"] : ""?>">
                        </div>
                        
                        <input type="text" value="<?php print isset($arrClassificacion["color_hex"]) ? $arrClassificacion["color_hex"] : ""?>" class="form-controld" id="txtColor" name="txtColor" >
                    
                        
                    </div>
                    
                    <div class="form-group">
                        
                        
                    </div>
                  
                </form>     
                
            </div>    
        </div>
        <script>
             
            $( document ).ready(function() {
                
                
                $('#txtColor').ColorPicker({
                    onSubmit: function(hsb, hex, rgb, el) {
                        $(el).val(hex);
                        $(el).ColorPickerHide();
                        $("#divIcono").css("background-color", "#"+hex); 
                    },
                    onBeforeShow: function () {
                        $(this).ColorPickerSetColor(this.value);
                    }
                })
                .bind('keyup', function(){
                    $(this).ColorPickerSetColor(this.value);
                });
                    
            });
            
            function fntSaveClasificacion(){
                
                var formData = new FormData(document.getElementById("frmClasificacion"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setClasificacion=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });
                
            }
            
            function fntSetImgfile(){
                
                    
                var filename = $('#fllogo').val();
                
                arr = filename.split(".");
                
                if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                    
                    swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
                    fntDeleteImageSeccion1(index);
                    return false;                    
                    
                }
                
                
                if ( ! window.FileReader ) {
                    return alert( 'FileReader API is not supported by your browser.' );
                }
                var $i = $( '#fllogo'), // Put file input ID here
                    input = $i[0]; // Getting the element from jQuery
                if ( input.files && input.files[0] ) {
                    file = input.files[0]; // The file
                    fr = new FileReader(); // FileReader instance
                    fr.onload = function () {
                        // Do stuff on onload, use fr.result for contents of file
                        //$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
                        //console.log(fr.result);
                        document.getElementById("IMGdivIcono").src = fr.result;
                    };
                    //fr.readAsText( file );
                    fr.readAsDataURL( file );
                } else {
                    // Handle errors here
                    alert( "File not selected or browser incompatible." )
                }
                                
            }
            
                            
        </script>
        
        <?php
        
    }
     
    public function fntDrawAdminClasificacionPadre($strAction, $intClasification){
        
        $arrClasificacionPadre = $this->objModel->getClasificacionPadre($intClasification);
        
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntCargarPaginaActiva();">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewClasificationPadre();">New Classification Padre</button> 
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb p-0 m-0">
                    <ol class="breadcrumb p-1 m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $arrClasificacionPadre["nombreCla"]?></a></li>
                    </ol>
                </nav>
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                
                <table class="table table-secondary table-hover" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Tag EN</th>
                            <th scope="col">Tag ES</th>
                            <th scope="col">Orden</th>
                            <th scope="col">Synonyms</th>
                            <th scope="col" style="width: 20%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        if( isset($arrClasificacionPadre["ClaPadre"]) && count($arrClasificacionPadre["ClaPadre"])  > 0 ){
                            
                            while( $rTMP = each($arrClasificacionPadre["ClaPadre"]) ){
                                ?>
                                <tr>
                                    <td><?php print $rTMP["value"]["nombre"]?></td>
                                    <td><?php print $rTMP["value"]["tag_en"]?></td>
                                    <td><?php print $rTMP["value"]["tag_es"]?></td>
                                    <td><?php print $rTMP["value"]["orden"]?></td>
                                    <td><?php print $rTMP["value"]["sinonimo"]?></td>
                                    <td class="text-right nowrap">
                                        <button onclick="fntDrawFormNewClasificationPadre('<?php print $rTMP["value"]["id_clasificacion_padre"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                        <button onclick="fntDrawAdminClasificacionPadreHijo('<?php print $rTMP["value"]["id_clasificacion_padre"]?>');" class="btn btn-success btn-raised btn-sm">Admin</button>
                                        <button onclick="fntEliminarClasificacionPadre('<?php print $rTMP["value"]["id_clasificacion_padre"]?>');" class="btn btn-danger btn-raised btn-sm">Eliminar</button>
                                    </td>
                                </tr>
                                <?php                                        
                            }
                            
                        }
                            
                        
                        ?>
                    </tbody>
                </table>

                
                
                
            </div>    
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            });
            
            function fntDrawFormNewClasificationPadre(intClasificacionPadre){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawFormNewClassificationPadre=true&clasificacion=<?php print $intClasification?>&clasificacionPadre="+intClasificacionPadre, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });    
                                
            }
            
            function fntEliminarClasificacionPadre(intClasificacionPadre){
                
                swal({
                    title: "Estas Seguro",
                    text: "Se Eliminaran todos los datos",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "<?php print $strAction?>?setEliminarClasificacionPadre=true&key="+intClasificacionPadre, 
                            success: function(result){
                                
                                fntDrawAdminClasificacionPadre('<?php print $intClasification;?>');
                                
                            }
                            
                        });
                        
                    
                    } 
                });
                
                
            }
            
        </script>
        <?php    
        
        
    }
    
    public function fntDrawFormNewClassificacionPadre($strAction, $intClasificacion, $intClasificacionPadre){
        
        $arrClassificacionPadre = $this->objModel->getClasificacionPadre($intClasificacion, $intClasificacionPadre);
        $strNombreClasificacion = $arrClassificacionPadre["nombreCla"];
        $arrClassificacionPadre = isset($arrClassificacionPadre["ClaPadre"][$intClasificacionPadre]) ? $arrClassificacionPadre["ClaPadre"][$intClasificacionPadre] : array();
        
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntDrawAdminClasificacionPadre('<?php print $intClasificacion;?>');">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveClasificacionPadre();">Save</button>
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb p-0 m-0">
                    <ol class="breadcrumb p-1 m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $strNombreClasificacion?></a></li>
                    </ol>
                </nav>
            </div>    
        </div>
        
        <div class="row">
            <div class="col-6 offset-3 ">
                
                <form onsubmit="return false;" id="frmClasificacionPadre" method="POST">
                    <input type="hidden" name="hidIdClassification" value="<?php print $intClasificacion?>">
                    <input type="hidden" name="hidIdClassificationPadre" value="<?php print $intClasificacionPadre?>">
                    <div class="form-group">
                        <label for="txtNombre"><small>Nombre</small></label>
                        <input type="text" value="<?php print isset($arrClassificacionPadre["nombre"]) ? $arrClassificacionPadre["nombre"] : ""?>" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="txtTag_en"><small>Tag EN</small></label>
                        <input type="text" value="<?php print isset($arrClassificacionPadre["tag_en"]) ? $arrClassificacionPadre["tag_en"] : ""?>" class="form-control" id="txtTag_en" name="txtTag_en" placeholder="Tag EN" required>
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Tag ES</small></label>
                        <input type="text" value="<?php print isset($arrClassificacionPadre["tag_es"]) ? $arrClassificacionPadre["tag_es"] : ""?>" class="form-control" id="txtTag_es" name="txtTag_es" placeholder="Tag ES" required>
                    </div>
                    <div class="form-group">
                        <label for="txtOrden"><small>Orden</small></label>
                        <input type="text" value="<?php print isset($arrClassificacionPadre["orden"]) ? $arrClassificacionPadre["orden"] : ""?>" class="form-control" id="txtOrden" name="txtOrden" placeholder="Orden" required>
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Synonyms</small></label>
                        <input type="text" value="<?php print isset($arrClassificacionPadre["sinonimo"]) ? $arrClassificacionPadre["sinonimo"] : ""?>" class="form-control" id="txtSinonimo" name="txtSinonimo" placeholder="Synonyms" required>
                    </div>
                  
                </form>     
                
            </div>    
        </div>
        <script>
             
            $( document ).ready(function() {
                
                
            });
            
            function fntSaveClasificacionPadre(){
                
                var formData = new FormData(document.getElementById("frmClasificacionPadre"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setClasificacionPadre=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntDrawAdminClasificacionPadre('<?php print $intClasificacion;?>');
                        
                    }
                            
                });
                
            }
                            
        </script>
        
        <?php
        
    }
    
    public function fntDrawAdminClasificacionPadreHijo($strAction, $intClasificacionPadre, $intClasificacionPadreHijo){
        
        $arrClasificacionPadreHijo = $this->objModel->getClasificacionPadreHijo($intClasificacionPadre, $intClasificacionPadreHijo);
        
        
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntDrawAdminClasificacionPadre('<?php print $arrClasificacionPadreHijo["id_clasificacion"];?>');">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewClasificationPadreHijo();">New Classificacion Padre Hijo</button> 
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb p-0 m-0">
                    <ol class="breadcrumb p-1 m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $arrClasificacionPadreHijo["nombreCla"]?></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $arrClasificacionPadreHijo["nombreClaPadre"]?></a></li>
                    </ol>
                </nav>
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                
                <table class="table table-secondary table-hover" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Tag EN</th>
                            <th scope="col">Tag ES</th>
                            <th scope="col">Orden</th>
                            <th scope="col">Synonyms</th>
                            <th scope="col" style="width: 20%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        if( isset($arrClasificacionPadreHijo["ClaPadreHijo"]) && count($arrClasificacionPadreHijo["ClaPadreHijo"])  > 0 ){
                            
                            while( $rTMP = each($arrClasificacionPadreHijo["ClaPadreHijo"]) ){
                                ?>
                                <tr>
                                    <td><?php print $rTMP["value"]["nombre"]?></td>
                                    <td><?php print $rTMP["value"]["tag_en"]?></td>
                                    <td><?php print $rTMP["value"]["tag_es"]?></td>
                                    <td><?php print $rTMP["value"]["orden"]?></td>
                                    <td><?php print $rTMP["value"]["sinonimo"]?></td>
                                    <td class="text-right nowrap">
                                        <button onclick="fntDrawFormNewClasificationPadreHijo('<?php print $rTMP["value"]["id_clasificacion_padre_hijo"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                        <button onclick="fntDrawAdminClasificacionPadreHijoDetalle('<?php print $rTMP["value"]["id_clasificacion_padre_hijo"]?>');" class="btn btn-success btn-raised btn-sm">Admin</button>
                                        <button onclick="fntEliminarClasificacionPadreHijo('<?php print $rTMP["value"]["id_clasificacion_padre_hijo"]?>');" class="btn btn-danger btn-raised btn-sm">Eliminar</button>
                                    </td>
                                </tr>
                                <?php                                        
                            }
                            
                        }
                            
                        
                        ?>
                    </tbody>
                </table>

                
                
                
            </div>    
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            });
            
            function fntDrawFormNewClasificationPadreHijo(intClasificacionPadreHijo){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawFormNewClassificationPadreHijo=true&clasificacionPadre=<?php print $intClasificacionPadre?>&clasificacionPadreHijo="+intClasificacionPadreHijo, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });    
                                
            }
            
            function fntEliminarClasificacionPadreHijo(intClasificacionPadreHijo){
                
                swal({
                    title: "Estas Seguro",
                    text: "Se Eliminaran todos los datos",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "<?php print $strAction?>?setEliminarClasificacionPadreHijo=true&key="+intClasificacionPadreHijo, 
                            success: function(result){
                                
                                fntDrawAdminClasificacionPadreHijo('<?php print $intClasificacionPadre;?>');
                            }
                            
                        });
                        
                    
                    } 
                });
                
                
            }
            
        </script>
        <?php    
        
        
    }
    
    public function fntDrawFormNewClassificacionPadreHijo($strAction, $intClasificacionPadre, $intClasificacionPadreHijo){
        
        $arrClasificacionPadreHijo = $this->objModel->getClasificacionPadreHijo($intClasificacionPadre, $intClasificacionPadreHijo);
        $strNombreClasificacion = $arrClasificacionPadreHijo["nombreCla"];
        $strNombreClasificacionPadre = $arrClasificacionPadreHijo["nombreClaPadre"];
        $intClasificacionPadre = $arrClasificacionPadreHijo["id_clasificacion_padre"];
        $arrClasificacionPadreHijo = isset($arrClasificacionPadreHijo["ClaPadreHijo"][$intClasificacionPadreHijo]) ? $arrClasificacionPadreHijo["ClaPadreHijo"][$intClasificacionPadreHijo] : array();
        
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntDrawAdminClasificacionPadreHijo('<?php print $intClasificacionPadre;?>');">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveClasificacionPadreHijo();">Save</button>
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb p-0 m-0">
                    <ol class="breadcrumb p-1 m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $strNombreClasificacion?></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $strNombreClasificacionPadre?></a></li>
                    </ol>
                </nav>
            </div>    
        </div>
        
        <div class="row">
            <div class="col-6 offset-3 ">
                
                <form onsubmit="return false;" id="frmClasificacionPadreHijo" method="POST">
                    <input type="hidden" name="hidIdClassificationPadre" value="<?php print $intClasificacionPadre?>">
                    <input type="hidden" name="hidIdClassificationPadreHijo" value="<?php print $intClasificacionPadreHijo?>">
                    <div class="form-group">
                        <label for="txtNombre"><small>Nombre</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["nombre"]) ? $arrClasificacionPadreHijo["nombre"] : ""?>" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="txtTag_en"><small>Tag EN</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["tag_en"]) ? $arrClasificacionPadreHijo["tag_en"] : ""?>" class="form-control" id="txtTag_en" name="txtTag_en" placeholder="Tag EN" required>
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Tag ES</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["tag_es"]) ? $arrClasificacionPadreHijo["tag_es"] : ""?>" class="form-control" id="txtTag_es" name="txtTag_es" placeholder="Tag ES" required>
                    </div>
                    <div class="form-group">
                        <label for="txtOrden"><small>Orden</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["orden"]) ? $arrClasificacionPadreHijo["orden"] : ""?>" class="form-control" id="txtOrden" name="txtOrden" placeholder="Orden" required>
                    </div>
                    <div class="form-group">
                        <label for="txtSinonimo"><small>Synonyms</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["sinonimo"]) ? $arrClasificacionPadreHijo["sinonimo"] : ""?>" class="form-control" id="txtSinonimo" name="txtSinonimo" placeholder="Synonyms" required>
                    </div>
                  
                </form>     
                
            </div>    
        </div>
        <script>
             
            $( document ).ready(function() {
                
                
            });
            
            function fntSaveClasificacionPadreHijo(){
                
                var formData = new FormData(document.getElementById("frmClasificacionPadreHijo"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setClasificacionPadreHijo=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntDrawAdminClasificacionPadreHijo('<?php print $intClasificacionPadre;?>');
                    
                    }
                            
                });
                
            }
                            
        </script>
        
        <?php
        
    }
    
    public function fntDrawAdminClasificacionPadreHijoDetalle($strAction, $intClasificacionPadreHijo){
        
        $arrClasificacionPadreHijoDetalle = $this->objModel->getClasificacionPadreHijoDetalle($intClasificacionPadreHijo);
        
        
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntDrawAdminClasificacionPadreHijo('<?php print $arrClasificacionPadreHijoDetalle["id_clasificacion_padre"];?>');">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewClasificationPadreHijoDetalle();">New Classificacion Padre Hijo Detalle</button> 
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb p-0 m-0">
                    <ol class="breadcrumb p-1 m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $arrClasificacionPadreHijoDetalle["nombreCla"]?></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $arrClasificacionPadreHijoDetalle["nombreClaPadre"]?></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $arrClasificacionPadreHijoDetalle["nombreClaPadreHijo"]?></a></li>
                    </ol>
                </nav>
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                
                <table class="table table-secondary table-hover" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Tag EN</th>
                            <th scope="col">Tag ES</th>
                            <th scope="col">Orden</th>
                            <th scope="col">Synonyms</th>
                            <th scope="col" style="width: 20%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        if( isset($arrClasificacionPadreHijoDetalle["ClaPadreHijoDetalle"]) && count($arrClasificacionPadreHijoDetalle["ClaPadreHijoDetalle"])  > 0 ){
                            
                            while( $rTMP = each($arrClasificacionPadreHijoDetalle["ClaPadreHijoDetalle"]) ){
                                ?>
                                <tr>
                                    <td><?php print $rTMP["value"]["nombre"]?></td>
                                    <td><?php print $rTMP["value"]["tag_en"]?></td>
                                    <td><?php print $rTMP["value"]["tag_es"]?></td>
                                    <td><?php print $rTMP["value"]["orden"]?></td>
                                    <td><?php print $rTMP["value"]["sinonimo"]?></td>
                                    <td class="text-right nowrap">
                                        <button onclick="fntDrawFormNewClasificationPadreHijoDetalle('<?php print $rTMP["value"]["id_clasificacion_padre_hijo_detalle"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                        <button onclick="fntDEliminarClasificacionPadreHijoDetalle('<?php print $rTMP["value"]["id_clasificacion_padre_hijo_detalle"]?>');" class="btn btn-danger btn-raised btn-sm">Eliminar</button>
                                    </td>
                                </tr>
                                <?php                                        
                            }
                            
                        }
                            
                        
                        ?>
                    </tbody>
                </table>

                
                
                
            </div>    
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            });
            
            function fntDrawFormNewClasificationPadreHijoDetalle(intClasificacionPadreHijoDetalle){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawFormNewClassificationPadreHijoDetalle=true&clasificacionPadreHijo=<?php print $arrClasificacionPadreHijoDetalle["id_clasificacion_padre_hijo"]?>&clasificacionPadreHijoDetalle="+intClasificacionPadreHijoDetalle, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });    
                                
            }
            
            function fntDEliminarClasificacionPadreHijoDetalle(intClasificacionPadreHijoDetalle){
                
                swal({
                    title: "Estas Seguro",
                    text: "Se Eliminaran todos los datos",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "<?php print $strAction?>?setEliminarClasificacionPadreHijoDetalle=true&key="+intClasificacionPadreHijoDetalle, 
                            success: function(result){
                                
                                fntDrawAdminClasificacionPadreHijoDetalle('<?php print $intClasificacionPadreHijo;?>');
                                
                            }
                            
                        });
                        
                    
                    } 
                });
                
                
            }
            
        </script>
        <?php    
        
        
    }
    
    public function fntDrawFormNewClassificacionPadreHijoDetalle($strAction, $intClasificacionPadreHijo, $intClasificacionPadreHijoDetalle){
        
        $arrClasificacionPadreHijoDetalle = $this->objModel->getClasificacionPadreHijoDetalle($intClasificacionPadreHijo, $intClasificacionPadreHijoDetalle);
        $strNombreClasificacion = $arrClasificacionPadreHijoDetalle["nombreCla"];
        $strNombreClasificacionPadre = $arrClasificacionPadreHijoDetalle["nombreClaPadre"];
        $strNombreClasificacionPadreHijo = $arrClasificacionPadreHijoDetalle["nombreClaPadreHijo"];
        $intClasificacionPadre = $arrClasificacionPadreHijoDetalle["id_clasificacion_padre"];
        $intClasificacionPadreHijo = $arrClasificacionPadreHijoDetalle["id_clasificacion_padre_hijo"];
        
        $arrClasificacionPadreHijo = isset($arrClasificacionPadreHijoDetalle["ClaPadreHijoDetalle"][$intClasificacionPadreHijoDetalle]) ? $arrClasificacionPadreHijoDetalle["ClaPadreHijoDetalle"][$intClasificacionPadreHijoDetalle] : array();
        
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntDrawAdminClasificacionPadreHijoDetalle('<?php print $intClasificacionPadreHijo;?>');">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveClasificacionPadreHijoDetalle();">Save</button>
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb p-0 m-0">
                    <ol class="breadcrumb p-1 m-0">
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $strNombreClasificacion?></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $strNombreClasificacionPadre?></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)" onclick=""><?php print $strNombreClasificacionPadreHijo?></a></li>
                    </ol>
                </nav>
            </div>    
        </div>
        
        <div class="row">
            <div class="col-6 offset-3 ">
                
                <form onsubmit="return false;" id="frmClasificacionPadreHijoDetalle" method="POST">
                    <input type="hidden" name="hidIdClassificationPadre" value="<?php print $intClasificacionPadre?>">
                    <input type="hidden" name="hidIdClassificationPadreHijo" value="<?php print $intClasificacionPadreHijo?>">
                    <input type="hidden" name="hidIdClassificationPadreHijoDetalle" value="<?php print $intClasificacionPadreHijoDetalle?>">
                    <div class="form-group">
                        <label for="txtNombre"><small>Nombre</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["nombre"]) ? $arrClasificacionPadreHijo["nombre"] : ""?>" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="txtTag_en"><small>Tag EN</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["tag_en"]) ? $arrClasificacionPadreHijo["tag_en"] : ""?>" class="form-control" id="txtTag_en" name="txtTag_en" placeholder="Tag EN" required>
                    </div>
                    <div class="form-group">
                        <label for="txtTag_es"><small>Tag ES</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["tag_es"]) ? $arrClasificacionPadreHijo["tag_es"] : ""?>" class="form-control" id="txtTag_es" name="txtTag_es" placeholder="Tag ES" required>
                    </div>
                    <div class="form-group">
                        <label for="txtOrden"><small>Orden</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["orden"]) ? $arrClasificacionPadreHijo["orden"] : ""?>" class="form-control" id="txtOrden" name="txtOrden" placeholder="Orden" required>
                    </div>
                    <div class="form-group">
                        <label for="txtSinonimo"><small>Synonyms</small></label>
                        <input type="text" value="<?php print isset($arrClasificacionPadreHijo["sinonimo"]) ? $arrClasificacionPadreHijo["sinonimo"] : ""?>" class="form-control" id="txtSinonimo" name="txtSinonimo" placeholder="Synonyms" required>
                    </div>
                  
                </form>     
                
            </div>    
        </div>
        <script>
             
            $( document ).ready(function() {
                
                
            });
            
            function fntSaveClasificacionPadreHijoDetalle(){
                
                var formData = new FormData(document.getElementById("frmClasificacionPadreHijoDetalle"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setClasificacionPadreHijoDetalle=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntDrawAdminClasificacionPadreHijoDetalle('<?php print $intClasificacionPadreHijo;?>');
                        
                    }
                            
                });
                
            }
                            
        </script>
        
        <?php
        
    }
    
                                        
}

?>
