<?php
 
class amenidad_model{
      
    var $objDBClass;
    
    public function __construct(){
        
        $this->objDBClass = new dbClass(); 
            
    }
    
    public function getobjDBClass(){
        return $this->objDBClass;
    }
    
    public function setCloseDB(){
        
        $this->objDBClass->db_close();
    
    }
    
    public function setAmenidad($strNombre, $strTagEN, $strTagES){
        
        $strQuery = "INSERT INTO amenidad(nombre, tag_en, tag_es)
                                  VALUES( '{$strNombre}', '{$strTagEN}', '{$strTagES}' )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setEditAmenidad($intClasification, $strNombre, $strTagEN, $strTagES){
        
        
        $strQuery = "UPDATE amenidad
                     SET    nombre = '{$strNombre}', 
                            tag_en = '{$strTagEN}',
                            tag_es = '{$strTagES}'
                     WHERE  id_amenidad = {$intClasification} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getAmenidad($intAmenidad = 0){
        
        $intAmenidad = intval($intAmenidad);
        
        $strFiltro = $intAmenidad ? "WHERE id_amenidad = {$intAmenidad}" : "";
        
        $strQuery = "SELECT id_amenidad, 
                            nombre,
                            tag_en,
                            tag_es
                     FROM   amenidad
                     {$strFiltro} ";
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_amenidad"]]["id_amenidad"] = $rTMP["id_amenidad"];
            $arrInfo[$rTMP["id_amenidad"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_amenidad"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_amenidad"]]["tag_es"] = $rTMP["tag_es"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setEliminarAmenidad($intAmenidad){
        
        $intAmenidad = intval($intAmenidad);
        
        $strQuery = "DELETE FROM amenidad WHERE id_amenidad = {$intAmenidad}";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
}

?>
