<?php

require_once "amenidad_model.php";

class amenidad_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new amenidad_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        $arrUsuario = $this->objModel->getAmenidad();
        
        ?>
        
         
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
            <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
       
            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-2  " id="divContenidoBody"  >
                    
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFomrAmenidad();">New Amenidad</button>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-12">
                            
                            <table class="table table-secondary table-hover" id="myTable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Tag EN</th>
                                        <th scope="col">Tag ES</th>
                                        <th scope="col" style="width: 20%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    while( $rTMP = each($arrUsuario) ){
                                        ?>
                                        <tr>
                                            <td><?php print $rTMP["value"]["nombre"]?></td>
                                            <td><?php print $rTMP["value"]["tag_en"]?></td>
                                            <td><?php print $rTMP["value"]["tag_es"]?></td>
                                            <td class="text-right nowrap">
                                                <button onclick="fntDrawFomrAmenidad('<?php print $rTMP["value"]["id_amenidad"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                                <button onclick="fntEliminarAmenidad('<?php print $rTMP["value"]["id_amenidad"]?>');" class="btn btn-danger btn-raised btn-sm">Eliminar</button>
                                            </td>
                                        </tr>
                                        <?php                                        
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>

                            
                            
                            
                        </div>    
                    </div>    
                    
                    
                                        
                </div> 
            </main>
               
          
        </div>    
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            }); 
            
            function fntDrawFomrAmenidad(intAmenidad){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawFomrAmenidad=true&amenidad="+intAmenidad, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }
                            
            function fntEliminarAmenidad(intAmenidad){
                
                swal({
                    title: "Estas Seguro",
                    text: "Se Eliminaran todos los datos",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "<?php print $strAction?>?setEliminarAmenidad=true&key="+intAmenidad, 
                            success: function(result){
                                
                                fntCargarPaginaActiva();
                                
                            }
                            
                        });
                        
                    
                    } 
                });
                
                
            }
                
        </script>        
        <?php 
        
    }
    
    public function fntDrawFomrAmenidad($strAction, $intAmenidad){
        
        $arrAmenidad = $this->objModel->getAmenidad($intAmenidad);
        $arrAmenidad = isset($arrAmenidad[$intAmenidad]) ? $arrAmenidad[$intAmenidad] : array();
        
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntCargarPaginaActiva();">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveAmenidad();">Save</button>
            </div>    
        </div>
        
        <div class="row">
            <div class="col-6 offset-3 ">
                
                <form onsubmit="return false;" id="frmAmenidad" method="POST">
                    <input type="hidden" name="hidIdAmenidad" value="<?php print $intAmenidad?>">
                    <div class="form-group">
                        <label for="txtNombre"><small>Nombre</small></label>
                        <input type="text" value="<?php print isset($arrAmenidad["nombre"]) ? $arrAmenidad["nombre"] : ""?>" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="txtTag_en"><small>Tag EN</small></label>
                        <input type="text" value="<?php print isset($arrAmenidad["tag_en"]) ? $arrAmenidad["tag_en"] : ""?>" class="form-control" id="txtTag_en" name="txtTag_en" placeholder="Tag EN" required>
                    </div>
                  <div class="form-group">
                        <label for="txtTag_es"><small>Tag ES</small></label>
                        <input type="text" value="<?php print isset($arrAmenidad["tag_es"]) ? $arrAmenidad["tag_es"] : ""?>" class="form-control" id="txtTag_es" name="txtTag_es" placeholder="Tag ES" required>
                    </div>
                  
                </form>     
                
            </div>    
        </div>
        <script>
             
            $( document ).ready(function() {
                
                
            });
            
            function fntSaveAmenidad(){
                
                var formData = new FormData(document.getElementById("frmAmenidad"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setAmenidad=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });
                
            }
                            
        </script>
        
        <?php
        
    }
     
}

?>
