<?php
  
require_once "amenidad_model.php";
require_once "amenidad_view.php";

class amenidad_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new amenidad_model();
        $this->objView = new amenidad_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["drawFomrAmenidad"]) ){
            
            $intAmenidad = isset($_GET["amenidad"]) ? trim($_GET["amenidad"]) : "";
            
            $this->objView->fntDrawFomrAmenidad($strAction, $intAmenidad);
            
            die();
            
        } 
        
        if( isset($_GET["setAmenidad"]) ){
            
            $intAmenidad = isset($_POST["hidIdAmenidad"]) ? intval($_POST["hidIdAmenidad"]) : 0;
            $strNombre = isset($_POST["txtNombre"]) ? addslashes($_POST["txtNombre"]) : "";
            $strTagEN = isset($_POST["txtTag_en"]) ? addslashes($_POST["txtTag_en"]) : "";
            $strTagES = isset($_POST["txtTag_es"]) ? addslashes($_POST["txtTag_es"]) : "";
            
            if( $intAmenidad ){
                
                $this->objModel->setEditAmenidad($intAmenidad, $strNombre, $strTagEN, $strTagES);
                    
            }
            else{
                
                $this->objModel->setAmenidad( $strNombre, $strTagEN, $strTagES);    
            
            }
            
            die();
        }
        
        if( isset($_GET["setEliminarAmenidad"]) ){
            
            $intAmenidad = isset($_GET["key"]) ? intval($_GET["key"]) : 0;
            
            $this->objModel->setEliminarAmenidad($intAmenidad);
            
            die();
        }
        
                     
    }
    
}

  
?>
