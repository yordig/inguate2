<?php

require_once "lugar_dato_model.php";

class lugar_dato_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new lugar_dato_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        
        ?>
         
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
            <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
            
            <script src="dist/html2canvas/html2canvas.js"></script>

            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-2  " id="divContenidoBody"  >
                    
                    <div class="row">
                        <div class="col-12">
                            
                            <button class="btn btn-primary btn-raised" onclick="fntShowNewCarga();">Nueva Carga</button>
                            <table class="table table-secondary table-hover" id="myTable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            
                        </div>    
                    </div>    
                                        
                </div> 
            </main>
            
            <div class="modal fade" id="mlSolicitudRechazo" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" style="" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Rechazo</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="divBodyModalMisUsuarios">

                            <input type="hidden" id="hidSolicitudModal" name="hidSolicitudModal" value="0">
                            <label for="txtMotivoRechazo">Motivo</label>
                            <textarea class="form-control" name="txtMotivoRechazo" id="txtMotivoRechazo" rows="3" placeholder="Escrbe el motivo de rechazo"></textarea>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-raised btn-sm mr-2" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-danger btn-raised btn-sm" onclick="fntRechazarSolProduto();" >Enviar Rechazo</button>
                        </div>
                    </div>
                </div>
            </div>          
          
        </div>    
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable();
                
                fntShowNewCarga();    
                    
            }); 
            
            function fntShowNewCarga(){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawShowNewCarga=true", 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                
                    
                    }
                });
                
            }
                
        </script>        
        <?php 
        
    }
    
    function drawShowNewCarga($strAction){
        
        $arrCatalogoUbicacion = fntUbicacion();
        $arrCatalogoUbicacionLugar = fntUbicacionLugar();
        
        ?>
        
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntCargarPaginaActiva();">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveClasificacion();">Save</button>
            
            </div>    
        </div>
        <div class="row justify-content-center">
                
            <div class="col-4 mt-2">
                <label for="txtTitulo"  id="lbtxtTextoBusqueda"><small>Ingresa tu Texto</small></label>
                <input type="text" value="" class="form-control form-control-sm   " id="txtTextoBusqueda" name="txtTextoBusqueda" placeholder="Ingresa tu Texto">

            </div>    
            <div class="col-4">
                <label for="slcUbicacionLugar" id="lbslcUbicacionLugar"><small>Region</small></label>
                <select class="custom-select custom-select-sm " name="slcUbicacionLugar" id="slcUbicacionLugar">
                    <option value="">Seleccione una opcion...</option>
                                
                    <?php
                    
                    while( $rTMP = each($arrCatalogoUbicacion) ){
                        ?>
                        <optgroup label="<?php print $rTMP["value"]["tag_es"]?>">
                            <?php
                            
                            while( $arrTMP = each($arrCatalogoUbicacionLugar[$rTMP["key"]]["lugar"]) ){
                                
                                ?>
                                
                                <option value="<?php print $rTMP["key"]."_".$arrTMP["key"]?>"><?php print $arrTMP["value"]["tag_es"]?></option>
                                
                                <?php
                                
                            }
                            
                            ?>
                        </optgroup>
                        <?php
                    }
                    
                    ?>
                        
                </select>         
            </div>
            <div class="col-12 text-center mt-2">

                <button class="btn btn-primary btn-raised btn-sm" onclick="fntBuscar();">Buscar</button>
            
            </div>    
        </div>
        
        <div class="row">
            <div class="col-12 mt-2" id="divContenidoBuscardd">
                <form name="frmGaleria" id="frmGaleria"  onsubmit="return false;" method="POST">
                    
                    <button onclick="fntRottate()">ddddd</button>
                    <button onclick="fntSave()">Saveee</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mt-2" style="" id="divContenidoBuscar">
            </div>
        </div>
        
         
        <script>
            
            var formDataFileRotate = new Array();
            var cropper = new Array();
                    
            $(document).ready(function() { 
                
                
            });
            
            function fntRottate(){
                
                var iframe = document.getElementById("imgTextGoogle");
                var elmnt = iframe.contentWindow.document.getElementsByTagName("img")[0];
                                     
                console.log(elmnt);
                console.log(elmnt.src);
                
            }   
                
            
            function fntSave(){
                
                var formDataPort = new FormData(document.getElementById("frmGaleria"));
                
                        
                $.ajax({
                    url: "<?php print $strAction?>?saveBuscar=true", 
                    type: "POST",
                    data: formDataPort,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                
                    }
                            
                });
                
            }
            
            function fntBuscar(){
                
                $.ajax({
                    url: "<?php print $strAction?>?drawBuscar=true&texto="+$("#txtTextoBusqueda").val()+"&ubicacion="+$("#slcUbicacionLugar").val(), 
                    success: function(result){
                        
                        $("#divContenidoBuscar").html(result);
                
                    
                    }
                });    
                
            }
            
        </script>
        
        <?php
                
    }
    
    public function drawBuscar($strAction, $strTexto, $intUbicacion){
        
        drawdebug($strTexto);
        drawdebug($intUbicacion);
        
    }
    
}

?>
