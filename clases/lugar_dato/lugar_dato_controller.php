<?php
  
require_once "lugar_dato_model.php";
require_once "lugar_dato_view.php";

class lugar_dato_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new lugar_dato_model();
        $this->objView = new lugar_dato_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["drawShowNewCarga"]) ){
            
            $this->objView->drawShowNewCarga($strAction);
            
            die();
        }
        
        if( isset($_GET["drawBuscar"]) ){
            
            $strTexto = isset($_GET["texto"]) ? $_GET["texto"] : "";
            $intUbicacion = isset($_GET["ubicacion"]) ? $_GET["ubicacion"] : "";
            
            $arrCatalogoUbicacionLugar = fntUbicacionLugar();
            $arrKeyUbicacion = explode("_", $intUbicacion);
            
            $strLat = $arrCatalogoUbicacionLugar[$arrKeyUbicacion[0]]["lugar"][$arrKeyUbicacion[1]]["lat"]; 
            $strLng = $arrCatalogoUbicacionLugar[$arrKeyUbicacion[0]]["lugar"][$arrKeyUbicacion[1]]["lng"]; 
            $strRadio = $arrCatalogoUbicacionLugar[$arrKeyUbicacion[0]]["lugar"][$arrKeyUbicacion[1]]["radio"]; 
            
            $strApiKey = fntGetApiKeyPlace();
            
            
            // Remote image URL
            $strLinkMap = "https://maps.googleapis.com/maps/api/place/photo?&key=".$strApiKey."&maxwidth=400&photoreference=";
            $strLinkMap .= "CmRaAAAA1AB1Iic_R7rll83RLg9wvWdijb5FWeomIqssp-bmkL9bfC3O-brH1-8GrsHZ-hv723E94l3sLKTujTLwAWk5EjZSC0IWrRSFSj_PzKeDMV5bxr76uGwL0Pv5Yy1IcJKxEhCJkwUHvdCooSba2v2XGFPtGhSuLg71yl2hx6WGA1G7To4v949D2Q";
            $strLinkMap = "https://lh3.googleusercontent.com/p/AF1QipOyQ_PKQFj6UUV8XoEwsGt2arEKILPWk1MqmjDZ=s1600-w400";
                   
            drawdebug($strLinkMap);           
            $url = $strLinkMap;

            // Image path
            $img = 'yordi_audi.jpg';

            // Save image
            $ch = curl_init($url);
            $fp = fopen($img, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
            
            die();
            
            
        
            
            $strToken = "";
            
            $boolCiclo = true;
            while( $boolCiclo ){
                
                $strUrlApi = "https://maps.googleapis.com/maps/api/place/textsearch/json?location=".$strLat.",".$strLng."&radius=".$strRadio."&key=".$strApiKey."&language=es&query=".$strTexto;
        
                $objJsonDatos = file_get_contents($strUrlApi);
                $objJsonDatos = json_decode($objJsonDatos);
        
                drawdebug($objJsonDatos);
                
                
                
                
                
                
                
                
                $boolCiclo = true;
                    
            }
        
        
            
                        
            //$this->objView->drawBuscar($strAction, $strTexto, $intUbicacion);
            
            die();
        }
        
        if( isset($_GET["saveBuscar"]) ){
            
            $strDataIMG = isset($_POST["txtBase64"]) ? addslashes($_POST["txtBase64"]) : 0;
            
            $base_to_php = explode(',', $strDataIMG);
                
            $data = base64_decode($base_to_php[1]);
            
            $filepath = "yordiiiii.png";
                
            file_put_contents($filepath, $data);
            
            chmod($filepath, 0777);
    
            
            die();
        }
                             
    }
    
}

  
?>
