<?php

require_once "registro_model.php";

class registro_view{
                
    var $objModel;  
                    
    public function __construct(){
        
        $this->objModel = new registro_model(); 
        
    }
    
    public function fntContentPage($strAction){
    
        $arrUsuario = $this->objModel->getUsuario();
        $arrTipoUsuario = $this->objModel->getTipoUsuario();
        $arrRolInguate = fntCatalogoRolInguate();
        $arrTraficoRegistro = fntCatalogoTraficoRegistro();
        
        ?>
        
         
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            <?php 
            fntDrawDrawerCore();
            ?>    
            
            <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
            <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>

            
            <link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
            <link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> <!-- for live demo page -->
            <script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>

            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content "  >
                <div class="container-fluid mt-2  " id="divContenidoBody"  >
                    
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewUser();">New User</button>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-12">
                            
                            <table class="table table-secondary table-hover" id="myTable">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Email</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Tipo</th>
                                        <th scope="col">Rol Inguate</th>
                                        <th scope="col">Fecha Creacion</th>
                                        <th scope="col">Trafico</th>
                                        <th scope="col" style="width: 20%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    while( $rTMP = each($arrUsuario) ){
                                        ?>
                                        <tr>
                                            <td><?php print $rTMP["value"]["email"]?></td>
                                            <td><?php print $rTMP["value"]["nombre"]?></td>
                                            <td><?php print $arrTipoUsuario[$rTMP["value"]["tipo"]]["nombre"]?></td>
                                            <td><?php print $arrRolInguate[$rTMP["value"]["rol_inguate"]]["nombre"]?></td>
                                            <td><?php print $rTMP["value"]["add_fecha"]?></td>
                                            <td><?php print $arrTraficoRegistro[$rTMP["value"]["trafico"]]["nombre"]?></td>
                                            
                                            <td class="text-right">
                                                <button onclick="fntDrawFormNewUser('<?php print $rTMP["value"]["id_usuario"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                                <button onclick="fntPlaceUser('<?php print $rTMP["value"]["id_usuario"]?>');" class="btn btn-success btn-raised btn-sm">Admin</button>
                                            </td>
                                        </tr>
                                        <?php                                        
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>

                            
                            
                            
                        </div>    
                    </div>    
                    
                    
                                        
                </div> 
            </main>
          
            <!-- Espacio para Modal  -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>    
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            }); 
            
            function fntDrawFormNewUser(intUsuario){
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawFormNewUser=true&usuario="+intUsuario, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }
            
            function fntPlaceUser(intUsuario){
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawAmdinPlaceUsuario=true&usuario="+intUsuario, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
                
            }
                   
        </script>        
        <?php 
        
    }
    
    public function fntDrawFormNewUser($strAction, $intUsuario){
        
        $arrTipoUsuario = $this->objModel->getTipoUsuario();
        $arrUsuario = $this->objModel->getUsuario($intUsuario);
        $arrUsuario = isset($arrUsuario[$intUsuario]) ? $arrUsuario[$intUsuario] : array();
        
        $arrCatalogoRolInguate = fntCatalogoRolInguate();
        
        ?>
        
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntCargarPaginaActiva();">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSaveUsuario();">Save</button>
            </div>    
        </div>
        <div class="row">
            <div class="col-4 offset-lg-4 ">
                       
                <form onsubmit="return false;" id="frmUsuario" method="POST">
                  <input type="hidden" name="hidIdUsuario" value="<?php print $intUsuario?>">
                  <div class="form-group">
                    <label for="txtEmail"><small>Email address</small></label>
                    <input type="email" value="<?php print isset($arrUsuario["email"]) ? $arrUsuario["email"] : ""?>" class="form-control" id="txtEmail" name="txtEmail" placeholder="Enter email" required>
                  </div>
                  <div class="form-group">
                    <label for="txtPassWord"><small>Password</small></label>
                    <input type="text" class="form-control" id="txtPassWord"  name="txtPassWord" placeholder="Password" required>
                  </div>
                  <div class="form-group">
                    <label for="txtNombre"><small>Nombre</small></label>
                    <input type="text" value="<?php print isset($arrUsuario["nombre"]) ? $arrUsuario["nombre"] : ""?>" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                  </div>  
                  <div class="form-group">
                    <label for="txtApellido"><small>Apellido</small></label>
                    <input type="text" value="<?php print isset($arrUsuario["apellido"]) ? $arrUsuario["apellido"] : ""?>" class="form-control" id="txtApellido" name="txtApellido" placeholder="Apellido" required>
                  </div>
                   
                  <div class="form-group">
                    <label for="slcTipo"><small>Tipo</small></label>
                    <select class="custom-select " name="slcTipo">
                        <?php
                        
                        while( $rTMP = each($arrTipoUsuario) ){
                            $strSelected = $rTMP["key"] == ( isset($arrUsuario["tipo"]) ? $arrUsuario["tipo"] : "2" ) ? "selected" : "";
                            ?>
                            <option <?php print $strSelected;?> value="<?php print $rTMP["key"]?>"><?php print $rTMP["value"]["nombre"]?></option>
                            <?php
                        }
                        
                        ?>
                            
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <label for="slcRolInguate"><small>Rol Inguate</small></label>
                    
                    <select class="custom-select " name="slcRolInguate">
                    
                        <?php
                        
                        while( $rTMP = each($arrCatalogoRolInguate) ){
                            
                            $strSelected = $rTMP["key"] == ( isset($arrUsuario["rol_inguate"]) ? $arrUsuario["rol_inguate"] : "0" ) ? "selected" : "";
                            
                            ?>
                            
                            <option <?php print $strSelected;?> value="<?php print $rTMP["key"]?>"><?php print $rTMP["value"]["nombre"]?></option>
                            
                            <?php
                            
                        }
                        
                        ?>                    
                        
                    </select>
                    
                    <br>
                    
                    <input type="text" value="<?php print isset($arrUsuario["codigo"]) ? $arrUsuario["codigo"] : ""?>" class="form-control" id="txtCodigoVendedor" name="txtCodigoVendedor" placeholder="Codigo Vendedor" >
                  
                  </div>
                  
                </form>
                
            </div>    
        </div>
        <script>
            
            function fntSaveUsuario(){
                
                var formData = new FormData(document.getElementById("frmUsuario"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setUsuarioRegistro=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntCargarPaginaActiva();
                        
                    }
                            
                });
                
            }
            
        </script>
        
        <?php
        
    }
    
    public function fntDrawAmdinPlaceUsuario($strAction, $intUsuario){
    
        $arrPlace = $this->objModel->getUsuarioPlace($intUsuario);
                
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntCargarPaginaActiva();">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntDrawFormNewPlace('0');">New Place</button>
            </div>    
        </div>
        <div class="row">
            <div class="col-12">
                
                <table class="table table-secondary table-hover" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Titulo</th>
                            <th scope="col">Config Galeria</th>
                            <th scope="col">Estado</th>
                            <th scope="col" style="width: 20%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        while( $rTMP = each($arrPlace) ){
                            ?>
                            <tr>
                                <td><?php print $rTMP["value"]["titulo"]?></td>
                                <td><?php print $rTMP["value"]["num_galeria"]?></td>
                                <td><?php print $rTMP["value"]["estado"]?></td>
                                <td class="text-right">
                                    <button onclick="fntDrawFormNewPlace('<?php print $rTMP["value"]["id_place"]?>');" class="btn btn-primary btn-raised btn-sm">Edit</button>
                                    <button onclick="fntDrawFormNewPlaceProduct('<?php print $rTMP["value"]["id_place"]?>');" class="btn btn-primary btn-raised btn-sm">Product</button>
                                </td>
                            </tr>
                            <?php                                        
                        }
                        
                        ?>
                    </tbody>
                </table>
                
            </div>    
        </div>
        <script>
            $(document).ready(function() { 
                
                $('#myTable').DataTable();    
                    
            }); 
            
            function fntDrawFormNewPlace(intPlace){
                
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawFormNewPlace=true&usuario=<?php print $intUsuario?>&place="+intPlace, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }        
                
            function fntDrawFormNewPlaceProduct(intPlace){
                
                $.ajax({
                    url: "<?php print $strAction?>?fntDrawFormNewPlaceProduct=true&usuario=<?php print $intUsuario?>&place="+intPlace, 
                    success: function(result){
                        
                        $("#divContenidoBody").html(result);
                    
                    }
                });
                        
            }        
                   
        </script>
        <?php
        
    }
    
    public function fntDrawFormNewPlace($strAction, $intUsuario, $intPlace = 0){
        
        $arrClasificacion = $this->objModel->getClasificacion();
        $arrInfoPlace = $this->objModel->getUsuarioPlace($intUsuario, $intPlace);
        $arrInfoPlace = isset($arrInfoPlace[$intPlace]) ? $arrInfoPlace[$intPlace] : array();
        
        $arrPlaceClasificacion = $this->objModel->getPlaceClasificacion($intPlace);
                
        ?>
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntPlaceUser('<?php print $intUsuario?>');">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSavePlace();">Save</button>
            </div>    
        </div>  
        <div class="row">
            <div class="col-4 offset-lg-4 ">
                       
                <form onsubmit="return false;" id="frmPlace" method="POST">
                  <input type="hidden" name="hidIdUsuario" value="<?php print $intUsuario?>">
                  <input type="hidden" name="hidIdPlace" value="<?php print $intPlace?>">
                  <div class="form-group">
                    <label for="txtTitulo"><small>Titulo</small></label>
                    <input type="text" value="<?php print isset($arrInfoPlace["titulo"]) ? $arrInfoPlace["titulo"] : ""?>" class="form-control" id="txtTitulo" name="txtTitulo" placeholder="Titulo" >
                  
                  </div>   
                  <div class="form-group">
                    <label for="txtCantidadGaleria"><small>Cantidad de Galeria</small></label>
                    <input type="text" value="<?php print isset($arrInfoPlace["num_galeria"]) ? $arrInfoPlace["num_galeria"] : ""?>" class="form-control" id="txtCantidadGaleria" name="txtCantidadGaleria" placeholder="Cantidad de Galeria" >
                  </div>    
                  <div class="form-group">
                    <label for="txtCantidadProductos"><small>Cantidad de Producto</small></label>
                    <input type="text" value="<?php print isset($arrInfoPlace["num_producto"]) ? $arrInfoPlace["num_producto"] : ""?>" class="form-control" id="txtCantidadProductos" name="txtCantidadProductos" placeholder="Cantidad de Galeria" >
                  </div> 
                  
                  <div class="row">
                    <div class="col-12 ">
                    
                        <div class="form-group">
                            <label for="slcClasificacionPrincipal"><small>Main classification</small></label>
                            <div class="input-group">
                                <select id="slcClasificacionPrincipal" name="slcClasificacionPrincipal[]" class="form-control-sm select2-multiple" multiple>
                                    <?php
                                
                                    while( $rTMP = each($arrClasificacion) ){
                                        
                                        $strSelected = isset($arrPlaceClasificacion[$rTMP["key"]]) ? " selected " : "";                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $rTMP["value"]["id_clasificacion"]?>"><?php print $rTMP["value"]["tag_en"]?></option>
                                        <?php
                                        
                                    }
                                    
                                    ?>
                                </select>
                                <button class="btn btn-raised btn-sm btn-success ml-1" id="btnNextClasificacionPrincipal" onclick="fntDrawClas2();">Next</button>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                                        
                <div id="divClasificacionSecundaria"></div>                        
                <div id="divClasificacionDetalle"></div>                        
                <div id="divClasificacionDetalleOtros"></div>                        
                 
                  
                </form>
                
            </div>    
        </div>
        <script>
        
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPrincipal").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#slcClasificacionPrincipal").change(function (){
                    
                    $("#btnNextClasificacionPrincipal").show();
                    $("#divClasificacionSecundaria").html("");
                    $("#divClasificacionDetalle").html("");
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                <?php
                
                if( $intPlace ){
                    ?>
                    fntDrawClas2();
                    <?php
                }
                
                ?>
                
            });
            
            function fntSavePlace(){
                
                var formData = new FormData(document.getElementById("frmPlace"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setSavePlace=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntPlaceUser('<?php print $intUsuario?>');
                        
                    }
                            
                });
                
            }
            
            function fntDrawClas2(){
                             
                strClass1 = "";
                $("#slcClasificacionPrincipal option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    
                });          
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                }    
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas2=true&place=<?php print $intPlace;?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionSecundaria").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            
        </script>
        <?php
                
    }
    
    public function fntDrawFormNewPlaceProduct($strAction, $intUsuario, $intPlace = 0){
        
        
        $arrPlaceClasificacionPadre = $this->objModel->getPlaceClasificacionPadre($intPlace);
        
                
        ?>
                
        <div class="row">
            <div class="col-12">
                <button class="btn btn-secondary btn-raised btn-sm" onclick="fntPlaceUser('<?php print $intUsuario?>');">Back</button>
                <button class="btn btn-primary btn-raised btn-sm" onclick="fntSavePlaceProducto();">Save</button>
            </div>    
        </div>  
        <div class="row">
            <div class="col-6 offset-3 ">
                        
                <table class="table table-secondary table-hover" style="width: 100%;" >
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" style="width: 40%;">Main classification</th>
                            <th scope="col" style="width: 10%;">Quantity Of Products</th>
                        </tr>
                    </thead>
                    <tbody>
                
                        <?php
                        
                        $intCount = 1;
                        while( $rTMP = each($arrPlaceClasificacionPadre) ){
                            ?>
                            <tr>
                                <td><?php print $rTMP["value"]["tag_en"]?></td>
                                <td>
                                    
                                    <input value="<?php print $rTMP["key"]?>" type="hidden" id="hidClasificacionPadre_<?php print $intCount;?>" name="hidClasificacionPadre_<?php print $intCount;?>"> 
                                    <input value="<?php print $rTMP["value"]["producto"]?>" type="text" class="form-control" id="txtProducto_<?php print $intCount;?>" name="txtProducto_<?php print $intCount;?>" onchange="fntMantenerDecimales(this, true)">
              
                                    
                                </td>
                            </tr>
                            <?php                                        
                            $intCount++;
                        }
                        
                        ?>
                          
                    </tbody>
                </table>  
                
            </div>    
        </div>
       
        <script>
        
            $( document ).ready(function() {
                
            });
            
            function fntSavePlaceProducto(){
                
                var formData = new FormData();
                
                formData.append("txtPlace", "<?php print $intPlace;?>");
                    
                $(":input[id^=hidClasificacionPadre_]").each(function (){
                    
                    arrExplode = this.id.split("_");
                    
                    formData.append("hidClasificacionPadre_"+arrExplode[1], $("#hidClasificacionPadre_"+arrExplode[1]).val());
                    formData.append("txtProducto_"+arrExplode[1], $("#txtProducto_"+arrExplode[1]).val());
                    
                });
                
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "<?php print $strAction?>?setSavePlaceClasificacionProducto=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        fntPlaceUser('<?php print $intUsuario?>');
                        
                    }
                            
                });
                
            }
                     
        </script>
        <?php
                
    }
    
    public function fntDrawClas2($strAction, $strKey, $intPlace = 0){
        
        $arrInfo = $this->objModel->getClasificacionPadre($strKey);
        $arrPlaceClasificacionPadre = $this->objModel->getPlaceClasificacionPadre($intPlace);
        
        ?>
        
        <div class="row">
            <div class="col-12">
            
                <div class="form-group">
                    <label for="slcClasificacionPadre"><small>Secondary Classification</small></label>
                    <div class="input-group">
                        <select id="slcClasificacionPadre" name="slcClasificacionPadre[]" class="form-control-sm select2-multiple" multiple>
                            <?php
                        
                            while( $rTMP = each($arrInfo) ){
                                
                                ?>
                                <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                    <?php
                                    
                                    while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                        
                                        $strSelected = isset($arrPlaceClasificacionPadre[$arrTMP["key"]]) ? " selected " : "";                                        
                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre"]?>"><?php print $arrTMP["value"]["tag_enPadre"]?></option>
                                
                                        <?php
                                    }
                                    
                                    ?>
                                </optgroup>
                                <?php
                                
                            }
                            
                            ?>
                        </select>
                        <button class="btn btn-raised btn-sm btn-success ml-1" id="btnNextClasificacionPrincipalPadre" onclick="fntDrawClas3();">Next</button>
                    </div>
                </div>
                
            </div>
            
        </div>
        <script>
            
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPadre").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#btnNextClasificacionPrincipal").hide();
                
                $("#slcClasificacionPadre").change(function (){
                    
                    $("#btnNextClasificacionPrincipalPadre").show();
                    $("#divClasificacionDetalle").html("");
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                <?php
                
                if( $intPlace ){
                    ?>
                    fntDrawClas3();
                    <?php
                }
                
                ?>
                
            });
            
            function fntDrawClas3(){
                             
                strClass1 = "";
                $("#slcClasificacionPadre option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    
                });          
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                } 
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas3=true&place=<?php print $intPlace?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionDetalle").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            
        </script>
        
        <?php        
        
    }
    
    public function fntDrawClas3($strAction, $strKey, $intPlace = 0){
        
        $arrInfo = $this->objModel->getClasificacionPadreHijo($strKey);
        $arrPlaceClasificacionPadreHijo = $this->objModel->getPlaceClasificacionPadreHijo($intPlace);
        
        ?>
        
        <div class="row">
            <div class="col-12">
            
                <div class="form-group">
                    <label for="slcClasificacionPadreHijo"><small>Detail Classification</small></label>
                    <div class="input-group">
                        <select id="slcClasificacionPadreHijo" name="slcClasificacionPadreHijo[]" class="form-control-sm select2-multiple" multiple>
                            <?php
                        
                            while( $rTMP = each($arrInfo) ){
                                
                                ?>
                                <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                    <?php
                                    
                                    while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                        $strSelected = isset($arrPlaceClasificacionPadreHijo[$arrTMP["key"]]) ? " selected " : "";                                        
                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo"]?>"><?php print $arrTMP["value"]["tag_enHijo"]?></option>
                                
                                        <?php
                                    }
                                    
                                    ?>
                                </optgroup>
                                <?php
                                
                            }
                            
                            ?>
                        </select>
                        <button class="btn btn-raised btn-sm btn-success ml-1" id="btnNextClasificacionPrincipalPadreHijo" onclick="fntDrawClas4();">Next</button>
                    </div>
                </div>
                
            </div>
            
        </div>
        <script>
            
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPadreHijo").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#btnNextClasificacionPrincipalPadre").hide();
                
                $("#slcClasificacionPadreHijo").change(function (){
                    
                    $("#btnNextClasificacionPrincipalPadreHijo").show();
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                
                <?php
                
                if( $intPlace ){
                    ?>
                    fntDrawClas4();
                    <?php
                }
                
                ?>
                
            });
            
            function fntDrawClas4(){
                             
                strClass1 = "";
                $("#slcClasificacionPadreHijo option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    
                });          
                
                if( strClass1 == "" ){
                    
                    $.snackbar({
                        content: "Select An Option", 
                        timeout: 1000
                    }); 
                    
                    return false;   
                    
                } 
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "<?php print $strAction?>?getDrawClas4=true&place=<?php print $intPlace?>", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionDetalleOtros").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            
        </script>
        
        <?php        
        
    }
    
    public function fntDrawClas4($strAction, $strKey, $intPlace = 0){
        
        $arrInfo = $this->objModel->getClasificacionPadreHijoDetalle($strKey);
        $arrPlaceClasificacionPadreHijoDetalle = $this->objModel->getPlaceClasificacionPadreHijoDetalle($intPlace);
        
        
        ?>
        
        <div class="row">
            <div class="col-12">
            
                <div class="form-group">
                    <label for="slcClasificacionPadreHijo"><small>Other Classification</small></label>
                    <div class="input-group">
                        <select id="slcClasificacionPadreHijoDetalle" name="slcClasificacionPadreHijoDetalle[]" class="form-control-sm select2-multiple" multiple>
                            <?php
                        
                            while( $rTMP = each($arrInfo) ){
                                
                                ?>
                                <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                    <?php
                                    
                                    while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                        $strSelected = isset($arrPlaceClasificacionPadreHijoDetalle[$arrTMP["key"]]) ? " selected " : "";                                        
                                        
                                        ?>
                                        <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo_detalle"]?>"><?php print $arrTMP["value"]["tag_enDetalle"]?></option>
                                
                                        <?php
                                    }
                                    
                                    ?>
                                </optgroup>
                                <?php
                                
                            }
                            
                            ?>
                        </select>
                    </div>
                </div>
                
            </div>
            
        </div>
        <script>
            
            $( document ).ready(function() {
                
                
                $("#slcClasificacionPadreHijoDetalle").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#btnNextClasificacionPrincipalPadreHijo").hide();
                
                
            });
            
            
        </script>
        
        <?php        
        
    }
                                        
}

?>
