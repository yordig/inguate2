<?php
  
require_once "registro_model.php";
require_once "registro_view.php";

class registro_controller{
    
    var $objModel;
    var $objView;
                    
    public function __construct(){
        
        $this->objModel = new registro_model();
        $this->objView = new registro_view();  
        
    }
    
    public function drawContentPage($strAction){
              
        $this->objView->fntContentPage($strAction);    
                
    }
                      
    public function getAjax($strAction){
        
        if( isset($_GET["fntDrawFormNewUser"]) ){
            
            $intUsuario = isset($_GET["usuario"]) ? trim($_GET["usuario"]) : "";
            
            $this->objView->fntDrawFormNewUser($strAction, $intUsuario);
            
            die();
            
        }
        
        if( isset($_GET["setUsuarioRegistro"]) ){
            
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : "";
            $strEmail = isset($_POST["txtEmail"]) ? trim($_POST["txtEmail"]) : "";
            $strPassWord = isset($_POST["txtPassWord"]) ? trim($_POST["txtPassWord"]) : "";
            $strNombre = isset($_POST["txtNombre"]) ? trim($_POST["txtNombre"]) : "";
            $strApellido = isset($_POST["txtApellido"]) ? trim($_POST["txtApellido"]) : "";
            $strTipo = isset($_POST["slcTipo"]) ? trim($_POST["slcTipo"]) : "";
            $intRolInguate = isset($_POST["slcRolInguate"]) ? intval($_POST["slcRolInguate"]) : "";
            $strcodigoVendedor = isset($_POST["txtCodigoVendedor"]) ? trim($_POST["txtCodigoVendedor"]) : "";
                        
            
            if( !$intUsuario ){
                
                $intUsuario = $this->objModel->setUsuario($strEmail, $strPassWord, $strNombre, $strApellido, $strTipo, $intRolInguate);            
                
            }
            else{
                
                $this->objModel->setEditUsuario($intUsuario, $strEmail, $strPassWord, $strNombre, $strApellido, $strTipo, $intRolInguate);            
                
            }
            
            if( $intUsuario ){
                
                $this->objModel->setCodigoVendedor($intUsuario, $strcodigoVendedor);
                
            }
            
            die();
            
        }
        
        if( isset($_GET["fntDrawAmdinPlaceUsuario"]) ){
            
            $intUsuario = isset($_GET["usuario"]) ? trim($_GET["usuario"]) : "";
            
            $this->objView->fntDrawAmdinPlaceUsuario($strAction, $intUsuario);
            
            die();
            
        }
        
        if( isset($_GET["fntDrawFormNewPlace"]) ){
            
            $intUsuario = isset($_GET["usuario"]) ? trim($_GET["usuario"]) : "";
            $intPlace = isset($_GET["place"]) ? trim($_GET["place"]) : "";
            
            $this->objView->fntDrawFormNewPlace($strAction, $intUsuario, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["fntDrawFormNewPlaceProduct"]) ){
            
            $intUsuario = isset($_GET["usuario"]) ? trim($_GET["usuario"]) : "";
            $intPlace = isset($_GET["place"]) ? trim($_GET["place"]) : "";
            
            $this->objView->fntDrawFormNewPlaceProduct($strAction, $intUsuario, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["fntDrawFormNewPlace"]) ){
            
            $intPlace = isset($_GET["place"]) ? trim($_GET["place"]) : "";
            
            $this->objView->fntDrawFormPlace($strAction, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["getDrawClas2"]) ){
            
            $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : "";
            
            $this->objView->fntDrawClas2($strAction, $strKey, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["getDrawClas3"]) ){
            
            $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : "";
            
            $this->objView->fntDrawClas3($strAction, $strKey, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["getDrawClas4"]) ){
            
            $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
            $intPlace = isset($_GET["place"]) ? intval($_GET["place"]) : "";
            
            $this->objView->fntDrawClas4($strAction, $strKey, $intPlace);
            
            die();
            
        }
        
        if( isset($_GET["setSavePlace"]) ){
            
            $intUsuario = isset($_POST["hidIdUsuario"]) ? intval($_POST["hidIdUsuario"]) : 0;
            $intPlace = isset($_POST["hidIdPlace"]) ? intval($_POST["hidIdPlace"]) : 0;
            $strTitulo = isset($_POST["txtTitulo"]) ? addslashes($_POST["txtTitulo"]) : "";
            $intCantidadProducto = isset($_POST["txtCantidadProductos"]) ? intval($_POST["txtCantidadProductos"]) : "";
            $intCantidadGaleria = isset($_POST["txtCantidadGaleria"]) ? intval($_POST["txtCantidadGaleria"]) : "";
            
            if( !$intPlace ){
                
                $intPlace = $this->objModel->setPlace($intUsuario, $strTitulo, $intCantidadProducto, $intCantidadGaleria);
            }
            else{
                
                $this->objModel->setEditPlace($intPlace, $strTitulo, $intCantidadProducto, $intCantidadGaleria);
                
            }
            
            $this->objModel->setDeletePlaceClasificacion($intPlace);
            if( isset($_POST["slcClasificacionPrincipal"]) && is_array($_POST["slcClasificacionPrincipal"]) && count($_POST["slcClasificacionPrincipal"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPrincipal"]) ){
                    
                    $this->objModel->setPlaceClasificacion($intPlace, $rTMP["value"]);
                    
                }
                
            }
            
            $this->objModel->setDeletePlaceClasificacionPadre($intPlace);
            if( isset($_POST["slcClasificacionPadre"]) && is_array($_POST["slcClasificacionPadre"]) && count($_POST["slcClasificacionPadre"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPadre"]) ){
                    
                    $this->objModel->setPlaceClasificacionPadre($intPlace, $rTMP["value"]);
                    
                }
                
            }
            
            $this->objModel->setDeletePlaceClasificacionPadreHijo($intPlace);
            if( isset($_POST["slcClasificacionPadreHijo"]) && is_array($_POST["slcClasificacionPadreHijo"]) && count($_POST["slcClasificacionPadreHijo"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPadreHijo"]) ){
                    
                    $this->objModel->setPlaceClasificacionPadreHijo($intPlace, $rTMP["value"]);
                    
                }
                
            }
            
            
            $this->objModel->setDeletePlaceClasificacionPadreHijoDetalle($intPlace);
            if( isset($_POST["slcClasificacionPadreHijoDetalle"]) && is_array($_POST["slcClasificacionPadreHijoDetalle"]) && count($_POST["slcClasificacionPadreHijoDetalle"]) > 0 ){
                
                while( $rTMP = each($_POST["slcClasificacionPadreHijoDetalle"]) ){
                    
                    $this->objModel->setPlaceClasificacionPadreHijoDetalle($intPlace, $rTMP["value"]);
                    
                }
                
            }
            
            die();
        }
        
        if( isset($_GET["setSavePlaceClasificacionProducto"]) ){
            
            $intPlace = isset($_POST["txtPlace"]) ? intval($_POST["txtPlace"]) : 0;
            
            while( $rTMP = each($_POST) ){
                
                $arrExplode = explode("_", $rTMP["key"]);
                
                if( $arrExplode[0] == "hidClasificacionPadre" ){
                    
                    $intPlaceClasificacionPadre = isset($_POST["hidClasificacionPadre_{$arrExplode[1]}"]) ? intval($_POST["hidClasificacionPadre_{$arrExplode[1]}"]) : 0;
                    $intProducto = isset($_POST["txtProducto_{$arrExplode[1]}"]) ? intval($_POST["txtProducto_{$arrExplode[1]}"]) : 0;
                    
                    
                    $this->objModel->setProductoClasificacionPadrePlace($intPlace, $intPlaceClasificacionPadre, $intProducto);
                    
                }
                
            }
                        
            die();
        }
                     
    }
    
}

  
?>
