<?php                                                             
 
class registro_model{
      
    var $objDBClass;
    
    public function __construct(){
        
        $this->objDBClass = new dbClass(); 
            
    }
    
    public function getobjDBClass(){
        return $this->objDBClass;
    }
    
    public function setCloseDB(){
        
        $this->objDBClass->db_close();
    
    }
    
    public function setUsuario($strEmail, $strPassWord, $strNombre, $strApellido, $strTipo, $intRolInguate){
        
        $strPassWord = md5($strPassWord);
        $strQuery = "INSERT INTO usuario(email, clave, nombre, apellido, estado, tipo, lenguaje,
                                          rol_inguate  )
                                  VALUES( '{$strEmail}', '{$strPassWord}', '{$strNombre}', '{$strApellido}', 'A', '{$strTipo}', 'es',
                                          {$intRolInguate}  )";
        $this->objDBClass->db_consulta($strQuery);
        
        
        return $this->objDBClass->db_last_id();        
        
    }
    
    public function setEditUsuario($intUsuario, $strEmail, $strPassWord, $strNombre, $strApellido, $strTipo, $intRolInguate){
        
        $strUpdateClave = !empty($strPassWord) ? " ,clave = '".md5($strPassWord)."' " : "";
        
        $strQuery = "UPDATE usuario
                     SET    nombre = '{$strNombre}', 
                            apellido = '{$strApellido}',
                            tipo = '{$strTipo}',
                            rol_inguate = '{$intRolInguate}',
                            email = '{$strEmail}'
                            {$strUpdateClave}
                     WHERE  id_usuario = {$intUsuario} 
                     ";
        $this->objDBClass->db_consulta($strQuery);
        
        
    }
    
    public function getUsuario($intUsuario = 0){
        
        $intUsuario = intval($intUsuario);
        
        $strFiltro = $intUsuario ? "WHERE usuario.id_usuario = {$intUsuario}" : "";
                                     
        $strQuery = "SELECT usuario.id_usuario, 
                            usuario.email, 
                            usuario.clave, 
                            usuario.nombre, 
                            usuario.apellido, 
                            usuario.estado, 
                            usuario.tipo, 
                            usuario.lenguaje,
                            usuario.add_fecha,
                            usuario.rol_inguate,
                            usuario.trafico,
                            usuario_vendedor.codigo
                     FROM   usuario
                                LEFT JOIN usuario_vendedor
                                    ON usuario.id_usuario = usuario_vendedor.id_usuario
                     {$strFiltro} ";
        $arrInfo = array();
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_usuario"]]["id_usuario"] = $rTMP["id_usuario"];
            $arrInfo[$rTMP["id_usuario"]]["email"] = $rTMP["email"];
            $arrInfo[$rTMP["id_usuario"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_usuario"]]["apellido"] = $rTMP["apellido"];
            $arrInfo[$rTMP["id_usuario"]]["estado"] = $rTMP["estado"];
            $arrInfo[$rTMP["id_usuario"]]["tipo"] = $rTMP["tipo"];
            $arrInfo[$rTMP["id_usuario"]]["lenguaje"] = $rTMP["lenguaje"];
            $arrInfo[$rTMP["id_usuario"]]["add_fecha"] = $rTMP["add_fecha"];
            $arrInfo[$rTMP["id_usuario"]]["rol_inguate"] = $rTMP["rol_inguate"];
            $arrInfo[$rTMP["id_usuario"]]["codigo"] = $rTMP["codigo"];
            $arrInfo[$rTMP["id_usuario"]]["trafico"] = $rTMP["trafico"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getTipoUsuario(){
        
        $arr[1]["nombre"] = "Admin";
        $arr[2]["nombre"] = "Usuario con Negocio";
        $arr[3]["nombre"] = "Usuario";
        
        return $arr;
    }
    
    public function getUsuarioPlace($intUsuario, $intPlace = 0){
        
        $intUsuario = intval($intUsuario);
        $intPlace = intval($intPlace);
        
        $strFiltro = $intPlace ? "AND   id_place = {$intPlace} " : "";
        
        $strQuery = "SELECT id_place,
                            titulo,
                            num_producto,
                            num_galeria,
                            estado
                     FROM   place   
                     WHERE  id_usuario = {$intUsuario}
                     {$strFiltro} ";    
        
        $arrPlace = array();
        
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrPlace[$rTMP["id_place"]]["id_place"] = $rTMP["id_place"];
            $arrPlace[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
            $arrPlace[$rTMP["id_place"]]["num_producto"] = $rTMP["num_producto"];
            $arrPlace[$rTMP["id_place"]]["num_galeria"] = $rTMP["num_galeria"];
            $arrPlace[$rTMP["id_place"]]["estado"] = $rTMP["estado"];
                
        }
        
        $this->objDBClass->db_free_result($qTMP);
        return $arrPlace;
                        
    }
    
    public function getClasificacion(){
        
        $strQuery = "SELECT id_clasificacion,
                            nombre, 
                            tag_en,
                            tag_es
                     FROM   clasificacion
                     ";
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrClasificacion[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
            $arrClasificacion[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
            
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrClasificacion;
        
    }
    
    public function getClasificacionPadre($strKey){
        
        $strQuery = "SELECT clasificacion.nombre,
                            clasificacion.id_clasificacion,
                            clasificacion.tag_en,
                            clasificacion.tag_es,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre.nombre nombrePadre,
                            clasificacion_padre.tag_en tag_enPadre,
                            clasificacion_padre.tag_es tag_esPadre
                     FROM   clasificacion,
                            clasificacion_padre
                     WHERE  clasificacion.id_clasificacion IN ({$strKey})
                     AND    clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
            $arrInfo[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["nombrePadre"] = $rTMP["nombrePadre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_enPadre"] = $rTMP["tag_enPadre"];
            $arrInfo[$rTMP["id_clasificacion"]]["detalle"][$rTMP["id_clasificacion_padre"]]["tag_esPadre"] = $rTMP["tag_esPadre"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getClasificacionPadreHijo($strKey){
        
        $strQuery = "SELECT clasificacion_padre.nombre,
                            clasificacion_padre.id_clasificacion_padre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es,
                            
                            clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                            clasificacion_padre_hijo.nombre nombreHijo,
                            clasificacion_padre_hijo.tag_en tag_enHijo,
                            clasificacion_padre_hijo.tag_es tag_esHijo
                            
                     FROM   clasificacion_padre,
                            clasificacion_padre_hijo
                     WHERE  clasificacion_padre.id_clasificacion_padre IN ({$strKey})
                     AND    clasificacion_padre.id_clasificacion_padre = clasificacion_padre_hijo.id_clasificacion_padre 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["nombreHijo"] = $rTMP["nombreHijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_enHijo"] = $rTMP["tag_enHijo"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["detalle"][$rTMP["id_clasificacion_padre_hijo"]]["tag_esHijo"] = $rTMP["tag_esHijo"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function getClasificacionPadreHijoDetalle($strKey){
        
        $strQuery = "SELECT clasificacion_padre_hijo.nombre,
                            clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                            clasificacion_padre_hijo.tag_en,
                            clasificacion_padre_hijo.tag_es,
                            
                            clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle,
                            clasificacion_padre_hijo_detalle.nombre nombreDetalle,
                            clasificacion_padre_hijo_detalle.tag_en tag_enDetalle,
                            clasificacion_padre_hijo_detalle.tag_es tag_esDetalle
                            
                     FROM   clasificacion_padre_hijo,
                            clasificacion_padre_hijo_detalle
                     WHERE  clasificacion_padre_hijo.id_clasificacion_padre_hijo IN ({$strKey})
                     AND    clasificacion_padre_hijo.id_clasificacion_padre_hijo = clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo 
                     ";
        $arrInfo = array();             
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["id_clasificacion_padre_hijo_detalle"] = $rTMP["id_clasificacion_padre_hijo_detalle"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["nombreDetalle"] = $rTMP["nombreDetalle"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_enDetalle"] = $rTMP["tag_enDetalle"];
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]]["detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]]["tag_esDetalle"] = $rTMP["tag_esDetalle"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
    }
    
    public function setPlaceClasificacion($intPlace, $intPlaceClasificacion){
        
        $strQuery = "INSERT INTO place_clasificacion (id_place, id_clasificacion)
                                                VALUES( {$intPlace}, {$intPlaceClasificacion} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setDeletePlaceClasificacion($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlaceClasificacionPadre($intPlace, $intPlaceClasificacionPadre){
        
        $strQuery = "INSERT INTO place_clasificacion_padre (id_place, id_clasificacion_padre)
                                                VALUES( {$intPlace}, {$intPlaceClasificacionPadre} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setDeletePlaceClasificacionPadre($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion_padre WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlaceClasificacionPadreHijo($intPlace, $intPlaceClasificacionPadreHijo){
        
        $strQuery = "INSERT INTO place_clasificacion_padre_hijo (id_place, id_clasificacion_padre_hijo)
                                                VALUES( {$intPlace}, {$intPlaceClasificacionPadreHijo} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setDeletePlaceClasificacionPadreHijo($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion_padre_hijo WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlaceClasificacionPadreHijoDetalle($intPlace, $intPlaceClasificacionPadreHijo){
        
        $strQuery = "INSERT INTO place_clasificacion_padre_hijo_detalle (id_place, id_clasificacion_padre_hijo_detalle)
                                                VALUES( {$intPlace}, {$intPlaceClasificacionPadreHijo} )";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setDeletePlaceClasificacionPadreHijoDetalle($intPlace){
        
        $strQuery = "DELETE FROM place_clasificacion_padre_hijo_detalle WHERE id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
    
    public function setPlace($intUsuario, $strTitulo, $intCantidadProducto, $intCantidadGaleria){
        
        $intUsuario = intval($intUsuario);
        $intCantidadProducto = intval($intCantidadProducto);
        $intCantidadGaleria = intval($intCantidadGaleria);
        $strTitulo = trim($strTitulo);
        
        $strQuery = "INSERT INTO place(id_usuario, titulo, num_producto, num_galeria)
                                VALUES({$intUsuario}, '{$strTitulo}', {$intCantidadProducto}, {$intCantidadGaleria} )";
        
        $this->objDBClass->db_consulta($strQuery);
        
        return $this->objDBClass->db_last_id();        
    }
    
    public function setEditPlace($intPlace, $strTitulo, $intCantidadProducto, $intCantidadGaleria){
        
        $intPlace = intval($intPlace); 
        $intCantidadProducto = intval($intCantidadProducto); 
        $intCantidadGaleria = intval($intCantidadGaleria); 
        $strTitulo = trim($strTitulo); 
        
        $strQuery = "UPDATE place
                     SET    titulo = '{$strTitulo}',
                            num_producto = {$intCantidadProducto},
                            num_galeria = {$intCantidadGaleria}   
                     WHERE  id_place = {$intPlace} ";
        $this->objDBClass->db_consulta($strQuery);
                
    }
    
    public function getPlaceClasificacion($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion
                     FROM   place_clasificacion
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion"]] = $rTMP["id_clasificacion"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadre($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT place_clasificacion_padre.id_clasificacion_padre,
                            place_clasificacion_padre.producto,
                            clasificacion_padre.nombre,
                            clasificacion_padre.tag_en,
                            clasificacion_padre.tag_es
                     FROM   place_clasificacion_padre,
                            clasificacion_padre
                     WHERE  place_clasificacion_padre.id_place = {$intPlace}
                     AND    place_clasificacion_padre.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
            $arrInfo[$rTMP["id_clasificacion_padre"]]["producto"] = $rTMP["producto"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadreHijo($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion_padre_hijo
                     FROM   place_clasificacion_padre_hijo
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function getPlaceClasificacionPadreHijoDetalle($intPlace){
        
        $intPlace = intval($intPlace);
        
        $strQuery = "SELECT id_clasificacion_padre_hijo_detalle
                     FROM   place_clasificacion_padre_hijo_detalle
                     WHERE  id_place = {$intPlace} ";
               
        $arrInfo = array();                   
        $qTMP = $this->objDBClass->db_consulta($strQuery);
        while( $rTMP = $this->objDBClass->db_fetch_array($qTMP) ){
            
            $arrInfo[$rTMP["id_clasificacion_padre_hijo_detalle"]] = $rTMP["id_clasificacion_padre_hijo_detalle"];
                        
        }
        
        $this->objDBClass->db_free_result($qTMP);
        
        return $arrInfo;
        
    }
    
    public function setProductoClasificacionPadrePlace($intPlace, $intPlaceClasificacionPadre, $intProducto){
        
        $strQuery = "UPDATE place_clasificacion_padre
                     SET    producto = {$intProducto} 
                     WHERE  id_place = {$intPlace}
                     AND    id_clasificacion_padre = {$intPlaceClasificacionPadre} ";
        $this->objDBClass->db_consulta($strQuery);
        
    }
            
    public function setCodigoVendedor($intUsuario, $strcodigoVendedor){
        
        $strQuery = "DELETE FROM usuario_vendedor WHERE id_usuario = {$intUsuario} ";
        $this->objDBClass->db_consulta($strQuery);
        
        $strQuery = "INSERT INTO usuario_vendedor(id_usuario, codigo, activo)
                                            VALUES({$intUsuario}, '{$strcodigoVendedor}', 'A') ";
        $this->objDBClass->db_consulta($strQuery);
                
    }        
                               
}

?>
