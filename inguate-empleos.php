<?php

include "core/function_servicio.php";

if( isset($_GET["setSolicitudEmpleo"]) ){
    
    $strFechaNacimiento = isset($_POST["txtFechaNacimiento"]) ? $_POST["txtFechaNacimiento"] : "";
    $strUiltimoEmpleo = isset($_POST["txtDatoUltimoEmpleo"]) ? $_POST["txtDatoUltimoEmpleo"] : "";
    $strComunicacionSocial = isset($_POST["slcComunicacionSocial"]) ? $_POST["slcComunicacionSocial"] : "";
    $strPersistencia = isset($_POST["slcPersistencia"]) ? $_POST["slcPersistencia"] : "";
    $strTimida = isset($_POST["slcTimida"]) ? $_POST["slcTimida"] : "";
    $strTiempoTrabajo = isset($_POST["txtTiempoTrabajo"]) ? $_POST["txtTiempoTrabajo"] : "";
    $strDireccionVivienda = isset($_POST["txtDireccionVive"]) ? $_POST["txtDireccionVive"] : "";
    $strTokenRecap = isset($_POST["g-recaptcha-response"]) ? $_POST["g-recaptcha-response"] : "";
    
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
    $strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";
    define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

    $arr["error"] = "true";
    $arr["msn"] = lang["datos_error_1"];
    
    if( !empty($strTokenRecap) ){
        
        //Lo primerito, creamos una variable iniciando curl, pasándole la url
        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
         
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt ($ch, CURLOPT_POST, 1);
         
        //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
        curl_setopt ($ch, CURLOPT_POSTFIELDS, "secret=6LfJ7agUAAAAAOt0iGbsPjudsKSUX6PHiK1qq6ek&response=".$strTokenRecap);
         
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
         
        //recogemos la respuesta
        $crulrespuesta = curl_exec ($ch);
        
         
        //o el error, por si falla
        $error = curl_error($ch);
         
        //y finalmente cerramos curl
        curl_close ($ch);
        
        $arrRespuesta = json_decode($crulrespuesta);
        
        if( !$arrRespuesta->success ){
            
            $arr["msn"] = lang["error_captcha"];
        
            print json_encode($arr);            
            die();         
            
        }
        
        
        $strUrlArchivo = "";
        if( isset($_FILES["flCV"]) && $_FILES["flCV"]["size"] > 0 ){
                
            $arrNameFile = explode(".", $_FILES["flCV"]["name"]); 
            $strUrlArchivo = "../../file_inguate/cv/cv_".$intIdUsuario.".".$arrNameFile[1];
            rename($_FILES["flCV"]["tmp_name"], $strUrlArchivo);
            
        }
        
        
        $strUrlArchivoFoto1 = "";
        if( isset($_FILES["flFoto_1"]) && $_FILES["flFoto_1"]["size"] > 0 ){
                
            $arrNameFile = explode(".", $_FILES["flFoto_1"]["name"]); 
            $strUrlArchivoFoto1 = "../../file_inguate/cv/foto_1_".$intIdUsuario.".".$arrNameFile[1];
            rename($_FILES["flFoto_1"]["tmp_name"], $strUrlArchivoFoto1);
            
            fntOptimizarImagen($strUrlArchivoFoto1, "foto_1_".$intIdUsuario);
                
        }
        
        $strUrlArchivoFoto2 = "";
        if( isset($_FILES["flFoto_2"]) && $_FILES["flFoto_2"]["size"] > 0 ){
                
            $arrNameFile = explode(".", $_FILES["flFoto_2"]["name"]); 
            $strUrlArchivoFoto2 = "../../file_inguate/cv/foto_2_".$intIdUsuario.".".$arrNameFile[1];
            rename($_FILES["flFoto_2"]["tmp_name"], $strUrlArchivoFoto2);
            fntOptimizarImagen($strUrlArchivoFoto2, "foto_2_".$intIdUsuario);
            
        }
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
        $strQuery = "SELECT *
                     FROM   usuario
                     WHERE  id_usuario = {$intIdUsuario} ";
        
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        
        $objDBClass->db_free_result($qTMP);
        
        
        $strBody = "<table style=\"width: 100%;\">
                        
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Nombre</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$rTMP["nombre"]}</td>
                        </tr>
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Telefono</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$rTMP["telefono"]}</td>
                        </tr>
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">email</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$rTMP["email"]}</td>
                        </tr>
                    
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Fecha Nacimiento</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$strFechaNacimiento}</td>
                        </tr>
                    
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Ultimo Empleo</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$strUiltimoEmpleo}</td>
                        </tr>
                    
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Comunicacion Social</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$strComunicacionSocial}</td>
                        </tr>
                    
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Persistencia</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$strPersistencia}</td>
                        </tr>
                    
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Timida</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$strTimida}</td>
                        </tr>
                    
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Tiempo Trabajo</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$strTiempoTrabajo}</td>
                        </tr>
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Vivienda</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\">{$strDireccionVivienda}</td>
                        </tr>
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Url CV</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\"><a href=\"https://inguate.com/de.php?src={$strUrlArchivo}\">https://inguate.com/de.php?src={$strUrlArchivo}</a></td>
                        </tr>
                    
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Foto 1</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\"><a href=\"https://inguate.com/imgInguate.php?t=up&src={$strUrlArchivoFoto1}\">https://inguate.com/imgInguate.php?t=up&src={$strUrlArchivoFoto1}</a></td>
                        </tr>
                        <tr>
                            <td style=\"width: 20%; border-bottom: 1px solid black;\">Foto 2</td>
                            <td style=\"width: 80%; border-bottom: 1px solid black;\"><a href=\"https://inguate.com/imgInguate.php?t=up&src={$strUrlArchivoFoto2}\">https://inguate.com/imgInguate.php?t=up&src={$strUrlArchivoFoto2}</a></td>
                        </tr>
                    
                    </table>";
        
        fntEnviarCorreo("info@inguate.com", "Solicitud Empleo", $strBody);
        
        $objDBClass->db_close();            
        
        $arr["error"] = "false";
        $arr["msn"] = "Gracias por enviar tu Solicitud";
    
        
    }    
    
    print json_encode($arr);
    die();
    
}

?>
    <!DOCTYPE html>
    <html lang="en">
        
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Inguate Empleos</title>
            
            <link rel="shortcut icon" href="dist/images/favicon.ico">
            
            <link href="dist/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" type="text/css" href="dist/css/fonts.css"/>
               <meta name="yandex-verification" content="f41d07283a5dc6e5" />
            

            <script src="dist_interno/js/jquery-3.4.0.min.js" ></script>
            <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
            <script src="dist/bootstrap-4.3.1-dist/js/bootstrap.min.js" ></script>
            
            <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
            <script src="dist_interno/sweetalert/sweetalert.min.js"></script>

            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142437492-1"></script>
            <script>
             window.dataLayer = window.dataLayer || [];
             function gtag(){dataLayer.push(arguments);}
             gtag('js', new Date());

             gtag('config', 'UA-142437492-1');
            </script> 
            
        </head>
        <body style="height: 100% !important;" class="clBody ">
            <!-- The core Firebase JS SDK is always required and must be listed first -->
            <script src="https://www.gstatic.com/firebasejs/6.3.1/firebase-app.js"></script>

            <!-- Add Firebase products that you want to use -->
            <script src="https://www.gstatic.com/firebasejs/6.3.1/firebase-auth.js"></script>
            
            <div class="preloader">
                <div class="preloaderdetalle">
                    <img src="dist/images/30.gif" alt="NILA">
                </div>
            </div>
            <style>
                
                .preloader {
                    opacity: 0.5;
                    height: 100%;
                    width: 100%;
                    background: #FFF;
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 9999999;
                }
                
                .preloader .preloaderdetalle {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    -webkit-transform: translate(-50%, -50%);
                    transform: translate(-50%, -50%);
                    width: 120px;
                }
                
            </style>
            
            <!--
            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3&appId=2795767433798083&autoLogAppEvents=1"></script>
             -->

       
<link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
<link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> 
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 

<link rel="stylesheet" type="text/css" href="dist/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="dist/slick/slick-theme.css"/>

<link href="dist/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet"> 
<link href="dist/lightbox-master/dist/ekko-lightbox.css" rel="stylesheet"> 

    

<input type="hidden" id="hidKey" value="kn2f">
<input type="hidden" id="hidKeyRegistroCore" value="kn2f">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">

<style>
        
    .temalugar_border_back{
        border: 2px solid #3E53AC; 
        color: #3E53AC !important;    
        background: white;
    }    
    .text-background-theme {
        background-color: #3E53AC !important;
        
    }
    .text-color-theme {
        color: #3E53AC !important;
    
    }

    .btn-theme-circular {
        background-color: white;
        color: #3E53AC;
        border: 2px solid #3E53AC;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
        position:absolute;
        top:0%;
        right:2%;
        z-index: 99;

    }
     
           
    
                    
    .fondo {
     -webkit-box-sizing: content-box;
      -moz-box-sizing: content-box;
      box-sizing: content-box;
      padding: 54px 55px 55px;
      border: none;
      -webkit-border-radius: 0 0 0 2000px / 0 0 0 230px;
      border-radius: 0 0 0 2000px / 0 0 0 230px;
      font: normal 16px/1 "Times New Roman", Times, serif;
      color: black;
      -o-text-overflow: ellipsis;
      text-overflow: ellipsis;
     background: -moz-linear-gradient(top, rgba(46,57,117,0.83) 0%, rgba(46,57,117,0.83) 1%, rgba(41,52,119,0.93) 40%, rgba(39,50,119,0.95) 53%, rgba(39,50,119,1) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='red', endColorstr='blue',GradientType=0 ); 
    }
    
</style>
<div class="container-fluid m-0 p-0" >
    
    <div class="row m-0 p-0">
    
        <div class="col-12 p-0 m-0 text-center" >
            <!--
            <div class="fondo" style="">
                   
            
            </div>
              -->
        
            <img src="images/fondo.png" style="width: 100%; height: auto;">
                   
            <div class="m-0 p-0 pt-2 pt-lg-4  text-center" style="position: absolute; top: 0px; width: 100%;  "> 
                
                    <!-- Header WEB -->       
    <div id="divHeaderPrincipaWeb">
        <div class="d-none d-md-block">
            
             
            
            <div class="row justify-content-center m-0">
                
                <div class="col-lg-2 col-md-3 col-sm-3" onclick="location.href = 'index.php';" style="z-index: 1049; cursor: pointer;">
                    <img src="images/LOGO-_INGUATE_WHITE.png" class="img-fluid " style="width: 100%; height: auto;" >
                </div>
                
            </div>
            
            <div class="row justify-content-end m-0 p-0 pt-3 mt-sm-4" style="position: absolute; top: 0px; width: 100%;">
                <div class="col-11 m-0 p-0  text-center " >
                    
                    <div class="row">
                                    
                        <div class="col-12">
                                     
                            <div class="pos-f-t text-right"> 
                                <div class="collapse show" id="navbarToggleExternalContent"> 
                                    <div class=" p-1 nowrap" >
                                        
                                        <table style="width: 100%;" >
                                            <tr>
                                                <td  style="width: 100%;"  nowrap class="">
                                                
                                                                                        
                                                <button type="button" onclick="setLogInDashCookie();" class="btn btn-link text-white" style="cursor: pointer;">Mi Negocio <i class="fa fa-external-link"></i> </button>
                                                
                                                                                            
                                            <span class="dropdown">
                                                <button type="button" onclick="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link text-white" style="cursor: pointer;">
                                                    Hola Yordi web <i class="fa fa-angle-down"></i>
                                                </button>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <!--<a class="dropdown-item" href="profile.php">Perfil</a>  -->
                                                    <a class="dropdown-item" href="javascript:void(0)" onclick="fntCerrarSesion();">Cerrar Sesión </a>
                                                </div>
                                            </span>

                                                                                    
                                        </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                                                                                                                                
                        </div>
                        
                    </div>
                </div>
                <div class="col-1 m-0 p-0 pr-4 text-right">
                             
                        <img src="images/menu.png" style="cursor: pointer;" class="img-fluid" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                    
                    
                </div>
            </div>
        
        </div>
    </div>
    
            
                
                <!-- Contenido -->       
                <div class="row p-0 m-0 justify-content-center mt-3" style="">
                    <div class="col-12 p-0 m-0 ">
                        
                                                     
                        
                        <div id="divContenidoSeccion_1">
                            
                        </div>
                                                
                    </div>
                </div>
                
                <div class="row p-0 m-0 pl-1 justify-content-center container__" style="">
                    
                    <div class="col-12 col-lg-4 p-1 m-0 bg-white police" style="" id="divContenidoSeccion_2">
                        
                                  
                        
                        
                        
                    </div>
                    
                    <div class="col-12 col-lg-8 p-0 m-0 pl-1 pr-1 bg-white" style="">
                        
                        
                        <div class="row p-0 m-0 ">
                            <div class="col-12 p-0 m-0">
                                
                                <form name="frmSolicitudEmpleo" id="frmSolicitudEmpleo"  onsubmit="return false;" method="POST">
                    
                                <div class="row p-0 m-0 ">
                                    <div class="col-12 p-1 m-0">
                                        
                                        <h4 class="text-left">Llena la información que se te pide</h4>
                                        
                                        <p class="text-left">Se parte de la empresa de tecnología mas innovadora de Guatemala</p>
                                        
                                        <div class="row p-0 m-0 mt-4 ">
                                        
                                            <div class="col-lg-6 col-12">
                                                
                                                <h6 class="text-left">Fecha de Nacimiento</h6>
                                                <input name="txtFechaNacimiento" id="txtFechaNacimiento" type="text" class="form-control form-control-sm" placeholder="MM/DD/YYYY">                                                
                                            </div>
                                            
                                            
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Datos de tu ultimo empleo: Nombre de la empresa, fecha de inicio, fecha fin y si nunca has trabajado solamente escribe "No aplica"</h6>
                                                <textarea name="txtDatoUltimoEmpleo" id="txtDatoUltimoEmpleo" class="form-control form-control-sm"></textarea>                                             
                                            </div>
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Del 1 al 10 que cual es tu capacidad de comunicación social? </h6>
                                                <select class="form-control form-control-sm" id="slcComunicacionSocial" name="slcComunicacionSocial">
                                                    
                                                    <option value="0">Seleccione un opción...</option>
                                                    <?php
                                                    
                                                    for( $i = 1; $i <= 10; $i++ ){
                                                        ?>
                                                        <option value="<?php print $i?>"><?php print $i?></option>
                                                        <?php
                                                    }
                                                    
                                                    ?>
                                                    
                                                </select>                                            
                                            </div>
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Del 1 al 10 que tan persistente eres? </h6>
                                                <select class="form-control form-control-sm" id="slcPersistencia" name="slcPersistencia">
                                                    
                                                    <option value="0">Seleccione un opción...</option>
                                                    <?php
                                                    
                                                    for( $i = 1; $i <= 10; $i++ ){
                                                        ?>
                                                        <option value="<?php print $i?>"><?php print $i?></option>
                                                        <?php
                                                    }
                                                    
                                                    ?>
                                                    
                                                </select>                                            
                                            </div>
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Del 1 al 10 que tan tímida eres? </h6>
                                                <select class="form-control form-control-sm" id="slcTimida" name="slcTimida">
                                                    
                                                    <option value="0">Seleccione un opción...</option>
                                                    <?php
                                                    
                                                    for( $i = 1; $i <= 10; $i++ ){
                                                        ?>
                                                        <option value="<?php print $i?>"><?php print $i?></option>
                                                        <?php
                                                    }
                                                    
                                                    ?>
                                                    
                                                </select>                                            
                                            </div>
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Cual es el trabajo donde mas tiempo has trabajado, que hacías y cuanto tiempo? y si nunca has trabajado solamente escribe "No aplica"</h6>
                                                <textarea name="txtTiempoTrabajo" id="txtTiempoTrabajo" class="form-control form-control-sm"></textarea>                                             
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Direccion donde vive</h6>
                                                <textarea name="txtDireccionVive" id="txtDireccionVive" class="form-control form-control-sm"></textarea>                                             
                                            </div>
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <h6 class="text-left">Adjuntar Fotografia</h6>
                                                
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Fotografia 1</h6>
                                                <input type="file" id="flFoto_1" name="flFoto_1">                                             
                                            </div>
                                            
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Fotografia 2</h6>
                                                <input type="file" id="flFoto_2" name="flFoto_2">                                             
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                    
                                                <h6 class="text-left">Adjunta tu CV</h6>
                                                <input type="file" id="flCV" name="flCV">                                             
                                            </div>
                                        </div>
                                        
                                        <div class="row p-0 m-0 mt-2">
                                        
                                            <div class=" col-12">
                                                
                                                <button id="btnEnviar" type="button" class="btn btn-success text-background-theme" onclick="fntShowComentarPlace();">Enviar Aplicación</button>
                
                                                <div id="divRecap" style="display: none;" class="mt-1">
                                                        
                                                    <center>
                                                        <div id="divRecapss" ></div>
                                                        
                                                    </center>
                                                
                                                </div>
                                                                                                    
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                </form> 
                                
                                <script>
                                    
                                    function fntShowComentarPlace(){
                                        
                                        var intSizeFile = $("#flCV")[0].files.length;
                                        if( intSizeFile === 0){
                                                       
                                            swal({   
                                                title: "",
                                                text: "Adjunta tu CV",
                                                type: "error",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true 
                                            },function(isConfirm){   
                                                
                                            });
                                                
                                            return false;
                                        }
                                        
                                        var intSizeFile = $("#flFoto_1")[0].files.length;
                                        if( intSizeFile === 0){
                                                       
                                            swal({   
                                                title: "",
                                                text: "Adjunta tu Fotografia",
                                                type: "error",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true 
                                            },function(isConfirm){   
                                                
                                            });
                                                
                                            return false;
                                        }
                                        
                                        $('#divRecap').show(); 
                                        $('#btnEnviar').hide()
                                            
                                    }
                                    
                                    var onloadCallback = function() {
                                        grecaptcha.render('divRecapss', {
                                          'sitekey' : '6LfJ7agUAAAAAEdlQUwhTyZ_XnWVkXHmYyZCgPdn',
                                          'callback' : onloadCallbackSubmit
                                        });
                                      };
                                      
                                    function onloadCallbackSubmit(){
                                        
                                        
                                        var formData = new FormData(document.getElementById("frmSolicitudEmpleo"));
                                        
                                        $(".preloader").fadeIn();
                                        
                                        $.ajax({
                                            url: "inguate-empleos.php?setSolicitudEmpleo=true", 
                                            type: "POST",
                                            data: formData,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            dataType : "json",
                                            success: function(result){
                                                
                                                $(".preloader").fadeOut();
                                                
                                                swal({   
                                                    title: "",
                                                    text: result["msn"],
                                                    type: result["error"] == "true" ? "error" : "success",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true 
                                                },function(isConfirm){   
                                                    
                                                    if( result["error"] == "false" )
                                                        location.href = "index";
                                                
                                                });
                                        
                                            }
                                                    
                                        });                            
                                        
                                    }
                                    
                                </script>
                                <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
                                    async defer>
                                </script>               
                                
                                <div class="row p-0 m-0">
                                        <div class="col-12">  
                                             
                                             <br>                                                                                                         
                                            <button class="btn btn-sm btn-outline btn-outline-light " onclick="location.href = 'https://maps.google.com/?q=14.582460,-90.739224'" style="border:  1px solid #3E53AC; color: #3E53AC;" >
                                                Google Maps <i class="fa fa-paper-plane"></i>
                                            </button>

                                            <button class="btn btn-sm btn-outline btn-outline-light " onclick="fntSetWaze();" style="border:  1px solid #3E53AC; color: #3E53AC;" >
                                                Waze <i class="fa fa-rocket"></i>
                                            </button>
                                            <br>
                                            <br>

                                                                                            
                                                <iframe style="width: 100%; height: 450px; border: 0px;"  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyABJ9worirb3vAoxbghUZj_rNUTrfTtRPY&q=14.582460,-90.739224" allowfullscreen></iframe>
                                             
                                                                                            
                                            
                                        </div>
                                    </div>
                            
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                
                    <div class="row m-0 p-0">
        <div class="col-12 m-0 pt-5 pb-5" style="background-color: #3E53AC; margin-top: 80px !important; ">
              
            <!-- Web  -->
            <div class="d-none d-sm-block">
            
                <div class="row justify-content-center m-0 p-0">
                    <div class="col-3 text-white text-left pl-4 m-0" style="">
                        
                        <i class="fa fa-map-marker"></i> Colonia las victorias F-29, Jocotenango
                        <br>
                        <i class="fa fa-phone"></i> <a href="tel:+502 4229 7744" class="m-0 p-0 text-white">+502 4229 7744</a>
                        <br>
                        <span onclick="window.open(' https://wa.me/14242378844?text=Estoy%20interesado%20en%20Inguate');" style="cursor: pointer"><img src="images/whatsapp.png"  style="width: 20px; height: 20px;">  Whatsapp chat</span>
                                                
                    </div>
                    <div class="col-6 m-0">
                        <h5 class="text-center text-white" style="font-size: 15px;">
                            <img src="images/LOGO-_INGUATE_WHITE.png" >
                            <br>
                            © 2019. Todos los derechos reservados.
                            
                        </h5>
                        
                    </div>
                    <div class="col-3 m-0  ">
                        
                        <img style="width: 50%; height: auto; "  src="images/google-cloud-platform.png">
                        
                    </div>
                </div>
            </div>
                

            <!-- Movil   -->

            <div class="d-block d-sm-none">
                <h6 class="text-center text-white">
                    <img src="images/LOGO-_INGUATE_WHITE.png" >
                    <br>
                    © 2019. Todos los derechos reservados.
                    
                    <br>
                    <br>
                    <br>
                </h6>
            </div>                     
                
        </div>
    </div>
                    
            </div>
                   
        </div>
    </div>
    
        
    <div id="divBarraNavegacionFooter">
                    <script src="dist/autocomplete/jquery.auto-complete.min.js"></script>

                    
        <div class="d-block d-md-none" style="position: fixed; bottom: 0%; width: 100%; z-index: 998">
            <div class="row p-0 m-0">
                
                <div onclick="location.href = 'index.php'" class="col-3 mt-2 pb-1 pr-0 bg-white text-center " style="border-top: 10px solid #3E53AC;">
                    
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td class="text-right pr-2" style="vertical-align: middle;">
                                <img src="images/LogoInguateAZUL.png" class="img-fluidd " style="width: 100%; height: auto;" >
                    
                            </td>
                        </tr>
                    </table>
                    
                </div>
                
                <div class="col-6 bg-white pb-1 p-0" onclick="" style="border-radius:10px 10px 0px 0px; border-top: 18px solid #3E53AC;">
                    
                    <div class="bg-white m-1" style="position: absolute; top: -15px; left: 0px; border-radius:10px 10px 0px 0px; width: 96%;">
                    
                        <table style="width: 100%; height: 100%;">
                            <tr>
                                <td class="text-right" style="vertical-align: middle; ">
                                    
                                    <i class="fa fa-search" style="color: #3E53AC;  font-size: 18px;"></i>
                                    
                                </td>
                                <td class="text-center" nowrap style="vertical-align: middle; width: 78%;">
                                    
                                    <input type="text" id="txtcoreQueBuscar" value="¿Qué buscas?" class="form-control form-con border-0 " onfocus="fntShowAutocomplete();" style="border-radius:10px 10px 0px 0px; font-size: 18px;"> 
                                                        
                                </td>
                            </tr>
                        </table>
                        
                    </div>
                    <br>
                    <br>    
                </div>
                
                <div class="col-3 bg-white mt-2 pb-1  p-0" style="border-top: 10px solid #3E53AC;">
                    
                    <table style="width: 100%; height: 100%;">
                        <tr>     
                            <td class="text-center" style="vertical-align: middle; width: 50%;">
                                <i class="fa fa-users hide" style="color: #3E53AC; font-size: 18px;"></i> 
                                
                                
                                <!--
                                <div style="position: absolute; top: 0px; right: 50%;">
                                    <div class="badge badge-danger" style="font-size: 10px;">1</div>
                                </div> 
                                -->
                                
                            </td>
                            <td class="text-center " style="vertical-align: middle; width: 50%;">
                                
                                <i class="fa fa-bars" id="sidebarCollapse" style="color: #3E53AC; font-size: 18px;"></i>
                                
                                                                    
                                
                            </td>
                        </tr>
                    </table>
                    
                </div>
                
            </div>
        </div>
        
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="dist/bootstrap-4.3.1-dist/jquery.mCustomScrollbar.min.css">
      
        <!-- jQuery Custom Scroller CDN -->
        <script src="dist/bootstrap-4.3.1-dist//jquery.mCustomScrollbar.concat.min.js"></script>
           
        <style>
            
            #sidebar {
                width: 250px;
                position: fixed;
                top: 0;
                right: -250px;
                height: 100%;
                z-index: 999;
                background: #3E53AC;
                color: #fff;
                transition: all 0.3s;
                overflow-y: scroll;
                box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.2);
            }

            #sidebar.active {
                right: 0;
            }

            #dismiss {
                width: 100%;
                height: 35px;
                line-height: 35px;
                text-align: center;
                background: #7386D5;
                position: absolute;
                bottom: 20%;
                left: 0%;
                cursor: pointer;
                -webkit-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }

            #dismiss:hover {
                background: #fff;
                color: #7386D5;
            }

            .overlay {
                display: none;
                position: fixed;
                width: 100vw;
                height: 100vh;
                background: rgba(0, 0, 0, 0.7);
                z-index: 998;
                opacity: 0;
                transition: all 0.5s ease-in-out;
                top: 0px
            }
            .overlay.active {
                display: block;
                opacity: 1;
            }

            #sidebar .sidebar-header {
                padding: 20px;
                background: #3E53AC;
            }

            #sidebar ul.components {
                padding: 20px 0;
                border-bottom: 1px solid #47748b;
                height: 10%;
                background: 100%;
            }

            #sidebar ul p {
                color: #fff;
                padding: 10px;
            }

            #sidebar ul li a {
                padding: 10px;
                font-size: 1.1em;
                display: block;
            }

            #sidebar ul li a:hover {
                color: #7386D5;
                background: #fff;
            }

            #sidebar ul li.active>a,
            a[aria-expanded="true"] {
                color: white;
                background: #6d7fcc;
            }

            a[data-toggle="collapse"] {
                position: relative;
            }

            .dropdown-toggle::after {
                display: block;
                position: absolute;
                top: 50%;
                right: 20px;
                transform: translateY(-50%);
            }

            ul ul a {
                font-size: 0.9em !important;
                padding-left: 30px !important;
                background: #6d7fcc;
            }
            
            .mCSB_container {
                overflow: hidden;
                width: auto;
                height: 100%;
            }
                              
        </style>  
        
        <!-- Sidebar  -->
        <nav id="sidebar">
            

            <div class="sidebar-header pt-4 pl-4 pr-4 pb-0">
                <h3 class="">
                    
                    <img src="images/LOGO-_INGUATE_WHITE.png" class="img-fluidd " style="width: 70%; height: auto;" >
                    
                
                </h3>
            </div>

            <ul class="list-unstyled components" style="height: 60%;">
            
                                        <li class="mt-1">
                            <a href="javascript:void(0)" onclick="setLogInDashCookie();" class="text-white p-1">
                                Mi Negocio <i class="fa fa-external-link"></i>
                            </a>
                        </li>
                                                                
                                            
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="p-1">
                            Hola Yordi web <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <!--
                            <li>
                                <a href="javascript:void(0)" onclick="location.href = 'profile.php'" class="text-white">
                                    Perfil <i class="fa fa-user-circle"></i>
                                </a>
                            </li>  -->
                            <li>
                                <a href="javascript:void(0)" onclick="fntCerrarSesion();" class="text-white">
                                    Cerrar Sesión <i class="fa fa-power-off"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    
                                    
                <li class="active mt-1 ">
                    <a href="#liUbicacionDrawer" class="text-white p-1" data-toggle="collapse" aria-expanded="false">
                        Ubicación <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="liUbicacionDrawer">
                        
                                                    
                            <li class="">
                                <a href="javascript:void(0)" onclick="fntUbicacionCookie('1');" class="text-white">
                                    Antigua Guatemala                                </a>
                            </li>    
                                                
                    </ul>
                </li>
                
                <li class="active mt-1">
                    <a href="#liIdiomaDrawer" class="text-white p-1" data-toggle="collapse" aria-expanded="false">
                        Idioma <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="collapse list-unstyled" id="liIdiomaDrawer">
                        <li>
                            <a href="javascript:void(0)" onclick="fntSetLenguajeCookie('es');" class="text-white">
                                Español                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" onclick="fntSetLenguajeCookie('en');" class="text-dark">
                                Ingles                            </a>
                        </li>
                    </ul>
                </li>
                
                                
                <li class="active mt-1">
                    <a href="javascript:void(0)" id="dismissDrawer" class="text-white text-center p-1" >
                        <i class="fa fa-arrow-left"></i>
                    </a>
                </li>
                <li class="active mt-1 p-2">
                    
                </li>
            </ul>

            <div class="p-3">
                <i class="fa fa-map-marker"></i> Colonia las victorias F-29, Jocotenango
                <br>
                <i class="fa fa-phone"></i> +502 4229 7744
                <br>
                <span onclick="window.open(' https://wa.me/14242378844?text=Estoy%20interesado%20en%20Inguate');" style="cursor: pointer"><img src="images/whatsapp.png"  style="width: 20px; height: 20px;">  Whatsapp chat</span>

            </div>
            
               
        </nav>
        
        <div class="overlay"></div>        
        
        <div class="modal fade" id="mlAutoComplete" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content " style="margin-top: 10% ;">

                      <input type="text" value=""  id="txtAutocomplete_2" name="txtAutocomplete_2" class="form-control bg-white text-dark" placeholder="Qué estás buscando?" style="z-index: 999999999999999;">
                                            

                </div>
            </div>
        </div>
        
        <script>
                        
            $(document).ready(function() { 
                
                var xhr;
                
                $('input[name="txtAutocomplete_2"]').autoComplete({
                    minChars: 2,
                    source: function(term, suggest){
                        
                        try {
                            xhr.abort();
                        }
                        catch(error) {
                        }

                        xhr = $.ajax({
                            url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete_2").val()+"&len="+$("#slcLenguaje_2").val(), 
                            dataType : "json",
                            success: function(result){
                                suggest(result);
                            }
                        });
                        
                    },
                    renderItem: function (item, search){
                        
                        //console.log(item);
                        
                        return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-identificador_2="'+item["identificador_2"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
                    
                    },
                    onSelect: function(e, term, item){
                        
                        if( item.data("tipo") == "5" ){
                            
                            str = "/"+item.data("identificador_2");
                            location.href = str;
                            return false;
                            
                        }
                        else if( item.data("tipo") == "4" ){
                            
                            str = "/"+item.data("identificador_2");
                        
                            location.href = str;
                            return false;
                            
                        }
                        else{
                            
                            str = "s.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                            location.href = str;
                            
                        }
                        
                        return false;
                        
                    }
                }).change(function (){
                    
                    str = "s.php?o=1&key=0&t=4&q="+$("#txtAutocomplete_2").val()+"&ran=all";
                    location.href = str;
                            
                });
                  
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismissDrawer, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').removeClass('active');
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').addClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
                
                $('#mlAutoComplete').on('shown.bs.modal', function (e) {
                    
                    $("#txtAutocomplete_2").blur();

                    $("#txtAutocomplete_2").addClass('active').focus();
                     
                });
                
            });
                        
            
            function fntShowAutocomplete(){
                
                $("#mlAutoComplete").modal("show");       
                
            }
                    
            
        </script>
    </div>
        
</div>

<script type="text/javascript" src="dist/slick/slick.min.js"></script>
<script src="dist/js/jsAntigua.js"></script>
<script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
<script type="text/javascript" src="dist/lightbox-master/dist/ekko-lightbox.js"></script>
   
<script src="dist/imageLoaded/imagesloaded.pkgd.min.js"></script>
     
        
<style>
    
    .flotante_change_theme {
        
        position:absolute;
        top: 1%;
        left:1%;
    
    }

    .slick-center-Border {
        
        padding: 0px !important;
        
    }
    
    .flotante {
            
        display:none;
        position:fixed;
        top:90%;
        right:2%;
        background-color: #3E53AC;
        color: white;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
    
    }
</style>
<script >
           
    var d = new Date();
    var intDiaSemana = d.getDay()
        
    $(document).ready(function() {
        
                    
            $.ajax({
                url: "placeup.php?viewSession=true", 
                dataType : "json",
                async : false,
                success: function(result){
                    
                    if( !result["sesion"] ){
                        
                        
                        location.href = "index?bref=inguate-empleos";
                                               
                    }   
                    else{
                        
                        $.ajax({
                            url: "placeup.php?setHeaderWeb=true",
                            async : false,
                            success: function(result){
                                
                                $("#divHeaderPrincipaWeb").html(result);
                                
                            }
                        });
                        
                        $.ajax({
                            url: "placeup.php?setHeaderMovil=true&color_hex=3E53AC", 
                            async : false,
                            success: function(result){
                                
                                $("#divBarraNavegacionFooter").html(result);
                                
                            }
                        });
                        
                    }                     
                    
                }
                
            });
            
                        
            
                          
        fntDrawSeccion(1);
        fntDrawSeccion(2);
        fntDrawSeccion(3);
                       
        
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });    
        
                    
            //swal("", "Continúa completando la información para el registro y envia la solicitud.");
                                
                    
        
    });
    
    function initializeMap() {

        var mapOptions = {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: center
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

             //console.log(position);    
            var markerOptions = { 
                position: center, 
                draggable: true
            }
            
            markerInicial = new google.maps.Marker(markerOptions);
            markerInicial.setMap(map);
            
            map.setCenter(center);
            map.setZoom(16);
            
            markerInicial.addListener('dragend', function(){
                
                $("#hidLatUbicacion").val(markerInicial.position.lat());
                $("#hidLngUbicacion").val(markerInicial.position.lng());
            
            });
                                      
            $("#hidLatUbicacion").val();
            $("#hidLngUbicacion").val();
            
            
        
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          draggable: true
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          else{
              markerInicial.setMap(null);
          }

          
          //console.log(place);
          //console.log(place.place_id);
          $("#hidPlaceId").val(place.place_id);
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            
            $("#hidLatUbicacion").val(place.geometry.location.lat);
            $("#hidLngUbicacion").val(place.geometry.location.lng);
            
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
            $("#hidLatUbicacion").val(place.geometry.location.lat);
            $("#hidLngUbicacion").val(place.geometry.location.lng);
            
          }
          
          
          //console.log($("#hidLatUbicacion").val());
          //console.log($("#hidLngUbicacion").val());
            
          
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          
          marker.addListener('dragend', function(){
                
                $("#hidLatUbicacion").val(marker.position.lat());
                $("#hidLngUbicacion").val(marker.position.lng());
            
            });
          
          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
           
            
        
    }
              
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
    }
    
    function fntDrawSeccionProductoClasificacionPadre(strKey){
        
        $.ajax({
            url: "placeup.php?getDrawSeccionProductoClasificacionPadre=true&pdd=Y&place=kn2f&pp=kQ==&key="+strKey, 
            success: function(result){
                
                $("#divContenidoClasificacionPadreo_"+strKey).html(result);   
                
            }
        });
        
    }
    
    function fntDrawSeccion(intSeccion, strKey){
        
        strKey = !strKey ? "" : strKey;
                        
        $.ajax({
            url: "placeup.php?fntDrawSeccion=true&place=kn2f&pdd=Y&&seccion="+intSeccion+"&key="+strKey+"&diaS="+intDiaSemana, 
            success: function(result){
                
                $("#divContenidoSeccion_"+intSeccion).html(result);
                
            }
        });
        
    }
    
    function fntShowProductoEspecial(strProducto, strProductoCla){
        
                    
            window.open("proes.php?p="+strProducto)
            
                    
    }
                
    function fntSetWaze(){
        
        location.href = "https://waze.com/ul?q=14.582460,-90.739224";
        
    }
          
                 
            
</script>
        <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog"  aria-hidden="true"  style="">
            <div class="modal-dialog modal-lg modal-dialog-centered" style="min-width: 90%;" role="document" style="">
                <div class="modal-content" style="min-height: 400px;">
                    
                    
                    <div class="modal-body p-0" id="modalLoginContenido">
                    
                            
                    
                                
                    </div>
                    
                </div>
            </div>
        </div>
            
    <!-- Modal -->
              
              <!--
    
    <div class="d-none d-sm-block flotanteCentroArriba " onclick="location.href = 'index.php';">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br>
        <div class="col-3 p-0 m-0 d-none d-md-block" >
            <br>
        </div>
        
    </div>  -->
    
    <div class="flotante" onclick="fntTopScroll();">
        &nbsp;<i class="fa fa-arrow-up"></i>&nbsp;
    </div>
    
    
    
    <style>
            
        .classMenuAutocomplete {
            padding: 10px;
        }
        
        #customBtn {
          display: inline-block;
          background: white;
          color: #444;
          width: 190px;
          border-radius: 5px;
          border: thin solid #888;
          box-shadow: 1px 1px 1px grey;
          white-space: nowrap;
        }
        
        .text-tema{
            color: #3E53AC;
        }  
        
        .flotante {
            
            display:none;
            position:fixed;
            top:80%;
            right:2%;
            background-color: white;
            border: 2px solid #3E53AC;
            color: #3E53AC;
            padding: 10px;
            border-radius: 50%;
            cursor: pointer;
            
        
        }
        .flotanteCentroArriba {
            
            position:fixed;
            top:0%;
            right:45%;
            background-color: transparent;
            color: white;
            padding: 10px;
            cursor: pointer; 
        
        }
        
        .card-img-wrap { 
          overflow: hidden;
          position: relative;
        }
        .card-img-wrap:after {
          content: '';
          position: absolute;
          top: 0; left: 0; right: 0; bottom: 0;
          background: rgba(255,255,255,0.3);
          opacity: 0;
          transition: opacity .25s;
        }
        .card-img-wrap img {
          transition: transform .25s;
          width: 100%;
        }
        .card-img-wrap:hover img {
          transform: scale(1.1);
        }
        /*
        .card-img-wrap:hover:after {
          opacity: 1;
        }
        */
        
        .text-hover-underline:hover{
            text-decoration: underline;
            cursor: pointer;
        }
        
        .hide {
            display: none;
        }
        
        .nofocusBorder:focus {
            
            color: #495057;
            background-color: #fff;
            border-color: #80bdff;
            outline: 0;
            box-shadow: 0 0 0 0.2rem transparent;
        
        }
        
        .btn-inguate{
            color: #3E53AC;
            border: 1px solid #3E53AC;
        }
        
        .btn-inguate:focus{
            color: #3E53AC;
            border: 1px solid #3E53AC;
        }
        
    </style>
        
        <script>
              
            function fntLogOut(){
                
                location.href = "index.php?setlogOut=true";
                    
                
            }     
                 /*
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '2795767433798083',
                    cookie     : false,
                    xfbml      : false,
                    version    : 'v3.3'
                });

                FB.AppEvents.logPageView();   

            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
                     
            function checkLoginState() {
              FB.getLoginStatus(function(response) {
                  
                  if( response.status == "connected" ){
                      
                      FB.api('/me?fields=email,first_name,last_name,picture', function(response) {
                                                
                          $("#txtEmail").val(response.email);
                          $("#txtNombre").val(response.first_name+" "+response.last_name);
                          $("#txtApellido").val(response.last_name);
                          $("#txtIdentificador").val(response.id);
                          $("#txtTipoLogIn").val("F");
                          $("#txtPictureUrl").val(response.picture.data.url);
                      
                          fntLogInPublico();
                          
                      });
                      
                  }
                
              }, {scope: 'public_profile,email'});
            }
                */
            function fntCambioTelefonoModalRegistro(){
                           
                $("#divFormCodigo").hide();    
                $("#divFormCodigoCambioTelefono").show();    
                $("#hidCambioTelefono").val("Y");    
                
            }
            
            function fntLogInPublicoConfirmacion(boolReenvio){
                
                boolReenvio = !boolReenvio ? false : boolReenvio;
                
                var formData = new FormData(document.getElementById("frmModalLogInRegistroConfirmacion"));
                
                formData.append("reenvio", boolReenvio);
                
                $(".preloader").fadeIn();
                $.ajax({
                    url: "servicio_core.php?servicio=setLogInRegistroPConfirmacion", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["error"] == true ){
                            
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            });
                                                            
                        }
                        else {
                            
                            if( result["reenvio"] == true ){
                                
                                if( result["reenvio_telefono_nuevo"] == true ){
                                    
                                    $("#divFormCodigo").show();    
                                    $("#divFormCodigoCambioTelefono").hide();    
                                    $("#hidCambioTelefono").val("N");    
                                    $("#spnNumeroEnvioCodigo").html(result["reenvio_telefono_nuevo_telefono"]);    
                                    
                                    
                                    
                                }
                                
                                swal({
                                    title: "Listo",
                                    text: result["msn"],
                                    type: "success",
                                    confirmButtonClass: "btn-info",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                });
                                    
                            }
                            else{
                                
                                location.reload();
                            
                            }
                                    
                        }
                            
                        
                    }
                            
                });
                
                return false;
                
            }
            
            function fntLogInPublico(){
                
                $("#txtAction").val(window.location.href);
                
                var formData = new FormData(document.getElementById("frmModalLogInRegistro"));
                formData.append("hidKeyRegistroCore", $("#hidKeyRegistroCore").val());    
                formData.append("hidLogInFb", $("#hidKeyRegistroCore").val());    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "servicio_core.php?servicio=setLogInRegistroP", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["confirmacion_sms"] == true ){
                            
                            $("#divFormConfirmacionUsuario").show();
                            $("#divFormRegistro").hide();
                            $("#hidKeyUsuario").val(result["u_key"]);
                            //$("#spnNumeroEnvioCodigo").html(result["telefonoEnvio"]); 
                            $("#spnNumeroEnvioCodigo").html(result["emailEnvio"]); 
                            
                            //if( result["telefonoEnvio"] == "" ){
                            if( result["emailEnvio"] == "" ){
                                            
                                fntCambioTelefonoModalRegistro(); 
                                    
                            }   
                                                                
                        }
                        else {
                            
                            if( result["error"] == "true" ){
                                
                                if( result["error_telefono_registro"] == "true" ){
                                    
                                    fntSetFormRegristro(true);    
                                        
                                }
                                else{
                                    
                                    swal({
                                        title: "Error",
                                        text: result["msn"],
                                        type: "error",
                                        confirmButtonClass: "btn-danger",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                    
                                    
                                    grecaptcha.reset(objgrecaptcha);
                                    
                                    if( result["sms_pendiente"] == "true" ){
                                        
                                        $("#divFormConfirmacionUsuario").show();
                                        $("#divFormRegistro").hide();
                                        $("#hidKeyUsuario").val(result["u_key"]);
                                        //$("#spnNumeroEnvioCodigo").html(result["telefonoEnvio"]);    
                                        $("#spnNumeroEnvioCodigo").html(result["emailEnvio"]);    
                                            
                                        //if( result["telefonoEnvio"] == "" ){
                                        if( result["emailEnvio"] == "" ){
                                            
                                            fntCambioTelefonoModalRegistro(); 
                                                
                                        }
                                                
                                        
                                    }
                                    else{
                                        
                                        fntSetFormRegristro();
                                        
                                    }
                                    
                                }
                                
                                    
                                    
                            }
                            else{
                                
                                location.href = result["href"];
                                                            
                            }
                            
                        }
                            
                        
                    }
                            
                });
                
                return false;
                
            }
            
            
                 
            function fntRegistro(){
                $("#dicContenidRecap").show();    
                $("#btnRegistro").hide();    
                
            }
            
            
            function fntDrawPreguntaNegocio(){
                
                //location.href = "pn.php";
                
                //return false;
                
                swal({
                    title: "",
                    text: "Deseas registrar tu negocio para que puedan encontrarse en Inguate?",
                    type: "",
                    showCancelButton: true,
                    confirmButtonClass: "#FF0000",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                         
                        $.ajax({
                            url: "servicio_core.php?servicio=setUsuarioNegocio", 
                            success: function(result){
                                
                                setLogInDashCookie();    
                                
                            }
                            
                        });
                        
                    
                    } 
                });
            }
            
            function setLogInDashCookie(boolHref){
                
                boolHref = !boolHref ? false : boolHref;
                
                $.ajax({
                    url: "login.php?login=true&cookie=Y", 
                    success: function(result){
                        
                        //if( boolHref ){
                            
                            location.href = "home.php";
                        //}
                        //else{
                            
                        //    window.open("home.php");
                            
                        //}
                        
                    }
                    
                });
                
            }
            
            function setAlertaVista(intKeyAlerta, boolHref){
                
                boolHref = !boolHref ? false : boolHref;
                
                $.ajax({
                    url: "servicio_core.php?servicio=setAlertaVista&key="+intKeyAlerta,
                    dataType: "json", 
                    success: function(result){
                        
                        setLogInDashCookie(boolHref);    
                        
                    }
                    
                });
                
                
            }
            
            function fntTopScroll(){
                
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;    
                
            }
            
            $(document).ready(function() {       
            
                $(window).scroll(function() {
                    var scroll = $(window).scrollTop();
                    if (scroll >= 50) {
                    $(".flotante").show();

                    } else {
                    $(".flotante").hide();
                    }
                });
                
                $('#crResgistro').carousel()
                
                $('#modalLogin').on('hidden.bs.modal', function (e) {
                    
                    $.ajax({
                        url: "servicio_core.php?servicio=getSession",
                        dataType: "json", 
                        success: function(result){
                            
                            if( result["session"] == "N" ){
                                
                                $("#modalLogin").modal("show");
                                    
                            }
                            
                        }
                        
                    });
                    
                });
                
                                 
                $(".preloader").fadeOut();
                   
            });
            
            function fntOpenModalRegistroCore(){
                
                $.ajax({
                        
                    url: "index.php?drawRegistroIndexPagina=true&bref=&cc=&tt=", 
                    success: function(result){
                        
                                                    $("#divContendedorPagina").html(result);
                            
                            var sinWindowsHeight2 = $("#divContenidoFormRegistroModal").height();
                            var sinWindowsHeight = $(window).height();
                    
                            sinWindowsHeight = sinWindowsHeight - sinWindowsHeight2;
                            
                            $(".imgCrRegistro").each(function (){
                                
                                $(this).height( sinWindowsHeight+'px');    
                                
                            });
                            
                            $('html, body').css({
                                overflow: 'hidden',
                                height: '100%'
                            });
                                                    
                    }
                    
                });
                                
            }
            
            function fntSetFormRegristro(boolRegistro){
                
                       
                if( boolRegistro ){
                    
                    $("#txtEmail").css("border-bottom", "2px solid #3E53AC");
                    $("#txtClave").css("border-bottom", "2px solid #3E53AC");
                    
                    $("#sptxtEmail").css("border-bottom", "2px solid #3E53AC");
                    $("#sptxtClave").css("border-bottom", "2px solid #3E53AC");
                
                    var boolError = false;
                    
                    if( $("#txtEmail").val() == ""  ){
                        
                        
                        $("#sptxtEmail").css("color", "red !important");
                        $("#sptxtEmail").css("border-bottom", "2px solid red");
                        $("#txtEmail").css("border-bottom", "2px solid red");
                        
                        boolError = true;
                    
                    }
                    
                    if( $("#txtClave").val() == "" ){
                        
                        
                        $("#sptxtClave").css("color", "red !important");
                        $("#sptxtClave").css("border-bottom", "2px solid red");
                        $("#txtClave").css("border-bottom", "2px solid red");
                        
                        boolError = true;
                    }
                    
                    if( boolError )
                        return false;
                    
                    $("#divEmailClaveInput").hide();
                    
                    $("#divContenidoRegistro").show();
                    $("#divContenidoLogIn").hide();
                    $("#txtRegistro").val('Y');
                    
                }
                else{
                    
                    $("#divContenidoRegistro").hide();
                    $("#divContenidoLogIn").show();
                    $("#txtRegistro").val('N');
                    
                    $("#divEmailClaveInput").show();
                    
                    
                }                
            }
                
            function fntCerrarSesion(){
                
                $.ajax({
                    
                    url: "index.php?setlogOut=true", 
                    success: function(result){
                        
                        location.href = "index.php";
                        
                    }
                    
                });    
                
            }
            
            function fntUbicacionCookie(intUbicacion){
                
                $.ajax({
                    
                    url: "servicio_core.php?servicio=setUbicacionCookie&ubicacion="+intUbicacion, 
                    dataType : "json",
                    success: function(result){
                        location.reload();    
                    }
                    
                });     
                
            }
            
            function fntSetLenguajeCookie(strLenguaje){
                
                $.ajax({
                    
                    url: "servicio_core.php?servicio=setLenguajeCookie&lenguaje="+strLenguaje, 
                    dataType : "json",
                    success: function(result){
                        location.reload();    
                    }
                    
                });
                
            }
            
        </script>
         
        </body>

    </html>
    
    