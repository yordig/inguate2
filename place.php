<?php
include "core/function_servicio.php";

$boolInterno = isset($_GET["inter"]) && $_GET["inter"] == "true" ? true : false;
$intPlace = isset($_GET["p"]) ? intval(fntCoreDecrypt($_GET["p"])) : 0;
$strLenguaje = isset($_GET["len"]) ? trim($_GET["len"]) : "es";

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

$intMaxProductoDestacado = 10;
$intMaxClasificacionDestacada = 2;
$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}          

if( $intPlace ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    
    $arrPlace = fntGetPlace($intPlace, true, $objDBClass);
        
    $arrPlaceClasificacion = array();
    $strQuery = "SELECT clasificacion.id_clasificacion,
                        clasificacion.nombre,
                        clasificacion.tag_en,
                        clasificacion.tag_es,
                        clasificacion.url_img,
                        clasificacion.color_hex,
                        clasificacion.producto
                 FROM   place_clasificacion,
                        clasificacion
                 WHERE  place_clasificacion.id_place = {$intPlace} 
                 AND    place_clasificacion.id_clasificacion = clasificacion.id_clasificacion
                 ";
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["url_img"] = $rTMP["url_img"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["color_hex"] = $rTMP["color_hex"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["producto"] = $rTMP["producto"];
            
    }   
    
    $objDBClass->db_free_result($qTMP); 
    
    $arrPlaceClasificacionPadre = array();
    $strQuery = "SELECT clasificacion_padre.id_clasificacion_padre,
                        clasificacion_padre.id_clasificacion,
                        clasificacion_padre.nombre,
                        clasificacion_padre.tag_en,
                        clasificacion_padre.tag_es
                 FROM   place_clasificacion_padre,
                        clasificacion_padre
                 WHERE  place_clasificacion_padre.id_place = {$intPlace} 
                 AND    place_clasificacion_padre.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre
                 ";
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["producto"] = isset($arrPlaceClasificacion[$rTMP["id_clasificacion"]]) ? $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["producto"] : "Y";
            
    }   
    
    $objDBClass->db_free_result($qTMP); 
    
    $arrPlaceGaleria = fntGetPlaceGaleria($intPlace, true, $objDBClass);
    
    $strQuery = "SELECT place_producto.id_place_producto,
                        place_producto.id_clasificacion_padre,   
                        place_producto.nombre,   
                        place_producto.precio,   
                        place_producto.descripcion,   
                        place_producto.logo_url,   
                        place_producto.destacado,   
                        place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo,   
                        place_producto_clasificacion_padre_hijo_detalle.id_clasificacion_padre_hijo_detalle,
                        clasificacion_padre_hijo.tag_en,  
                        clasificacion_padre_hijo.tag_es  
                        
                 FROM   place_producto
                            LEFT JOIN   place_producto_clasificacion_padre_hijo
                                ON  place_producto_clasificacion_padre_hijo.id_place_producto =  place_producto.id_place_producto
                                
                                LEFT JOIN   clasificacion_padre_hijo
                                    ON  clasificacion_padre_hijo.id_clasificacion_padre_hijo = place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo
                                
                                
                            LEFT JOIN   place_producto_clasificacion_padre_hijo_detalle
                                ON  place_producto_clasificacion_padre_hijo_detalle.id_place_producto =  place_producto.id_place_producto
                 WHERE  place_producto.id_place = {$intPlace} 
                         ";
    $arrClasificacionProducto = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
        $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
        $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
        $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
        $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["destacado"] = $rTMP["destacado"];
        $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
        
        if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
            
            $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
        
        }
            
        if( intval($rTMP["id_clasificacion_padre_hijo_detalle"]) )
            $arrClasificacionProducto[$rTMP["id_clasificacion_padre"]][$rTMP["id_place_producto"]]["clasificacion_padre_hijo_detalle"][$rTMP["id_clasificacion_padre_hijo_detalle"]] = $rTMP["id_clasificacion_padre_hijo_detalle"];
                
    }
    
    $objDBClass->db_free_result($qTMP);
    
        
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--
    <meta name="viewport" content="width=device-width, initial-scale=1">
    -->
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=NO, width=device-width, initial-scale=1.0">
    
    <title>Lister</title>
    <link rel="shortcut icon" href="dist/images/favicon.ico">
    <!--Plugin CSS-->
    <link href="dist/css/plugins.min.css" rel="stylesheet">
    <!--main Css-->
    <link href="dist/css/main.min.css" rel="stylesheet">  
    <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
    
    <link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
    <link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> <!-- for live demo page -->
    <link href="dist/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
 
        
</head>

<body style="<?php print $boolInterno ? "overflow: hidden;" : ""?>   ">
    <style>
        
        .InputdirectionLTR {
            direction: ltr;
            width: 100%;
        }
        
        .grediant-bt-dark:before {
            background: #dc3545 !important;
        }
        
        .text-color-global {
            color: black !important;
        }
        
        .cardNoBackground {
            background-color: transparent; border: 0px
        }
        
        .cursor_pointer {
            cursor: pointer;
        }
        
        .lis-id-info {
          background-color: #ff214f;
          color: white;
          width: 36px;
          height: 36px;
        line-height: 38px; 
        }
          .lis-id-info:hover {
              opacity: 0.7;
              color: white ;
            background-color: #ff214f; }
        
        .lis-id-info-sm {
          background-color: #ff214f;
          color: white;
          width: 26px;
          height: 26px;
          font-size: 14px;
        line-height: 28px; 
        }
          .lis-id-info-sm:hover {
              opacity: 0.7;
              color: white ;
            background-color: #ff214f; }
    
    
               
        .select2-dropdown {
            background-color: white;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 999999999999999;
            border: 2px solid red;
        }
        
        .bg-delete{
            background-color: #FFC0C0 !important;
        }
            
        .shadow_mia{
            box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.41);
        }
        
        .clockpicker-popover .popover-title {
            background-color: #fff;
            color: #999;
            font-size: 24px;
            font-weight: 700;
            line-height: 30px;
            text-align: center;
            direction: ltr;
        }
        
  
        .slideImgActiva {
            
            padding: 5px 0px 0px 0px ;
            border-radius: 20px !important;
            
            box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
            -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
            -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
            
        }
            
        
        .slideImgNoActiva {
            
            border-radius: 20px !important;
            padding: 10px 10px 10px 10px ;
            border-radius: 20px !important;
            
        }
        .slideImgNoActivaIMG {
            
            border-radius: 20px !important;
            
            box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);   
            -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);  
            -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
            
        }
        
        #lean_overlay {
            position: fixed;
            z-index: 1500;
            top: 0px;
            left: 0px;
            height: 100%;
            width: 100%;
            background: #000;
            display: none;
        }
        
        .popover {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 9999999999999999999999999999;
            display: block;
            max-width: 276px;
            padding: 1px;
            text-align: left;
            text-align: start;
            background-color: #fff;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: .3rem;
        }
        
        
        .transperant nav.navbar-toggleable-md.navbar-light {
            background: #dc3545;
            border: 0px;
        }
        
        #header-fix.nav-affix .navbar {
          background-color: #dc3545;
          /*
          box-shadow: 0 10px 20px -12px rgba(0, 0, 0, 0.42), 0 3px 20px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
          border-color: #000;
          */
          transition: 0.3s; }
        
         /*
        .login_form{
            display: none !important;
        }
        
        
        .slick-current {
            background: yellow !important; 
        }
        */
    </style>
    <!-- header
    
    <a class="navbar-brand mr-4 mr-md-5 p-4" style="border-radius: 0% 0% 50% 50%;  background-color: #dc3545; color: white; font-weight: bold; font-size: 45px; font-family: Calibri;" href="index.php">
                INGUATE
            </a>
    
     -->
    <div id="header-fix" class="header fixed-top transperant p-0 m-0" style="margin-left: 0px; ">
        
        <?php
        
        fntDrawNavbarPublico(true, true, $objDBClass);
                
        
        ?>
        
        <div id="divContenidoSeccion_1" class="p-0 m-0">
            
            <h5 class="bg-danger  p-0 m-0 text-dark font-weight-bold" style="line-height: 1.25rem; ">
                
                <span class="text-white" style=" <?php print $boolMovil ? "font-size: 20px;" : "font-size: 30px;"?> ">
                    
                    <?php print $arrPlace["titulo"]?>
                
                </span>
                
            </h5>
            <div class="bg-danger" style="  position: center; height: <?php print $boolMovil ? "100px" : "150px"?>; top: 0px;" >
                <div class="sliderInicio slider  "  style="direction: <?php print $boolMovil ? "ltr" : "ltr"?>; background: transparent; <?php print $boolMovil ? "padding-left: 20px;" : ""?>">
                    <?php
                    
                    if( count($arrPlaceGaleria) > 0 ){
                        
                        $arrPlace["num_galeria"] = count($arrPlaceGaleria);
                        $intNumFoto = 1;
                        while( $rTMP = each($arrPlaceGaleria) ){
                            
                            ?>
                            <div  aria-hidden="<?php print $intNumFoto == 1 ? "true" : "false"?>" style=" margin: 0px; padding: 0px;  ">
                                <div class="gallery text-center lis-relative classPrueba imgGalery_<?php print $intNumFoto;?> <?php print $intNumFoto == 1 ? "slideImgActiva" : "slideImgNoActiva"?>" style="margin: 5px;   ">
                                    <img id="imgGalery_<?php print $intNumFoto;?>" src="<?php print $rTMP["value"]["url_imagen"]?>" alt="" class="img-fluid slideImgNoActivaIMG" style="" >
                                    <div class="gallery-fade fade w-100 h-100 rounded">
                                        <div class="d-table w-100 h-100">
                                            <div class="d-table-cell align-middle">
                                                <h4 class="mb-0"><a data-toggle="lightbox" data-gallery="example-gallery" href="dist/images/gallery<?php print $intNumFoto;?>.jpg" class="text-white">
                                                    <?php print $intNumFoto;?>
                                                    <i class="fa fa-search-plus"></i></a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $intNumFoto++;                    
                        }
                        
                    }
                    else{
                        $arrPlace["num_galeria"] = 5;
                        $intNumFoto = 1;
                        for( $intNumFoto = 1; $intNumFoto <= $arrPlace["num_galeria"] ; $intNumFoto++ ){
                        
                            ?>
                            <div  aria-hidden="<?php print $intNumFoto == 1 ? "true" : "false"?>" style=" margin: 0px; padding: 0px;  ">
                                <div class="gallery text-center lis-relative classPrueba imgGalery_<?php print $intNumFoto;?> <?php print $intNumFoto == 1 ? "slideImgActiva" : "slideImgNoActiva"?>" style="margin: 5px;   ">
                                    <img id="imgGalery_<?php print $intNumFoto;?>" src="images/Noimagee.jpg?1" alt="" class="img-fluid slideImgNoActivaIMG" style="" >
                                    <div class="gallery-fade fade w-100 h-100 rounded">
                                        <div class="d-table w-100 h-100">
                                            <div class="d-table-cell align-middle">
                                                <h4 class="mb-0"><a data-toggle="lightbox" data-gallery="example-gallery" href="images/Noimagee.jpg?1" class="text-white">
                                                    <?php print $intNumFoto;?>
                                                    <i class="fa fa-search-plus"></i></a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            
                        }
                        
                    }
                        
                    ?>
                      
                </div>                  
            </div>
                
        </div>
    </div>
    <!--End header -->
    
    
 
    
    <!-- Profile header -->
    <div class="profile-header " id="divSeccionDatos">
        <div class="container " id="divContenidoSeccion_2" style="">
            
            <div class="row justify-content-center wow fadeInDown">
                <div class="col-12 col-md-8 mb-0 mb-lg-0 ">
                    
                    
                    
                    <a href="#" class="text-dark">
                       <div class="media d-block d-md-flex text-md-right text-center   lis-relative mt-lg-2">
                           <img style="display: none; width: 100px; height: 100px;" src="<?php print $arrPlace["url_logo"]?>" class="img-fluid d-md-flex ml-md-4 border border-white lis-border-width-4 rounded mb-4 mb-md-0" alt="" />
                            <div class="media-body align-self-center">
                                
                                <h2 class="text-dark font-weight-bold lis-line-height-1 d-none d-sm-block" >
                                
                                    <i class="fa fa-heart-o" onclick="fntDisplayIconoFavorito('iconProFavoritoSi', 'iconProFavoritoNo', true, '<?php print fntCoreEncrypt($intPlace)?>');" id="iconProFavoritoNo" style="font-size: 20px; cursor: pointer; color: red; <?php print intval($arrPlace["id_usuario_place_favorito"]) ? "display: none;" : ""?>"></i>    
                                    <i class="fa fa-heart"   onclick="fntDisplayIconoFavorito('iconProFavoritoSi', 'iconProFavoritoNo', false, '<?php print fntCoreEncrypt($intPlace)?>');" id="iconProFavoritoSi" style="font-size: 20px; cursor: pointer; color: red; <?php print intval($arrPlace["id_usuario_place_favorito"]) ? "" : "display: none;"?>"></i>
                                    
                                    <?php print $arrPlace["titulo"]?>
                                    
                                </h2>
                                <br class="d-block d-sm-none">
                                <p class="mb-0">
                                    <br class="d-block d-sm-none">
                                    <?php print !empty($arrPlace["subTitulo"]) ? $arrPlace["subTitulo"] : "Add your Subtitle"?>
                                </p>
                                <ul class="list-inline my-0 text-center text-md-right  m-o p-0 ">
                                    <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["facebook"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-facebook"></i></a></li>
                                    <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["twitter"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-twitter"></i></a></li>
                                    <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["instagram"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-instagram"></i></a></li>
                                    <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["youtube"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-youtube"></i></a></li>
                                
                                </ul>
                            </div>
                            
                        </div>     
                    </a>
                </div>
                <div class="col-12 col-md-4 align-self-center ">
                    <ul class="list-unstyled mb-0 lis-line-height-2 text-md-left text-center m-o p-0">
                        <li>
                            <a href="tel:<?php print $arrPlace["telefono"]?>" class="text-dark">
                                <i class="fa fa-phone pl-2 text-dark"></i><?php print !empty($arrPlace["telefono"]) ? $arrPlace["telefono"] : "Add your Phone"?></li>
                            </a>
                        <li>
                            <a href="mailto:<?php print $arrPlace["email"]?>" class="text-dark">
                                <i class="fa fa-envelope pl-2"></i><?php print !empty($arrPlace["email"]) ? $arrPlace["email"] : "Add your Email"?>
                            </A>
                        </li>
                        <li>
                            
                            <a href="https://www.google.com/maps?q=<?php print $arrPlace["direccion"]?>" target="_blank" class="text-dark">
                                <i class="fa fa-map-o pl-2"></i> <?php print !empty($arrPlace["direccion"]) ? $arrPlace["direccion"] : "Add your Addresss"?> 
                            </a>
                            
                        </li> 
                    </ul>
                    
                </div>
            </div>    
            
        </div>
    </div>
    <!-- End header -->
           
    <!-- Profile Content -->
    <section class=" pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xs-12 mb-lg-0 text-right">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile"> 
                        
                        <div class="d-block d-sm-none">
                            <h6 class="lis-font-weight-500">
                                    
                                <i class="fa fa-align-right pl-2 lis-f-14"></i> Relevant
                            
                            </h6> 
                            <div id="divContenidoRelevante2" class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground ">
                                <?php print $arrPlace["relevante"]?>    
                            </div>
                        </div>
                        <h6 class="lis-font-weight-500">
                                
                            <i class="fa fa-align-right pl-2 lis-f-14"></i> Description
                        
                        </h6> 
                        <div id="divContenidoSeccion_3" class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground ">
                            <div class="card-body p-4">
                                <p><?php print $arrPlace["descripcion"]?></p>
                            </div>    
                        </div>
                        
                        <h6 class="lis-font-weight-500"><i class="fa fa-chain pl-2 lis-f-14"></i> Products</h6>
                          
                        <?php
                        
                        while( $rTMP = each($arrPlaceClasificacionPadre) ){
                            
                            ?>
                            <h6 class="lis-font-weight-500 text-left">
                                <i class="fa fa-chain pl-2 lis-f-14"></i> 
                                <?php print $rTMP["value"]["tag_en"]?>
                            </h6>   
                            <hr>
                            <?php    
                            if( isset($arrClasificacionProducto[$rTMP["key"]]) ){
                                
                                $arrPlaceProducto = $arrClasificacionProducto[$rTMP["key"]];
                                
                                while( $rTMP = each($arrPlaceProducto) ){
                                    ?>
                                    <div class="row" style="cursor: pointer;" onclick="window.open('pro.php?iden=<?php print fntCoreEncrypt($rTMP["key"])?>')">
                                                                    
                                        <div class="col-9 m-0 p-0 pt-1 text-left" style="vertical-align: middle;">
                                            <h5 class="text-dark  lis-line-height-1 m-0 p-0" style="font-size: 16px;">
                                                
                                                <?php
                                                
                                                if( $rTMP["value"]["destacado"] == "Y" ){
                                                    ?>
                                                    
                                                    <i class="fa fa-star pl-2 lis-f-14"></i>
                                                    
                                                    <?php
                                                }
                                                
                                                ?>
                                                    
                                                <?php print $rTMP["value"]["nombre"]?>
                                                
                                                
                                            </h5>
                                            <h5 class="  lis-line-height-1 m-0 p-0" style="font-size: 14px;">
                                                <?php print $rTMP["value"]["descripcion"]?>
                                            </h5>
                                            <h5 class="lis-primary  lis-line-height-1 m-0 p-0" style="font-size: 14px;">
                                                Q <?php print $rTMP["value"]["precio"]?>
                                            </h5>
                                            <?php
                                                
                                            if( isset($rTMP["value"]["clasificacion_padre_hijo"]) && count($rTMP["value"]["clasificacion_padre_hijo"]) > 0 ){
                                                
                                                ?>
                                                <h3 class="lis-primary   m-0 p-0" style="font-size: 16px;">
                                                    
                                                <?php
                                                while( $arrTMP = each($rTMP["value"]["clasificacion_padre_hijo"]) ) {
                                                    
                                                    ?>
                                                    
                                                    <span class="badge  text-white" style="background-color: #dc3545;">
                                                        <?php print $arrTMP["value"]["texto"]?>
                                                    </span>
                                                    <?php
                                                }
                                                ?>
                                                </h3>
                                                                            
                                                <?php
                                                
                                            }
                                            
                                            ?>
                                                    
                                        </div>
                                        <div class="col-3 m-0 p-0">
                                            <img src="<?php print $rTMP["value"]["logo_url"]?>" style="width: 140px; height: 110px;" class="img-fluid d-md-fle  border border-white lis-border-width-4 rounded mb-4 mb-md-0" alt="" />
                                               
                                        </div>
                                    </div>        
                                    <?php
                                }    
                                
                                    
                                                                
                            }
                            
                            ?>
                            
                            <div class="divCategoriaProducto"  id="divContenidoCategoriaProducto_<?php print $rTMP["key"]?>">
                                
                            </div>
                            
                            <hr>
                            <?php
                        }
                        
                        ?>
                               
                        
                        <h6 class="lis-font-weight-500">
                            <i class="fa fa-chain pl-2 lis-f-14"></i> 
                            Amenities
                        </h6>
                        
                        <div id="divContenidoSeccion_4" class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground">
                            <div class="card-body p-4">    
                                <div class="row">
                                                
                                    <?php
                                    $arrPlaceAmenidad = fntGetPlaceAmenidad($intPlace, true, $objDBClass);
        
                                    
                                    $intCount = 1;
                                    while( $rTMP = each($arrPlaceAmenidad) ){
                                        
                                        if( $intCount == 1 ){
                                            ?>
                                            <div class="col-lg-4">
                                                <ul class="list-unstyled lis-line-height-2 mb-0">
                                    
                                            <?php
                                        }
                                        
                                        ?>
                                        <li><i class="fa fa-check-square pl-2 lis-primary"></i> <?php print $rTMP["value"]["tag_en"]?></li>
                                            
                                        <?php
                                        
                                        if( $intCount == 4 ){
                                            ?>
                                                    </ul>
                                            </div>
                                            <?php
                                            $intCount = 0;
                                        }
                                        
                                        
                                        $intCount++;                        
                                    }
                                    
                                    if( $intCount <= 4 ){
                                        ?>
                                                </ul>
                                            </div>
                                        <?php
                                    }
                                    
                                    ?>         
                                    
                                </div>    
                            </div>    
                        </div>
                        
                        <h6 class="lis-font-weight-500">
                            
                            Location
                            <i class="fa fa-map-o pl-2 lis-f-14"></i> 
                            
                        </h6>
                        <div class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground">
                            <div class="card-body p-1" style="direction: ltr;">    
                                
                                <style>
                                  #map {
                                    height: 100%;
                                  }
                                 
                                  .controls {
                                    margin-top: 10px;
                                    border: 1px solid transparent;
                                    border-radius: 2px 0 0 2px;
                                    box-sizing: border-box;
                                    -moz-box-sizing: border-box;
                                    height: 32px;
                                    outline: none;
                                    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                                  }

                                  #pac-input {
                                    background-color: #fff;
                                    font-family: Roboto;
                                    font-size: 15px;
                                    font-weight: 300;
                                    margin-left: 12px;
                                    padding: 0 11px 0 13px;
                                    text-overflow: ellipsis;
                                    width: 300px;
                                  }

                                  #pac-input:focus {
                                    border-color: #4d90fe;
                                  }

                                  .pac-container {
                                    font-family: Roboto;
                                  }

                                  #type-selector {
                                    color: #fff;
                                    background-color: #4d90fe;
                                    padding: 5px 11px 0px 11px;
                                  }

                                  #type-selector label {
                                    font-family: Roboto;
                                    font-size: 13px;
                                    font-weight: 300;
                                  }
                                </style>
                            
                                <input type="hidden" name="hidLngUbicacion" id="hidLngUbicacion">
                                <input type="hidden" name="hidLatUbicacion" id="hidLatUbicacion">
                                <input type="hidden" name="hidPlaceId" id="hidPlaceId">
                                <input id="pac-input" class="controls" type="text"
                                    placeholder="Enter a location">
                                <div id="type-selector" class="controls">
                                  <input type="radio" name="type" id="changetype-all" checked="checked">
                                  <label for="changetype-all">All</label>

                                  <input type="radio" name="type" id="changetype-establishment">
                                  <label for="changetype-establishment">Establishments</label>

                                  <input type="radio" name="type" id="changetype-address">
                                  <label for="changetype-address">Addresses</label>

                                  <input type="radio" name="type" id="changetype-geocode">
                                  <label for="changetype-geocode">Geocodes</label>
                                </div>
                                <div id="map-canvas" style="height: 400px;"></div>
                                 
                                
                                
                            </div>
                        </div>
                                 
                    </div>
                </div>
                <div class="col-12 col-lg-4 text-right">
                    
                    <div class="d-none d-sm-block">
                    
                        <h6 class="lis-font-weight-500">
                            <i class="fa fa-star pl-2 lis-f-14"></i> 
                            Relevant
                        </h6>
                        <div class="card lis-brd-light wow fadeInUp mb-4">
                            <div class="card-body p-4" >
                                <p style="font-weight: bold;" id="divContenidoRelevante">
                                    <?php print $arrPlace["relevante"]?>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <h6 class="lis-font-weight-500"><i class="fa fa-star pl-2 lis-f-14"></i> Listing Ratings</h6>
                    <div class="card lis-brd-light wow fadeInUp mb-4" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="card-body p-4">
                            <ul class="list-inline mb-0 lis-light-gold font-weight-normal h4">
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                <li class="list-inline-item"> 3.0</li>
                            </ul>
                            <small>2.77 average based on 19184 ratings</small>
                        </div>
                    </div>
                    
                    <h6 class="lis-font-weight-500">
                        <i class="fa fa-clock-o pl-2 lis-f-14"></i> 
                        Opening Hours
                    </h6>
                    <div id="divContenidoSeccion_5" class="card lis-brd-light wow fadeInUp mb-4">
                        <div class="card-body p-4" style="direction: ltr;">
                            <?php
                            $arrPlaceHorario = fntGetPlaceHorario($intPlace, true, $objDBClass);
        
                            $arrDiasSemana = getSemana();
                            $intDiaSemana = 0;
            
                            if( isset($arrPlaceHorario[$intDiaSemana]) ){
                                
                                ?>
                                <button class="text-btn bg-transparent border-0  w-100 text-left collapsed px-0 lis-light" data-toggle="collapse" data-target="#demo">
                                    
                                    
                                    
                                    <?php
                                    
                                    if( $arrPlaceHorario[$intDiaSemana]["cerrado"] == "Y" ){
                                        ?>
                                        <span class='text-danger font-weight-bold'>Closed Today</span>
                                        
                                        <?php
                                    }
                                    else{
                                        ?>
                                        <span class='lis-light-green font-weight-bold'>Open Today</span> : <?php print $arrPlaceHorario[$intDiaSemana]["hora_inicio_text"]?> - <?php print $arrPlaceHorario[$intDiaSemana]["hora_fin_text"]?>
                                    
                                        <?php
                                    }
                                    
                                    
                                    ?>
                                    
                                </button>
                                
                                <?php    
                            }   
                            
                            ?>
                            <div id="demo" class="collapse">
                                <dl class="row mb-0 mt-4 lis-line-height-2">
                                    <?php
                                    
                                    while( $rTMP = each($arrPlaceHorario) ){
                                        
                                        ?>
                                        <dt class="col-sm-6 font-weight-normal"><?php print $arrDiasSemana[$rTMP["key"]]["nombre"]?></dt>
                                        
                                        <?php
                                        if( $rTMP["value"]["cerrado"] == "Y" ){
                                            ?>
                                            <dd class="col-sm-6">Closed</dd>
                                    
                                            <?php
                                        }
                                        else{
                                            ?>
                                            <dd class="col-sm-6"><?php print $rTMP["value"]["hora_inicio_text"];?> - <?php print $rTMP["value"]["hora_fin_text"];?></dd>
                                    
                                            <?php
                                        }
                                    }
                                    
                                    ?>
                                    
                                </dl>
                            </div>
                        </div>
                    </div>
                    
                    <h6 class="lis-font-weight-500">
                        
                        <i class="fa fa-tags pl-2 lis-f-14"></i> 
                        Categories
                    </h6>
                    <div class="card lis-brd-light wow fadeInUp mb-4">
                        <div class="card-body p-4">
                            <ul class="list-inline mb-0 lis-light-gold font-weight-normal h4">
                             
                            <?php 
                            
                            while( $rTMP = each($arrPlaceClasificacion) ){
                                
                                ?>
                                <li class="list-inline-item">
                                     <div style="background-color: <?php print "#".$rTMP["value"]["color_hex"]?>; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                        <img src="<?php print $rTMP["value"]["url_img"]?>">
                                    </div>
                                </li>
                                <?php
                                
                            }
                            
                            ?>
                        
                            </ul>  
                                
                            
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
    </section>
     <!-- End Profile Content -->
    
                
    
     <!-- Footer-->
    <section class="image-bg footer lis-grediant grediant-bt pb-0">
        <div class="background-image-maker"></div>
        <div class="holder-image">
            <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none">
        </div>
        <div class="container">
            <div class="row pb-5">
                <div class="col-12 col-md-8">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                            <h5 class="footer-head">Useful Links</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                            <h5 class="footer-head">My Account</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Dashboard</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Profile</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> My Listing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Favorites</A></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                            <h5 class="footer-head">Pages</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <h5 class="footer-head">Help</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-logo">
                        <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a> 
                    </div>
                    <p class="my-4">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu in  felis eu pede mollis enim.</p>
                    <a href="#" class="text-white"><u>App Download</u></a> 
                    <ul class="list-inline mb-0 mt-2">
                        <li class="list-inline-item"><A href="#"><img src="dist/images/play-store.png" alt="" class="img-fluid" /></a></li>
                        <li class="list-inline-item"><A href="#"><img src="dist/images/google-play.png" alt="" class="img-fluid" /></a></li>
                        <li class="list-inline-item"><A href="#"><img src="dist/images/windows.png" alt="" class="img-fluid" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom mt-5 py-4">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 text-center text-md-right mb-3 mb-md-0">
                        <span> &copy; 2017 Lister. Powered by <a href="#" class="lis-primary">PSD2allconversion.</a></span>
                    </div> 
                    <div class="col-12 col-md-6 text-center text-md-left">
                        <ul class="list-inline footer-social mb-0">
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item"><A href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div> 
            </div>
        </div>
    </section>
    <!--End  Footer-->

    
               
    <!-- Top To Bottom-->
    <!-- Top To Bottom-->
    <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
        <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
    </a>
    <!-- End Top To Bottom-->
    
    <!-- Button trigger modal -->
    
            
    <div class="container">

        <div id="modalFormContendio" class="popupContainer" style="display: none; width: 90%; height: 90%; overflow: auto;">
            

        </div>

    </div>

    <!-- jQuery -->                          
    <script src="dist/js/plugins.min.js"></script>
    <script src="dist/js/common.js"></script>
    <script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
       
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMWRurR0tA9aejnmohWlKtmuo_bjxjfdc&libraries=places"></script>
    <script src="dist/js/jsAntigua.js"></script>
    <script src="dist/clockpicker/jquery-clockpicker.min.js"></script>
    <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>

    <script>   
        
        
        <?php
        
        if( !empty($arrPlace["ubicacion_lng"]) ){
            ?>
            var center = new google.maps.LatLng(<?php print $arrPlace["ubicacion_lat"];?>, <?php print $arrPlace["ubicacion_lng"];?>);
            
            <?php
        }
        else{
            ?>
            var center = new google.maps.LatLng(14.5585707, -90.72952299999997);
            
            <?php
        }
        
        ?>
                
        var markerInicial;
        var map;
        
        var d = new Date();
        var intDiaSemana = d.getDay()
        
        
        $( document ).ready(function() {
            
            $('.sliderInicio').slick({ 
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 4,
                arrows: false,
                <?php print $boolMovil ? "ltr: true," : "ltr: true,"?>
                <?php print $boolMovil ? "autoplay: false," : "autoplay: true,"?>
                 <?php print $boolMovil ? "speed: 100," : "speed: 1500,"?>
                responsive: [
                    {
                        breakpoint: 1199,
                        settings: {
                            arrows: false,
                            centerMode: false,
                            centerPadding: '0px',
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            infinite: false,
                            arrows: false,
                            centerMode: false,
                            centerPadding: '0px',
                            speed: 100,
                            slidesToShow: 1.5,
                            slidesToScoll: 1.5,
                            
                        }
                    }
                ]   
            });

            arrOrden = new Array();
            intReal = 1;
            intTotal = <?php print $arrPlace["num_galeria"];?>;
            for( i = (intTotal/2) ; i >= 0; i-- ){
                index = i ;
                
                arrOrden[index] = new Array();
                arrOrden[index]["index"] = index;
                arrOrden[index]["real"] = intReal;
                intReal++;
            }
              
            for( i = intTotal  ; i > (intTotal/2) + 1; i-- ){
                index = i - 1;
                arrOrden[index] = new Array();
                arrOrden[index]["index"] = index;
                arrOrden[index]["real"] = intReal;
                intReal++;
            }
            
            $('.sliderInicio').on('afterChange', function(event, slick, currentSlide, nextSlide){
                var dataId = $('.slick-current').attr("data-slick-index");    
              
              
                dataId++;
                indexReal = dataId;                  
              $('.classPrueba').removeClass('slideImgActiva');        
              $('.classPrueba').addClass('slideImgNoActiva');        
              $('.imgGalery_'+indexReal).removeClass('slideImgNoActiva');        
              $('.imgGalery_'+indexReal).addClass('slideImgActiva');        
              
              
            });
            
            $('.sliderInicio').slick('slickGoTo',"0");
     
            
            $("#divSeccionDatos").css("padding-top", ( parseFloat($("#header-fix").height()) <?php print !$boolMovil ? "+ parseFloat($('.sliderInicio').height() / 2)" : ""?> )+"px");
            
            initializeMap();
            
        }); 
                     
        function initializeMap() {

            var mapOptions = {
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: center
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            <?php
        
            if( empty($arrPlace["ubicacion_lng"]) ){
                ?>
                if (navigator.geolocation) {
                    
                    navigator.geolocation.getCurrentPosition(function(position) {
                        
                        
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        //console.log(position);    
                        var markerOptions = { 
                            position: pos, 
                            draggable: true
                        }
                        
                        markerInicial = new google.maps.Marker(markerOptions);
                        markerInicial.setMap(map);
                        
                        map.setCenter(pos);
                        
                        $("#hidLatUbicacion").val(position.coords.latitude);
                        $("#hidLngUbicacion").val(position.coords.longitude);
                        
                        
                        
                        
                    }, function() {
                        
                        handleLocationError(true, infoWindow, map.getCenter());
                        
                    });
                    
                } 
                else {
                    
                    handleLocationError(false, infoWindow, map.getCenter());
                    
                }
                <?php
            }
            else{
                ?>
                 //console.log(position);    
                var markerOptions = { 
                    position: center, 
                    draggable: true
                }
                
                markerInicial = new google.maps.Marker(markerOptions);
                markerInicial.setMap(map);
                
                map.setCenter(center);
                map.setZoom(16);
                                          
                $("#hidLatUbicacion").val(<?php print $arrPlace["ubicacion_lat"];?>);
                $("#hidLngUbicacion").val(<?php print $arrPlace["ubicacion_lng"];?>);
                
                <?php
            }
            
            ?>
                
            
            var input = /** @type {!HTMLInputElement} */(
                document.getElementById('pac-input'));

            var types = document.getElementById('type-selector');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29),
              draggable: true
            });

            autocomplete.addListener('place_changed', function() {
              infowindow.close();
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }
              else{
                  markerInicial.setMap(null);
              }

              
              //console.log(place);
              //console.log(place.place_id);
              $("#hidPlaceId").val(place.place_id);
              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
                
                $("#hidLatUbicacion").val(place.geometry.location.lat);
                $("#hidLngUbicacion").val(place.geometry.location.lng);
                
              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
                $("#hidLatUbicacion").val(place.geometry.location.lat);
                $("#hidLngUbicacion").val(place.geometry.location.lng);
                
              }
              
              
              //console.log($("#hidLatUbicacion").val());
              //console.log($("#hidLngUbicacion").val());
                
              
              marker.setPosition(place.geometry.location);
              marker.setVisible(true);
              
              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }

              infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
              infowindow.open(map, marker);
            });

            // Sets a listener on a radio button to change the filter type on Places
            // Autocomplete.
            function setupClickListener(id, types) {
              var radioButton = document.getElementById(id);
              radioButton.addEventListener('click', function() {
                autocomplete.setTypes(types);
              });
            }

            setupClickListener('changetype-all', []);
            setupClickListener('changetype-address', ['address']);
            setupClickListener('changetype-establishment', ['establishment']);
            setupClickListener('changetype-geocode', ['geocode']);
               
        }
                  
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                  'Error: The Geolocation service failed.' :
                                  'Error: Your browser doesn\'t support geolocation.');
        }
        
        function fntDisplayIconoFavorito(idObjSi, idObjNo, boolFavorito, strKey){
            
            if( boolFavorito ){
                
                $("#"+idObjSi).show();
                $("#"+idObjNo).hide();
                
            }
            else{
                
                $("#"+idObjSi).hide();
                $("#"+idObjNo).show();
                
            }
            
            $.ajax({
                url: "servicio_core.php?servicio=setPlaceFavorito&key="+strKey+"&fav="+( boolFavorito ? "Y" : "N" ), 
                dataType : "json",
                success: function(result){
                
                }
            });
            
                        
        }
        
                                 
    </script>   
 </body>

</html>