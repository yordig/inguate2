<?php
include "core/function_servicio.php";

$boolInterno = isset($_GET["inter"]) && $_GET["inter"] == "true" ? true : false;
$intPlace = isset($_GET["p"]) ? intval(fntCoreDecrypt($_GET["p"])) : 0;

$intMaxProductoDestacado = 10;
$intMaxClasificacionDestacada = 2;
$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

if( isset($_GET["getDrawClas2"]) ){
            
    $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
    
    $arrInfo = fntClasificacionPadre(false, null, $strKey);
        
    ?>
    
    <div class="row">
        <div class="col-5 offset-3">
        
            <div class="form-group">
                <label for="slcClasificacionPadre"><small>Secondary Classification</small></label>
                <div class="input-group">
                    <select id="slcClasificacionPadre" name="slcClasificacionPadre[]" class="form-control-sm select2-multiple" multiple>
                        <?php
                    
                        while( $rTMP = each($arrInfo) ){
                            
                            ?>
                            <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                <?php
                                
                                while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                    ?>
                                    <option value="<?php print $arrTMP["value"]["id_clasificacion_padre"]?>"><?php print $arrTMP["value"]["tag_enPadre"]?></option>
                            
                                    <?php
                                }
                                
                                ?>
                            </optgroup>
                            <?php
                            
                        }
                        
                        ?>
                    </select>
                    <button class="btn btn-raised btn-sm btn-success ml-1" id="btnNextClasificacionPrincipalPadre" onclick="fntDrawClas3();">Next</button>
                </div>
            </div>
            
        </div>
        
    </div>
    <script>
        
        $( document ).ready(function() {
            
            
            $("#slcClasificacionPadre").select2({
              theme: 'bootstrap4',
              width: 'style',
              placeholder: $(this).attr('placeholder'),
              allowClear: Boolean($(this).data('allow-clear')),
            });
            
            $("#btnNextClasificacionPrincipal").hide();
            
            $("#slcClasificacionPadre").change(function (){
                
                $("#btnNextClasificacionPrincipalPadre").show();
                $("#divClasificacionDetalle").html("");
                $("#divClasificacionDetalleOtros").html("");
            
            });
            
            
        });
        
        function fntDrawClas3(){
                         
            strClass1 = "";
            $("#slcClasificacionPadre option:selected").each(function(){
                
                strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                
            });          
            
            if( strClass1 == "" ){
                
                $.snackbar({
                    content: "Select An Option", 
                    timeout: 1000
                }); 
                
                return false;   
                
            } 
            
            var formData = new FormData();
            
            formData.append("keys", strClass1);
                
            $(".preloader").fadeIn();
            
            $.ajax({
                url: "place_admin.php?getDrawClas3=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                    $("#divClasificacionDetalle").html(result);
                    
                }
                        
            });
            
            return false;                
        }
        
    </script>
    
    <?php
    
    die();
    
}

if( isset($_GET["getDrawClas3"]) ){
    
    $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
    
    $arrInfo = fntClasificacionPadreHijo(false, true, $strKey);
    
    ?>
        
    <div class="row">
        <div class="col-5 offset-3">
        
            <div class="form-group">
                <label for="slcClasificacionPadreHijo"><small>Detail Classification</small></label>
                <div class="input-group">
                    <select id="slcClasificacionPadreHijo" name="slcClasificacionPadreHijo[]" class="form-control-sm select2-multiple" multiple>
                        <?php
                    
                        while( $rTMP = each($arrInfo) ){
                            
                            ?>
                            <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                <?php
                                
                                while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                    ?>
                                    <option value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo"]?>"><?php print $arrTMP["value"]["tag_enHijo"]?></option>
                            
                                    <?php
                                }
                                
                                ?>
                            </optgroup>
                            <?php
                            
                        }
                        
                        ?>
                    </select>
                    <button class="btn btn-raised btn-sm btn-success ml-1" id="btnNextClasificacionPrincipalPadreHijo" onclick="fntDrawClas4();">Next</button>
                </div>
            </div>
            
        </div>
        
    </div>
    <script>
        
        $( document ).ready(function() {
            
            
            $("#slcClasificacionPadreHijo").select2({
              theme: 'bootstrap4',
              width: 'style',
              placeholder: $(this).attr('placeholder'),
              allowClear: Boolean($(this).data('allow-clear')),
            });
            
            $("#btnNextClasificacionPrincipalPadre").hide();
            
            $("#slcClasificacionPadreHijo").change(function (){
                
                $("#btnNextClasificacionPrincipalPadreHijo").show();
                $("#divClasificacionDetalleOtros").html("");
            
            });
            
            
        });
        
        function fntDrawClas4(){
                         
            strClass1 = "";
            $("#slcClasificacionPadreHijo option:selected").each(function(){
                
                strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                
            });          
            
            if( strClass1 == "" ){
                
                $.snackbar({
                    content: "Select An Option", 
                    timeout: 1000
                }); 
                
                return false;   
                
            } 
            
            var formData = new FormData();
            
            formData.append("keys", strClass1);
                
            $(".preloader").fadeIn();
            
            $.ajax({
                url: "place_admin.php?getDrawClas4=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                    $("#divClasificacionDetalleOtros").html(result);
                    
                }
                        
            });
            
            return false;                
        }
        
        
    </script>
    
    <?php
        
    die();
    
}

if( isset($_GET["getDrawClas4"]) ){
    
    $strKey = isset($_POST["keys"]) ? trim($_POST["keys"]) : "";
    
    $arrInfo = fntClasificacionPadreHijoDetalle(null, true, $strKey);
        
    ?>
    
    <div class="row">
        <div class="col-5 offset-3">
        
            <div class="form-group">
                <label for="slcClasificacionPadreHijo"><small>Other Classification</small></label>
                <div class="input-group">
                    <select id="slcClasificacionPadreHijoDetalle" name="slcClasificacionPadreHijoDetalle[]" class="form-control-sm select2-multiple" multiple>
                        <?php
                    
                        while( $rTMP = each($arrInfo) ){
                            
                            ?>
                            <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                                <?php
                                
                                while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                                    ?>
                                    <option value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo_detalle"]?>"><?php print $arrTMP["value"]["tag_enDetalle"]?></option>
                            
                                    <?php
                                }
                                
                                ?>
                            </optgroup>
                            <?php
                            
                        }
                        
                        ?>
                    </select>
                </div>
            </div>
            
        </div>
        
    </div>
    <script>
        
        $( document ).ready(function() {
            
            
            $("#slcClasificacionPadreHijoDetalle").select2({
              theme: 'bootstrap4',
              width: 'style',
              placeholder: $(this).attr('placeholder'),
              allowClear: Boolean($(this).data('allow-clear')),
            });
            
            $("#btnNextClasificacionPrincipalPadreHijo").hide();
            
            
        });
        
        
    </script>
    
    <?php
    die();
    
}

if( isset($_GET["setSolPlaceRechazada"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $intSolPlace = isset($_POST["txtKeyPlaceSol"]) ? fntCoreDecrypt($_POST["txtKeyPlaceSol"]) : 0;
    
    $strQuery = "UPDATE sol_place
                 SET    estado = 'S'
                 WHERE  id_sol_place = {$intSolPlace} ";
    $objDBClass->db_consulta($strQuery);
    
    die();
}

if( isset($_GET["setSaveProductoClasificacionPadre"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
        
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceClasificacionPadre = isset($_POST["hidPlaceClasificacionPadre"]) ? intval(fntCoreDecrypt($_POST["hidPlaceClasificacionPadre"])) : 0;
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "hidIdPlaceProducto" ){
            
            $intPlaceProducto = isset($_POST["hidIdPlaceProducto_{$arrExplode[1]}"]) ? intval($_POST["hidIdPlaceProducto_{$arrExplode[1]}"]) : "";
            $strNombre = isset($_POST["txtNombre_{$arrExplode[1]}"]) ? addslashes($_POST["txtNombre_{$arrExplode[1]}"]) : "";
            $strPrecio = isset($_POST["txtPrecio_{$arrExplode[1]}"]) ? addslashes($_POST["txtPrecio_{$arrExplode[1]}"]) : "";
            $strDescripcion = isset($_POST["txtDescripcion_{$arrExplode[1]}"]) ? addslashes($_POST["txtDescripcion_{$arrExplode[1]}"]) : "";
            $boolProductoInset = isset($_POST["hidProductoInsert_{$arrExplode[1]}"]) && trim($_POST["hidProductoInsert_{$arrExplode[1]}"]) == "Y" ? true : false;
            $boolProductoDelete = isset($_POST["hidProductoDelete_{$arrExplode[1]}"]) && trim($_POST["hidProductoDelete_{$arrExplode[1]}"]) == "Y" ? true : false;
            $boolProductoEdit = isset($_POST["hidProductoUpdate_{$arrExplode[1]}"]) && trim($_POST["hidProductoUpdate_{$arrExplode[1]}"]) == "Y" ? true : false;
            
            if( $boolProductoDelete ){
                
                $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo WHERE id_place_producto = {$intPlaceProducto}";
                $objDBClass->db_consulta($strQuery);
                
                $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo_detalle WHERE id_place_producto = {$intPlaceProducto}";
                $objDBClass->db_consulta($strQuery);
                
                $strUrlArchivo = "images/place/product/".fntCoreEncrypt($intPlaceProducto).".png";
                
                if( file_exists($strUrlArchivo) )
                    unlink($strUrlArchivo);
                
                $strQuery = "DELETE FROM place_producto WHERE id_place_producto = {$intPlaceProducto}";
                $objDBClass->db_consulta($strQuery);
                
            }
            else{
                
                if( $boolProductoInset ){
                    
                    $strQuery = "INSERT INTO place_producto(id_place, id_clasificacion_padre, nombre, precio, descripcion)
                                                     VALUES( {$intPlace}, {$intPlaceClasificacionPadre}, '{$strNombre}', '{$strPrecio}', '{$strDescripcion}' )";
                    $objDBClass->db_consulta($strQuery);
                    $intPlaceProducto = $objDBClass->db_last_id();
                    
                }
                else{
                    
                    $strQuery = "UPDATE place_producto
                                 SET    nombre = '{$strNombre}',
                                        precio = '{$strPrecio}', 
                                        descripcion = '{$strDescripcion}' 
                                 WHERE  id_place_producto = {$intPlaceProducto} ";
                    $objDBClass->db_consulta($strQuery);
                    
                }
                
                if( $intPlaceProducto && ( $boolProductoInset || $boolProductoEdit ) ){
                    
                    if( isset($_FILES["flLogoProducto_{$arrExplode[1]}"]) && $_FILES["flLogoProducto_{$arrExplode[1]}"]["size"] > 0 ){
                        
                        $arrNameFile = explode("_", $_FILES["flLogoProducto_{$arrExplode[1]}"]["name"]); 
                        $strUrlArchivo = "images/place/product/".fntCoreEncrypt($intPlaceProducto).".webp";
                        rename($_FILES["flLogoProducto_{$arrExplode[1]}"]["tmp_name"], $strUrlArchivo);
                        
                        $strQuery = "UPDATE place_producto
                                     SET    logo_url = '{$strUrlArchivo}' 
                                     WHERE  id_place_producto = {$intPlaceProducto} ";
                        $objDBClass->db_consulta($strQuery);
                                
                        fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceProducto));
                            
                    }
                    
                    $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo WHERE id_place_producto = {$intPlaceProducto}";
                    $objDBClass->db_consulta($strQuery);
                    
                    if( isset($_POST["slcClasificacionPadreHijo_{$arrExplode[1]}"]) && count($_POST["slcClasificacionPadreHijo_{$arrExplode[1]}"]) > 0 ){
                        
                        while( $arrTMP = each($_POST["slcClasificacionPadreHijo_{$arrExplode[1]}"]) ){
                            
                            $strQuery = "INSERT INTO place_producto_clasificacion_padre_hijo(id_place_producto, id_clasificacion_padre_hijo)
                                                                                    VALUES({$intPlaceProducto}, {$arrTMP["value"]})";
                            $objDBClass->db_consulta($strQuery);
                        }
                        
                    }
                    
                    $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo_detalle WHERE id_place_producto = {$intPlaceProducto}";
                    $objDBClass->db_consulta($strQuery);
                    
                    if( isset($_POST["slcClasificacionPadreHijoDetalle_{$arrExplode[1]}"]) && count($_POST["slcClasificacionPadreHijoDetalle_{$arrExplode[1]}"]) > 0 ){
                        
                        while( $arrTMP = each($_POST["slcClasificacionPadreHijoDetalle_{$arrExplode[1]}"]) ){
                            
                            $strQuery = "INSERT INTO place_producto_clasificacion_padre_hijo_detalle(id_place_producto, id_clasificacion_padre_hijo_detalle)
                                                                                    VALUES({$intPlaceProducto}, {$arrTMP["value"]})";
                            $objDBClass->db_consulta($strQuery);
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }    
    
    
    die();
}

if( isset($_GET["setGaleriaPlace"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "hidIdGaleria" ){
            
            $intPlaceGaleria = isset($_POST["hidIdGaleria_{$arrExplode[1]}"]) ? intval($_POST["hidIdGaleria_{$arrExplode[1]}"]) : 0;
            $boolUpdate = isset($_POST["hidUpdate_{$arrExplode[1]}"]) && $_POST["hidUpdate_{$arrExplode[1]}"] == "Y" ? true : false;
            $boolDelete = isset($_POST["hidDelete_{$arrExplode[1]}"]) && $_POST["hidDelete_{$arrExplode[1]}"] == "Y" ? true : false;
            $boolInsert = isset($_POST["hidInsert_{$arrExplode[1]}"]) && $_POST["hidInsert_{$arrExplode[1]}"] == "Y" ? true : false;
            
            if( $boolDelete ){
                
                $strQuery = "DELETE FROM place_galeria WHERE id_place_galeria = {$intPlaceGaleria}";
                $objDBClass->db_consulta($strQuery);
                $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".png";
                if( file_exists($strUrlArchivo) )
                    unlink($strUrlArchivo);
                
                                
            }
            else{
                
                if( isset($_FILES["flImagen_{$arrExplode[1]}"]) && $_FILES["flImagen_{$arrExplode[1]}"]["size"] > 0 ){
                    
                    if( !$intPlaceGaleria ){
                        
                        $strQuery = "INSERT INTO place_galeria(id_place)
                                                        VALUES({$intPlace})";
                        $objDBClass->db_consulta($strQuery);
                        $intPlaceGaleria = $objDBClass->db_last_id();
                        
                    }
                    
                    $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".webp";
                    rename($_FILES["flImagen_{$arrExplode[1]}"]["tmp_name"], $strUrlArchivo);
                    
                    $strQuery = "UPDATE place_galeria
                                 SET    url_imagen = '{$strUrlArchivo}' 
                                 WHERE  id_place_galeria = {$intPlaceGaleria}";
                    $objDBClass->db_consulta($strQuery);
                    
                    fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceGaleria));
                            
                    
                }
                
            }
                        
        }
        
    }
        
    die();
}

if( isset($_GET["setPlace"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $strTitulo = isset($_POST["txtTitulo"]) ? addslashes($_POST["txtTitulo"]) : "";
    $strSubTitulo = isset($_POST["txtSubTitulo"]) ? addslashes($_POST["txtSubTitulo"]) : "";
    $strEmail = isset($_POST["txtEmail"]) ? addslashes($_POST["txtEmail"]) : "";
    $strTelefono = isset($_POST["txtTelefono"]) ? addslashes($_POST["txtTelefono"]) : "";
    $strDireccion = isset($_POST["txtDireccion"]) ? addslashes($_POST["txtDireccion"]) : "";
    $strInstagram = isset($_POST["txtInstagram"]) ? addslashes($_POST["txtInstagram"]) : "";
    $strFacebook = isset($_POST["txtFacebook"]) ? addslashes($_POST["txtFacebook"]) : "";
    $strTwitter = isset($_POST["txtTwitter"]) ? addslashes($_POST["txtTwitter"]) : "";
    $strYoutube = isset($_POST["txtYoutube"]) ? addslashes($_POST["txtYoutube"]) : "";
    
    
    $strTituloBusqueda = fntGetNameEstablecimeintoFiltro($strTitulo, true, $objDBClass);
    
    $strFiltroLogo = "";
    
    if( isset($_FILES["flLogo"]) && $_FILES["flLogo"]["size"] > 0 ){
        
        $strUrlArchivo = "images/place/gallery/logo_".fntCoreEncrypt($intPlace).".webp";
        rename($_FILES["flLogo"]["tmp_name"], $strUrlArchivo);
        
        fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlace));
                            
                      
        $strFiltroLogo = ", url_logo = '{$strUrlArchivo}'";        
    }
    
    $strQuery = "UPDATE place
                 SET    titulo = '{$strTitulo}',
                        subTitulo = '{$strSubTitulo}',
                        texto_autocomplete = '{$strTituloBusqueda}',
                        email = '{$strEmail}',
                        telefono = '{$strTelefono}',
                        direccion = '{$strDireccion}',
                        instagram = '{$strInstagram}',
                        facebook = '{$strFacebook}',
                        twitter = '{$strTwitter}',
                        youtube = '{$strYoutube}'
                        {$strFiltroLogo}
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
    
    
        
    die();
}

if( isset($_GET["setPlaceDescripcion"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $strDescripcion = isset($_POST["txtDescipcion"]) ? addslashes($_POST["txtDescipcion"]) : "";
    
    $strQuery = "UPDATE place
                 SET    descripcion = '{$strDescripcion}'
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
        
    die();
}

if( isset($_GET["setPlaceRelevante"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $strRelevante = isset($_POST["txtRelevante"]) ? addslashes($_POST["txtRelevante"]) : "";
    
    $strQuery = "UPDATE place
                 SET    relevante = '{$strRelevante}'
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
        
    die();
}

if( isset($_GET["setPlaceHorario"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "hidKeyDia" ){
            
            $intPlaceHorario = isset($_POST["hidPlaceHorario_{$arrExplode[1]}"]) ? intval($_POST["hidPlaceHorario_{$arrExplode[1]}"]) : 0;
            $intKeyDia = isset($_POST["hidKeyDia_{$arrExplode[1]}"]) ? intval($_POST["hidKeyDia_{$arrExplode[1]}"]) : 0;
            $strClose = isset($_POST["chkClose_{$arrExplode[1]}"]) ? "Y" : "N";
            $intHoraInicio = isset($_POST["slcHorarioInicio_{$arrExplode[1]}"]) ? trim($_POST["slcHorarioInicio_{$arrExplode[1]}"]) : 0;
            $intHoraFin = isset($_POST["slcHorarioFin_{$arrExplode[1]}"]) ? trim($_POST["slcHorarioFin_{$arrExplode[1]}"]) : 0;
                       
            $intHoraInicio = $intHoraInicio.":00";
            $intHoraFin = $intHoraFin.":00";
            
            if( !$intPlaceHorario ){
                
                $strQuery = "INSERT INTO place_horario (id_place, id_dia, hora_inicio, hora_fin, cerrado)
                                                VALUES( {$intPlace}, {$intKeyDia}, '{$intHoraInicio}', '{$intHoraFin}', '{$strClose}')";
                $objDBClass->db_consulta($strQuery);                
            }
            else{
                
                $strQuery = "UPDATE place_horario
                             SET    id_dia = '{$intKeyDia}',
                                    hora_inicio = '{$intHoraInicio}',
                                    hora_fin = '{$intHoraFin}',
                                    cerrado = '{$strClose}'
                             WHERE  id_place_horario = {$intPlaceHorario} ";
                $objDBClass->db_consulta($strQuery);
                                
            }
            
            
        }
        
    }
    
    
            
    die();
}

if( isset($_GET["setPlaceAmenidad"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $strQuery = "DELETE FROM place_amenidad WHERE id_place = {$intPlace} ";
    $objDBClass->db_consulta($strQuery);
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "chkAmenidad" ){
            
            $strQuery = "INSERT INTO place_amenidad(id_place, id_amenidad)
                                            VALUES({$intPlace}, {$arrExplode[1]})";
            $objDBClass->db_consulta($strQuery);
            
        }
        
    }
    
    die();    
    
}

if( isset($_GET["setPlaceProductoRelevante"]) ) {
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceClasificacionPadre = isset($_POST["hidPlaceClasificacionPadre"]) ? intval(fntCoreDecrypt($_POST["hidPlaceClasificacionPadre"])) : 0;
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $strTipoPost = isset($_POST["hidTipoPost"]) ? trim($_POST["hidTipoPost"]) : "";
                                                                              
    $strQuery = "UPDATE place_producto, 
                        place_producto_clasificacion_padre_hijo
                 SET    place_producto.destacado = 'N',
                        place_producto_clasificacion_padre_hijo.destacado = 'N'
                 WHERE  place_producto.id_place = {$intPlace}
                 AND    place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto 
                 ";
                 
    $objDBClass->db_consulta($strQuery);
    
    if( $strTipoPost == "C" ){
        
        $intCount = 0;
        $strKeyPlaceClasificacionPadreHijo = "";
        while( $rTMP = each($_POST) ){
            
            $arrExplode = explode("_", $rTMP["key"]);
            
            if( $arrExplode[0] == "chkClasificacionPadreHijo" ){
                
                if( $intCount > $intMaxClasificacionDestacada ){
                    
                    break;
                }
                
                $intPlaceClasificacionPadreHijo = isset($_POST["hidKeyClasificacionPadre_{$arrExplode[1]}"]) ? trim($_POST["hidKeyClasificacionPadre_{$arrExplode[1]}"]) : 0;
                
                //$strKeyPlaceClasificacionPadreHijo .= ( $strKeyPlaceClasificacionPadreHijo == "" ? "" : "," ).$intPlaceClasificacionPadreHijo;
                
                $strQuery = "UPDATE place_producto, 
                                    place_producto_clasificacion_padre_hijo
                             SET    place_producto.destacado = 'Y',
                                    place_producto_clasificacion_padre_hijo.destacado = 'Y'
                             WHERE  place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo IN ({$intPlaceClasificacionPadreHijo})
                             AND    place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto
                             AND    place_producto.id_place = {$intPlace} 
                             ";
                             
                $objDBClass->db_consulta($strQuery);
                
                $intCount++;
            }
            
        }
        
                
        
        
    }
    
    if( $strTipoPost == "P" ){
        
        $intCount = 0;
        while( $rTMP = each($_POST) ){
            
            $arrExplode = explode("_", $rTMP["key"]);
            
            if( $arrExplode[0] == "chkProducto" ){
                                                  
                if( $intCount > $intMaxProductoDestacado ){
                    
                    break;
                }                              
                
                
                $intPlaceProducto = isset($_POST["hidKeyProducto_{$arrExplode[1]}"]) ? intval($_POST["hidKeyProducto_{$arrExplode[1]}"]) : 0;
                
                if( $intPlaceProducto ){
                    
                    $strQuery = "UPDATE place_producto
                                 SET    destacado = 'Y'
                                 WHERE  id_place_producto = {$intPlaceProducto} ";
                    $objDBClass->db_consulta($strQuery);
                    
                }
                
                $intCount++;
                
                
            }
            
            
        }
    
    }
    
    fntCalculoPrecioPlaceProducto($intPlace, $intPlaceClasificacionPadre, true, $objDBClass);
    
    die();
}

if( isset($_GET["fntDrawSeccionForm"]) ){
    
    include "core/dbClass.php";
    
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $intCantidadImagenes = 2;
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    
    
    if( $intSeccion == 1 ){
        
        $objDBClass = new dbClass();  
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrGetGaleriaPlace = fntGetPlaceGaleria($intPlace, true, $objDBClass);
    
        ?>
        <link href="dist_interno/fine-uploader/fine-uploader-new.min.css" rel="stylesheet">
        <script src="dist_interno/fine-uploader/fine-uploader.min.js"></script>
        
        
        <header class="popupHeader" style="height: 10%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="direction: ltr; height: 75%; overflow: auto;" >
            
            <form name="frmGaleria" id="frmGaleria"  onsubmit="return false;" method="POST">
            <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                
            <div class="row" id="dsafadsfsadfsadf">
                
                <?php 
               
                $intCount = 1;
                while( $rTMP = each($arrGetGaleriaPlace) ){
                    ?>
                    <input type="hidden" value="<?php print $rTMP["key"]?>" id="hidIdGaleria_<?php print $intCount?>" name="hidIdGaleria_<?php print $intCount?>">
                    <input type="hidden" value="N" id="hidDelete_<?php print $intCount?>" name="hidDelete_<?php print $intCount?>">
                                
                    <div class="col-lg-3 col-xs-12" id="divContentImageSeccion1_<?php print $intCount;?>">
                    
                        <div class="card lis-brd-light text-center text-lg-left mt-2">
                            
                            <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top">
                                <input onchange="fntSetImgfile('<?php print $intCount;?>')" type="file" id="flImagen_<?php print $intCount;?>" name="flImagen_<?php print $intCount;?>" style="display: none;">
                                <img id="flIMGImagen_<?php print $intCount;?>" src="<?php print $rTMP["value"]["url_imagen"]?>" alt="" class="img-fluid rounded-top w-100">
                            </div>
                                     
                            <div  class="lis-absolute lis-right-20 lis-top-20 cursor_pointer">
                                <div  class="lis-post-meta  text-white rounded lis-f-14 p-0 m-0">
                                    <button class=" btn btn-xs btn-primary cursor_pointer text-center" onclick="$('#flImagen_<?php print $intCount;?>').click();">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button class=" btn btn-xs btn-primary cursor_pointer text-center" onclick="fntDeleteImageSeccion1('<?php print $intCount;?>');">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>  
                            </div>
                            
                        </div>
                        
                    </div>

                    <?php
                    $intCount++;
                }
                
                ?>
                
                <div class="col-lg-3 col-xs-12" id="divAddMoreSeccion1">
                    
                    <div class="card lis-brd-light text-center text-lg-left mt-2" style="border: 0px;">
                        <br>
                        <a href="javascript:void(0)" tabindex="-1" onclick="fntAddImageSeccion1();">
                            <div class="lis-grediant grediant-tb-light2d lis-relative modImage rounded-top text-center">
                                <button class="btn btn-sm btn-primary cursor_pointer m-5 text-center" >
                                    Add Image
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                            </div>

                        </a>    
                        
                    </div>
                    
                </div>
                
            </div>
            
            </form>      
        </div> 
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-lg-3 col-xs-12 offset-lg-9 text-right nowrap">
                    <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSaveGaleria();">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer" onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script> 
        
            var intCountImage = "<?php print $intCount;?>";       
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("80%");
                    
            }); 
            
            function fntSetImgfile(index){
                
                var filename = $('#flImagen_'+index).val();
                
                arr = filename.split(".");
                
                if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                    
                    swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
                    fntDeleteImageSeccion1(index);
                    return false;                    
                    
                }
                
                
                if ( ! window.FileReader ) {
                    return alert( 'FileReader API is not supported by your browser.' );
                }
                var $i = $( '#flImagen_'+index ), // Put file input ID here
                    input = $i[0]; // Getting the element from jQuery
                
                if ( input.files && input.files[0] ) {
                    
                    file = input.files[0]; // The file
                        
                    fr = new FileReader(); // FileReader instance
                    
                    fr.onload = function () {
                        
                        var image = new Image();

                        image.onload = function() {      

                            if( this.naturalWidth >= 900 || this.naturalHeight >= 600 ){

                                document.getElementById("flIMGImagen_"+index).src = fr.result;

                            }
                            else{

                                swal("Error!", "The minimum dimensions are 900 x 600", "error");
                                fntDeleteImageSeccion1(index);
                    
                            }

                        }

                        // Load image.
                        image.src = fr.result;
                        
                    }; 
                    //fr.readAsText( file );
                    fr.readAsDataURL( file );
                    
                } 
                else {
                    // Handle errors here
                    alert( "File not selected or browser incompatible." )
                }
                                
            }
            
            function fntAddImageSeccion1(){
                
                intCount = 0;
                $(":input[id^=hidIdGaleria_]").each(function (){
                    
                    intCount++;                    
                    
                });
                
                if( intCount == "<?php print $arrDatosPlace["num_galeria"]?>" ){
                    
                    swal("Error!", "Have a total of <?php print $arrDatosPlace["num_galeria"]?> photos configured for your gallery", "error");
                    
                    return false;
                                        
                }
                
                $("#divAddMoreSeccion1").before("<div class=\"col-lg-3 col-xs-12\" id=\"divContentImageSeccion1_"+intCountImage+"\">   "+
                                                "    <div class=\"card lis-brd-light text-center text-lg-left mt-2\">   "+
                                                "        <div class=\"lis-grediant grediant-tb-light2 lis-relative modImage rounded-top\">    "+
                                                "            <input type=\"hidden\" id=\"hidIdGaleria_"+intCountImage+"\" name=\"hidIdGaleria_"+intCountImage+"\" >    "+
                                                "            <input onchange=\"fntSetImgfile('"+intCountImage+"')\" type=\"file\" id=\"flImagen_"+intCountImage+"\" name=\"flImagen_"+intCountImage+"\" style=\"display: none;\">    "+
                                                "            <img id=\"flIMGImagen_"+intCountImage+"\" src=\"images/Noimagee.jpg?1\" alt=\"\" class=\"img-fluid rounded-top w-100\">  "+
                                                "        </div>   "+
                                                "        <div  class=\"lis-absolute lis-right-20 lis-top-20 cursor_pointer\">   "+
                                                "            <div  class=\"lis-post-meta  text-white rounded lis-f-14 p-0 m-0\">   "+
                                                "                <button class=\" btn btn-xs btn-primary cursor_pointer text-center\" onclick=\"$('#flImagen_"+intCountImage+"').click();\">   "+
                                                "                    <i class=\"fa fa-pencil\"></i>  "+
                                                "                </button>    "+
                                                "                <button class=\" btn btn-xs btn-primary cursor_pointer text-center\" onclick=\"fntDeleteImageSeccion1('"+intCountImage+"');\">  "+
                                                "                    <i class=\"fa fa-trash\"></i>     "+
                                                "                </button>    "+
                                                "            </div>   "+
                                                "        </div>   "+
                                                "    </div>     "+
                                                "</div>");
                
                $("#flImagen_"+intCountImage).click();
                intCountImage++;
                
                
            }
            
            function fntDeleteImageSeccion1(intIndex){
                
                $("#hidDelete_"+intIndex).val("Y");
                $("#divContentImageSeccion1_"+intIndex).remove();
                
            }
            
            function fntSaveGaleria(){
                
                var formData = new FormData(document.getElementById("frmGaleria"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setGaleriaPlace=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $('#lean_overlay').click();
                        fntDrawSeccion(1);
                        
                    }
                            
                });
                
            }
            
        </script>        
        <?php   
                
    }
    
    if( $intSeccion == 2 ){
        
        $objDBClass = new dbClass();  
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        
        ?>
        
        <header class="popupHeader" style="height: 10%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody " style="height: 75%; overflow: auto;">
            
            <form name="frmDatosPlace" id="frmDatosPlace"  onsubmit="return false;" method="POST">
            <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
            
            <div class="row">
                <div class="col-lg-10 col-xs-12">
                    <input value="<?php print $arrDatosPlace["titulo"]?>" id="txtTitulo" name="txtTitulo" placeholder="Title" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Title</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-xs-12">
                    <input value="<?php print $arrDatosPlace["subTitulo"]?>" id="txtSubTitulo" name="txtSubTitulo" placeholder="Sub Title" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Sub Title</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <input value="<?php print $arrDatosPlace["email"]?>" id="txtEmail" name="txtEmail" placeholder="Email" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Email</h6>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <input value="<?php print $arrDatosPlace["telefono"]?>" id="txtTelefono" name="txtTelefono" placeholder="Phone" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Phone</h6>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-10 col-xs-12">
                    <input value="<?php print $arrDatosPlace["direccion"]?>" id="txtDireccion" name="txtDireccion" placeholder="Address" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Address</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <input value="<?php print $arrDatosPlace["instagram"]?>" id="txtInstagram" name="txtInstagram" placeholder="Instagram" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Instagram</h6>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <input value="<?php print $arrDatosPlace["facebook"]?>" id="txtFacebook" name="txtFacebook" placeholder="Facebook" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Facebook</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xs-12">
                    <input value="<?php print $arrDatosPlace["twitter"]?>" id="txtTwitter" name="txtTwitter" placeholder="Twitter" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Twitter</h6>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <input value="<?php print $arrDatosPlace["youtube"]?>" id="txtYoutube" name="txtYoutube" placeholder="Youtube" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                </div>
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Youtube</h6>
                </div>
            </div>
            <div class="row">
                
                <div class="col-lg-8 col-xs-12 text-left">
                    &nbsp;
                </div>
                <div class="col-lg-2 col-xs-12 text-left lis-relative">
                    <input onchange="fntSetImgfile();" id="flLogo" name="flLogo" class="form-control-sm border-0" type="file" style="display: none;">
                    <img id="IMGflLogo" style="width: 100px; height: 100px;" src="<?php print $arrDatosPlace["url_logo"] != "" ? $arrDatosPlace["url_logo"] : "dist/images/profile-1.png"?>" class="img-fluid text-left d-md-flex ml-md-4 border border-white lis-border-width-4 rounded mb-4 mb-md-0" alt="">
                    <div onclick="$('#flLogo').click();" class="lis-absolute lis-top-0 " style="z-index: 999999999999; right: 0px; ">
                    
                        <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                        
                    </div>
                </div>
                
                <div class="col-lg-2 col-xs-12">
                    <h6 class="text-right">Logo</h6>
                </div>
            </div>
            </form>
        </div>
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-lg-3 col-xs-12 offset-lg-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSavePlace();">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div> 
        <script>
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("70%");
                    
            }); 
            
            function fntSavePlace(){
                
                var formData = new FormData(document.getElementById("frmDatosPlace"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setPlace=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $('#lean_overlay').click();
                        fntDrawSeccion(2);
                        
                    }
                            
                });
                
            }
            
            function fntSetImgfile(){
                
                var filename = $('#flLogo').val();
                
                arr = filename.split(".");
                
                if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                    
                    swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
                    fntDeleteImageSeccion1(index);
                    return false;                    
                    
                }
                
                
                if ( ! window.FileReader ) {
                    return alert( 'FileReader API is not supported by your browser.' );
                }
                var $i = $( '#flLogo' ), // Put file input ID here
                    input = $i[0]; // Getting the element from jQuery
                if ( input.files && input.files[0] ) {
                    file = input.files[0]; // The file
                    fr = new FileReader(); // FileReader instance
                    fr.onload = function () {
                        // Do stuff on onload, use fr.result for contents of file
                        //$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
                        //console.log(fr.result);
                        document.getElementById("IMGflLogo").src = fr.result;
                    };
                    //fr.readAsText( file );
                    fr.readAsDataURL( file );
                } else {
                    // Handle errors here
                    alert( "File not selected or browser incompatible." )
                }
                                
            }
            
            
        </script>       
        <?php
    }
    
    if( $intSeccion == 3 ){
        
        $objDBClass = new dbClass();  
    
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        
        ?>
        <header class="popupHeader" style="height: 12%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="height: 70%; overflow: auto; direction: ltr;">
            <form name="frmDescripcion" id="frmDescripcion"  onsubmit="return false;" method="POST">
            <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                
            <div class="row">
                <div class="col-lg-2 col-xs-12"  >
                    <h6 class="text-right">Description</h6>
                </div>
                <div class="col-lg-10 col-xs-12">
                    <textarea name="txtDescipcion" id="txtDescipcion" placeholder="Description" rows="10" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"><?php print $arrDatosPlace["descripcion"]?></textarea>
                </div>
                
            </div>
            </form>
        </div>
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-lg-3 col-xs-12 offset-lg-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSavePlace();">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("60%");
                    
            }); 
            
            function fntSavePlace(){
                
                var formData = new FormData(document.getElementById("frmDescripcion"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setPlaceDescripcion=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        fntDrawSeccion(3);
                        $('#lean_overlay').click();
                         
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
    
    if( $intSeccion == 4 ){
        
        $objDBClass = new dbClass();  
        
        $arrPlaceAmenidad = fntGetPlaceAmenidad($intPlace, true, $objDBClass);
        $arrAmenidades = fntGetAmenidades(true, $objDBClass);
        
        ?>
        
        <header class="popupHeader" style="height: 12%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="height: 73%; overflow: auto;">
            
            <style>
                .form-control {
                    /* height: 51px; */
                    border-radius: 0px;
                    border-color: #dadada;
                    color: #707070;
                    transition: .2s ease-out;
                }
                select.form-control:not([size]):not([multiple]) {
                    height: 30px;  
                    -moz-appearance: none;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0px;
                    background: url(../images/arrow.png) no-repeat 4%;
                    cursor: pointer;
                }
            </style>
            <form name="frmAmenidades" id="frmAmenidades"  onsubmit="return false;" method="POST">
            <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                        
            <div class="row p-4" style="direction: ltr;">
                
                <div class="col-12" style="overflow-x: scroll; ">
                <table class=" " id="tblAmenidades" style="width: <?php print $boolMovil ? "300px" : "100%"?> ; ">
                    <thead style="display: none;"> 
                        <tr>    
                            <th>Amenities</th>
                            <th>Amenities</th>
                            <th>Amenities</th>
                            <th>Amenities</th>
                        </tr>
                    </thead> 
                    <tbody>    
                    <?php
                    
                    $intCount = 1;
                    $intCountGrupo = 1;
                                              
                    while( $rTMP = each($arrAmenidades) ){
                        
                        $strSelected = isset($arrPlaceAmenidad[$rTMP["key"]]) ? "checked" : "";
                        
                        if( $intCountGrupo == 1 ){
                            ?>
                            <tr>
                            <?php    
                        }
                        ?>
                        
                            <td  nowrap>
                                <label class="custom-checkbox">
                                    <?php print $rTMP["value"]["tag_en"]?>
                                    <input <?php print $strSelected;?> name="chkAmenidad_<?php print $rTMP["key"]?>" type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                        
                        <?php
                        
                        if( $intCountGrupo == 4 ){
                            ?>
                            </tr>
                            <?php
                            $intCountGrupo = 0;
                        }
                        
                        $intCountGrupo++;
                        
                        
                    }
                    
                    ?>
                        
                    </tbody>                            
                </table>
                </div>                    
                        

            </div>
            </form>
        </div>
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-lg-3 col-xs-12 offset-lg-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSaveAmenidad();">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("80%");
                $('#tblAmenidades').DataTable();    
                    
            }); 
            
            function fntSaveAmenidad(){
                
                var formData = new FormData(document.getElementById("frmAmenidades"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setPlaceAmenidad=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $('#lean_overlay').click();
                        fntDrawSeccion(4);
                        
                    }
                            
                });
                
            }
            
            
        </script>        
        <?php
        
    }
    
    if( $intSeccion == 5 ){
        
        $objDBClass = new dbClass();  
        
        $arrPlaceHorario = fntGetPlaceHorario($intPlace, true, $objDBClass);
        
        $arrDiasSemana = getSemana();
        $arrHorario = getHoras24Horas();
        
        ?>
        
        <header class="popupHeader" style="height: 12%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="direction: ltr; height: 73%; overflow: auto;">
            
            <form name="frmHorario" id="frmHorario"  onsubmit="return false;" method="POST">
            <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
            
            <?php
            
            while( $rTMP = each($arrDiasSemana) ){
                
                $arrInfo = isset($arrPlaceHorario[$rTMP["key"]]) ? $arrPlaceHorario[$rTMP["key"]] : array();
                
                ?>
                
                <input type="hidden" value="<?php print isset($arrInfo["id_place_horario"]) ? $arrInfo["id_place_horario"] : 0?>" name="hidPlaceHorario_<?php print $rTMP["key"]?>" id="hidPlaceHorario_<?php print $rTMP["key"]?>">
                <input type="hidden" value="<?php print $rTMP["key"]?>" name="hidKeyDia_<?php print $rTMP["key"]?>" id="hidKeyDia_<?php print $rTMP["key"]?>">
                <div class="row">
                    <div class="col-lg-2 offset-lg-2 col-xs-12">
                        <h6 class="text-right"><?php print $rTMP["value"]["nombre"]?></h6>
                    </div>
                    <div class="col-lg-2  col-xs-12">
                        
                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                            <input type="text" readonly <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "disabled" : ""?> id="slcHorarioInicio_<?php print $rTMP["key"]?>" name="slcHorarioInicio_<?php print $rTMP["key"]?>" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR" value="<?php print isset($arrInfo["hora_inicio"]) ? $arrInfo["hora_inicio"] : "00:00"?>">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-time fa fa-clock-o"></i>
                            </span>
                        </div>
                        
                    </div>
                    <div class="col-lg-2  col-xs-12">
                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                            <input type="text" readonly  <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "disabled" : ""?> id="slcHorarioFin_<?php print $rTMP["key"]?>" name="slcHorarioFin_<?php print $rTMP["key"]?>" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR" value="<?php print isset($arrInfo["hora_fin"]) ? $arrInfo["hora_fin"] : "00:00"?>">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-time fa fa-clock-o"></i>
                            </span>
                        </div>    
                        
                    </div>
                    <div class="col-lg-2  col-xs-12">
                        <label class="custom-checkbox">
                            Close
                            <input <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "checked" : ""?> id="chkClose_<?php print $rTMP["key"]?>" name="chkClose_<?php print $rTMP["key"]?>" type="checkbox" onchange="fntDisableDia('<?php print $rTMP["key"]?>');">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    
                </div>
                <?php
            }
            
            ?>
            </form>

        </div>
        <div class="popupBody bg-light"  style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-lg-3 col-xs-12 offset-lg-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSavePlaceHorario();">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("75%");
                
                $('.clockpicker').clockpicker({
                    placement: 'bottom',
                    align: 'left',
                    donetext: 'Done'
                });
                    
            }); 
            
            function fntDisableDia(index){
                
                check = document.getElementById("chkClose_"+index).checked;
                document.getElementById("slcHorarioFin_"+index).disabled = check;                
                document.getElementById("slcHorarioInicio_"+index).disabled = check;                
                
            }
            
            function fntSavePlaceHorario(){
                
                var formData = new FormData(document.getElementById("frmHorario"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setPlaceHorario=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $('#lean_overlay').click();
                        fntDrawSeccion(5);
                        
                    }
                            
                });
                
            }
            
        </script>        
        <?php
        
    }
    
    if( $intSeccion == 6 ){
        
        $objDBClass = new dbClass();  
        
        $arrClasificacion = fntGetClasificacion(true, $objDBClass);
         
        ?>
        
        
        <header class="popupHeader" style="height: 12%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="direction: ltr; height: 73%; overflow: auto;">
            
            <div class="row">
                <div class="col-5 offset-3">
                
                    <div class="form-group">
                        <label for="slcClasificacionPrincipal"><small>Main classification</small></label>
                        <div class="input-group">
                            <select id="slcClasificacionPrincipal" name="slcClasificacionPrincipal[]" class="form-control-sm border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR" multiple>
                                <?php
                            
                                while( $rTMP = each($arrClasificacion) ){
                                    
                                    ?>
                                    <option value="<?php print $rTMP["value"]["id_clasificacion"]?>"><?php print $rTMP["value"]["tag_en"]?></option>
                                    <?php
                                    
                                }
                                
                                ?>
                            </select>
                            <button class="btn btn-raised btn-xs btn-primary ml-1" id="btnNextClasificacionPrincipal" onclick="fntDrawClas2();">Next</button>
                        </div>
                    </div>
                    
                </div>
                
            </div>
                                    
            <div id="divClasificacionSecundaria"></div>                        
            <div id="divClasificacionDetalle"></div>                        
            <div id="divClasificacionDetalleOtros"></div>                        
            

        </div>
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-3 offset-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("85%");
                 
                $("#slcClasificacionPrincipal").select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#slcClasificacionPrincipal").change(function (){
                    
                    $("#btnNextClasificacionPrincipal").show();
                    $("#divClasificacionSecundaria").html("");
                    $("#divClasificacionDetalle").html("");
                    $("#divClasificacionDetalleOtros").html("");
                
                });
                 
                   
            }); 
            
            function fntDrawClas2(){
                             
                strClass1 = "";
                $("#slcClasificacionPrincipal option:selected").each(function(){
                    
                    strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                    
                });          
                
                if( strClass1 == "" ){
                        
                    swal({
                        title: "Select An Option",
                        //text: "Select An Option",
                        type: "warning",
                        showConfirmButton: false,
                        timer: 1000
                    });                                                                                                     
                    
                    return false;   
                    
                }    
                
                var formData = new FormData();
                
                formData.append("keys", strClass1);
                    
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "place_admin.php?getDrawClas2=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        $("#divClasificacionSecundaria").html(result);
                        
                    }
                            
                });
                
                return false;                
            }
            
            
            
        </script>        
        <?php
        
    }
    
    if( $intSeccion == 7 ){
        
        ?>
        
        
        <header class="popupHeader" style="height: 12%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="direction: ltr; height: 73%; overflow: auto;">
            
            <style>
              #map {
                height: 100%;
              }
             
              .controls {
                margin-top: 10px;
                border: 1px solid transparent;
                border-radius: 2px 0 0 2px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                height: 32px;
                outline: none;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
              }

              #pac-input {
                background-color: #fff;
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
                margin-left: 12px;
                padding: 0 11px 0 13px;
                text-overflow: ellipsis;
                width: 300px;
              }

              #pac-input:focus {
                border-color: #4d90fe;
              }

              .pac-container {
                font-family: Roboto;
              }

              #type-selector {
                color: #fff;
                background-color: #4d90fe;
                padding: 5px 11px 0px 11px;
              }

              #type-selector label {
                font-family: Roboto;
                font-size: 13px;
                font-weight: 300;
              }
            </style>
        
            <input type="hidden" name="hidLngUbicacion" id="hidLngUbicacion">
            <input type="hidden" name="hidLatUbicacion" id="hidLatUbicacion">
            <input type="hidden" name="hidPlaceId" id="hidPlaceId">
            <input id="pac-input" class="controls" type="text"
                placeholder="Enter a location">
            <div id="type-selector" class="controls">
              <input type="radio" name="type" id="changetype-all" checked="checked">
              <label for="changetype-all">All</label>

              <input type="radio" name="type" id="changetype-establishment">
              <label for="changetype-establishment">Establishments</label>

              <input type="radio" name="type" id="changetype-address">
              <label for="changetype-address">Addresses</label>

              <input type="radio" name="type" id="changetype-geocode">
              <label for="changetype-geocode">Geocodes</label>
            </div>
            <div id="map-canvas" style="height: 400px;"></div>
                

        </div>
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-3 offset-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script>
            
            var center = new google.maps.LatLng(59.76522, 18.35002);
            
            var markerInicial;
            var map;
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("85%");
                
                initializeMap();
                 
            }); 
            
            function initializeMap() {

                var mapOptions = {
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: center
                };

                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                if (navigator.geolocation) {
                    
                    navigator.geolocation.getCurrentPosition(function(position) {
                        
                        
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        console.log(position);    
                        var markerOptions = { 
                            position: pos, 
                            draggable: true
                        }
                        
                        markerInicial = new google.maps.Marker(markerOptions);
                        markerInicial.setMap(map);
                        
                        map.setCenter(pos);
                        
                        $("#hidLatUbicacion").val(position.coords.latitude);
                        $("#hidLngUbicacion").val(position.coords.longitude);
                        
                        
                        
                        
                    }, function() {
                        
                        handleLocationError(true, infoWindow, map.getCenter());
                        
                    });
                    
                } 
                else {
                    
                    handleLocationError(false, infoWindow, map.getCenter());
                    
                }
                
                var input = /** @type {!HTMLInputElement} */(
                    document.getElementById('pac-input'));

                var types = document.getElementById('type-selector');
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();
                var marker = new google.maps.Marker({
                  map: map,
                  anchorPoint: new google.maps.Point(0, -29),
                  draggable: true
                });

                autocomplete.addListener('place_changed', function() {
                  infowindow.close();
                  marker.setVisible(false);
                  var place = autocomplete.getPlace();
                  if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                  }
                  else{
                      markerInicial.setMap(null);
                  }

                  
                  //console.log(place);
                  //console.log(place.place_id);
                  $("#hidPlaceId").val(place.place_id);
                  // If the place has a geometry, then present it on a map.
                  if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                    
                    $("#hidLatUbicacion").val(place.geometry.location.lat);
                    $("#hidLngUbicacion").val(place.geometry.location.lng);
                    
                  } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                    $("#hidLatUbicacion").val(place.geometry.location.lat);
                    $("#hidLngUbicacion").val(place.geometry.location.lng);
                    
                  }
                  
                  marker.setPosition(place.geometry.location);
                  marker.setVisible(true);
                  
                  var address = '';
                  if (place.address_components) {
                    address = [
                      (place.address_components[0] && place.address_components[0].short_name || ''),
                      (place.address_components[1] && place.address_components[1].short_name || ''),
                      (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                  }

                  infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                  infowindow.open(map, marker);
                });

                // Sets a listener on a radio button to change the filter type on Places
                // Autocomplete.
                function setupClickListener(id, types) {
                  var radioButton = document.getElementById(id);
                  radioButton.addEventListener('click', function() {
                    autocomplete.setTypes(types);
                  });
                }

                setupClickListener('changetype-all', []);
                setupClickListener('changetype-address', ['address']);
                setupClickListener('changetype-establishment', ['establishment']);
                setupClickListener('changetype-geocode', ['geocode']);
                   
            }
                   
            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                                      'Error: The Geolocation service failed.' :
                                      'Error: Your browser doesn\'t support geolocation.');
            }
            
            
        </script>        
        <?php
        
    }
    
    if( $intSeccion == 8 ){
        
        $objDBClass = new dbClass();  
    
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        
        ?>
        <header class="popupHeader" style="height: 12%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="height: 70%; overflow: auto; direction: ltr;">
            <form name="frmRelevante" id="frmRelevante"  onsubmit="return false;" method="POST">
            <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                
            <div class="row">
                <div class="col-lg-2 col-xs-12"  >
                    <h6 class="text-right">Relevant</h6>
                </div>
                <div class="col-lg-10 col-xs-12">
                    <textarea name="txtRelevante" id="txtRelevante" placeholder="Relevant" rows="10" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"><?php print $arrDatosPlace["relevante"]?></textarea>
                </div>
                
            </div>
            </form>
        </div>
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-lg-3 col-xs-12 offset-lg-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSavePlace();">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("60%");
                    
            }); 
            
            function fntSavePlace(){
                
                var formData = new FormData(document.getElementById("frmRelevante"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setPlaceRelevante=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $('#divContenidoRelevante').html($('#txtRelevante').val());
                        $('#divContenidoRelevante2').html($('#txtRelevante').val());
                        
                        $(".preloader").fadeOut();
                        $('#lean_overlay').click()
                        
                        
                        
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
    
    if( $intSeccion == 9 ){
        
        $objDBClass = new dbClass();  
    
        $arrDatosPlace = fntGetPlaceProductoDestacadoForm($intPlace, true, $objDBClass);
        
        $intPlaceClasificacionPadre = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
    
        ?>
        <header class="popupHeader" style="height: 12%;">

            <span class="header_title">Section <?php print $intSeccion?></span>

            <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

        </header>

        <div class="popupBody" style="height: 70%; overflow: auto; direction: ltr;">
            <form name="frmProductoRelevante" id="frmProductoRelevante"  onsubmit="return false;" method="POST">
            <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
            <input type="hidden" id="hidPlaceClasificacionPadre" name="hidPlaceClasificacionPadre" value="<?php print fntCoreEncrypt($intPlaceClasificacionPadre)?>">
           
            
            <?php
            
            if( isset($arrDatosPlace["clasificacion_padre_hijo"]) ){
                
                ?>
                <input type="hidden" name="hidTipoPost" value="C">
                
                <table class="table table-sm table-hover">
                    <thead class="thead-dark">
                        <tr>                                             
                            <th scope="col">Classification of existing products</th>
                            <th scope="col" style="width: 10%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        $intCount = 1;
                        while( $rTMP = each($arrDatosPlace["clasificacion_padre_hijo"]) ){
                            ?>
                            <tr>
                                <th scope="row"><?php print $rTMP["value"]["tag_en"]?></th>
                                <td>

                                    <input type="hidden" name="hidKeyClasificacionPadre_<?php print $intCount;?>" value="<?php print $rTMP["value"]["id_clasificacion_padre_hijo"]?>">
                                    <label class="custom-checkbox">Select
                                        <input type="checkbox"  <?php print $rTMP["value"]["destacado"] == "Y" ? "checked" : ""?> name="chkClasificacionPadreHijo_<?php print $intCount;?>" id="chkClasificacionPadreHijo_<?php print $intCount;?>">
                                        <span class="checkmark"></span>
                                    </label>

                                </td>
                            </tr>
                            <?php
                            $intCount++;
                        }
                        
                        ?>
                            
                    </tbody>
                </table>

                
                <?php
            }
            else{
                
                ?>
                <input type="hidden" name="hidTipoPost" value="P">
                <table class="table table-sm table-hover">
                    <thead class="thead-dark">
                        <tr>                                             
                            <th scope="col">Existing Products</th>
                            <th scope="col" style="width: 10%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        $intCount = 1;
                        while( $rTMP = each($arrDatosPlace["producto"]) ){
                            ?>
                            <tr>
                                <th scope="row"><?php print $rTMP["value"]["nombre"]?></th>
                                <td>

                                    <input type="hidden" name="hidKeyProducto_<?php print $intCount;?>" value="<?php print $rTMP["value"]["id_place_producto"]?>">      
                                    <label class="custom-checkbox">Select
                                        <input type="checkbox" <?php print $rTMP["value"]["destacado"] == "Y" ? "checked" : ""?>  name="chkProducto_<?php print $intCount;?>" id="chkProducto_<?php print $intCount;?>">
                                        <span class="checkmark"></span>
                                    </label>

                                </td>
                            </tr>
                            <?php
                            $intCount++;
                        }
                        
                        ?>
                            
                    </tbody>
                </table>
                <?php                
            }
            
            ?>
            
            
            </form>
        </div>
        <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
            
            <div class="row">
                <div class="col-lg-3 col-xs-12 offset-lg-9 text-right">
                    <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSavePlaceProductoRelevante();">Save</button>
                    <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
                </div>
            </div>
            
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("60%");
                    
            }); 
            
            function fntSavePlaceProductoRelevante(){
                
                intCount = 0;
                
                if( $("#hidTipoPost").val() == "P" ){
                    
                    $(":input[id^=chkProducto_]").each(function (){
                        
                        if( this.checked ){
                            intCount++;            
                        }                        
                        
                    });
                    
                    if( intCount > <?php print $intMaxProductoDestacado?> ){
                        
                        swal("Error!", "You can only select <?php print $intMaxProductoDestacado?> options", "error");
                    
                        return false;
                    }
                    
                }
                else{
                    
                    $(":input[id^=chkClasificacionPadreHijo_]").each(function (){
                        
                        if( this.checked ){
                            intCount++;            
                        }                        
                        
                    });    
                    
                    if( intCount > <?php print $intMaxClasificacionDestacada?> ){
                        
                        swal("Error!", "You can only select <?php print $intMaxClasificacionDestacada?> options", "error");
                    
                        return false;
                    }
                
                }
                    
                
                
                var formData = new FormData(document.getElementById("frmProductoRelevante"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setPlaceProductoRelevante=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $('#lean_overlay').click();
                        
                        $(".divCategoriaProducto").each(function (){
                            
                            arrExplode = this.id.split("_");
                            fntDrawSeccionProductoDetalle(arrExplode[1]);
                
                        });
                        
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
    
    die();
}

if( isset($_GET["getDrawClasificacionPadreHijoDetallePlace"]) ){
    
    
    $strKey = $_POST["keys"];    
    $intIndex = $_POST["index"];    
    $arrInfo = fntClasificacionPadreHijoDetalle(null, true, $strKey);
     
    ?>
    
    <div class="form-group">
        <label for="slcClasificacionPadre"><small>Other Classification</small></label>
        <div class="input-group">
            <select id="slcClasificacionPadreHijoDetalle_<?php print $intIndex;?>" name="slcClasificacionPadreHijoDetalle_<?php print $intIndex;?>[]" class="form-control-sm select2-multiple" multiple>
                <?php
            
                while( $rTMP = each($arrInfo) ){
                    
                    ?>
                    <optgroup label="<?php print $rTMP["value"]["tag_en"]?>">
                        <?php
                        
                        while( $arrTMP = each($rTMP["value"]["detalle"]) ){
                            ?>
                            <option value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo_detalle"]?>"><?php print $arrTMP["value"]["tag_enDetalle"]?></option>
                    
                            <?php
                        }
                        
                        ?>
                    </optgroup>
                    <?php
                    
                }
                
                ?>
            </select>
        </div>
    </div>
    <script>
        
        $( document ).ready(function() {
            
            
            $("#slcClasificacionPadreHijoDetalle_<?php print $intIndex;?>").select2({
              theme: 'bootstrap4',
              width: 'style',
              placeholder: $(this).attr('placeholder'),
              allowClear: Boolean($(this).data('allow-clear')),
            });
            
            
        });
        
    </script>
    
    <?php
    
    die();
}

if( isset($_GET["fntDrawSeccion"]) ) {
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
        
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $intDiaSemana = isset($_GET["diaSemana"]) ? intval($_GET["diaSemana"]) : 0;
    
    if( $intSeccion == 1 ){
        
        $arrPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrPlaceGaleria = fntGetPlaceGaleria($intPlace, true, $objDBClass);
        
        $boolBotonReenvioSolPlaceRechazada = false;
        $intSolPlace = 0;
        if( $arrPlace["estado"] == "S" ){
            
            $strQuery = "SELECT *
                         FROM   sol_place
                         WHERE  sol_place.id_place = {$intPlace} 
                         AND    estado IN('R')";    
            
            $qTMP = $objDBClass->db_consulta($strQuery);
            $arrSolPlace = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_free_result($qTMP);
            $intSolPlace = $arrSolPlace["id_sol_place"];
            
            if( isset($arrSolPlace["id_sol_place"]) && intval($arrSolPlace["id_sol_place"]) )
                $boolBotonReenvioSolPlaceRechazada = true;
        
        }
        
        ?>
        
        <h5 class="bg-danger  p-0 m-0 text-dark font-weight-bold" style="line-height: 1.25rem; ">
            
            <div class="row">
                <div class="col-4 text-right">
                    <a href="#modalFormContendio" class="login_form mr-5" onclick="fntCargarModalSeccion('1');" >
                        <i class="fa fa-pencil  text-center cursor_pointer p-2 bg-secondary text-white " style="border-radius: 50%; font-size: 14px;" ></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <?php
                    
                    if( $boolBotonReenvioSolPlaceRechazada ){
                        ?>
                        <button class="btn btn-raised btn-info" id="btnEnvioCambiosRechazo" onclick="fntEnviarCambiosRechazo();">Enviar Cambios de Rechazo</button>
                
                        <?php    
                    }
                    else{
                        ?>
                        
                        &nbsp;
                        <?php
                    }
                    
                    ?>
                    
                </div>
                <div class="col-4">
                    
                    <span class="text-white" style=" <?php print $boolMovil ? "font-size: 20px;" : "font-size: 30px;"?> ">
                        
                        <?php print $arrPlace["titulo"]?>
                    
                    </span>
                    
                </div>
            </div>
            
            
            
            
            
        </h5>
        <div class="bg-danger" style="  position: center; height: <?php print $boolMovil ? "100px" : "150px"?>; top: 0px;" >
            <div class="sliderInicio slider  "  style="direction: <?php print $boolMovil ? "ltr" : "ltr"?>; background: transparent; <?php print $boolMovil ? "padding-left: 20px;" : ""?>">
                <?php
                
                if( count($arrPlaceGaleria) > 0 ){
                    
                    $arrPlace["num_galeria"] = count($arrPlaceGaleria);
                    $intNumFoto = 1;
                    while( $rTMP = each($arrPlaceGaleria) ){
                        
                        ?>
                        <div  aria-hidden="<?php print $intNumFoto == 1 ? "true" : "false"?>" style=" margin: 0px; padding: 0px;  ">
                            <div class="gallery text-center lis-relative classPrueba imgGalery_<?php print $intNumFoto;?> <?php print $intNumFoto == 1 ? "slideImgActiva" : "slideImgNoActiva"?>" style="margin: 5px;   ">
                                <img id="imgGalery_<?php print $intNumFoto;?>" src="<?php print $rTMP["value"]["url_imagen"]?>" alt="" class="img-fluid slideImgNoActivaIMG" style="" >
                                <div class="gallery-fade fade w-100 h-100 rounded">
                                    <div class="d-table w-100 h-100">
                                        <div class="d-table-cell align-middle">
                                            <h4 class="mb-0"><a data-toggle="lightbox" data-gallery="example-gallery" href="dist/images/gallery<?php print $intNumFoto;?>.jpg" class="text-white">
                                                <?php print $intNumFoto;?>
                                                <i class="fa fa-search-plus"></i></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $intNumFoto++;                    
                    }
                    
                }
                else{
                    $arrPlace["num_galeria"] = 5;
                    $intNumFoto = 1;
                    for( $intNumFoto = 1; $intNumFoto <= $arrPlace["num_galeria"] ; $intNumFoto++ ){
                    
                        ?>
                        <div  aria-hidden="<?php print $intNumFoto == 1 ? "true" : "false"?>" style=" margin: 0px; padding: 0px;  ">
                            <div class="gallery text-center lis-relative classPrueba imgGalery_<?php print $intNumFoto;?> <?php print $intNumFoto == 1 ? "slideImgActiva" : "slideImgNoActiva"?>" style="margin: 5px;   ">
                                <img id="imgGalery_<?php print $intNumFoto;?>" src="images/Noimagee.jpg?1" alt="" class="img-fluid slideImgNoActivaIMG" style="" >
                                <div class="gallery-fade fade w-100 h-100 rounded">
                                    <div class="d-table w-100 h-100">
                                        <div class="d-table-cell align-middle">
                                            <h4 class="mb-0"><a data-toggle="lightbox" data-gallery="example-gallery" href="images/Noimagee.jpg?1" class="text-white">
                                                <?php print $intNumFoto;?>
                                                <i class="fa fa-search-plus"></i></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        
                    }
                    
                }
                    
                /*
                 */
                
                ?>
                  
            </div>                  
        </div>
        <script>
            
            $( document ).ready(function() {
        
                $('.sliderInicio').slick({ 
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 4,
                    arrows: false,
                    <?php print $boolMovil ? "ltr: true," : "ltr: true,"?>
                    <?php print $boolMovil ? "autoplay: false," : "autoplay: true,"?>
                     <?php print $boolMovil ? "speed: 100," : "speed: 1500,"?>
                    responsive: [
                        {
                            breakpoint: 1199,
                            settings: {
                                arrows: false,
                                centerMode: false,
                                centerPadding: '0px',
                                slidesToShow: 2
                            }
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                infinite: false,
                                arrows: false,
                                centerMode: false,
                                centerPadding: '0px',
                                speed: 100,
                                slidesToShow: 1.5,
                                slidesToScoll: 1.5,
                                
                            }
                        }
                    ]   
                });

                arrOrden = new Array();
                intReal = 1;
                intTotal = <?php print $arrPlace["num_galeria"];?>;
                for( i = (intTotal/2) ; i >= 0; i-- ){
                    index = i ;
                    
                    arrOrden[index] = new Array();
                    arrOrden[index]["index"] = index;
                    arrOrden[index]["real"] = intReal;
                    intReal++;
                }
                  
                for( i = intTotal  ; i > (intTotal/2) + 1; i-- ){
                    index = i - 1;
                    arrOrden[index] = new Array();
                    arrOrden[index]["index"] = index;
                    arrOrden[index]["real"] = intReal;
                    intReal++;
                }
                
                $('.sliderInicio').on('afterChange', function(event, slick, currentSlide, nextSlide){
                    var dataId = $('.slick-current').attr("data-slick-index");    
                  
                  
                    dataId++;
                    indexReal = dataId;                  
                  $('.classPrueba').removeClass('slideImgActiva');        
                  $('.classPrueba').addClass('slideImgNoActiva');        
                  $('.imgGalery_'+indexReal).removeClass('slideImgNoActiva');        
                  $('.imgGalery_'+indexReal).addClass('slideImgActiva');        
                  
                  
                });
                
                $('.sliderInicio').slick('slickGoTo',"0");
         
                
                $("#divSeccionDatos").css("padding-top", ( parseFloat($("#header-fix").height()) <?php print !$boolMovil ? "+ parseFloat($('.sliderInicio').height() / 2)" : ""?> )+"px");
                
                    
            }); 
            
            function fntEnviarCambiosRechazo(){
                
                
                var formData = new FormData();
                
                formData.append("txtKeyPlace", "<?php print fntCoreEncrypt($intPlace)?>");
                formData.append("txtKeyPlaceSol", "<?php print fntCoreEncrypt($intSolPlace)?>");
                                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "place_admin.php?setSolPlaceRechazada=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        $('#btnEnvioCambiosRechazo').hide();
                        swal({   
                            title: "Cambios Enviado",   
                            text: "Los cambios enviados seran revisados en breve",
                            type: "success",   
                            showConfirmButton: true 
                        },function(isConfirm){   
                            
                        
                        });                        
                    }
                            
                });
                
            }       
            
        </script>
        
        <?php
        
    }
    
    if( $intSeccion == 2 ){
        
        $arrPlace = fntGetPlace($intPlace, true, $objDBClass);
        ?>
        
        <div class="row justify-content-center wow fadeInDown">
            <div class="col-12 col-md-8 mb-0 mb-lg-0 ">
                
                
                
                <a href="#" class="text-dark">
                   <div class="media d-block d-md-flex text-md-right text-center   lis-relative mt-lg-2">
                       <a href="#modalFormContendio" class="login_form d-none d-sm-block" onclick="fntCargarModalSeccion('2');" >
                            <i class="fa fa-pencil lis-id-info   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                        </a> 
                       <img style="display: none; width: 100px; height: 100px;" src="<?php print $arrPlace["url_logo"]?>" class="img-fluid d-md-flex ml-md-4 border border-white lis-border-width-4 rounded mb-4 mb-md-0" alt="" />
                        <div class="media-body align-self-center">
                            
                            <h2 class="text-dark font-weight-bold lis-line-height-1 d-none d-sm-block" >
                            
                            
                                <?php print $arrPlace["titulo"]?>
                            
                            </h2>
                            <br class="d-block d-sm-none">
                            <p class="mb-0">
                                <br class="d-block d-sm-none">
                                <?php print !empty($arrPlace["subTitulo"]) ? $arrPlace["subTitulo"] : "Add your Subtitle"?>
                            </p>
                            <ul class="list-inline my-0 text-center text-md-right  m-o p-0 ">
                                <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["facebook"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["twitter"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["instagram"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-instagram"></i></a></li>
                                <li class="list-inline-item mr-0"><a href="<?php print $arrPlace["youtube"]?>" target="_blank" class="lis-light lis-social  lis-brd-light text-center d-block"><i class="fa fa-youtube"></i></a></li>
                            
                            </ul>
                        </div>
                        
                    </div>     
                </a>
            </div>
            <div class="col-12 col-md-4 align-self-center ">
                <ul class="list-unstyled mb-0 lis-line-height-2 text-md-left text-center m-o p-0">
                    <li>
                        <a href="tel:<?php print $arrPlace["telefono"]?>" class="text-dark">
                            <i class="fa fa-phone pl-2 text-dark"></i><?php print !empty($arrPlace["telefono"]) ? $arrPlace["telefono"] : "Add your Phone"?></li>
                        </a>
                    <li>
                        <a href="mailto:<?php print $arrPlace["email"]?>" class="text-dark">
                            <i class="fa fa-envelope pl-2"></i><?php print !empty($arrPlace["email"]) ? $arrPlace["email"] : "Add your Email"?>
                        </A>
                    </li>
                    <li>
                        
                        <a href="https://www.google.com/maps?q=<?php print $arrPlace["direccion"]?>" target="_blank" class="text-dark">
                            <i class="fa fa-map-o pl-2"></i> <?php print !empty($arrPlace["direccion"]) ? $arrPlace["direccion"] : "Add your Addresss"?> 
                        </a>
                        <a href="#modalFormContendio" class=" d-block d-sm-none login_form" onclick="fntCargarModalSeccion('2');" >
                            <i class="fa fa-pencil lis-id-info   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                        </a>
                        
                    </li> 
                </ul>
                
            </div>
        </div>
        <script>
            
            $( document ).ready(function() {
                
                $("#divSeccionDatos").css("padding-top", ( parseFloat($("#header-fix").height()) <?php print !$boolMovil ? "+ parseFloat($('.sliderInicio').height() / 2)" : ""?> )+"px");
                
                    
            });        
            
        </script>
        <?php
        
    }
    
    if( $intSeccion == 3 ){
        
        $arrPlace = fntGetPlace($intPlace, true, $objDBClass);
        ?>
        
        <div class="card-body p-4">
            <p><?php print $arrPlace["descripcion"]?></p>
        </div>
        <script>
            
            $( document ).ready(function() {
        
                    
            });        
            
        </script>
        <?php
        
    }
    
    if( $intSeccion == 4 ){
        
        $arrPlaceAmenidad = fntGetPlaceAmenidad($intPlace, true, $objDBClass);
        
        ?>
        
        <div class="card-body p-4">    
            <div class="row">
                            
                <?php
                
                $intCount = 1;
                while( $rTMP = each($arrPlaceAmenidad) ){
                    
                    if( $intCount == 1 ){
                        ?>
                        <div class="col-lg-4">
                            <ul class="list-unstyled lis-line-height-2 mb-0">
                
                        <?php
                    }
                    
                    ?>
                    <li><i class="fa fa-check-square pl-2 lis-primary"></i> <?php print $rTMP["value"]["tag_en"]?></li>
                        
                    <?php
                    
                    if( $intCount == 4 ){
                        ?>
                                </ul>
                        </div>
                        <?php
                        $intCount = 0;
                    }
                    
                    
                    $intCount++;                        
                }
                
                if( $intCount < 4 ){
                    ?>
                    </ul>
                        </div>
                    <?php
                }
                
                ?>         
                    
                
            </div>
        <script>
            
            $( document ).ready(function() {
        
                    
            });        
            
        </script>
        <?php
        
    }
    
    if( $intSeccion == 5 ){
        
        $arrPlaceHorario = fntGetPlaceHorario($intPlace, true, $objDBClass);
        
        $arrDiasSemana = getSemana();
        
        ?>      
        
        <div class="card-body p-4" style="direction: ltr;">
            <?php
                
            if( isset($arrPlaceHorario[$intDiaSemana]) ){
                
                ?>
                <button class="text-btn bg-transparent border-0  w-100 text-left collapsed px-0 lis-light" data-toggle="collapse" data-target="#demo">
                    
                    
                    
                    <?php
                    
                    if( $arrPlaceHorario[$intDiaSemana]["cerrado"] == "Y" ){
                        ?>
                        <span class='text-danger font-weight-bold'>Closed Today</span>
                        
                        <?php
                    }
                    else{
                        ?>
                        <span class='lis-light-green font-weight-bold'>Open Today</span> : <?php print $arrPlaceHorario[$intDiaSemana]["hora_inicio_text"]?> - <?php print $arrPlaceHorario[$intDiaSemana]["hora_fin_text"]?>
                    
                        <?php
                    }
                    
                    
                    ?>
                    
                </button>
                
                <?php    
            }   
            
            ?>
            <div id="demo" class="collapse">
                <dl class="row mb-0 mt-4 lis-line-height-2">
                    <?php
                    
                    while( $rTMP = each($arrPlaceHorario) ){
                        
                        ?>
                        <dt class="col-sm-6 font-weight-normal"><?php print $arrDiasSemana[$rTMP["key"]]["nombre"]?></dt>
                        
                        <?php
                        if( $rTMP["value"]["cerrado"] == "Y" ){
                            ?>
                            <dd class="col-sm-6">Closed</dd>
                    
                            <?php
                        }
                        else{
                            ?>
                            <dd class="col-sm-6"><?php print $rTMP["value"]["hora_inicio_text"];?> - <?php print $rTMP["value"]["hora_fin_text"];?></dd>
                    
                            <?php
                        }
                    }
                    
                    ?>
                    
                </dl>
            </div>
        </div>
        <script>
            
            $( document ).ready(function() {
        
                    
            });        
            
        </script>
        <?php    
        
        
    }
    
    if( $intSeccion == 8 ){
        
        $arrPlace = fntGetPlace($intPlace, true, $objDBClass);
        print $arrPlace["relevante"];
        
    }
       
     
    die();
}

if( isset($_GET["fntCargarModalSeccionProducto"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
        
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intPlaceClasificacionPadre = isset($_GET["clasificacionPadre"]) ? intval(fntCoreDecrypt($_GET["clasificacionPadre"])) : 0;
    
    $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
    
    $arrPlaceClasificacionPadre = fntGetClasificacionPadre($intPlaceClasificacionPadre, true, $objDBClass);
    $arrPlaceProducto = fntGetPlaceProducto($intPlace, $intPlaceClasificacionPadre, true, $objDBClass);
    $arrClasificacionPadreHijo = fntGetPlaceClasificacionPadreHijo($intPlace, $intPlaceClasificacionPadre, true, $objDBClass);
    
    $arrClasificacionPadreHijoDetalle = array();
    if( count($arrPlaceProducto) > 0 ){
        
        $strKey = "";
        while( $rTMP = each($arrPlaceProducto) ){
            
            if( isset($rTMP["value"]["clasificacion_padre_hijo"]) ){
                
                while( $arrTMP = each($rTMP["value"]["clasificacion_padre_hijo"]) ){
                    
                    $strKey .= ( $strKey == "" ? "" : "," ).$arrTMP["key"];
                }
                        
            }
            
        }
        
        if( $strKey != "" )
            $arrClasificacionPadreHijoDetalle = fntClasificacionPadreHijoDetalle(true, $objDBClass, $strKey);
        
    }
        
    ?>
    <header class="popupHeader" style="height: 12%;">

        <span class="header_title"><?php print $arrPlaceClasificacionPadre["tag_en"]?> Products </span>

        <span class="modal_close" onclick="$('#lean_overlay').click()"><i class="fa fa-times"></i></span>

    </header>

    <div class="popupBody" style="height: 70%; overflow: auto; direction: ltr;">
        
        <div class="row">
            <div class="col-12 text-right" >
                
                <a id="hrefModalFavoritoProdcuto" href="#modalFormContendio" class="login_form" onclick="fntCargarModalSeccion('9', '<?php print fntCoreEncrypt($intPlaceClasificacionPadre)?>');" style="display: none;">
                    <i class="fa fa-pencil  text-center cursor_pointer p-2 bg-secondary text-white " style="border-radius: 50%; font-size: 14px;" ></i>
                </a>
                <form name="frmProductoClasicicacionPadre" id="frmProductoClasicicacionPadre"  onsubmit="return false;" method="POST">
                <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                <input type="hidden" id="hidPlaceClasificacionPadre" name="hidPlaceClasificacionPadre" value="<?php print fntCoreEncrypt($intPlaceClasificacionPadre)?>">
                <table class="table table-sm table-responsive" id="tblProducto" style="width: 100%;">
                    
                    <tbody>
                        <?php
                        
                        $intCount = 1;
                        reset($arrPlaceProducto);
                        if( count($arrPlaceProducto)  > 0 ){
                            
                            while( $rTMP = each($arrPlaceProducto) ){
                                
                                ?>
                                <input type="hidden" id="hidIdPlaceProducto_<?php print $intCount;?>" name="hidIdPlaceProducto_<?php print $intCount;?>" value="<?php print $rTMP["value"]["id_place_producto"]?>">
                                <input type="hidden" id="hidProductoDelete_<?php print $intCount;?>" name="hidProductoDelete_<?php print $intCount;?>" value="N">
                                <input type="hidden" id="hidProductoUpdate_<?php print $intCount;?>" name="hidProductoUpdate_<?php print $intCount;?>" value="N">
                                
                                <tr id="tdGeneralProducto_<?php print $intCount;?>">
                                    <td id="td1Producto_<?php print $intCount;?>" style="border-bottom: 1px solid #D0D0D0; width: 90%;">
                                        
                                        <table  style="width:  <?php print $boolMovil ? "400px" : "100%"?>;">
                                            <tr>
                                                <td class="text-right " style="width: 20%;">
                                                                                           
                                                    <div class="lis-relative" style="width: 100%; height: 100%; vertical-align: top;">
                                                        <input onchange="fntSetLogoPreview('<?php print $intCount;?>');" id="flLogoProducto_<?php print $intCount;?>" name="flLogoProducto_<?php print $intCount;?>" type="file" style="display: none;">
                                                        <img id="flLogoIMGProducto_<?php print $intCount;?>" src="<?php print $rTMP["value"]["logo_url"] == "" ? "dist/images/profile-1.png" : $rTMP["value"]["logo_url"]?>" style="width: 100px; height: 100px;" class="img-fluid cursor_pointer d-md-fle ml-4 border border-white lis-border-width-4 rounded mb-4 mb-md-0" alt="" />
                                                        
                                                        <div id="divPencilEditLogo_<?php print $intCount;?>" onclick="$('#flLogoProducto_<?php print $intCount;?>').click();" class="lis-absolute lis-top-0" style="z-index: 99999999999; right: 0px; display: none;">
                    
                                                            <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                                                            
                                                        </div>
                                                    </div>
                                                </td>            
                                                <td class="text-left" style="width: 80%; vertical-align: middle;">
                                                    <h6 class="text-dark font-weight-bold lis-line-height-1 m-0 p-0">
                                                        <span id="spntxtNombre_<?php print $intCount;?>"><?php print $rTMP["value"]["nombre"]?></span>
                                                        <input id="txtNombre_<?php print $intCount;?>" name="txtNombre_<?php print $intCount;?>" value="<?php print $rTMP["value"]["nombre"]?>" placeholder="Name Product" style="display: none;" type="text" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"  >
                                                    </h6>
                                                    <h5 class="lis-primary m-0 p-0" style="font-size: 16px;">
                                                        
                                                        <span id="spntxtPrecio_<?php print $intCount;?>">Q <?php print $rTMP["value"]["precio"]?></span>
                                                        <input id="txtPrecio_<?php print $intCount;?>" name="txtPrecio_<?php print $intCount;?>" value="<?php print $rTMP["value"]["precio"]?>" placeholder="Price Product" style="display: none;" type="text" class="form-control-sm mt-2 text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR" onchange="fntMantenerDecimales(this)"  > 
                                                    </h5>
                                                    <h5 class=" m-0 p-0" style="font-size: 16px;">   
                                                        <span  id="spntxtDescripcion_<?php print $intCount;?>"> <?php print $rTMP["value"]["descripcion"]?></span>
                                                        <textarea id="txtDescripcion_<?php print $intCount;?>" name="txtDescripcion_<?php print $intCount;?>" style="display: none;" placeholder="Description" rows="2" class="form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR"><?php print $rTMP["value"]["descripcion"]?></textarea>
                                                    </h5>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="slcClasificacionPadre_<?php print $intCount;?>"><small>Detail Classification</small></label>
                                                                <div class="input-group">
                                                                    <select id="slcClasificacionPadreHijo_<?php print $intCount;?>" disabled name="slcClasificacionPadreHijo_<?php print $intCount;?>[]" class="form-control-sm select2-multiple" multiple>
                                                                        <?php
                                                                    
                                                                        reset($arrClasificacionPadreHijo);
                                                                        while( $arrTMP = each($arrClasificacionPadreHijo) ){
                                                                            
                                                                            $strSelected = isset($rTMP["value"]["clasificacion_padre_hijo"][$arrTMP["key"]]) ? "selected" : "";
                                                                            ?>
                                                                            <option <?php print $strSelected;?> value="<?php print $arrTMP["value"]["id_clasificacion_padre_hijo"]?>"><?php print $arrTMP["value"]["tag_en"]?></option>
                                                                            
                                                                            <?php
                                                                            
                                                                        }
                                                                        
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>    
                                                        <div class="col-6" id="divContenidoClasificacionPadreHijoDetalle_<?php print $intCount;?>">
                                                            <div class="form-group">
                                                                <label for="slcClasificacionPadre"><small>Other Classification</small></label>
                                                                <div class="input-group">                                                             
                                                                    <select id="slcClasificacionPadreHijoDetalle_<?php print $intCount;?>" disabled name="slcClasificacionPadreHijoDetalle_<?php print $intCount;?>[]" class="form-control-sm select2-multiple" multiple>
                                                                        <?php
                                                                    
                                                                        reset($arrClasificacionPadreHijoDetalle);
                                                                        while( $arrTMP = each($arrClasificacionPadreHijoDetalle) ){
                                                                            
                                                                            ?>
                                                                            <optgroup label="<?php print $arrTMP["value"]["tag_en"]?>">
                                                                                <?php
                                                                                
                                                                                while( $arrTMP1 = each($arrTMP["value"]["detalle"]) ){
                                                                                    $strSelected = isset($rTMP["value"]["clasificacion_padre_hijo_detalle"][$arrTMP1["value"]["id_clasificacion_padre_hijo_detalle"]]) ? "selected" : "";
                                                                            
                                                                                    ?>
                                                                                    <option <?php print $strSelected;?> value="<?php print $arrTMP1["value"]["id_clasificacion_padre_hijo_detalle"]?>"><?php print $arrTMP1["value"]["tag_enDetalle"]?></option>
                                                                            
                                                                                    <?php
                                                                                }
                                                                                
                                                                                ?>
                                                                            </optgroup>
                                                                            <?php
                                                                            
                                                                        }
                                                                        
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>    
                                                        </div>    
                                                    </div>
                                                </td>
                                                
                                            </tr> 
                                        </table>
                                        
                                    </td>                                                       
                                    <td id="td2Producto_<?php print $intCount;?>" class="text-right " nowrap style="vertical-align: bottom; border-bottom: 1px solid #D0D0D0;">
                                        
                                        <button onclick="fntEditProducto('<?php print $intCount;?>');" id="btnEditPro_<?php print $intCount;?>" class="btn btn-primary btn-xs cursor_pointer">Edit</button>
                                        <button onclick="fntDeleteProducto('<?php print $intCount;?>');" id="btnDeletePro_<?php print $intCount;?>" class="btn btn-secondary btn-xs cursor_pointer">Delete</button>
                                        <button onclick="fntCancelDeleteProducto('<?php print $intCount;?>');" id="btnCancelDeletePro_<?php print $intCount;?>" style="display: none;" class="btn btn-secondary btn-xs cursor_pointer">Cancel</button>
                                    </td>
                                </tr>
                                <?php
                                
                                $intCount++;
                            }
                            
                        }
                        
                        ?>
                        
                    </tbody>
                </table>
                <button onclick="fntAddProducto();" class="btn btn-block btn-primary btn-xs cursor_pointer"><i class="fa fa-plus"></i> Add Product</button>
                </form>
            </div>
        </div>

    </div>
    <div class="popupBody bg-light" style="direction: ltr; height: 10%;" >
        
        <div class="row">
            <div class="col-lg-3 col-xs-12 offset-lg-9 text-right">
                <button class="btn btn-xs btn-primary cursor_pointer" onclick="fntSaveProductoClasificacionPadre();">Save</button>
                <button class="btn btn-xs btn-secondary cursor_pointer " onclick="$('#lean_overlay').click()">Cancel</button>
            </div>
        </div>
        
    </div>
    <script>
        
        var intCountProducto = "<?php print $intCount;?>";
        
        $(document).ready(function() { 
        
            $("#modalFormContendio").height("70%");
            
            $(":input[id^=hidIdPlaceProducto_]").each(function (){
                
                arrExplode = this.id.split("_");
                intKey = arrExplode[1];
                
                $("#slcClasificacionPadreHijo_"+intKey).select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                $("#slcClasificacionPadreHijo_"+intKey).change(function (){
                    
                    //var arrSplit = $(this).attr("id").split("_");
                            
                    //fntDrawSelectClasificacionPadreHijoDetalle(arrSplit[1]);
                        
                });
                
                $("#slcClasificacionPadreHijoDetalle_"+intKey).select2({
                  theme: 'bootstrap4',
                  width: 'style',
                  placeholder: $(this).attr('placeholder'),
                  allowClear: Boolean($(this).data('allow-clear')),
                });
                
                
            });
            
                
        });
        
        function fntDrawSelectClasificacionPadreHijoDetalle(index){
            
            strClass1 = "";
            $("#slcClasificacionPadreHijo_"+index+" option:selected").each(function(){
                
                strClass1 += ( strClass1 == "" ? "" : "," ) + $(this).attr('value');
                
            });          
            
            if( strClass1 == "" ){
                
                $.snackbar({
                    content: "Select An Option", 
                    timeout: 1000
                }); 
                
                return false;   
                
            } 
            
            var formData = new FormData();
            
            formData.append("keys", strClass1);
            formData.append("index", index);
                
            $(".preloader").fadeIn();
            
            $.ajax({
                url: "place_admin.php?getDrawClasificacionPadreHijoDetallePlace=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                    $("#divContenidoClasificacionPadreHijoDetalle_"+index).html(result);
                    
                }
                        
            });
            
            return false;
            
        }
        
        function fntEditProducto(intIndex){
            
            $("#btnEditPro_"+intIndex).hide();
            $("#divPencilEditLogo_"+intIndex).show();
            $("#spntxtNombre_"+intIndex).hide();
            $("#txtNombre_"+intIndex).show();
            $("#spntxtPrecio_"+intIndex).hide();
            $("#txtPrecio_"+intIndex).show();
            $("#spntxtDescripcion_"+intIndex).hide();
            $("#txtDescripcion_"+intIndex).show();
            $("#hidProductoUpdate_"+intIndex).val("Y");
            
            if( document.getElementById("slcClasificacionPadreHijo_"+intIndex) )
                document.getElementById("slcClasificacionPadreHijo_"+intIndex).disabled = false;
            
            if( document.getElementById("slcClasificacionPadreHijoDetalle_"+intIndex) )
                document.getElementById("slcClasificacionPadreHijoDetalle_"+intIndex).disabled = false;
            
                
        }
        
        function fntDeleteProducto(intIndex){
            
            swal({
              title: "Are you sure",
              text: "",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes",
              closeOnConfirm: true
            },
            function(){
                $("#hidProductoDelete_"+intIndex).val("Y");
                $("#btnEditPro_"+intIndex).hide();    
                $("#btnDeletePro_"+intIndex).hide();    
                $("#btnCancelDeletePro_"+intIndex).show();    
                $("#td1Producto_"+intIndex).addClass("bg-delete");    
                $("#td2Producto_"+intIndex).addClass("bg-delete"); 
                fntRemoveProducto(intIndex);
            });
            
            
               
        }  
        
        function fntCancelDeleteProducto(intIndex){
            
            $("#hidProductoDelete_"+intIndex).val("N");
            
            $("#btnEditPro_"+intIndex).show();    
            $("#btnDeletePro_"+intIndex).show();    
            $("#btnCancelDeletePro_"+intIndex).hide();    
            $("#td1Producto_"+intIndex).removeClass("bg-delete");    
            $("#td2Producto_"+intIndex).removeClass("bg-delete");    
            
        }  
        
        function fntSetLogoPreview(index){
            
            var filename = $('#flLogoProducto_'+index).val();
            arr = filename.split(".");
 
            if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                
                swal("Error!", "The only accepted formats are: .png .jpg .jpeg", "error");
                return false;                    
                
            }
            
            
            if ( ! window.FileReader ) {
                return alert( 'FileReader API is not supported by your browser.' );
            }
            var $i = $( '#flLogoProducto_'+index ), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if ( input.files && input.files[0] ) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    //$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
                    //console.log(fr.result);
                    document.getElementById("flLogoIMGProducto_"+index).src = fr.result;
                };
                //fr.readAsText( file );
                fr.readAsDataURL( file );
            } else {
                // Handle errors here
                alert( "File not selected or browser incompatible." )
            }
                
        }
        
        function fntAddProducto(){
            
            intCount = 0;
            $(":input[id^=hidIdPlaceProducto_]").each(function (){
                
                intCount++;                    
                
            });
            
            if( intCount == "<?php print $arrDatosPlace["num_producto"]?>" ){
                
                swal("Error!", "Have a total of <?php print $arrDatosPlace["num_producto"]?> product configured", "error");
                
                return false;
                                    
            }
            
            $("#tblProducto > tbody ").append("<tr id=\"tdGeneralProducto_"+intCountProducto+"\">  "+
                                              "    <td id=\"td1Producto_"+intCountProducto+"\" style=\"border-bottom: 1px solid #D0D0D0; width: 90%;\">  "+
                                              "        <input type=\"hidden\" id=\"hidIdPlaceProducto_"+intCountProducto+"\" name=\"hidIdPlaceProducto_"+intCountProducto+"\" value=\"0\">  "+
                                              "        <input type=\"hidden\" id=\"hidProductoInsert_"+intCountProducto+"\" name=\"hidProductoInsert_"+intCountProducto+"\" value=\"Y\">  "+
                                              "        <table style=\"width: 100%;\">  "+
                                              "            <tr>  "+
                                              "                <td class=\"text-right \" style=\"width: 20%;\">   "+
                                              "                    <div class=\"lis-relative\" style=\"width: 100%; height: 100%; vertical-align: top;\">   "+
                                              "                        <input onchange=\"fntSetLogoPreview('"+intCountProducto+"');\" id=\"flLogoProducto_"+intCountProducto+"\" name=\"flLogoProducto_"+intCountProducto+"\" type=\"file\" style=\"display: none;\">   "+
                                              "                        <img id=\"flLogoIMGProducto_"+intCountProducto+"\" src=\"dist/images/profile-1.png\" style=\"width: 100px; height: 100px;\" class=\"img-fluid cursor_pointer d-md-fle ml-4 border border-white lis-border-width-4 rounded mb-4 mb-md-0\"  />    "+
                                              "                        <div id=\"divPencilEditLogo_"+intCountProducto+"\" onclick=\"$('#flLogoProducto_"+intCountProducto+"').click();\" class=\"lis-absolute lis-top-0\" style=\"z-index: 99999999999; right: 0px; display: none;\">    "+
                                              "                            <i class=\"fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer\" ></i>  "+
                                              "                        </div>     "+
                                              "                    </div>    "+
                                              "                </td>       "+     
                                              "                <td class=\"text-left\" style=\"width: 80%; vertical-align: middle;\">   "+
                                              "                    <h6 class=\"text-dark font-weight-bold lis-line-height-1 m-0 p-0\">      "+
                                              "                        <span id=\"spntxtNombre_"+intCountProducto+"\"></span>   "+
                                              "                        <input id=\"txtNombre_"+intCountProducto+"\" name=\"txtNombre_"+intCountProducto+"\" placeholder=\"Name Product\" style=\"display: none;\" type=\"text\" class=\"form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR\"  >   "+
                                              "                    </h6>        "+
                                              "                    <h5 class=\"lis-primary m-0 p-0\" style=\"font-size: 16px;\">   "+
                                              "                        <span id=\"spntxtPrecio_"+intCountProducto+"\">Q 0.00</span>    "+
                                              "                        <input id=\"txtPrecio_"+intCountProducto+"\" name=\"txtPrecio_"+intCountProducto+"\" placeholder=\"Price Product\" style=\"display: none;\" type=\"text\" class=\"form-control-sm mt-2 text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR\" onchange=\"fntMantenerDecimales(this)\"  >   "+
                                              "                    </h5>      "+
                                              "                     <h5 class=\"lis-primary m-0 p-0\" style=\"font-size: 16px;\">     "+
                                              "                          <textarea id=\"txtDescripcion_"+intCountProducto+"\" name=\"txtDescripcion_"+intCountProducto+"\"  placeholder=\"Description\" rows=\"2\" class=\"form-control-sm  text-left border-top-0 border-left-0 border-right-0 rounded-0 InputdirectionLTR\"></textarea>   "+
                                              "                      </h5>      "+
                                              "                    <div class=\"row\">  "+
                                              "                          <div class=\"col-6\">    "+
                                              "                              <div class=\"form-group\">   "+
                                              "                                  <label for=\"slcClasificacionPadre_"+intCountProducto+"\"><small>Detail Classification</small></label>  "+
                                              "                                  <div class=\"input-group\">     "+
                                              "                                      <select id=\"slcClasificacionPadreHijo_"+intCountProducto+"\" name=\"slcClasificacionPadreHijo_"+intCountProducto+"[]\" class=\"form-control-sm select2-multiple\" multiple>  "+
                                                                                        <?php
                                                                                        reset($arrClasificacionPadreHijo);
                                                                                        while( $rTMP = each($arrClasificacionPadreHijo) ){
                                                                                            ?>
                                              "                                              <option value=\"<?php print $rTMP["value"]["id_clasificacion_padre_hijo"]?>\"><?php print $rTMP["value"]["tag_en"]?></option>   "+
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                              "                                      </select>   "+
                                              "                                  </div>   "+
                                              "                              </div>     "+
                                              "                          </div>         "+
                                              "                          <div class=\"col-6\" id=\"divContenidoClasificacionPadreHijoDetalle_"+intCountProducto+"\">    "+
                                              "                          </div>    "+
                                              "                      </div>      "+
                                              "                </td>    "+
                                              "            </tr>    "+
                                              "        </table>   "+
                                              "    </td>         "+                                              
                                              "    <td id=\"td2Producto_"+intCountProducto+"\" class=\"text-right\" style=\"vertical-align: bottom; border-bottom: 1px solid #D0D0D0;\">   "+
                                              "        <button onclick=\"fntRemoveProducto('"+intCountProducto+"');\" class=\"btn btn-secondary btn-xs cursor_pointer\">Remove</button>        "+
                                              "    </td>   "+
                                              "</tr>");
            fntEditProducto(intCountProducto);
            $("#slcClasificacionPadreHijo_"+intCountProducto).select2({
              theme: 'bootstrap4',
              width: 'style',
              placeholder: $(this).attr('placeholder'),
              allowClear: Boolean($(this).data('allow-clear')),
            });
            
            $("#slcClasificacionPadreHijo_"+intCountProducto).change(function (){
                
                //var arrSplit = $(this).attr("id").split("_");
                        
                //fntDrawSelectClasificacionPadreHijoDetalle(arrSplit[1]);
                    
            });
            
            intCountProducto++;
        }
        
        function fntRemoveProducto(intIndex){
                
            $("#tdGeneralProducto_"+intIndex).remove();
            
        }
        
        function fntSaveProductoClasificacionPadre(){
            
            var formData = new FormData(document.getElementById("frmProductoClasicicacionPadre"));
                    
            $(".preloader").fadeIn();
            $.ajax({
                url: "place_admin.php?setSaveProductoClasificacionPadre=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    $('#hrefModalFavoritoProdcuto').click();
                    //$('#lean_overlay').click();
                    
                    
                    
                }
                        
            });
            
        }
        
    </script>
    <?php
    
    die();
}

if( isset($_GET["setUbicacionPlace"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
        
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $strLat = isset($_GET["lat"]) ? $_GET["lat"] : 0;
    $strLng = isset($_GET["lng"]) ? $_GET["lng"] : 0;
    
    $strQuery = "UPDATE place
                 SET    ubicacion_lat = '{$strLat}',
                        ubicacion_lng = '{$strLng}'
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
    
    
    die();    
}

if( isset($_GET["fntDrawSeccionProductoDetale"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
        
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intPlaceClasificacionPadre = isset($_GET["key"]) ? intval($_GET["key"]) : 0;
    
    $arrPlaceProducto = fntGetPlaceProducto($intPlace, $intPlaceClasificacionPadre, true, $objDBClass);
    $arrClasificacionPadreHijo = fntGetPlaceClasificacionPadreHijo($intPlace, $intPlaceClasificacionPadre, true, $objDBClass);
    
    while( $rTMP = each($arrPlaceProducto) ){
        ?>
        <div class="row" style="cursor: pointer;" onclick="window.open('pro.php?iden=<?php print fntCoreEncrypt($rTMP["key"])?>')">
                                        
            <div class="col-9 m-0 p-0 pt-1 text-left" style="vertical-align: middle;">
                <h5 class="text-dark  lis-line-height-1 m-0 p-0" style="font-size: 16px;">
                    
                    <?php
                    
                    if( $rTMP["value"]["destacado"] == "Y" ){
                        ?>
                        
                        <i class="fa fa-star pl-2 lis-f-14"></i>
                        
                        <?php
                    }
                    
                    ?>
                        
                    <?php print $rTMP["value"]["nombre"]?>
                    
                    
                </h5>
                <h5 class="  lis-line-height-1 m-0 p-0" style="font-size: 14px;">
                    <?php print $rTMP["value"]["descripcion"]?>
                </h5>
                <h5 class="lis-primary  lis-line-height-1 m-0 p-0" style="font-size: 14px;">
                    Q <?php print $rTMP["value"]["precio"]?>
                </h5>
                <?php
                    
                if( isset($rTMP["value"]["clasificacion_padre_hijo"]) && count($rTMP["value"]["clasificacion_padre_hijo"]) > 0 ){
                    
                    ?>
                    <h3 class="lis-primary   m-0 p-0" style="font-size: 16px;">
                        
                    <?php
                    while( $arrTMP = each($rTMP["value"]["clasificacion_padre_hijo"]) ) {
                        
                        ?>
                        
                        <span class="badge  text-white" style="background-color: #dc3545;">
                            <?php print $arrClasificacionPadreHijo[$arrTMP["key"]]["tag_en"]?>
                        </span>
                        <?php
                    }
                    ?>
                    </h3>
                                                
                    <?php
                    
                }
                
                ?>
                        
            </div>
            <div class="col-3 m-0 p-0">
                <img src="<?php print $rTMP["value"]["logo_url"]?>" style="width: 140px; height: 110px;" class="img-fluid d-md-fle  border border-white lis-border-width-4 rounded mb-4 mb-md-0" alt="" />
                   
            </div>
        </div>        
        <?php
    }    
    
    die();    
}

if( $intPlace ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  

    $strQuery = "SELECT id_usuario,
                        num_producto,
                        num_galeria,
                        estado,
                        ubicacion_lat,
                        ubicacion_lng,
                        relevante
                 FROM   place
                 WHERE  id_place = {$intPlace} ";
    $arrPlace = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    $arrPlace = $objDBClass->db_fetch_array($qTMP);
    $objDBClass->db_free_result($qTMP);
    
    
    
    $arrPlaceClasificacion = array();
    $strQuery = "SELECT clasificacion.id_clasificacion,
                        clasificacion.nombre,
                        clasificacion.tag_en,
                        clasificacion.tag_es,
                        clasificacion.url_img,
                        clasificacion.color_hex,
                        clasificacion.producto
                 FROM   place_clasificacion,
                        clasificacion
                 WHERE  place_clasificacion.id_place = {$intPlace} 
                 AND    place_clasificacion.id_clasificacion = clasificacion.id_clasificacion
                 ";
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["nombre"] = $rTMP["nombre"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["tag_en"] = $rTMP["tag_en"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["tag_es"] = $rTMP["tag_es"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["url_img"] = $rTMP["url_img"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["color_hex"] = $rTMP["color_hex"];
        $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["producto"] = $rTMP["producto"];
            
    }   
    
    $objDBClass->db_free_result($qTMP); 
    
    $arrPlaceClasificacionPadre = array();
    $strQuery = "SELECT clasificacion_padre.id_clasificacion_padre,
                        clasificacion_padre.id_clasificacion,
                        clasificacion_padre.nombre,
                        clasificacion_padre.tag_en,
                        clasificacion_padre.tag_es
                 FROM   place_clasificacion_padre,
                        clasificacion_padre
                 WHERE  place_clasificacion_padre.id_place = {$intPlace} 
                 AND    place_clasificacion_padre.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre
                 ";
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["producto"] = isset($arrPlaceClasificacion[$rTMP["id_clasificacion"]]) ? $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["producto"] : "Y";
            
    }   
    
    $objDBClass->db_free_result($qTMP); 
    
    
        
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--
    <meta name="viewport" content="width=device-width, initial-scale=1">
    -->
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=NO, width=device-width, initial-scale=1.0">
    
    <title>Lister</title>
    <link rel="shortcut icon" href="dist/images/favicon.ico">
    <!--Plugin CSS-->
    <link href="dist/css/plugins.min.css" rel="stylesheet">
    <!--main Css-->
    <link href="dist/css/main.min.css" rel="stylesheet"> 
    <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
    
    <link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
    <link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> <!-- for live demo page -->
    <link href="dist/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
 
 
        
</head>

<body style="<?php print $boolInterno ? "overflow: hidden;" : ""?>   ">
    <style>
        
        .InputdirectionLTR {
            direction: ltr;
            width: 100%;
        }
        
        .grediant-bt-dark:before {
            background: #dc3545 !important;
        }
        
        .text-color-global {
            color: black !important;
        }
        
        .cardNoBackground {
            background-color: transparent; border: 0px
        }
        
        .cursor_pointer {
            cursor: pointer;
        }
        
        .lis-id-info {
          background-color: #ff214f;
          color: white;
          width: 36px;
          height: 36px;
        line-height: 38px; 
        }
          .lis-id-info:hover {
              opacity: 0.7;
              color: white ;
            background-color: #ff214f; }
        
        .lis-id-info-sm {
          background-color: #ff214f;
          color: white;
          width: 26px;
          height: 26px;
          font-size: 14px;
        line-height: 28px; 
        }
          .lis-id-info-sm:hover {
              opacity: 0.7;
              color: white ;
            background-color: #ff214f; }
    
    
               
        .select2-dropdown {
            background-color: white;
            border: 1px solid #aaa;
            border-radius: 4px;
            box-sizing: border-box;
            display: block;
            position: absolute;
            left: -100000px;
            width: 100%;
            z-index: 999999999999999;
            border: 2px solid red;
        }
        
        .bg-delete{
            background-color: #FFC0C0 !important;
        }
            
        .shadow_mia{
            box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.41);
        }
        
        .clockpicker-popover .popover-title {
            background-color: #fff;
            color: #999;
            font-size: 24px;
            font-weight: 700;
            line-height: 30px;
            text-align: center;
            direction: ltr;
        }
        
  
        .slideImgActiva {
            
            padding: 5px 0px 0px 0px ;
            border-radius: 20px !important;
            
            box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
            -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
            -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
            
        }
            
        
        .slideImgNoActiva {
            
            border-radius: 20px !important;
            padding: 10px 10px 10px 10px ;
            border-radius: 20px !important;
            
        }
        .slideImgNoActivaIMG {
            
            border-radius: 20px !important;
            
            box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);   
            -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);  
            -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
            
        }
        
        #lean_overlay {
            position: fixed;
            z-index: 1500;
            top: 0px;
            left: 0px;
            height: 100%;
            width: 100%;
            background: #000;
            display: none;
        }
        
        .popover {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 9999999999999999999999999999;
            display: block;
            max-width: 276px;
            padding: 1px;
            text-align: left;
            text-align: start;
            background-color: #fff;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: .3rem;
        }
        
        
        .transperant nav.navbar-toggleable-md.navbar-light {
            background: #dc3545;
            border: 0px;
        }
        
        #header-fix.nav-affix .navbar {
          background-color: #dc3545;
          /*
          box-shadow: 0 10px 20px -12px rgba(0, 0, 0, 0.42), 0 3px 20px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
          border-color: #000;
          */
          transition: 0.3s; }
        
         /*
        .login_form{
            display: none !important;
        }
        
        
        .slick-current {
            background: yellow !important; 
        }
        */
    </style>
    <!-- header
    
    <a class="navbar-brand mr-4 mr-md-5 p-4" style="border-radius: 0% 0% 50% 50%;  background-color: #dc3545; color: white; font-weight: bold; font-size: 45px; font-family: Calibri;" href="index.php">
                INGUATE
            </a>
    
     -->
    <div id="header-fix" class="header fixed-top transperant p-0 m-0" style="margin-left: 0px; ">
        
        <?php
        
        fntDrawNavbarPublico(false, true, $objDBClass);
                
        
        ?>
        
        <div id="divContenidoSeccion_1" class="p-0 m-0">
            <!--
            <h5 class="bg-danger pt-2 pl-3 m-0 text-white font-weight-bold" >
            
                <a href="#modalFormContendio" class="login_form" onclick="fntCargarModalSeccion('1');">
                    <i class="fa fa-pencil  text-center cursor_pointer p-2 bg-secondary text-white " style="border-radius: 50%; font-size: 14px;" ></i>
                </a>
                <span style="font-size: <?php print $boolMovil ? "20px" : "30px"?>;">
                    
                    Vintage Italian Beer Bar
                
                </span>
                
            </h5>
            <div class="bg-danger" style="  position: center; height: <?php print $boolMovil ? "100px" : "150px"?>; top: 0px;" >
                <div class="sliderInicio slider  "  style="direction: <?php print $boolMovil ? "ltr" : "ltr"?>; background: transparent; <?php print $boolMovil ? "padding-left: 20px;" : ""?>">
                    <?php
                    $arrPlace["num_galeria"] = 7;
                    for( $intNumFoto = 1; $intNumFoto <= $arrPlace["num_galeria"] ; $intNumFoto++ ){
                        
                        ?>
                        <div  aria-hidden="<?php print $intNumFoto == 1 ? "true" : "false"?>" style=" margin: 0px; padding: 0px;  ">
                            <div class="gallery text-center lis-relative classPrueba imgGalery_<?php print $intNumFoto;?> <?php print $intNumFoto == 1 ? "slideImgActiva" : "slideImgNoActiva"?>" style="margin: 5px;   ">
                                <img id="imgGalery_<?php print $intNumFoto;?>" src="dist/images/gallery<?php print $intNumFoto;?>.jpg" alt="" class="img-fluid slideImgNoActivaIMG" style="" >
                                <div class="gallery-fade fade w-100 h-100 rounded">
                                    <div class="d-table w-100 h-100">
                                        <div class="d-table-cell align-middle">
                                            <h4 class="mb-0"><a data-toggle="lightbox" data-gallery="example-gallery" href="dist/images/gallery<?php print $intNumFoto;?>.jpg" class="text-white">
                                                <?php print $intNumFoto;?>
                                                <i class="fa fa-search-plus"></i></a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        
                    }
                    
                    ?>
                      
                </div>                  
            </div>
            -->
        </div>
    </div>
    <!--End header -->
    
    
 
    
    <!-- Profile header -->
    <div class="profile-header " id="divSeccionDatos">
        <div class="container " id="divContenidoSeccion_2" style="">
            
            
        </div>
    </div>
    <!-- End header -->
           
    <!-- Profile Content -->
    <section class=" pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xs-12 mb-lg-0 text-right">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile"> 
                        
                        <div class="d-block d-sm-none">
                            <h6 class="lis-font-weight-500">
                                    
                                <a href="#modalFormContendio" class="login_form " onclick="fntCargarModalSeccion('8');" >
                                    <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                                </a>         
                                <i class="fa fa-align-right pl-2 lis-f-14"></i> Relevant
                            
                            </h6> 
                            <div id="divContenidoRelevante2" class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground ">
                                <?php print $arrPlace["relevante"]?>    
                            </div>
                        </div>
                        <h6 class="lis-font-weight-500">
                                
                            <a href="#modalFormContendio" class="login_form " onclick="fntCargarModalSeccion('3');" >
                                <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                            </a>         
                            <i class="fa fa-align-right pl-2 lis-f-14"></i> Description
                        
                        </h6> 
                        <div id="divContenidoSeccion_3" class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground ">
                            
                        </div>
                        
                        <h6 class="lis-font-weight-500"><i class="fa fa-chain pl-2 lis-f-14"></i> Products</h6>
                          
                        <?php
                        
                        while( $rTMP = each($arrPlaceClasificacionPadre) ){
                            
                            if( $rTMP["value"]["producto"] == "N" ){
                                continue;
                            }
                            
                            ?>
                            <h6 class="lis-font-weight-500 text-left">
                                <a href="#modalFormContendio" class="login_form " onclick="fntCargarModalSeccionProducto('<?php print fntCoreEncrypt($rTMP["value"]["id_clasificacion_padre"])?>');" >
                                    <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                                </a> 
                                <i class="fa fa-chain pl-2 lis-f-14"></i> 
                                <?php print $rTMP["value"]["tag_en"]?>
                                
                            </h6>   
                            <hr>
                            <div class="divCategoriaProducto"  id="divContenidoCategoriaProducto_<?php print $rTMP["key"]?>">
                                
                            </div>
                            
                            <hr>
                            <?php
                        }
                        
                        ?>
                               
                        
                        <h6 class="lis-font-weight-500">
                            <a href="#modalFormContendio" class="login_form " onclick="fntCargarModalSeccion('4');" >
                                <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                            </a>
                            <i class="fa fa-chain pl-2 lis-f-14"></i> 
                            Amenities
                        </h6>
                        
                        <div id="divContenidoSeccion_4" class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground">
                            
                        </div>
                        
                        <h6 class="lis-font-weight-500">
                            
                            Location
                            <i class="fa fa-map-o pl-2 lis-f-14"></i> 
                            
                            <a  class="login_form " onclick="fntSetUbicacionPlace();" >
                                <i class=" lis-id-info-sm p-1  lis-rounded-circle-50 text-center cursor_pointer" onclick="">Save</i>
                            </a>
                        </h6>
                        <div class="card lis-brd-light wow fadeInUp mb-4 cardNoBackground">
                            <div class="card-body p-1" style="direction: ltr;">    
                                
                                <style>
                                  #map {
                                    height: 100%;
                                  }
                                 
                                  .controls {
                                    margin-top: 10px;
                                    border: 1px solid transparent;
                                    border-radius: 2px 0 0 2px;
                                    box-sizing: border-box;
                                    -moz-box-sizing: border-box;
                                    height: 32px;
                                    outline: none;
                                    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                                  }

                                  #pac-input {
                                    background-color: #fff;
                                    font-family: Roboto;
                                    font-size: 15px;
                                    font-weight: 300;
                                    margin-left: 12px;
                                    padding: 0 11px 0 13px;
                                    text-overflow: ellipsis;
                                    width: 300px;
                                  }

                                  #pac-input:focus {
                                    border-color: #4d90fe;
                                  }

                                  .pac-container {
                                    font-family: Roboto;
                                  }

                                  #type-selector {
                                    color: #fff;
                                    background-color: #4d90fe;
                                    padding: 5px 11px 0px 11px;
                                  }

                                  #type-selector label {
                                    font-family: Roboto;
                                    font-size: 13px;
                                    font-weight: 300;
                                  }
                                </style>
                            
                                <input type="hidden" name="hidLngUbicacion" id="hidLngUbicacion">
                                <input type="hidden" name="hidLatUbicacion" id="hidLatUbicacion">
                                <input type="hidden" name="hidPlaceId" id="hidPlaceId">
                                <input id="pac-input" class="controls" type="text"
                                    placeholder="Enter a location">
                                <div id="type-selector" class="controls">
                                  <input type="radio" name="type" id="changetype-all" checked="checked">
                                  <label for="changetype-all">All</label>

                                  <input type="radio" name="type" id="changetype-establishment">
                                  <label for="changetype-establishment">Establishments</label>

                                  <input type="radio" name="type" id="changetype-address">
                                  <label for="changetype-address">Addresses</label>

                                  <input type="radio" name="type" id="changetype-geocode">
                                  <label for="changetype-geocode">Geocodes</label>
                                </div>
                                <div id="map-canvas" style="height: 400px;"></div>
                                 
                                
                                
                            </div>
                        </div>
                                 
                    </div>
                </div>
                <div class="col-12 col-lg-4 text-right">
                    
                    <div class="d-none d-sm-block">
                    
                        <h6 class="lis-font-weight-500">
                            <a href="#modalFormContendio" class="login_form " onclick="fntCargarModalSeccion('8');" >
                                <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                            </a>
                            <i class="fa fa-star pl-2 lis-f-14"></i> 
                            Relevant
                        </h6>
                        <div class="card lis-brd-light wow fadeInUp mb-4">
                            <div class="card-body p-4" >
                                <p style="font-weight: bold;" id="divContenidoRelevante">
                                    <?php print $arrPlace["relevante"]?>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <h6 class="lis-font-weight-500"><i class="fa fa-star pl-2 lis-f-14"></i> Listing Ratings</h6>
                    <div class="card lis-brd-light wow fadeInUp mb-4" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="card-body p-4">
                            <ul class="list-inline mb-0 lis-light-gold font-weight-normal h4">
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                <li class="list-inline-item"> 3.0</li>
                            </ul>
                            <small>2.77 average based on 19184 ratings</small>
                        </div>
                    </div>
                    
                    <h6 class="lis-font-weight-500">
                        <a href="#modalFormContendio" class="login_form " onclick="fntCargarModalSeccion('5');" >
                            <i class="fa fa-pencil lis-id-info-sm   lis-rounded-circle-50 text-center cursor_pointer" onclick=""></i>
                        </a>
                        <i class="fa fa-clock-o pl-2 lis-f-14"></i> 
                        Opening Hours
                    </h6>
                    <div id="divContenidoSeccion_5" class="card lis-brd-light wow fadeInUp mb-4">
                        <div class="card-body p-4">
                            <button class="text-btn bg-transparent border-0  w-100 text-left collapsed px-0 lis-light" data-toggle="collapse" data-target="#demo"><span class='lis-light-green font-weight-bold'>Open Today</span> : 9:30AM - 7:30PM </button>
                            <div id="demo" class="collapse">
                                <dl class="row mb-0 mt-4 lis-line-height-2">
                                    <dt class="col-sm-6 font-weight-normal">Monday</dt>
                                    <dd class="col-sm-6">9:30AM - 7:30PM</dd>
                                    
                                    <dt class="col-sm-6 font-weight-normal">Tuesday</dt>
                                    <dd class="col-sm-6">9:30AM - 7:30PM</dd>
                                    
                                    <dt class="col-sm-6 font-weight-normal">Wednesday</dt>
                                    <dd class="col-sm-6">9:30AM - 7:30PM</dd>
                                    
                                    <dt class="col-sm-6 font-weight-normal">Thursday</dt>
                                    <dd class="col-sm-6">9:30AM - 7:30PM</dd>
                                    
                                    <dt class="col-sm-6 font-weight-normal">Friday</dt>
                                    <dd class="col-sm-6">9:30AM - 7:30PM</dd>
                                    
                                    <dt class="col-sm-6 font-weight-normal">Saturday</dt>
                                    <dd class="col-sm-6">Closed</dd>
                                    
                                    <dt class="col-sm-6 font-weight-normal">Sunday</dt>
                                    <dd class="col-sm-6">Closed</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    
                    <h6 class="lis-font-weight-500">
                        
                        <i class="fa fa-tags pl-2 lis-f-14"></i> 
                        Categories
                    </h6>
                    <div class="card lis-brd-light wow fadeInUp mb-4">
                        <div class="card-body p-4">
                            <ul class="list-inline mb-0 lis-light-gold font-weight-normal h4">
                             
                            <?php 
                            
                            while( $rTMP = each($arrPlaceClasificacion) ){
                                
                                ?>
                                <li class="list-inline-item">
                                     <div style="background-color: <?php print "#".$rTMP["value"]["color_hex"]?>; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                        <img src="<?php print $rTMP["value"]["url_img"]?>">
                                    </div>
                                </li>
                                <?php
                                
                            }
                            
                            ?>
                        
                            </ul>  
                                
                            
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
    </section>
     <!-- End Profile Content -->
    
                
    
     <!-- Footer-->
    <section class="image-bg footer lis-grediant grediant-bt pb-0">
        <div class="background-image-maker"></div>
        <div class="holder-image">
            <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none">
        </div>
        <div class="container">
            <div class="row pb-5">
                <div class="col-12 col-md-8">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                            <h5 class="footer-head">Useful Links</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                            <h5 class="footer-head">My Account</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Dashboard</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Profile</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> My Listing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Favorites</A></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                            <h5 class="footer-head">Pages</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <h5 class="footer-head">Help</h5>
                            <ul class="list-unstyled footer-links lis-line-height-2_5">
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-logo">
                        <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a> 
                    </div>
                    <p class="my-4">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu in  felis eu pede mollis enim.</p>
                    <a href="#" class="text-white"><u>App Download</u></a> 
                    <ul class="list-inline mb-0 mt-2">
                        <li class="list-inline-item"><A href="#"><img src="dist/images/play-store.png" alt="" class="img-fluid" /></a></li>
                        <li class="list-inline-item"><A href="#"><img src="dist/images/google-play.png" alt="" class="img-fluid" /></a></li>
                        <li class="list-inline-item"><A href="#"><img src="dist/images/windows.png" alt="" class="img-fluid" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom mt-5 py-4">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 text-center text-md-right mb-3 mb-md-0">
                        <span> &copy; 2017 Lister. Powered by <a href="#" class="lis-primary">PSD2allconversion.</a></span>
                    </div> 
                    <div class="col-12 col-md-6 text-center text-md-left">
                        <ul class="list-inline footer-social mb-0">
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item"><A href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div> 
            </div>
        </div>
    </section>
    <!--End  Footer-->

    
               
    <!-- Top To Bottom-->
    <!-- Top To Bottom-->
    <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
        <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
    </a>
    <!-- End Top To Bottom-->
    
    <!-- Button trigger modal -->
    
            
    <div class="container">

        <div id="modalFormContendio" class="popupContainer" style="display: none; width: 90%; height: 90%; overflow: auto;">
            

        </div>

    </div>

    <!-- jQuery -->               
    <script src="dist/js/plugins.min.js"></script>
    <script src="dist/js/common.js"></script>
    <script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
       
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMWRurR0tA9aejnmohWlKtmuo_bjxjfdc&libraries=places"></script>
    <script src="dist/js/jsAntigua.js"></script>
    <script src="dist/clockpicker/jquery-clockpicker.min.js"></script>
    <script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>

    <script>   
        
        
        <?php
        
        if( !empty($arrPlace["ubicacion_lng"]) ){
            ?>
            var center = new google.maps.LatLng(<?php print $arrPlace["ubicacion_lat"];?>, <?php print $arrPlace["ubicacion_lng"];?>);
            
            <?php
        }
        else{
            ?>
            var center = new google.maps.LatLng(14.5585707, -90.72952299999997);
            
            <?php
        }
        
        ?>
                
        var markerInicial;
        var map;
        
        var d = new Date();
        var intDiaSemana = d.getDay()
        
        
        $( document ).ready(function() {
            
            //initializeMap();
            
            $('.sliderInicio').slick({ 
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 4,
                arrows: false,
                <?php print $boolMovil ? "ltr: true," : "ltr: true,"?>
                autoplay: <?php print $boolMovil ? "false" : "true"?> ,
                speed: <?php print $boolMovil ? "100" : "1500"?>,
                responsive: [
                    {
                        breakpoint: 1199,
                        settings: {
                            arrows: false,
                            centerMode: false,
                            centerPadding: '0px',
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            infinite: false,
                            arrows: false,
                            centerMode: false,
                            centerPadding: '0px',
                            speed: 100,
                            slidesToShow: 1.5,
                            slidesToScoll: 1.5,
                            
                        }
                    }
                ]   
            });

            arrOrden = new Array();
            intReal = 1;
            intTotal = <?php print $arrPlace["num_galeria"];?>;
            for( i = (intTotal/2) ; i >= 0; i-- ){
                index = i ;
                
                arrOrden[index] = new Array();
                arrOrden[index]["index"] = index;
                arrOrden[index]["real"] = intReal;
                intReal++;
            }
              
            for( i = intTotal  ; i > (intTotal/2) + 1; i-- ){
                index = i - 1;
                arrOrden[index] = new Array();
                arrOrden[index]["index"] = index;
                arrOrden[index]["real"] = intReal;
                intReal++;
            }
            
            $('.sliderInicio').on('afterChange', function(event, slick, currentSlide, nextSlide){
                var dataId = $('.slick-current').attr("data-slick-index");    
              
              
                dataId++;
                indexReal = dataId;                  
              $('.classPrueba').removeClass('slideImgActiva');        
              $('.classPrueba').addClass('slideImgNoActiva');        
              $('.imgGalery_'+indexReal).removeClass('slideImgNoActiva');        
              $('.imgGalery_'+indexReal).addClass('slideImgActiva');        
              
              
            });
            
            $('.sliderInicio').slick('slickGoTo',"0");
     
            
            $("#divSeccionDatos").css("padding-top", ( parseFloat($("#header-fix").height()) <?php print !$boolMovil ? "+ parseFloat($('.sliderInicio').height() / 2)" : ""?> )+"px");
            
            initializeMap();
            
            fntDrawSeccion(1, true);
            
            
            
            
        }); 
        
        function initializeMap() {

            var mapOptions = {
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: center
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            <?php
        
            if( empty($arrPlace["ubicacion_lng"]) ){
                ?>
                if (navigator.geolocation) {
                    
                    navigator.geolocation.getCurrentPosition(function(position) {
                        
                        
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        //console.log(position);    
                        var markerOptions = { 
                            position: pos, 
                            draggable: true
                        }
                        
                        markerInicial = new google.maps.Marker(markerOptions);
                        markerInicial.setMap(map);
                        
                        map.setCenter(pos);
                        
                        $("#hidLatUbicacion").val(position.coords.latitude);
                        $("#hidLngUbicacion").val(position.coords.longitude);
                        
                        
                        
                        
                    }, function() {
                        
                        handleLocationError(true, infoWindow, map.getCenter());
                        
                    });
                    
                } 
                else {
                    
                    handleLocationError(false, infoWindow, map.getCenter());
                    
                }
                <?php
            }
            else{
                ?>
                 //console.log(position);    
                var markerOptions = { 
                    position: center, 
                    draggable: true
                }
                
                markerInicial = new google.maps.Marker(markerOptions);
                markerInicial.setMap(map);
                
                map.setCenter(center);
                map.setZoom(16);
                                          
                $("#hidLatUbicacion").val(<?php print $arrPlace["ubicacion_lat"];?>);
                $("#hidLngUbicacion").val(<?php print $arrPlace["ubicacion_lng"];?>);
                
                <?php
            }
            
            ?>
                
            
            var input = /** @type {!HTMLInputElement} */(
                document.getElementById('pac-input'));

            var types = document.getElementById('type-selector');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
              map: map,
              anchorPoint: new google.maps.Point(0, -29),
              draggable: true
            });

            autocomplete.addListener('place_changed', function() {
              infowindow.close();
              marker.setVisible(false);
              var place = autocomplete.getPlace();
              if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
              }
              else{
                  markerInicial.setMap(null);
              }

              
              //console.log(place);
              //console.log(place.place_id);
              $("#hidPlaceId").val(place.place_id);
              // If the place has a geometry, then present it on a map.
              if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
                
                $("#hidLatUbicacion").val(place.geometry.location.lat);
                $("#hidLngUbicacion").val(place.geometry.location.lng);
                
              } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
                $("#hidLatUbicacion").val(place.geometry.location.lat);
                $("#hidLngUbicacion").val(place.geometry.location.lng);
                
              }
              
              
              //console.log($("#hidLatUbicacion").val());
              //console.log($("#hidLngUbicacion").val());
                
              
              marker.setPosition(place.geometry.location);
              marker.setVisible(true);
              
              var address = '';
              if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
              }

              infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
              infowindow.open(map, marker);
            });

            // Sets a listener on a radio button to change the filter type on Places
            // Autocomplete.
            function setupClickListener(id, types) {
              var radioButton = document.getElementById(id);
              radioButton.addEventListener('click', function() {
                autocomplete.setTypes(types);
              });
            }

            setupClickListener('changetype-all', []);
            setupClickListener('changetype-address', ['address']);
            setupClickListener('changetype-establishment', ['establishment']);
            setupClickListener('changetype-geocode', ['geocode']);
               
        }
               
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                  'Error: The Geolocation service failed.' :
                                  'Error: Your browser doesn\'t support geolocation.');
        }
        
        function fntCargarModalSeccion(intSeccion, strKey){
            
            strKey = !strKey ? "" : strKey;
                        
            $.ajax({
                url: "place_admin.php?fntDrawSeccionForm=true&place=<?php print fntCoreEncrypt($intPlace);?>&&seccion="+intSeccion+"&key="+strKey, 
                success: function(result){
                    
                    $("#modalFormContendio").html(result);
                
                }
            });
                
        }
        
        function fntCargarModalSeccionProducto(intPlaceClasificacionPadre){
            
            $.ajax({
                url: "place_admin.php?fntCargarModalSeccionProducto=true&place=<?php print fntCoreEncrypt($intPlace);?>&clasificacionPadre="+intPlaceClasificacionPadre, 
                success: function(result){
                    
                    $("#modalFormContendio").html(result);
                
                }
            });
                
        }
        
        function fntSetUbicacionPlace(){
            
            $.ajax({
                url: "place_admin.php?setUbicacionPlace=true&place=<?php print fntCoreEncrypt($intPlace);?>&lat="+$("#hidLatUbicacion").val()+"&lng="+$("#hidLngUbicacion").val(), 
                success: function(result){
                    
                    swal({
                        title: "Data set",
                        //text: "Select An Option",
                        type: "success",
                        showConfirmButton: true,
                        //timer: 2000
                    });    
                
                }
            });
            
            
        }
        
        function fntDrawSeccion(intSeccion, boolSecuenciaInicio){ 
        
            boolSecuenciaInicio = !boolSecuenciaInicio ? false : boolSecuenciaInicio;  
            
            $.ajax({
                url: "place_admin.php?fntDrawSeccion=true&place=<?php print fntCoreEncrypt($intPlace);?>&seccion="+intSeccion+"&diaSemana="+intDiaSemana, 
                success: function(result){
                    
                    $("#divContenidoSeccion_"+intSeccion).html(result);
                    
                    var loginform = $(".login_form");
                    var userlogin = $(".user_login");
                    var userregister = $(".user_register");
                    
                    // Plugin options and our code
                    loginform.leanModal({
                        top: 50,
                        overlay: 0.6,
                        closeButton: ".modal_close"
                    });
                    // Calling Login Form


                    // Calling Login Form
                    loginform.on('click', function () {
                        userregister.hide();
                        userlogin.show();
                        return false;
                    });

                    // Calling Register Form
                    $(".register_form").on('click', function () {
                        userlogin.hide();
                        userregister.show();
                        $(".header_title").text('Register');
                        return false;
                    });
                
                    if( boolSecuenciaInicio ){
                        
                        fntDrawSeccion(2);
                        fntDrawSeccion(3);
                        fntDrawSeccion(4);
                        fntDrawSeccion(5);
                        
                        <?php
                        
                        reset($arrPlaceClasificacionPadre);
                        while( $rTMP = each($arrPlaceClasificacionPadre) ){
                            ?>
                            fntDrawSeccionProductoDetalle('<?php print $rTMP["key"];?>');
                            
                            <?php
                        }
                        
                        ?>    
                        
                    }
                
                }
            });    
            
        }
        
        function fntDrawSeccionProductoDetalle(intKey){   
            
            $.ajax({
                url: "place_admin.php?fntDrawSeccionProductoDetale=true&place=<?php print fntCoreEncrypt($intPlace);?>&key="+intKey, 
                success: function(result){
                    
                    $("#divContenidoCategoriaProducto_"+intKey).html(result);
                    
                    var loginform = $(".login_form");
                    var userlogin = $(".user_login");
                    var userregister = $(".user_register");
                    
                    // Plugin options and our code
                    loginform.leanModal({
                        top: 50,
                        overlay: 0.6,
                        closeButton: ".modal_close"
                    });
                    // Calling Login Form


                    // Calling Login Form
                    loginform.on('click', function () {
                        userregister.hide();
                        userlogin.show();
                        return false;
                    });

                    // Calling Register Form
                    $(".register_form").on('click', function () {
                        userlogin.hide();
                        userregister.show();
                        $(".header_title").text('Register');
                        return false;
                    });
                
                }
            });    
            
        }
        
    </script>   
 </body>

</html>