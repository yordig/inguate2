<?php
session_start();

include "core/function_servicio.php";

$strPlaceId = isset($_GET["p"]) ? trim(($_GET["p"])) : 0;

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$arrInfoDispositivoi = fntGetInformacionClienteNavegacion();
    
if( $arrInfoDispositivoi["plataforma"] == "iPhone" )
    $boolMovil = true;


$strLenguaje = isset($_GET["len"]) ? trim($_GET["len"]) : "es";
$strLenguaje = isset(sesion["lenguaje"]) ? sesion["lenguaje"] : "es";

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
  
include "core/dbClass.php";
$objDBClass = new dbClass();
 
$arrCatalogoFondo = fntCatalogoFondoPlacePublico();
    
$strColorHex = $arrCatalogoFondo[1]["color_hex"];

$arrDatosPlace = array();
    
fntDrawHeaderPublico($boolMovil);


$strQuery = "SELECT negocio.id_negocio,
                    negocio.nombre,
                    negocio.lat,
                    negocio.lng,
                    negocio.direccion,
                    negocio.rango,
                    negocio.precio,
                    negocio.telefono,
                    negocio.estado,
                    negocio_galeria.id_negocio_galeria,
                    negocio_galeria.url,
                    negocio_horario.id_negocio_horario,
                    negocio_horario.dia,
                    negocio_horario.horario
             FROM   negocio
                        LEFT JOIN negocio_galeria
                            ON  negocio_galeria.id_negocio = negocio.id_negocio
                      
                        LEFT JOIN negocio_horario
                            ON  negocio_horario.id_negocio = negocio.id_negocio
                      
             WHERE  md5(negocio.id_negocio) = '{$strPlaceId}'
             "; 
$arrCatalogoPrecioGoogle = fntCatalogoPriceLevelGoogle();

$arrPlace = array();
$arrDatos = array();
$qTMP = $objDBClass->db_consulta($strQuery);
while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
    
    $arrDatos["id_negocio"] = $rTMP["id_negocio"];
    $arrDatos["nombre"] = $rTMP["nombre"];
    $arrDatos["lat"] = $rTMP["lat"];
    $arrDatos["lng"] = $rTMP["lng"];
    $arrDatos["direccion"] = $rTMP["direccion"];
    $arrDatos["rating"] = $rTMP["rango"];
    $arrDatos["telefono"] = $rTMP["telefono"];
    $arrDatos["estado"] = $rTMP["estado"];
    $arrDatos["price_level"] =  $arrCatalogoPrecioGoogle[$rTMP["precio"]]["nombre"];
    
    
    if( intval($rTMP["id_negocio_galeria"]) ){
        
        $arrDatos["galeria"][$rTMP["id_negocio_galeria"]]["url"] = $rTMP["url"];
        
    } 
        
    if( intval($rTMP["id_negocio_horario"]) ){
        
        $arrDatos["horario"][$rTMP["id_negocio_horario"]]["dia"] = $rTMP["dia"];
        $arrDatos["horario"][$rTMP["id_negocio_horario"]]["horario"] = $rTMP["horario"];
    
    } 
        
}

$objDBClass->db_free_result($qTMP);

               /*
$strApiKey = fntGetApiKeyPlace();

$strUrlApi = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$strPlaceId."&language=".$strLenguaje."&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,utc_offset,vicinity,formatted_phone_number,international_phone_number,opening_hours,website,price_level,rating,review,user_ratings_total&key=".$strApiKey;
        
$objJsonDatos = file_get_contents($strUrlApi);
$objJsonDatos = json_decode($objJsonDatos);
//drawdebug($objJsonDatos);

$arrFotos = isset($objJsonDatos->result->photos) ? $objJsonDatos->result->photos : array();
$arrDatos = array();

$arrDatos["nombre"] = isset($objJsonDatos->result->name) ? $objJsonDatos->result->name : "";
$arrDatos["direccion"] = isset($objJsonDatos->result->formatted_address) ? $objJsonDatos->result->formatted_address : "";
$arrDatos["telefono"] = isset($objJsonDatos->result->international_phone_number) ? $objJsonDatos->result->international_phone_number : "";
$arrDatos["lat"] = isset($objJsonDatos->result->geometry->location->lat) ? $objJsonDatos->result->geometry->location->lat : "14.5585707";
$arrDatos["lng"] = isset($objJsonDatos->result->geometry->location->lng) ? $objJsonDatos->result->geometry->location->lng : "-90.72952299999997";
$arrDatos["horario"] = isset($objJsonDatos->result->opening_hours) ? $objJsonDatos->result->opening_hours : array();
$arrDatos["rating"] = isset($objJsonDatos->result->rating) ? intval($objJsonDatos->result->rating) : 0;

          */
   

   
?>   
<link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
<link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> 
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>

<link rel="stylesheet" type="text/css" href="dist/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="dist/slick/slick-theme.css"/>

<link href="dist/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet"> 


<input type="hidden" id="hidOrigen" value="<?php print $intOrigen;?>">
<input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intKey);?>">
<input type="hidden" id="hidTipo" value="<?php print $intTipo;?>">
<input type="hidden" id="hidTexto" value="<?php print $strTexto;?>">
<input type="hidden" id="hidRango" value="<?php print $strRango;?>">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">


<style>
    
        
    .text-background-theme {
        background-color: #<?php print $strColorHex?> !important;
        
    }
    .text-color-theme {
        color: #<?php print $strColorHex?> !important;
    
    }

    .btn-theme-circular {
        background-color: #<?php print $strColorHex?>;
        color: white;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
        position:absolute;
        top:0%;
        right:2%;
        z-index: 99;

    }
            
    .container__ {
      display: flex;
      justify-content: space-around;
      align-items: flex-start;
      height: 100%;
    }
        
    .police {
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    } 
                    
    
</style>
<div class="container-fluid m-0 p-0" >
    
    <div class="row m-0 p-0">
    
        <div class="col-12 p-0 m-0 text-center" >
        
            <img src="<?php print  $arrCatalogoFondo[1]["url_fondo"]?>" style="width: 100%; height: auto;">
                   
            <div class="m-0 p-0 pt-2 pt-lg-4  text-center" style="position: absolute; top: 0px; width: 100%;"> 
                
                <?php
                
                fntDrawHeaderPrincipal(1, true, $objDBClass);
                
                ?>        
                
                <style>
                    .imgSlie:focus{
                        border: 0px !important;
                    }
                </style>
                <!-- Contenido -->       
                <div class="row p-0 m-0 justify-content-center mt-4" style="">
                    <div class="col-12 p-0 m-0 ">
                        
                        <div id="divContenidoSeccion_1">
                            <div class="slidePlace slider center" style="" >
                                
                                <?php
                                
                                if( isset($arrDatos["galeria"]) && is_array($arrDatos["galeria"]) && count($arrDatos["galeria"])  > 0 ){
                                    
                                    $intCount = 1;
                                    while( $rTMP = each($arrDatos["galeria"]) ){
                                            
                                        
                                        ?>
                                        <!--
                                        <div class="p-0  imgGaleryDIV_<?php print $intCount;?>" style="" yordi="<?php print $intCount;?>" style="">
                                            <img src="imgInguate.php?t=up&src=<?php print $rTMP["value"]["url"];?>" class="img-fluid p-3 imgSlideGallery imgGalery_<?php print $intCount;?>" 
                                            style="border-radius: 8% !important; width: 100%; height: <?php print $boolMovil ? "200px" : "250px"?>; object-fit: cover;"> 
                                        </div>
                                              -->
                                        <div class="p-0 imgSlie imgGaleryDIV_<?php print $intCount;?>" style="" yordi="<?php print $intCount;?>" style="">
                                            <img src="imgInguate.php?t=up&src=<?php print $rTMP["value"]["url"];?>" class="img-fluid p-3 imgSlie imgSlideGallery imgGalery_<?php print $intCount;?>" style="border-radius: 8% !important; width: 100%; height: auto; object-fit: cover; "> 
                                        </div>
                                        
                                        <?php
                                        $intCount++;
                                    
                                    }
                                    
                                }
                                else{
                                    
                                    
                                    for( $i = 1 ; $i <= 5; $i++ ){
                                        
                                        ?>
                                        <div class="p-0  imgGaleryDIV_<?php print $i;?>" style="" yordi="<?php print $i;?>" style="">
                                            <img src="images/Noimagee.jpg?1" class="img-fluid p-3 imgSlideGallery imgGalery_<?php print $i;?>" style="border-radius: 10% !important; "> 
                                        </div>
                                        <?php
                                    }
                                }
                                /*
                                $intCount = 1;
                                if( is_array($arrFotos) && count($arrFotos)  > 0 ){
                                    
                                    $strLinkMap = "https://maps.googleapis.com/maps/api/place/photo?&key=".$strApiKey."&maxwidth=400&photoreference=";
                        
                                    while( $rTMP = each($arrFotos) ){
                                        
                                        $path= $strLinkMap.$rTMP["value"]->photo_reference;
                                        $type = pathinfo($path, PATHINFO_EXTENSION);
                                        $data = file_get_contents($path);
                                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                        
                                            
                                        
                                        ?>
                                        <div class="p-0  imgGaleryDIV_<?php print $intCount;?>" style="" yordi="<?php print $intCount;?>" style="">
                                            <img src="<?php print $base64;?>" class="img-fluid p-3 imgSlideGallery imgGalery_<?php print $intCount;?>" 
                                            style="border-radius: 8% !important; width: 100%; height: <?php print $boolMovil ? "200px" : "250px"?>; object-fit: cover;"> 
                                        </div>
                                        
                                        <?php
                                        $intCount++;
                                        if( $intCount == 4 ){
                                            break;
                                        }
                                    }
                                    
                                }
                                
                                */    
                                ?>
                                
                            </div>
                        </div>
                                                
                    </div>
                </div>
                
                <div class="row p-0 m-0 pl-1 justify-content-center container__" style="">
                    
                    <div class="col-12 col-lg-4 p-1 m-0 bg-white police" style="" id="divContenidoSeccion_2">
                        
                                  
                        <div class="row m-0 p-0">
                            <div class="col-12 m-0 p-0 text-center text-lg-left">
                               
                                <h4 class="text-center text-lg-left m-0 p-0">
                                    <?php print $arrDatos["nombre"]?>
                                </h4>
                                
                                <p class="text-center text-lg-left m-0" style="">
                                    <?php print $arrDatos["direccion"]?>
                                    <br>
                                </p>
                                
                                
                                <div class="row m-0 p-0">
                                    <div class="col-6 m-0 p-0">
                                        
                                        <h4 class="m-0 p-0 text-center text-lg-left" style="line-height: 0.1;">
                                        <?php
                                        for( $i = 1; $i <= 5 ;  $i++ ){
                                            
                                            $strEstrella =  $i <=  $arrDatos["rating"] ? "fa-star" : "fa-star-o";
                                            ?>
                                            <i class="fa  <?php print $strEstrella;?> text-color-theme m-0 p-0" style="font-size: 12px;"></i>
                                            <?php
                                        }
                                        ?>
                                        </h4>
                                            
                                    </div>
                                    <?php 
                                    
                                    if( $arrDatos["price_level"] != ""  ){
                                        
                                        ?>
                                        <div class="col-6">
                                            
                                            <span class="badge  text-white text-background-theme" >
                                                
                                                <?php print $arrDatos["price_level"]?>
                                                
                                            </span>
                                            
                                        </div>
                                        <?php
                                    }
                                    
                                    ?>
                                        
                                </div>
                            
                                <button class="btn btn-sm btn-outline btn-outline-light mt-2" onclick="location.href = 'https://maps.google.com/?q=<?php print $arrDatos["lat"]?>,<?php print $arrDatos["lng"]?>' " style="border:  1px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>;" >
                                    Google Maps <i class="fa fa-paper-plane"></i>
                                </button>

                                <button class="btn btn-sm btn-outline btn-outline-light mt-2 " onclick="location.href = 'https://waze.com/ul?ll=<?php print $arrDatos["lat"]?>,<?php print $arrDatos["lng"]?>&z=10' " style="border:  1px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>;" >
                                    Waze <i class="fa fa-rocket"></i>
                                </button>
                                <?php
                                if( isset($arrDatos["horario"]) && count($arrDatos["horario"]) > 0 ){
                                    
                                    ?>
                                       
                                    <h6 class="text-center text-lg-left m-0 mt-2" style="cursor: pointer; text-decoration: underline;" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                        Horario  
                                        <i class="fa fa-angle-down"></i>
                                    </h6>
                                    
                                    <div class="collapse" id="collapseExample">
                                        
                                        <div class="row">    
                                            <div class="col-12 text-left">
                                                <table style="width: 100%;">
                                                    <?php 
                                                    
                                                    while( $rTMP = each($arrDatos["horario"]) ){
                                                        ?>
                                                        
                                                        <tr>
                                                            <td style="width: 100%; " class="pl-3">
                                                            
                                                                <div class="row p-0 m-0">
                                                                    <div class="col-6 text-right p-0 m-0 pr-4 col-lg-3 text-lg-left">
                                                                        <?php print $rTMP["value"]["dia"]?>
                                                                    </div>
                                                                    <div class="col-6 text-left p-0 m-0  pl-2 col-lg-3 text-lg-left">
                                                                        <?php print $rTMP["value"]["horario"]?>
                                                                    </div>
                                                                </div>
                                                                
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    
                                                    ?>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                        
                                        
                                    <br>
                                    <?php
                                    
                                }
                                ?>
                                
                                <!--
                                <h6 class="text-center text-lg-left" style="line-height: 0.1;">
                                    
                                    <a href="javascript:void(0)" class="text-dark">
                                        <i class="fa fa-clock-o pl-1 text-color-theme"></i>
                                        Horario
                                        
                                    </a>
                                </h6>
                                <div class="accordion" id="accordionExample">
                                    <div class="card " style="border: 0px;">
                                        <div class="card-header p-0 m-0 bg-white" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; border: 0px;">
                                            
                                            <div class="row">
                                                <div class="col-12 text-lg-left text-center">
                                                    
                                                    &nbsp;
                                                    &nbsp;
                                                    <?php 
                                                    
                                                    if( isset($arrDatos["horario"]->open_now) && $arrDatos["horario"]->open_now ){
                                                        ?>
                                                        <span class="text-success font-weight-bold">
                                                            Hoy Abierto
                                                        </span>
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                        <span class="text-danger font-weight-bold">
                                                            Cerrado Hoy
                                                        </span>
                                                        <?php
                                                    }
                                                    
                                                    ?>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <i class="fa fa-angle-down text-color-theme"></i>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body p-0 m-0">
                                                
                                                <dl class="row mb-0 mt-4 lis-line-height-2">
                                                
                                                <?php
                                                
                                                if( isset($arrDatos["horario"]->weekday_text) ){
                                                    
                                                    while( $rTMP = each($arrDatos["horario"]->weekday_text) ){
                                                        ?>
                                                        <dt class="col-12 text-left"><?php print $rTMP["value"]?></dt>
                                                    
                                                        <?php
                                                    }
                                                    
                                                }
                                                
                                                ?>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                
                                   --> 
                                
                                
                                
                            </div>
                        </div>
                        
                        
                    </div>
                    
                    <div class="col-12 col-lg-8 p-0 m-0 pl-1 pr-1 bg-white" style="">
                        
                        
                        <div class="row p-0 m-0 ">
                            <div class="col-12 p-0 m-0">
                                    
                                <div class="row p-0 m-0">
                                    <div class="col-12 pt-1">
                                         
                                         <iframe style="width: 100%; height: 450px; border: 0px;"  src="https://www.google.com/maps/embed/v1/place?key=<?php print "AIzaSyABJ9worirb3vAoxbghUZj_rNUTrfTtRPY"?>&q=<?php print $arrDatos["direccion"]?>" allowfullscreen></iframe>
                                         
                                         
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                
                    </div>
                    
                </div>
            
                <?php
                        
                fntDrawFooter($arrCatalogoFondo[1]["color_hex"]);
                
                ?>
            
            </div>
                   
        </div>
    </div>
    
    <?php
        
    fntShowBarNavegacionFooter(true, $arrCatalogoFondo[1]["color_hex"]);
    
    ?>
    
</div>
     
   
<script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>
<script type="text/javascript" src="dist/slick/slick.min.js"></script>
<script src="dist/clockpicker/jquery-clockpicker.min.js"></script>
<script src="dist/js/jsAntigua.js"></script>
<script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
        
        
<style>
    
    .flotante_change_theme {
        
        position:absolute;
        top: 1%;
        left:1%;
    
    }

    
    
    .flotante {
            
        display:none;
        position:fixed;
        top:90%;
        right:2%;
        background-color: #<?php print $strColorHex?>;
        color: white;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
    
    }
    
    .slick-center-Border {
        
        padding: 0px !important;
        
    }
    
    
</style>


<script >                                 
           
    var d = new Date();
    var intDiaSemana = d.getDay()
   
        
    $(document).ready(function() {
              
        $('.slidePlace').slick({
            arrows: false,
            centerMode: true,
            infinite: true,
            centerPadding: '60px',
            slidesToShow: 4,
            speed: 200,
            autoplay: true,
            variableWidth: false,
            rtl:false,
            responsive: [ {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1,
                    autoplay: true,
                    speed: 200
                }
            }]
                
        });
        
        $('.slidePlace').on('afterChange', function(event, slick, currentSlide, nextSlide){
            
            var dataId = $('.slick-current').attr("data-slick-index");    
            dataId++;
            $(".imgSlideGallery").removeClass("slick-center-Border");
            $(".imgGalery_"+dataId).addClass("slick-center-Border");
        
        });
        
        $('.slidePlace').slick('slickGoTo',"0");
        
        var sinWindowsHeight = $(window).height();

        $('.imgSlideGallery ').each(function (){
            
            $(this).height( ( sinWindowsHeight * ( 30 / 100 ) )+'px');
            
        });
            
    });
     
</script> 
<?php
fntDrawFooterPublico($strColorHex); 
?>
