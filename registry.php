<?php
$strAction = basename($_SERVER["PHP_SELF"]);

if( isset($_GET["setRegistro"]) ){
    
    
    include "core/function_servicio.php";
    
    $strEmail = isset($_POST["txtEmail"]) ? fntCoreClearToQuery($_POST["txtEmail"]) : "";
    $strClave = isset($_POST["txtClave"]) ? (fntCoreClearToQuery($_POST["txtClave"])) : "";
    $strNombre = isset($_POST["txtNombre"]) ? fntCoreStringQueryLetra($_POST["txtNombre"]) : "";
    $strApellido = isset($_POST["txtApellido"]) ? fntCoreStringQueryLetra($_POST["txtApellido"]) : "";
    $strTelefono = isset($_POST["txtTelefono"]) ? fntCoreStringQueryLetra($_POST["txtTelefono"]) : "";
    
    $arr["error"] = "true";
    $arr["msn"] = "Datos incorrectos, complete todos los campos";
        
    if( !empty($strEmail) && !empty($strClave) && !empty($strNombre) && !empty($strTelefono) ){
        
        include "core/dbClass.php";                            
        $objDBClass = new dbClass();
        
        $strQuery = "SELECT id_usuario
                     FROM   usuario
                     WHERE  email = '{$strEmail}'  ";    
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        if( intval($rTMP["id_usuario"]) ){
            
            $arr["error"] = "true";
            $arr["msn"] = "Correo Electronico registrado anteriormente";
                    
        }
        else{
            
            $strClave = md5($strClave);
            $intIdUsuarioPlaceRestrinccion = 1;
            $strEstado = "A";
            $strLenguaje = "es";
            $intTipo = 2;  
            
            $strQuery = "INSERT INTO usuario(email, clave, nombre, apellido, 
                                             estado, tipo, add_fecha, lenguaje, 
                                             id_usuario_place_restrinccion, telefono)
                                      VALUES( '{$strEmail}', '{$strClave}', '{$strNombre}', '{$strApellido}',
                                              '{$strEstado}', '{$intTipo}', sysdate(), '{$strLenguaje}',
                                              {$intIdUsuarioPlaceRestrinccion}, '{$strTelefono}'  ) ";
            $objDBClass->db_consulta($strQuery);
            $intIdUsuario = $objDBClass->db_last_id();
            
            if( intval($intIdUsuario) ){
                
                session_start();
                
                $_SESSION["_open_antigua"]["core"]["login"] = true;
                $_SESSION["_open_antigua"]["core"]["id_usuario"] = $intIdUsuario;
                $_SESSION["_open_antigua"]["core"]["email"] = $strEmail;
                $_SESSION["_open_antigua"]["core"]["nombre"] = $strNombre;
                $_SESSION["_open_antigua"]["core"]["apellido"] = $strApellido;
                $_SESSION["_open_antigua"]["core"]["estado"] = $strEstado;
                $_SESSION["_open_antigua"]["core"]["tipo"] = $intTipo;
                $_SESSION["_open_antigua"]["core"]["lenguaje"] = $strLenguaje;
                
                $_SESSION["_open_antigua"]["core"]["pagina"] = "place";
                $_SESSION["_open_antigua"]["core"]["pagina_nombre"] =  "Place";
            
                $arr["error"] = "false";
                $arr["msn"] = "";
                $arr["href"] = "home.php";
                
            }
            else{
                
                $arr["error"] = "true";
                $arr["msn"] = "Error";
                
            }
            
        }
                        
    }
    
    print json_encode($arr);
    
    die();
}

?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="dist/images/favicon.ico">
            
        <!-- Material Design for Bootstrap fonts and icons -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

        <!-- Material Design for Bootstrap CSS -->
        <link rel="stylesheet" href="dist_interno/bootstrap-material-design-dist/css/bootstrap-material-design.css" >
        

        <title>OpenAntigua</title>
        <style>
            
            body, html {
                height: 100%;
            }

            .flex-grow {
                flex: 1 0 auto;
            }

            .bmd-layout-canvas {
              flex-grow: 1;
            }
            
            .preloader {
                opacity: 0.5;
                height: 100%;
                width: 100%;
                background: #FFF;
                position: fixed;
                top: 0;
                left: 0;
                z-index: 9999999;
            }
             
            .preloader .preloaderdetalle {
                position: absolute;
                top: 50%;
                left: 50%;
                -webkit-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                width: 120px;
            }
            
        </style>
        
        
        <script src="dist_interno/js/jquery-3.4.0.min.js" ></script>
        <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
        <script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>
        <script src="dist_interno/bootstrap-material-design-dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
        <script src="dist/js/jsAntigua.js" ></script>
        
    </head>
    <body id="bodyContenedor" >
        
        
        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            
            
            <main class="bmd-layout-content " >
                <div class="container-fluid  " >
                    
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-xs-12 mt-5">
                            
                            <form class="form-signin " onsubmit="return false" method="POST" id="frmRegistro">
                                <div class="text-center mb-4">
                                    <h1 class="h3 mb-3 font-weight-normal">Register</h1>
                                </div>

                                <div class="form-label-group">
                                    <input type="email" id="txtEmail" name="txtEmail" class="form-control" placeholder="Correo Electronico" required autofocus>
                                    
                                </div>

                                <div class="form-label-group">
                                    <input type="password" id="txtClave" name="txtClave" class="form-control" placeholder="Clave" required>
                                    
                                </div>
                                
                                <div class="form-label-group mt-2">
                                    <input type="text" id="txtNombre" name="txtNombre" class="form-control" placeholder="Nombre" required >
                                    
                                </div> 
                                <div class="form-label-group mt-2">
                                    <input type="text" id="txtApellido" name="txtApellido" class="form-control" placeholder="Apellido" required >
                                    
                                </div>  
                                <div class="form-label-group mt-2">
                                    <input type="text" id="txtTelefono" name="txtTelefono" class="form-control" placeholder="Telefono Contacto" required >
                                    
                                </div>  

                                <button class="btn btn-lg btn-raised btn-primary btn-block mt-4" onclick="fntSetRegistro();">Sign in</button>
                                <p class="mt-5 mb-3 text-muted text-center">&copy; 2019</p>
                            </form>                        
                        
                        </div>                    
                    </div>                    
                  
                </div> 
            </main>
            
            <div class="preloader">
                <div class="preloaderdetalle">
                    <img src="dist/images/30.gif" alt="NILA">
                </div>
            </div>
            <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
        
        <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
    
        <script>
            $(".preloader").fadeOut(); 
            
            function fntSetRegistro(){
                
                var formData = new FormData(document.getElementById("frmRegistro"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "registry.php?setRegistro=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        if( result["error"] == "true" ){
                            
                            swal({
                                title: "Error",
                                text: result["msn"],
                                type: "error",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            });
                                
                        }
                        else{
                            
                            location.href = result["href"];
                                                        
                        }
                        
                        
                    }
                            
                });
                
                return false;
                
            }      
            
        </script>
        
    </body>
</html>