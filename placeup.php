<?php
session_start();

include "core/function_servicio.php";

$intPlace = isset($_GET["p"]) ? intval(fntCoreDecrypt($_GET["p"])) : 0;
$intPlaceProducto = isset($_GET["pro"]) ? intval(fntCoreDecrypt($_GET["pro"])) : 0;
$boolVistaPublica = isset($_GET["publico"]) && $_GET["publico"] == "Y" ? true : false;

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

$boolUserPanelLogIn = false;

$arrInfoDispositivoi = fntGetInformacionClienteNavegacion();
    
if( $arrInfoDispositivoi["plataforma"] == "iPhone" )
    $boolMovil = true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";
define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

$_SESSION["boolUserPanelLogIn"] = isset($_SESSION["boolUserPanelLogIn"]) ? $_SESSION["boolUserPanelLogIn"] : false;

if( isset($_GET["setAutoCompletarInfoPlace"]) ){
    
    $strPlaceId = isset($_GET["place"]) ? $_GET["place"] : "";
    
    
    $strApiKey = fntGetApiKeyPlace();

    $strUrlApi = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$strPlaceId."&language=".$strLenguaje."&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,utc_offset,vicinity,formatted_phone_number,international_phone_number,opening_hours,website,price_level,rating,review,user_ratings_total&key=".$strApiKey;
            
    $objJsonDatos = file_get_contents($strUrlApi);
    $objJsonDatos = json_decode($objJsonDatos);
    //drawdebug($objJsonDatos);

    $arrFotos = isset($objJsonDatos->result->photos) ? $objJsonDatos->result->photos : array();
    $arrDatos = array();

    $arrDatos["nombre"] = isset($objJsonDatos->result->name) ? $objJsonDatos->result->name : "";
    $arrDatos["direccion"] = isset($objJsonDatos->result->formatted_address) ? addslashes($objJsonDatos->result->formatted_address) : "";
    $arrDatos["telefono"] = isset($objJsonDatos->result->international_phone_number) ? addslashes($objJsonDatos->result->international_phone_number) : "";
    $arrDatos["lat"] = isset($objJsonDatos->result->geometry->location->lat) ? $objJsonDatos->result->geometry->location->lat : "14.5585707";
    $arrDatos["lng"] = isset($objJsonDatos->result->geometry->location->lng) ? $objJsonDatos->result->geometry->location->lng : "-90.72952299999997";
    $arrDatos["horario"] = isset($objJsonDatos->result->opening_hours) ? $objJsonDatos->result->opening_hours : array();
    $arrDatos["rating"] = isset($objJsonDatos->result->rating) ? intval($objJsonDatos->result->rating) : 0;
                                  
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "UPDATE place
                 SET    direccion = '{$arrDatos["direccion"]}',
                        telefono = '{$arrDatos["telefono"]}',
                        ubicacion_lat = '{$arrDatos["lat"]}',
                        ubicacion_lng = '{$arrDatos["lng"]}',
                        place_pregunta = 'N'
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
    
    if( is_array($arrFotos) && count($arrFotos) > 0 ){
        
            
        while( $rTMP = each($arrFotos) ){
            
            $strLinkMap = "https://maps.googleapis.com/maps/api/place/photo?&key=".$strApiKey."&maxwidth=400&photoreference=";
        
            $strLinkMap .= $rTMP["value"]->photo_reference;
            
            $strQuery = "INSERT INTO place_galeria(id_place, url_imagen)
                                    VALUES({$intPlace}, '{$strLinkMap}')";
            $objDBClass->db_consulta($strQuery);
            
        }
        
    }
    die();
}

if( isset($_GET["fntDrawSeccionForm"]) ){
    
    include "core/dbClass.php";
    
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $intCantidadImagenes = 2;
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    
    
    if( $intSeccion == 1 ){
        
        $objDBClass = new dbClass();  
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrGetGaleriaPlace = fntGetPlaceGaleria($intPlace, true, $objDBClass);
        $intCount = 1;       
        $arrDatosPlace["num_galeria"] = 15;
        ?>          
        <div class="modal-dialog modal-lg" style="min-width: 95%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header" id="modalGeneralHeader">
                    
                    <h5 class="modal-title"><?php print lang["galeria"]?></h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body" id="modalGeneralBody">
                    <form name="frmGaleria" id="frmGaleria"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                    <input type="hidden" id="hidExcluirFile" name="hidExcluirFile" value="">
            
                    <style>
                        
                        img {
                          max-width: 100% !important; /* This rule is very important, please do not ignore this! */
                        }
                        
                    </style>       
                    <div class="row justify-content-start" id="divImagenes">
                          
                        <?php
                        
                        if( is_array($arrGetGaleriaPlace) && count($arrGetGaleriaPlace) > 0 ){
                            
                            while( $rTMP = each($arrGetGaleriaPlace) ){
                                ?>
                                <input type="hidden" value="<?php print $rTMP["key"]?>" id="hidIdGaleria_<?php print $intCount?>" name="hidIdGaleria_<?php print $intCount?>">
                                <input type="hidden" value="N" id="hidUpdate_<?php print $intCount?>" name="hidUpdate_<?php print $intCount?>">
                                <input type="hidden" value="N" id="hidDelete_<?php print $intCount?>" name="hidDelete_<?php print $intCount?>">
                                <input type="hidden" value="<?php print $rTMP["value"]["orden"]?>" id="hidOrdenGaleria_<?php print $intCount?>" name="hidOrdenGaleria_<?php print $intCount?>">
                                <input type="hidden" value="<?php print $intCount?>-0" id="hidTipoImagen_<?php print $intCount?>" name="hidTipoImagen_<?php print $intCount?>">
                                  
                                <div class="col-12 col-lg-3 p-4 p-lg-1" id="divContentImageSeccion1_<?php print $intCount;?>">
                                    
                                    <input type="file" id="flFileImage_<?php print $intCount;?>" name="flFileImage_<?php print $intCount;?>" multiple="" onchange="fntSetImgfile('<?php print $intCount;?>')" style="display: none;">
                                    <div class="card" style="width: 100%;">
                                        
                                        <div class="card-img-wasdfrap ">
                                               
                                            <img id="IMGflFileImage_<?php print $intCount;?>" class="card-iasdfmg-top iasdfmgZoom" src="imgInguate.php?t=up&src=<?php print $rTMP["value"]["url_imagen"]?>" style="width: 100% !important; height: auto !important; max-width: 100% !important">
                                            
                                            <input type="hidden" value="N" id="hidRotateIMGflFileImage_<?php print $intCount;?>" name="hidRotateIMGflFileImage_<?php print $intCount;?>"> 
                                        </div>                                
                                        
                                        <div class="p-1 text-right" style="position: absolute; top: 0px; right: 0px;">
                                            
                                            <button class="btn btn-light btn-sm" onclick="$('#flFileImage_<?php print $intCount;?>').click();">
                                                <i class="fa fa-pencil"></i>
                                            </button>                                                
                                        
                                            <button class="btn btn-light btn-sm" onclick="fntDeleteImageSeccion1('<?php print $intCount;?>');">
                                                <i class="fa fa-trash"></i>
                                            </button> 
                                            <button class="btn btn-light btn-sm" onclick="fntRotateSeccion1('<?php print $intCount;?>');">
                                                <i class="fa fa-rotate-right"></i>
                                            </button>                                                     
                                        
                                        </div>
                                    </div>
                                    
                                </div>
                                <?php
                                $intCount++;
                            }
                            
                        }
                        
                        ?>
                        
                                            
                        
                        <div class="col-12 col-lg-3 p-4 p-lg-1 align-middle" id="divAddMoreSeccion1">
                                   
                            <table style="height: 100%; width: 100%;">
                                <tbody>
                                    <tr>
                                        <td class="align-middle">

                                            <button class="btn btn-secondary btn-sm btn-block pt-5 pb-5" onclick="fntAddImageSeccion1(true);">
                                                <?php print lang["anadir_imagen"]?> <i class="fa fa-plus"></i>
                                            </button>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>                                       
                            
                        </div>            
                                   
                    
                    </div>            
                    </form>
                </div>
                
                <div class="modal-footer" id="modalGeneralFooter">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php print lang["cerrar"]?></button>
                    <button type="button" class="btn btn-success text-background-theme" onclick="fntSaveGaleria();"><?php print lang["guardar"]?></button>
                
                </div>
                
            </div>
        </div>  
        <link rel="stylesheet" href="dist/crooperImage/css/cropper.css" />
        <script src="dist/crooperImage/js/cropper.js"></script>
        
        <script> 
        
            var intCountImage = "<?php print $intCount;?>";       
            var input;
            var indexImg;
            var indexImgUso;
            var intCountFIle = 0;
            var cropper = new Array();
        
            $(document).ready(function() { 
                
                $("#modalFormContendio").height("80%");
                
                $('#divImagenes').imagesLoaded().always( function( instance ) {
                    
                    setTimeout(function(){
                        
                        $(":input[id^=hidRotateIMGflFileImage_]").each(function (){
                            
                            arrExplode = this.id.split("_");

                            var image = document.getElementById('IMGflFileImage_'+arrExplode[1]);
                                
                            cropper[arrExplode[1]] = new Cropper(image, {
                                autoCrop: false,
                                restore: false,
                                checkCrossOrigin: false,
                                checkOrientation: false,
                                modal: false,
                                guides: false,
                                highlight: false,
                                background: false,
                                autoCrop: false,
                                movable: false,
                                rotatable: true,
                                scalable: false,
                                zoomable: false,
                                zoomOnTouch: false,
                                zoomOnWheel: false,
                                cropBoxMovable: false,
                                cropBoxResizable: false,
                                toggleDragModeOnDblclick: false,
                                dragMode: 'none',
                              ready: function () {
                                //Should set crop box data first here
                              }
                            }); 
                            
                        });
                        
                    }, 1000);
                    
                });
                    
            }); 
            
            function fntSetImgfile(index){
                
                $('#hidUpdate_'+index).val("Y");
                var filename = $('#flFileImage_'+index).val();
                
                arr = filename.split(".");
                
                if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                    
                    swal("Error!", "<?php print lang["alerta_solo_imagenes"]?>", "error");
                    fntDeleteImageSeccion1(index);
                    return false;                    
                    
                }
                
                
                if ( ! window.FileReader ) {
                    return alert( 'FileReader API is not supported by your browser.' );
                }
                
                indexImg = index;   
                indexImgUso = index;   
                
                var $i = $( '#flFileImage_'+index ); // Put file input ID here
                var input = $i[0]; // Getting the element from jQuery
                
                setImgGaleriaMini(0, input);        
                
                intCountFIle = 0;
                        
            }
            
            function setImgGaleriaMini(ifile, input){
                
                if ( input.files && input.files[ifile] ) {
                        
                    file = input.files[ifile]; // The file
                        
                    fr = new FileReader(); // FileReader instance
                    
                    fr.onload = function () {
                                                  
                        var image = new Image();

                        image.onload = function() {      


                            $("#hidTipoImagen_"+indexImg).val(indexImgUso+"-"+ifile);
                            document.getElementById("IMGflFileImage_"+indexImg).src = fr.result;
                            
                            var image = document.getElementById('IMGflFileImage_'+indexImg);
                    
                            if( cropper[indexImg] ){
                                
                                cropper[indexImg].destroy();
                                
                            }
                                
                            cropper[indexImg] = new Cropper(image, {
                                autoCrop: false,
                                restore: false,
                                checkCrossOrigin: false,
                                checkOrientation: false,
                                modal: false,
                                guides: false,
                                highlight: false,
                                background: false,
                                autoCrop: false,
                                movable: false,
                                rotatable: true,
                                scalable: false,
                                zoomable: false,
                                zoomOnTouch: false,
                                zoomOnWheel: false,
                                cropBoxMovable: false,
                                cropBoxResizable: false,
                                toggleDragModeOnDblclick: false,
                                dragMode: 'none',
                                ready: function () {
                                //Should set crop box data first here
                                }
                            });
                                
                                    
                            intCountFIle++;
                            
                            if( intCountFIle < input.files.length ){
                                
                                fntAddImageSeccion1(false);
                                indexImg = intCountImage;
                                indexImg--;   
                
                                setImgGaleriaMini(intCountFIle, input);        
                                
                            }

                        }

                        // Load image.
                        image.src = fr.result;
                        
                    }; 
                    //fr.readAsText( file );
                    fr.readAsDataURL( file );
                    
                } 
                else {
                    // Handle errors here
                    alert( "File not selected or browser incompatible." )
                }
                
                
                
            }
            
            function fntAddImageSeccion1(boolClick){
                
                intCount = 0;
                $(":input[id^=flFileImage_]").each(function (){
                    
                    intCount++;                    
                    
                });
                
                if( intCount == "<?php print $arrDatosPlace["num_galeria"]?>" ){
                    
                    swal("Error!", "<?php print lang["alerta_maximo_imagenes_1"]?> <?php print $arrDatosPlace["num_galeria"]?> <?php print lang["alerta_maximo_imagenes_2"]?>", "error");
                    
                    return false;
                                        
                }                                     
                
                $("#divAddMoreSeccion1").before("<input type=\"hidden\" value=\"0\" id=\"hidIdGaleria_"+intCountImage+"\" name=\"hidIdGaleria_"+intCountImage+"\">    "+
                                                "<input type=\"hidden\" value=\"N\" id=\"hidDelete_"+intCountImage+"\" name=\"hidDelete_"+intCountImage+"\">       "+
                                                "<input type=\"hidden\" value=\"N\" id=\"hidUpdate_"+intCountImage+"\" name=\"hidUpdate_"+intCountImage+"\">       "+
                                                "<input type=\"hidden\" value=\""+intCountImage+"\" id=\"hidOrdenGaleria_"+intCountImage+"\" name=\"hidOrdenGaleria_"+intCountImage+"\">       "+
                                                "<input type=\"hidden\" value=\"\" id=\"hidTipoImagen_"+intCountImage+"\" name=\"hidTipoImagen_"+intCountImage+"\">       "+
                                                "<div class=\"col-12 col-lg-3\" id=\"divContentImageSeccion1_"+intCountImage+"\">           "+
                                                "    <input type=\"file\" id=\"flFileImage_"+intCountImage+"\" name=\"flFileImage_"+intCountImage+"[]\" multiple=\"\"  onchange=\"fntSetImgfile('"+intCountImage+"')\" style=\"display: none;\">   "+
                                                "    <div class=\"card\" style=\"width: 18rem;\">   "+
                                                "        <div class=\"card-img-wrap\">        "+
                                                "            <img id=\"IMGflFileImage_"+intCountImage+"\" class=\"card-img-top imgZoom\" src=\"images/Noimagee.jpg?1\" >    "+
                                                "            <input type=\"hidden\" value=\"N\" id=\"hidRotateIMGflFileImage_"+intCountImage+"\" name=\"hidRotateIMGflFileImage_"+intCountImage+"\">       "+
                                                "        </div>      "+                          
                                                "        <div class=\"p-1 text-right\" style=\"position: absolute; top: 0px; right: 0px;\">  "+
                                                "            <button class=\"btn btn-light btn-sm\" onclick=\"$('#flFileImage_"+intCountImage+"').click();\">    "+
                                                "                <i class=\"fa fa-pencil\"></i>     "+
                                                "            </button>  "+                                              
                                                "            <button class=\"btn btn-light btn-sm\" onclick=\"fntDeleteImageSeccion1('"+intCountImage+"');\">   "+
                                                "                <i class=\"fa fa-trash\"></i>    "+
                                                "            </button>     "+                                             
                                                "            <button class=\"btn btn-light btn-sm\" onclick=\"fntRotateSeccion1('"+intCountImage+"');\">   "+
                                                "                <i class=\"fa fa-rotate-right\"></i>   "+
                                                "            </button>     "+                                           
                                                "        </div>   "+
                                                "    </div>    "+
                                                "</div>");
                
                if( boolClick )
                    $("#flFileImage_"+intCountImage).click();
                
                intCountImage++;
                       
                
            }
            
            function fntRotateSeccion1(intIndex){
                
                //$("#IMGflFileImage_").rotate(90);
                cropper[intIndex].rotate(45);
                
                cropper[intIndex].getCroppedCanvas().toBlob( (blob) => {
                                                     
                    $("#hidRotateIMGflFileImage_"+intIndex).val("Y");
                    $("#hidUpdate_"+intIndex).val("Y");
                            
                    formDataFileRotate[intIndex] = blob;
                    
                });
                               
            }
            
            function fntDeleteImageSeccion1(intIndex){
                
                $("#hidExcluirFile").val( $("#hidExcluirFile").val() + ( $("#hidExcluirFile").val() == "" ? "" : "," ) + $("#hidTipoImagen_"+intIndex).val()  );
                $("#hidDelete_"+intIndex).val("Y");
                $("#divContentImageSeccion1_"+intIndex).remove();
                
            }
                
            var formDataFileRotate = new Array();
                
            function fntSaveGaleria(){
                
                var formDataPort = new FormData(document.getElementById("frmGaleria"));
                
                $(":input[id^=hidRotateIMGflFileImage_]").each(function (){
            
                    if( this.value == "Y" ){
                        
                        arrExplode = this.id.split("_");
                                                             
                        formDataPort.append('hidFileIMGflFileImage_'+$("#hidTipoImagen_"+arrExplode[1]).val(), formDataFileRotate[arrExplode[1]]);
                        formDataPort.append('hidFileIMGflFileImageOrden_'+$("#hidTipoImagen_"+arrExplode[1]).val(), $("#hidOrdenGaleria_"+arrExplode[1]).val());
                                
                    }
                    
                });
                
                $(".preloader").fadeIn();
                
                $.ajax({
                    url: "placeup.php?setGaleriaPlace=true", 
                    type: "POST",
                    data: formDataPort,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        setPaginaPublica();
                        $(".preloader").fadeOut();
                        fntDrawSeccion(1);
                        $("#modalGeneral").modal("hide");
                
                    }
                            
                });
                
            }
            
        </script>   
        
        <?php   
                
    }
    
    if( $intSeccion == 2 ){
        
        $objDBClass = new dbClass();  
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrPlaceHorario = fntGetPlaceHorario($intPlace, true, $objDBClass);
        
        $arrDiasSemana = getSemana();
        $arrHorario = getHoras24Horas();
        
        ?>
                           
        <div class="modal-dialog modal-lg" style="min-width: 60%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header p-2" id="modalGeneralHeader">
                    
                    <h5 class="modal-title"><?php print lang["datos_principales"]?></h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body pt-2" id="modalGeneralBody">
                    <form name="frmDatosPrincipales" id="frmDatosPrincipales"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                        
                        <div class="row">
                            <div class="col-12">
                                <label for="txtTitulo"><?php print lang["titulo"]?></label>
                                <input value="<?php print $arrDatosPlace["titulo"]?>" type="text" class="form-control form-control-sm" id="txtTitulo" name="txtTitulo" placeholder="">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-1">
                                <label for="txtDescripcion">Descripción español</label>
                                <label class="">
                                    <div id="divtoolbar" style="display: none;">
                                       
                                        <a class="badge badge-secondary" data-wysihtml-command="bold" title="CTRL+B" style="font-weight: 100 !important;">
                                            <i class="fa fa-bold" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="italic" title="CTRL+I" style="font-weight: 100 !important;">
                                            <i class="fa fa-italic" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="insertUnorderedList" style="font-weight: 100 !important;">
                                            <i class="fa fa-list-ul" style="font-weight: 100 !important;"></i>
                                        </a> 
                                        <a class="badge badge-secondary" data-wysihtml-command="insertOrderedList" style="font-weight: 100 !important;">
                                            <i class="fa fa-list-ol" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignLeftStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-left" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignCenterStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-center" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="alignRightStyle" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-right" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        <a class="badge badge-secondary" data-wysihtml-command="justifyFull" style="font-weight: 100 !important;">
                                            
                                            <i class="fa fa-align-justify" style="font-weight: 100 !important;"></i>
                                            
                                        </a> 
                                        
                                        
                                      </div>
                                </label>
                                <textarea class="form-control form-control-sm" id="txtDescripcion" name="txtDescripcion" placeholder="" rows="<?php print $boolMovil ? "10" : "6"?>"><?php print $arrDatosPlace["descripcion"]?></textarea>
                                        
                            </div>
                        </div>
                              
                        <div class="row">
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label for="txtTelefono"><?php print lang["telefono"]?></label>
                                <input value="<?php print $arrDatosPlace["telefono"]?>" type="tel" class="form-control form-control-sm" id="txtTelefono" name="txtTelefono" placeholder="">
                                
                            </div>
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label for="txtDireccion"><?php print lang["direccion"]?></label>
                                <input value="<?php print $arrDatosPlace["direccion"]?>" type="text" class="form-control form-control-sm" id="txtDireccion" name="txtDireccion" placeholder="">
                                
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label for="txtTelefono">Youtube</label>
                                <input value="<?php print $arrDatosPlace["url_youtube"]?>" type="text" class="form-control form-control-sm" id="txtUrlYoutube" name="txtUrlYoutube" placeholder="ej: https://www.youtube.com/watch?v=zwbK3sXv">
                                
                            </div>
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <div class="row">
                                    <div class="col-12 mt-2">
                                        <br>
                                        <div class="accordion" id="accFormHorario">
                                            <div class="card " style="border: 0px;">
                                                <div class="card-header p-0 m-0 bg-white" id="headingOne" data-toggle="collapse" data-target="#accFormHorarioOne" aria-expanded="true" aria-controls="accFormHorarioOne" style="cursor: pointer; border: 0px;">
                                                    
                                                    <div class="row">
                                                        <div class="col-12  text-center">
                                                            <?php print lang["horario"]?>
                                                            &nbsp;
                                                            &nbsp;
                                                            &nbsp;
                                                            &nbsp;
                                                            <i class="fa fa-angle-down text-color-theme"></i>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                </div>

                                                <div id="accFormHorarioOne" class="collapse " aria-labelledby="headingOne" data-parent="#accFormHorario">
                                                    <div class="card-body p-0 m-0 pt-2">
                                                        
                                                        <?php 
                                                        
                                                        while(  $rTMP = each($arrDiasSemana)){
                                                            
                                                            $arrInfo = isset($arrPlaceHorario[$rTMP["key"]]) ? $arrPlaceHorario[$rTMP["key"]] : array();
                                                            
                                                            ?>
                                                            
                                                            <input type="hidden" value="<?php print isset($arrInfo["id_place_horario"]) ? $arrInfo["id_place_horario"] : 0?>" name="hidPlaceHorario_<?php print $rTMP["key"]?>" id="hidPlaceHorario_<?php print $rTMP["key"]?>">
                                                            <input type="hidden" value="<?php print $rTMP["key"]?>" name="hidKeyDia_<?php print $rTMP["key"]?>" id="hidKeyDia_<?php print $rTMP["key"]?>">
                        
                                                            <div class="row justify-content-center">
                                                                <div class="col-3 p-0 p-lg-1 ">
                                                                
                                                                    <h6 class="text-right" ><?php print $rTMP["value"]["nombre"]?></h6>                                        
                                                                    
                                                                </div>
                                                                <div class="col-5 p-0 p-lg-1">
                                                                    
                                                                    <div class="input-group mb-3">
                                                                        
                                                                        <input readonly value="<?php print isset($arrInfo["hora_inicio"]) ? $arrInfo["hora_inicio"] : "00:00"?>" <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "disabled" : ""?> id="slcHorarioInicio_<?php print $rTMP["key"]?>" name="slcHorarioInicio_<?php print $rTMP["key"]?>" type="text" class="form-control form-control-sm text-center clockpicker" data-placement="left" data-align="top" data-autoclose="true" >

                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text p-0 m-0">&nbsp;-&nbsp;</span>
                                                                        </div>

                                                                        <input readonly value="<?php print isset($arrInfo["hora_fin"]) ? $arrInfo["hora_fin"] : "00:00"?>" <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "disabled" : ""?> id="slcHorarioFin_<?php print $rTMP["key"]?>" name="slcHorarioFin_<?php print $rTMP["key"]?>" type="text" class="form-control form-control-sm text-center clockpicker" data-placement="left" data-align="top" data-autoclose="true" >

                                                                    </div>
                                                                                                                
                                                                </div>
                                                                
                                                                <div class="col-3 p-0 p-lg-1 pl-1">
                                                                    <div class="custom-control custom-checkbox ">
                                                                        
                                                                        <input onchange="fntDisableDia('<?php print $rTMP["key"]?>');" class="custom-control-input" type="checkbox" id="chkClose_<?php print $rTMP["key"]?>" name="chkClose_<?php print $rTMP["key"]?>" <?php print isset($arrInfo["cerrado"]) && $arrInfo["cerrado"] == "Y" ? "checked" : ""?>>
                                                                        <label class="custom-control-label" for="chkClose_<?php print $rTMP["key"]?>" >
                                                                            <?php print lang["cerrado"]?>
                                                                        </label>
                                                                    </div>  
                                                                    
                                                                             
                                                                                                          
                                                                </div>
                                                            </div>
                                                            
                                                            <?php
                                                        }
                                                        
                                                        ?>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                                   
                        
                        
                                    
                    </form>
                </div>
                
                <div class="modal-footer" id="modalGeneralFooter">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php print lang["cerrar"]?></button>
                    <button type="button" class="btn btn-success text-background-theme" onclick="fntSaveDatosPrincipales();"><?php print lang["guardar"]?></button>
                
                </div>
            </div>
        </div>
        
        <script src="dist/nueva_textarea/dist/wysihtml.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.all-commands.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.table_editing.js"></script>
        <script src="dist/nueva_textarea/dist/wysihtml.toolbar.js"></script>

        <script src="dist/nueva_textarea/parser_rules/advanced_and_extended.js"></script>

        <script>
            
            $(document).ready(function() { 
                
                $('.clockpicker').clockpicker({
                    placement: 'top',
                    align: 'left',
                    donetext: 'Done'
                });
                    
                var editor = new wysihtml.Editor("txtDescripcion", {
                    toolbar:      "divtoolbar",
                    parserRules:  wysihtmlParserRules,
                    pasteParserRulesets: wysihtmlParserPasteRulesets
                  });
                
                    
            });
            
            function fntDisableDia(index){
                
                check = document.getElementById("chkClose_"+index).checked;
                document.getElementById("slcHorarioFin_"+index).disabled = check;                
                document.getElementById("slcHorarioInicio_"+index).disabled = check;                
                
            }
            
            function fntSaveDatosPrincipales(){
                
                var formData = new FormData(document.getElementById("frmDatosPrincipales"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "placeup.php?setSaveDatosPrincipales=true", 
                    type: "POST",
                    data: formData,
                    dataType : "json",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                            
                        if( result["error"] ){
                            
                            swal("Error!", result["msn"], "error");
                            
                        }
                        else{
                            
                            setPaginaPublica();
                            fntDrawSeccion(2);
                            
                            $("#modalGeneral").modal("hide");
                                
                        }
                        
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
    
    if( $intSeccion == 3 ){
        
        $objDBClass = new dbClass();  
        
        $arrPlaceAmenidad = fntGetPlaceAmenidad($intPlace, true, $objDBClass);
        $arrAmenidades = fntGetAmenidades(true, $objDBClass);
        
        ?>
        <div class="modal-dialog modal-lg" style="min-width: 70%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header" id="modalGeneralHeader">
                    
                    <h5 class="modal-title"><?php print lang["amenidades"]?></h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body" id="modalGeneralBody">
                    <form name="frmAmenidades" id="frmAmenidades"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                        
                        <div class="row">
                            <div class="col-12 p-4">
                                
                                <table class="table table-responsive-sm " id="tblAmenidades" style="width: 100%; ">
                                    <thead style="display: none;"> 
                                        <tr>    
                                            <th>Amenities</th>
                                            <th>Amenities</th>
                                            <th>Amenities</th>
                                            <th>Amenities</th>
                                        </tr>
                                    </thead> 
                                    <tbody>    
                                    <?php
                                    
                                    $intCount = 1;
                                    $intCountGrupo = 1;
                                                              
                                    while( $rTMP = each($arrAmenidades) ){
                                        
                                        $strSelected = isset($arrPlaceAmenidad[$rTMP["key"]]) ? "checked" : "";
                                        
                                        if( $intCountGrupo == 1 ){
                                            ?>
                                            <tr>
                                            <?php    
                                        }
                                        ?>
                                        
                                            <td  nowrap>
                                                
                                                
                                                <div class="custom-control custom-checkbox ">
                                                    
                                                    <input  class="custom-control-input" type="checkbox" name="chkAmenidad_<?php print $rTMP["key"]?>"  id="chkAmenidad_<?php print $rTMP["key"]?>" <?php print $strSelected?>>
                                                    <label class="custom-control-label" for="chkAmenidad_<?php print $rTMP["key"]?>" >
                                                        <?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?>
                                                    </label>
                                                </div>
                                                
                                            </td>
                                        
                                        <?php
                                        
                                        if( $intCountGrupo == 4 ){
                                            ?>
                                            </tr>
                                            <?php
                                            $intCountGrupo = 0;
                                        }
                                        
                                        $intCountGrupo++;
                                        
                                        
                                    }
                                    
                                    if( $intCountGrupo >= 1 && $intCountGrupo < 4 ){
                                        
                                        for( $i = $intCountGrupo ; $i <= 4; $i++ ){
                                            
                                            ?>
                                            <td>&nbsp;</td>
                                        
                                            <?php
                                        }
                                        
                                        ?>
                                        </tr>
                                        <?php
                                    }
                                    
                                    ?>
                                        
                                    </tbody>                            
                                </table>
                                
                            </div>                        
                        </div>                        
                                        
                    </form>
                </div>
                
                <div class="modal-footer" id="modalGeneralFooter">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php print lang["cerrar"]?></button>
                    <button type="button" class="btn btn-success text-background-theme" onclick="fntSaveAmenidad();"><?php print lang["guardar"]?></button>
                
                </div>
            </div>
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $('#tblAmenidades').DataTable({
                    "ordering": false
                });    
                    
                    
            });
                
            function fntSaveAmenidad(){
                
                var formData = new FormData(document.getElementById("frmAmenidades"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "placeup.php?setPlaceAmenidad=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        fntDrawSeccion(3);
                        $("#modalGeneral").modal("hide");
                
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
        
    if( $intSeccion == 4 ){
        
        $objDBClass = new dbClass();  
        
        $arrCatalogoFondo = fntCatalogoFondoPlacePublico();
        
        ?>
        <div class="modal-dialog modal-lg" style="min-width: 70%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                ?>
                <div class="modal-header" id="modalGeneralHeader">
                    
                    <h5 class="modal-title">Fondo</h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                
                <div class="modal-body" id="modalGeneralBody">
                    <form name="frmFondo" id="frmFondo"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                    
                    <?php
                    
                    while( $rTMP = each($arrCatalogoFondo) ){
                        ?>
                        <div class="row justify-content-center">
                            <div class="col-lg-2 col-4 p-0 m-0 mt-3 text-right border-bottom">
                                        
                                <img style="width: 100%; height: auto;" src="<?php print $rTMP["value"]["url_fondo"]?>" class="img-fluid rounded">
                                
                                
                            </div>
                            <div class="col-lg-6 col-8 p-0 m-0 pl-3 mt-3 border-bottom pb-1">
                                
                                <div class="row">
                                    <div class="col-6">
                                        
                                        <h5 class="text-dark text-left lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                            <?php print $rTMP["value"]["nombre"]?>
                                        </h5>
                                    
                                    </div>
                                    <div class="col-6 ">
                                        
                                        <button class="btn btn-secondary" onclick="fntSetFondoPlace('<?php print $rTMP["key"]?>');">Select</button>
                                    
                                    </div>
                                </div>
                            
                            
                            </div>
                        </div>
                        <?php
                    }
                    
                    ?>
                            
                        
                    </form>
                </div>
                  
            </div>
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $('#tblAmenidades').DataTable({
                    "ordering": false
                });    
                    
                    
            });
            
            function fntSetFondoPlace(intKeyCatalogo){
                
                var formData = new FormData();
                formData.append("intFondoPlace", intKeyCatalogo);                    
                formData.append("hidPlace", "<?php print fntCoreEncrypt($intPlace)?>");
                                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "placeup.php?setFondoPlace=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        location.href = "placeup.php?p=<?php print fntCoreEncrypt($intPlace)?>&len=<?php $strLenguaje?>";
                        
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
        
    if( $intSeccion == 5 ){
        
        $objDBClass = new dbClass();  
        
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrCatalogoTipoPlace = fntCatalogoTipoPlace();
        
        $strQuery = "SELECT sol_place.id_sol_place,
                            sol_place.nombre,
                            sol_place.telefono_propietario,
                            sol_place.email_propietario,
                            sol_place.tipo_negocio,
                            sol_place.id_usuario_vendedor,
                            sol_place.estado,
                            usuario_vendedor.codigo,
                            sol_place_motivo.motivo
                     FROM   sol_place
                                LEFT JOIN   sol_place_motivo
                                    ON  sol_place.id_sol_place = sol_place_motivo.id_sol_place
                                LEFT JOIN   usuario_vendedor 
                                    ON    usuario_vendedor.id_usuario = sol_place.id_usuario_vendedor
                     WHERE  sol_place.id_place = {$intPlace}
                     ";
                     
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMPSolPlace = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        
        ?>
                           
        <div class="modal-dialog modal-lg" style="min-width: 60%;" role="document">
            <div class="modal-content" id="modalGeneraContenido">
              
                <?php
                
                ?>
                <div class="modal-header p-2" id="modalGeneralHeader">
                    
                    <h5 class="modal-title"><?php print lang["solicitud"]?></h5>
                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    
                </div>
                                                    
                <div class="modal-body pt-2" id="modalGeneralBody">
                    <form name="frmDatosSolicitudEnvio" id="frmDatosSolicitudEnvio"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                    <input type="hidden" id="hidSolPlace" name="hidSolPlace" value="<?php print fntCoreEncrypt($rTMPSolPlace["id_sol_place"])?>">
                    <input type="hidden" id="hidNombreNegocio" name="hidNombreNegocio" value="<?php print $arrDatosPlace["titulo"]?>">
                        
                        <?php
                        
                        if( isset($rTMPSolPlace["estado"]) && $rTMPSolPlace["estado"] == "R" ){
                            
                            ?>
                            <div class="row">
                                <div class="col-12 mt-1 text-danger text-center">
                                    
                                    
                                    <?php
                                    
                                    print lang["motivo_rechazo"];
                                    
                                    print isset($rTMPSolPlace["motivo"]) ? $rTMPSolPlace["motivo"] : "";
                                    
                                    ?>                                    
                                                                    
                                </div>
                            </div>
                            <?php
                            
                        }
                        
                        ?>     
                                
                        <div class="row">
                            <div class="col-12 mt-1">
                                
                                <label for="txtCodigoVendedor"><?php print lang["codigo_vendedor"]?></label>   
                                <input value="<?php print isset($rTMPSolPlace["codigo"]) ? $rTMPSolPlace["codigo"] : ""?>" onchange="fntBuscarCodigoVendedor();" type="tel" class="form-control form-control-sm" id="txtCodigoVendedor" name="txtCodigoVendedor" placeholder="">
                                <label id="lbspEstadoCodigo" style="display: none;">
                                    <span id="spEstadoCodigo" class="" style="font-size: 12px;"></span>
                                </label>
                                                                
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-12 mt-1">
                                
                                <label id="lbtxtNombrePropietario"><?php print lang["nombre_completo_propietario"]?></label>
                                <input value="<?php print isset($rTMPSolPlace["nombre"]) ? $rTMPSolPlace["nombre"] : ""?>" type="tel" class="form-control form-control-sm" id="txtNombrePropietario" name="txtNombrePropietario" placeholder="">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label id="lbtxtTelefonoPropietario"><?php print lang["telefono_propietario"]?></label>
                                <input value="<?php print isset($rTMPSolPlace["telefono_propietario"]) ? $rTMPSolPlace["telefono_propietario"] : ""?>" type="tel" class="form-control form-control-sm" id="txtTelefonoPropietario" name="txtTelefonoPropietario" placeholder="">
                                
                            </div>
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label id="lbtxtEmailPropietario"><?php print lang["email_propietario"]?></label>
                                <input value="<?php print isset($rTMPSolPlace["email_propietario"]) ? $rTMPSolPlace["email_propietario"] : ""?>" type="text" class="form-control form-control-sm" id="txtEmailPropietario" name="txtEmailPropietario" placeholder="">
                                
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label id="lbslcTipoNegocio"><?php print lang["tipo_de_negocio"]?></label>
                                <select class="form-control form-control-sm" id="slcTipoNegocio" name="slcTipoNegocio">
                                    
                                    <?php
                                    
                                    while( $rTMP = each($arrCatalogoTipoPlace) ){
                                        
                                        $strSelected = $rTMP["key"] == (  isset($rTMPSolPlace["tipo_negocio"]) ? $rTMPSolPlace["tipo_negocio"] : "" ) ? "selected" : "";
                                        
                                        ?>
                                        
                                        <option <?php print $strSelected;?> value="<?php print $rTMP["key"]?>"><?php print  sesion["lenguaje"] == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"] ?></option>
                                        
                                        <?php
                                    }
                                    
                                    ?>    
                                    
                                </select>
                            </div>
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label id="lbflDPI">DPI</label>
                                <br>
                                <input type="file" id="flDPI" name="flDPI" style="cursor: pointer;">
                            </div>
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label id="lbflPatente"><?php print lang["foto_patente"]?></label>
                                <br>
                                <input type="file" id="flPatente" name="flPatente" style="cursor: pointer;">
                            </div>
                            <div class="col-lg-6 col-12 mt-1">
                                
                                <label id="lbflSA"><?php print lang["foto_sa"]?></label>
                                <br>
                                <input type="file" id="flSA" name="flSA" style="cursor: pointer;">
                            </div>
                            
                            
                        </div>
                        
                    </form>
                </div>
                
                <div class="modal-footer" id="modalGeneralFooter">
                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php print lang["cerrar"]?></button>
                    <button type="button" class="btn btn-success text-background-theme" onclick="fntSaveEnviosSoliticut();"><?php print lang["enviar_solicitud"]?></button>
                
                </div>
            </div>
        </div>
        

        <script>
            
            var boolVendedor = false;
            $(document).ready(function() { 
                
                <?php 
                
                if( isset($rTMPSolPlace["codigo"]) && !empty($rTMPSolPlace["codigo"]) ){
                    
                    ?>
                    fntBuscarCodigoVendedor();
                    <?php
                    
                }
                
                ?>    
                    
            });
            
            function fntBuscarCodigoVendedor(){
                
                $.ajax({
                    url: "placeup.php?getCodigoVendedor=true&codigo="+$("#txtCodigoVendedor").val(), 
                    dataType : "json",
                    success: function(result){
                        
                        $("#lbspEstadoCodigo").show();
                        $("#spEstadoCodigo").html(result["texto"]);
                        boolVendedor = result["existe"] == "Y" ? true : false;
                        
                        if( result["existe"] == "Y" ){
                            
                            $("#spEstadoCodigo").addClass("text-success");
                            $("#spEstadoCodigo").removeClass("text-warning");
                                
                        }
                        else{
                            
                            $("#spEstadoCodigo").removeClass("text-success");
                            $("#spEstadoCodigo").addClass("text-warning");
                            
                        }
                        
                    }
                });
                
            }
            
            function fntSaveEnviosSoliticut(){
                
                
                var boolError = false;
                    
                if( $("#txtNombrePropietario").val() == "" ){
                    
                    $("#lbtxtNombrePropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtNombrePropietario").removeClass("text-danger");
                    
                }
                
                
                if( $("#txtTelefonoPropietario").val() == "" ){
                    
                    $("#lbtxtTelefonoPropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtTelefonoPropietario").removeClass("text-danger");
                    
                }
                
                if( $("#txtEmailPropietario").val() == "" ){
                    
                    $("#lbtxtEmailPropietario").addClass("text-danger");
                        
                    boolError = true;
                }
                else{
                    
                    $("#lbtxtEmailPropietario").removeClass("text-danger");
                    
                }
                
                if( !boolVendedor ){
                    
                    var intSizeFile = $("#flDPI")[0].files.length;
                    if( intSizeFile === 0){
                                   
                        $("#lbflDPI").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbflDPI").removeClass("text-danger");
                            
                    }
                    
                    var intSizeFile = $("#flPatente")[0].files.length;
                    if( intSizeFile === 0){
                                   
                        $("#lbflPatente").addClass("text-danger");
                            
                        boolError = true;
                    }
                    else{
                        
                        $("#lbflPatente").removeClass("text-danger");
                            
                    }
                    
                    if( $("#slcTipoNegocio").val() == 2 ){
                        
                        var intSizeFile = $("#flSA")[0].files.length;
                        if( intSizeFile === 0){
                                       
                            $("#lbflSA").addClass("text-danger");
                                
                            boolError = true;
                        }
                        else{
                            
                            $("#lbflSA").removeClass("text-danger");
                                
                        }
                        
                    }
                    
                    
                }
                
                    
                if( boolError ){
                        
                    swal("<?php print lang["advertencia"]?>", "<?php print lang["completa_los_datos_obligatorios"]?>", "");
                    
                    return false;

                }
                
                var formData = new FormData(document.getElementById("frmDatosSolicitudEnvio"));
                    
                $(".preloader").fadeIn();
                $.ajax({
                    url: "placeup.php?setEnvioSolicitud=true", 
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(result){
                        
                        $(".preloader").fadeOut();
                        
                        swal({   
                            title: "<?php print lang["listo"]?>",   
                            text: "<?php print lang["aviso_envio_sol_place"]?>",
                            type: "success",   
                            showConfirmButton: true 
                        },function(isConfirm){   
                            
                            location.reload();
                        
                        });
                
                    }
                            
                });
                
            }
            
        </script>
        <?php
        
    }
                      
    die();
}

if( isset($_GET["drawProductoEspecial"]) ){
    
    $intPlaceProducto = isset($_GET["key"]) ? $_GET["key"] : 0;
    $instrProductoCla = isset($_GET["strProductoCla"]) ? $_GET["strProductoCla"] : 0;
    $strHeight = isset($_GET["strHeightPx"]) ? $_GET["strHeightPx"] : 0;
    
    ?>
    <div class="modal-dialog modal-lg m-0" style="min-width: 100%;" role="document">
        <div class="modal-content" id="modalGeneraContenido">
          
            <?php 
            ?>
            <div class="modal-header p-3" id="modalGeneralHeader">
                
                <button type="button" class="btn btn-secondary btn-sm text-background-theme text-white" data-dismiss="modal" onclick="fntDrawSeccionProductoClasificacionPadre('<?php print ($instrProductoCla)?>')"><?php print lang["regresar"]?></button>
                &nbsp;
                &nbsp;
                <h5 class="modal-title"><?php print lang["producto_especial"]?></h5>
                
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
            </div>
            
            <div class="modal-body pt-2 text-right" id="modalGeneralBody">
                
                <iframe src="proes.php?p=<?php print $intPlaceProducto?>" style="width: 100%; height: <?php print $strHeight?>; border: 0px;"></iframe>
                <button type="button" class="btn btn-secondary text-background-theme text-white" data-dismiss="modal" onclick="fntDrawSeccionProductoClasificacionPadre('<?php print ($instrProductoCla)?>')"><?php print lang["regresar"]?></button>
                            
            </div>
            
        </div>
    </div>
    <script>
        
        $(document).ready(function() { 
            
                
        });
        
    </script>
    <?php
    
    die();
}

if( isset($_GET["setEnvioSolicitud"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $strCodigoVendedor = isset($_POST["txtCodigoVendedor"]) ? addslashes($_POST["txtCodigoVendedor"]) : "";
    $strNombrePropietario = isset($_POST["txtNombrePropietario"]) ? addslashes($_POST["txtNombrePropietario"]) : "";
    $strTelefonoPropietario = isset($_POST["txtTelefonoPropietario"]) ? addslashes($_POST["txtTelefonoPropietario"]) : "";
    $strEmailPropietario = isset($_POST["txtEmailPropietario"]) ? addslashes($_POST["txtEmailPropietario"]) : "";
    $intTipoNegocio = isset($_POST["slcTipoNegocio"]) ? intval($_POST["slcTipoNegocio"]) : "";
    $strNombreNegocio = isset($_POST["hidNombreNegocio"]) ? trim($_POST["hidNombreNegocio"]) : "";
    
    if( $intPlace ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
        
        $strQuery = "SELECT usuario.id_usuario,
                            usuario.nombre
                     FROM   usuario_vendedor,
                            usuario
                     WHERE  usuario_vendedor.codigo = '{$strCodigoVendedor}'
                     AND    usuario_vendedor.activo = 'A'
                     AND    usuario_vendedor.id_usuario = usuario.id_usuario
                      ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        $intIdUsuarioVendedor = isset($rTMP["id_usuario"]) ? intval($rTMP["id_usuario"]) : 0;
        
        
        $strQuery = "UPDATE sol_place
                     SET    nombre = '{$strNombrePropietario}',
                            telefono_propietario = '{$strTelefonoPropietario}',
                            email_propietario = '{$strEmailPropietario}',
                            tipo_negocio = '{$intTipoNegocio}',
                            id_usuario_vendedor = '{$intIdUsuarioVendedor}',
                            estado = 'S'
                     WHERE  id_place = {$intPlace} ";
                     
        $objDBClass->db_consulta($strQuery);
        
        $strQuery = "UPDATE place
                     SET    estado = 'S'
                     WHERE  id_place = {$intPlace} ";
                     
        $objDBClass->db_consulta($strQuery);
        
        fntEnviarCorreo("info@inguate.com", "Solicitud de Negocio INGUATE", "Solicitud de Negocio: {$strNombreNegocio}");
        
        if( isset($_FILES["flPatente"]) && $_FILES["flPatente"]["size"] > 0 ){
                
            $arrNameFile = explode(".", $_FILES["flPatente"]["name"]); 
            $strUrlArchivo = "../../file_inguate/place/PC_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
            rename($_FILES["flPatente"]["tmp_name"], $strUrlArchivo);
            
            $strQuery = "UPDATE sol_place
                         SET    url_patente = '{$strUrlArchivo}'
                         WHERE  id_place = {$intPlace} ";
                         
            $objDBClass->db_consulta($strQuery);
            
                    
        }

        if( isset($_FILES["flDPI"]) && $_FILES["flDPI"]["size"] > 0 ){
                
            $arrNameFile = explode(".", $_FILES["flDPI"]["name"]); 
            $strUrlArchivo = "../../file_inguate/place/PCdpi_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
            rename($_FILES["flDPI"]["tmp_name"], $strUrlArchivo);
            
            $strQuery = "UPDATE sol_place
                         SET    url_dpi = '{$strUrlArchivo}'
                         WHERE  id_place = {$intPlace} ";
                         
            $objDBClass->db_consulta($strQuery);
                    
        }

        if( isset($_FILES["flSA"]) && $_FILES["flSA"]["size"] > 0 ){
                
            $arrNameFile = explode(".", $_FILES["flSA"]["name"]); 
            $strUrlArchivo = "../../file_inguate/place/PCsa_".fntCoreEncrypt($intPlace).".".$arrNameFile[1];
            rename($_FILES["flSA"]["tmp_name"], $strUrlArchivo);
            
            $strQuery = "UPDATE sol_place
                         SET    url_sa = '{$strUrlArchivo}'
                         WHERE  id_place = {$intPlace} ";
                         
            $objDBClass->db_consulta($strQuery);
                    
        }
        
    }
        
    die();
}

if( isset($_GET["getCodigoVendedor"]) ){
    
    $strCodigo = isset($_GET["codigo"]) ? fntCoreClearToQuery($_GET["codigo"]) : "";
    
    $arr["existe"] = "N";
    $arr["texto"] = lang["codigo_venta_no_valido"];
    
    if( !empty($strCodigo) ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
    
        $strQuery = "SELECT usuario.id_usuario,
                            usuario.nombre
                     FROM   usuario_vendedor,
                            usuario
                     WHERE  usuario_vendedor.codigo = '{$strCodigo}'
                     AND    usuario_vendedor.activo = 'A'
                     AND    usuario_vendedor.id_usuario = usuario.id_usuario
                      ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        if( isset($rTMP["id_usuario"]) && intval($rTMP["id_usuario"]) ){
            
            $arr["existe"] = "Y";
            $arr["texto"] = lang["asesor_de_ventas"]." ".$rTMP["nombre"];
            
        }
    
        $objDBClass->db_close();
            
    }
    
    print json_encode($arr);
            
    die();
}

if( isset($_GET["setFondoPlace"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceCatalogo = isset($_POST["intFondoPlace"]) ? intval(($_POST["intFondoPlace"])) : 0;
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "UPDATE place
                 SET    id_tema = {$intPlaceCatalogo}
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
    $objDBClass->db_close();
    
    die();
}

if( isset($_GET["getShowModalProductoClasificacionPadre"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intClasificacionPadre = isset($_GET["key"]) ? intval(($_GET["key"])) : 0;
    $intPlaceProducto = isset($_GET["strKey2"]) ? intval(($_GET["strKey2"])) : 0;
    
    $arrClasificacionPadreHijo = fntGetPlaceClasificacionPadreHijo($intPlace, $intClasificacionPadre, true, $objDBClass);
    
    $arrPlaceProducto = array();
    
    if( $intPlaceProducto ) 
        $arrPlaceProducto = fntGetPlaceProducto($intPlace, $intClasificacionPadre, true, $objDBClass, $intPlaceProducto);
    
    $intCount = 1;
    $strHeightPX = "height: 130px;";
    
    
    
    ?>
    <div class="modal-dialog modal-lg" style="min-width: 85%;" role="document">
        <div class="modal-content" id="modalGeneraContenido">
          
            <?php
            ?>
            <div class="modal-header pb-1 pt-1" id="modalGeneralHeader">
                
                <h5 class="modal-title"><?php print lang["productos"]?></h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
            </div>
            
            <div class="modal-body  p-1 p-lg-2" id="modalGeneralBody">
                <style>
                        
                    img {
                      max-width: 100% !important; /* This rule is very important, please do not ignore this! */
                    }
                    
                </style>
                <form name="frmProducto" id="frmProducto"  onsubmit="return false;" method="POST">
                <input type="hidden" id="hidPlace" name="hidPlace" value="<?php print fntCoreEncrypt($intPlace)?>">
                <input type="hidden" id="hidClasificacionPadre" name="hidClasificacionPadre" value="<?php print fntCoreEncrypt($intClasificacionPadre)?>">
                <?php
                
                if( count($arrPlaceProducto) > 0 ){
                    
                    while( $rTMP = each($arrPlaceProducto) ){
                        
                        ?>
                        
                        <input type="hidden" id="hidIdPlaceProducto_<?php print $intCount;?>" name="hidIdPlaceProducto_<?php print $intCount;?>" value="<?php print $rTMP["key"]?>">
                        <input type="hidden" id="hidProductoUpdate_<?php print $intCount;?>" name="hidProductoUpdate_<?php print $intCount;?>" value="N">
                        <input type="hidden" id="hidProductoDelete_<?php print $intCount;?>" name="hidProductoDelete_<?php print $intCount;?>" value="N">
                        <input type="hidden" id="hidProductoFotoRotar_<?php print $intCount;?>" name="hidProductoFotoRotar_<?php print $intCount;?>" value="N">
                        
                        <div class="row p-0 m-0 justify-content-center" id="divContenidoProducto_<?php print $intCount;?>">
                            <div class="col-lg-3 col-8 p-0 m-0 mt-3 text-right ">
                                
                                <?php
                                
                                if( $rTMP["value"]["tipo"] == 2 ){
                                    
                                    if( isset($rTMP["value"]["galeria"]) ){
                                        
                                        while( $arrTMP = each($rTMP["value"]["galeria"]) ){
                                            ?>
                                            <img id="imgProducto_<?php print $intCount;?>" style="width: <?php print $boolMovil ? "200px" : "150px"?>; height: <?php print $boolMovil ? "200px" : "150px"?>; object-fit: contain;" src="imgInguate.php?t=up&src=<?php print $arrTMP["value"]["url_imagen"]?>" class="img-fluid rounded">
                                        
                                            <?php
                                            break;
                                        }
                                        
                                    }
                                        
                                            
                                }
                                else{
                                    ?>
                                    <input type="file" id="flImgProducto_<?php print $intCount;?>" name="flImgProducto_<?php print $intCount;?>" onchange="fntSetLogoPreview('<?php print $intCount;?>');" style="display: none;">
                                              
                                    <img id="imgProducto_<?php print $intCount;?>" style="width: <?php print $boolMovil ? "200px" : "150px"?>; height: <?php print $boolMovil ? "200px" : "150px"?>; object-fit: contain;" src="<?php print !empty($rTMP["value"]["logo_url"]) ? "imgInguate.php?t=up&src=".$rTMP["value"]["logo_url"] : "images/Noimagee.jpg?1"?>" class="img-fluid rounded">
                                                
                                    <div id="btnImgProducto_<?php print $intCount;?>" class="btn-theme-circular" onclick="$('#flImgProducto_<?php print $intCount;?>').click();" style="padding: 2px 3px 2px 3px; font-size: 14px; right:<?php print $boolMovil ? "33%" : "35%"?> !important; display: none;">
                                        &nbsp;<i class="fa fa-pencil"></i>&nbsp;
                                    </div>                                                       
                                    <div id="btnImgProductoRotate_<?php print $intCount;?>" class="btn-theme-circular" onclick="fntRotateSeccion1('<?php print $intCount;?>');" style="padding: 2px 3px 2px 3px; font-size: 14px; right:<?php print $boolMovil ? "52%" : "50%"?> !important; display: none;">
                                        &nbsp;<i id="ibtnImgProductoRotate_<?php print $intCount;?>" class="fa fa-rotate-right"></i>&nbsp;
                                    </div>
                                    <?php
                                }
                                
                                ?>
                                
                                     
                                
                            </div>
                            <div class="col-lg-9 col-12 p-0 m-0  mt-3  pb-1">
                                                  
                                <div id="divVistaEdicion_<?php print $intCount?>" style="display: none;">
                                    <input value="<?php print $rTMP["value"]["nombre"];?>" type="text" class="form-control form-control-sm" id="txtNombre_<?php print $intCount;?>" name="txtNombre_<?php print $intCount;?>" placeholder="Nombre en español">
                                    
                                    <textarea class="form-control form-control-sm mt-1" id="txtDescripcion_<?php print $intCount;?>" name="txtDescripcion_<?php print $intCount;?>" rows="2" placeholder="Descripción en español"><?php print $rTMP["value"]["descripcion"];?></textarea>
              
                                    <button class="btn btn-secondary btn-sm text-background-theme mt-1" onclick="fntTraducirNombreDescripcion('<?php print $intCount;?>');" > 
                                        Traducir / Translate
                                    </button>
                                    
                                    <input value="<?php print $rTMP["value"]["nombre_en"];?>" type="text" class="form-control form-control-sm mt-1" id="txtNombreEN_<?php print $intCount;?>" name="txtNombreEN_<?php print $intCount;?>" placeholder="Name in english">
                                    
                                    <textarea class="form-control form-control-sm mt-1" id="txtDescripcionEN_<?php print $intCount;?>" name="txtDescripcionEN_<?php print $intCount;?>" rows="2" placeholder="Description in english"><?php print $rTMP["value"]["descripcion_en"];?></textarea>
              
                                    
                                    <input value="<?php print $rTMP["value"]["precio"];?>" type="text" class="form-control form-control-sm mt-1 mb-1 col-12 col-lg-4 text-right" id="txtPrecio_<?php print $intCount;?>" name="txtPrecio_<?php print $intCount;?>" placeholder="<?php print lang["precio"]?> Q." onchange="fntMantenerDecimales(this)">
                                    
                                    <div class="row">
                                        <div class="col-12 col-lg-8">
                                            <label for="slcClasificacionPadreProducto_<?php print $intCount;?>"><small><?php print lang["clasificacion"]?></small></label>
                                            <select  id="slcClasificacionPadreProducto_<?php print $intCount;?>" name="slcClasificacionPadreProducto_<?php print $intCount;?>[]" class="form-control-sm select2-multiple col-2 " multiple>
                                                
                                                <?php
                                                reset($arrClasificacionPadreHijo);                                                
                                                while( $arrTMP = each($arrClasificacionPadreHijo) ){
                                                    
                                                    $strSelected = isset($rTMP["value"]["clasificacion_padre_hijo"][$arrTMP["key"]]) ? "selected" : "";
                                                    
                                                    ?>
                                                    <option <?php print $strSelected;?> value="<?php print $arrTMP["key"]?>"><?php print $strLenguaje == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?></option>
                                                    <?php
                                                    
                                                }                            
                                                
                                                ?>
                                                
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-4 pt-3 text-center text-lg-right">
                                            
                                            <button class="btn btn-secondary btn-sm mt-lg-3" onclick="fntDeleteProducto('<?php print $intCount;?>')"><?php print lang["eliminar"]?></button>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="divVistaPrevia_<?php print $intCount?>" >
                                    
                                    <div class="row justify-content-center">
                                        <div class="col-8 col-lg-10">
                                            <h5 class="text-dark text-center text-lg-left  lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                                <?php print $rTMP["value"]["nombre"];?>                   
                                    
                                            </h5>
                                        
                                        </div>
                                        <div class=" col-12 col-lg-2 text-center text-lg-left">
                                            
                                            <?php
                                            
                                            if( $rTMP["value"]["tipo"] == 1 ){
                                                ?>
                                                
                                                <button onclick="fntEditProducto('<?php print $intCount?>');" class="btn btn-secondary btn-sm text-background-theme"  >
                                                    <?php print lang["editar"]?>                        
                                                </button>
                                                <button  onclick="fntDeleteProducto('<?php print $intCount;?>')" class="btn btn-secondary btn-sm text-background-theme  mt-0"  >
                                                    <?php print lang["eliminar"]?>                        
                                                </button>
                                                <?php    
                                            }
                                            if( $rTMP["value"]["tipo"] == 2 ){
                                                ?>
                                                <button onclick="fntShowProductoEspecial('<?php print fntCoreEncrypt($rTMP["key"]);?>', '<?php print ($rTMP["value"]["id_clasificacion_padre"]);?>')" class="btn btn-secondary btn-sm text-background-theme"  >
                                                    <?php print lang["editar"]?>                        
                                                </button>
                                                <button  onclick="fntEliminarProductoEspecial('<?php print fntCoreEncrypt($rTMP["key"]);?>', '<?php print ($rTMP["value"]["id_clasificacion_padre"]);?>')" class="btn btn-secondary btn-sm text-background-theme  mt-0"  >
                                                    <?php print lang["eliminar"]?>                        
                                                </button>
                                            
                                                <?php    
                                            }
                                            
                                            ?>
                                        </div>
                                    </div>
                                        
                                                
                                        
                                    <h5 class="  text-left m-0 p-0" style="font-size: 14px;">
                                        <?php print $rTMP["value"]["descripcion"];?>               
                                    </h5>
                                    <h5 class="text-left m-0 p-0 text-color-theme" style="font-size: 14px;">
                                        Q <?php print $rTMP["value"]["precio"];?>             
                                    </h5>
                                    <h3 class="text-left  m-0 p-0" style="font-size: 16px;">
                                        
                                        <?php
                                        reset($arrClasificacionPadreHijo);                                                
                                        while( $arrTMP = each($arrClasificacionPadreHijo) ){
                                            
                                            if( isset($rTMP["value"]["clasificacion_padre_hijo"][$arrTMP["key"]]) ){
                                                
                                                ?>
                                                <span class="badge  text-white text-background-theme"  style="">
                                                    <?php print $strLenguaje == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?>                        
                                                </span>
                                                <?php
                                                
                                            }                                            
                                            
                                        }                            
                                        
                                        ?>
                                    </h3>
                                </div>
                                
                            </div>
                        </div>
                        <?php
                        $intCount++;
                    }
                    
                }
                
                if( !$intPlaceProducto ){
                    ?>
                    
                    <div class="row" id="divBotonAgregar">
                        <div class="col-12 col-lg-6 mt-4">
                            <button class="btn btn-secondary btn-block text-background-theme" onclick="fntAddProducto();" > 
                                <?php print lang["agregar_producto"]?> <i class="fa fa-plus"></i> 
                            </button>
                        </div>
                        <div class="col-12 col-lg-6 mt-4">
                            <button class="btn btn-secondary btn-block text-background-theme" onclick="fntAddProductoEspecial();" > 
                                <?php print lang["agregar_producto_con_pagina"]?> 
                                <i class="fa fa-plus"></i> 
                            </button>
                        </div>
                    </div>
                    
                    <?php
                }
                
                ?>
                            
                </form>
            </div>
            
            <div class="modal-footer pb-1 pt-2" id="modalGeneralFooter">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php print lang["cerrar"]?></button>
                <button type="button" class="btn btn-success text-background-theme" onclick="fntSaveProducto();"><?php print lang["guardar"]?></button>
            
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="dist/crooperImage/css/cropper.css" />
    <script src="dist/crooperImage/js/cropper.js"></script>
        
    <script> 
    
        var intCount = "<?php  print $intCount;?>"; 
        var cropper = new Array();  
        var formDataFileRotate = new Array();
        
        $(document).ready(function() { 
                
            $(".select2-multiple").select2({
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });   
            
            
            $('#modalGeneralBody').imagesLoaded().always( function( instance ) {
                
                setTimeout(function(){
                    
                    $(":input[id^=hidIdPlaceProducto_]").each(function (){
                        
                        arrExplode = this.id.split("_");

                        var image = document.getElementById('imgProducto_'+arrExplode[1]);
                            
                        cropper[arrExplode[1]] = new Cropper(image, {
                            autoCrop: false,
                            restore: false,
                            checkCrossOrigin: false,
                            checkOrientation: false,
                            modal: false,
                            guides: false,
                            highlight: false,
                            background: false,
                            autoCrop: false,
                            movable: false,
                            rotatable: true,
                            scalable: false,
                            zoomable: false,
                            zoomOnTouch: false,
                            zoomOnWheel: false,
                            cropBoxMovable: false,
                            cropBoxResizable: false,
                            toggleDragModeOnDblclick: false,
                            dragMode: 'none',
                            ready: function () {
                            //Should set crop box data first here
                            }
                        }); 
                        
                        
                    });
                    
                }, 1000);
                
            });
            

        }); 
        
        function fntRotateSeccion1(intIndex){
               
               
            //$("#btnImgProductoRotate_"+intIndex).hide();
            $("#ibtnImgProductoRotate_"+intIndex).removeClass("fa-rotate-right");
            $("#ibtnImgProductoRotate_"+intIndex).addClass("fa-spinner");
            $("#ibtnImgProductoRotate_"+intIndex).addClass("fa-spin");
            
            setTimeout(function(){
                
                cropper[intIndex].rotate(45);
            
                cropper[intIndex].getCroppedCanvas().toBlob( (blob) => {
                                                     
                    
                    $("#hidProductoFotoRotar_"+intIndex).val("Y");
                    $("#hidUpdate_"+intIndex).val("Y");
                            
                    formDataFileRotate[intIndex] = blob;
                    
                });
                
            }, 300);
            
            setTimeout(function(){
                
                //$("#btnImgProductoRotate_"+intIndex).show();
                $("#ibtnImgProductoRotate_"+intIndex).addClass("fa-rotate-right");
                $("#ibtnImgProductoRotate_"+intIndex).removeClass("fa-spinner");
                $("#ibtnImgProductoRotate_"+intIndex).removeClass("fa-spin");
                
            
            }, 1000);
            
                               
        }
            
        function fntSetLogoPreview(index){
                                        
            var filename = $('#flImgProducto_'+index).val();
            arr = filename.split(".");
 
            if( !( arr[1] == "jpg" || arr[1] == "png" || arr[1] == "jpeg" ) ){
                
                swal("Error!", "<?php print lang["alerta_solo_imagenes"]?>", "error");
                return false;                    
                
            }
            
            
            if ( ! window.FileReader ) {
                return alert( 'FileReader API is not supported by your browser.' );
            }
            var $i = $( '#flImgProducto_'+index ), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if ( input.files && input.files[0] ) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    //$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
                    
                    image = document.getElementById("imgProducto_"+index);
                    
                    image.src = fr.result
                                        
                    cropper[index].destroy();
                    
                    cropper[index] = new Cropper(image, {
                        autoCrop: false,
                        restore: false,
                        checkCrossOrigin: false,
                        checkOrientation: false,
                        modal: false,
                        guides: false,
                        highlight: false,
                        background: false,
                        autoCrop: false,
                        movable: false,
                        rotatable: true,
                        scalable: false,
                        zoomable: false,
                        zoomOnTouch: false,
                        zoomOnWheel: false,
                        cropBoxMovable: false,
                        cropBoxResizable: false,
                        toggleDragModeOnDblclick: false,
                        dragMode: 'none',
                        ready: function () {
                        //Should set crop box data first here
                        }
                    });
                };
                //fr.readAsText( file );
                fr.readAsDataURL( file );
            } else {
                // Handle errors here
                alert( "File not selected or browser incompatible." )
            }
                
        }
        
        function fntAddProducto(){
            
            /*
            intCount = 0;
            $(":input[id^=hidIdGaleria_]").each(function (){
                
                intCount++;                    
                
            });
            
            if( intCount == "<?php print 2?>" ){
                
                swal("Error!", "Have a total of <?php print 2?> photos configured for your gallery", "error");
                
                return false;
                                    
            }
             */
            
             
                                                                    
            $("#divBotonAgregar").before("<input type=\"hidden\" id=\"hidIdPlaceProducto_"+intCount+"\" name=\"hidIdPlaceProducto_"+intCount+"\" value=\"0\">  "+
                                         "<input type=\"hidden\" id=\"hidProductoUpdate_"+intCount+"\" name=\"hidProductoUpdate_"+intCount+"\" value=\"Y\"> "+
                                         "<input type=\"hidden\" id=\"hidProductoFotoRotar_"+intCount+"\" name=\"hidProductoFotoRotar_"+intCount+"\" value=\"N\"> "+
                                         "<div class=\"row p-0 m-0\" id=\"divContenidoProducto_"+intCount+"\" llave=\""+intCount+"\" >   "+
                                         "   <div class=\"col-lg-3 col-12 p-0 m-0 mt-3 text-right \">    "+
                                         "       <input type=\"file\" id=\"flImgProducto_"+intCount+"\" name=\"flImgProducto_"+intCount+"\" onchange=\"fntSetLogoPreview('"+intCount+"');\" style=\"display: none;\">   "+
                                         "       <img id=\"imgProducto_"+intCount+"\" style=\"width: <?php print $boolMovil ? "200px" : "150px"?>; height: <?php print $boolMovil ? "200px" : "150px"?>; object-fit: contain;\" src=\"images/Noimagee.jpg?1\" class=\"img-fluid rounded\">    "+
                                         "        <div id=\"btnImgProducto_"+intCount+"\" class=\"btn-theme-circular\" onclick=\"$('#flImgProducto_"+intCount+"').click();\" style=\"padding: 2px 3px 2px 3px; font-size: 14px; display:none; right:<?php print $boolMovil ? "33%" : "35%"?> !important; \">   "+
                                         "           &nbsp;<i class=\"fa fa-pencil\"></i>&nbsp;   "+
                                         "       </div>  "+
                                         "       <div id=\"btnImgProductoRotate_"+intCount+"\" class=\"btn-theme-circular\" onclick=\"fntRotateSeccion1('"+intCount+"');\" style=\"padding: 2px 3px 2px 3px; font-size: 14px; display:none; right:<?php print $boolMovil ? "52%" : "50%"?> !important; \">   "+
                                         "           &nbsp;<i id=\"ibtnImgProductoRotate_"+intCount+"\" class=\"fa fa-rotate-right\"></i>&nbsp;     "+
                                         "       </div>    "+
                                         "   </div>        "+
                                         "   <div class=\"col-lg-9 col-12 p-0 m-0 pl-3 mt-3  pb-1\">        "+
                                         "       <div id=\"divVistaEdicion_"+intCount+"\">     "+                         
                                         "           <input type=\"text\" class=\"form-control form-control-sm\" id=\"txtNombre_"+intCount+"\" name=\"txtNombre_"+intCount+"\" placeholder=\"Nombre en español\">        "+
                                         "           <textarea class=\"form-control form-control-sm mt-1\" id=\"txtDescripcion_"+intCount+"\" name=\"txtDescripcion_"+intCount+"\" rows=\"2\" placeholder=\"Descripción en español\"></textarea>     "+
                                         "           <button class=\"btn btn-secondary btn-sm text-background-theme mt-1\" onclick=\"fntTraducirNombreDescripcion("+intCount+")\" >  "+
                                         "               Traducir / Translate "+
                                         "           </button>"+
                                         "           <input type=\"text\" class=\"form-control form-control-sm mt-1\" id=\"txtNombreEN_"+intCount+"\" name=\"txtNombreEN_"+intCount+"\" placeholder=\"Name in english\">        "+
                                         "           <textarea class=\"form-control form-control-sm mt-1\" id=\"txtDescripcionEN_"+intCount+"\" name=\"txtDescripcionEN_"+intCount+"\" rows=\"2\" placeholder=\"Description in english\"></textarea>     "+
                                         "           <input value=\"\" type=\"text\" class=\"form-control form-control-sm mt-1 mb-1 col-12 col-lg-4 text-right\" id=\"txtPrecio_"+intCount+"\" name=\"txtPrecio_"+intCount+"\" placeholder=\"<?php print lang["precio"]?> Q.\" onchange=\"fntMantenerDecimales(this);\">  "+
                                         "           <div class=\"row\">         "+
                                         "               <div class=\"col-12 col-lg-8\">      "+
                                         "                   <label for=\"slcClasificacionPadreProducto_"+intCount+"\"><small><?php print lang["clasificacion"]?></small></label>   "+
                                         "                   <select  id=\"slcClasificacionPadreProducto_"+intCount+"\" name=\"slcClasificacionPadreProducto_"+intCount+"[]\" class=\"form-control-sm select2-multiple col-2 \" multiple>    "+
                                                                <?php
                                                                reset($arrClasificacionPadreHijo);
                                                                while( $rTMP = each($arrClasificacionPadreHijo) ){
                                                                    ?>
                                         "                           <option value=\"<?php print $rTMP["key"]?>\"><?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>      "+
                                                                    <?php
                                                                }                            
                                                                ?>
                                         "                   </select>      "+
                                         "               </div>         "+
                                         "               <div class=\"col-12 col-lg-4 pt-3 text-center text-lg-right\">    "+
                                         "                   <button class=\"btn btn-secondary btn-sm mt-lg-3\" onclick=\"$('#divContenidoProducto_"+intCount+"').remove(); $('#divBotonAgregar').show();\"><?php print lang["eliminar"]?></button>  "+
                                         "               </div>    "+
                                         "           </div>     "+
                                         "       </div>     "+
                                         "   </div>  "+
                                         "</div>");
            
            $("#slcClasificacionPadreProducto_"+intCount).select2({
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            }); 
            
            $('#divContenidoProducto_'+intCount).imagesLoaded().always( function( instance ) {
                
                arrExplode = instance.images[0].img.id.split("_");
                
                setTimeout(function(){
                    
                    $(":input[id^=hidIdPlaceProducto_"+arrExplode[1]+"]").each(function (){
                        
                        arrExplode = this.id.split("_");

                        var image = document.getElementById('imgProducto_'+arrExplode[1]);
                            
                        console.log(arrExplode[1]);
                        cropper[arrExplode[1]] = new Cropper(image, {
                            autoCrop: false,
                            restore: false,
                            checkCrossOrigin: false,
                            checkOrientation: false,
                            modal: false,
                            guides: false,
                            highlight: false,
                            background: false,
                            autoCrop: false,
                            movable: false,
                            rotatable: true,
                            scalable: false,
                            zoomable: false,
                            zoomOnTouch: false,
                            zoomOnWheel: false,
                            cropBoxMovable: false,
                            cropBoxResizable: false,
                            toggleDragModeOnDblclick: false,
                            dragMode: 'none',
                            ready: function () {
                            //Should set crop box data first here
                            }
                        }); 
                        
                            
                        $("#btnImgProducto_"+arrExplode[1]).show();
                        $("#btnImgProductoRotate_"+arrExplode[1]).show();
                        
                    });
                    
                }, 2000);
                
                
            });
                  
            intCount++;
            
            $("#divBotonAgregar").hide();
        }    
        
        function fntDeleteProducto(intIndex){
                           
            swal({
                title: "",
                text: "<?php print lang["pregunta_eliminar_producto"]?>",
                type: "",
                showCancelButton: true,
                confirmButtonClass: "#FF0000",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                     
                    $("#hidProductoDelete_"+intIndex).val('Y');
                    fntSaveProducto();
            
                    
                
                } 
            });
            
        }
        
        function fntEditProducto(intIndex){
                    
            $("#btnImgProducto_"+intIndex).show();
            $("#btnImgProductoRotate_"+intIndex).show();
            $("#divVistaEdicion_"+intIndex).show();
            $("#divVistaPrevia_"+intIndex).hide();
            $("#hidProductoUpdate_"+intIndex).val('Y');
                        
        }
        
        function fntSaveProducto(){
            
            var formData = new FormData(document.getElementById("frmProducto"));
            
            $(":input[id^=hidProductoFotoRotar_]").each(function (){
            
                if( this.value == "Y" ){
                    
                    arrExplode = this.id.split("_");
                                                         
                    formData.append('hidFileIMGflFileImage_'+arrExplode[1], formDataFileRotate[arrExplode[1]]);
                            
                }
                
            });
                
            $(".preloader").fadeIn();
            $.ajax({
                url: "placeup.php?setProductoClasificacion=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    setPaginaPublica();
                    $(".preloader").fadeOut();
                    fntDrawSeccionProductoClasificacionPadre('<?php print ($intClasificacionPadre)?>');
                    
                    $("#modalGeneral").modal("hide");
            
                }
                        
            });
            
        }
        
        function fntEliminarProductoEspecial(strProducto, strProductoCla){
            
            swal({
                title: "",
                text: "<?php print lang["pregunta_eliminar_producto"]?>",
                type: "",
                showCancelButton: true,
                confirmButtonClass: "#FF0000",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                     
                    $.ajax({
                        url: "placeup.php?drawEliminarProductoEspecial=true&key="+strProducto+"&strProductoCla="+strProductoCla, 
                        success: function(result){
                            
                            $(".preloader").fadeOut();
                            fntDrawSeccionProductoClasificacionPadre('<?php print ($intClasificacionPadre)?>');
                            $("#modalGeneral").modal("hide");    
                            
                        }
                    });
                
                } 
            });
                        
        }
        
        function fntAddProductoEspecial(){
            
            $.ajax({
                url: "placeup.php?getProductoEspecial=true&place=<?php print fntCoreEncrypt($intPlace);?>&cpadre=<?php print fntCoreEncrypt($intClasificacionPadre);?>", 
                success: function(result){
                    
                    //$("#modalGeneral").modal("hide");
                    //window.open('proes.php?p='+result);
                    fntShowProductoEspecial(result, '<?php print ($intClasificacionPadre);?>');
                    
                }
            });
                
        }
        
        function fntTraducirNombreDescripcion(intIndex){
            
            var formData = new FormData();
                                                           
            formData.append('txtNombre', $("#txtNombre_"+intIndex).val());
            formData.append('txtDescripcion', $("#txtDescripcion_"+intIndex).val());
            
            $(".preloader").fadeIn();
            $.ajax({
                url: "placeup.php?getTraduccion=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType : "json",
                success: function(result){
                    
                    
                    $(".preloader").fadeOut();
                    $("#txtNombreEN_"+intIndex).val(result["nombre"]);
                    $("#txtDescripcionEN_"+intIndex).val(result["descripcion"]);                                      
            
                }
                        
            });
                        
        }
        
    </script>        
    <?php   
                       
    die();
}

if( isset($_GET["drawEliminarProductoEspecial"]) ){
    
    $intPlaceProducto = isset($_GET["key"]) ? intval(fntCoreDecrypt($_GET["key"])) : 0;
        
    if( $intPlaceProducto ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $strQuery = "SELECT url_imagen
                     FROM   place_producto_galeria
                     WHERE  id_place_producto = {$intPlaceProducto} ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        drawdebug($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            if( !empty($rTMP["url_imagen"]) )
                unlink($rTMP["url_imagen"]);
                        
        }
        
        $objDBClass->db_free_result($qTMP);
                
        $strQuery = "DELETE FROM place_producto_galeria WHERE id_place_producto = {$intPlaceProducto} "; 
        $objDBClass->db_consulta($strQuery);
        
        $strQuery = "DELETE FROM place_producto WHERE id_place_producto = {$intPlaceProducto} "; 
        $objDBClass->db_consulta($strQuery);
        
        $objDBClass->db_close();                               
        
    }
    
    die();
}

use \Statickidz\GoogleTranslate;
if( isset($_GET["getTraduccion"]) ){
    
    
    $strNombre = isset($_POST["txtNombre"]) ? $_POST["txtNombre"] : "";
    $strDescripcion = isset($_POST["txtDescripcion"]) ? $_POST["txtDescripcion"] : "";
    
    require_once ('dist_interno/test_trans/vendor/autoload.php');
    
    $source = 'es';
    $target = 'en';
    $text = 'buenos días, como estas';

    $trans = new GoogleTranslate();
    $arr["nombre"] = $trans->translate("es", "en",  $strNombre );
    $arr["descripcion"] = $trans->translate("es", "en",  $strDescripcion );

    print json_encode($arr);
    
    die();
    
}

if( isset($_GET["getProductoEspecial"]) ){
    
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intClasificacionPadre = isset($_GET["cpadre"]) ? intval(fntCoreDecrypt($_GET["cpadre"])) : 0;
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "INSERT INTO place_producto(id_place, id_clasificacion_padre, estado, tipo)
                                     VALUES({$intPlace}, {$intClasificacionPadre} , 'P', '2')  "; 
    $objDBClass->db_consulta($strQuery);
    $intPlaceProducto = $objDBClass->db_last_id();
    $objDBClass->db_close();                               
    
    print fntCoreEncrypt($intPlaceProducto);
    
    die();
}

if( isset($_GET["setProductoClasificacion"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $intPlaceClasificacionPadre = isset($_POST["hidClasificacionPadre"]) ? intval(fntCoreDecrypt($_POST["hidClasificacionPadre"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "hidIdPlaceProducto" ){
                                                     
            $intPlaceProducto = isset($_POST["hidIdPlaceProducto_{$arrExplode[1]}"]) ? intval($_POST["hidIdPlaceProducto_{$arrExplode[1]}"]) : "";
            $strNombre = isset($_POST["txtNombre_{$arrExplode[1]}"]) ? addslashes($_POST["txtNombre_{$arrExplode[1]}"]) : "";
            $strNombreEN = isset($_POST["txtNombreEN_{$arrExplode[1]}"]) ? addslashes($_POST["txtNombreEN_{$arrExplode[1]}"]) : "";
            $strPrecio = isset($_POST["txtPrecio_{$arrExplode[1]}"]) ? addslashes($_POST["txtPrecio_{$arrExplode[1]}"]) : "";
            $strDescripcion = isset($_POST["txtDescripcion_{$arrExplode[1]}"]) ? addslashes($_POST["txtDescripcion_{$arrExplode[1]}"]) : "";
            $strDescripcionEN = isset($_POST["txtDescripcionEN_{$arrExplode[1]}"]) ? addslashes($_POST["txtDescripcionEN_{$arrExplode[1]}"]) : "";
            
            $boolProductoDelete = isset($_POST["hidProductoDelete_{$arrExplode[1]}"]) && trim($_POST["hidProductoDelete_{$arrExplode[1]}"]) == "Y" ? true : false;
            $boolProductoEdit = isset($_POST["hidProductoUpdate_{$arrExplode[1]}"]) && trim($_POST["hidProductoUpdate_{$arrExplode[1]}"]) == "Y" ? true : false;
            
            if( $boolProductoDelete ){
                                   
                $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo WHERE id_place_producto = {$intPlaceProducto}";
                $objDBClass->db_consulta($strQuery);
                                    
                $strUrlArchivo = "images/place/product/".fntCoreEncrypt($intPlaceProducto).".png";
                
                if( file_exists($strUrlArchivo) )
                    unlink($strUrlArchivo);
                
                $strQuery = "DELETE FROM place_producto WHERE id_place_producto = {$intPlaceProducto}";
                $objDBClass->db_consulta($strQuery);
                
                die();
            }
            elseif( $boolProductoEdit ){
                
                if( !$intPlaceProducto ){
                    
                    $strQuery = "INSERT INTO place_producto(id_place, id_clasificacion_padre, nombre, precio, descripcion,
                                                            nombre_en, descripcion_en)
                                                     VALUES( {$intPlace}, {$intPlaceClasificacionPadre}, '{$strNombre}', '{$strPrecio}', '{$strDescripcion}',
                                                            '{$strNombreEN}', '{$strDescripcionEN}' )";
                    $objDBClass->db_consulta($strQuery);
                    $intPlaceProducto = $objDBClass->db_last_id();
                    
                }
                else{
                    
                    $strQuery = "UPDATE place_producto
                                 SET    nombre = '{$strNombre}',
                                        precio = '{$strPrecio}', 
                                        descripcion = '{$strDescripcion}',
                                        nombre_en = '{$strNombreEN}',
                                        descripcion_en = '{$strDescripcionEN}'
                                         
                                 WHERE  id_place_producto = {$intPlaceProducto} ";
                    $objDBClass->db_consulta($strQuery);
                    
                }
                
                if( $intPlaceProducto ){
                    
                    $intMkTime = fntGetMkTime();
    
                    if( isset($_FILES["hidFileIMGflFileImage_{$arrExplode[1]}"]) && $_FILES["hidFileIMGflFileImage_{$arrExplode[1]}"]["size"] > 0 ){
                        
                        $arrNameFile = explode("_", $_FILES["hidFileIMGflFileImage_{$arrExplode[1]}"]["name"]); 
                        $strUrlArchivo = "../../file_inguate/place_interno/producto/".fntCoreEncrypt($intPlaceProducto).",{$intMkTime}.webp";
                        rename($_FILES["hidFileIMGflFileImage_{$arrExplode[1]}"]["tmp_name"], $strUrlArchivo);
                        
                        $strQuery = "UPDATE place_producto
                                     SET    logo_url = '{$strUrlArchivo}' 
                                     WHERE  id_place_producto = {$intPlaceProducto} ";
                        $objDBClass->db_consulta($strQuery);
                        fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceProducto).",".$intMkTime);
                
                    }elseif( isset($_FILES["flImgProducto_{$arrExplode[1]}"]) && $_FILES["flImgProducto_{$arrExplode[1]}"]["size"] > 0 ){
                        
                        $arrNameFile = explode("_", $_FILES["flImgProducto_{$arrExplode[1]}"]["name"]); 
                        $strUrlArchivo = "../../file_inguate/place_interno/producto/".fntCoreEncrypt($intPlaceProducto).",{$intMkTime}.webp";
                        rename($_FILES["flImgProducto_{$arrExplode[1]}"]["tmp_name"], $strUrlArchivo);
                        
                        $strQuery = "UPDATE place_producto
                                     SET    logo_url = '{$strUrlArchivo}' 
                                     WHERE  id_place_producto = {$intPlaceProducto} ";
                        $objDBClass->db_consulta($strQuery);
                        fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceProducto).",".$intMkTime);
                
                    }
                    
                    $strQuery = "DELETE FROM place_producto_clasificacion_padre_hijo WHERE id_place_producto = {$intPlaceProducto}";
                    $objDBClass->db_consulta($strQuery);
                    
                    if( isset($_POST["slcClasificacionPadreProducto_{$arrExplode[1]}"]) && count($_POST["slcClasificacionPadreProducto_{$arrExplode[1]}"]) > 0 ){
                        
                        while( $arrTMP = each($_POST["slcClasificacionPadreProducto_{$arrExplode[1]}"]) ){
                            
                            $strQuery = "INSERT INTO place_producto_clasificacion_padre_hijo(id_place_producto, id_clasificacion_padre_hijo)
                                                                                    VALUES({$intPlaceProducto}, {$arrTMP["value"]})";
                            $objDBClass->db_consulta($strQuery);
                        }
                        
                    }
                    
                    
                }
                
            }
            
        }
        
    }    
    
    //fntCalculoPrecioPlaceProducto($intPlace, $intPlaceClasificacionPadre, true, $objDBClass);
    fntCarculoPrecio($intPlace, true, $objDBClass);
    
    die();
}

if( isset($_GET["setPlaceAmenidad"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "DELETE FROM place_amenidad WHERE id_place = {$intPlace} ";
    $objDBClass->db_consulta($strQuery);
    
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "chkAmenidad" ){
            
            $strQuery = "INSERT INTO place_amenidad(id_place, id_amenidad)
                                            VALUES({$intPlace}, {$arrExplode[1]})";
            $objDBClass->db_consulta($strQuery);
            
        }
        
    }
    
    die();    
    
}

if( isset($_GET["setSaveDatosPrincipales"]) ){
    
    include "core/dbClass.php";
    
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    
    $strTitulo = isset($_POST["txtTitulo"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtTitulo"])) : 0;
    $strDescripcion = isset($_POST["txtDescripcion"]) ? addslashes(fntEliminarTildes($_POST["txtDescripcion"])) : 0;
    $strTelefono = isset($_POST["txtTelefono"]) ? addslashes(fntCoreStringQueryLetra($_POST["txtTelefono"])) : 0;
    $strDireccion = isset($_POST["txtDireccion"]) ? addslashes(fntEliminarTildes($_POST["txtDireccion"])) : 0;
    $strUrlYoutube = isset($_POST["txtUrlYoutube"]) ? addslashes(($_POST["txtUrlYoutube"])) : 0;
    
    $arr["error"] = false;
    $arr["msn"] = "";
    if( $intPlace ){
        
        $objDBClass = new dbClass();
        
        $strTituloBusqueda = fntGetNameEstablecimeintoFiltro($strTitulo, true, $objDBClass);
        $arrNombrePaginaPublica = fntGetNombrePaginaPublica($strTitulo, $intPlace, true, $objDBClass);
    
    
        if( $arrNombrePaginaPublica["error"] ){
            
            $arr["error"] = true;
            $arr["msn"] = lang["error_titulo_negocio_no"];
            
            print json_encode($arr);
            die();
            
        }
    
        $strQuery = "UPDATE place
                     SET    titulo = '{$strTitulo}',
                            descripcion = '{$strDescripcion}', 
                            telefono = '{$strTelefono}', 
                            direccion = '{$strDireccion}',
                            texto_autocomplete = '{$strTituloBusqueda}', 
                            url_place = '{$arrNombrePaginaPublica["url_place"]}', 
                            url_youtube = '{$strUrlYoutube}'
                     WHERE  id_place = {$intPlace} ";
        $objDBClass->db_consulta($strQuery);
        
        
        while( $rTMP = each($_POST) ){
            
            $arrExplode = explode("_", $rTMP["key"]);
            
            if( $arrExplode[0] == "hidKeyDia" ){
                
                $intPlaceHorario = isset($_POST["hidPlaceHorario_{$arrExplode[1]}"]) ? intval($_POST["hidPlaceHorario_{$arrExplode[1]}"]) : 0;
                $intKeyDia = isset($_POST["hidKeyDia_{$arrExplode[1]}"]) ? intval($_POST["hidKeyDia_{$arrExplode[1]}"]) : 0;
                $strClose = isset($_POST["chkClose_{$arrExplode[1]}"]) ? "Y" : "N";
                $intHoraInicio = isset($_POST["slcHorarioInicio_{$arrExplode[1]}"]) ? trim($_POST["slcHorarioInicio_{$arrExplode[1]}"]) : 0;
                $intHoraFin = isset($_POST["slcHorarioFin_{$arrExplode[1]}"]) ? trim($_POST["slcHorarioFin_{$arrExplode[1]}"]) : 0;
                           
                $intHoraInicio = $intHoraInicio.":00";
                $intHoraFin = $intHoraFin.":00";
                
                if( !$intPlaceHorario ){
                    
                    $strQuery = "INSERT INTO place_horario (id_place, id_dia, hora_inicio, hora_fin, cerrado)
                                                    VALUES( {$intPlace}, {$intKeyDia}, '{$intHoraInicio}', '{$intHoraFin}', '{$strClose}')";
                    $objDBClass->db_consulta($strQuery);                
                }
                else{
                    
                    $strQuery = "UPDATE place_horario
                                 SET    id_dia = '{$intKeyDia}',
                                        hora_inicio = '{$intHoraInicio}',
                                        hora_fin = '{$intHoraFin}',
                                        cerrado = '{$strClose}'
                                 WHERE  id_place_horario = {$intPlaceHorario} ";
                    $objDBClass->db_consulta($strQuery);
                                    
                }
                
                
            }
            
        }
            
    
            
    }
    
    print json_encode($arr);
    die();
}

if( isset($_GET["setUbicacionPlace"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
        
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $strLat = isset($_GET["lat"]) ? $_GET["lat"] : 0;
    $strLng = isset($_GET["lng"]) ? $_GET["lng"] : 0;
    
    $strQuery = "UPDATE place
                 SET    ubicacion_lat = '{$strLat}',
                        ubicacion_lng = '{$strLng}'
                 WHERE  id_place = {$intPlace} ";
    
    $objDBClass->db_consulta($strQuery);
    
    
    die();    
}

if( isset($_GET["fntDrawSeccion"]) ){
    
    include "core/dbClass.php";
    
    $intSeccion = isset($_GET["seccion"]) ? intval($_GET["seccion"]) : 0;
    $boolPuclica = isset($_GET["pdd"]) && $_GET["pdd"] =="Y" ? true : false;
    
    $intDiaSemana = isset($_GET["diaS"]) ? intval($_GET["diaS"]) : 0;
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    
    if( $intSeccion == 1 ){
        
        $objDBClass = new dbClass();  
        
        $arrGetGaleriaPlace = fntGetPlaceGaleria($intPlace, true, $objDBClass);
        
        ?>
        <style>
            .imgSlie:focus{
                border: 0px !important;
            }
        </style>
        <div class="slidePlace slider center" style="" >
            
            <?php
            
            if( is_array($arrGetGaleriaPlace) &&  count($arrGetGaleriaPlace) > 0 ){
                
                $i = 1;
                while( $rTMP = each($arrGetGaleriaPlace) ){
                    
                    ?>
                    <div class="p-0 imgSlie imgGaleryDIV_<?php print $i;?>" style="" yordi="<?php print $i;?>" style="">
                        <img src="imgInguate.php?t=up&src=<?php print $rTMP["value"]["url_imagen"]?>" class="img-fluid p-3 imgSlie imgSlideGallery imgGalery_<?php print $i;?>" style="border-radius: 8% !important; width: 100%; height: auto; object-fit: cover; "> 
                    </div>
                    
                    <?php
                    $i++;                        
                }                
                
            }
            else{
                
                for( $i = 1 ; $i <= 10; $i++ ){
                    
                    ?>
                    <div class="p-0  imgGaleryDIV_<?php print $i;?>" style="" yordi="<?php print $i;?>" style="">
                        <img src="images/Noimagee.jpg?1" class="img-fluid p-3 imgSlideGallery imgGalery_<?php print $i;?>" style="border-radius: 10% !important; "> 
                    </div>
                    <?php
                }
            }
            
                
            ?>
            
        </div>
        <script>
            
            $(document).ready(function() {
                
                $('.slidePlace').slick({
                    arrows: false,
                    centerMode: true,
                    infinite: true,
                    centerPadding: '60px',
                    slidesToShow: 4,
                    speed: 200,
                    autoplay: true,
                    variableWidth: false,
                    rtl:false,
                    responsive: [ {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1,
                            autoplay: true,
                            speed: 200
                        }
                    }]
                        
                });
                
                $('.slidePlace').on('afterChange', function(event, slick, currentSlide, nextSlide){
                    
                    var dataId = $('.slick-current').attr("data-slick-index");    
                    dataId++;
                    $(".imgSlideGallery").removeClass("slick-center-Border");
                    $(".imgGalery_"+dataId).addClass("slick-center-Border");
                
                });
                
                $('.slidePlace').slick('slickGoTo',"0");
                
                var sinWindowsHeight = $(window).height();
        
                $('.imgSlideGallery ').each(function (){
                    
                    $(this).height( ( sinWindowsHeight * ( 30 / 100 ) )+'px');
                    
                });  
                    
            });
            
        </script>
        <?php            
    }
    
    if( $intSeccion == 2 ){
        
        $objDBClass = new dbClass();  
        $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
        $arrDatosPlaceComentario = fntGetPlaceComentario($intPlace, true, $objDBClass);
        $arrPlaceHorario = fntGetPlaceHorario($intPlace, true, $objDBClass);
        
        $arrDiasSemana = getSemana();
        $arrHorario = getHoras24Horas();
        
        ?> 
        <div class="row m-0 p-0">
            <div class="col-12 m-0 p-0">
                <?php
                        
                if( $_SESSION["boolUserPanelLogIn"] && !$boolPuclica ){
                    ?>
                    <div class="btn-theme-circular p-1" onclick="fntCargarModalSeccion('2')">
                        &nbsp;<i class="fa fa-pencil"></i>&nbsp;
                    </div>
                    <?php
                }
                
                ?>
                    
                <div class="text-center text-lg-left m-0 p-0  h4">
                    <?php print $arrDatosPlace["titulo"]?>
                </div>
                <div class=" text-center text-lg-left  m-0 p-0" style="">
                
                    <?php
                    for( $i = 1; $i <= 5 ;  $i++ ){
                        
                        $strEstrella =  $i <=  $arrDatosPlace["estrellas"] ? "fa-star" : "fa-star-o";
                        ?>
                        <i class="fa  <?php print $strEstrella;?> text-color-theme m-0 p-0" style="font-size: 12px;"></i>
                        <?php
                    }
                    ?>
                
                    
                </div>
                
                <?php
                
                if( !empty($arrDatosPlace["descripcion"]) ){
                    ?>
                    <p class="text-left text-lg-left text-secondary">
                        <div class="col-12 text-left">
                            
                            <?php 
                            print $arrDatosPlace["descripcion"];
                            ?>
                        </div>
                        
                    </p>
                    <?php
                }
                
                if( !empty($arrDatosPlace["telefono"]) ){
                    ?>
                    <div class="text-center text-lg-left h6" style="">
                        <a href="tel:3434343434" class="text-dark">
                            <i class="fa fa-phone pl-1 text-color-theme"></i>
                            <?php print $arrDatosPlace["telefono"]?>
                            
                        </a>
                    </div>
                    <?php
                }
                
                if( !empty($arrDatosPlace["direccion"]) ){
                    ?>
                    <div class="text-center text-lg-left h6" style="">
                        <i class="fa fa-map-marker pl-1 mr-1 text-color-theme"></i>
                        <?php print $arrDatosPlace["direccion"]?>
                    </div>
                    <?php
                }
                
                ?>
                    
                <div class="text-center text-lg-left h6" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;">
                    <i class="fa fa-clock-o pl-1 text-color-theme"></i>
                    <?php
                    
                    if( isset($arrPlaceHorario[$intDiaSemana]) ){
                        
                        if( $arrPlaceHorario[$intDiaSemana]["cerrado"] == "Y" ){
                            
                            ?>
                            <span class="text-danger font-weight-bold">Cerrado</span> 
                            &nbsp;
                            &nbsp;
                            <i class="fa fa-angle-down text-color-theme"></i>
                            <?php
                            
                        }
                        else{
                            
                            ?>
                            <span class="text-success font-weight-bold">Hoy Abierto</span> : 
                            <?php print $arrPlaceHorario[$intDiaSemana]["hora_inicio_text"]?> - <?php print $arrPlaceHorario[$intDiaSemana]["hora_fin_text"]?>
                            &nbsp;
                            &nbsp;
                            <i class="fa fa-angle-down text-color-theme"></i>
                            <?php
                                    
                        }
                        
                    }
                        
                    ?>
                </div>
                
                <div class="accordion" id="accordionExample">
                    <div class="card " style="border: 0px;">
                        

                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body p-0 m-0">
                                <dl class="row mb-0 mt-4 lis-line-height-2 ">
                                    <?php
                                    
                                    if( count($arrPlaceHorario) > 0 ){
                                        
                                        while( $rTMP = each($arrPlaceHorario) ){
                                            ?>
                                            <dt class="col-6 text-left"><?php print $arrDiasSemana[$rTMP["key"]]["nombre"]?></dt>
                                            
                                            <?php
                                            if( $rTMP["value"]["cerrado"] == "Y" ){
                                                ?>
                                                <dd class="col-6 text-left text-danger">Cerrado</dd>
                                        
                                                <?php
                                            }
                                            else{
                                                ?>
                                                <dd class="col-6 text-left">
                                                    <?php print $rTMP["value"]["hora_inicio_text"];?> - <?php print $rTMP["value"]["hora_fin_text"];?>
                                                </dd>
                                        
                                                <?php
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                    ?>
                                </dl>
                            </div>
                        </div>
                    </div>
                
                </div>
                
            </div>
        </div>
        
        <?php
                
        $arrExplode = explode("?", $arrDatosPlace["url_youtube"]);
        
        $strKeyVideo = "";
        if( isset($arrExplode[1]) ){
            
            $arrExplode = explode("&", $arrExplode[1]);
            
            if( isset($arrExplode[0]) ){
                
                $arrExplode = explode("=", $arrExplode[0]);
            
                if( $arrExplode[0] == "v" ){
                    
                    $strKeyVideo = $arrExplode[1];
                    
                }
            
                    
            }                
            
        }                
        
        if( !empty($strKeyVideo) ){
            ?>
            <div class="row m-0 p-0">
                <div class="col-12 m-0 p-0 mt-2">
                    
                    <iframe style="width: 100%; height: auto;" src="https://www.youtube.com/embed/<?php print $strKeyVideo;?>?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <br>
                </div>
            </div>
            
            <?php
        }   
        
        ?>
        <div class="row m-0 p-0" style="">
            <div class="col-12 m-0 p-0 mt-2 text-right">
                
                <h5 class="text-left " data-toggle="collapse" href="#collapseExample"  aria-expanded="false" aria-controls="collapseExample" style="cursor: pointer;">
                    <?php print lang["opiniones"]?>
                    <i class="fa fa-angle-down"></i>
                </h5>
                
                <?php
                
                if( count($arrDatosPlaceComentario) > 0 ){
                    
                    ?>
                    <div class="collapse mb-1 show" id="collapseExample">
                        <div class="card card-body border-0 p-0 m-0">
                            
                            <?php
                            while( $rTMP = each($arrDatosPlaceComentario) ){
                                
                                ?>
                                <div class="row justify-content-center p-0 m-0">
                                    <div class="col-10 p-0 m-0">
                                        
                                        <h6 class="text-left" style="font-weight: 400;">"<?php print $rTMP["value"]["comentario"]?>"</h6>
                                        
                                    </div>
                                </div>
                                <div class="row justify-content-center ">
                                    <div class="col-10 p-0 m-0">
                                        
                                        <span style="font-size: 15px; font-weight: 200;"><?php print $rTMP["value"]["nombre"]?></span>
                                        <span style="font-size: 10px;"><?php print $rTMP["value"]["fecha"]?></span>
                                                                                
                                    </div>
                                </div>
                                <?php
                                
                            }
                            ?>
                            
                        </div>
                    </div>
                    <?php
                        
                }
                
                if( !$_SESSION["boolUserPanelLogIn"] && $boolVistaPublica ){
                    
                    ?>
                    <form name="frmComentario" id="frmComentario"  onsubmit="return false;" method="POST">
                    <input type="hidden" id="hidPlaceId" name="hidPlaceId" value="<?php print fntCoreEncrypt($intPlace)?>">
                    <textarea class="form-control form-control-sm" id="txtComentario" name="txtComentario" placeholder="<?php print lang["add_your_comment"]?>" rows="2"></textarea>
                    <button class="btn btn-sm text-background-theme text-white mt-1" id="btnComentar" onclick="fntShowComentarPlace();"><?php print lang["comentar"]?></button>
                    
                    <div id="divRecap" style="display: none;" class="mt-1">
                        
                        <center>
                            <div id="divRecapss" ></div>
                            
                        </center>
                    
                    </div>
                            
                    </form>
                    <script>
                        
                        function fntShowComentarPlace(){
                            
                            if( $("#txtComentario").val() == ""  ){
                                
                                swal("", "<?php print lang["add_your_comment"]?>", "");
                                    
                            }
                            else{
                                
                                $('#divRecap').show(); 
                                $('#btnComentar').hide()
                                
                            }
                            
                                
                        }
                        
                        var onloadCallback = function() {
                            grecaptcha.render('divRecapss', {
                              'sitekey' : '6LfJ7agUAAAAAEdlQUwhTyZ_XnWVkXHmYyZCgPdn',
                              'callback' : onloadCallbackSubmit
                            });
                          };
                          
                        function onloadCallbackSubmit(){
                            
                            var formData = new FormData(document.getElementById("frmComentario"));
                            
                            $(".preloader").fadeIn();
                            
                            $.ajax({
                                url: "placeup.php?setComentarioPlace=true", 
                                type: "POST",
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                dataType : "json",
                                success: function(result){
                                    
                                    $(".preloader").fadeOut();
                                    
                                    swal({   
                                        title: "",
                                        text: result["msn"],
                                        type: result["error"] == "true" ? "error" : "success",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true 
                                    },function(isConfirm){   
                                        
                                        if( result["error"] == "false" )
                                            location.reload();
                                    
                                    });
                            
                                }
                                        
                            });                            
                            
                        }
                        
                    </script>
                    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
                        async defer>
                    </script>
                    <?php
                    
                }
                                
                ?>
            </div>
        </div>
        <?php
                      
    }
    
    if( $intSeccion == 3 ){
        
        $objDBClass = new dbClass();
        $arrPlaceAmenidad = fntGetPlaceAmenidad($intPlace, true, $objDBClass);
        
        
        if( ( $_SESSION["boolUserPanelLogIn"] && !$boolVistaPublica ) || count($arrPlaceAmenidad) > 0 ){
            ?>
            <h5 class="text-right mt-2">
                Amenidades 
                <?php
        
                if( $_SESSION["boolUserPanelLogIn"] && !$boolPuclica ){
                    ?>
                    <span class="badge text-white temalugar_border_back" onclick="fntCargarModalSeccion('3');" style=" border-radius: 50%; cursor: pointer; ">
                        <i class="fa fa-pencil"></i>                        
                    </span>
                    <?php
                }
                
                ?>
                
            </h5>
            <?php
        }
        
        if( count($arrPlaceAmenidad) > 0 ){
            ?>
            <div class="card-body p-4">    
                <div class="row">
                                
                    <?php
                    
                    $intCount = 1;
                    while( $rTMP = each($arrPlaceAmenidad) ){
                        
                        if( $intCount == 1 ){
                            ?>
                            <div class="col-lg-4">
                                <ul class="list-unstyled lis-line-height-2 mb-0 text-right">

                            <?php
                        }
                        
                        ?>
                        <li>
                            <?php print $strLenguaje == "es"  ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?>
                            <i class="fa fa-check-square pl-2 text-color-theme "></i> 
                        </li>     
                        <?php
                        
                        if( $intCount == 4 ){
                            ?>
                                    </ul>
                            </div>
                            <?php
                            $intCount = 0;
                        }
                        
                        
                        $intCount++;                        
                    }
                    
                    if( $intCount < 4 ){
                        ?>
                        </ul>
                            </div>
                        <?php
                    }
                    
                    ?>         
                        
                    
                </div>
            <?php
        }
    
    }
        
    die();
}

if( isset($_GET["setComentarioPlace"]) ){
    
    $intPlace = isset($_POST["hidPlaceId"]) ? intval(fntCoreDecrypt($_POST["hidPlaceId"])) : "";
    $strComentario = isset($_POST["txtComentario"]) ? fntCoreClearToQuery($_POST["txtComentario"]) : "";
    $strTokenRecap = isset($_POST["g-recaptcha-response"]) ? fntCoreClearToQuery($_POST["g-recaptcha-response"]) : "";
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
    
    $arr["error"] = "true";
    $arr["msn"] = lang["datos_error_1"];
    
    if( !empty($strComentario) && !empty($strTokenRecap) && $intIdUsuario && $intPlace ){
        
        //Lo primerito, creamos una variable iniciando curl, pasándole la url
        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
         
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt ($ch, CURLOPT_POST, 1);
         
        //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
        curl_setopt ($ch, CURLOPT_POSTFIELDS, "secret=6LfJ7agUAAAAAOt0iGbsPjudsKSUX6PHiK1qq6ek&response=".$strTokenRecap);
         
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
         
        //recogemos la respuesta
        $crulrespuesta = curl_exec ($ch);
        
         
        //o el error, por si falla
        $error = curl_error($ch);
         
        //y finalmente cerramos curl
        curl_close ($ch);
        
        $arrRespuesta = json_decode($crulrespuesta);
        
        if( !$arrRespuesta->success ){
            
            $arr["msn"] = lang["error_captcha"];
        
            print json_encode($arr);            
            die();         
            
        }
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();  
    
        $strQuery = "INSERT INTO place_comentario(id_usuario, id_place, comentario)
                                           VALUES({$intIdUsuario}, {$intPlace}, '{$strComentario}')";
        $objDBClass->db_consulta($strQuery);
        
        $arr["error"] = "false";
        $arr["msn"] = lang["gracias_por_comentario"];
    
        $objDBClass->db_close();
        
    }    
    
    print json_encode($arr);
    die();
    
}

if( isset($_GET["setGaleriaPlace"]) ){
    
    $intPlace = isset($_POST["hidPlace"]) ? intval(fntCoreDecrypt($_POST["hidPlace"])) : 0;
    $strExcluirFile = isset($_POST["hidExcluirFile"]) ? trim($_POST["hidExcluirFile"]) : "";
    $arrExcluirFile = explode(",", $strExcluirFile);
    $arrExcluirFileFinal = array();
    while( $rTMP = each($arrExcluirFile) ){
        
        $arrExcluirFileFinal[$rTMP["value"]] = $rTMP["value"];
        
    }
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    $intMkTime = fntGetMkTime();
            
    while( $rTMP = each($_POST) ){
        
        $arrExplode = explode("_", $rTMP["key"]);
        
        if( $arrExplode[0] == "hidIdGaleria" ){
            
            $intPlaceGaleria = isset($_POST["hidIdGaleria_{$arrExplode[1]}"]) ? intval($_POST["hidIdGaleria_{$arrExplode[1]}"]) : 0;
            $intOrdenPlaceGaleria = isset($_POST["hidOrdenGaleria_{$arrExplode[1]}"]) ? intval($_POST["hidOrdenGaleria_{$arrExplode[1]}"]) : 0;
            $strTipoImagen = isset($_POST["hidTipoImagen_{$arrExplode[1]}"]) ? trim($_POST["hidTipoImagen_{$arrExplode[1]}"]) : "";
            $boolUpdate = isset($_POST["hidUpdate_{$arrExplode[1]}"]) && $_POST["hidUpdate_{$arrExplode[1]}"] == "Y" ? true : false;
            $boolDelete = isset($_POST["hidDelete_{$arrExplode[1]}"]) && $_POST["hidDelete_{$arrExplode[1]}"] == "Y" ? true : false;
            $boolInsert = isset($_POST["hidInsert_{$arrExplode[1]}"]) && $_POST["hidInsert_{$arrExplode[1]}"] == "Y" ? true : false;
            
            if( ( $boolDelete ) && $intPlaceGaleria ){
                
                $strQuery = "DELETE FROM place_galeria WHERE id_place_galeria = {$intPlaceGaleria}";
                $objDBClass->db_consulta($strQuery);
                $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".png";
                if( file_exists($strUrlArchivo) )
                    unlink($strUrlArchivo);
                    
            }
            else{
                
                if( $boolUpdate ){
                    
                    $strQuery = "DELETE FROM place_galeria WHERE id_place_galeria = {$intPlaceGaleria}";
                    $objDBClass->db_consulta($strQuery);
                    $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".png";
                    if( file_exists($strUrlArchivo) )
                        unlink($strUrlArchivo);
                    
                    $intPlaceGaleria = 0;   
                    
                }
                
                if( isset($_FILES["flFileImage_{$arrExplode[1]}"])  ){
                    
                        
                    $intCunt = isset($_FILES["flFileImage_{$arrExplode[1]}"]["size"][1]) && intval($_FILES["flFileImage_{$arrExplode[1]}"]["size"][1]) ? count($_FILES["flFileImage_{$arrExplode[1]}"]["name"]) : 1;
                    
                    for( $i = 0; $i < $intCunt; $i++ ){
                        
                        $strKey = $arrExplode[1]."-".$i; 
                        $boolFileImg64 = isset($_FILES["hidFileIMGflFileImage_{$strKey}"]) ? true : false;
                        $intFileImg64Orden = isset($_FILES["hidFileIMGflFileImageOrden_{$strKey}"]) ? $_FILES["hidFileIMGflFileImageOrden_{$strKey}"] : "1";
                        
                        if( ( $_FILES["flFileImage_{$arrExplode[1]}"]["size"][$i] > 0 || $boolFileImg64 || ( $_FILES["flFileImage_{$arrExplode[1]}"]["size"] > 0 && ($_FILES["flFileImage_{$arrExplode[1]}"]["tmp_name"][0]) != "" ) ) && !isset($arrExcluirFileFinal[$strKey]) ){
                            
                            if( !$intPlaceGaleria ){
                                
                                $strQuery = "INSERT INTO place_galeria(id_place)
                                                                VALUES({$intPlace})";
                                $objDBClass->db_consulta($strQuery);
                                $intPlaceGaleria = $objDBClass->db_last_id();
                                
                            }
                            
                            if( $boolFileImg64 ){
                                
                                $strFileName = "hidFileIMGflFileImage_{$strKey}";
                                $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".webp";
                                $strUrlArchivo = "../../file_inguate/place_interno/galeria/".fntCoreEncrypt($intPlaceGaleria).".webp";
                                rename($_FILES[$strFileName]["tmp_name"], $strUrlArchivo);
                                $intOrdenPlaceGaleria = $intFileImg64Orden; 
                                    
                            }
                            else{
                                
                                if( intval($_FILES["flFileImage_{$arrExplode[1]}"]["size"][$i]) > 0  ){
                                    
                                    $strFileName = "flFileImage_{$arrExplode[1]}";
                                    $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".webp";
                                    $strUrlArchivo = "../../file_inguate/place_interno/galeria/".fntCoreEncrypt($intPlaceGaleria).".webp";
                                    rename($_FILES[$strFileName]["tmp_name"][$i], $strUrlArchivo);
                                    
                                    if( $i >= 1 ){
                                        
                                        $intOrdenPlaceGaleria = isset($_POST["hidOrdenGaleria_".( + 1)]) ? intval($_POST["hidOrdenGaleria_".($arrExplode[1] + 1)]) : 0;
                                    }
                                        
                                    $intOrdenPlaceGaleria = $intOrdenPlaceGaleria; 
                                        
                                }
                                else{
                                    
                                    $strFileName = "flFileImage_{$arrExplode[1]}";
                                    $strUrlArchivo = "images/place/gallery/".fntCoreEncrypt($intPlaceGaleria).".webp";
                                    $strUrlArchivo = "../../file_inguate/place_interno/galeria/".fntCoreEncrypt($intPlaceGaleria).".webp";
                                    rename($_FILES[$strFileName]["tmp_name"], $strUrlArchivo);
                                    
                                }
                                
                            }
                            
                            
                            fntOptimizarImagen($strUrlArchivo, fntCoreEncrypt($intPlaceGaleria));
                            
                            $strQuery = "UPDATE place_galeria
                                         SET    url_imagen = '{$strUrlArchivo}',
                                                orden = '{$intOrdenPlaceGaleria}' 
                                         WHERE  id_place_galeria = {$intPlaceGaleria}";
                            $objDBClass->db_consulta($strQuery);
                            
                        }
                        
                        $intPlaceGaleria = 0;
                    }
                    
                            
                }
                
            }
                        
        }
        
    }
        
    die();
}

if( isset($_GET["getDrawSeccionProductoClasificacionPadre"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();  
    
    $intPlace = isset($_GET["place"]) ? intval(fntCoreDecrypt($_GET["place"])) : 0;
    $intPlaceProducto = isset($_GET["pp"]) ? intval(fntCoreDecrypt($_GET["pp"])) : 0;
    $intClasificacionPadre = isset($_GET["key"]) ? intval(($_GET["key"])) : 0;
    $boolPuclica = isset($_GET["pdd"]) && $_GET["pdd"] =="Y" ? true : false;
    
    $arrPlaceProducto = fntGetPlaceProducto($intPlace, $intClasificacionPadre, true, $objDBClass);
    $strHeightPX = "height: 130px;";
    if( count($arrPlaceProducto) > 0 ){
        
        $arrClasificacionPadreHijo = fntGetPlaceClasificacionPadreHijo($intPlace, $intClasificacionPadre, true, $objDBClass);
    
        $intCount = 1;
        while( $rTMP = each($arrPlaceProducto) ){
            
            ?>
            
            <div class="row p-0 m-0">
                <div class="col-lg-2 col-4 p-0 m-0 mt-3 text-right " id="divProducto_<?php print ($rTMP["key"])?>">
                        
                    <?php
                    
                    if( $rTMP["value"]["tipo"] == 1 ){
                        ?>
                        <a href="<?php print !empty($rTMP["value"]["logo_url"]) ? "imgInguate.php?t=up&src=".$rTMP["value"]["logo_url"] : "images/Noimagee.jpg?1"?>" data-toggle="lightbox">
                            <img style="width: 100%; <?php print $strHeightPX;?>  object-fit: cover; " src="<?php print !empty($rTMP["value"]["logo_url"]) ? "imgInguate.php?t=up&src=".$rTMP["value"]["logo_url"] : "images/Noimagee.jpg?1"?>" class="img-fluid rounded">
                        </a>
                        <?php
                    }
                    
                    if( $rTMP["value"]["tipo"] == 2 && isset($rTMP["value"]["galeria"]) ){
                    
                        ?>
                        <div id="carouselExampleIndicators_<?php print $intCount?>" class="carousel slide" data-ride="carousel" data-interval="1500">
                            <ol class="carousel-indicators">
                            
                            <?php
                            
                            $intCountSlide = 0;
                            while( $arrTMP = each($rTMP["value"]["galeria"]) ){
                                ?>
                                <li data-target="#carouselExampleIndicators_<?php print $intCount?>" data-slide-to="<?php print $intCountSlide?>" class="<?php print $intCountSlide == 0 ? "active" : "" ?> "></li>
                                <?php
                                $intCountSlide++;
                            }
                            
                            ?>
                            </ol>
                            <div class="carousel-inner">
                                
                                <?php
                                
                                reset($rTMP["value"]["galeria"]);
                                $intCountSlide = 0;
                                while( $arrTMP = each($rTMP["value"]["galeria"]) ){
                                    ?>
                                    <div class="carousel-item <?php print $intCountSlide == 0 ? "active" : "" ?>">
                                    
                                        <a href="<?php print "imgInguate.php?t=up&src=".$arrTMP["value"]["url_imagen"]?>" data-toggle="lightbox" data-gallery="galley_<?php print $intCount?>">
                                            <img class=" img-fluid" src="<?php print "imgInguate.php?t=up&src=".$arrTMP["value"]["url_imagen"]?>" style="width: 100%;  <?php print $strHeightPX?> object-fit: cover;">
                                        </a>
                                    </div>
                                    <?php
                                    $intCountSlide++;
                                }
                                
                                ?>
                                
                            </div> 
                            <a class="carousel-control-prev" href="#carouselExampleIndicators_<?php print $intCount?>" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators_<?php print $intCount?>" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>                             
                        </div>    
                        <?php
                        
                    }
                    
                    ?>    
                      
                    
                      
                </div>
                <div class="col-lg-10 col-8 p-0 m-0 pl-3 mt-3  pb-1">
                    
                    <div class="row m-0 p-0">
                        <div class="col-12 m-0 p-0">
                            <h5 class="text-dark text-left lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                <?php 
                                
                                print $strLenguaje == "es" ? $rTMP["value"]["nombre"] : ( !empty($rTMP["value"]["nombre_en"]) ? $rTMP["value"]["nombre_en"] : $rTMP["value"]["nombre"] );
                                                   
                                
                                if( $_SESSION["boolUserPanelLogIn"]  && !$boolPuclica ){
                                    ?>
                                    <span onclick="fntShowModalProductoClasificacionPadre('<?php print ($intClasificacionPadre)?>', '<?php print ($rTMP["key"])?>');" class="badge  temalugar_border_back" style=" border-radius: 50%; cursor: pointer; border: 2px solid #<?php print $strColorHex?>; ">
                                        <i class="fa fa-pencil"></i>                        
                                    </span>
                                    
                                    <?php
                                }
                                ?>
                            </h5>
                        
                        </div>
                    </div>
                        
                    <h5 class=" text-secondary text-left m-0 p-0" style="font-size: 14px;">
                        
                        <?php
                    
                        if( $rTMP["value"]["tipo"] == 1 ){
                            
                            print $strLenguaje == "es" ? $rTMP["value"]["descripcion"] : ( !empty($rTMP["value"]["descripcion_en"]) ? $rTMP["value"]["descripcion_en"] : $rTMP["value"]["descripcion"] );                  
                    
                        }
                        
                        if( $rTMP["value"]["tipo"] == 2 && isset($rTMP["value"]["galeria"]) ){
                            
                            ?>
                            <br>
                            <span onclick="fntShowProductoEspecial('<?php print fntCoreEncrypt($rTMP["key"])?>')" class="badge  text-white text-background-theme " style="cursor: pointer;"><?php print lang["ver_mas_detalles"]?></span>
                            <br>
                            <br>
                            <?php
                            
                        }    
                        ?>
                            
                                      
                    </h5>
                    <h5 class="text-left m-0 p-0 text-color-theme" style="font-size: 14px;">
                        Q <?php print $rTMP["value"]["precio"];?>             
                    </h5>
                    <h3 class="text-left  m-0 p-0" style="font-size: 16px;">
                        
                        <?php
                        reset($arrClasificacionPadreHijo);                                                
                        while( $arrTMP = each($arrClasificacionPadreHijo) ){
                            
                            if( isset($rTMP["value"]["clasificacion_padre_hijo"][$arrTMP["key"]]) ){
                                
                                ?>
                                <span class="badge  text-white text-background-theme" style="">
                                    <?php print $strLenguaje == "es" ? $arrTMP["value"]["tag_es"] : $arrTMP["value"]["tag_en"]?>                        
                                </span>
                                <?php
                                
                            }                                            
                            
                        }                            
                        
                        ?>
                    </h3>
                
                </div>
            </div>
            
            <?php
            $intCount++;
        }
        
        ?>
        <script>
            $('.carousel').carousel();
            
            <?php
            
            if( intval($intPlaceProducto) && isset($arrPlaceProducto[$intPlaceProducto]) ){
                ?>
                
                setTimeout(function(){ 
                    
                    
                    $('html, body').animate({
                       'scrollTop':   $('#divProducto_<?php print ($intPlaceProducto)?>').offset().top - 100
                    }, 500); 
                
                }, 2500);

                    
                <?php
            }
            
            ?>
            
            
            
        </script>
        <?php
            
    }
        
    die();
}

if( isset($_GET["setHeaderWeb"]) ){
    
    fntDrawHeaderPrincipal(1);
      
    die();    
    
}

if( isset($_GET["setHeaderMovil"]) ){
    
    $strColorHex = isset($_GET["color_hex"]) ? $_GET["color_hex"] : "";
    fntShowBarNavegacionFooter(true, $strColorHex);
      
    die();    
    
}

if( isset($_GET["viewSession"]) ){
    
    $arr["sesion"] = isset(sesion["logIn"]) && sesion["logIn"] ? true : false;
    
    if( ( isset($_SESSION["_open_antigua"]["core"]["tipo"]) && $_SESSION["_open_antigua"]["core"]["tipo"] == 1  ) || ( isset($_SESSION["_open_antigua"]["core"]["login"]) && $_SESSION["_open_antigua"]["core"]["login"] 
        && $_SESSION["_open_antigua"]["core"]["id_usuario"] == $arrDatosPlace["id_usuario"] ) ){
        
        //$_SESSION["boolUserPanelLogIn"] = true;
            
    }
    else{
        
        //$_SESSION["boolUserPanelLogIn"] = false;
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $strPublico = "Y";
        $intIdUsuarioLogeado = 0;
        if( sesion["logIn"] ){
            
            $intIdUsuarioLogeado = fntCoreDecrypt(sesion["webToken"]);
            $intIdUsuarioLogeado = intval($intIdUsuarioLogeado);
            
            $strPublico = !$intIdUsuarioLogeado ? "Y" : "N";
            
        }
        
        
        $strQuery = "INSERT INTO place_visita(id_place, id_usuario, publico, id_place_producto)
                                       VALUES({$intPlace}, {$intIdUsuarioLogeado}, '{$strPublico}', {$intPlaceProducto})";
        $objDBClass->db_consulta($strQuery);
        $objDBClass->db_close();   
        
        
    }
      
    print json_encode($arr);
    die();    
    
}

if( $intPlace ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $arrDatosPlace = fntGetPlace($intPlace, true, $objDBClass);
    $arrCatalogoFondo = fntCatalogoFondoPlacePublico();
    
    $strColorHex = isset($arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"]) ? $arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"] : $arrCatalogoFondo[1]["color_hex"];

    
    $arrPlaceClasificacionPadre = array();
    $strQuery = "SELECT clasificacion_padre.id_clasificacion_padre,
                        clasificacion_padre.id_clasificacion,
                        clasificacion_padre.nombre,
                        clasificacion_padre.tag_en,
                        clasificacion_padre.tag_es
                 FROM   place_clasificacion_padre,
                        clasificacion_padre
                 WHERE  place_clasificacion_padre.id_place = {$intPlace} 
                 AND    place_clasificacion_padre.id_clasificacion_padre = clasificacion_padre.id_clasificacion_padre
                 ";
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["tag_en"] = $rTMP["tag_en"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["tag_es"] = $rTMP["tag_es"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion"] = $rTMP["id_clasificacion"];
        $arrPlaceClasificacionPadre[$rTMP["id_clasificacion_padre"]]["producto"] = isset($arrPlaceClasificacion[$rTMP["id_clasificacion"]]) ? $arrPlaceClasificacion[$rTMP["id_clasificacion"]]["producto"] : "Y";
            
    }   
    
    $objDBClass->db_free_result($qTMP); 
    
    if( $arrDatosPlace["estado"] == "R" ){
        
        $strQuery = "SELECT sol_place_motivo.motivo
                     FROM   sol_place,
                            sol_place_motivo
                     WHERE  sol_place.id_place = {$intPlace}
                     AND    sol_place.id_sol_place = sol_place_motivo.id_sol_place    ";
                     
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        $arrDatosPlace["motivo_rechazo"] = isset($rTMP["motivo"]) ? $rTMP["motivo"] : "";
        
        
    }
    
    
}

fntDrawHeaderPublico($boolMovil, false, false, $arrDatosPlace["titulo"]);    

if( ( isset($_SESSION["_open_antigua"]["core"]["tipo"]) && $_SESSION["_open_antigua"]["core"]["tipo"] == 1  ) || ( isset($_SESSION["_open_antigua"]["core"]["login"]) && $_SESSION["_open_antigua"]["core"]["login"] 
    && $_SESSION["_open_antigua"]["core"]["id_usuario"] == $arrDatosPlace["id_usuario"] ) ){
    
    $_SESSION["boolUserPanelLogIn"] = true;
        
}
else{
    
    $_SESSION["boolUserPanelLogIn"] = false;
    
    $strPublico = "Y";
    $intIdUsuarioLogeado = 0;
    if( sesion["logIn"] ){
        
        $intIdUsuarioLogeado = fntCoreDecrypt(sesion["webToken"]);
        $intIdUsuarioLogeado = intval($intIdUsuarioLogeado);
        
        $strPublico = !$intIdUsuarioLogeado ? "Y" : "N";
        
    }
    
    $strQuery = "INSERT INTO place_visita(id_place, id_usuario, publico, id_place_producto)
                                   VALUES({$intPlace}, {$intIdUsuarioLogeado}, '{$strPublico}', {$intPlaceProducto})";
    $objDBClass->db_consulta($strQuery);
        
    
    
}


?>   
<link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
<link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> 
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 

<link rel="stylesheet" type="text/css" href="dist/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="dist/slick/slick-theme.css"/>

<link href="dist/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet"> 
<link href="dist/lightbox-master/dist/ekko-lightbox.css" rel="stylesheet"> 

    

<input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intPlace);?>">
<input type="hidden" id="hidKeyRegistroCore" value="<?php print fntCoreEncrypt($intPlace);?>">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">

<style>
        
    .temalugar_border_back{
        border: 2px solid #<?php print $strColorHex?>; 
        color: #<?php print $strColorHex?> !important;    
        background: white;
    }    
    .text-background-theme {
        background-color: #<?php print $strColorHex?> !important;
        
    }
    .text-color-theme {
        color: #<?php print $strColorHex?> !important;
    
    }

    .btn-theme-circular {
        background-color: white;
        color: #<?php print $strColorHex?>;
        border: 2px solid #<?php print $strColorHex?>;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
        position:absolute;
        top:0%;
        right:2%;
        z-index: 99;

    }
     
    <?php
    
    if( false ){
        ?>
        .container__ {
      display: flex;
      justify-content: space-around;
      align-items: flex-start;
      height: 100%;
    }
        
    .police {
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    } 
        <?php
    }
    
    ?>       
    
                    
    .fondo {
     -webkit-box-sizing: content-box;
      -moz-box-sizing: content-box;
      box-sizing: content-box;
      padding: 54px 55px 55px;
      border: none;
      -webkit-border-radius: 0 0 0 2000px / 0 0 0 230px;
      border-radius: 0 0 0 2000px / 0 0 0 230px;
      font: normal 16px/1 "Times New Roman", Times, serif;
      color: black;
      -o-text-overflow: ellipsis;
      text-overflow: ellipsis;
     background: -moz-linear-gradient(top, rgba(46,57,117,0.83) 0%, rgba(46,57,117,0.83) 1%, rgba(41,52,119,0.93) 40%, rgba(39,50,119,0.95) 53%, rgba(39,50,119,1) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='red', endColorstr='blue',GradientType=0 ); 
    }
    
</style>
<div class="container-fluid m-0 p-0" >
    
    <div class="row m-0 p-0">
    
        <div class="col-12 p-0 m-0 text-center" >
            <!--
            <div class="fondo" style="">
                   
            
            </div>
              -->
        
            <img src="<?php print isset($arrCatalogoFondo[$arrDatosPlace["id_tema"]]["url_fondo"]) ? $arrCatalogoFondo[$arrDatosPlace["id_tema"]]["url_fondo"] : $arrCatalogoFondo[1]["url_fondo"]?>" style="width: 100%; height: auto;">
                   
            <div class="m-0 p-0 pt-2 pt-lg-4  text-center" style="position: absolute; top: 0px; width: 100%;  "> 
                
                <?php
                
                fntDrawHeaderPrincipal(1, true, $objDBClass);
                
                ?>        
                
                <!-- Contenido -->       
                <div class="row p-0 m-0 justify-content-center mt-3" style="">
                    <div class="col-12 p-0 m-0 ">
                        
                        <?php
                        
                        if( $_SESSION["boolUserPanelLogIn"] && !$boolVistaPublica ){
                            ?>
                            <div class="btn-theme-circular" onclick="fntCargarModalSeccion('1')">
                                &nbsp;<i class="fa fa-pencil"></i>&nbsp;
                            </div>
                            <?php
                        }
                        
                        ?>
                             
                        
                        <div id="divContenidoSeccion_1">
                            
                        </div>
                                                
                    </div>
                </div>
                
                <div class="row p-0 m-0 pl-1 justify-content-center <?php print $boolMovil ? "" : "container__"?>" style="">
                    
                    <div class="col-12 col-lg-4 p-1 m-0 bg-white <?php print $boolMovil ? "" : "police"?>" style="" id="divContenidoSeccion_2">
                        
                           
                    </div>
                    
                    <div class="col-12 col-lg-8 p-0 m-0 pl-1 pr-1 bg-white" style="">
                        
                        
                        <div class="row p-0 m-0 ">
                            <div class="col-12 p-0 m-0">
                                
                                <?php
                                
                                while( $rTMP = each($arrPlaceClasificacionPadre) ) {
                                    
                                    ?>
                                    <h5 class="text-left mt-2">
                                        <?php print $rTMP["value"]["texto"]?>
                                        
                                        <?php
                                    
                                        if( $_SESSION["boolUserPanelLogIn"] && !$boolVistaPublica ){
                                            ?>
                                            <span onclick="fntShowModalProductoClasificacionPadre('<?php print ($rTMP["key"])?>', '<?php print (0)?>');" class="badge  " style=" border-radius: 50%; cursor: pointer; border: 2px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>;">
                                                <i class="fa fa-pencil"></i>                        
                                            </span>
                                            
                                            <?php
                                        }
                                        
                                        ?>
                                        
                                            
                                    </h5>
                                    <div class="divContenidoClasificaionPadre" id="divContenidoClasificacionPadreo_<?php print ($rTMP["key"])?>">
                                        <?php
                                        /*
                                        for( $i = 1; $i <= 2 ; $i ++ ){
                                            ?>
                                            <div class="row p-0 m-0">
                                                <div class="col-lg-3 col-4 p-0 m-0 mt-3 text-right ">
                                                    <img style="width: 100%; height: 100%;" src="images/place/gallery/logo_lA==.png" class="img-fluid ">
                                                
                                                </div>
                                                <div class="col-lg-7 col-8 p-0 m-0 pl-3 mt-3">
                                                    <h5 class="text-dark text-left lis-line-height-1 m-0 p-0" style="font-size: 20px;">
                                                        Huevos Estrellados                    
                                                        
                                                    </h5>
                                                    <h5 class="  text-left m-0 p-0" style="font-size: 14px;">
                                                        Huevos Estrellados con frijoles                
                                                    </h5>
                                                    <h5 class="text-left m-0 p-0 text-color-theme" style="font-size: 14px;">
                                                        Q 40.00                
                                                    </h5>
                                                    <h3 class="text-left  m-0 p-0" style="font-size: 16px;">
                                                        <span class="badge  text-white" style="background-color: #3E53AC;">
                                                            Breakfast                        
                                                        </span>
                                                                                    
                                                        <span class="badge  text-white" style="background-color: #3E53AC;">
                                                            Brunch                        
                                                        </span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        */
                                        ?>
                                    </div>
                                    <?php
                                      
                                    
                                     
                                }
                                
                                ?>      
                                    
                                    
                                
                                
                                <div class="row justify-content-end p-0 m-0" id="divContenidoSeccion_3">
                                    
                                </div>
                                
                                <?php
                                
                                if( !empty($arrDatosPlace["direccion"]) ){
                                    ?>
                                    <div class="row p-0 m-0">
                                        <div class="col-12">  
                                                          
                                            <button class="btn btn-sm btn-outline btn-outline-light " onclick="location.href = 'https://maps.google.com/?q=<?php print $arrDatosPlace["direccion"]?>'" style="border:  1px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>;" >
                                                Google Maps <i class="fa fa-paper-plane"></i>
                                            </button>

                                            <button class="btn btn-sm btn-outline btn-outline-light " onclick="fntSetWaze();" style="border:  1px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>;" >
                                                Waze <i class="fa fa-rocket"></i>
                                            </button>
                                            <br>
                                            <br>

                                            <?php
                                            
                                            if( !empty($arrDatosPlace["direccion"]) ){
                                                $strApiKey = "AIzaSyABJ9worirb3vAoxbghUZj_rNUTrfTtRPY";

                                                ?>
                                                
                                                <iframe style="width: 100%; height: 450px; border: 0px;"  src="https://www.google.com/maps/embed/v1/place?key=<?php print $strApiKey?>&q=<?php print $arrDatosPlace["direccion"]?>" allowfullscreen></iframe>
                                             
                                                <?php
                                            }
                                            
                                            ?>
                                            
                                            
                                        </div>
                                    </div>
                                    <?php
                                }
                                
                                ?>
                                    
                                    
                                
                                
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                
                <?php
                        
                fntDrawFooter($strColorHex);
                
                ?>
                
            </div>
                   
        </div>
    </div>
    
    <?php
        
    fntShowBarNavegacionFooter(true, isset($arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"]) ? $arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"] : $arrCatalogoFondo[1]["color_hex"]);
    
    ?>
    
</div>

<!-- Modal -->
<div class="modal fade" id="modalGeneral" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg" style="min-width: 90%;" role="document">
        <div class="modal-content" id="modalGeneraContenido">
        
            <div class="modal-header" id="modalGeneralHeader">
            
                <h5 class="modal-title">Modal title</h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
            </div>
            
            <div class="modal-body" id="modalGeneralBody">
                <br>
            </div>
            
            <div class="modal-footer" id="modalGeneralFooter">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            
            </div>
        
        </div>
    </div>
</div>

<?php
                            
if( $_SESSION["boolUserPanelLogIn"] && !$boolVistaPublica ){
    ?>
    <div class="flotante_change_theme" >
        
        <?php
        
        while( $rTMP = each($arrCatalogoFondo) ){
            ?>
            
            <div class="badge " onclick="fntSetFondoPlace('<?php print $rTMP["key"]?>');" style="background-color: #<?php print $rTMP["value"]["color_hex"];?> ; cursor: pointer">&nbsp;&nbsp;</div>
        
            <?php
        }
        
        
        //if( $arrDatosPlace["estado"] == "C" || $arrDatosPlace["estado"] == "R" ) {
        if( false ) {
            
            ?>
                 
            <button onclick="fntCargarModalSeccion('5')" class="btn btn-sm btn-secondary" style="background-color: white; border: 2px solid #<?php print $strColorHex?>; color: #<?php print $strColorHex?>; "><?php print lang["enviar_solicitud"]?></button>
            
            <?php
            
        }
        ?>
            
    </div>
    <?php
}

?>

    
   
<script type="text/javascript" src="dist/slick/slick.min.js"></script>
<script src="dist/clockpicker/jquery-clockpicker.min.js"></script>
<script src="dist/js/jsAntigua.js"></script>
<script type="text/javascript" src="dist_interno/select2/js/select2.min.js"></script>
<script type="text/javascript" src="dist/lightbox-master/dist/ekko-lightbox.js"></script>
   
<script src="dist/imageLoaded/imagesloaded.pkgd.min.js"></script>
     
        
<style>
    
    .flotante_change_theme {
        
        position:absolute;
        top: 1%;
        left:1%;
    
    }

    .slick-center-Border {
        
        padding: 0px !important;
        
    }
    
    .flotante {
            
        display:none;
        position:fixed;
        top:90%;
        right:2%;
        background-color: #<?php print $strColorHex?>;
        color: white;
        padding: 10px;
        border-radius: 50%;
        cursor: pointer;
    
    }
</style>
<script >
           
    var d = new Date();
    var intDiaSemana = d.getDay()
        
    $(document).ready(function() {
        
        <?php
        
        if( $boolVistaPublica ){
            ?>
            
            $.ajax({
                url: "placeup.php?viewSession=true&p=<?php print fntCoreEncrypt($intPlace)?>&pro="+getParameterByName('pro'), 
                dataType : "json",
                async : false,
                success: function(result){
                    
                    if( !result["sesion"] ){
                        
                        
                        location.href = "index?bref=<?php print $arrDatosPlace["url_place"]?>";
                                               
                    }   
                    else{
                        
                        $.ajax({
                            url: "placeup.php?setHeaderWeb=true",
                            async : false,
                            success: function(result){
                                
                                $("#divHeaderPrincipaWeb").html(result);
                                
                            }
                        });
                        
                        $.ajax({
                            url: "placeup.php?setHeaderMovil=true&color_hex=<?php print isset($arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"]) ? $arrCatalogoFondo[$arrDatosPlace["id_tema"]]["color_hex"] : $arrCatalogoFondo[1]["color_hex"]?>", 
                            async : false,
                            success: function(result){
                                
                                $("#divBarraNavegacionFooter").html(result);
                                
                            }
                        });
                        
                    }                     
                    
                }
                
            });
            
                        
            
            <?php
        }
        
        
        ?>
              
        fntDrawSeccion(1);
        fntDrawSeccion(2);
        fntDrawSeccion(3);
        
        $(".divContenidoClasificaionPadre").each(function (){
            
            arrExplode = this.id.split("_");
            
            fntDrawSeccionProductoClasificacionPadre(arrExplode[1]);
            
        }); 
        
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });    
        
        <?php
        
        if( $arrDatosPlace["estado"] == "C" ) {
            
            ?>
            
            //swal("", "<?php print lang["alerta_negocio_publicado"]?>");
                                
            <?php
            
        }
        
        if( $arrDatosPlace["estado"] == "R" ) {
            
            ?>
            
            swal("", "<?php print lang["alerta_negocio_rechazo"]?> <?php print isset($arrDatosPlace["motivo_rechazo"]) ? $arrDatosPlace["motivo_rechazo"] : ""?> ");
                                
            <?php
            
        }
        
        
        ?>        
        
    });
    
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function initializeMap() {

        var mapOptions = {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: center
        };

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

             //console.log(position);    
            var markerOptions = { 
                position: center, 
                draggable: true
            }
            
            markerInicial = new google.maps.Marker(markerOptions);
            markerInicial.setMap(map);
            
            map.setCenter(center);
            map.setZoom(16);
            
            markerInicial.addListener('dragend', function(){
                
                $("#hidLatUbicacion").val(markerInicial.position.lat());
                $("#hidLngUbicacion").val(markerInicial.position.lng());
            
            });
                                      
            $("#hidLatUbicacion").val(<?php print $arrDatosPlace["ubicacion_lat"];?>);
            $("#hidLngUbicacion").val(<?php print $arrDatosPlace["ubicacion_lng"];?>);
            
            
        
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          draggable: true
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          else{
              markerInicial.setMap(null);
          }

          
          //console.log(place);
          //console.log(place.place_id);
          $("#hidPlaceId").val(place.place_id);
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            
            $("#hidLatUbicacion").val(place.geometry.location.lat);
            $("#hidLngUbicacion").val(place.geometry.location.lng);
            
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
            $("#hidLatUbicacion").val(place.geometry.location.lat);
            $("#hidLngUbicacion").val(place.geometry.location.lng);
            
          }
          
          
          //console.log($("#hidLatUbicacion").val());
          //console.log($("#hidLngUbicacion").val());
            
          
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
          
          marker.addListener('dragend', function(){
                
                $("#hidLatUbicacion").val(marker.position.lat());
                $("#hidLngUbicacion").val(marker.position.lng());
            
            });
          
          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
           
            
        
    }
              
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
    }
    
    function fntDrawSeccionProductoClasificacionPadre(strKey){
        
        $.ajax({
            url: "placeup.php?getDrawSeccionProductoClasificacionPadre=true&pdd=<?php print $boolVistaPublica ? "Y" : "N"?>&place=<?php print fntCoreEncrypt($intPlace);?>&pp=<?php print fntCoreEncrypt($intPlaceProducto);?>&key="+strKey, 
            success: function(result){
                
                $("#divContenidoClasificacionPadreo_"+strKey).html(result);   
                
            }
        });
        
    }
    
    function fntDrawSeccion(intSeccion, strKey){
        
        strKey = !strKey ? "" : strKey;
                        
        $.ajax({
            url: "placeup.php?fntDrawSeccion=true&place=<?php print fntCoreEncrypt($intPlace);?>&pdd=<?php print $boolVistaPublica ? "Y" : "N"?>&&seccion="+intSeccion+"&key="+strKey+"&diaS="+intDiaSemana, 
            success: function(result){
                
                $("#divContenidoSeccion_"+intSeccion).html(result);
                
            }
        });
        
    }
    
    function fntShowProductoEspecial(strProducto, strProductoCla){
        
        <?php
        
        if( $_SESSION["boolUserPanelLogIn"] && !$boolVistaPublica ) {
            
            ?>
            
            var strHeightPx = ( $(window).height() * ( 75 / 100 ) )+'px';
            
            $.ajax({
                url: "placeup.php?drawProductoEspecial=true&key="+strProducto+"&strHeightPx="+strHeightPx+"&strProductoCla="+strProductoCla, 
                success: function(result){
                    
                    $("#modalGeneral").html(result);
                    $("#modalGeneral").modal("show");
                        
                    
                }
            });
            
            <?php
                        
        }
        else{
            
            ?>
            
            window.open("proes.php?p="+strProducto)
            
            <?php
            
        }
        
        ?>
        
    }
                
    function fntSetWaze(){
        
        location.href = "https://waze.com/ul?q=<?php print $arrDatosPlace["direccion"]?>";
        
    }
          
    <?php
    
    if( $_SESSION["boolUserPanelLogIn"] && !$boolVistaPublica ){
        ?>
        
        function setPaginaPublica(){
            
            $.ajax({
                url: "placeup.php?p=<?php print fntCoreEncrypt($intPlace)?>&publico=Y",
                async : false, 
                success: function(result){
                    
                    var formData = new FormData();
                    formData.append("data", result);
                    formData.append("hidPlace", '<?php print fntCoreEncrypt($intPlace)?>');
                        
                    $.ajax({
                        url: "servicio_core.php?servicio=setPaginaPublica", 
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result){
                            
                                
                        }
                                
                    });
                
                }
            });
        
            
        }
              
        function fntShowModalProductoClasificacionPadre(strKey, strKey2){
                                   
            $.ajax({
                url: "placeup.php?getShowModalProductoClasificacionPadre=true&place=<?php print fntCoreEncrypt($intPlace);?>&key="+strKey+"&strKey2="+strKey2, 
                success: function(result){
                    
                    $("#modalGeneral").html(result);
                    $("#modalGeneral").modal("show");
                    
                }
            });
            
        }
        
        function fntCargarModalSeccion(intSeccion, strKey){
            
            strKey = !strKey ? "" : strKey;
                            
            $.ajax({
                url: "placeup.php?fntDrawSeccionForm=true&place=<?php print fntCoreEncrypt($intPlace);?>&&seccion="+intSeccion+"&key="+strKey, 
                success: function(result){
                    
                    $("#modalGeneral").html(result);
                    $("#modalGeneral").modal("show");
                    
                }
            });
            
        }
        
        function fntSetUbicacionPlace(){
                
            $.ajax({
                url: "placeup.php?setUbicacionPlace=true&place=<?php print fntCoreEncrypt($intPlace);?>&lat="+$("#hidLatUbicacion").val()+"&lng="+$("#hidLngUbicacion").val(), 
                success: function(result){
                    
                    swal({
                        title: "Data set",
                        //text: "Select An Option",
                        type: "success",
                        showConfirmButton: true,
                        //timer: 2000
                    });    
                
                }
            });
            
            
        }
        
        function fntSetFondoPlace(intKeyCatalogo){
            
            var formData = new FormData();
            formData.append("intFondoPlace", intKeyCatalogo);                    
            formData.append("hidPlace", "<?php print fntCoreEncrypt($intPlace)?>");
                                
            $(".preloader").fadeIn();
            $.ajax({
                url: "placeup.php?setFondoPlace=true", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    setPaginaPublica();
                    location.href = "placeup.php?p=<?php print fntCoreEncrypt($intPlace)?>&len=<?php $strLenguaje?>";
                    
                }
                        
            });
            
        }
        
        
        <?php
    }
     
    ?>
    
</script>
<?php
fntDrawFooterPublico($strColorHex); 
?>
