<?php
$strAction = basename($_SERVER["PHP_SELF"]);
include "core/core.php";


if( count($_GET) > 0 ){

    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "registro" ){
        
        include "clases/registro/registro_controller.php";
        $objController = new registro_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "place" ){
        
        include "clases/place/place_controller.php";
        $objController = new place_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "categoria" ){
        
        include "clases/categoria/categoria_controller.php";
        $objController = new categoria_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "amenidad" ){
        
        include "clases/amenidad/amenidad_controller.php";
        $objController = new amenidad_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "sol_place" ){
        
        include "clases/sol_place/sol_place_controller.php";
        $objController = new sol_place_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "categoria_especial" ){
        
        include "clases/categoria_especial/categoria_especial_controller.php";
        $objController = new categoria_especial_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    
    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "sol_categoria_especial" ){
        
        include "clases/sol_categoria_especial/sol_categoria_especial_controller.php";
        $objController = new sol_categoria_especial_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    if( $_SESSION["_open_antigua"]["core"]["pagina"] == "lugar_dato" ){
        
        include "clases/lugar_dato/lugar_dato_controller.php";
        $objController = new lugar_dato_controller();
        $objController->getAjax($strAction);
        
        die();
    }
    
    
}



fntDrawHeaderPage();


//fntDrawHeaderContenido("", $strAction);

fntDrawFooterPage();
?>
<script>

$(document).ready(function() { 
    
    fntCargarContenidoPagina('<?php print $_SESSION["_open_antigua"]["core"]["pagina"]?>', '<?php print $_SESSION["_open_antigua"]["core"]["pagina_nombre"]?>');
    
});

</script>
<?php
    
die();
?>


<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="dist/images/favicon.ico">
            
        <!-- Material Design for Bootstrap fonts and icons -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

        <!-- Material Design for Bootstrap CSS -->
        <link rel="stylesheet" href="dist_interno/bootstrap-material-design-dist/css/bootstrap-material-design.min.css" >

        <title>OpenAntigua</title>
        
    </head>
    <body >

        <style>
            
            body, html {
                height: 100%;
            }

            .flex-grow {
                flex: 1 0 auto;
            }

            .bmd-layout-canvas {
              flex-grow: 1;
            }
            
        </style>

        <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay" >
            <header class="bmd-layout-header">
                <div class="row ">
                    <div class="col col-1 text-left">
                        <button class="navbar-toggler text-secondary m-1" style="border: 1px solid rgba(0,0,0,.1);" type="button" data-toggle="drawer" data-target="#dw-p1">
                            <span class="sr-only">Toggle drawer</span>
                            <i class="material-icons">menu</i>
                        </button>
                    </div>
                    <div class="col col-9 text-left pt-1">
                        <h3 class="align-bottom">titulo</h3>                        
                    </div>  
                    <div class="col col-2 text-right  ">
                        <div class="dropdown pull-xs-right m-1">
                          <button class="btn bmd-btn-icon dropdown-toggle active" type="button" id="lr1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                          </button>
                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="lr1">
                            <button class="dropdown-item" type="button">Action</button>
                            <button class="dropdown-item" type="button">Another action</button>
                            <button class="dropdown-item disabled" type="button">Disabled action</button>
                            <div class="dropdown-divider"> </div>
                            <a class="dropdown-item" href="#">Separated link</a>
                          </div>
                        </div>
                    </div>
                </div>
                            
            </header>
            <div id="dw-p1" class="bmd-layout-drawer bg-faded">
                <header>
                    <a class="navbar-brand">Title</a>
                </header>
                <ul class="list-group">
                    <a class="list-group-item">Link 1</a>
                    <a class="list-group-item">Link 2</a>
                    <a class="list-group-item">Link 3</a>
                </ul>
            </div>
          
            <!-- Contenidi por Pagina  -->
            <main class="bmd-layout-content bg-dark" >
                <div class="container-fluid bg-danger " >
                    
                <div class="row justify-content-md-center">
                    <div class="col col-lg-2">
                        <button type="button" class="btn btn-raised btn-dark" data-toggle="modal" data-target="#exampleModal">
                          Launch demo modal
                        </button>
                    </div>
                    <div class="col-md-auto">
                        Variable width content
                    </div>
                    <div class="col col-lg-2">
                        3 of 3
                    </div>
                    </div>
                    <div class="row">
                    <div class="col">
                    1 of 3
                    </div>
                    <div class="col-md-auto">
                    Variable width content
                    </div>
                    <div class="col col-lg-2">
                    3 of 3
                    </div>
                </div>
                
                  
                  
                  
                </div>
            </main>
            
            
                  
                  <button type="button" class="btn btn-secondary" data-timeout="1000" data-toggle="snackbar" data-content="Free fried chicken here! <a href='https://example.org' class='btn btn-info'>Check it out</a>" data-html-allowed="true" data-timeout="0">
                  Snackbar
                </button>
          
            <!-- Espacio para Modal  -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>

        <script src="https://cdn.rawgit.com/FezVrasta/snackbarjs/1.1.0/dist/snackbar.min.js"></script>

        <script src="dist_interno/bootstrap-material-design-dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
        <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
    </body>
</html>