<?php
  
if( !isset($_SERVER["HTTPS"]) && $_SERVER["HTTP_HOST"] != "localhost" && $_SERVER["HTTP_HOST"] != "192.168.1.54" ){
    header('Location: https://inguate.com/');
    exit;   
}
 
$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}
include "core/function_servicio.php";

if( isset($_GET["setlogOut"]) ){
    
    setcookie( "inguate", "", time()+(60*60*24*30) );
    session_destroy();
    die();
}
/*    
$arrCookie = explode(",", $_COOKIE["inguate"]);
print_r($arrCookie);
print("<br>");
print_r($_SESSION);
die();
*/
$strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";

if( isset($_GET["drawRegistroIndexPagina"]) ){
    
    $strLinkRef = isset($_GET["bref"]) ? $_GET["bref"] : "";
    $strCodigoConfirmacionUsuario = isset($_GET["cc"]) ? $_GET["cc"] : "";
    $intTrafico = isset($_GET["tt"]) ? $_GET["tt"] : "";
    
    fntDrawRowContenidoModal($boolMovil, $strLinkRef, $strCodigoConfirmacionUsuario, $intTrafico);
            
    die();
}

if( isset($_GET["drawCodigoAreaFormulario"]) ){
    
    fntDrawContenidoCodigoArea();
            
    die();
}


    
if( isset($_GET["getAutoSearch"]) ){
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strParametroBusqueda = isset($_GET["q"]) ? trim($_GET["q"]) : "";
    $boolRetornoArray = isset($_GET["i"]) && $_GET["i"] == "true" ? true : false;
    $strParametroBusqueda = fntCoreStringQueryLetra($strParametroBusqueda);
    $strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";
    define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
    $intUbicacion = sesion["ubicacion"];
    $arrCatalogoUbicacionLugar1 = fntUbicacionLugar();
    $strKeyUbicaionLugar = "";
    while( $rTMP = each($arrCatalogoUbicacionLugar1[$intUbicacion]["lugar"]) ){
        
        $strKeyUbicaionLugar .= ( $strKeyUbicaionLugar == "" ? "" : "," ).$rTMP["key"];
        
    }
    
    $strTextoBusquedaPlace =  "";
    
    $arrSeparacionString = explode(" ", $strParametroBusqueda);
    
    while( $rTMP = each($arrSeparacionString) ){
        
        $strTextoBusquedaPlace .= ( $strTextoBusquedaPlace == "" ? "" : " OR " )." ( texto_autocomplete LIKE '%{$rTMP["value"]}%' )  ";
            
    }
            
    $strQuery = "SELECT id_clasificacion identificador,
                        '' identificador_2,
                        nombre,
                        tag_en,
                        tag_es,
                        '1' tipo
                 FROM   clasificacion
                 WHERE  sinonimo LIKE '%{$strParametroBusqueda}%' 
                 
                 UNION ALL 
                         
                 SELECT clasificacion_padre.id_clasificacion_padre identificador,
                        '' identificador_2,
                        clasificacion_padre.nombre,
                        CONCAT(clasificacion.tag_en, ' / ', clasificacion_padre.tag_en)  tag_en,
                        CONCAT(clasificacion.tag_es, ' / ', clasificacion_padre.tag_es)  tag_es,
                        '2' tipo
                 FROM   clasificacion_padre,
                        clasificacion
                 WHERE  clasificacion_padre.sinonimo LIKE '%{$strParametroBusqueda}%' 
                 AND    clasificacion_padre.id_clasificacion = clasificacion.id_clasificacion
                 
                 UNION ALL
                 
                 SELECT id_clasificacion_padre_hijo identificador,
                        '' identificador_2,
                        nombre,
                        tag_en,
                        tag_es,
                        '3' tipo
                 FROM   clasificacion_padre_hijo
                 WHERE  clasificacion_padre_hijo.sinonimo LIKE '%{$strParametroBusqueda}%' 
                 
                 UNION ALL 
                       
                 SELECT id_place identificador,
                        url_place identificador_2,
                        titulo nombre,
                        titulo tag_en,
                        titulo tag_es,
                        '5' tipo
                 FROM   place
                 WHERE  ( {$strTextoBusquedaPlace} ) 
                 AND    estado IN('A')
                 
                 UNION ALL
                 
                 SELECT id_categoria_especial identificador,
                        '' identificador_2,
                        nombre nombre,
                        tag_en tag_en,
                        tag_es tag_es,
                        '7' tipo
                 FROM   categoria_especial
                 WHERE  sinonimo LIKE '%{$strParametroBusqueda}%'
                 AND    estado IN('A')
                 
                 ORDER BY nombre
                ";
    
    $arrRetorno = array();
    
    $strKeyClasificacion = "";
    $strKeyClasificacionPadre = "";
    
    $arrClasificacion = array();
    $arrClasificacionPadre = array();
    $arrClasificacionPadreHijo = array();
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        //Key Para buscar Place
        if( $rTMP["tipo"] == "1" ){
            
            $arrClasificacion[$rTMP["identificador"]] = $rTMP["identificador"];
            
            $strKey = $rTMP["tipo"]."||".$rTMP["identificador"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["identificador"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            //$arrRetorno[$strKey]["texto_corto"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
                                            
        }
        
        if( $rTMP["tipo"] == "2" ){
            
            $arrClasificacionPadre[$rTMP["identificador"]] = $rTMP["identificador"];
            $strKey = $rTMP["tipo"]."||".$rTMP["identificador"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["identificador"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            //$arrRetorno[$strKey]["texto_corto"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
            
        }
        
        if( $rTMP["tipo"] == "3" ){
            
            $arrClasificacionPadreHijo[$rTMP["nombre"]][$rTMP["identificador"]] = $rTMP["identificador"];
            
            $strKey = $rTMP["tipo"]."||".$rTMP["nombre"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            //$arrRetorno[$strKey]["texto_corto"] = $rTMP["nombre"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
            
        }
               
        if( $rTMP["tipo"] == "4" || $rTMP["tipo"] == "5" || $rTMP["tipo"] == "7" ){
            
            $strKey = $rTMP["tipo"]."||".$rTMP["identificador"]; 
            
            $arrRetorno[$strKey]["identificador"] = $rTMP["identificador"];
            $arrRetorno[$strKey]["identificador_2"] = $rTMP["identificador_2"];
            $arrRetorno[$strKey]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["texto_corto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            $arrRetorno[$strKey]["tipo"] = $rTMP["tipo"];
                                            
        }
        
    }
    $objDBClass->db_free_result($qTMP);
    
    //drawdebug($arrRetorno);
    
    $arrDatosClasificacionRango = array();
    $arrDatosClasificacionPadreRango = array();
    if( count($arrClasificacion) > 0 || count($arrClasificacionPadre) > 0 ){
        
        $strKeyClasificacion = implode(",", $arrClasificacion);
        $strKeyClasificacionPadre = implode(",", $arrClasificacionPadre);
        
        $strFiltroKeyClasificacion = !empty($strKeyClasificacion) ? "AND    place_clasificacion.id_clasificacion IN({$strKeyClasificacion})" : "";
        $strFiltroKeyClasificacionPadre = !empty($strKeyClasificacionPadre) ? "AND    place_clasificacion_padre.id_clasificacion_padre IN({$strKeyClasificacionPadre})" : "";
        
        $strQuery = "SELECT place.id_place,
                            place.titulo,
                            place.precio_mediana_bajo,
                            place.precio_mediana_alto,
                            place_clasificacion.id_clasificacion,
                            place_clasificacion_padre.id_clasificacion_padre
                     FROM   place,
                            place_clasificacion,
                            place_clasificacion_padre
                     WHERE  place.id_ubicacion_lugar IN({$strKeyUbicaionLugar})
                     AND    place.estado = 'A'
                     AND    place.id_place = place_clasificacion.id_place
                     AND    place.id_place = place_clasificacion_padre.id_place
                     {$strFiltroKeyClasificacion}
                     {$strFiltroKeyClasificacionPadre}
                     ";
        $arrDatosClasificacion = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrDatosClasificacion[$rTMP["id_clasificacion"]][$rTMP["id_place"]]["precio_mediana_bajo"] = $rTMP["precio_mediana_bajo"];
            $arrDatosClasificacion[$rTMP["id_clasificacion"]][$rTMP["id_place"]]["precio_mediana_alto"] = $rTMP["precio_mediana_alto"];
            
            $arrDatosClasificacionPadre[$rTMP["id_clasificacion_padre"]][$rTMP["id_place"]]["precio_mediana_bajo"] = $rTMP["precio_mediana_bajo"];
            $arrDatosClasificacionPadre[$rTMP["id_clasificacion_padre"]][$rTMP["id_place"]]["precio_mediana_alto"] = $rTMP["precio_mediana_alto"];
            
            //////////////////////////////////////////////////////////////////////////////////
            //Key Clasificacion    
            //////////////////////////////////////////////////////////////////////////////////  
            if( !isset($arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"]) )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"] = $rTMP["precio_mediana_bajo"];
                
            if( $rTMP["precio_mediana_bajo"] <= $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"] )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["min"] = $rTMP["precio_mediana_bajo"];
            
            if( !isset($arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"]) )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"] = $rTMP["precio_mediana_alto"];
                
            if( $rTMP["precio_mediana_alto"] >= $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"] )
                $arrDatosClasificacionRango[$rTMP["id_clasificacion"]]["max"] = $rTMP["precio_mediana_alto"];
            
            
            //////////////////////////////////////////////////////////////////////////////////
            //Key Clasificacion    
            //////////////////////////////////////////////////////////////////////////////////  
            if( !isset($arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"]) )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"] = $rTMP["precio_mediana_bajo"];
                
            if( $rTMP["precio_mediana_bajo"] <= $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"] )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["min"] = $rTMP["precio_mediana_bajo"];
            
            if( !isset($arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"]) )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"] = $rTMP["precio_mediana_alto"];
                
            if( $rTMP["precio_mediana_alto"] >= $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"] )
                $arrDatosClasificacionPadreRango[$rTMP["id_clasificacion_padre"]]["max"] = $rTMP["precio_mediana_alto"];
            
                
            
        }
        $objDBClass->db_free_result($qTMP);
        
        while( $rTMP = each($arrDatosClasificacionRango) ){
            
            $sinMin = $rTMP["value"]["min"];
            $sinMax = $rTMP["value"]["max"];
            
            $arrDatosClasificacionRango[$rTMP["key"]]["precio_min"] = $sinMin; 
            $arrDatosClasificacionRango[$rTMP["key"]]["precio_max"] = $sinMax; 
                
            $sinDiferencia = $sinMax - $sinMin;
                
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrDatosClasificacionRango[$rTMP["key"]]["tipo"] = 1; 
                                                                                   
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["min"] = $sinMin; 
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["max"] = $sinMin + $intRango; 
                           
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["min"] = $sinMin + $intRango + 1; 
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                                
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["max"] = $sinMax; 
                                
            }   
            else{
                
                $arrDatosClasificacionRango[$rTMP["key"]]["tipo"] = $sinMin != $sinMax ? 2 : 3;
                
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["min"] = 0;
                $arrDatosClasificacionRango[$rTMP["key"]]["bajo"]["max"] = 0;
                           
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["min"] = $sinMin;
                $arrDatosClasificacionRango[$rTMP["key"]]["medio"]["max"] = $sinMax;
                                
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["min"] = 0;
                $arrDatosClasificacionRango[$rTMP["key"]]["alto"]["max"] = 0;
                
            }
            
        }
        
        while( $rTMP = each($arrDatosClasificacionPadreRango) ){
            
            $sinMin = $rTMP["value"]["min"];
            $sinMax = $rTMP["value"]["max"];
            
            $arrDatosClasificacionPadreRango[$rTMP["key"]]["precio_min"] = $sinMin; 
            $arrDatosClasificacionPadreRango[$rTMP["key"]]["precio_max"] = $sinMax; 
                
            $sinDiferencia = $sinMax - $sinMin;
                
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["tipo"] = 1; 
                                                                                   
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["min"] = $sinMin; 
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["max"] = $sinMin + $intRango; 
                           
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["min"] = $sinMin + $intRango + 1; 
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["max"] = $sinMax; 
                                
            }   
            else{
                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["tipo"] = $sinMin != $sinMax ? 2 : 3;
                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["min"] = 0;
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["bajo"]["max"] = 0;
                           
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["min"] = $sinMin;
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["medio"]["max"] = $sinMax;
                                
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["min"] = 0;
                $arrDatosClasificacionPadreRango[$rTMP["key"]]["alto"]["max"] = 0;
                
            }
            
        }
        
    }
    
    $arrClasificacionPadreHijoRango = array();
    if( count($arrClasificacionPadreHijo) > 0 ){
        
        $strNombreClasificacionPadreHijo = "";
        while( $rTMP = each($arrClasificacionPadreHijo) ){
            
            $strNombreClasificacionPadreHijo .= ( $strNombreClasificacionPadreHijo == "" ? "" : "," )."'".$rTMP["key"]."'";    
            
        }
        
        $strQuery = "SELECT id_ubicacion_rango_producto,
                            id_ubicacion_lugar,
                            nombre,
                            precio_min,
                            precio_max
                     FROM   ubicacion_rango_producto
                     WHERE  nombre IN ({$strNombreClasificacionPadreHijo})
                     AND    id_ubicacion_lugar IN({$strKeyUbicaionLugar})";
        
        $arrClasificacionPadreHijoRango = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $sinMin = $rTMP["precio_min"];
            $sinMax = $rTMP["precio_max"];
            
            $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["precio_min"] = $sinMin; 
            $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["precio_max"] = $sinMax; 
                
            $sinDiferencia = $sinMax - $sinMin;
                
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["tipo"] = 1; 
                                                                                   
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["min"] = $sinMin; 
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["max"] = $sinMin + $intRango; 
                           
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["min"] = $sinMin + $intRango + 1; 
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["max"] = $sinMax; 
                                
            }   
            else{
                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["tipo"] = $sinMin != $sinMax ? 2 : 3;
                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["min"] = 0;
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["bajo"]["max"] = 0;
                           
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["min"] = $sinMin;
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["medio"]["max"] = $sinMax;
                                
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["min"] = 0;
                $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["alto"]["max"] = 0;
                
            }
            
              
                    
        }    
        $objDBClass->db_free_result($qTMP);
        
        
    }
    
    
    $arrResultado = array();
    if( count($arrRetorno) > 0 ){
        
        while( $rTMP = each($arrRetorno) ){
            
            if( $rTMP["value"]["tipo"] == "1" ){
                
                if( isset($arrDatosClasificacion[$rTMP["value"]["identificador"]]) ){
                    
                    $arrRetorno[$rTMP["key"]]["rango"] = $arrDatosClasificacionRango[$rTMP["value"]["identificador"]]; 
                                    
                }
                
            }
            
            if( $rTMP["value"]["tipo"] == "2" ){
                
                if( isset($arrDatosClasificacionPadreRango[$rTMP["value"]["identificador"]]) ){
                    
                    $arrRetorno[$rTMP["key"]]["rango"] = $arrDatosClasificacionPadreRango[$rTMP["value"]["identificador"]]; 
                                    
                }
                
            }
            
            if( $rTMP["value"]["tipo"] == "3" ){
                
                $arrKey = explode("||", $rTMP["key"]);
                $strNombreClasificacionPadreHijo = $arrKey[1];
                
                if( isset($arrClasificacionPadreHijoRango[$strNombreClasificacionPadreHijo]) ){
                    
                    $arrRetorno[$rTMP["key"]]["rango"] = $arrClasificacionPadreHijoRango[$strNombreClasificacionPadreHijo];
                   
                }
                
            }
            
            
        }
        
        
        reset($arrRetorno);
        while( $rTMP = each($arrRetorno) ){
            
            $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
            $arrTMP["identificador_2"] = $rTMP["value"]["tipo"] == "5" || $rTMP["value"]["tipo"] == "4" ? $rTMP["value"]["identificador_2"] : fntCoreEncrypt($rTMP["value"]["identificador_2"]);
            $arrTMP["texto"] = $rTMP["value"]["texto"];
            $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
            $arrTMP["tipo"] = $rTMP["value"]["tipo"];
            $arrTMP["rango"] = "all";
            
            array_push($arrResultado, $arrTMP);
            
            if( isset($rTMP["value"]["rango"]) ){
                
                if( $rTMP["value"]["rango"]["tipo"] == 1 ){
                    
                    
                    //...........Alto
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["alto"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["alto"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["alto"]["max"], 2);
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["rango"] = $rTMP["value"]["rango"]["tipo"]."_A";
                    
                    if( $boolRetornoArray ){
                        
                        $arrTMP["min"] = $rTMP["value"]["rango"]["alto"]["min"];
                        $arrTMP["max"] = $rTMP["value"]["rango"]["alto"]["max"];
                        
                    }
                    
                    array_push($arrResultado, $arrTMP);
                
                
                    //...........Medio
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["medio"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["medio"]["max"], 2);
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["rango"] = $rTMP["value"]["rango"]["tipo"]."_M";
                    
                    if( $boolRetornoArray ){
                        
                        $arrTMP["min"] = $rTMP["value"]["rango"]["medio"]["min"];
                        $arrTMP["max"] = $rTMP["value"]["rango"]["medio"]["max"];
                        
                    }
                    
                    array_push($arrResultado, $arrTMP);
                
                    
                    //...........BAJO
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["bajo"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["bajo"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["bajo"]["max"], 2);
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["rango"] = $rTMP["value"]["rango"]["tipo"]."_B";
                    
                    if( $boolRetornoArray ){
                        
                        $arrTMP["min"] = $rTMP["value"]["rango"]["bajo"]["min"];
                        $arrTMP["max"] = $rTMP["value"]["rango"]["bajo"]["max"];
                        
                    }
                    
                    array_push($arrResultado, $arrTMP);
                                            
                }
                else{
                    
                    //...........Medio
                    $arrTMP["identificador"] = fntCoreEncrypt($rTMP["value"]["identificador"]);
                    $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q ".number_format($rTMP["value"]["rango"]["medio"]["min"], 2)." - Q ".number_format($rTMP["value"]["rango"]["medio"]["max"], 2);
                    $arrTMP["texto_corto"] = $rTMP["value"]["texto_corto"];
                    $arrTMP["tipo"] = $rTMP["value"]["tipo"];
                    
                    array_push($arrResultado, $arrTMP);
                    
                }
                
            }
        
        }
        
    }
    else{
        
        $arrTMP["identificador"] = "";
        $arrTMP["identificador_2"] = "";
        $arrTMP["texto"] = "Ver todos los resultados para {$strParametroBusqueda}...";
        $arrTMP["texto_corto"] = $strParametroBusqueda;
        $arrTMP["tipo"] = "6";
        $arrTMP["rango"] = "all";
        
        array_push($arrResultado, $arrTMP);
        
            
    } 
    
    //drawdebug($arrResultado);
    print json_encode($arrResultado);
    die(); 
     /*       
    drawdebug($arrDatosClasificacionRango);
    drawdebug("------------------------------------------------------------------------------------------------------------------------------------");
    drawdebug($arrDatosClasificacionPadreRango);
    drawdebug("------------------------------------------------------------------------------------------------------------------------------------");
    drawdebug($arrClasificacionPadreHijoRango);               
    */
    
    die();         
    $arrTMP["identificador"] = 0;            
    $arrTMP["texto"] = $strParametroBusqueda." <br>";    
    $arrTMP["texto_corto"] = $strParametroBusqueda;    
    $arrTMP["tipo"] = 0;    
    $arrTMP["rango"] = "1_M";    
    
    //array_push($arr, $arrTMP);
    
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $strTexto = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
        
        if( $rTMP["tipo"] == "3" )
            $strTexto .= " ({$rTMP["contandor"]})";
        
        //$strTexto .= " ({$rTMP["tipo"]})";

        $arrTMP["identificador"] = fntCoreEncrypt($rTMP["identificador"]);            
        $arrTMP["texto"] = $strTexto;    
        $arrTMP["texto_corto"] = trim($strTexto);    
        $arrTMP["tipo"] = $rTMP["tipo"];    
        $arrTMP["rango"] = "1_M";    
        
        array_push($arr, $arrTMP);       
        
        //Rangos
        if( $rTMP["tipo"] == 1 || $rTMP["tipo"] == 2  ){
            
            //Normal
            if( $rTMP["tipo_rango_precio"] == 1 ){
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["identificador"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["bajo"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["bajo_min"]} - Q{$rTMP["bajo_max"]} ";    
                $arrTMP["texto_corto"] = $strTexto;    
                $arrTMP["tipo"] = $rTMP["tipo"];    
                $arrTMP["rango"] = $rTMP["tipo_rango_precio"]."_B"; 
                
                array_push($arr, $arrTMP);
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["identificador"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["medio_min"]} - Q{$rTMP["medio_max"]} ";    
                $arrTMP["texto_corto"] = $strTexto;    
                $arrTMP["tipo"] = $rTMP["tipo"];    
                $arrTMP["rango"] = $rTMP["tipo_rango_precio"]."_M";    
                
                array_push($arr, $arrTMP);       
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["identificador"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["alto"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["alto_min"]} - Q{$rTMP["alto_max"]} ";    
                $arrTMP["texto_corto"] = $strTexto;    
                $arrTMP["tipo"] = $rTMP["tipo"];    
                $arrTMP["rango"] = $rTMP["tipo_rango_precio"]."_A";    
                
                array_push($arr, $arrTMP);       
                
                        
            }
            
            
            //Rango Corto
            if( $rTMP["tipo_rango_precio"] == 2 ){
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["identificador"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["medio_min"]} - Q{$rTMP["medio_max"]} ";    
                $arrTMP["texto_corto"] = $strTexto;
                $arrTMP["tipo"] = $rTMP["tipo"];    
                $arrTMP["rango"] = $rTMP["tipo_rango_precio"]."_M";    
                
                array_push($arr, $arrTMP);
                    
            }
            
            
            //Rango Igual
            if( $rTMP["tipo_rango_precio"] == 3 ){
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["identificador"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["medio_min"]} - Q{$rTMP["medio_max"]} ";    
                $arrTMP["texto_corto"] = $strTexto;
                $arrTMP["tipo"] = $rTMP["tipo"];    
                $arrTMP["rango"] = $rTMP["tipo_rango_precio"]."_M";    
                
                array_push($arr, $arrTMP);    
            }
            
        } 
                
    }
    $objDBClass->db_free_result($qTMP);
    
    
    
    die();
    
    //Agrupacion para calsificacion padre hijo
    $strQuery = "SELECT nombre,
                        tag_en,
                        tag_es,
                        tipo_rango_precio,
                        bajo_min,
                        bajo_max,
                        medio_min,
                        medio_max,
                        alto_min,
                        alto_max
                 FROM   clasificacion_padre_hijo
                 WHERE  tipo_rango_precio IN (1,2,3)
                 AND    sinonimo LIKE '%{$strParametroBusqueda}%'
                 ORDER BY nombre
                 ";   
    $arrAgrupacion = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
    
        $arrAgrupacion[$rTMP["nombre"]]["tag"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
        
        $arrTMP = array();
        
        if( $rTMP["bajo_min"] > 0 ) array_push($arrTMP, $rTMP["bajo_min"]);
        if( $rTMP["bajo_max"] > 0 ) array_push($arrTMP, $rTMP["bajo_max"]);
        if( $rTMP["medio_min"] > 0 ) array_push($arrTMP, $rTMP["medio_min"]);
        if( $rTMP["medio_max"] > 0 ) array_push($arrTMP, $rTMP["medio_max"]);
        if( $rTMP["alto_min"] > 0 ) array_push($arrTMP, $rTMP["alto_min"]);
        if( $rTMP["alto_max"] > 0 ) array_push($arrTMP, $rTMP["alto_max"]);
        
        $sinMaximoColumna = max($arrTMP);
        $sinMinimoColumna = min($arrTMP);
            
        if( !isset($arrAgrupacion[$rTMP["nombre"]]["min"]) )
            $arrAgrupacion[$rTMP["nombre"]]["min"] = $sinMinimoColumna;
            
        if( $sinMinimoColumna <= $arrAgrupacion[$rTMP["nombre"]]["min"] )
            $arrAgrupacion[$rTMP["nombre"]]["min"] = $sinMinimoColumna;
            
        if( !isset($arrAgrupacion[$rTMP["nombre"]]["max"]) )
            $arrAgrupacion[$rTMP["nombre"]]["max"] = $sinMaximoColumna;
            
        if( $sinMaximoColumna >= $arrAgrupacion[$rTMP["nombre"]]["max"] )
            $arrAgrupacion[$rTMP["nombre"]]["max"] = $sinMaximoColumna;
            
    }
    $objDBClass->db_free_result($qTMP);
    
    if( count($arrAgrupacion) > 0 ){
        
        while( $rTMP = each($arrAgrupacion) ){
            
            $sinDiferencia = $rTMP["value"]["max"] - $rTMP["value"]["min"];
            
            if( $sinDiferencia > 15 ){
                
                $intRango = $sinDiferencia / 3;
                $intRango = intval($intRango) ;
                
                $arrAgrupacion[$rTMP["key"]]["tipo"] = 1; 
                                                                                   
                $arrAgrupacion[$rTMP["key"]]["bajo"]["min"] = $rTMP["value"]["min"]; 
                $arrAgrupacion[$rTMP["key"]]["bajo"]["max"] = $rTMP["value"]["min"] + $intRango; 
                           
                $arrAgrupacion[$rTMP["key"]]["medio"]["min"] = $rTMP["value"]["min"] + $intRango + 1; 
                $arrAgrupacion[$rTMP["key"]]["medio"]["max"] = $rTMP["value"]["min"] + ( $intRango * 2 ); 
                                
                $arrAgrupacion[$rTMP["key"]]["alto"]["min"] = $rTMP["value"]["min"] + ( $intRango * 2 ) + 1; 
                $arrAgrupacion[$rTMP["key"]]["alto"]["max"] = $rTMP["value"]["max"]; 
                                
            }   
            else{
                
                $arrAgrupacion[$rTMP["key"]]["tipo"] = $rTMP["value"]["min"] != $rTMP["value"]["max"] ? 2 : 3;
                
                $arrAgrupacion[$rTMP["key"]]["bajo"]["min"] = 0;
                $arrAgrupacion[$rTMP["key"]]["bajo"]["max"] = 0;
                           
                $arrAgrupacion[$rTMP["key"]]["medio"]["min"] = $rTMP["value"]["min"];
                $arrAgrupacion[$rTMP["key"]]["medio"]["max"] = $rTMP["value"]["max"];
                                
                $arrAgrupacion[$rTMP["key"]]["alto"]["min"] = 0;
                $arrAgrupacion[$rTMP["key"]]["alto"]["max"] = 0;
                
            }                     
            
        }  
        
        reset($arrAgrupacion);
        
        while( $rTMP = each($arrAgrupacion) ){
            
            $arrTMP["identificador"] = fntCoreEncrypt($rTMP["key"]);            
            $arrTMP["texto"] = $rTMP["value"]["tag"];    
            $arrTMP["texto_corto"] = trim($rTMP["key"]);    
            $arrTMP["tipo"] = 3;    
            $arrTMP["rango"] = "N";    
            
            array_push($arr, $arrTMP);  
            
            //Normal
            if( $rTMP["value"]["tipo"] == 1 ){
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["key"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["bajo"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["value"]["bajo"]["min"]} - Q{$rTMP["value"]["bajo"]["max"]} ";    
                $arrTMP["texto_corto"] = $rTMP["value"]["tag"];    
                $arrTMP["tipo"] = 3;    
                $arrTMP["rango"] = $rTMP["value"]["tipo"]."_B"; 
                
                array_push($arr, $arrTMP);
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["key"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["value"]["medio"]["min"]} - Q{$rTMP["value"]["medio"]["max"]} ";    
                $arrTMP["texto_corto"] = $rTMP["value"]["tag"];    
                $arrTMP["tipo"] = 3;    
                $arrTMP["rango"] = $rTMP["value"]["tipo"]."_M";    
                
                array_push($arr, $arrTMP);       
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["key"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["alto"]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["value"]["alto"]["min"]} - Q{$rTMP["value"]["alto"]["max"]} ";    
                $arrTMP["texto_corto"] = $rTMP["key"];    
                $arrTMP["tipo"] = 3;    
                $arrTMP["rango"] = $rTMP["value"]["tipo"]."_A";    
                
                array_push($arr, $arrTMP);       
                
                        
            }
            
            
            //Rango Corto
            if( $rTMP["value"]["tipo"] == 2 ){
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["key"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["value"]["medio"]["min"]} - Q{$rTMP["value"]["medio"]["max"]} ";    
                $arrTMP["texto_corto"] = $rTMP["value"]["tag"];
                $arrTMP["tipo"] = 3;    
                $arrTMP["rango"] = $rTMP["value"]["tipo"]."_M";    
                
                array_push($arr, $arrTMP);
                    
            }
            
            
            //Rango Igual
            if( $rTMP["value"]["tipo"] == 3 ){
                
                $arrTMP["identificador"] = fntCoreEncrypt($rTMP["key"]);            
                $arrTMP["texto"] = "&nbsp;&nbsp; ".lang["medio"]." &nbsp;&nbsp;&nbsp;&nbsp;Q{$rTMP["value"]["medio"]["min"]} - Q{$rTMP["value"]["medio"]["max"]} ";    
                $arrTMP["texto_corto"] = $rTMP["key"];
                $arrTMP["tipo"] = 3;    
                $arrTMP["rango"] = $rTMP["value"]["tipo"]."_M";    
                
                array_push($arr, $arrTMP);    
            }
            
            
                  
        }
                
    }
    
    print json_encode($arr);
    die();
    
}



$boolInterno = isset($_GET["interno"]) ? true : false;

$intOrigen = isset($_GET["o"]) ? intval($_GET["o"]) : 0;
$intKey = isset($_GET["key"]) ? fntCoreDecrypt($_GET["key"]) : 0;
$intTipo = isset($_GET["t"]) ? $_GET["t"] : 0;
$strTexto = isset($_GET["q"]) ? trim($_GET["q"]) : 0;
$strRango = isset($_GET["ran"]) ? trim($_GET["ran"]) : 0;
$strFiltro = isset($_GET["fil_tipo"]) ? trim($_GET["fil_tipo"]) : 0;
$strFiltroKey = isset($_GET["fil_key"]) ? trim($_GET["fil_key"]) : 0;

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

fntDrawHeaderPublico($boolMovil, false, true);


?>   
<link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
<link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> 
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>


<input type="hidden" id="hidOrigen" value="<?php print $intOrigen;?>">
<input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intKey);?>">
<input type="hidden" id="hidTipo" value="<?php print $intTipo;?>">
<input type="hidden" id="hidTexto" value="<?php print $strTexto;?>">
<input type="hidden" id="hidRango" value="<?php print $strRango;?>">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">



<style>

.fondoasdf {
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    padding: 54px 55px 55px;
    border: none;
    -webkit-border-radius: 0 0 0 2000px / 0 0 0 230px;
    border-radius: 0 0 0 2000px / 0 0 0 230px;
    font: normal 16px/1 "Times New Roman", Times, serif;
    color: black;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis;
    background: -moz-linear-gradient(top, rgba(46,57,117,0.83) 0%, rgba(46,57,117,0.83) 1%, rgba(41,52,119,0.93) 40%, rgba(39,50,119,0.95) 53%, rgba(39,50,119,1) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='red', endColorstr='blue',GradientType=0 ); 
}
.fondo {
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    padding: 150px 55px 55px;
    border: none;
    -webkit-border-radius: 0 0 0 2000px / 0 0 0 230px;
    border-radius: 0 0 0 2000px / 0 0 0 230px;
    font: normal 16px/1 "Times New Roman", Times, serif;
    color: black;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis;
    background: -moz-linear-gradient(top, rgba(46,57,117,0.83) 0%, rgba(46,57,117,0.83) 1%, rgba(41,52,119,0.93) 40%, rgba(39,50,119,0.95) 53%, rgba(39,50,119,1) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(46,57,117,0.83) 0%,rgba(46,57,117,0.83) 1%,rgba(41,52,119,0.93) 40%,rgba(39,50,119,0.95) 53%,rgba(39,50,119,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='red', endColorstr='blue',GradientType=0 ); 
}
    
</style>

<?php

$boolInterno = true;
if( $boolInterno ){
    
    $arrCatalogoFondo = fntCatalogoFondoPlacePublico();
    
    
    
    
    ?>
    
    <div class="container-fluid m-0 p-0 content " style="" id="divContendedorPagina">
    
        <div class="row m-0 p-0">
        
            <div class="col-12 p-0 m-0 text-center " id="">
            
            <!--
                <img src="<?php print  $arrCatalogoFondo[1]["url_fondo"]?>" style="width: 100%; height: auto;">
                      -->
                       
                <div class="m-0 p-0 pt-2 pt-lg-4  text-center " style=""> 
                           
                    <?php
                    
                    include "core/dbClass.php";
                    $objDBClass = new dbClass();
            
                    $arrAlerta = fntGetNotificacionUsuario(true, $objDBClass, true);
                
                    fntDrawHeaderPrincipal(2, true, $objDBClass, $arrAlerta);
                    $arrUbicacion = fntUbicacion(true, $objDBClass);
                    $arrCategoriaEspecial = fntGetCatalogoCategoriaEspecial(true, $objDBClass);
                    
                    $strTextoBusqueda = "Restaurants";
                    $strUrl =  "{$_SERVER["REQUEST_SCHEME"]}://".$_SERVER["SERVER_NAME"]."".$_SERVER["SCRIPT_NAME"]."?getAutoSearch=true&q={$strTextoBusqueda}&i=true";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$strUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $arrRespuestaRestaurante = json_decode(curl_exec($ch));
                    curl_close($ch);
                    
                    $strTextoBusqueda = "Lodgings";
                    $strUrl =  "{$_SERVER["REQUEST_SCHEME"]}://".$_SERVER["SERVER_NAME"]."".$_SERVER["SCRIPT_NAME"]."?getAutoSearch=true&q={$strTextoBusqueda}&i=true";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$strUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $arrRespuestaAlojamiento = json_decode(curl_exec($ch));  
                    curl_close($ch);
                    
                    ////////////////////////////////////
                    ////////////////////////////////////
                    ////////////////////////////////////
                    //   Tiene que cambiar por Tours cuando se Agrege la categoria
                    ////////////////////////////////////
                    ////////////////////////////////////
                    ////////////////////////////////////
                    
                    $strTextoBusqueda = "Lodgings";
                    $strUrl =  "{$_SERVER["REQUEST_SCHEME"]}://".$_SERVER["SERVER_NAME"]."".$_SERVER["SCRIPT_NAME"]."?getAutoSearch=true&q={$strTextoBusqueda}&i=true";
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$strUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $arrRespuestaTour = json_decode(curl_exec($ch));
                    curl_close($ch);
                    
                    $arrCatalogoDescuentoOferta = array();
                    $arrCatalogoDescuentoOferta[1]["url_foto"] = "https://scontent.fgua5-1.fna.fbcdn.net/v/t1.0-9/64577344_10156354149455976_8493814413621460992_n.jpg?_nc_cat=109&_nc_eui2=AeFDflblFb2kGz1fc_FNQqFF2aAPlPxIGr7HuPRr4X6UaNfaZ2eI-SXoMHBJwpJh9nigBl2gzbPGMFarRFq1xE-hB6KmuAXIf0NDHniLhomYRg&_nc_oc=AQkr2iZ81tA9hWqcPZUahDyl06Jvh-YYYvhgImfd4TE7kOvn_SnOJj8e3ICWLiyiwnWXw_P-7EaOLUKhRR3MYCUm&_nc_ht=scontent.fgua5-1.fna&oh=b567f1fd36d5d293edae8d1f7ff04ce9&oe=5D7EF278";
                    $arrCatalogoDescuentoOferta[2]["url_foto"] = "images/oferta_2.png";
                    
                    $arrCatalogoParaVertirseBien = array();
                    $arrCatalogoParaVertirseBien[1]["url_foto"] = "http://www.theantiguaguide.com/wp-content/uploads/2016/05/feature-800x-8.jpg";
                    $arrCatalogoParaVertirseBien[2]["url_foto"] = "images/stela9_1.png";
                    $arrCatalogoParaVertirseBien[3]["url_foto"] = "images/stela9_2.png";
                    
                    ?>      
                        
                    <div class="d-none d-md-block">                        
                                
                        <div class="row p-0 m-0 justify-content-center" style="margin-top: 2% !important;">
                            <div class="col-10 col-lg-6 p-0 m-0 pt-lg-0 pt-4">
                                                          
                                <div class="input-group mb-3 rounded" style="">
                                    
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-white border-0 pr-0" >
                                            <i class="fa fa-search" style="color: #3E53AC;"></i>
                                        </span>
                                    </div>
                                    
                                    <input type="text" value="" id="txtAutocomplete" name="txtAutocomplete" class="form-control border-0 nofocusBorder" placeholder="<?php print lang["textoPlaceHolderAutocomplete"]?>" aria-label="What are you looking for?" aria-describedby="basic-addon2">
                                    
                                    <div class="input-group-append col-3 m-0 p-0">
                                        <select id="slcUbicacion" name="slcUbicacion" class="form-control text-center rounded  border-0 nofocusBorder" onchange="fntUbicacionCookie(this.value);">
                                            <?php
                                            
                                            while( $rTMP = each($arrUbicacion) ){
                                                
                                                $strSelected = sesion["ubicacion"] == $rTMP["key"] ? "selected" : "";
                                                ?>
                                                
                                                <option <?php print $strSelected;?> value="<?php print $rTMP["key"]?>"><?php print sesion["lenguaje"] == "en" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                                                
                                                <?php
                                                
                                            }
                                            
                                            ?>
                                        </select>
                                    </div>
                                
                                    <div class="input-group-append">
                                        <select id="slcLenguaje" name="slcLenguaje" class="form-control text-center rounded border-0 nofocusBorder" onchange="fntSetLenguajeCookie(this.value);">
                                            <option value="en" <?php print sesion["lenguaje"] == "en" ? "selected" : ""?> >en</option>
                                            <option value="es" <?php print sesion["lenguaje"] == "es" ? "selected" : ""?> >es</option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                            
                    
                    <div class="container-fluid p-0 " style="margin-top: 2% !important;">
                            
                        <div class="row justify-content-center p-0 m-0">
                            
                            <!-- Web -->
                            <div class="col-12 col-lg-8  d-block d-md-none" style="padding-left: 30px; padding-right: 30px;">
                                     
                                <div class="row justify-content-center">
                                    
                                    <?php
                                        
                                    $intCountCate = 1;
                                    reset($arrCategoriaEspecial);
                                    while( $rTMP = each($arrCategoriaEspecial) ){
                                        ?>
                                        <div class="col-6 p-0 mb-4" onclick="location.href = 's.php?o=1&key=<?php print fntCoreEncrypt($rTMP["key"])?>&t=5&q=<?php print  $rTMP["value"]["texto_google_place"]?>&ran=all'">
                                            
                                            <table style="width: 100%;">
                                                
                                                <tr>
                                                    <td class="" style="padding: 10px;">
                                                                
                                                        <img src="data:image/png;base64,<?php print  base64_encode(file_get_contents($rTMP["value"]["url_imagen_principal"])) ?>" class="img-fluid" > 
                                                    
                                                    </td>
                                                    <td class="text-left" style="width: 60%; vertical-align: middle; font-size: 12px; font-weight: bold; line-height: 15px;">
                                                        
                                                        <?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?>
                                                    
                                                    </td>
                                                </tr>
                                                                                        
                                            </table>
                                        
                                        </div>
                                        <?php
                                        $intCountCate++;
                                        if( $intCountCate == 7 ){
                                            break;
                                        }
                                        
                                    }
                                    
                                    ?>
                                      
                                    
                                    
                                </div>
                                
                            </div>
                            
                            <!-- Web -->
                            <div class="col-10  d-none d-md-block pt-1" style="">
                                    
                                <div class="row p-0 justify-content-sm-center ">
                                    
                                    <?php
                                              
                                    $intCountCate = 1;
                                    $boolOff = true;
                                    reset($arrCategoriaEspecial);
                                    while( $rTMP = each($arrCategoriaEspecial) ){
                                        ?>
                                        <div class="col-12 <?php print $intCountCate == 2 ? "col-lg-8" : "col-lg-4"?>  mb-4 <?php //print $boolOff ? "offset-lg-1" : "" ?>  " id="divContentImageSeccion1_<?php print $intCount;?>">
                                        
                                            <div class="card" style="cursor: pointer; border: 0px;" onclick="location.href = 's.php?o=1&key=<?php print fntCoreEncrypt($rTMP["key"])?>&t=5&q=<?php print  $rTMP["value"]["texto_google_place"]?>&ran=all'" >
                                                
                                                <div class="card-img-wrap" style="border-radius: 10px !important; ">
                                                    
                                                    <img class="card-img-top imgZoom" src="<?php print $rTMP["value"]["url_imagen_principal_web"]?>" style="height: 300px; object-fit: cover; " >
                                                
                                                </div>                                
                                                
                                                <div class="p-2 m-0 " style="position: absolute; top: 0px; left: 0px; width: 100%;">
                                                    
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td style=""  class=""> 
                                                                  
                                                                <img src="data:image/png;base64,<?php print  base64_encode(file_get_contents($rTMP["value"]["url_imagen_principal"])) ?>" class="img-fluid" style="width: 100%; height: auto; "> 
                                                            
                                                            </td>
                                                            <td style="width: <?php print $intCountCate == 2 ? "95" : "90"?>%;" class="text-left pl-2">
                                                                <span class="text-white" style="font-size: 19px; font-weight: bold;">
                                                                    
                                                                    <?php print $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?>
                                                                
                                                                </span>    
                                                            </td>
                                                        </tr>
                                                    </table>
                                                
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        <?php
                                        $boolOff = $boolOff ? false : true;
                                        $intCountCate++;
                                        if( $intCountCate == 6 ){
                                            break;
                                        }
                                        
                                    }
                                    
                                    ?>
                                
                                </div>
                                
                            
                            </div>
                            
                        </div>
                          
                        <div class="row p-0 m-0">
                            <div class="col-12 bg-white p-0 pl-4 m-0 pb-0  text-left">
                                    
                                <h2 class="mb-0" style="margin-top: 50px;"><?php print lang["diviertete"]?></h2>                                    
                                <h5 style="font-weight:100;" class="m-0 mb-3"><?php print lang["te_ayudamos_descubrir"]?></h5>                                    
                                                            
                            </div>
                        </div>   
                          
                        <div class="row p-0 m-0">
                            <div class="col-12 bg-white p-0 m-0 text-left">
                                
                                <table class="m-0 p-0" style="width: 100%;" class="" >
                                    <tr>
                                        <td class="bg-white m-0 p-0" style="width: 3%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 0px 10px 10px 0px;">
                                
                                        </td>
                                        
                                        <td class="bg-white m-0 p-0" style="width: 10%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px !important; margin-right: 10px !important;">
                                
                                        </td>
                                        <td class="bg-white m-0 p-0" style="width: 87%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px 0px 0px 10px; ">
                                
                                        </td>
                                        
                                        
                                    </tr>
                                </table>                                
                                                            
                            </div>
                        </div>   
                          
                        <div class="row p-0 m-0">     
                            
                            <!-- Web -->
                            <div class="col-12 p-0 m-0 d-none d-sm-block" style="background-image: url('images/index_1.jpg'); background-repeat: no-repeat; width: auto; background-attachment: fixed; height: 500px; background-size: cover;  ">
                            </div>
                            
                            <!-- Movil -->
                            <div class="col-12 p-0 m-0 d-block d-sm-none" style="">
                                
                                <img src="images/index_1.jpg" style="width: 100%; height: auto; object-fit: cover;">
                                
                            </div>
                            
                        </div>
                            
                        <div class="row p-0 m-0">
                            <div class="col-12 p-0 m-0 ">
                                    
                                <h2 class="mb-3"  style="margin-top: 80px;">
                                    <?php print lang["eventos_proximos"]?>
                                </h2>
                                
                                
                                <div class="row justify-content-center p-0 m-0">
                                    
                                    <div class="col-12 col-lg-6 p-0 m-0">
                                        <!-- Web d-none d-sm-block -->
                                        <img src="images/Party.jpg" class="img-fluid d-none d-sm-block" style="width: 100%; height: 500px; border-right: 10px solid #3E53AC; object-fit: cover;">
                                        
                                        <!-- Movil  d-block d-sm-none -->
                                        <img src="images/Party.jpg" class="img-fluid d-block d-sm-none" style="width: 100%; height: auto;  border-top: 10px solid #3E53AC; object-fit: cover;">
                                    
                                    </div>
                                    
                                    <div class="col-12 col-lg-6 p-0 m-0" style="">
                                            
                                        <table style="width: 100%; height: 100%;">
                                            <tr>
                                                <td class="text-center" style="vertical-align: middle;">
                                                
                                                    <!-- Web  -->
                                                    <div class="d-none d-md-block">
                                                        <h4 ><?php print lang["los_mejores_eventos"]?></h6>                                    
                                                        <h6 style="font-weight:100;"><?php print lang["escoge_tu_rango_precio"]?></h4>                                    
                                                    
                                                    </div>                     

                                                    <!-- Movil   -->
                                                    <div class="d-block d-md-none">
                                                        <h6 class="mt-2" style="font-weight:100;"><?php print lang["los_mejores_eventos"]?></h6>                                    
                                                        <h6 style="font-weight:100;"><?php print lang["escoge_tu_rango_precio"]?></h6>                                    
                                                    
                                                    </div>                     
                                                    
                                                       
                                                    <div class="row p-0 m-0 justify-content-center">
                                                        
                                                        <div class="col-12 m-0 p-0 mt-3">
                                                        
                                                            <button onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "bar" : "bar"?>&ran=1_A'" class="btn btn-sm btn-inguate pl-2 pr-2 mr-1 pl-lg-4 pr-lg-4 mr-lg-3 "><?php print lang["costoso"]?></button>
                                                        
                                                            <button onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "bar" : "bar"?>&ran=1_M'" class="btn btn-sm btn-inguate pl-2 pr-2 mr-1 pl-lg-4 pr-lg-4 mr-lg-3"><?php print lang["moderado"]?></button>
                                                        
                                                            <button onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "bar" : "bar"?>&ran=1_B'" class="btn btn-sm btn-inguate pl-2 pr-2 pl-lg-4 pr-lg-4"><?php print lang["barato"]?></button>
                                                        
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                    
                                              </td>
                                            </tr>
                                        </table>
                                    
                                    </div>
                                    
                                </div>    
                                
                            </div>
                        </div>
                        <div class="row p-0 m-0" >
                            
                            <div class="col-9 p-0 m-0 d-none d-md-block" style="margin-top: 80px !important;">
                                <table style="width: 100%; height: 100%;">
                                    <tr>
                                        <td style="width: 87%; vertical-align: top;">
                                            
                                            <hr style="border: 5px solid #3E53AC; border-radius: 10px; ">
                                
                                        </td>
                                        <td style="width: 10%; vertical-align: top;">
                                            
                                            <hr style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px; margin-right: 10px;">
                                
                                        </td>
                                        <td style="width: 3%; vertical-align: top;">
                                            
                                            <hr style="border: 5px solid #3E53AC; border-radius: 10px;">
                                
                                        </td>
                                        
                                    </tr>
                                </table>
                            </div>
                                     
                            <div class="col-3 p-0 m-0 d-none d-md-block" style="margin-top: 80px !important;">
                                <h3 class="text-dark "><?php print lang["experiencias_riquisimas"]?></h3>
                            </div>
                            
                        
                            <div class="col-12 p-0 m-0 d-block d-md-none">
                                
                                <div class="row p-0 m-0">
                                    <div class="col-12 bg-white p-0 m-0 pb-0  text-right">
                                        
                                        <h3 class="mb-3" style="margin-top: 50px;"><?php print lang["experiencias_riquisimas"]?></h3>                                    
                                    </div>
                                </div>   
                                  
                                <div class="row p-0 m-0">
                                    <div class="col-12  p-0 m-0 text-left">
                                        
                                        <table class="m-0 p-0" style="width: 100%;" class="" >
                                            <tr>
                                                <td style="width: 87%; vertical-align: top;">
                                                    
                                                    <hr class="m-0" style="border: 5px solid #3E53AC; border-radius: 0px 10px 10px 0px; ">
                                        
                                                </td>
                                                <td style="width: 10%; vertical-align: top;">
                                                    
                                                    <hr class="m-0" style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px !important; margin-right: 10px !important;">
                                        
                                                </td>
                                                <td style="width: 3%; vertical-align: top;">
                                                    
                                                    <hr class="m-0" style="border: 5px solid #3E53AC; border-radius: 10px 0px 0px 10px;">
                                        
                                                </td>
                                                
                                            </tr>
                                        </table>                                
                                                                    
                                    </div>
                                </div>      
                                                            
                            </div>                     
                                  
                            
                        </div>
                             
                        <div class="row p-0 m-0">
                            <div class="col-12 p-0 m-0 d-none d-md-block" style="margin-top: 50px !important; height: 500px !important;">
                                  
                                <?php

                                
                                $strIdentificador = isset($arrRespuestaRestaurante[2]->identificador) ? $arrRespuestaRestaurante[2]->identificador : "";
                                $strTexto = isset($arrRespuestaRestaurante[2]->texto_corto) ? $arrRespuestaRestaurante[2]->texto_corto : "";
                                
                                $sintAltoMin = isset($arrRespuestaRestaurante[1]->min) ? number_format($arrRespuestaRestaurante[1]->min, 0) : 0.00;
                                $sintAltoMax = isset($arrRespuestaRestaurante[1]->max) ? number_format($arrRespuestaRestaurante[1]->max, 0) : 0.00;

                                $sintMedioMin = isset($arrRespuestaRestaurante[2]->min) ? number_format($arrRespuestaRestaurante[2]->min, 0) : 0.00;
                                $sintMedioMax = isset($arrRespuestaRestaurante[2]->max) ? number_format($arrRespuestaRestaurante[2]->max, 0) : 0.00;

                                $sintBajoMin = isset($arrRespuestaRestaurante[3]->min) ? number_format($arrRespuestaRestaurante[3]->min, 0) : 0.00;
                                $sintBajoMax = isset($arrRespuestaRestaurante[3]->max) ? number_format($arrRespuestaRestaurante[3]->max, 0) : 0.00;
                                            
                                ?>
                                
                                
                                <div class="row justify-content-center p-0 m-0" style="height: 100% !important">
                                    <div class="col-12 col-lg-3 " style="height: 100% !important;">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td  onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_A'; " style="vertical-align: middle; cursor: pointer;"  >
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/index_delicioso.png" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center mt-4"><?php print lang["deliciosos"]?></h5>
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintAltoMin;?> - Q<?php print $sintAltoMax;?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    
                                    <div class="col-12 col-lg-3 " style="height: 100% !important;">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_M'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/index_sabroso.png" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center mt-4"><?php print lang["sabroso"]?></h5>
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintMedioMin;?> - Q<?php print $sintMedioMax;?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    
                                    <div class="col-12 col-lg-3 " style="height: 100% !important;">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_B'; " style="vertical-align: middle; cursor: pointer;">
                                                                                                                 
                                                    <img class="card-img-top imgZoom border-0" src="images/index_rico.png" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center mt-4"><?php print lang["rico"]?></h5>
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintBajoMin;?> - Q<?php print $sintBajoMax;?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    
                                    
                                </div>  
                                  
                            </div>
                            
                            <div class="col-12 p-0 m-0 d-block d-md-none" style="margin-top: 50px !important;">
                                
                                <div class="row m-0 p-0">
                                    <div class="col-12 m-0 p-1">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td  onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_A'; " style="vertical-align: middle; cursor: pointer;"  >
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/index_delicioso.png" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center mt-4"><?php print lang["deliciosos"]?></h5>
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintAltoMin;?> - Q<?php print $sintAltoMax;?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    <div class="col-12 m-0 p-1">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_M'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/index_sabroso.png" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center mt-4"><?php print lang["sabroso"]?></h5>
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintMedioMin;?> - Q<?php print $sintMedioMax;?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>    
                                        
                                    </div>
                                    <div class="col-12 m-0 p-1">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_B'; " style="vertical-align: middle; cursor: pointer;">
                                                                                                                 
                                                    <img class="card-img-top imgZoom border-0" src="images/index_rico.png" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center mt-4"><?php print lang["rico"]?></h5>
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintBajoMin;?> - Q<?php print $sintBajoMax;?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>    
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row p-0 m-0" >
                            
                            <!-- WEb -->
                            
                            <div class="col-5 p-0 m-0 d-none d-md-block " style="margin-top: 80px !important;">
                                
                                <table style="width: 100%; height: 100%;">
                                    <tr>
                                        <td class="" colspan="3" style="vertical-align: bottom;">
                                            
                                            <h3 class="text-dark text-left m-0 pl-4 mb-3"><?php print lang["descuentos_ofertas"]?></h3>
                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 87%; vertical-align: top;">
                                            
                                            <hr class="m-0" style="border: 5px solid #3E53AC; border-radius: 10px; ">
                                
                                        </td>
                                        <td style="width: 10%; vertical-align: top;">
                                            
                                            <hr class="m-0" style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px !important; margin-right: 10px !important;">
                                
                                        </td>
                                        <td style="width: 3%; vertical-align: top;">
                                            
                                            <hr class="m-0" style="border: 5px solid #3E53AC; border-radius: 10px;">
                                
                                        </td>
                                        
                                    </tr>
                                </table>
                                
                            </div>
                            <div class="col-7 p-0 m-0 pl-2 d-none d-md-block" style="margin-top: 80px !important;">
                                
                                <div class="row m-0 p-0">
                                
                                    <div class="col-6">
                                        <img onclick="location.href = 's.php?o=1&key=0&t=4&q=restaurante&ran=all'" src="<?php print $arrCatalogoDescuentoOferta[1]["url_foto"]?>" class="img-fluid" style="width: 100%; height: 500px; border-radius: 10px; object-fit: cover; cursor: pointer;">
                                        
                                    </div>
                                
                                    <div class="col-6 ">
                                        <img onclick="location.href = 's.php?o=1&key=0&t=4&q=restaurante&ran=all'" src="<?php print $arrCatalogoDescuentoOferta[2]["url_foto"]?>" class="img-fluid" style="width: 100%; height: 500px; border-radius: 10px; object-fit: cover; cursor: pointer;">
                                    
                                    </div>
                                
                                </div>
                                                            
                            </div>
                            
                            <!-- Movil -->
                            <div class="col-12 p-0 m-0  d-block d-md-none " style="margin-top: 80px !important;">
                                
                                <div class="row p-0 m-0">
                                    <div class="col-12 bg-white p-0 m-0 pb-0  text-right">
                                        
                                        <h3 class="mb-3" style="margin-top: 50px;"><?php print lang["descuentos_ofertas"]?></h3>                                    
                                    </div>
                                </div>   
                                  
                                <div class="row p-0 m-0">
                                    <div class="col-12  p-0 m-0 text-left">
                                        
                                        <table class="m-0 p-0" style="width: 100%;" class="" >
                                            <tr>
                                                
                                                <td class="bg-white m-0 p-0" style="width: 87%; vertical-align: top;">
                                                    
                                                    <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 0px 10px 10px 0px; ">
                                        
                                                </td>
                                                
                                                <td class="bg-white m-0 p-0" style="width: 10%; vertical-align: top;">
                                                    
                                                    <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px !important; margin-right: 10px !important;">
                                        
                                                </td>
                                                <td class="bg-white m-0 p-0" style="width: 3%; vertical-align: top;">
                                                    
                                                    <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px 0px 0px 10px;">
                                        
                                                </td>
                                                
                                                
                                                
                                            </tr>
                                        </table>                                
                                                                    
                                    </div>
                                </div>
                                
                                <div class="row m-0 p-0">
                                
                                    <div class="col-6 mt-4">
                                        <img onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "restaurante" : "restaurante"?>&ran=all'" src="<?php print $arrCatalogoDescuentoOferta[1]["url_foto"]?>" class="img-fluid" style="width: 100%; height: 220px; border-radius: 10px; object-fit: cover;">
                                        
                                    </div>
                                
                                    <div class="col-6 mt-4">
                                        <img onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "restaurante" : "restaurante"?>&ran=all'" src="<?php print $arrCatalogoDescuentoOferta[2]["url_foto"]?>" class="img-fluid" style="width: 100%; height: 220px; border-radius: 10px; object-fit: cover;">
                                    
                                    </div>
                                
                                </div>      
                                
                            </div>
                               
                        </div>
                        
                        <div class="row p-0 m-0">
                            <div class="col-12 p-0 m-0 ">
                                    
                                <h2 class="mb-3"  style="margin-top: 80px;">
                                    <?php print lang["descubre_guatemala"]?>
                                </h2>
                                
                                
                                <div class="row justify-content-center p-0 m-0">
                                    
                                    <div class="col-12 col-lg-6 p-0 m-0">
                                    
                                        <!-- Web d-none d-sm-block -->
                                        <img  src="images/volcano view.jpg" class="img-fluid d-none d-sm-block" style="width: 100%; height: 500px; border-right: 10px solid #3E53AC; object-fit: cover;">
                                        
                                        <!-- Movil  d-block d-sm-none -->
                                        <img src="images/volcano view.jpg" class="img-fluid d-block d-sm-none" style="width: 100%; height: auto; border-top: 10px solid #3E53AC; object-fit: cover;">
                                    
                                    </div>
                                    
                                    <div class="col-12 col-lg-6  p-0 m-0" style="">
                                    
                                        <table style="width: 100%; height: 100%;">
                                            <tr>
                                                <td class="text-center" style="vertical-align: middle;">
                                                
                                                    <!-- Web  -->
                                                    <div class="d-none d-sm-block">
                                                        <h4 ><?php print lang["lugares_actividades_emocionantes"]?></h6>                                    
                                                        <h6 style="font-weight:100;"><?php print lang["escoge_tu_rango_precio"]?></h4>                                    
                                                    
                                                    </div>                     

                                                    <!-- Movil   -->
                                                    <div class="d-block d-sm-none">
                                                        <h6 class="mt-2" style="font-weight:100;"><?php print lang["lugares_actividades_emocionantes"]?></h6>                                    
                                                        <h6 style="font-weight:100;"><?php print lang["escoge_tu_rango_precio"]?></h6>                                    
                                                    
                                                    </div>
                                                    
                                                    <div class="row p-0 m-0 justify-content-center">
                                                        
                                                        <div class="col-12 mt-3">
                                                        
                                                            <?php
                                                          
                                                            $strIdentificador = isset($arrRespuestaTour[2]->identificador) ? $arrRespuestaTour[2]->identificador : "";
                                                            $strTexto = isset($arrRespuestaTour[2]->texto_corto) ? $arrRespuestaTour[2]->texto_corto : "";
                                
                                                            $sintAltoMin = isset($arrRespuestaTour[1]->min) ? number_format($arrRespuestaTour[1]->min, 0) : 0;
                                                            $sintAltoMax = isset($arrRespuestaTour[1]->max) ? number_format($arrRespuestaTour[1]->max, 0) : 0;

                                                            $sintMedioMin = isset($arrRespuestaTour[2]->min) ? number_format($arrRespuestaTour[2]->min, 0) : 0;
                                                            $sintMedioMax = isset($arrRespuestaTour[2]->max) ? number_format($arrRespuestaTour[2]->max, 0) : 0;

                                                            $sintBajoMin = isset($arrRespuestaTour[3]->min) ? number_format($arrRespuestaTour[3]->min, 0) : 0;
                                                            $sintBajoMax = isset($arrRespuestaTour[3]->max) ? number_format($arrRespuestaTour[3]->max, 0) : 0;
                                                            
                                                            ?>
                                                        
                                                            <!--
                                                            <button onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_A'; " class="btn btn-sm btn-inguate pl-2 pr-2 mr-1 pl-lg-4 pr-lg-4 mr-lg-3">Q<?php print $sintAltoMin?>  - Q<?php print $sintAltoMax?></button>
                                                        
                                                            <button onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_M'; " class="btn btn-sm btn-inguate pl-2 pr-2 mr-1 pl-lg-4 pr-lg-4 mr-lg-3">Q<?php print $sintMedioMin?> - Q<?php print $sintMedioMax?></button>
                                                        
                                                            <button onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_B'; " class="btn btn-sm btn-inguate pl-2 pr-2 pl-lg-4 pr-lg-4">Q<?php print $sintBajoMin?> - Q<?php print $sintBajoMax?></button>
                                                            -->
                                                            <button onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "hotel" : "hotel"?>&ran=all'" class="btn btn-sm btn-inguate pl-2 pr-2 mr-1 pl-lg-4 pr-lg-4 mr-lg-3 "><?php print lang["costoso"]?></button>
                                                        
                                                            <button onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "hotel" : "hotel"?>&ran=all'" class="btn btn-sm btn-inguate pl-2 pr-2 mr-1 pl-lg-4 pr-lg-4 mr-lg-3"><?php print lang["moderado"]?></button>
                                                        
                                                            <button onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "hotel" : "hotel"?>&ran=all'" class="btn btn-sm btn-inguate pl-2 pr-2 pl-lg-4 pr-lg-4"><?php print lang["barato"]?></button>
                                                        
                                                        
                                                        </div>
                                                        
                                                    </div>
                                                    
                                              </td>
                                            </tr>
                                        </table>
                                                                                
                                    </div>
                                    
                                </div>    
                                
                            </div>
                        </div>    
                        <div class="row p-0 m-0" >
                            
                            
                            <div class="col-9 p-0 m-0 d-none d-md-block" style="margin-top: 80px !important;">
                                <table style="width: 100%; height: 100%;">
                                    <tr>
                                        <td style="width: 87%; vertical-align: top;">
                                            
                                            <hr style="border: 5px solid #3E53AC; border-radius: 10px; ">
                                
                                        </td>
                                        <td style="width: 10%; vertical-align: top;">
                                            
                                            <hr style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px; margin-right: 10px;">
                                
                                        </td>
                                        <td style="width: 3%; vertical-align: top;">
                                            
                                            <hr style="border: 5px solid #3E53AC; border-radius: 10px;">
                                
                                        </td>
                                        
                                    </tr>
                                </table>
                            </div>
                            
                            <div class="col-3 p-0 m-0 d-none d-md-block" style="margin-top: 80px !important;">
                                <h3 class="text-dark"><?php print lang["quedate_conmigo"]?></h3>
                            </div>
                            
                            
                            <div class="col-12 p-0 m-0 d-block d-md-none">
                                
                                <div class="row p-0 m-0">
                                    <div class="col-12 bg-white p-0 m-0 pb-0  text-right">
                                        
                                        <h3 class="mb-3" style="margin-top: 50px;"><?php print lang["quedate_conmigo"]?></h3>                                    
                                    </div>
                                </div>   
                                  
                                <div class="row p-0 m-0">
                                    <div class="col-12  p-0 m-0 text-left">
                                        
                                        <table class="m-0 p-0" style="width: 100%;" class="" >
                                            <tr>
                                                
                                                <td class="bg-white m-0 p-0" style="width: 87%; vertical-align: top;">
                                                    
                                                    <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 0px 10px 10px 0px; ">
                                        
                                                </td>
                                                
                                                <td class="bg-white m-0 p-0" style="width: 10%; vertical-align: top;">
                                                    
                                                    <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px !important; margin-right: 10px !important;">
                                        
                                                </td>
                                                <td class="bg-white m-0 p-0" style="width: 3%; vertical-align: top;">
                                                    
                                                    <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px 0px 0px 10px;">
                                        
                                                </td>
                                                
                                                
                                                
                                            </tr>
                                        </table>                                
                                                                    
                                    </div>
                                </div>      
                                                            
                            </div> 
                            
                        </div>
                        
                        <div class="row p-0 m-0">
                            <div class="col-12 p-0 m-0 d-none d-md-block" style="margin-top: 50px !important; height: 500px !important;">
                                <?php
                                                                                  
                                $strIdentificador = isset($arrRespuestaAlojamiento[2]->identificador) ? $arrRespuestaAlojamiento[2]->identificador : "";
                                $strTexto = isset($arrRespuestaAlojamiento[2]->texto_corto) ? $arrRespuestaAlojamiento[2]->texto_corto : "";
                                
                                $sintAltoMin = isset($arrRespuestaAlojamiento[1]->min) ? number_format($arrRespuestaAlojamiento[1]->min, 0) : 0.00;
                                $sintAltoMax = isset($arrRespuestaAlojamiento[1]->max) ? number_format($arrRespuestaAlojamiento[1]->max, 0) : 0.00;

                                $sintMedioMin = isset($arrRespuestaAlojamiento[2]->min) ? number_format($arrRespuestaAlojamiento[2]->min, 0) : 0.00;
                                $sintMedioMax = isset($arrRespuestaAlojamiento[2]->max) ? number_format($arrRespuestaAlojamiento[2]->max, 0) : 0.00;

                                $sintBajoMin = isset($arrRespuestaAlojamiento[3]->min) ? number_format($arrRespuestaAlojamiento[3]->min, 0) : 0.00;
                                $sintBajoMax = isset($arrRespuestaAlojamiento[3]->max) ? number_format($arrRespuestaAlojamiento[3]->max, 0) : 0.00;
                                
                                    
                                ?>
                                <div class="row justify-content-center p-0 m-0" style="height: 100% !important">
                                    <div class="col-12 col-lg-3 " style="height: 100% !important;">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_A'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/quedate_3.svg" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintAltoMin?> - Q<?php print $sintAltoMax?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                                     
                                    </div>
                                    
                                    <div class="col-12 col-lg-3 " style="height: 100% !important;">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_M'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/quedate_2.svg" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintMedioMin?> - Q<?php print $sintMedioMax?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                                     
                                    </div>
                                    
                                    <div class="col-12 col-lg-3 " style="height: 100% !important;">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_B'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/quedate_1.svg" style="width: 40%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintBajoMin?> - Q<?php print $sintBajoMax?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                                     
                                    </div>
                                    
                                    
                                </div>  
                                  
                            </div>
                            
                            <div class="col-12 p-0 m-0 d-block d-md-none" style="margin-top: 50px !important;">
                                
                                <div class="row m-0 p-0">
                                    <div class="col-12 m-0 p-1">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_A'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/quedate_3.svg" style="width: 30%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintAltoMin?> - Q<?php print $sintAltoMax?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    <div class="col-12 m-0 p-1">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_A'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/quedate_2.svg" style="width: 30%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintMedioMin?> - Q<?php print $sintMedioMax?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>    
                                        
                                    </div>
                                    <div class="col-12 m-0 p-1">
                                        
                                        <table style="width: 100%; height: 100% !important;">
                                            <tr>
                                                <td onclick="location.href = 's.php?o=1&key=<?php print $strIdentificador;?>&t=1&q=<?php print $strTexto?>&ran=1_A'; " style="vertical-align: middle; cursor: pointer;">
                                                
                                                    <img class="card-img-top imgZoom border-0" src="images/quedate_1.svg" style="width: 30%; height: auto;" >
                                                    <h5 class="mb-1 mt-2 text-dark text-center">Q<?php print $sintBajoMin?> - Q<?php print $sintBajoMax?></h5>
                                      
                                                </td>
                                            </tr>
                                        </table>    
                                        
                                    </div>
                                </div>
                                
                            </div>
                            
                            
                        </div>
                        <!--
                        <div class="row p-0 m-0">
                            
                            <div class="col-12 p-0 m-0 text-center" style="margin-top: 80px !important;">
                                
                                <h3 class="text-dark text-center m-0 mb-3"><?php print lang["para_vestirse_bien"]?></h3>
                                                                
                                
                            </div>
                              
                        </div>     
                               
                        <div class="row p-0 m-0">
                            <div class="col-6 bg-white p-0 m-0 text-left" style=";">
                                
                                <table class="m-0 p-0" style="width: 100%;" class="" >
                                    <tr>
                                        
                                        <td class="bg-white m-0 p-0" style="width: 87%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 0px 10px 10px 0px; ">
                                
                                        </td>
                                        
                                        <td class="bg-white m-0 p-0" style="width: 10%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px !important; margin-right: 10px !important;">
                                
                                        </td>
                                        <td class="bg-white m-0 p-0" style="width: 3%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px;">
                                
                                        </td>
                                        
                                        
                                        
                                    </tr>
                                </table>                                
                                                            
                            </div>
                            <div class="col-6 bg-white p-0 m-0 text-left" style="">
                                
                                <table class="m-0 p-0" style="width: 100%;" class="" >
                                    <tr>
                                        <td class="bg-white m-0 p-0" style="width: 3%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px;">
                                
                                        </td>
                                        
                                        <td class="bg-white m-0 p-0" style="width: 10%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px; margin-left: 10px !important; margin-right: 10px !important;">
                                
                                        </td>
                                        <td class="bg-white m-0 p-0" style="width: 87%; vertical-align: top;">
                                            
                                            <hr class="m-0 p-0" style="border: 5px solid #3E53AC; border-radius: 10px 0px 0px 10px; ">
                                
                                        </td>
                                        
                                        
                                    </tr>
                                </table>                                
                                                            
                            </div>
                        </div>
                        
                             
                        <div class="row p-0 m-0 ">
                           
                            <div class="col-12  col-lg-4 mt-1 p-2 mt-lg-4 p-lg-4">
                                <img onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "ropa" : "clothing"?>&ran=all'" style="width: 100%; height: <?php print $boolMovil ? "auto" : "500px";?>; "  src="<?php print $arrCatalogoParaVertirseBien[1]["url_foto"]?>" >
                                
                            </div>
                        
                            <div class="col-12 col-lg-4 mt-0 p-2 mt-lg-4 p-lg-4">
                                <img onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "ropa" : "clothing"?>&ran=all'" style="width: 100%; height: <?php print $boolMovil ? "auto" : "500px";?>; "  src="<?php print $arrCatalogoParaVertirseBien[2]["url_foto"]?>" >
                                
                            </div>  
                            <div class="col-12 col-lg-4 mt-0 p-2 mt-lg-4 p-lg-4">
                                <img onclick="location.href = 's.php?o=1&key=0&t=4&q=<?php print $strLenguaje == "es" ? "ropa" : "clothing"?>&ran=all'" style="width: 100%; height: <?php print $boolMovil ? "auto" : "500px";?>; "  src="<?php print $arrCatalogoParaVertirseBien[3]["url_foto"]?>" >
                                
                            </div>  
                              
                        </div>
                          -->
                        <?php
                        
                        fntDrawFooter();
                        
                        ?>
                         
                    </div>
                </div>
                       
            </div>
        </div>
        
        <?php
        
        fntShowBarNavegacionFooter(true, "3E53AC" , $arrAlerta);
        
        ?>
        
    </div> 
    
    <script >
                    
        $(document).ready(function() { 
            
            var xhr;
            $('input[name="txtAutocomplete"]').autoComplete({
                minChars: 2,
                source: function(term, suggest){
                    
                    try {
                        xhr.abort();
                    }
                    catch(error) {
                    }

                    xhr = $.ajax({
                        url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete").val()+"&len="+$("#slcLenguaje").val(), 
                        dataType : "json",
                        success: function(result){
                            suggest(result);
                        }
                    });
                    
                },
                renderItem: function (item, search){
                    
                    //console.log(item);
                    
                    return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-identificador_2="'+item["identificador_2"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
                
                },
                onSelect: function(e, term, item){
                    
                    if( item.data("tipo") == "5" ){
                        
                        str = "/"+item.data("identificador_2");
                        location.href = str;
                        return false;
                        
                    }
                    else if( item.data("tipo") == "4" ){
                        
                        str = "/"+item.data("identificador_2");
                        location.href = str;
                        return false;
                        
                    }
                    else{
                        
                        str = "s.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                        location.href = str;
                        
                    }
                    
                    return false;
                    
                }
            }).change(function (){
                
                str = "s.php?o=1&key=0&t=4&q="+$("#txtAutocomplete").val()+"&ran=all";
                location.href = str;
                        
            });
            
            $('input[name="txtAutocomplete_2"]').autoComplete({
                minChars: 2,
                source: function(term, suggest){
                    
                    try {
                        xhr.abort();
                    }
                    catch(error) {
                    }

                    xhr = $.ajax({
                        url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete_2").val()+"&len="+$("#slcLenguaje_2").val(), 
                        dataType : "json",
                        success: function(result){
                            suggest(result);
                        }
                    });
                    
                },
                renderItem: function (item, search){
                    
                    //console.log(item);
                    
                    return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-identificador_2="'+item["identificador_2"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
                
                },
                onSelect: function(e, term, item){
                    
                    if( item.data("tipo") == "5" ){
                        
                        str = "/"+item.data("identificador_2");
                        location.href = str;
                        return false;
                        
                    }
                    else if( item.data("tipo") == "4" ){
                        
                        str = "/"+item.data("identificador_2");
                        location.href = str;
                        return false;
                        
                    }
                    else{
                        
                        str = "s.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                        location.href = str;
                        
                    }
                    
                    return false;
                    
                }
            }).change(function (){
                
                str = "s.php?o=1&key=0&t=4&q="+$("#txtAutocomplete_2").val()+"&ran=all";
                location.href = str;
                        
            });
              
            var sinWindowsHeight = $(window).height();
            
            //$("#divContenidoPost").height(( sinWindowsHeight * ( 70 / 100 ) )+'px');
            
            fntShowContenido();
            
            $('#mlAutoComplete').on('shown.bs.modal', function (e) {
                
                $("#txtAutocomplete_2").blur();

                $("#txtAutocomplete_2").addClass('active').focus();
                 
            });
            
            
        });
        
        function fntSetTipoUsuarioIndex(intTipo){
            
            $("#txtTipoRegistro").val(intTipo);
            $("#divFormRegistro").show();
            $("#divFormRegistroOpcion").hide();
            $("#exampleModalCenter").modal("show");
                
        }
        
        function fntLogInPublicoIndex(){
            
            $("#txtAction").val(window.location.href);
            
            var formData = new FormData(document.getElementById("frmModalLogInRegistroIndex"));
                
            $(".preloader").fadeIn();
            $.ajax({
                url: "servicio_core.php?servicio=setLogInRegistroPIndex", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                    if( result["error"] == "true" ){
                        
                        swal({
                            title: "Error",
                            text: result["msn"],
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        });
                            
                    }
                    else{
                        
                        $("#divFormRegistro").hide();
                        $("#divFormRegistroOpcion").show();
            
                        if( $("#txtTipoRegistro").val() == "2" ){
                            
                            swal({
                                title: "Registro Con Exito",
                                text: "Gracias por registrarte en INGUATE.com, en pocos dias sera el lanzamiento por que lo tienes que estar atento. Comparte con tus amigos",
                                type: "success",
                                confirmButtonClass: "#FF0000",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true,
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                     
                                    $.ajax({
                                        url: "servicio_core.php?servicio=setUsuarioNegocio", 
                                        success: function(result){
                                            
                                            setLogInDashCookie();    
                                            
                                        }
                                        
                                    });
                                
                                } 
                            });
                                    
                                
                        }
                        else{
                            
                            swal({
                                title: "Registro Con Exito",
                                text: "Gracias por registrarte en INGUATE.com, en pocos dias sera el lanzamiento por que lo tienes que estar atento. Comparte con tus amigos",
                                type: "success",
                                confirmButtonClass: "#FF0000",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true,
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                     
                                    location.href = "index.php"
                                    
                                
                                } 
                            });    
                                                    
                        }
                                                    
                    }
                    
                }
                        
            });
            
            return false;
            
        }
        
        function fntShowContenido(){
            
            $.ajax({                                                                                                                                                    
                url: "search.php?drawCotenido=true&o=2&key="+$("#hidKey").val()+"&t="+$("#hidTipo").val()+"&q="+$("#hidTexto").val()+"&ran="+$("#hidRango").val()+"&fil_tipo="+$("#hidFiltroTipo").val()+"&fil_key="+$("#hidFiltroKey").val()+"&len="+$("#slcLenguaje").val(), 
                success: function(result){
                    
                    $("#divContendo").html(result);
                
                }
            });
                
        }
        
        function fntShowAutocomplete(){
            
            $("#mlAutoComplete").modal("show");       
            
        }
                
        /*
            $.ajax({
                url: "https://api.unsplash.com/search/photos?page="+1+"&query=car&client_id=7e27b730888b9daa33c2b405cefb31c10cb887e8bf931e79cbd97d145019c6c4", 
                success: function(result){

                    $(".preloader").fadeOut();
                    //
                    
                    $.each( result["results"], function( key, value ) {
                        
                        //console.log(value["user"]["name"]);                 
                        //console.log(value["urls"]["small"]);
                        
                        $("#photos").append('<div class=" card-img-wrap" style="">   '+
                                           '     <img src="'+value["urls"]["regular"]+'" alt="" class="img-fluid " style="border: 6px solid white; border-radius: 5px; width: 100% !important; height: auto !important;">    '+
                                           ' </div>');
                         
                      
                    });
                    
                    return false;
                    
                }
            });  
                */
    </script>
    <?php
}
else{
    
    ?>
    <div class="container-fluid m-0 p-0" >
    
        <div class="row m-0 p-0">
        
            <div class="col-12 p-0 m-0 text-center" style="" >
                        
                <div id="divGradiente" style="  height: 50%;">
                              
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img class="d-block w-100 imgSlide" style="object-fit: cover;" src="images/antigua_guatemala.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100 imgSlide" style="object-fit: cover;" src="images/capital_0.jpg" alt="Second slide">
                        </div>
                      </div>
                    </div>
                    
                </div>
                <div id="divGradiente_2" style=" background: linear-gradient(180deg, rgba(255,255,255,0.8939950980392157) 28%, rgba(254,254,254,0.29175420168067223) 97%);  height: 100%; position: absolute; top: 0pc; width: 100%;">
                      
                </div>
                          
                <div class="m-0 p-0 pt-2 pt-lg-4  text-center" style="position: absolute; top: 0px; width: 100%; "> 
                    
                    <div class="" style="">
                        
                        <?php
                        $strClassTextColor = "text-dark";
                        $strClassTextColorMovil = "text-white";
                        $strUrlLogo = "images/LogoInguateAZUL.png";
                        $strUrlMenu = "images/menu_black.png";
                        
                        
                        ?> 
                        <img src="<?php print $strUrlLogo;?>" class="img-fluid " style="" >
                    
                    </div>       
                       
                                          
                    <div id="" class="container-fluid " style="margin-top: 0% !important; ">
                        <div id="divCotendioForm">   
                        
                            <style>
                                

                                #clockdiv{
                                    font-family: sans-serif;
                                    color: #fff;
                                    display: inline-block;
                                    font-weight: 100;
                                    text-align: center;
                                    font-size: 30px;
                                }

                                #clockdiv > div{
                                    padding: 1px;
                                    border-radius: 3px;
                                    display: inline-block;
                                }

                                #clockdiv div > span{
                                    padding: 10px;
                                    border-radius: 3px;
                                    background: #969696;
                                    display: inline-block;
                                }

                                .smalltext{
                                    padding-top: 5px;
                                    font-size: 16px;color: black;
                                }
                                
                            </style>
                            <br>
                            <h5 class="text-center" >Lanzamiento en</h5>
                            
                            <div id="clockdiv">
                                <div>
                                    <span class="days"></span>       
                                    <b style="color: #3E53AC;">:</b>
                                    <div class="smalltext">Days</div>
                                </div>
                                <div>
                                    <span class="hours"></span>
                                    <b style="color: #3E53AC;">:</b>
                                    <div class="smalltext">Hours</div>
                                </div>
                                <div>
                                    <span class="minutes"></span>
                                    <b style="color: #3E53AC;">:</b>
                                    <div class="smalltext">Minutes</div>
                                </div>
                                <div>
                                    <span class="seconds"></span>
                                    <div class="smalltext">Seconds</div>
                                </div>
                            </div>
                        
                                                
                            
                            <div id="divFormRegistroOpciond">
                                <button class="btn btn-light text-white mt-2" onclick="fntSetTipoUsuarioIndex('2');" style="background-color: #3E53AC; border: 0px;">Inscribe tu negocio</button>
                                
                                <button class="btn btn-light text-white mt-2" onclick="fntSetTipoUsuarioIndex('3');" style="background-color: #3E53AC; border: 0px;">Suscribete como usuario</button>
                                <button class="btn btn-light text-white mt-2" onclick="fntSetTipoUsuarioIndex('4');" style="background-color: #3E53AC; border: 0px;">Tienes más de 5,000 seguidores en redes sociales?</button>
                            </div>
                            
                            
                        </div>
                    </div>
                       
                          
                </div>
                       
            </div>
        </div>
        
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content p-2">
                    
                <form name="frmModalLogInRegistroIndex" id="frmModalLogInRegistroIndex"  onsubmit="return false;" method="POST">
                    
                    <input type="hidden" name="txtTipoLogIn" id="txtTipoLogIn" value="E">
                    <input type="hidden" name="txtTipoRegistro" id="txtTipoRegistro" value="3">
                    <input type="hidden" name="txtRegistro" id="txtRegistro" value="Y">
                    <input type="hidden" name="txtIdentificador" id="txtIdentificador" value="">
                    <input type="hidden" name="txtPictureUrl" id="txtPictureUrl" value="">
                    <input type="hidden" name="txtAction" id="txtAction" value="">
                    <input type="hidden" name="hidTokenCap" id="hidTokenCap" value="">
                                
                    <div class="row ">
                        <div class="col-12 text-center">
                            <fb:login-button 
                              scope="public_profile,email"
                              onlogin="checkLoginStateIndex();"
                              data-width="" data-size="large" data-button-type="continue_with" data-auto-logout-link="false" data-use-continue-as="true"
                              >
                            </fb:login-button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                                                            
                            <input type="email" id="txtEmail" name="txtEmail" class="form-control form-control-sm" placeholder="Email">
                            
                        </div>
                    </div>
                          
                    <div class="row">
                    
                        <div class="col-12 mt-2">
                            
                            <input type="password" id="txtClave" name="txtClave" class="form-control form-control-sm" placeholder="Contraseña">
                            
                        </div>
                    </div>
                    
                    <div class="row">
                
                        <div class="col-12 mt-2">
                                                         
                            <input type="text" id="txtNombre" name="txtNombre" class="form-control form-control-sm" placeholder="Nombre">
                            
                        </div>
                    </div>
                    <div class="row">
                    
                        <div class="col-12 mt-2">
                                                          
                            <input type="text" id="txtTelefono" name="txtTelefono" class="form-control form-control-sm" placeholder="Telefono">
                            
                        </div>
                    </div>
                    <div class="row justify-content-center">
                    
                        <div class="col-12 col-lg-4 mt-4 text-center">
                            
                            <div id="divRecap" style="width: 100%;"></div>
                        </div>
                    </div>
                          
                </form>

            </div>
        </div>
    </div>
    
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
    </script>
    <script >
    
        var verifyCallback = function(response) {
            
            $("#hidTokenCap").val(response);
            fntLogInPublicoIndex();
            
        };
        
        var onloadCallback = function() {
          
            grecaptcha.render('divRecap', {
                'sitekey' : '6LfJ7agUAAAAAEdlQUwhTyZ_XnWVkXHmYyZCgPdn',
                'callback' : verifyCallback,
            });
            
        };
                    
        $(document).ready(function() { 
            
             
            var sinWindowsHeight = $(window).height();
            $("#divGradiente").height( ( sinWindowsHeight * ( 100 / 100 ) )+'px');
            $("#divCotendioForm").height( ( sinWindowsHeight * ( 50 / 100 ) )+'px');
            
            $(".imgSlide").each(function (){
                
                $(this).height( ( sinWindowsHeight * ( 100 / 100 ) )+'px');
                    
            }); 
            
                          
            //fntShowContenido();
            
                    
            
        });
        
        function getTimeRemaining(endtime) {
          var t = Date.parse(endtime) - Date.parse(new Date());
          var seconds = Math.floor((t / 1000) % 60);
          var minutes = Math.floor((t / 1000 / 60) % 60);
          var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
          var days = Math.floor(t / (1000 * 60 * 60 * 24));
          return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
          };
        }

        function initializeClock(id, endtime) {
          var clock = document.getElementById(id);
          var daysSpan = clock.querySelector('.days');
          var hoursSpan = clock.querySelector('.hours');
          var minutesSpan = clock.querySelector('.minutes');
          var secondsSpan = clock.querySelector('.seconds');

          function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
              clearInterval(timeinterval);
            }
          }

          updateClock();
          var timeinterval = setInterval(updateClock, 1000);
        }

        var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
        var deadline = new Date("Jul 31 2019 15:00:00 GMT-0600");
        
        initializeClock('clockdiv', deadline);
        
        function fntSetTipoUsuarioIndex(intTipo){
            
            $("#txtTipoRegistro").val(intTipo);
            $("#divFormRegistro").show();
            $("#divFormRegistroOpcion").hide();
            $("#exampleModalCenter").modal("show");
                
        }
        
        function fntLogInPublicoIndex(){
            
            $("#txtAction").val(window.location.href);
            
            var formData = new FormData(document.getElementById("frmModalLogInRegistroIndex"));
                
            $(".preloader").fadeIn();
            $.ajax({
                url: "servicio_core.php?servicio=setLogInRegistroPIndex", 
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(result){
                    
                    $(".preloader").fadeOut();
                    
                    if( result["error"] == "true" ){
                        
                        swal({
                            title: "Error",
                            text: result["msn"],
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        });
                            
                    }
                    else{
                        
                        $("#divFormRegistro").hide();
                        $("#divFormRegistroOpcion").show();
            
                        if( $("#txtTipoRegistro").val() == "2" ){
                            
                            swal({
                                title: "Registro Con Exito",
                                text: "",
                                type: "success",
                                confirmButtonClass: "#FF0000",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true,
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                     
                                    $.ajax({
                                        url: "servicio_core.php?servicio=setUsuarioNegocio", 
                                        success: function(result){
                                            
                                            setLogInDashCookie(true);    
                                            
                                        }
                                        
                                    });
                                
                                } 
                            });
                                    
                                
                        }
                        else{
                            
                            swal({
                                title: "Registro Con Exito",
                                text: "",
                                type: "success",
                                confirmButtonClass: "#FF0000",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true,
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                     
                                    location.href = "index.php"
                                    
                                
                                } 
                            });    
                                                    
                        }
                                                    
                    }
                    
                }
                        
            });
            
            return false;
            
        }
        
        function checkLoginStateIndex() {           
          FB.getLoginStatus(function(response) {
              
              if( response.status == "connected" ){
                  
                  FB.api('/me?fields=email,first_name,last_name,picture', function(response) {
                                            
                      $("#txtEmail").val(response.email);
                      $("#txtNombre").val(response.first_name+" "+response.last_name);
                      $("#txtIdentificador").val(response.id);
                      $("#txtPictureUrl").val(response.picture.data.url);
                      $("#txtTipoLogIn").val("F");
                        
                      fntLogInPublicoIndex();
                      
                  });
                  
              }
            
          }, {scope: 'public_profile,email'});
        }
        function fntShowContenido(){
            
            $.ajax({                                                                                                                                                    
                url: "search.php?drawCotenido=true&o=2&key="+$("#hidKey").val()+"&t="+$("#hidTipo").val()+"&q="+$("#hidTexto").val()+"&ran="+$("#hidRango").val()+"&fil_tipo="+$("#hidFiltroTipo").val()+"&fil_key="+$("#hidFiltroKey").val()+"&len="+$("#slcLenguaje").val(), 
                success: function(result){
                    
                    $("#divContendo").html(result);
                
                }
            });
                
        }        /*
            $.ajax({
                url: "https://api.unsplash.com/search/photos?page="+1+"&query=car&client_id=7e27b730888b9daa33c2b405cefb31c10cb887e8bf931e79cbd97d145019c6c4", 
                success: function(result){

                    $(".preloader").fadeOut();
                    //
                    
                    $.each( result["results"], function( key, value ) {
                        
                        //console.log(value["user"]["name"]);                 
                        //console.log(value["urls"]["small"]);
                        
                        $("#photos").append('<div class=" card-img-wrap" style="">   '+
                                           '     <img src="'+value["urls"]["regular"]+'" alt="" class="img-fluid " style="border: 6px solid white; border-radius: 5px; width: 100% !important; height: auto !important;">    '+
                                           ' </div>');
                         
                      
                    });
                    
                    return false;
                    
                }
            });  
                */
    </script>
    <?php
    
}

?>

       

<link rel="stylesheet" type="text/css" href="dist/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="dist/slick/slick-theme.css"/>
<script type="text/javascript" src="dist/slick/slick.min.js"></script>

<script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
<script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>


<?php
fntDrawFooterPublico("3E53AC", $boolInterno);
?>