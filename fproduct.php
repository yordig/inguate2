<?php
include "core/function_servicio.php";

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$strLenguaje = isset($_GET["len"]) ? trim($_GET["len"]) : "es";

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
$intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
              

fntDrawHeaderPublico($boolMovil, true);
//include "core/dbClass.php";
$objDBClass = new dbClass();

$arrProducto = fntGetProductoFavoritoUsuario($intIdUsuario, true, $objDBClass, $strLenguaje);

?>
<link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
<br>
<div class="row justify-content-center  p-0 m-0" >
    <div class="col-12" style="margin-top: 60px; direction: ltr;">
        
        <table class="p-0 m-0" id="myTable" style="width: 100%;">
            <thead class="" style="display: none;">
                <tr>
                    <th >Product</th>
                    <th >Product</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $intLetraDescripcion = 90;
                while( $rTMP = each($arrProducto) ){
                    ?>
                    <tr >
                        <td class=" text-right pr-2 " style="<?php print $boolMovil ? "width: 40%;" : "width: 30%;"?>">
                            <img onclick="window.open('pro.php?iden=<?php print fntCoreEncrypt($rTMP["key"])?>')" src="<?php print $rTMP["value"]["logo_url"]?>" style="<?php print $boolMovil ? "width: 160px; height: 120px;" : "width: 280px; height: 200px;"?> cursor: pointer;" class="img-fluid d-md-fle  border border-white lis-border-width-4 rounded m-0" alt="" />
                        </td>
                        <td style="vertical-align: top;">
                            <ul class="list-unstyled my-0 pr-0">
                                <li class="lis-font-weight-600 m-0  <?php print $boolMovil ? "h5" : "h2"?>">
                                    <a href="#" class="lis-dark" onclick="window.open('pro.php?iden=<?php print fntCoreEncrypt($rTMP["key"])?>')">
                                        <?php print $rTMP["value"]["nombre"]?>
                                        
                                    </a>
                                    <?php
                                    
                                    if( sesion["logIn"] && isset($rTMP["value"]["favorito"]) ){
                                        
                                        ?>
                                        
                                        <i class="fa fa-heart-o" onclick="fntDisplayIconoFavorito('iconProFavoritoSi_<?php print $rTMP["key"]?>', 'iconProFavoritoNo_<?php print $rTMP["key"]?>', true, '<?php print fntCoreEncrypt($rTMP["key"])?>');" id="iconProFavoritoNo_<?php print $rTMP["key"]?>" style="font-size: 20px; cursor: pointer; color: red; <?php print $rTMP["value"]["favorito"] == "Y" ? "display: none;" : ""?>"></i>
                                        <i class="fa fa-heart"   onclick="fntDisplayIconoFavorito('iconProFavoritoSi_<?php print $rTMP["key"]?>', 'iconProFavoritoNo_<?php print $rTMP["key"]?>', false, '<?php print fntCoreEncrypt($rTMP["key"])?>');" id="iconProFavoritoSi_<?php print $rTMP["key"]?>" style="font-size: 20px; cursor: pointer; color: red; <?php print $rTMP["value"]["favorito"] == "Y" ? "" : "display: none;"?>"></i>
                                        
                                        <?php
                                    }
                                    
                                    ?>
                                        
                                </li>
                                <li class="text-secondary mb-2" style="font-size: <?php print $boolMovil ? "13px" : "18px"?> ; line-height: 1.1;">
                                    <?php print $rTMP["value"]["titulo"]?>
                                </li>
                                <li class=" " style="font-size: <?php print $boolMovil ? "14px" : "19px"?>; line-height: 1.2;">
                                    <?php print strlen($rTMP["value"]["descripcion"]) > $intLetraDescripcion && $boolMovil ? substr($rTMP["value"]["descripcion"], 0, $intLetraDescripcion)."..." :$rTMP["value"]["descripcion"]?>
                                </li>
                                <li class=" <?php print $boolMovil ? "" : "h5"?>" style=" line-height: 1.2;">
                                    Q<?php print number_format($rTMP["value"]["precio"], 2)?>
                                </li>
                            </ul>    
                        </td>
                    </tr>
                    
                    <?php
                }                    
                
                ?>
        
            </tbody>
        </table>
        <script>    
            
            $(document).ready(function() { 
                    
                $('#myTable').DataTable({
                    "searching": false,
                    "ordering": false,
                    "info":     false
                });
            
            });
            
            function fntDisplayIconoFavorito(idObjSi, idObjNo, boolFavorito, strKey){
                
                if( boolFavorito ){
                    
                    $("#"+idObjSi).show();
                    $("#"+idObjNo).hide();
                    
                }
                else{
                    
                    $("#"+idObjSi).hide();
                    $("#"+idObjNo).show();
                    
                }
                
                $.ajax({
                    url: "servicio_core.php?servicio=setProductoFavorito&key="+strKey+"&fav="+( boolFavorito ? "Y" : "N" ), 
                    dataType : "json",
                    success: function(result){
                    }
                });
                
                            
            }
            
        </script>    
                
        
    </div>
    
</div>
<div  id="divContendo">
</div>

<section class="image-bg footer lis-grediant grediant-bt pb-0">
    <div class="background-image-maker"></div>
    <div class="holder-image">
        <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none">
    </div>
    <div class="container">
        <div class="row pb-5">
            <div class="col-12 col-md-8">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <h5 class="footer-head">Useful Links</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <h5 class="footer-head">My Account</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Dashboard</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Profile</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> My Listing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Favorites</A></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                        <h5 class="footer-head">Pages</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3">
                        <h5 class="footer-head">Help</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="footer-logo">
                    <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a> 
                </div>
                <p class="my-4">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu in  felis eu pede mollis enim.</p>
                <a href="#" class="text-white"><u>App Download</u></a> 
                <ul class="list-inline mb-0 mt-2">
                    <li class="list-inline-item"><A href="#"><img src="dist/images/play-store.png" alt="" class="img-fluid" /></a></li>
                    <li class="list-inline-item"><A href="#"><img src="dist/images/google-play.png" alt="" class="img-fluid" /></a></li>
                    <li class="list-inline-item"><A href="#"><img src="dist/images/windows.png" alt="" class="img-fluid" /></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom mt-5 py-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 text-center text-md-right mb-3 mb-md-0">
                    <span> &copy; 2017 Lister. Powered by <a href="#" class="lis-primary">PSD2allconversion.</a></span>
                </div> 
                <div class="col-12 col-md-6 text-center text-md-left">
                    <ul class="list-inline footer-social mb-0">
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-tumblr"></i></a></li>
                        <li class="list-inline-item"><A href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</section>

<a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50"> 
    <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
</a> 

<script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
<script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>

<script >

    $(document).ready(function() { 
        
        var xhr;
        $('input[name="txtAutocomplete"]').autoComplete({
            minChars: 2,
            source: function(term, suggest){
                
                try {
                    xhr.abort();
                }
                catch(error) {
                }

                xhr = $.ajax({
                    url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete").val()+"&len="+$("#slcLenguaje").val(), 
                    dataType : "json",
                    success: function(result){
                        suggest(result);
                    }
                });
                
            },
            renderItem: function (item, search){
                
                return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
            
            },
            onSelect: function(e, term, item){
                
                if( item.data("tipo") == "5" ){
                    
                    str = "place.php?p="+item.data("identificador")+"&len="+$("#slcLenguaje").val();
                    location.href = str;
                    return false;
                    
                }
                else if( item.data("tipo") == "4" ){
                    
                    str = "pro.php?iden="+item.data("identificador")+"&len="+$("#slcLenguaje").val();
                    location.href = str;
                    return false;
                    
                }
                else{
                    
                    str = "search.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                    location.href = str;
                    
                }
                
            }
        }).keyup(function(e){
            
            if(e.keyCode == 13){
                
                str = "search.php?o=1&key=0&t=0&q="+this.value; 
                //location.href = str;  
                //window.open(str);
            
            }
            
        });
        
        var mySlider = $("#rangePrecio").bootstrapSlider();
        $('#rangePrecio').slider().on('slide', function () {
            
            arrRango = $('#rangePrecio').val().split(",");
            
            $("#spnRangoMin").html(arrRango[0]);
            $("#spnRangoMaz").html(arrRango[1]);
            
        });
        
        fntShowContenido();
        
    });
    
    function fntShowContenido(){
        
        $.ajax({                                                                                                                                                    
            url: "search.php?drawCotenido=true&o=2&key="+$("#hidKey").val()+"&t="+$("#hidTipo").val()+"&q="+$("#hidTexto").val()+"&ran="+$("#hidRango").val()+"&fil_tipo="+$("#hidFiltroTipo").val()+"&fil_key="+$("#hidFiltroKey").val()+"&len="+$("#slcLenguaje").val(), 
            success: function(result){
                
                $("#divContendo").html(result);
            
            }
        });
            
    } 
    
</script>
<?php
fntDrawFooterPublico();
?>