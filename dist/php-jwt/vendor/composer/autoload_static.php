<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf306ccafde8bba2f50d076a7792e8c38
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Firebase\\JWT\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Firebase\\JWT\\' => 
        array (
            0 => __DIR__ . '/..' . '/firebase/php-jwt/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf306ccafde8bba2f50d076a7792e8c38::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf306ccafde8bba2f50d076a7792e8c38::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
