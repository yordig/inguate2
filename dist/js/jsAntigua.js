function getDocumentLayer(strName, objDoc) {
    var p,i,x=false;

    if(!objDoc) objDoc=document;

    if(objDoc[strName]) {
        x=objDoc[strName];
        if (!x.tagName) x = false;
    }

    if (!x && objDoc.all) x=objDoc.all[strName];
    for (i=0;!x && i<objDoc.forms.length; i++) x=objDoc.forms[i][strName];
    if (!x && objDoc.getElementById) x=objDoc.getElementById(strName);
    for (i=0;!x && objDoc.layers && i<objDoc.layers.length; i++) x=getDocumentLayer(strName,objDoc.layers[i].document);
    //for(i=0;!x && i<objDoc.all.length; i++) if (objDoc.all(i).id == strName || objDoc.all(i).name == strName) x = objDoc.all(i);

    return x;
}
              
function JavaScriptTextTrim(str) {
    var whitespace = new String(" \t\n\r");
    var s = new String(str);

    if (whitespace.indexOf(s.charAt(0)) != -1) {
        var j=0, i = s.length;
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1) j++;
        s = s.substring(j, i);
    }
    if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
        var i = s.length - 1;
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1) i--;
        s = s.substring(0, i+1);
    }
    return s;
}

function outInts(number, boolAddComma) {
    if (number.length <= 3)
        return (number == '' ? '0' : number);
    else {
        var mod = number.length%3;
        var output = (mod == 0 ? '' : (number.substring(0,mod)));
        for (i=0 ; i < Math.floor(number.length/3) ; i++) {
            if (((mod ==0) && (i ==0)) || !boolAddComma)
            output+= number.substring(mod+3*i,mod+3*i+3);
            else
            output+= ',' + number.substring(mod+3*i,mod+3*i+3);
        }
        return (output);
    }
}

function outCents(amount,intDec) {
    if (!intDec)
        intDec = 2;
    var intTenExp =    Math.pow(10,intDec);
    amount = Math.round( ( (amount) - Math.floor(amount) ) *intTenExp);
    var strZeros = "";
    for (i=1;i<=intDec;i++){
        if (amount < Math.pow(10,i-1))
            strZeros+="0";
    }
    if (amount==0)
        return "."+strZeros;
    else
        return "."+strZeros+amount;
}

function format_monto(monto,intDec) {
    var comas  =/,/ig;
    var strTotal = JavaScriptTextTrim(monto) + '';

    if (!intDec) intDec = 2;
    strTotal = strTotal.replace(comas,'');
    strTotal = strTotal * 1;

    var intTenExp =    Math.pow(10,intDec);
    strTotal = Math.round(strTotal * intTenExp)/intTenExp;
    var addMinus = false;
    if (strTotal < 0 ){
        strTotal = Math.abs(strTotal);
        addMinus = true;
    }
    return ((addMinus ? '-':'')+(outInts(Math.floor(strTotal-0) + '', true) + outCents(strTotal - 0,intDec)));
}

function format_monto_sincomas(monto, intDec) {
    var strTotal = "";
    var comas  =/,/ig;
    if (!intDec && intDec != 0 ) intDec = 2;
    var intTenExp =    Math.pow(10,intDec);

    monto = JavaScriptTextTrim(monto);
    monto = monto.replace(comas,'');
    monto = Math.round(monto * intTenExp)/intTenExp;

    strTotal = monto + "";
    strTotal = strTotal.replace(comas,'');

    return outInts(Math.floor(strTotal-0) + '', false) + outCents(strTotal - 0, intDec);
}

function fntMantenerDecimales(obj, boolIsEntero, boolIsMayorCero){
    
    boolIsEntero = ( !boolIsEntero ) ? false : true;
    boolIsMayorCero = ( !boolIsMayorCero ) ? false : true;
    var objValue = obj.value;
    
    if ( isNaN(objValue) || ( objValue <= 0 ) )
        objValue = 0;
    
    objValue = ( boolIsMayorCero ) ? ( ( objValue <= 0 ) ? 1 : objValue ) : objValue;
    
    obj.value = ( boolIsEntero ) ? parseInt(objValue) : format_monto_sincomas(objValue, 2);
    
    
}

function fntValidarCamposObligatorio(strObjId, strTipo){
    
    var BoolError = false;
    if( getDocumentLayer(strObjId) ){
        
        $( "#"+strObjId ).removeClass( "alert-danger" );
       
        if( strTipo == "num" && parseFloat(getDocumentLayer(strObjId).value*1) < 1 ){
            
            $( "#"+strObjId ).addClass( "alert-danger" );
            BoolError = true;
            
        }
        else if( strTipo == "text" && ( getDocumentLayer(strObjId).value == "" || getDocumentLayer(strObjId).value == "..." ) ){
            $( "#"+strObjId ).addClass( "alert-danger" );
            BoolError = true;
        } 
        else if( strTipo == "textarea" && $( "#"+strObjId ).val() == "" ){
            
            $( "#"+strObjId ).addClass( "alert-danger" );
            BoolError = true;
        }
        
    }
        
    return BoolError;    
}

function fntValidarCamposObligatorioTipoFile(strObjId, strObjIdAlerta){
    
    var BoolError = false;
    if( getDocumentLayer(strObjId) ){
        
        $( "#"+strObjIdAlerta ).removeClass( "btn-danger" );
        
        if( getDocumentLayer(strObjId).value == "" ){
            $( "#"+strObjIdAlerta ).addClass( "btn-danger" );
            BoolError = true;
        }
        
    }
        
    return BoolError;    
}

function fntSetIntervalo(){
    
    setInterval(function(){ document.location.reload(true); }, 10000);
    
}