<?php
                         
include "core/function_servicio.php";



$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$intOrigen = isset($_GET["o"]) ? intval($_GET["o"]) : 0;
$intKey = isset($_GET["key"]) ? fntCoreDecrypt($_GET["key"]) : 0;
$intTipo = isset($_GET["t"]) ? $_GET["t"] : 0;
$strTexto = isset($_GET["q"]) ? trim($_GET["q"]) : "";
$strRango = isset($_GET["ran"]) ? trim($_GET["ran"]) : "all";
$strFiltro = isset($_GET["fil_tipo"]) ? trim($_GET["fil_tipo"]) : "";
$strFiltroKey = isset($_GET["fil_key"]) ? trim($_GET["fil_key"]) : "";
//$strLenguaje = isset($_GET["len"]) ? trim($_GET["len"]) : "es";
$strLenguaje = isset(sesion["lenguaje"]) ? trim(sesion["lenguaje"]) : "es";
define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

if( isset($_GET["getMoreResultado_gogole"]) ){
    
    $strTipo = isset($_GET["tipo"]) ? $_GET["tipo"]  : "";
    $strRango = isset($_GET["rango"]) ? $_GET["rango"]  : "";
    
    $strTexto = isset($_GET["texto"]) ? $_GET["texto"]  : "";
    $strNextPageToken = isset($_GET["next_page_token"]) ? $_GET["next_page_token"]  : "";
    
    $strApiKey = fntGetApiKeyPlace();
    
    $arrUbicacion = fntUbicacion();
    $arrUbicacion = $arrUbicacion[sesion["ubicacion"]];
    $strLenguaje = isset(sesion["lenguaje"]) ? sesion["lenguaje"] : "es";
    $strTexto = str_replace(" ", "+", $strTexto);
    
    if( empty($strNextPageToken) ){
        
        $strRangoFiltro = "";
        if( $strTipo == 4 ){
            
            if( $strRango == "1_A" ){
                
                $strRangoFiltro = "&minprice=3&maxprice=4";
            
            }
            
            if( $strRango == "1_M" ){
                
                $strRangoFiltro = "&minprice=2&maxprice=3";
            
            }
            
            if( $strRango == "1_B" ){
                
                $strRangoFiltro = "&minprice=0&maxprice=1";
            
            }
                        
        }
        
        $strUrlApi = "https://maps.googleapis.com/maps/api/place/textsearch/json?location=".$arrUbicacion["lat"].",".$arrUbicacion["lng"]."&radius=".$arrUbicacion["radio"]."&key=".$strApiKey."&language=".$strLenguaje."&query=".$strTexto.$strRangoFiltro;
        
    }
    else{
        
        $strUrlApi = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=".$strNextPageToken."&key=".$strApiKey;
        
        
    }
    
    $arrCatalogoPrecioGoogle = fntCatalogoPriceLevelGoogle();
                  
    $objJsonDatos = file_get_contents($strUrlApi);
    $objJsonDatos = json_decode($objJsonDatos);
    $arrPlace = array();  
    
    if( isset($objJsonDatos->results) && count($objJsonDatos->results) > 0 ){
        
        $strKeyPlaceId = "";
        while( $rTMP = each($objJsonDatos->results) ){
            
            $strKeyPlaceId .= ( $strKeyPlaceId == "" ? "" : "," )."'".$rTMP["value"]->place_id."'";
                        
        }
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        
        $strQuery = "SELECT place_id
                     FROM   place
                     WHERE  place_id IN ({$strKeyPlaceId})";
        $arrPlaceIsset = array();        
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrPlaceIsset[$rTMP["place_id"]] = $rTMP["place_id"];
               
        }   
        
        $objDBClass->db_free_result($qTMP);     
        
        reset($objJsonDatos->results);
        while( $rTMP = each($objJsonDatos->results) ){
            
            if( !isset($arrPlaceIsset[$rTMP["value"]->place_id] ) ){
                
                $arrTMP["direccion"] = isset($rTMP["value"]->formatted_address) ? $rTMP["value"]->formatted_address : "";
                $arrTMP["nombre"] = isset($rTMP["value"]->name) ? $rTMP["value"]->name : "";
                $arrTMP["identificador"] = ($rTMP["value"]->place_id);
                $arrTMP["rating"] = isset($rTMP["value"]->rating) ? intval($rTMP["value"]->rating) : "";
                $arrTMP["icon"] = isset($rTMP["value"]->icon) ? ($rTMP["value"]->icon) : "";
                $arrTMP["opening_hours"] = isset($rTMP["value"]->opening_hours) ? ($rTMP["value"]->opening_hours) : "";
                $arrTMP["price_level"] = isset($rTMP["value"]->price_level) && isset($arrCatalogoPrecioGoogle[$rTMP["value"]->price_level]) ?  $arrCatalogoPrecioGoogle[$rTMP["value"]->price_level]["nombre"] : "";
                $arrTMP["foto"] = "images/Noimagee.jpg?11";
                
                if( isset($rTMP["value"]->photos) && count($rTMP["value"]->photos) > 0 ){
                              
                    while( $arrTMP1 = each($rTMP["value"]->photos) ){
                           /*
                        $strLinkMap = "https://maps.googleapis.com/maps/api/place/photo?&key=".$strApiKey."&maxwidth=400&photoreference=";
                        
                        $path= $strLinkMap.$arrTMP1["value"]->photo_reference;
                        $type = pathinfo($path, PATHINFO_EXTENSION);
                        $data = file_get_contents($path);
                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                        $arrTMP["foto"] = $base64;
                        
                          */
                        $arrTMP["foto"] = "";
                        
                        
                        break;
                    }
                    
                }
                                             
                array_push($arrPlace, $arrTMP);
                
            }
            
        }
        
    }
    
    shuffle($arrPlace);
    
    $arr["next_page_token"] = isset($objJsonDatos->next_page_token) ? $objJsonDatos->next_page_token : "";
    $arr["resultados"] = $arrPlace;
    $arr["api"] = $strUrlApi;
    //$objJsonDatos = json_decode($objJsonDatos);
    print json_encode($arr);         
    die();
    
}

if( isset($_GET["getMoreResultado"]) ){
    
    $strTipo = isset($_GET["tipo"]) ? $_GET["tipo"]  : "";
    $strRango = isset($_GET["rango"]) ? $_GET["rango"]  : "";
    
    $strTexto = isset($_GET["texto"]) ? $_GET["texto"]  : "";
    $strNextPageToken = isset($_GET["next_page_token"]) ? intval($_GET["next_page_token"])  : 0;
    
    $strApiKey = fntGetApiKeyPlace();
    
    $strLenguaje = isset(sesion["lenguaje"]) ? sesion["lenguaje"] : "es";
    $strTexto = str_replace(" ", "+", $strTexto);
    
    
    $intUbicacion = sesion["ubicacion"];
    $arrCatalogoUbicacionLugar1 = fntUbicacionLugar();
    $strKeyUbicaionLugar = "";
    while( $rTMP = each($arrCatalogoUbicacionLugar1[$intUbicacion]["lugar"]) ){
        
        $strKeyUbicaionLugar .= ( $strKeyUbicaionLugar == "" ? "" : "," ).$rTMP["key"];
        
    }  
    
    $arrCatalogoPrecioGoogle = fntCatalogoPriceLevelGoogle();
       
    $strTexto = strtolower($strTexto);
    
    include "core/dbClass.php";
    $objDBClass = new dbClass();
    
    $strQuery = "SELECT negocio.id_negocio
                 FROM   texto,
                        negocio_texto,
                        negocio
                 WHERE  texto.sinonimo LIKE '%{$strTexto}%'
                 AND    negocio_texto.id_texto = texto.id_texto                 
                 AND    negocio.id_negocio = negocio_texto.id_negocio                 
                 AND    negocio.id_ubicacion_lugar IN({$strKeyUbicaionLugar})
                 LIMIT  {$strNextPageToken}, 10
                 
                 ";
                 
    $arrKeyNegocio = array();
    $arrPlace = array();
    $qTMP = $objDBClass->db_consulta($strQuery);
    while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
        
        $arrKeyNegocio[$rTMP["id_negocio"]] = $rTMP["id_negocio"];
    
    }
    $objDBClass->db_free_result($qTMP);
    
    if( count($arrKeyNegocio) > 0 ){
        
        $strKeyNegocio = implode(",", $arrKeyNegocio);
        
        $strQuery = "SELECT negocio.id_negocio,
                            negocio.nombre,
                            negocio.lat,
                            negocio.lng,
                            negocio.direccion,
                            negocio.rango,
                            negocio.precio,
                            negocio.telefono,
                            negocio.estado,
                            negocio_galeria.id_negocio_galeria,
                            negocio_galeria.url
                     FROM   negocio
                                LEFT JOIN negocio_galeria
                                    ON  negocio_galeria.id_negocio = negocio.id_negocio
                              
                     WHERE  negocio.id_negocio IN({$strKeyNegocio})                
                     ORDER BY rango DESC, id_negocio_galeria DESC 
                     ";        
        
        $arrPlace = array();
        $arrDatos = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrDatos[$rTMP["id_negocio"]]["id_negocio"] = $rTMP["id_negocio"];
            $arrDatos[$rTMP["id_negocio"]]["nombre"] = $rTMP["nombre"];
            $arrDatos[$rTMP["id_negocio"]]["lat"] = $rTMP["lat"];
            $arrDatos[$rTMP["id_negocio"]]["lng"] = $rTMP["lng"];
            $arrDatos[$rTMP["id_negocio"]]["direccion"] = $rTMP["direccion"];
            $arrDatos[$rTMP["id_negocio"]]["rango"] = $rTMP["rango"];
            $arrDatos[$rTMP["id_negocio"]]["telefono"] = $rTMP["telefono"];
            $arrDatos[$rTMP["id_negocio"]]["estado"] = $rTMP["estado"];
            $arrDatos[$rTMP["id_negocio"]]["precio"] = $rTMP["precio"];
            
            
            if( intval($rTMP["id_negocio_galeria"]) ){
                
                $arrDatos[$rTMP["id_negocio"]]["galeria"][$rTMP["id_negocio_galeria"]]["url"] = $rTMP["url"];
                
            } 
                
        }
        
        $objDBClass->db_free_result($qTMP);
                     
        
        if( count($arrDatos)  > 0 ) {
            
            while( $rTMP = each($arrDatos)){
                
                $arrTMP["direccion"] = $rTMP["value"]["direccion"];
                $arrTMP["nombre"] = $rTMP["value"]["nombre"];
                $arrTMP["identificador"] = md5($rTMP["value"]["id_negocio"]);
                $arrTMP["rating"] = $rTMP["value"]["rango"];
                $arrTMP["precio"] = $rTMP["value"]["precio"];
                $arrTMP["opening_hours"] = "ssssssss";
                
                if( intval($rTMP["value"]["precio"]) )
                    $arrTMP["price_level"] = isset($arrCatalogoPrecioGoogle[$rTMP["value"]["precio"]]) ?  $arrCatalogoPrecioGoogle[$rTMP["value"]["precio"]]["nombre"] : "";
                
                
                $arrTMP["foto"] = "images/Noimagee.jpg?1";
                    
                if( isset($rTMP["value"]["galeria"]) ){
                    
                    while( $arrTMP1 = each($rTMP["value"]["galeria"]) ){
                        
                        $arrTMP["foto"] = "imgInguate.php?t=up&src=".$arrTMP1["value"]["url"];
                                       
                    }
                    
                }
                                             
                array_push($arrPlace, $arrTMP);    
                
            }
            
        }            
        
    }
    
    $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
    fntSetRastreoBusquedaUsuario($intIdUsuario, 3, 8, "Y", $strTexto, $strTexto, "", $intUbicacion, count( $arrPlace ),
                                 $strLenguaje, "", "", true, $objDBClass);
            
    $arr["next_page_token"] = intval($strNextPageToken) + 10;
    $arr["resultados"] = $arrPlace;
    
    print json_encode($arr);         
    die();
    
}
   
if( isset($_GET["drawCotenido"]) ){
     
    $intUbicacion = sesion["ubicacion"];
    $arrCatalogoUbicacionLugar1 = fntUbicacionLugar();
    $strKeyUbicaionLugar = "";
    while( $rTMP = each($arrCatalogoUbicacionLugar1[$intUbicacion]["lugar"]) ){
        
        $strKeyUbicaionLugar .= ( $strKeyUbicaionLugar == "" ? "" : "," ).$rTMP["key"];
        
    }
    
    $boolShowRangoBajo = false;
    $boolShowRangoMedio = false;
    $boolShowRangoAlto = false;
    
    $intIdUsuario = 0;
    if( sesion["logIn"] ){
        
        $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
        
    }
    
    $arrResultado = array();    
            
    if( $intTipo == 1 ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $strFiltroPlaceSelectFavorito = "";
        $strFiltroPlaceSelectFavoritoJoin = "";
        
        if( sesion["logIn"] ){
            
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario ){
                
                $strFiltroPlaceSelectFavorito = ", usuario_place_favorito.id_usuario_place_favorito id_favorito";
                $strFiltroPlaceSelectFavoritoJoin = "LEFT JOIN usuario_place_favorito
                                                        ON  usuario_place_favorito.id_place = place.id_place
                                                        AND usuario_place_favorito.id_usuario = {$intIdUsuario}";
        
                        
            }
            
        }
        
        $arrProducto = array();
                          
        $strQuery = "SELECT id_clasificacion_padre,
                            nombre,
                            tag_en,
                            tag_es
                     FROM   clasificacion_padre
                     WHERE  id_clasificacion = {$intKey}
                     
                     ";
        $strKeyClasificacionPadreFiltro = "";
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            
            $strKeyClasificacionPadreFiltro .= ( $strKeyClasificacionPadreFiltro == "" ? "" : "," ).$rTMP["id_clasificacion_padre"];
                                    
        }
        $objDBClass->db_free_result($qTMP);
        
        $strQuery = "SELECT place.id_place,
                            place.url_place,
                            place.titulo,
                            place.descripcion,
                            place.estrellas,
                            place.url_logo,
                            place.precio_mediana_bajo,
                            place.precio_mediana_alto,
                            place_clasificacion.id_clasificacion,
                            place_clasificacion_padre.id_clasificacion_padre,
                            place_galeria.id_place_galeria,
                            place_galeria.url_imagen
                            {$strFiltroPlaceSelectFavorito}
                     FROM   place
                                {$strFiltroPlaceSelectFavoritoJoin},
                            place_clasificacion,
                            place_clasificacion_padre,
                            place_galeria
                     WHERE  place_clasificacion.id_clasificacion = {$intKey}
                     AND    place.id_place = place_clasificacion_padre.id_place
                     AND    place.id_place = place_clasificacion.id_place
                     AND    place.id_place = place_galeria.id_place
                     AND    place.estado = 'A'
                     AND    place.id_ubicacion_lugar IN ({$strKeyUbicaionLugar})                     
                     ";
        $sinMin = 0;
        $sinMax= 0;
        $arrResultado = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrResultado[$rTMP["id_place"]]["id_place"] = $rTMP["id_place"];
            $arrResultado[$rTMP["id_place"]]["url_place"] = $rTMP["url_place"];
            $arrResultado[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
            $arrResultado[$rTMP["id_place"]]["descripcion"] = $rTMP["descripcion"];
            $arrResultado[$rTMP["id_place"]]["estrellas"] = $rTMP["estrellas"];
            $arrResultado[$rTMP["id_place"]]["url_logo"] = $rTMP["url_logo"];
            $arrResultado[$rTMP["id_place"]]["precio_mediana_bajo"] = $rTMP["precio_mediana_bajo"];
            $arrResultado[$rTMP["id_place"]]["precio_mediana_alto"] = $rTMP["precio_mediana_alto"];
            $arrResultado[$rTMP["id_place"]]["favorito"] = isset($rTMP["id_favorito"]) && intval($rTMP["id_favorito"]) ? "Y" : "N";
            $arrResultado[$rTMP["id_place"]]["galeria"][$rTMP["id_place_galeria"]]["url_imagen"] = $rTMP["url_imagen"];
            $arrResultado[$rTMP["id_place"]]["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            
            if( $sinMin == 0 )
                $sinMin = $rTMP["precio_mediana_bajo"];
            
            if( $sinMax == 0 )
                $sinMax = $rTMP["precio_mediana_alto"];
                
            if( $rTMP["precio_mediana_bajo"] <= $sinMin )
                $sinMin = $rTMP["precio_mediana_bajo"];
            
            if( $rTMP["precio_mediana_alto"] >= $sinMax )
                $sinMax = $rTMP["precio_mediana_alto"];
               
               
            $arrClasificacionPadreIsset[$rTMP["id_clasificacion_padre"]] = $rTMP["id_clasificacion_padre"];   
             
        }
        
        $arrRango = array();
        $sinDiferencia = $sinMax - $sinMin;
                
        if( $sinDiferencia > 15 ){
            
            $intRango = $sinDiferencia / 3;
            $intRango = intval($intRango) ;
            
            $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["tipo"] = 1; 
                                                                               
            $arrRango["bajo"]["min"] = $sinMin; 
            $arrRango["bajo"]["max"] = $sinMin + $intRango; 
                       
            $arrRango["medio"]["min"] = $sinMin + $intRango + 1; 
            $arrRango["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                            
            $arrRango["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
            $arrRango["alto"]["max"] = $sinMax; 
                            
        }   
        else{
            
            $arrRango["bajo"]["min"] = 0;
            $arrRango["bajo"]["max"] = 0;
                       
            $arrRango["medio"]["min"] = $sinMin;
            $arrRango["medio"]["max"] = $sinMax;
                            
            $arrRango["alto"]["min"] = 0;
            $arrRango["alto"]["max"] = 0;
            
        }
        
        if( count($arrResultado) > 0 ){
            
            
            while( $rTMP = each($arrResultado) ){
                
                $boolSalto = false;
                if( $strFiltro == "C" && intval($strFiltroKey) ){
                                                     
                    if( !isset($rTMP["value"]["clasificacion_padre"][$strFiltroKey]) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        $boolSalto = true;
                    
                    }                          
                        
                }
                
                if( !$boolSalto ){
                    
                    if( $rTMP["value"]["precio_mediana_bajo"] >= $arrRango["bajo"]["min"] 
                        && 
                        $rTMP["value"]["precio_mediana_bajo"] <= $arrRango["bajo"]["max"] ){
                        
                        $boolShowRangoBajo = true;
                        
                    }
                    else{
                        
                        if( $strRango == "1_B" ){
                            
                            unset($arrResultado[$rTMP["key"]]);
                            
                        }
                            
                    }    
                    
                    if( $rTMP["value"]["precio_mediana_bajo"] <= $arrRango["medio"]["min"] 
                        && 
                        $rTMP["value"]["precio_mediana_alto"] >= $arrRango["medio"]["max"]  ){
                        
                        $boolShowRangoMedio = true;
                        
                    }
                    else{
                        
                        if( $strRango == "1_M" || $strRango == "2_M" || $strRango == "3_M" ){
                            
                            unset($arrResultado[$rTMP["key"]]);
                            
                        }
                            
                    }   
                    
                    if( $rTMP["value"]["precio_mediana_alto"] >= $arrRango["alto"]["min"] 
                        && 
                        $rTMP["value"]["precio_mediana_alto"] <= $arrRango["alto"]["max"]  ){
                        
                        $boolShowRangoAlto = true;
                        
                    }
                    else{
                        
                        if( $strRango == "1_A" ){
                            
                            unset($arrResultado[$rTMP["key"]]);
                                
                        }
                            
                    }
                          
                    
                }
                    
            }
            
        }
        
        if( count($arrFiltroClasificacionPadre) > 0 ){
            
            reset($arrFiltroClasificacionPadre);
            
            while( $rTMP = each($arrFiltroClasificacionPadre) ){
                
                if( !isset($arrClasificacionPadreIsset[$rTMP["key"]]) ){
                    
                    unset($arrFiltroClasificacionPadre[$rTMP["key"]]);
                    
                }
                
            }
            
            reset($arrFiltroClasificacionPadre);
            
        }
        
        fntSetRastreoBusquedaUsuario($intIdUsuario, $intOrigen, $intTipo, "N", $intKey, $strTexto, $strRango, $intUbicacion, count( $arrResultado ),
                                     $strLenguaje, $strFiltro, $strFiltroKey, true, $objDBClass);
        
    } 
    
    if( $intTipo == 2 ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        if( sesion["logIn"] ){
            
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario ){
                
                $strFiltroPlaceSelectFavorito = ", usuario_place_favorito.id_usuario_place_favorito id_favorito";
                $strFiltroPlaceSelectFavoritoJoin = "LEFT JOIN usuario_place_favorito
                                                        ON  usuario_place_favorito.id_place = place.id_place
                                                        AND usuario_place_favorito.id_usuario = {$intIdUsuario}";
        
                        
            }
            
        }
        
        $arrProducto = array();
        $arrFiltroClasificacionPadre = array();
           /*               
        $strQuery = "SELECT id_clasificacion_padre_hijo,
                            nombre,
                            tag_en,
                            tag_es
                     FROM   clasificacion_padre_hijo
                     WHERE  id_clasificacion_padre = {$intKey}
                     
                     ";
        $strKeyClasificacionPadreHijoFiltro = "";
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre_hijo"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            
            $strKeyClasificacionPadreHijoFiltro .= ( $strKeyClasificacionPadreHijoFiltro == "" ? "" : "," ).$rTMP["id_clasificacion_padre_hijo"];
                                    
        }
        $objDBClass->db_free_result($qTMP);
          */
        $strQuery = "SELECT place.id_place,
                            place.url_place,
                            place.titulo,
                            place.descripcion,
                            place.estrellas,
                            place.url_logo,
                            place.precio_mediana_bajo,
                            place.precio_mediana_alto,
                            place_clasificacion_padre.id_clasificacion_padre,
                            place_galeria.id_place_galeria,
                            place_galeria.url_imagen
                            {$strFiltroPlaceSelectFavorito}
                     FROM   place
                                {$strFiltroPlaceSelectFavoritoJoin},
                            place_clasificacion_padre,
                            place_galeria
                     WHERE  place_clasificacion_padre.id_clasificacion_padre = {$intKey}
                     AND    place.id_place = place_clasificacion_padre.id_place
                     AND    place.id_place = place_galeria.id_place
                     AND    place.estado = 'A'
                     AND    place.id_ubicacion_lugar IN ({$strKeyUbicaionLugar})                     
                     ";
        $sinMin = 0;
        $sinMax= 0;
        $arrResultado = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrResultado[$rTMP["id_place"]]["id_place"] = $rTMP["id_place"];
            $arrResultado[$rTMP["id_place"]]["url_place"] = $rTMP["url_place"];
            $arrResultado[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
            $arrResultado[$rTMP["id_place"]]["descripcion"] = $rTMP["descripcion"];
            $arrResultado[$rTMP["id_place"]]["estrellas"] = $rTMP["estrellas"];
            $arrResultado[$rTMP["id_place"]]["url_logo"] = $rTMP["url_logo"];
            $arrResultado[$rTMP["id_place"]]["precio_mediana_bajo"] = $rTMP["precio_mediana_bajo"];
            $arrResultado[$rTMP["id_place"]]["precio_mediana_alto"] = $rTMP["precio_mediana_alto"];
            $arrResultado[$rTMP["id_place"]]["favorito"] = isset($rTMP["id_favorito"]) && intval($rTMP["id_favorito"]) ? "Y" : "N";
            $arrResultado[$rTMP["id_place"]]["galeria"][$rTMP["id_place_galeria"]]["url_imagen"] = $rTMP["url_imagen"];
            $arrResultado[$rTMP["id_place"]]["clasificacion_padre"][$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            
            if( $sinMin == 0 )
                $sinMin = $rTMP["precio_mediana_bajo"];
            
            if( $sinMax == 0 )
                $sinMax = $rTMP["precio_mediana_alto"];
                
            if( $rTMP["precio_mediana_bajo"] <= $sinMin )
                $sinMin = $rTMP["precio_mediana_bajo"];
            
            if( $rTMP["precio_mediana_alto"] >= $sinMax )
                $sinMax = $rTMP["precio_mediana_alto"];
                
        }
           
        $arrRango = array();
        $sinDiferencia = $sinMax - $sinMin;
                
        if( $sinDiferencia > 15 ){
            
            $intRango = $sinDiferencia / 3;
            $intRango = intval($intRango) ;
            
            $arrClasificacionPadreHijoRango[$rTMP["nombre"]]["tipo"] = 1; 
                                                                               
            $arrRango["bajo"]["min"] = $sinMin; 
            $arrRango["bajo"]["max"] = $sinMin + $intRango; 
                       
            $arrRango["medio"]["min"] = $sinMin + $intRango + 1; 
            $arrRango["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                            
            $arrRango["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
            $arrRango["alto"]["max"] = $sinMax; 
                            
        }   
        else{
            
            $arrRango["bajo"]["min"] = 0;
            $arrRango["bajo"]["max"] = 0;
                       
            $arrRango["medio"]["min"] = $sinMin;
            $arrRango["medio"]["max"] = $sinMax;
                            
            $arrRango["alto"]["min"] = 0;
            $arrRango["alto"]["max"] = 0;
            
        }
        
        if( count($arrResultado) > 0 ){
            
            while( $rTMP = each($arrResultado) ){
                
                if( $strFiltro == "C" && intval($strFiltroKey) ){
                                                     
                    if( !isset($rTMP["value"]["clasificacion_padre"][$strFiltroKey]) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        break;
                    }                          
                        
                }
                
                
                
                if( $rTMP["value"]["precio_mediana_bajo"] >= $arrRango["bajo"]["min"] 
                    && 
                    $rTMP["value"]["precio_mediana_bajo"] <= $arrRango["bajo"]["max"] ){
                    
                    $boolShowRangoBajo = true;
                    
                }
                else{
                    
                    if( $strRango == "1_B" ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }
                        
                }    
                
                if( $rTMP["value"]["precio_mediana_bajo"] <= $arrRango["medio"]["min"] 
                    && 
                    $rTMP["value"]["precio_mediana_alto"] >= $arrRango["medio"]["max"]  ){
                    
                    $boolShowRangoMedio = true;
                    
                }
                else{
                    
                    if( $strRango == "1_M" || $strRango == "2_M" || $strRango == "3_M" ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }
                        
                }   
                
                if( $rTMP["value"]["precio_mediana_alto"] >= $arrRango["alto"]["min"] 
                    && 
                    $rTMP["value"]["precio_mediana_alto"] <= $arrRango["alto"]["max"]  ){
                    
                    $boolShowRangoAlto = true;
                    
                }
                else{
                    
                    if( $strRango == "1_A" ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                            
                    }
                        
                }
                
                  /*
                //Bajo
                if( $strRango == "1_B" ){
                    
                    if( !( $rTMP["value"]["precio_mediana_bajo"] >= $arrRango["bajo"]["min"] 
                        && 
                        $rTMP["value"]["precio_mediana_alto"] <= $arrRango["bajo"]["max"] ) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }
                    
                }
                
                //Medio
                if( $strRango == "1_M" || $strRango == "2_M" || $strRango == "3_M" ){
                    
                    if( !( $rTMP["value"]["precio_mediana_bajo"] >= $arrRango["medio"]["min"] 
                        && 
                        $rTMP["value"]["precio_mediana_alto"] <= $arrRango["medio"]["max"] ) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }    
                    
                }
                
                //Alto
                if( $strRango == "1_A" ){
                    
                    if( !( $rTMP["value"]["precio_mediana_bajo"] >= $arrRango["alto"]["min"] 
                        && 
                        $rTMP["value"]["precio_mediana_alto"] <= $arrRango["alto"]["max"] ) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }
                        
                }    
                 */
            }
            
        }
        
        fntSetRastreoBusquedaUsuario($intIdUsuario, $intOrigen, $intTipo, "N", $intKey, $strTexto, $strRango, $intUbicacion, count( $arrResultado ),
                                     $strLenguaje, $strFiltro, $strFiltroKey, true, $objDBClass);
        
        
    } 
    
    if( $intTipo == 3 ){
        
        $intTipoA = $intTipo;
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $strFiltroSelectFavorito = "";
        $strFiltroSelectFavoritoJoin = "";
        if( sesion["logIn"] ){
            
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario ){
                
                $strFiltroSelectFavorito = ", usuario_place_producto_favorito.id_usuario_producto_favorito ";
                $strFiltroSelectFavoritoJoin = "LEFT JOIN usuario_place_producto_favorito
                                                ON  usuario_place_producto_favorito.id_place_producto = place_producto.id_place_producto
                                                AND usuario_place_producto_favorito.id_usuario = {$intIdUsuario}";
        
                        
            }
            
        }
        
        $arrFiltroClasificacionPadre = array();
            
        //Agrupacion para calsificacion padre hijo
        $strQuery = "SELECT clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                            clasificacion_padre_hijo.nombre,
                            place_producto.id_place_producto,
                            place_producto.nombre,
                            place_producto.descripcion,
                            place_producto.precio,
                            place_producto.logo_url,
                            place_producto.tipo,
                            place.id_place,
                            place.url_place,
                            place.titulo,
                            place.estrellas,
                            place_producto_galeria.id_place_producto_galeria,
                            place_producto_galeria.url_imagen
                            {$strFiltroSelectFavorito}
                     FROM   clasificacion_padre_hijo,
                            place_producto_clasificacion_padre_hijo,
                            place_producto
                                LEFT JOIN   place_producto_galeria
                                    ON  place_producto.id_place_producto = place_producto_galeria.id_place_producto
                                {$strFiltroSelectFavoritoJoin},
                            place
                     WHERE  clasificacion_padre_hijo.nombre = '{$intKey}'
                     AND    clasificacion_padre_hijo.id_clasificacion_padre_hijo = place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo 
                     AND    place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto
                     AND    place_producto.id_place = place.id_place
                     AND    place.id_ubicacion_lugar IN ({$strKeyUbicaionLugar})
                     ";  
        //Agrupacion para calsificacion padre hijo
        //if( sesion["webToken"] == "kg==" )
        //            drawdebug($strQuery);
        
        
        $sinMin = 0;
        $sinMax= 0;
        $arrResultado = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                                             
            $arrResultado[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
            $arrResultado[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
            $arrResultado[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
            $arrResultado[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
            $arrResultado[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
            $arrResultado[$rTMP["id_place_producto"]]["tipo"] = $rTMP["tipo"];
            $arrResultado[$rTMP["id_place_producto"]]["titulo"] = $rTMP["titulo"];
            $arrResultado[$rTMP["id_place_producto"]]["estrellas"] = $rTMP["estrellas"];
            $arrResultado[$rTMP["id_place_producto"]]["id_place"] = $rTMP["id_place"];
            $arrResultado[$rTMP["id_place_producto"]]["url_place"] = $rTMP["url_place"];
            $arrResultado[$rTMP["id_place_producto"]]["like"] = 0;
            $arrResultado[$rTMP["id_place_producto"]]["mi_like"] = "N";
            $arrResultado[$rTMP["id_place_producto"]]["no_like"] = 0;
            $arrResultado[$rTMP["id_place_producto"]]["no_mi_like"] = "N";
            $arrResultado[$rTMP["id_place_producto"]]["favorito"] = isset($rTMP["id_usuario_producto_favorito"]) && intval($rTMP["id_usuario_producto_favorito"]) ? "Y" : "N";
            
            if( intval($rTMP["id_place_producto_galeria"]) )
                $arrResultado[$rTMP["id_place_producto"]]["galeria"][$rTMP["id_place_producto_galeria"]]["url_imagen"] = $rTMP["url_imagen"];
                
        }
        $objDBClass->db_free_result($qTMP);
        
        $strQuery = "SELECT precio_min,
                            precio_max
                     FROM   ubicacion_rango_producto
                     WHERE  nombre = '{$intKey}'
                     AND    id_ubicacion_lugar IN ({$strKeyUbicaionLugar}) ";
        
        $qTMP = $objDBClass->db_consulta($strQuery);
        $rTMP = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        $sinMin = $rTMP["precio_min"];
        $sinMax = $rTMP["precio_max"];
        
        $arrRango = array();
        $sinDiferencia = $sinMax - $sinMin;
                
        if( $sinDiferencia > 15 ){
            
            $intRango = $sinDiferencia / 3;
            $intRango = intval($intRango) ;
            
            $arrRango["bajo"]["min"] = $sinMin; 
            $arrRango["bajo"]["max"] = $sinMin + $intRango; 
                       
            $arrRango["medio"]["min"] = $sinMin + $intRango + 1; 
            $arrRango["medio"]["max"] = $sinMin + ( $intRango * 2 ); 
                            
            $arrRango["alto"]["min"] = $sinMin + ( $intRango * 2 ) + 1; 
            $arrRango["alto"]["max"] = $sinMax; 
                            
        }   
        else{
            
            $arrRango["bajo"]["min"] = 0;
            $arrRango["bajo"]["max"] = 0;
                       
            $arrRango["medio"]["min"] = $sinMin;
            $arrRango["medio"]["max"] = $sinMax;
                            
            $arrRango["alto"]["min"] = 0;
            $arrRango["alto"]["max"] = 0;
            
        }
                       
                
        if( count($arrResultado) > 0 ){
            
            while( $rTMP = each($arrResultado) ){
                
                if( $rTMP["value"]["precio"] >= $arrRango["bajo"]["min"] 
                    && 
                    $rTMP["value"]["precio"] <= $arrRango["bajo"]["max"]  ){
                    
                    $boolShowRangoBajo = true;
                    
                }    
                
                if( $rTMP["value"]["precio"] >= $arrRango["medio"]["min"] 
                    && 
                    $rTMP["value"]["precio"] <= $arrRango["medio"]["max"]  ){
                    
                    $boolShowRangoMedio = true;
                    
                }    
                
                if( $rTMP["value"]["precio"] >= $arrRango["alto"]["min"] 
                    && 
                    $rTMP["value"]["precio"] <= $arrRango["alto"]["max"]  ){
                    
                    $boolShowRangoAlto = true;
                    
                }    
                
                
            }
            
            reset($arrResultado);
            while( $rTMP = each($arrResultado) ){
            
                //Bajo
                if( $strRango == "1_B" ){
                    
                    if( !( $rTMP["value"]["precio"] >= $arrRango["bajo"]["min"] 
                        && 
                        $rTMP["value"]["precio"] <= $arrRango["bajo"]["max"] ) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }
                    
                }
                
                //Medio
                if( $strRango == "1_M" || $strRango == "2_M" || $strRango == "3_M" ){
                    
                    if( !( $rTMP["value"]["precio"] >= $arrRango["medio"]["min"] 
                        && 
                        $rTMP["value"]["precio"] <= $arrRango["medio"]["max"] ) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }    
                    
                }
                
                //Alto
                if( $strRango == "1_A" ){
                    
                    if( !( $rTMP["value"]["precio"] >= $arrRango["alto"]["min"] 
                        && 
                        $rTMP["value"]["precio"] <= $arrRango["alto"]["max"] ) ){
                        
                        unset($arrResultado[$rTMP["key"]]);
                        
                    }
                        
                }    
                
            }
            
            
            if( count($arrResultado) > 0 ){
                
                reset($arrResultado);
                $strPlaceProducto = "";
                while( $rTMP = each($arrResultado) ){
                    
                    $strPlaceProducto .= ( $strPlaceProducto == "" ? "" : "," ).$rTMP["key"];
                        
                }
                reset($arrResultado);
                
                
                              
                $strQuery = "SELECT id_place_producto,
                                    COUNT(*) contador_like,
                                    MAX(CASE WHEN id_usuario = {$intIdUsuario} THEN 'Y' ELSE 'N' END) mi_like
                             FROM   place_producto_like
                             WHERE  id_place_producto IN ({$strPlaceProducto})
                             GROUP BY id_place_producto";
                            
                $qTMP = $objDBClass->db_consulta($strQuery);
                while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
                                                                  
                    $arrResultado[$rTMP["id_place_producto"]]["like"] = $rTMP["contador_like"];
                    $arrResultado[$rTMP["id_place_producto"]]["mi_like"] = $rTMP["mi_like"];
                    
                }
                $objDBClass->db_free_result($qTMP);
                
                
                $strQuery = "SELECT id_place_producto,
                                    COUNT(*) contador_like,
                                    MAX(CASE WHEN id_usuario = {$intIdUsuario} THEN 'Y' ELSE 'N' END) mi_like
                             FROM   place_producto_no_like
                             WHERE  id_place_producto IN ({$strPlaceProducto})
                             GROUP BY id_place_producto";
                            
                $qTMP = $objDBClass->db_consulta($strQuery);
                while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
                                                                  
                    $arrResultado[$rTMP["id_place_producto"]]["no_like"] = $rTMP["contador_like"];
                    $arrResultado[$rTMP["id_place_producto"]]["no_mi_like"] = $rTMP["mi_like"];
                    
                }
                $objDBClass->db_free_result($qTMP);
                
            }
            
                
            
        }
        
        fntSetRastreoBusquedaUsuario($intIdUsuario, $intOrigen, $intTipoA, "N", $intKey, $strTexto, $strRango, $intUbicacion, count( $arrResultado ),
                                     $strLenguaje, $strFiltro, $strFiltroKey, true, $objDBClass);
        
    } 
    
    if( ( $intTipo == 4 || $intTipo == 6 ) && !empty($strTexto) ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $strFiltroSelectFavorito = "";
        $strFiltroSelectFavoritoJoin = "";
        
        $strFiltroPlaceSelectFavorito = "";
        $strFiltroPlaceSelectFavoritoJoin = "";
        
        if( sesion["logIn"] ){
            
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario ){
                
                $strFiltroSelectFavorito = ", usuario_place_producto_favorito.id_usuario_producto_favorito id_favorito ";
                $strFiltroSelectFavoritoJoin = "LEFT JOIN usuario_place_producto_favorito
                                                ON  usuario_place_producto_favorito.id_place_producto = place_producto.id_place_producto
                                                AND usuario_place_producto_favorito.id_usuario = {$intIdUsuario}";
        
                    
                $strFiltroPlaceSelectFavorito = ", usuario_place_favorito.id_usuario_place_favorito id_favorito";
                $strFiltroPlaceSelectFavoritoJoin = "LEFT JOIN usuario_place_favorito
                                                        ON  usuario_place_favorito.id_place = place.id_place
                                                        AND usuario_place_favorito.id_usuario = {$intIdUsuario}";
        
                        
            }
            
        }
        
        $arrClasificacionPadre = array();
        $arrFiltroClasificacionPadre = array();
        $arrResultado = array();
        
        $strQuery = "
                     
                     SELECT clasificacion_padre_hijo.id_clasificacion_padre_hijo campo_1,
                            clasificacion_padre_hijo.nombre campo_2,
                            place_producto.id_place_producto campo_3,
                            place_producto.nombre campo_4,
                            place_producto.descripcion campo_5,
                            place_producto.precio campo_6,
                            place_producto.logo_url campo_7,
                            place_producto.tipo campo_8,
                            place.id_place campo_9,
                            place.titulo campo_10,
                            place_producto_galeria.id_place_producto_galeria campo_11,
                            place_producto_galeria.url_imagen campo_12,
                            place.estrellas campo_13,
                            place.url_place campo_14,
                            '1' tipoQuery,
                            place_producto.nombre texto_autocomplete
                            {$strFiltroSelectFavorito}
                     FROM   place_producto
                                LEFT JOIN   place_producto_galeria
                                    ON  place_producto.id_place_producto = place_producto_galeria.id_place_producto
                                LEFT JOIN place_producto_clasificacion_padre_hijo
                                    ON  place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto
                                
                                LEFT JOIN clasificacion_padre_hijo
                                    ON  clasificacion_padre_hijo.id_clasificacion_padre_hijo = place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo 
                                {$strFiltroSelectFavoritoJoin}
                                ,
                            place
                     WHERE  place_producto.nombre LIKE '%{$strTexto}%'
                    
                     
                     AND    place_producto.id_place = place.id_place
                     AND    place.id_ubicacion_lugar IN ({$strKeyUbicaionLugar})
                     
                     UNION ALL        
                     
                     SELECT place.id_place campo_1,
                            place.titulo campo_2,
                            place.descripcion campo_3,
                            place.url_logo campo_4,
                            place.precio_mediana_bajo campo_5,
                            place.precio_mediana_alto campo_6,
                            place_clasificacion.id_clasificacion campo_7,
                            place_clasificacion_padre.id_clasificacion_padre campo_8,
                            place_galeria.id_place_galeria campo_9,
                            place_galeria.url_imagen campo_10,
                            '' campo_11,
                            '' campo_12,
                            place.estrellas campo_13,
                            place.url_place campo_14,
                            '2' tipoQuery,
                            place.texto_autocomplete
                            {$strFiltroPlaceSelectFavorito}                            
                     FROM   place
                                {$strFiltroPlaceSelectFavoritoJoin},
                            place_clasificacion,
                            place_clasificacion_padre,
                            place_galeria
                     WHERE  place.texto_autocomplete LIKE '%{$strTexto}%'
                     AND    place.id_place = place_clasificacion_padre.id_place
                     AND    place.id_place = place_clasificacion.id_place
                     AND    place.id_place = place_galeria.id_place
                     AND    place.estado = 'A'
                     AND    place.id_ubicacion_lugar IN ({$strKeyUbicaionLugar}) 
                     
                     ORDER BY texto_autocomplete";
        
        $arrPlaceProducto = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                
            if( $rTMP["tipoQuery"] == 1 ){
                
                $arrPlaceProducto[$rTMP["campo_3"]] = $rTMP["campo_3"];
                
                $strKey = $rTMP["tipoQuery"]."_".$rTMP["campo_3"];
                
                $arrResultado[$strKey]["tipoQuery"] = $rTMP["tipoQuery"];    
                $arrResultado[$strKey]["id_place_producto"] = $rTMP["campo_3"];    
                $arrResultado[$strKey]["nombre"] = $rTMP["campo_4"];    
                $arrResultado[$strKey]["descripcion"] = $rTMP["campo_5"];    
                $arrResultado[$strKey]["precio"] = $rTMP["campo_6"];    
                $arrResultado[$strKey]["logo_url"] = $rTMP["campo_7"];    
                $arrResultado[$strKey]["tipo"] = $rTMP["campo_8"];
                $arrResultado[$strKey]["id_place"] = $rTMP["campo_9"];
                $arrResultado[$strKey]["titulo"] = $rTMP["campo_10"];
                $arrResultado[$strKey]["estrellas"] = $rTMP["campo_13"];
                $arrResultado[$strKey]["url_place"] = $rTMP["campo_14"];
                $arrResultado[$strKey]["like"] = 0;
                $arrResultado[$strKey]["mi_like"] = "N";
                $arrResultado[$strKey]["no_like"] = 0;
                $arrResultado[$strKey]["no_mi_like"] = "N";
                $arrResultado[$strKey]["favorito"] = isset($rTMP["id_favorito"]) && intval($rTMP["id_favorito"]) ? "Y" : "N";
                    
                if( intval($rTMP["campo_11"]) )
                    $arrResultado[$strKey]["galeria"][$rTMP["campo_11"]]["url_imagen"] = $rTMP["campo_12"];   
                 
                if( intval($rTMP["campo_1"]) ){
                    
                    $arrResultado[$strKey]["clasificacion_padre_hijo"][$rTMP["campo_1"]]["id_clasificacion_padre_hijo"] = $rTMP["campo_1"];    
                    $arrResultado[$strKey]["clasificacion_padre_hijo"][$rTMP["campo_1"]]["nombre"] = $rTMP["campo_2"];    
                    
                }
                                    
            }    
                
            if( $rTMP["tipoQuery"] == 2 ){
                
                $strKey = $rTMP["tipoQuery"]."_".$rTMP["campo_3"];
                
                $arrResultado[$strKey]["tipoQuery"] = $rTMP["tipoQuery"];    
                $arrResultado[$strKey]["id_place"] = $rTMP["campo_1"];    
                $arrResultado[$strKey]["titulo"] = $rTMP["campo_2"];  
                $arrResultado[$strKey]["estrellas"] = $rTMP["campo_13"];
                $arrResultado[$strKey]["url_place"] = $rTMP["campo_14"];
                $arrResultado[$strKey]["descripcion"] = $rTMP["campo_3"];    
                $arrResultado[$strKey]["url_logo"] = $rTMP["campo_4"];    
                $arrResultado[$strKey]["precio_mediana_bajo"] = $rTMP["campo_5"];    
                $arrResultado[$strKey]["precio_mediana_alto"] = $rTMP["campo_6"];
                $arrResultado[$strKey]["like"] = 0;
                $arrResultado[$strKey]["mi_like"] = "N";
                $arrResultado[$strKey]["no_like"] = 0;
                $arrResultado[$strKey]["no_mi_like"] = "N";    
                $arrResultado[$strKey]["favorito"] = isset($rTMP["id_favorito"]) && intval($rTMP["id_favorito"]) ? "Y" : "N";
                
                $arrResultado[$strKey]["clasificacion"][$rTMP["campo_7"]]["id_clasificacion"] = $rTMP["campo_7"];
                    
                $arrResultado[$strKey]["clasificacion_padre"][$rTMP["campo_8"]]["id_clasificacion_padre"] = $rTMP["campo_8"];    
                
                $arrResultado[$strKey]["galeria"][$rTMP["campo_9"]]["id_place_galeria"] = $rTMP["campo_9"];    
                $arrResultado[$strKey]["galeria"][$rTMP["campo_9"]]["url_imagen"] = $rTMP["campo_10"];    
                
                $arrClasificacionPadre[$rTMP["campo_7"]] = $rTMP["campo_7"];    
            }    
                        
        }
        $objDBClass->db_free_result($qTMP);
        
        
        if( count($arrClasificacionPadre) > 0 ){
            
            $strClasificacionPadre = implode(",", $arrClasificacionPadre);
            $strQuery = "SELECT id_clasificacion_padre,
                                nombre,
                                tag_en,
                                tag_es
                         FROM   clasificacion_padre
                         WHERE  id_clasificacion IN ({$strClasificacionPadre})
                         
                         ";
            $strKeyClasificacionPadreFiltro = "";
            $qTMP = $objDBClass->db_consulta($strQuery);
            while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                
                $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
                $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
                $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
                
                $strKeyClasificacionPadreFiltro .= ( $strKeyClasificacionPadreFiltro == "" ? "" : "," ).$rTMP["id_clasificacion_padre"];
                                        
            }
            $objDBClass->db_free_result($qTMP);
            
            
        }
        
        if( count($arrPlaceProducto) > 0 ){
            
            $strPlaceProducto = implode(",", $arrPlaceProducto);
            
            $strQuery = "SELECT id_place_producto,
                                COUNT(*) contador_like,
                                MAX(CASE WHEN id_usuario = {$intIdUsuario} THEN 'Y' ELSE 'N' END) mi_like
                         FROM   place_producto_like
                         WHERE  id_place_producto IN ({$strPlaceProducto})
                         GROUP BY id_place_producto";
                        
            $qTMP = $objDBClass->db_consulta($strQuery);
            while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
                                   
                $strKey = "1_".$rTMP["id_place_producto"];
                                           
                $arrResultado[$strKey]["like"] = $rTMP["contador_like"];
                $arrResultado[$strKey]["mi_like"] = $rTMP["mi_like"];
                
            }
            $objDBClass->db_free_result($qTMP);
            
            
            $strQuery = "SELECT id_place_producto,
                                COUNT(*) contador_like,
                                MAX(CASE WHEN id_usuario = {$intIdUsuario} THEN 'Y' ELSE 'N' END) mi_like
                         FROM   place_producto_no_like
                         WHERE  id_place_producto IN ({$strPlaceProducto})
                         GROUP BY id_place_producto";
                        
            $qTMP = $objDBClass->db_consulta($strQuery);
            while( $rTMP = $objDBClass->db_fetch_array($qTMP)  ){
                      
                $strKey = "1_".$rTMP["id_place_producto"];
                                                              
                $arrResultado[$strKey]["no_like"] = $rTMP["contador_like"];
                $arrResultado[$strKey]["no_mi_like"] = $rTMP["mi_like"];
                
            }
            $objDBClass->db_free_result($qTMP);
            
            
        }  
        
        fntSetRastreoBusquedaUsuario($intIdUsuario, $intOrigen, $intTipo, "N", $intKey, $strTexto, $strRango, $intUbicacion, count( $arrResultado ),
                                     $strLenguaje, $strFiltro, $strFiltroKey, true, $objDBClass);
                  
        
    }
    
    if( ( $intTipo == 5 || $intTipo == 7 ) && intval($intKey) ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $strFiltroSelectFavorito = "";
        $strFiltroSelectFavoritoJoin = "";
        if( sesion["logIn"] ){
            
            $intIdUsuario = intval(fntCoreDecrypt(sesion["webToken"]));
            
            if( $intIdUsuario ){
                
                $strFiltroSelectFavorito = ", usuario_place_producto_favorito.id_usuario_producto_favorito ";
                $strFiltroSelectFavoritoJoin = "LEFT JOIN usuario_place_producto_favorito
                                                ON  usuario_place_producto_favorito.id_place_producto = place_producto.id_place_producto
                                                AND usuario_place_producto_favorito.id_usuario = {$intIdUsuario}";
        
                        
            }
            
        }
        
        $strQuery = "SELECT place_producto.id_place_producto,
                            categoria_especial.texto_google_place,
                            place_producto.nombre,
                            place_producto.descripcion,
                            place_producto.precio,
                            place_producto.tipo,
                            place_producto.logo_url,
                            place.id_place,
                            place.url_place,
                            place.titulo,
                            place.estrellas,
                            place_producto_galeria.id_place_producto_galeria,
                            place_producto_galeria.url_imagen
                            {$strFiltroSelectFavorito}
                     FROM   sol_categoria_especial,
                            place_producto
                                LEFT JOIN place_producto_galeria
                                    ON  place_producto_galeria.id_place_producto = place_producto.id_place_producto
                                {$strFiltroSelectFavoritoJoin} ,
                            place,
                            categoria_especial
                     WHERE  sol_categoria_especial.id_categoria_especial = {$intKey}
                     AND    sol_categoria_especial.estado IN('A')
                     AND    sol_categoria_especial.id_place_producto = place_producto.id_place_producto
                     AND    place_producto.id_place = place.id_place
                     AND    sol_categoria_especial.id_categoria_especial = categoria_especial.id_categoria_especial
                     AND    place.id_ubicacion_lugar IN ({$strKeyUbicaionLugar})
                     ";
        $strTextoAutoComplete = "";
        $arrResultado = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $strTextoAutoComplete = $rTMP["texto_google_place"];
            
            $arrResultado[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
            $arrResultado[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
            $arrResultado[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
            $arrResultado[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
            $arrResultado[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
            $arrResultado[$rTMP["id_place_producto"]]["tipo"] = $rTMP["tipo"];
            $arrResultado[$rTMP["id_place_producto"]]["titulo"] = $rTMP["titulo"];
            $arrResultado[$rTMP["id_place_producto"]]["estrellas"] = $rTMP["estrellas"];
            $arrResultado[$rTMP["id_place_producto"]]["like"] = 0;
            $arrResultado[$rTMP["id_place_producto"]]["mi_like"] = "N";
            $arrResultado[$rTMP["id_place_producto"]]["no_like"] = 0;
            $arrResultado[$rTMP["id_place_producto"]]["no_mi_like"] = "N";
            $arrResultado[$rTMP["id_place_producto"]]["id_place"] = $rTMP["id_place"];
            $arrResultado[$rTMP["id_place_producto"]]["favorito"] = isset($rTMP["id_usuario_producto_favorito"]) && intval($rTMP["id_usuario_producto_favorito"]) ? "Y" : "N";
            
            if( intval($rTMP["id_place_producto_galeria"]) )
                $arrResultado[$rTMP["id_place_producto"]]["galeria"][$rTMP["id_place_producto_galeria"]]["url_imagen"] = $rTMP["url_imagen"];
           
                
        }
        
        $objDBClass->db_free_result($qTMP);
        
        fntSetRastreoBusquedaUsuario($intIdUsuario, $intOrigen, $intTipo, "N", $intKey, $strTexto, $strRango, $intUbicacion, count( $arrResultado ),
                                     $strLenguaje, $strFiltro, $strFiltroKey, true, $objDBClass);
        
        
    } 
    
    $strHeightPX = "height: 200px;";
    
    if( $boolMovil )  
        $strHeightPX = "height: 180px;";
    
    $strColorHex = "3E53AC";
    
    $boolFiltroRango = false;
    $boolFiltroCategoria = false;
    if( $boolShowRangoBajo || $boolShowRangoMedio || $boolShowRangoAlto ){
        
        $boolFiltroRango = true;
    
    }
    
    if( isset($arrFiltroClasificacionPadre) && count($arrFiltroClasificacionPadre) > 0 && $intTipo != 4 ){
        
        $boolFiltroCategoria = true;
      
    }
        
        
    if( $boolFiltroRango ){
        ?>
        
        <div class="row justify-content-center p-0 m-0">
    
            <div class="col-12 col-lg-4 p-0 m-0">
                
                <table style="width: 100%;">
                    
                    <tr>
                    
                        <?php
                        
                        $strTextRango = "Rangos";
                        
                        if( 
                            ( $boolShowRangoAlto && $boolShowRangoMedio ) || ( $boolShowRangoAlto && $boolShowRangoBajo )    ||
                            ( $boolShowRangoMedio && $boolShowRangoBajo ) || ( $boolShowRangoAlto && $boolShowRangoMedio ) 
                        ){
                            
                            $strClase = "botonRangoNoSelect";
                            if( $strRango == "all" )
                                $strClase = "botonRangoSelect";
                            
                            ?>
                            
                            <td >
                                <button onclick="fntSetRangoFiltroModal('all', this)" class="btn btn-block m-0 btn-sm botonRango <?php print $strClase?>" style="">
                                    <?php print lang["todos"]?>
                                </button>
                                <input type="hidden" id="hidFiltroAll" name="hidFiltroAll" value="all">
                            
                            </td>
                            <td style="vertical-align: middle; width: 10%;">
                                <i class="fa fa-long-arrow-right"></i>
                            </td>        
                            <?php
                        }
                        
                        if( (  $strRango == "2_M" || $strRango == "3_M" ) && $boolShowRangoMedio ){
                            
                            $strClase = "botonRangoNoSelect";
                            if( $strRango == "2_M" || $strRango == "3_M" )
                                $strClase = "botonRangoSelect";
                            
                    
                            ?>
                            
                            <td >
                                <button onclick="fntSetRangoFiltroModal('<?php print $strRango;?>', this)" class="btn btn-block m-0 btn-sm botonRango <?php print $strClase?>" >
                                    Q<?php print number_format($arrRango["medio"]["min"], 0)?>
                                </button>
                                <input type="hidden" id="hidFiltroMedio" name="hidFiltroMedio" value="<?php print $strRango;?>">
                            </td>
                            <?php
                        }
                        else{
                            
                            if( $boolShowRangoAlto ){
                                
                                $strClase = "botonRangoNoSelect";
                                if( $strRango == "1_A" )
                                    $strClase = "botonRangoSelect";
                                
                        
                                ?>
                                        
                                <td >
                                    <button onclick="fntSetRangoFiltroModal('1_A', this)" class="btn btn-block m-0 btn-sm botonRango <?php print $strClase?>" >
                                        Q<?php print number_format($arrRango["alto"]["min"], 0)?>
                                    </button>
                                    <input type="hidden" id="hidFiltroAlto" name="hidFiltroAlto" value="1_A">
                                </td>
                                <td style="vertical-align: middle; width: 10%;">
                                    <i class="fa fa-long-arrow-right"></i>
                                </td>                                       
                            
                                <?php
                            }
                            
                            if( $boolShowRangoMedio ){
                                
                                $strClase = "botonRangoNoSelect";
                                if( $strRango == "1_M" )
                                    $strClase = "botonRangoSelect";
                                
                        
                                ?>
                                <td >
                                    <button onclick="fntSetRangoFiltroModal('1_M', this)" class="btn btn-block m-0 btn-sm botonRango <?php print $strClase?>" >
                                        Q<?php print number_format($arrRango["medio"]["min"], 0)?>
                                    </button>
                                    <input type="hidden" id="hidFiltroMedio" name="hidFiltroMedio" value="1_M">
                                </td>
                                <td style="vertical-align: middle; width: 10%;">
                                    <i class="fa fa-long-arrow-right"></i>
                                </td>                                       
                            
                                <?php
                            }
                            
                            if( $boolShowRangoBajo ){
                                                                    
                                $strClase = "botonRangoNoSelect";
                                if( $strRango == "1_B" )
                                    $strClase = "botonRangoSelect";
                                
                                ?>
                                <td >
                                    <button onclick="fntSetRangoFiltroModal('1_B', this)" class="btn btn-block m-0 btn-sm botonRango <?php print $strClase?>" >
                                        Q<?php print number_format($arrRango["bajo"]["min"], 0)?>
                                    </button>
                                    <input type="hidden" id="hidFiltroBajo" name="hidFiltroBajo" value="1_B">
                                </td>
                            
                                <?php
                            }
                            
                            
                        }                    
                        
                        
                        ?>
                    
                    </tr>
                    
                </table>
                        
            </div>
        </div>
        
        <script>
            
            function fntShowModalFiltroRango(){
                
                $("#mlFiltroRango").modal("show");    
                
            }
            
            function fntSetRangoFiltroModal(intKey, obj){
                
                $("#hidRango").val(intKey);
                
                $(".botonRango").each(function(){
                    
                                       
                    $(this).removeClass("botonRangoSelect");
                    $(this).addClass("botonRangoNoSelect");
                   
                    
                }); 
                
                $(obj).removeClass("botonRangoNoSelect");
                $(obj).addClass("botonRangoSelect");
                                      
                fntShowContenido();
                                    
            }
            
            
        </script>
        
        <?php
    }    
    
    if( $boolFiltroCategoria ){
        ?>
        <!-- Modal -->
        <div class="modal fade" id="mlFiltroCategoria" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body p-1">
                        
                        <div class="row justify-content-center">
                            
                            <div class="col-10 p-1 m-0" onclick="fntSetFiltroRango('all', '<?php print lang["todas_la_categorias"]?>');" style="cursor: pointer;">
                                    
                                <button class="btn btn-block botonRangoNoSelect"><?php print lang["todas_la_categorias"]?></button>                                        
                                
                            </div>
                            <?php
            
                            $strSelectTextoCategoria = isset($arrFiltroClasificacionPadre[$strFiltroKey]) ? $arrFiltroClasificacionPadre[$strFiltroKey]["texto"] : lang["categorias"];
                            $intCount = 1;
                            reset($arrFiltroClasificacionPadre);
                            while( $rTMP = each($arrFiltroClasificacionPadre) ){
                                
                                
                                ?>
                                <div class="col-5 p-1 m-0" onclick="fntSetFiltroRango('<?php print $rTMP["key"]?>', '<?php print $rTMP["value"]["texto"]?>');" style="cursor: pointer;">
                                    
                                    <button class="btn btn-block botonRangoNoSelect"><?php print $rTMP["value"]["texto"]?></button>                                        
                                    
                                </div>
                                    
                                <?php
                                $intCount++;
                                    
                            }                            
                            
                            ?>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center p-0 m-0">
    
            <div  class="col-12 col-lg-4 p-0 m-0 pt-2" style="cursor: pointer;">
                
                <button onclick="fntShowModalFiltroCategoria()" class="btn btn-block m-0 btn-sm botonRangoNoSelect" style="">
                    
                    <span id="spTextoFiltroCatetoria">
                    
                        <?php print $strSelectTextoCategoria?>
                        <i class="fa fa-angle-down"></i>
                
                    </span>
                
                </button>
                            
                
                
            </div>
        </div>
        <script>
            
            function fntShowModalFiltroCategoria(){
                
                $("#mlFiltroCategoria").modal("show");    
                
            }
            
            function fntSetFiltroRango(intKey, strTexto){
                
                $("#hidFiltroTipo").val("C");
                $("#hidFiltroKey").val(intKey);
        
        
                $("#spTextoFiltroCatetoria").html(strTexto);    
                
                $("#mlFiltroCategoria").modal("hide");    
                
                $('#mlFiltroCategoria').on('hidden.bs.modal', function (e) {
                    
                    fntShowContenido();
                    
                });      
                
                                    
            }
            
        </script>
        <?php
    }
    
    ?>    
     
    <style>
                    
        .botonRangoNoSelect{
            color: #3E53AC; background: white; border: 1px solid #3E53AC;    
        }
        
        .botonRangoSelect{
            color: white; background: #3E53AC; border: 1px solid #3E53AC;    
        }
        
        .botonRangoSelect:hover{
            color: white; background: #3E53AC; border: 1px solid #3E53AC;    
        }

        .text-background-theme {
            background-color: #<?php print $strColorHex?> !important;
            
        }
    </style>
    <div  class="row justify-content-center pt-1 p-0 m-0 mb-3 wow fadeInUp" id="">
        <div class="col-12 col-lg-11 ">
            <div class="row" id="myTable">
                <?php
                
                $intCount = 1;
                
                if( $intTipo == 1 || $intTipo == 2 ){
                    
                    
                    reset($arrResultado);
                    while( $rTMP = each($arrResultado) ){
                         
                        fntDrawTrProductoPlace($rTMP, $strHeightPX, $strColorHex, $intCount, $arrFiltroClasificacionPadre);
                            
                        $intCount++;
                    }
                    
                }
                
                if( count( $arrResultado ) > 0 && ( $intTipo == 3 ||  $intTipo == 5 ||  $intTipo == 7 )  ){
                    
                    reset($arrResultado);
                    while( $rTMP = each($arrResultado) ){
                        
                        fntDrawTrProducto($rTMP, $strHeightPX, $strColorHex, $intCount);
                            
                        $intCount++;
                    }    
                    
                }
                
                if( ( $intTipo == 4 || $intTipo == 6 )  && isset($arrResultado) ){
                    
                    reset($arrResultado);
                    while( $rTMP = each($arrResultado) ){
                        
                        if( $rTMP["value"]["tipoQuery"] == 1 ){
                            
                            fntDrawTrProducto($rTMP, $strHeightPX, $strColorHex, $intCount);
                                
                        }
                        if( $rTMP["value"]["tipoQuery"] == 2 ){
                            
                            fntDrawTrProductoPlace($rTMP, $strHeightPX, $strColorHex, $intCount, $arrFiltroClasificacionPadre);
                                
                        }
                        
                        $intCount++;
                    }    
                    
                }
                        
                ?>
        
            </div>
        </div>
    </div>
        
    <script>
        
        
        $(document).ready(function() { 
            
            $(".rdCategoria").each(function (){
                
                $(this).change(function (){
                    
                    $("#hidFiltroTipo").val("C");
                    $("#hidFiltroKey").val($(this).val());
            
                    fntSetParametroFiltro()
            
                });
                
            });
        
                 
            var sinWindowsWidth = $(window).width();
            
            $(".imgCuadroRow").each(function (){
                
                $(this).width( ( sinWindowsWidth * ( 50 / 100 ) )+'px');
                    
            }); 
            
            
            
            <?php
            
            if( isset($arrResultado) && count($arrResultado) <= 10 && $intTipo >= 1 ){
                ?>
                fntAddPlace();
                <?php
            }
            
            if( !$boolMovil ){
                ?>
                //fntAddPlace();
                
                <?php
            }
            
            ?>                
        
                
        });
        
        function fntSetParametroFiltro(){
                   
            $("#hidRango").val($("#slcRango").val());
            
            fntShowContenido();
            
        }
        
        function fntShowRow(strKey){
            
            window.open("placepp.php?p="+strKey);
            
        }
        
    </script>  
    <?php
    
    die();   
    
}

fntDrawHeaderPublico($boolMovil);

$arrCatalogoFondo = fntCatalogoFondoPlacePublico();

$strHeightPX = "height: 200px;";

if( $boolMovil )  
    $strHeightPX = "height: 180px;";
    
    
?>   
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>
        
<input type="hidden" id="hidOrigen" value="<?php print $intOrigen;?>">
<input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intKey);?>">
<input type="hidden" id="hidTipo" value="<?php print $intTipo;?>">
<input type="hidden" id="hidTexto" value="<?php print $strTexto;?>">
<input type="hidden" id="hidRango" value="<?php print $strRango;?>">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">
<input type="hidden" id="hidToken" value="0">

<div class="container-fluid m-0 p-0" >
    
    <div class="row m-0 p-0">
    
        <div class="col-12 p-0 m-0 text-center bg-white" >
                  <!--
            <img src="<?php print  $arrCatalogoFondo[1]["url_fondo"]?>" style="width: 100%; height: auto;">
            
               -->
            <div class="m-0 p-0 pt-2 pt-lg-4  text-center" style=""> 
                       
                <?php
                include "core/dbClass.php";
                $objDBClass = new dbClass();
        
                $arrAlerta = fntGetNotificacionUsuario(true, $objDBClass, true);
                
                fntDrawHeaderPrincipal(2, true, $objDBClass, $arrAlerta);
                $arrUbicacion = fntUbicacion(true, $objDBClass);
                
                ?>      
                <div class="d-none d-md-block">                        
                            
                    <div class="row p-0 m-0 justify-content-center" style="margin-top: 2% !important;">
                        <div class="col-11 col-lg-6 p-0 m-0 pt-lg-0 pt-4">
                              
                            <div class="input-group mb-3 rounded" style="">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-white border-0 pr-0" >
                                        <i class="fa fa-search" style="color: #3E53AC;"></i>
                                    </span>
                                </div>
                                
                                <input type="text" value="<?php print $strTexto?>" id="txtAutocomplete" name="txtAutocomplete" class="form-control nofocusBorder border-0" placeholder="What are you looking for?" aria-label="What are you looking for?" aria-describedby="basic-addon2">
                                
                                
                                <div class="input-group-append col-3 m-0 p-0">
                                    <select id="slcUbicacion" name="slcUbicacion" class="form-control text-center rounded border-0 nofocusBorder" onchange="fntUbicacionCookie(this.value);">
                                        <?php
                                        
                                        while( $rTMP = each($arrUbicacion) ){
                                            
                                            $strSelected = sesion["ubicacion"] == $rTMP["key"] ? "selected" : "";
                                            ?>
                                            
                                            <option <?php print $strSelected;?> value="<?php print $rTMP["key"]?>"><?php print sesion["lenguaje"] == "en" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"]?></option>
                                            
                                            <?php
                                            
                                        }
                                        
                                        ?>
                                    </select>
                                </div>
                            
                                <div class="input-group-append">
                                    <select id="slcLenguaje" name="slcLenguaje" class="form-control text-center rounded border-0 nofocusBorder" onchange="fntSetLenguajeCookie(this.value);">
                                        <option value="en" <?php print sesion["lenguaje"] == "en" ? "selected" : ""?> >en</option>
                                        <option value="es" <?php print sesion["lenguaje"] == "es" ? "selected" : ""?> >es</option>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                <div class="p-1" id="divContendo">
                
                </div>
                
                <?php
                        
                fntDrawFooter();
                
                ?>
                        
                
            </div>
                   
        </div>
    </div>
    
    <?php
        
    fntShowBarNavegacionFooter(false, "3E53AC" , $arrAlerta);
        
    
    ?>
    
    
</div>
            


<script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
<script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>

<script src="dist/js/jsAntigua.js"></script>
    
<script >
    
    var boolCorriendoAjax = true;
    var table;
    var boolDetenerBusqueda = false;
    var strPlaceNextPlaceToken = "";
    var intLastRangoScrool = 0;
        
             
               
    $(document).ready(function() { 
        
        
        $("#txtcoreQueBuscar").val("<?php print $strTexto?>");
        
        var xhr;
        $('input[name="txtAutocomplete"]').autoComplete({
            minChars: 2,
            source: function(term, suggest){
                
                try {
                    xhr.abort();
                }
                catch(error) {
                }

                xhr = $.ajax({
                    url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete").val()+"&len="+$("#slcLenguaje").val(), 
                    dataType : "json",
                    success: function(result){
                        suggest(result);
                    }
                });
                
            },
            renderItem: function (item, search){
                
                //console.log(item);
                
                return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-identificador_2="'+item["identificador_2"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
            
            },
            onSelect: function(e, term, item){
                
                if( item.data("tipo") == "5" ){
                    
                    str = "/"+item.data("identificador_2");
                    location.href = str;
                    return false;
                    
                }
                else if( item.data("tipo") == "4" ){
                    
                    str = "/"+item.data("identificador_2");
                        
                    location.href = str;
                    return false;
                    
                }
                else{
                    
                    str = "s.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                    location.href = str;
                    
                }
                
                return false;
                
            }
        }).change(function (){
            
            str = "s.php?o=1&key=0&t=4&q="+$("#txtAutocomplete").val()+"&ran=all";
            location.href = str;
                    
        });
             
        
        fntShowContenido();  
                
                
        const offset = 80 / 100;
        $(window).scroll(function() {
           // modificamos ligeramente la condición
           
           if($(window).scrollTop() + $(window).height() > $(document).height() * offset) {
               
               if ( !boolCorriendoAjax )
                fntAddPlace();
               // llamada a Ajax aquí
           }
        });
        
        
    });   
    
    function fntShowContenido(){
        
        $.ajax({                                                                                                                                                    
            url: "s.php?drawCotenido=true&o=2&key="+$("#hidKey").val()+"&t="+$("#hidTipo").val()+"&q="+$("#hidTexto").val()+"&ran="+$("#hidRango").val()+"&fil_tipo="+$("#hidFiltroTipo").val()+"&fil_key="+$("#hidFiltroKey").val()+"&len="+$("#slcLenguaje").val(), 
            success: function(result){
                
                $("#divContendo").html(result);
                
            }
        });
            
    }
    
    function fntDisplayIconoFavorito(idObjSi, idObjNo, boolFavorito, strKey){
        
        if( boolFavorito ){
            
            $("#"+idObjSi).show();
            $("#"+idObjNo).hide();
            
        }
        else{
            
            $("#"+idObjSi).hide();
            $("#"+idObjNo).show();
            
        }
        
        $.ajax({
            url: "servicio_core.php?servicio=setProductoFavorito&key="+strKey+"&fav="+( boolFavorito ? "Y" : "N" ), 
            dataType : "json",
            success: function(result){
            }
        });
        
                    
    } 
    
    function fntDisplayIconoFavoritoPlace(idObjSi, idObjNo, boolFavorito, strKey){
        
        if( boolFavorito ){
            
            $("#"+idObjSi).show();
            $("#"+idObjNo).hide();
            
        }
        else{
            
            $("#"+idObjSi).hide();
            $("#"+idObjNo).show();
            
        }
        
        $.ajax({
            url: "servicio_core.php?servicio=setPlaceFavorito&key="+strKey+"&fav="+( boolFavorito ? "Y" : "N" ), 
            dataType : "json",
            success: function(result){
            }
        });
        
                    
    } 
    
    function fntLikeProducto(strKey, strIdentificador){
        
        objCount = "#spCountLikeProducto_"+strIdentificador;
        objIconoCount = "#iIconoLikeProducto_"+strIdentificador;
        
        strMiLike = $(objCount).attr( "like" );
        
        if( strMiLike == "Y" )
            return false;
        
        intCount = $(objCount).attr( "countLike" );
        
        intCount++;
        
        $(objCount).attr("countLike", intCount);
        $(objCount).attr("like", "Y");
        
        $(objCount).html(intCount);
        $(objIconoCount).css("color", "#3E53AC");
        
        
        //No Like
        objCount = "#spCountNoLikeProducto_"+strIdentificador;
        objIconoCount = "#iIconoNoLikeProducto_"+strIdentificador;
        
        intCount = $(objCount).attr( "countLike" );
        
        if( intCount != 0 )
            intCount--;
        
        $(objCount).attr("countLike", intCount);
        $(objCount).attr("nolike", "N");
        
        $(objCount).html(intCount);
        $(objIconoCount).css("color", "black");
        
        
        $.ajax({
            url: "servicio_core.php?servicio=setLikeProducto&key="+strKey, 
            dataType : "json",
            success: function(result){
            }
        });
        
    }
    
    function fntNoLikeProducto(strKey, strIdentificador){
        
        objCount = "#spCountNoLikeProducto_"+strIdentificador;
        objIconoCount = "#iIconoNoLikeProducto_"+strIdentificador;
        
        strMiLike = $(objCount).attr( "nolike" );
        
        if( strMiLike == "Y" )
            return false;
        
        intCount = $(objCount).attr( "countLike" );
                    
        intCount++;
        
        $(objCount).attr("countLike", intCount);
        $(objCount).attr("nolike", "Y");
        
        $(objCount).html(intCount);
        $(objIconoCount).css("color", "#3E53AC");
        
        
        //No Like
        objCount = "#spCountLikeProducto_"+strIdentificador;
        objIconoCount = "#iIconoLikeProducto_"+strIdentificador;
        
        intCount = $(objCount).attr( "countLike" );
        
        if( intCount != 0 )
            intCount--;
        
        $(objCount).attr("countLike", intCount);
        $(objCount).attr("like", "N");
        
        $(objCount).html(intCount);
        $(objIconoCount).css("color", "black");
        
        $.ajax({
            url: "servicio_core.php?servicio=setNoLikeProducto&key="+strKey, 
            dataType : "json",
            success: function(result){
            }
        });
        
    }
    
    function fntAddPlace(){
        
        boolCorriendoAjax = true;
        
        if( boolDetenerBusqueda )
            return false;
            
        intLastRangoScrool++;
                
        console.log($("#hidToken").val()+"----");
        $.ajax({
            url: "s.php?getMoreResultado=true&texto="+$("#txtAutocomplete").val()+"&next_page_token="+$("#hidToken").val()+"&tipo="+$("#hidTipo").val()+"&rango="+$("#hidRango").val(), 
            dataType: 'json',
            success: function(result){
                
                if( ( $("#hidToken").val() == result.next_page_token ) || result.next_page_token == "" ){
                    
                    boolDetenerBusqueda = true;
                
                }
                $("#hidToken").val(result.next_page_token)
                
                $.each(result.resultados, function( index, value ) {
                    
                    
                    strRating = "";
                    for( var i = 1; i <= value.rating; i++ ){
                        
                        strRating += "<i class=\"fa fa-star text-tema\"></i>";
                            
                    }
                    
                    strFoto = value.foto;
                    
                    strTr = "    <div class=\"col-12 col-lg-3 p-lg-4 p-0  m-0 mb-3 mb-lg-0\" >  "+
                            "        <div class=\"row p-0 m-0\" onclick=\"window.open('placepp.php?p="+value.identificador+"&op="+( value.opening_hours.open_now ? "Y" : "N" )+"')\" >  "+
                            "            <div class=\"col-lg-12  col-12 p-0 m-0  pl-lg-1 pr-lg-1 mt-1 \" >    "+
                            "                <div class=\"row m-0 p-0\">          "+
                            
                            "                    <div class=\"col-12 m-0 p-0\" style=\"margin-top: 5px;\" >    "+
                            "                    <img style=\"width: 100%; <?php print $strHeightPX;?>  object-fit: cover; border-radius: 15px;\" src=\""+value.foto+"\" class=\"img-fluid rounded\">   "+
                            "                    </div >    "+
                            "                    <div class=\"col-12 m-0 p-0\" style=\"margin-top: 5px;\" >    "+
                            "                        <h5  class=\"text-dark text-left text-hover-underline lis-line-height-1 m-0 p-0\" style=\"font-size: 18px;\">   "+
                            "                            "+value.nombre+"      "+             
                            "                        </h5>       "+
                            "                        <p  class=\"text-dark text-left text-hover-underline lis-line-height-1 m-0 p-0\" style=\"font-size: 15px;\">   "+
                            "                            "+value.direccion+"      "+             
                            "                        </p>       "+
                            "                    </div>       "+
                            "                    <div class=\"col-6 m-0 p-0\" style=\"margin-top: 5px;\" >    "+
                            "                        <h5  class=\"text-left text-hover-underline lis-line-height-1 m-0 p-0 \" style=\"font-size: 15px;\">   "+
                            "                           "+( strRating == "" ? "" : strRating)+
                            "                       </h5>           "+
                            "                    </div>       ";
                            if( value.price_level ){
                                strTr += "       <div class=\"col-6 m-0 p-0\" style=\"margin-top: 5px;\" >    "+
                                "                        <h5  class=\"text-right text-hover-underline lis-line-height-1 m-0 p-0 \" style=\"font-size: 15px;\">   "+
                                "                           "+( value.price_level == "" ? "" : " <span class=\"badge  text-white text-background-theme \" > "+value.price_level+" </span>"  )+"     "+
                                "                       </h5>           ";
                                    
                            }
                            "                    </div>       "+
                            "                </div>          "+
                            "            </div>  "+
                            "        </div>   "+
                            "    </div>   ";
                           /*
                    strTr = "<tr >  "+
                            "    <td>   "+
                            "        <div style=\"cursor: pointer;\" onclick=\"fntShowRow('"+value.identificador+"');\" class=\"row p-0 m-0\">     "+
                            "            <div class=\"col-lg-3 col-4 p-0 m-0 mt-3 text-right \">      "+
                            "                <img style=\"<?php print $boolMovil ? "width: 100%; height: auto;" : "width: 240px; height: 160px;"?>\" src=\""+strFoto+"\" class=\"img-fluid rounded\">   "+
                            "            </div>     "+
                            "            <div class=\"col-lg-7 col-8 p-0 m-0 pl-3 mt-3\"> "+
                            "                <h5 class=\"text-dark text-left lis-line-height-1 m-0 p-0\" style=\"font-size: 20px;\">  "+
                            "                    "+value.nombre+"     "+              
                            "                </h5>    "+
                            "                <h5 class=\"  text-left m-0 p-0\" style=\"font-size: 14px;\">  "+
                            "                     "+value.direccion+"  "+
                            "                </h5>     "+
                            "                <h5 class=\"  text-left m-0 p-0\" style=\"font-size: 14px;\">  "+
                            "                    dddd  "+              
                            "                </h5>      "+
                            "                <h5 class=\"text-left m-0 p-0 text-color-theme\" style=\"font-size: 14px;\">  "+
                            "                    Q   "+           
                            "                </h5>     "+
                            "            </div>     "+
                            "        </div>   "+
                            "    </td>  "+
                            "</tr>";       */
                            $("#myTable").append(strTr);
                           //table.row.add([strTr]).draw(false);
                                   
                });
                
                boolCorriendoAjax = false; 
            }
        });
        
    }
    
    
</script>
<?php
fntDrawFooterPublico();
?>