<?php  
use Firebase\JWT\JWT;

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
die();
}

try {

    if( isset($_GET["servicio"])  ){
        
        $strServicio = trim($_GET["servicio"]);
        
        $arrP = json_decode(file_get_contents('php://input'));
        $strLenguaje = isset($arrP->lenguaje) && $arrP->lenguaje == "es" ? "es" : "en";
        
        if( $strServicio == "aaaaaaaaaaaaaa" ){
            
            include "core/function_servicio.php";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            $strNombreRegistro = isset($arrP->nombre) ? addslashes($arrP->nombre)  : "";
            
            
            $arr["error"] = true;
            $arr["msn"] = "";
            
            
            
            print json_encode($arr);            
            die();             
                                    
        }
        
        if( $strServicio == "getIssetEmail" ){
            
            $arrP = json_decode(file_get_contents('php://input'));
            $strLenguaje = isset($arrP->lenguaje) && $arrP->lenguaje == "es" ? "es" : "";
            
            include "core/function_servicio.php";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $strEmail = isset($arrP->email) ? addslashes($arrP->email)  : "";
            
            $arr["error"] = true;
            $arr["msn"] = "";
            
            if( empty($strEmail) ){
                
                $arr["msn"] = lang["datos_error_1"];
                
                $arr["error"] = true;
                print json_encode($arr);            
                die();         
                
            }            
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
                                
            $strQuery = "SELECT id_usuario, estado
                         FROM   usuario   
                         WHERE  email = '{$strEmail}'";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_fetch_array($qTMP);
            
            if( isset($rTMP["id_usuario"]) && intval($rTMP["id_usuario"]) ){
                
                $arr["error"] = false;
                $arr["existe_email"] = true;
                $arr["msn"] = lang["mismo_email"];
                
            }
            else{
                
                $arr["error"] = false;
                $arr["existe_email"] = false;
                $arr["msn"] = "";
                
            }
            
            print json_encode($arr);            
            die();             
                                    
        }
        
        if( $strServicio == "setRegistro" ){
            
            $arrP = json_decode(file_get_contents('php://input'));
            $strLenguaje = isset($arrP->lenguaje) && $arrP->lenguaje == "es" ? "es" : "";
            
            include "core/function_servicio.php";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $strEmail = isset($arrP->email) ? addslashes($arrP->email)  : "";
            $strTelefono = isset($arrP->telefono) ? addslashes($arrP->telefono)  : "";
            $strClave = isset($arrP->clave) ? addslashes($arrP->clave)  : "";
            $strCodigoArea = isset($arrP->codigo_area) ? addslashes($arrP->codigo_area)  : "";
            $strSexo = isset($arrP->sexo) ? addslashes($arrP->sexo)  : "";
            $strNombre = isset($arrP->nombre) ? addslashes($arrP->nombre)  : "";
            $strTokenFirebase = isset($arrP->token_firebase) ? addslashes($arrP->token_firebase)  : "";
            
            $arr["error"] = true;
            $arr["msn"] = "";
            $arr["token_sesion"] = "";
            
            if( empty($strEmail) || empty($strTelefono) || empty($strClave) || empty($strCodigoArea) 
                || empty($strSexo) || empty($strNombre)  || empty($strTokenFirebase) ){
                
                $arr["msn"] = lang["datos_error_1"];
                
                $arr["error"] = true;
                print json_encode($arr);            
                die();         
                
            }            
            
            
            //Lo primerito, creamos una variable iniciando curl, pasándole la url
            $ch = curl_init('https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyCc8-C6Tuc7vNHZ2WGVaDsuGgGsygmh3fs');
             
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt ($ch, CURLOPT_POST, 1);
             
            //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
            curl_setopt ($ch, CURLOPT_POSTFIELDS, "idToken=".$strTokenFirebase);
             
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
             
            //recogemos la respuesta
            $crulrespuesta = curl_exec ($ch);

             
            //o el error, por si falla
            $error = curl_error($ch);
             
            //y finalmente cerramos curl
            curl_close ($ch);

            $arrRespuesta = json_decode($crulrespuesta);

            if( !( isset($arrRespuesta->users[0]->localId) && $strUid == $arrRespuesta->users[0]->localId ) ){
                
                $arr["msn"] = lang["error_captcha"];
                $arr["error_recap"] = true;
                $arr["error"] = true;

                print json_encode($arr);            
                die();         
                
            }  
            
            include "core/dbClass.php";
            $objDBClass = new dbClass();
                                
            $strQuery = "SELECT id_usuario, estado
                         FROM   usuario   
                         WHERE  email = '{$strEmail}'";
            $qTMP = $objDBClass->db_consulta($strQuery);
            $rTMP = $objDBClass->db_fetch_array($qTMP);
            $objDBClass->db_fetch_array($qTMP);
            
            if( isset($rTMP["id_usuario"]) && intval($rTMP["id_usuario"]) ){
                
                $arr["error"] = false;
                $arr["existe_email"] = true;
                $arr["msn"] = lang["mismo_email"];
                
            }
            else{
                
                $arr["error"] = true;
                $arr["existe_email"] = false;
                $arr["msn"] = lang["intente_nuevamente"];
                
                $strClave = md5($strClave);
                $strTelefono = $strCodigoArea.$strTelefono;
                
                $strQuery = "INSERT INTO usuario(email,  nombre, apellido, 
                                                 estado, tipo, add_fecha, lenguaje, telefono, tipo_registro, identificador,
                                                 clave )
                                          VALUES('{$strEmail}', '{$strNombre}', '',
                                                 'A', '3', SYSDATE(), '{$strLenguaje}', '{$strTelefono}', 'E', '',
                                                 '{$strClave}')  ";
                $objDBClass->db_consulta($strQuery);
                
                $intIdUsuario = intval($objDBClass->db_last_id());
                
                if( $intIdUsuario ){
                    
                    $arr["error"] = false;
                    $arr["token_sesion"] = base64_encode(fntCoreEncrypt($intIdUsuario));
                    $arr["existe_email"] = false;
                    $arr["msn"] = "";
                        
                }
                
                fntEnviarCorreo("info@inguate.com", "Regristro INGUATE", "Regristro de Usuario: ".$strNombre." - telefono: ".$strTelefono." - email: ".$strEmail);
                
                
            }
            
            $objDBClass->db_close();
            
            print json_encode($arr);            
            die();             
                                    
        }
        
        if( $strServicio == "getLogIn" ){
            
            $arrP = json_decode(file_get_contents('php://input'));
            
            include "core/function_servicio.php";
            
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $strEmail = isset($arrP->email) ? trim($arrP->email) : 0;
            $strClave = isset($arrP->clave) ? trim($arrP->clave) : 0;
            
            $arr["token"] = "";
            $arr["error"] = true;
            $arr["msn"] = lang["datos_error_1"];
            
            if( !empty($strEmail) && !empty($strClave) ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $strClave = md5($strClave);
                
                $strQuery = "SELECT *
                             FROM   usuario
                             WHERE  email = '{$strEmail}'
                             AND    clave = '{$strClave}' ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                $rTMP = $objDBClass->db_fetch_array($qTMP);
                $objDBClass->db_free_result($qTMP);
                
                $intIdUsuario = intval($rTMP["id_usuario"]);
                
                $arr["error"] = true;
                $arr["msn"] = lang["usuario_clave_error"];
                
                if( $intIdUsuario ){
                                                                              
                    $arr["token"] = base64_encode(fntCoreEncrypt($intIdUsuario));
                    //$arr["token_de"] = fntCoreDecrypt(base64_decode($arr["token"]));
                    $arr["error"] = false;
                    $arr["msn"] = "";
                    
                } 
                
            }
                            
            print json_encode($arr);            
            die();
                
        }
        
        if( $strServicio == "getContenidoInicio" ){
            
            $arrP = json_decode(file_get_contents('php://input'));
            
            include "core/function_servicio.php";
            
            $strLenguaje = isset($arrP->lenguaje) && $arrP->lenguaje == "es" ? "es" : "";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $intIdUsuario = isset($arrP->token) ? intval(fntCoreDecrypt(base64_decode($arrP->token))) : 0;
            
            $arr["error"] = true;
            $arr["msn"] = lang["datos_error_1"];
            
            if( intval($intIdUsuario) ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $arrCategoriaEspecial = fntGetCatalogoCategoriaEspecial(true, $objDBClass);
                
                $arr["categoria_especial"] = array();
                
                while( $rTMP = each($arrCategoriaEspecial) ){
                    
                    $arrTMP["id_categoria_especial"] = fntCoreEncrypt($rTMP["value"]["id_categoria_especial"]);
                    $arrTMP["tipo"] = "ces";
                    $arrTMP["nombre"] = $rTMP["value"]["nombre"];
                    $arrTMP["tag"] = $strLenguaje == "es" ? $rTMP["value"]["tag_es"] : $rTMP["value"]["tag_en"];
                    $arrTMP["url_imagen"] = $rTMP["value"]["url_imagen"];
                    $arrTMP["estado"] = $rTMP["value"]["estado"];
                    $arrTMP["orden"] = $rTMP["value"]["orden"];
                    $arrTMP["color_hex"] = $rTMP["value"]["color_hex"];
                    $arrTMP["url_imagen_principal"] = $rTMP["value"]["url_imagen_principal"];
                    $arrTMP["url_imagen_principal_web"] = $rTMP["value"]["url_imagen_principal_web"];
                    $arrTMP["texto_salto"] = $rTMP["value"]["texto_google_place"];
                    
                    array_push($arr["categoria_especial"], $arrTMP);    
                    
                }
                
                $arr["error"] = false;
                $arr["msn"] = "";
            
                                    
                $objDBClass->db_close();
                
            }
                            
            print json_encode($arr);            
            die();
                
        }
        
        if( $strServicio == "getContenidoBusqueda" ){
        
            $arrP = json_decode(file_get_contents('php://input'));
            
            include "core/function_servicio.php";
            
            $strLenguaje = isset($arrP->lenguaje) && $arrP->lenguaje == "es" ? "es" : "";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $intIdUsuario = isset($arrP->token) ? intval(fntCoreDecrypt(base64_decode($arrP->token))) : 0;
            $strTipo = isset($arrP->tipo) ? addslashes($arrP->tipo) : 0;
            $strIdentificador = isset($arrP->identificador) ? addslashes($arrP->identificador) : 0;
            $intUbicacion = isset($arrP->ubicacion) ? intval($arrP->ubicacion) : 1;
            
            
            $arr["error"] = true;
            $arr["msn"] = lang["datos_error_1"];
            
            if( intval($intIdUsuario) ){
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                $arr["error"] = false;
                $arr["msn"] = "";
            
                    
                if( $strTipo == "ces" ){
                    
                    $arrLugar = app_fntGetLugarCategoriaEspecial(fntCoreDecrypt($strIdentificador), $intUbicacion, true, $objDBClass);
                    
                    $arr["lugar"] = $arrLugar["resultados"];
                        
                }
                                    
                $objDBClass->db_close();
                
            }
                            
            print json_encode($arr);            
            die();
            
        }
        
        if( $strServicio == "getNegocio" ){
        
            $arrP = json_decode(file_get_contents('php://input'));
            
            include "core/function_servicio.php";
            
            $strLenguaje = isset($arrP->lenguaje) && $arrP->lenguaje == "es" ? "es" : "";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $intIdUsuario = isset($arrP->token) ? intval(fntCoreDecrypt(base64_decode($arrP->token))) : 0;
            $strTipo = isset($arrP->tipo_lugar) ? addslashes($arrP->tipo_lugar) : 0;
            $strIdentificador = isset($arrP->identificador) ? addslashes($arrP->identificador) : 0;
            
            $arr["error"] = true;
            $arr["msn"] = lang["datos_error_1"];
            $arr["datos"] = "";
            
            if( intval($intIdUsuario) ){
                
                $arr["error"] = false;
                $arr["msn"] = "";
            
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                if( $strTipo == "NC" ){
                    
                    $arr["datos"] = app_fntGetLugarNoConfirmado($strIdentificador, true, $objDBClass);
                    
                }
                                    
                $objDBClass->db_close();
                
            }
                            
            print json_encode($arr);            
            die();
            
        }
        
        if( $strServicio == "getAutoCompleteBusqueda" ){
        
            $arrP = json_decode(file_get_contents('php://input'));
            
            include "core/function_servicio.php";
            define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );
            
            $intIdUsuario = isset($arrP->token) ? intval(fntCoreDecrypt(base64_decode($arrP->token))) : 0;
            $strTexto = isset($arrP->texto) ? ($arrP->texto) : 1;
            $intUbicacion = isset($arrP->ubicacion) ? intval($arrP->ubicacion) : 1;
            
            $arr["error"] = true;
            $arr["msn"] = lang["datos_error_1"];
            $arr["datos"] = "";
            
            if( intval($intIdUsuario) ){
                
                $arr["error"] = false;
                $arr["msn"] = "";
            
                
                include "core/dbClass.php";
                $objDBClass = new dbClass();
                
                    
                $arr["datos"] = app_fntGetAutoComplete($intIdUsuario, $intUbicacion, $strTexto, $strLenguaje, false, true, true, $objDBClass);
                    
                                    
                $objDBClass->db_close();
                
            }
                            
            print json_encode($arr);            
            die();
            
        }
        
        $arr["error"] = "true";
        
        print json_encode($arr);
        
    }

} catch (Exception $e) {
    
    //echo 'Caught exception: ',  $e->getMessage(), "\n";
    
    $arr["error_e"] = "true exception";
    $arr["error"] = $e->getMessage();
    
    print json_encode($arr);
    
}


?>