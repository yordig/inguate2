<?php
                         
include "core/function_servicio.php";

include "core/dbClass.php";
$objDBClass = new dbClass();

$arrCatalogoUbicacion = fntUbicacion();
$arrCatalogoUbicacionLugar1 = fntUbicacionLugar();
$arrCatalogoUbicacionLugar = array();
while( $rTMP = each($arrCatalogoUbicacionLugar1) ){

    while( $arrTMP = each($rTMP["value"]["lugar"]) ){
        
        $arrCatalogoUbicacionLugar[$arrTMP["key"]]["ubicacion_lugar"] = $arrTMP["key"];    
        $arrCatalogoUbicacionLugar[$arrTMP["key"]]["ubicacion"] = $rTMP["key"];    
    
            
    }
    
}

$strQuery = "SELECT place.id_place,
                    place.id_ubicacion_lugar,
                    place.titulo,
                    place_producto.id_place_producto,
                    place_producto.precio,
                    clasificacion_padre.id_clasificacion,
                    place_producto.id_clasificacion_padre,
                    place_producto.id_categoria_especial,
                    place_producto.tipo,
                    place_producto.nombre nombre_producto,
                    place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo,
                    clasificacion_padre_hijo.nombre,
                    
                    clasificacion.gasto_bajo gasto_bajo,
                    clasificacion.gasto_alto gasto_alto,
                    
                    
                    clasificacion_padre.gasto_bajo gasto_bajo_padre,
                    clasificacion_padre.gasto_alto gasto_alto_padre
                    
                    
             FROM   place,
                    place_producto
                        LEFT JOIN place_producto_clasificacion_padre_hijo
                            INNER JOIN  clasificacion_padre_hijo
                                ON  clasificacion_padre_hijo.id_clasificacion_padre_hijo = place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo
                            ON  place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto
                        INNER JOIN  clasificacion_padre
                            ON  clasificacion_padre.id_clasificacion_padre = place_producto.id_clasificacion_padre
                        INNER JOIN  clasificacion
                                ON  clasificacion.id_clasificacion = clasificacion_padre.id_clasificacion   
                            
             WHERE  place.estado IN('A')
             AND    place.id_place = place_producto.id_place
             AND    place_producto.estado IN('A')
             ORDER BY precio DESC
             ";
//AND    place_producto.tipo IN('1')
             
$arr = array();
$arrClasificacion = array();
$arrClasificacionPadre = array();
$qTMP = $objDBClass->db_consulta($strQuery);
while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
    
    
    $arrClasificacion[$rTMP["id_clasificacion"]]["gasto_bajo"] = $rTMP["gasto_bajo"];
    $arrClasificacion[$rTMP["id_clasificacion"]]["gasto_alto"] = $rTMP["gasto_alto"];
    
    
    $arrClasificacionPadre[$rTMP["id_clasificacion_padre"]]["gasto_bajo"] = $rTMP["gasto_bajo_padre"];
    $arrClasificacionPadre[$rTMP["id_clasificacion_padre"]]["gasto_alto"] = $rTMP["gasto_alto_padre"];
    
    
    $intUbicacion = $arrCatalogoUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["ubicacion"]; 
    
    $intUbicacion = $rTMP["id_ubicacion_lugar"];
     
    $arr[$rTMP["id_place"]]["id_ubicacion"] = $intUbicacion;
    $arr[$rTMP["id_place"]]["id_ubicacion_lugar"] = $rTMP["id_ubicacion_lugar"];
    $arr[$rTMP["id_place"]]["titulo"] = $rTMP["titulo"];
    
    $arr[$rTMP["id_place"]]["clasificacion"][$rTMP["id_clasificacion"]] = $rTMP["id_clasificacion"];
    $arr[$rTMP["id_place"]]["clasificacion_padre"][$rTMP["id_clasificacion_padre"]] = $rTMP["id_clasificacion_padre"];
    
    $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
    $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
    $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["nombre_producto"] = $rTMP["nombre_producto"];
    $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["tipo"] = $rTMP["tipo"];
    $arr[$rTMP["id_place"]]["producto"][$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
    $arr[$rTMP["id_place"]]["precio"] = array();
    $arr[$rTMP["id_place"]]["precio_clasificacion"] = array();
    $arr[$rTMP["id_place"]]["precio_mediana_bajo"] = 0;
    $arr[$rTMP["id_place"]]["precio_mediana_alto"] = 0;
    
    if( intval($rTMP["id_clasificacion_padre_hijo"]) ){
        
        $arr[$rTMP["id_place"]]["clasificacion_padre_hijo"][$rTMP["nombre"]]["producto"][$rTMP["id_place_producto"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
        $arr[$rTMP["id_place"]]["clasificacion_padre_hijo"][$rTMP["nombre"]]["producto"][$rTMP["id_place_producto"]]["nombre_producto"] = $rTMP["nombre_producto"];
        $arr[$rTMP["id_place"]]["clasificacion_padre_hijo"][$rTMP["nombre"]]["producto"][$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
    
            
    }
    
    //////////////////////////////////////////////////////////////////////////////////
    //Key Hijo    
    //////////////////////////////////////////////////////////////////////////////////  
    if( !empty($rTMP["nombre"]) ){
        
        
        if( !isset($arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"]) )
            $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"] = $rTMP["precio"];
            
        if( $rTMP["precio"] <= $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"] )
            $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["min"] = $rTMP["precio"];
        
        if( !isset($arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"]) )
            $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"] = $rTMP["precio"];
            
        if( $rTMP["precio"] >= $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"] )
            $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["max"] = $rTMP["precio"];
        
        $arrData[$intUbicacion]["hijo_key"][$rTMP["nombre"]]["key_clasificacion_padre_hijo"][$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
        
    }  
    
}
$objDBClass->db_free_result($qTMP);

//drawdebug($arr);
//drawdebug($arrData);

$arr2 = array();

//Agrupar precios y ordenarlos
while( $rTMP = each($arr) ){
    
    // Group by por Clasificacion (desayuno, almuerzo, cena)
    if( isset($rTMP["value"]["clasificacion_padre_hijo"]) ){
        
        while( $arrTMP = each($rTMP["value"]["clasificacion_padre_hijo"]) ){
            
            while( $arrTMP1 = each($arrTMP["value"]["producto"]) ){
                
                $arr[$rTMP["key"]]["precio_clasificacion"][$arrTMP["key"]][$arrTMP1["key"]] = $arrTMP1["value"]["precio"];
                
            }            
            
            sort($arr[$rTMP["key"]]["precio_clasificacion"][$arrTMP["key"]]);
        
        }
           
    }
    // Place sin clasificacion de productos
    else{
        
        while( $arrTMP = each($rTMP["value"]["producto"]) ){
            
            array_push($arr[$rTMP["key"]]["precio"], $arrTMP["value"]["precio"]);
            
        }
        
        sort($arr[$rTMP["key"]]["precio"]);
        
    }
        
}

reset($arr);
while( $rTMP = each($arr) ){
    
    if( count($rTMP["value"]["precio"]) > 0 ){
        
        $arrPrecio = $rTMP["value"]["precio"];
        
        $intCount = count($arrPrecio) - 1 ;
        $intPostMitad =  $intCount / 2;
        
        $arrPrecioMitadBaja = array();
        $arrPrecioMitadAlta = array();
        if( is_float($intPostMitad) ){
            
            $intPostMitad = intval($intPostMitad);
            $intPostMitadAlta = intval($intPostMitad) + 1;
                
            while( $arrTMP = each($arrPrecio) ){
                
                if( $arrTMP["key"] <= $intPostMitad ){
                    
                    array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                        
                }
                if( $arrTMP["key"] >= $intPostMitadAlta ){
                    
                    array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                        
                }
                
            }
        }
        else{
            
            while( $arrTMP = each($arrPrecio) ){
                
                if( $arrTMP["key"] <= $intPostMitad ){
                    
                    array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                        
                }
                if( $arrTMP["key"] >= $intPostMitad ){
                    
                    array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                        
                }
                
            }
                    
        }
        
        //Baja
        $intCount = count($arrPrecioMitadBaja) - 1 ;
        $intPostMitad =  $intCount / 2;
        
        $intIndexMedianaBaja = $intCount;
         
        if( intval($intPostMitad) ){
            $intIndexMedianaBaja = $intPostMitad; 
        }
        $sinRangoMedianaBajo = $arrPrecioMitadBaja[$intPostMitad];
        
        //Alta
        $intCount = count($arrPrecioMitadAlta) - 1 ;
        $intPostMitad =  $intCount / 2;
        
        $intIndexMedianaAlta = $intCount;
         
        if( intval($intPostMitad) ){
            $intIndexMedianaAlta = $intPostMitad; 
        }
        $sinRangoMedianaAlta = $arrPrecioMitadAlta[$intIndexMedianaAlta];
        
        $arr[$rTMP["key"]]["precio_mediana_bajo"] = $sinRangoMedianaBajo;
        $arr[$rTMP["key"]]["precio_mediana_alto"] = $sinRangoMedianaAlta;
        
        //drawdebug($sinRangoMedianaBajo." - ".$sinRangoMedianaAlta);
        //drawdebug($arrPrecioMitadBaja);
        //drawdebug($arrPrecioMitadAlta);
        //drawdebug($arrPrecio);
        //drawdebug("------------------------------------------------------------------------------------------------------------------------");
        
        
    }    
    
    if( count($rTMP["value"]["precio_clasificacion"]) > 0 ){
        
        $arrClasificacionPrecio = $rTMP["value"]["precio_clasificacion"];
        
        $sinTotalRangoMedianaBajo = 0;
        $sinTotalRangoMedianaAlta = 0;
        $intTotalCalculo = 0;
        
        while( $arrTMP = each($arrClasificacionPrecio) ){
            
            $arrPrecio = $arrTMP["value"];
            
            $intCount = count($arrPrecio) - 1 ;
            $intPostMitad =  $intCount / 2;
            
            $arrPrecioMitadBaja = array();
            $arrPrecioMitadAlta = array();
            if( is_float($intPostMitad) ){
                
                $intPostMitad = intval($intPostMitad);
                $intPostMitadAlta = intval($intPostMitad) + 1;
                    
                while( $arrTMP = each($arrPrecio) ){
                    
                    if( $arrTMP["key"] <= $intPostMitad ){
                        
                        array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                            
                    }
                    if( $arrTMP["key"] >= $intPostMitadAlta ){
                        
                        array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                            
                    }
                    
                }
            }
            else{
                
                while( $arrTMP = each($arrPrecio) ){
                    
                    if( $arrTMP["key"] <= $intPostMitad ){
                        
                        array_push($arrPrecioMitadBaja, $arrTMP["value"]);
                            
                    }
                    if( $arrTMP["key"] >= $intPostMitad ){
                        
                        array_push($arrPrecioMitadAlta, $arrTMP["value"]);
                            
                    }
                    
                }
                        
            }
            
            //Baja
            $intCount = count($arrPrecioMitadBaja) - 1 ;
            $intPostMitad =  $intCount / 2;
            
            $intIndexMedianaBaja = $intCount;
             
            if( intval($intPostMitad) ){
                $intIndexMedianaBaja = $intPostMitad; 
            }
            $sinRangoMedianaBajo = $arrPrecioMitadBaja[$intPostMitad];
            
            //Alta
            $intCount = count($arrPrecioMitadAlta) - 1 ;
            $intPostMitad =  $intCount / 2;
            
            $intIndexMedianaAlta = $intCount;
             
            if( intval($intPostMitad) ){
                $intIndexMedianaAlta = $intPostMitad; 
            }
            $sinRangoMedianaAlta = $arrPrecioMitadAlta[$intIndexMedianaAlta];
            
            //drawdebug($sinRangoMedianaBajo. " - ".$sinRangoMedianaAlta);
            
            $sinTotalRangoMedianaBajo += $sinRangoMedianaBajo;
            $sinTotalRangoMedianaAlta += $sinRangoMedianaAlta;
            $intTotalCalculo++;
        
        }
        
        $sinTotalRangoMedianaBajo = $sinTotalRangoMedianaBajo / $intTotalCalculo;
        $sinTotalRangoMedianaAlta = $sinTotalRangoMedianaAlta / $intTotalCalculo;
        
        $arr[$rTMP["key"]]["precio_mediana_bajo"] = intval($sinTotalRangoMedianaBajo);
        $arr[$rTMP["key"]]["precio_mediana_alto"] = intval($sinTotalRangoMedianaAlta);
        
        //drawdebug("_________________________________________________________________________________________________________");
        
        //drawdebug($sinTotalRangoMedianaBajo. " - ".$sinTotalRangoMedianaAlta);
            
        //drawdebug("_________________________________________________________________________________________________________");
        
            
    }        
}


reset($arr);
while( $rTMP = each($arr) ){
    
    //drawdebug($rTMP["value"]["titulo"]);
    //drawdebug($rTMP["value"]["precio_mediana_bajo"]." - ".$rTMP["value"]["precio_mediana_alto"]);
    //drawdebug($rTMP["value"]["clasificacion"]);
    //drawdebug($rTMP["value"]["clasificacion_padre"]);
    
    $rTMP["value"]["precio_mediana_bajo"] = isset($rTMP["value"]["precio_mediana_bajo"]) ? intval($rTMP["value"]["precio_mediana_bajo"]) : 0;
    $rTMP["value"]["precio_mediana_alto"] = isset($rTMP["value"]["precio_mediana_alto"]) ? intval($rTMP["value"]["precio_mediana_alto"]) : 0;
    $strQuery = "UPDATE place
                 SET    precio_mediana_bajo = '{$rTMP["value"]["precio_mediana_bajo"]}',
                        precio_mediana_alto = '{$rTMP["value"]["precio_mediana_alto"]}'
                 WHERE  id_place = {$rTMP["key"]}    ";
    $objDBClass->db_consulta($strQuery);
    /*
    if( isset($rTMP["value"]["clasificacion"]) && count($rTMP["value"]["clasificacion"]) > 0 ){
        
        while( $arrTMP = each($rTMP["value"]["clasificacion"]) ){
            
            if( isset($arrClasificacion[$arrTMP["key"]]) ){
                
                if( ( $rTMP["value"]["precio_mediana_bajo"] < $arrClasificacion[$arrTMP["key"]]["gasto_bajo"] ) || $arrClasificacion[$arrTMP["key"]]["gasto_bajo"] == 0 ){
                    
                    $arrClasificacion[$arrTMP["key"]]["gasto_bajo"] = $rTMP["value"]["precio_mediana_bajo"];
                        
                }
                
                if( ( $rTMP["value"]["precio_mediana_alto"] > $arrClasificacion[$arrTMP["key"]]["gasto_alto"] ) || $arrClasificacion[$arrTMP["key"]]["gasto_alto"] == 0 ){
                    
                    $arrClasificacion[$arrTMP["key"]]["gasto_alto"] = $rTMP["value"]["precio_mediana_alto"];
                        
                }
                
            }
            
        }
        
        while( $arrTMP = each($rTMP["value"]["clasificacion_padre"]) ){
            
            if( isset($arrClasificacionPadre[$arrTMP["key"]]) ){
                
                if( ( $rTMP["value"]["precio_mediana_bajo"] < $arrClasificacionPadre[$arrTMP["key"]]["gasto_bajo"] ) || $arrClasificacionPadre[$arrTMP["key"]]["gasto_bajo"] == 0 ){
                    
                    $arrClasificacionPadre[$arrTMP["key"]]["gasto_bajo"] = $rTMP["value"]["precio_mediana_bajo"];
                        
                }
                
                if( ( $rTMP["value"]["precio_mediana_alto"] > $arrClasificacionPadre[$arrTMP["key"]]["gasto_alto"] ) || $arrClasificacionPadre[$arrTMP["key"]]["gasto_alto"] == 0 ){
                    
                    $arrClasificacionPadre[$arrTMP["key"]]["gasto_alto"] = $rTMP["value"]["precio_mediana_alto"];
                        
                }
                
            }
            
        }
        
    }    
    */
}

$strUbicacion = "";
$strUbicacionClasificacionPadreNombre = "";

while( $rTMP = each($arrData) ){
                                                                    
    $strUbicacion .= ( $strUbicacion == "" ? "" : "," ).$rTMP["key"]; 
       
    while( $arrTMP = each($rTMP["value"]["hijo_key"]) ){
        
        $strUbicacionClasificacionPadreNombre .= ( $strUbicacionClasificacionPadreNombre == "" ? "" : "," )."'".$arrTMP["key"]."'";
        
    }
    
}

$strQuery = "SELECT id_ubicacion_rango_producto,
                    id_ubicacion_lugar,
                    nombre,
                    precio_min,
                    precio_max
             FROM   ubicacion_rango_producto
             WHERE  id_ubicacion_lugar IN ({$strUbicacion})
             AND    nombre IN({$strUbicacionClasificacionPadreNombre})";

$arrUbicacionLugar = array();
$qTMP = $objDBClass->db_consulta($strQuery);
while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
    
    $arrUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["id_ubicacion_rango_producto"] = $rTMP["id_ubicacion_rango_producto"];    
    $arrUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["hijo_key"][$rTMP["nombre"]]["precio_min"] = $rTMP["precio_min"];    
    $arrUbicacionLugar[$rTMP["id_ubicacion_lugar"]]["hijo_key"][$rTMP["nombre"]]["precio_max"] = $rTMP["precio_max"];    
        
}

$objDBClass->db_free_result($qTMP);


reset($arrData);
while( $rTMP = each($arrData) ){
                                                                    
       
    while( $arrTMP = each($rTMP["value"]["hijo_key"]) ){
        
        if( isset($arrUbicacionLugar[$rTMP["key"]]["hijo_key"][$arrTMP["key"]]) ){
            
            $sinPrecioMin = $arrUbicacionLugar[$rTMP["key"]]["hijo_key"][$arrTMP["key"]]["precio_min"];
            $sinPrecioMax = $arrUbicacionLugar[$rTMP["key"]]["hijo_key"][$arrTMP["key"]]["precio_max"];
            
            if( $arrTMP["value"]["min"] < $sinPrecioMin ){
                
                $strQuery = "UPDATE ubicacion_rango_producto
                             SET    precio_min = '{$arrTMP["value"]["min"]}'
                             WHERE  id_ubicacion_lugar = {$rTMP["key"]}
                             AND    nombre = '{$arrTMP["key"]}'
                             ";       
                
                $objDBClass->db_consulta($strQuery);
                
            }
                               
            if( $arrTMP["value"]["max"] > $sinPrecioMax ){
                
                $strQuery = "UPDATE ubicacion_rango_producto
                             SET    precio_max = '{$arrTMP["value"]["max"]}'
                             WHERE  id_ubicacion_lugar = {$rTMP["key"]}
                             AND    nombre = '{$arrTMP["key"]}'
                             ";
                $objDBClass->db_consulta($strQuery);
                
            }
                                    
        }
        else{
            
            $strQuery = "INSERT INTO ubicacion_rango_producto(id_ubicacion_lugar, nombre, precio_min, precio_max)
                                                        VALUES({$rTMP["key"]}, '{$arrTMP["key"]}', '{$arrTMP["value"]["min"]}', '{$arrTMP["value"]["max"]}')";
            $objDBClass->db_consulta($strQuery);
            
        }
        
    }
    
}



$objDBClass->db_close();
?>