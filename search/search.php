<?php
include "/core/function_servicio.php";

$intOrigen = isset($_GET["o"]) ? intval($_GET["o"]) : 0;
$intKey = isset($_GET["key"]) ? fntCoreDecrypt($_GET["key"]) : 0;
$intTipo = isset($_GET["t"]) ? $_GET["t"] : 0;

drawdebug($intOrigen);
drawdebug($intKey);
drawdebug($intTipo);
?>
<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/lister/rtl/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Mar 2019 21:46:35 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Lister</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
        <link href="dist_interno/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <script src="dist_interno/sweetalert/sweetalert.min.js"></script>
        
        <link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
        <link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> <!-- for live demo page -->
        <link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> <!-- for live demo page -->
        
    </head>
    <body>
        <style>
            
            .InputdirectionLTR {
                direction: ltr;
                width: 100%;
            }
            
            .grediant-bt-dark:before {
                background: #dc3545 !important;
            }
            
            .text-color-global {
                color: black !important;
            }
            
            .cardNoBackground {
                background-color: transparent; border: 0px
            }
            
            .cursor_pointer {
                cursor: pointer;
            }
            
            .lis-id-info {
              background-color: #ff214f;
              color: white;
              width: 36px;
              height: 36px;
            line-height: 38px; 
            }
              .lis-id-info:hover {
                  opacity: 0.7;
                  color: white ;
                background-color: #ff214f; }
            
            .lis-id-info-sm {
              background-color: #ff214f;
              color: white;
              width: 26px;
              height: 26px;
              font-size: 14px;
            line-height: 28px; 
            }
              .lis-id-info-sm:hover {
                  opacity: 0.7;
                  color: white ;
                background-color: #ff214f; }
        
        
                   
            .select2-dropdown {
                background-color: white;
                border: 1px solid #aaa;
                border-radius: 4px;
                box-sizing: border-box;
                display: block;
                position: absolute;
                left: -100000px;
                width: 100%;
                z-index: 999999999999999;
                border: 2px solid red;
            }
            
            .bg-delete{
                background-color: #FFC0C0 !important;
            }
                
            .shadow_mia{
                box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.41);
            }
            
            
      
            .slideImgActiva {
                
                padding: 5px 0px 0px 0px ;
                border-radius: 20px !important;
                
                box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
                -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1); 
                -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
                
            }
                
            
            .slideImgNoActiva {
                
                border-radius: 20px !important;
                padding: 10px 10px 10px 10px ;
                border-radius: 20px !important;
                
            }
            .slideImgNoActivaIMG {
                
                border-radius: 20px !important;
                
                box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);   
                -webkit-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);  
                -moz-box-shadow: 1px 7px 10px -12px rgba(0,0,0,1);
                
            }
            
            #lean_overlay {
                position: fixed;
                z-index: 1500;
                top: 0px;
                left: 0px;
                height: 100%;
                width: 100%;
                background: #000;
                display: none;
            }
            
            .popover {
                position: absolute;
                top: 0;
                left: 0;
                z-index: 9999999999999999999999999999;
                display: block;
                max-width: 276px;
                padding: 1px;
                text-align: left;
                text-align: start;
                background-color: #fff;
                border: 1px solid rgba(0,0,0,.2);
                border-radius: .3rem;
            }
            
            
            .transperant nav.navbar-toggleable-md.navbar-light {
                background: #dc3545;
                border: 0px;
            }
            
            #header-fix.nav-affix .navbar {
              background-color: #dc3545;
              /*
              box-shadow: 0 10px 20px -12px rgba(0, 0, 0, 0.42), 0 3px 20px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
              border-color: #000;
              */
              transition: 0.3s; }
            
            /*
            .login_form{
                display: none !important;
            }
            
            
            .slick-current {
                background: yellow !important; 
            }
            */
        </style>
        <!-- header -->
        <div id="header-fix" class="header fixed-top transperant  p-0">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-1" style="direction: ltr; ">
                <a class="navbar-brand mr-4 mr-md-5" href="index.html">
                    <img src="dist/images/logo_inguate.png" style="width: 134px; height: 40px;" alt="">
                </a> 

                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right text-right" style=""  >
                    <button class="btn btn-white float-right"  style="color: white; background: #dc3545; font-weight: bold; font-size: 20px;"><i class="fa fa-reorder "></i></button>
                    <ul class="dl-menu">

                        <li class="nav-item active dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Home</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li><a href="index.html">Home 1</a></li>
                                <li><a href="index-2.html">Home 2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Explore</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li><a href="listing-explore-place-profile.html">Explore Place</a></li>
                                <li><a href="listing-explore-event-profile.html">Explore Event</a></li>
                                <li><a href="listing-explore-property-profile.html">Explore Property</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Listing</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing By Categories</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="listing-categories-style1.html">Category 1</a></li>
                                        <li><a href="listing-categories-style2.html">Category 2</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing No Map</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="listing-no-map-sidebar.html">With Sidebar</a></li>
                                        <li><a href="listing-no-map-fullwidth.html">Full Width</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Half Screen Map</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back bg-dark"><a href="#">back</a></li>
                                        <li><a href="half-screen-search-sidebar-place.html">With Search Place</a></li>
                                        <li><a href="half-screen-search-sidebar-event.html">With Search Event</a></li>
                                        <li><a href="half-screen-search-sidebar-property.html">With Search Property</a></li>
                                        <li><a href="half-screen-with-categories-sidebar.html">With Categories Sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back bg-dark"><a href="#">back</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Blog</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back bg-dark"><a href="#">back</a></li>
                                        <li><a href="blog-grid.html"> Blog Grid</a></li>
                                        <li><a href="blog-listing.html"> Blog Listing</a></li>
                                        <li><a href="blog-single.html"> Single Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"> Contact</a></li>
                                <li><a href="user-profile.html"> User Profile</a></li>
                                <li><a href="faq.html"> Faq</a></li>
                                <li><a href="pricing.html"> Pricing Table</a></li>
                                <li><a href="coming-soon.html"> Coming Soon</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">404 Error</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back bg-dark"><a href="#">back</a></li>
                                        <li><a href="error-dark.html"> 404 Error Dark</a></li>
                                        <li><a href="error-light.html"> 404 Error Light</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>


                        <li><a href="#modal" class="text-white login_form"><i class="fa fa-sign-in pr-2"></i> Sign In | Register</a></li>

                        <li> <a href="add-listing.html" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>



                    </ul>
                </div>

                <div class="collapse navbar-collapse d-none d-lg-block" id="navbarNav" style="height: 50px;">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Home <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html">Home 1</a></li>
                                <li><a href="index-2.html">Home 2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Explore <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="listing-explore-place-profile.html">Explore Place</a></li>
                                <li><a href="listing-explore-event-profile.html">Explore Event</a></li>
                                <li><a href="listing-explore-property-profile.html">Explore Property</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Listing <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing By Categories</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="listing-categories-style1.html">Category 1</a></li>
                                        <li><a href="listing-categories-style2.html">Category 2</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing No Map</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="listing-no-map-sidebar.html">With Sidebar</a></li>
                                        <li><a href="listing-no-map-fullwidth.html">Full Width</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Half Screen Map</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="half-screen-search-sidebar-place.html">With Search Place</a></li>
                                        <li><a href="half-screen-search-sidebar-event.html">With Search Event</a></li>
                                        <li><a href="half-screen-search-sidebar-property.html">With Search Property</a></li>
                                        <li><a href="half-screen-with-categories-sidebar.html">With Categories Sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link text-dark font-weight-bold" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down text-dark font-weight-bold"></i></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Blog</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="blog-grid.html"> Blog Grid</a></li>
                                        <li><a href="blog-listing.html"> Blog Listing</a></li>
                                        <li><a href="blog-single.html"> Single Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"> Contact</a></li>
                                <li><a href="user-profile.html"> User Profile</a></li>
                                <li><a href="faq.html"> Faq</a></li>
                                <li><a href="pricing.html"> Pricing Table</a></li>
                                <li><a href="coming-soon.html"> Coming Soon</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">404 Error</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="error-dark.html"> 404 Error Dark</a></li>
                                        <li><a href="error-light.html"> 404 Error Light</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="list-unstyled my-2 my-lg-0">
                        <li class="text-white"><i class="fa fa-sign-in pl-2"></i> <a href="#modal" class="text-white login_form">Sign In | Register</a></li>
                    </ul>
                    <a href="add-listing.html" class="btn btn-outline-light btn-sm mr-0 mr-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a>
                </div>
            </nav>                    
        </div>
        <!--End header -->

        <!-- Page Inner -->
        <section class="image-bg lis-grediandt grediant-tb p-0 m-0" style="width: 100%; direction: ltr; background-color: white;">
            <div class="background-image-maker"></div>
            <div class="holder-image p-0 m-0 text-center" style="position: relative; width: 100%; height: 680px; vertical-align: top;">
                <video width="100%" class="p-0 m-0" height="100%" id="videoPortada" autoplay >
                  <source src="dist/video/portada2.mp4" type="video/mp4  ">
                  Your browser does not support the video tag.
                </video>
            </div>
            <div class="" style="position: absolute; top: 10%; width: 100%;  ">
                <div class="row justify-content-center pt-5 p-0 m-0">
                    <div class="col-12 col-md-10 text-center wow fadeInUp">
                        <div class="heading pb-5">
                            <div class="d-block d-sm-none">
                                <h1 class=" text-color ">Find Nearby</h1>
                                <h4 class="font-weight-normal mb-0 text-color">Expolore top-rated attractions</h4>
                            </div>
                            <div class="d-none d-sm-block d-md-none d-md-block d-lg-none d-lg-block d-xl-none d-xl-block" >
                                <h1 class="display-4 text-color ">Find Nearby </h1>
                                <h4 class="font-weight-normal mb-0 text-color">Expolore top-rated attractions, activities and more</h4>
                            </div>
                        </div>
                        
                        <div class="tab-content bg-white p-2 rounded rounded-left" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <div class="form-group">
                                            <input type="text" id="autocomplete" name="autocomplete" class="form-control text-center border-top-0 border-left-0 border-right-0 rounded-0 pr-4" placeholder="What are you looking for?" />
                                            <div class="lis-search">
                                                <i class="fa fa-search lis-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-4 offset-4" style="display: none;">
                                        <button class="btn btn-primary btn-block">
                                            <i class="fa fa-search pl-1"></i> Search
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pr-4" placeholder="What are you looking for?" />
                                            <div class="lis-search">
                                                <i class="fa fa-search lis-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pr-4" placeholder="Location" />
                                            <div class="lis-search">
                                                <i class="fa fa-map-o lis-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-4">
                                        <div class="form-group">
                                            <select class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pr-4">
                                                <option> All Categories</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                            <div class="lis-search">
                                                <i class="fa fa-tags lis-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <a href="#" class="btn btn-primary btn-block btn-lg"><i class="fa fa-search pl-1"></i> Search</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pr-4" placeholder="What are you looking for?" />
                                            <div class="lis-search">
                                                <i class="fa fa-search lis-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pr-4" placeholder="Location" />
                                            <div class="lis-search">
                                                <i class="fa fa-map-o lis-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-4">
                                        <div class="form-group">
                                            <select class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pr-4">
                                                <option> All Categories</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                            </select>
                                            <div class="lis-search">
                                                <i class="fa fa-tags lis-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <a href="#" class="btn btn-primary btn-block btn-lg"><i class="fa fa-search pl-1"></i> Search</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </section>
        
        <!--End Page Inner -->

        <!-- Categories -->
        <section class="lis-grediant grediant-tb-white pt-2" style="direction: ltr;">
            <div class="container">
                <div class="row justify-content-center ">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading">
                            <h2 class="f-weight-500">What Do You Want to Do Tonight</h2>
                            <h5 class="lis-light">Discover & connect with great local businesses in Antigua Guatemala</h5>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    
                    <div class="col-lg-4 mt-4 ">
                        <div class="card  border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="images/category/restaurant.jpg" alt="" class="img-fluid rounded">
                                </div> 
                                <div class="lis-absolute lis-left-10 lis-top-10" style="width: 80%;">
                                    <div class="lis-post-meta ml-2 text-white rounded p-1">
                                        
                                        <div class="row">
                                            <div class="  text-right">
                                                <div style="background-color: #eb09eb; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                                    <img src="images/icon/clas/lg==.png">
                                                </div>
                                                
                                            </div>
                                            <div class="col-9 text-left   " style="vertical-align: middle;">
                                                <h5 class="m-0 p-0 text-white" style="font-size: 18px; line-height: 1.35rem">Fast Food</h5> 
                                                <h6 class="m-0 p-0 text-white" style="font-size: 15px; line-height: 1.35rem">Check out selection</h6>
                                            </div>
                                        </div>
                                        
                                            
                                    </div>
                                </div>
                            </a>
                            
                        </div>
                    </div>
                    
                    <div class="col-lg-8 mt-4 ">
                        <div class="card w-100 border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="images/category/largo.jpg" alt="" class="img-fluid rounded">
                                </div> 
                                <div class="lis-absolute lis-left-10 lis-top-10" style="width: 80%;">
                                    <div class="lis-post-meta ml-2 text-white rounded p-1">
                                        
                                        <div class="row">
                                            <div class="  text-right">
                                                <div style="background-color: #eb09eb; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                                    <img src="images/icon/clas/lg==.png">
                                                </div>
                                                
                                            </div>
                                            <div class="col-9 text-left   " style="vertical-align: middle;">
                                                <h5 class="m-0 p-0 text-white" style="font-size: 18px; line-height: 1.35rem">Fast Food</h5> 
                                                <h6 class="m-0 p-0 text-white" style="font-size: 15px; line-height: 1.35rem">Check out selection</h6>
                                            </div>
                                        </div>
                                        
                                            
                                    </div>
                                </div>
                            </a>
                            
                        </div>                  
                    </div>
                </div>
                <div class="row text-center ">
                    
                    
                    <div class="col-lg-4  mt-4">
                        <div class="card border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="images/category/food.jpg" alt="" class="img-fluid rounded">
                                </div> 
                                <div class="lis-absolute lis-left-10 lis-top-10" style="width: 80%;">
                                    <div class="lis-post-meta ml-2 text-white rounded p-1">
                                        
                                        <div class="row">
                                            <div class="  text-right">
                                                <div style="background-color: #eb09eb; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                                    <img src="images/icon/clas/lg==.png">
                                                </div>
                                                
                                            </div>
                                            <div class="col-9 text-left   " style="vertical-align: middle;">
                                                <h5 class="m-0 p-0 text-white" style="font-size: 18px; line-height: 1.35rem">Fast Food</h5> 
                                                <h6 class="m-0 p-0 text-white" style="font-size: 15px; line-height: 1.35rem">Check out selection</h6>
                                            </div>
                                        </div>
                                        
                                            
                                    </div>
                                </div>
                            </a>
                            
                        </div>
                    </div>
                    
                    <div class="col-lg-4 mt-4">
                        <div class="card border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="images/category/hotel.jpg" alt="" class="img-fluid rounded">
                                </div> 
                                <div class="lis-absolute lis-left-10 lis-top-10" style="width: 80%;">
                                    <div class="lis-post-meta ml-2 text-white rounded p-1">
                                        
                                        <div class="row">
                                            <div class="  text-right">
                                                <div style="background-color: #eb09eb; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                                    <img src="images/icon/clas/lg==.png">
                                                </div>
                                                
                                            </div>
                                            <div class="col-9 text-left   " style="vertical-align: middle;">
                                                <h5 class="m-0 p-0 text-white" style="font-size: 18px; line-height: 1.35rem">Fast Food</h5> 
                                                <h6 class="m-0 p-0 text-white" style="font-size: 15px; line-height: 1.35rem">Check out selection</h6>
                                            </div>
                                        </div>
                                        
                                            
                                    </div>
                                </div>
                            </a>
                            
                        </div>                  
                    </div>
                    
                    <div class="col-lg-4 mt-4">
                        <div class="card border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="images/category/boutiques.jpg" alt="" class="img-fluid rounded">
                                </div> 
                                <div class="lis-absolute lis-left-10 lis-top-10" style="width: 80%;">
                                    <div class="lis-post-meta ml-2 text-white rounded p-1">
                                        
                                        <div class="row">
                                            <div class="  text-right">
                                                <div style="background-color: #eb09eb; padding: 10px; border-radius: 50%; max-width: 50px; max-height: 60px; text-align: center;">
                                                    <img src="images/icon/clas/lg==.png">
                                                </div>
                                                
                                            </div>
                                            <div class="col-9 text-left   " style="vertical-align: middle;">
                                                <h5 class="m-0 p-0 text-white" style="font-size: 18px; line-height: 1.35rem">Fast Food</h5> 
                                                <h6 class="m-0 p-0 text-white" style="font-size: 15px; line-height: 1.35rem">Check out selection</h6>
                                            </div>
                                        </div>
                                        
                                            
                                    </div>
                                </div>
                            </a>
                            
                        </div>                  
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-12 text-center mt-2">
                        <a href="#" class="btn btn-success btn-default">View More Featured Categories</a>
                    </div>
                </div>
            </div>
        </section>
        <!--End Categories -->

        <!-- Visited Places -->
        <!--
        <section>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h2 class="f-weight-500">Most Visited Places</h2>
                            <h5 class="lis-light">Discover & connect with top-rated local businesses</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid px-0">
                <div class="fullwidth-carousel-container center slider">
                    <div>
                        <div class="card lis-brd-light text-center text-lg-left">
                            <a href="#">
                                <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top">
                                    <img src="dist/images/img1.jpg" alt="" class="img-fluid rounded-top w-100" />
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Open</div>
                                </div>
                            </a>     
                            <div class="card-body pt-0">
                                <div class="media d-block d-lg-flex lis-relative">
                                    <img src="dist/images/card-logo.png" alt="" class="lis-mt-minus-15 img-fluid d-lg-flex mx-auto mr-lg-3 mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="80" />
                                    <div class="media-body align-self-start mt-2">
                                        <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark">Chico Express Car Taxi and Limo Service</A></h6>
                                    </div>
                                </div>
                                <ul class="list-unstyled my-4 lis-line-height-2 text-right">
                                    <li><i class="fa fa-phone pl-2"></i> +88 25 5894 2589</li>
                                    <li><i class="fa fa-map-o pl-2"></i> First Street, New York, USA</li>
                                </ul>
                                <div class="clearfix">
                                    <div class="float-none float-lg-left mb-3 mb-lg-0 mt-1">
                                        <A href="#" class="text-white"><i class="icofont icofont-travelling px-2 lis-bg5 py-2 lis-rounded-circle-50 lis-f-14"></i></A>

                                    </div>
                                    <div class="float-none float-lg-right mt-1">
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-envelope-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-green-light text-white text-white p-2 lis-rounded-circle-50 lis-f-14 lis-line-height-2"><i class="fa fa-star"></i> 4.0</A>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card lis-brd-light text-center text-lg-left">
                            <a href="#">
                                <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top">
                                    <img src="dist/images/img2.jpg" alt="" class="img-fluid rounded-top w-100" />
                                </div>

                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Closed</div>
                                </div>
                            </a>    
                            <div class="card-body pt-0">
                                <div class="media d-block d-lg-flex lis-relative">
                                    <img src="dist/images/card-logo2.png" alt="" class="lis-mt-minus-15  img-fluid d-lg-flex mx-auto mr-lg-3 mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="80" />
                                    <div class="media-body align-self-start mt-2">
                                        <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark">Bodega Garage - Filipino Night Club</A></h6>
                                    </div>
                                </div>
                                <ul class="list-unstyled my-4 lis-line-height-2 text-right">
                                    <li><i class="fa fa-phone pl-2"></i> +88 25 5894 2589</li>
                                    <li><i class="fa fa-map-o pl-2"></i> First Street, New York, USA</li>
                                </ul>
                                <div class="clearfix">
                                    <div class="float-none float-lg-left mb-3 mb-lg-0 mt-1">
                                        <A href="#" class="text-white"><i class="icofont icofont-radio-mic px-2 lis-bg3 py-2 lis-rounded-circle-50 lis-f-14"></i></A>
                                        <A href="#" class="text-white"><i class="icofont icofont-beer px-2 lis-bg2 py-2 lis-rounded-circle-50 lis-f-14"></i></A>

                                    </div>
                                    <div class="float-none float-lg-right mt-1">
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-envelope-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-green-light text-white p-2 lis-rounded-circle-50 lis-f-14"><i class="fa fa-star"></i> 5.0</A>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card lis-brd-light text-center text-lg-left">
                            <a href="#">
                                <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top">
                                    <img src="dist/images/img3.jpg" alt="" class="img-fluid rounded-top w-100" />
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Open</div>
                                </div>
                            </a>    
                            <div class="card-body pt-0">
                                <div class="media d-block d-lg-flex lis-relative">
                                    <img src="dist/images/card-logo3.png" alt="" class="lis-mt-minus-15 img-fluid d-lg-flex mx-auto mr-lg-3 mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="80" />
                                    <div class="media-body align-self-start mt-2">
                                        <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark">Regal Cinemas Destiny USA 19 IMAX & RPX</A></h6>
                                    </div>
                                </div>
                                <ul class="list-unstyled my-4 lis-line-height-2 text-right">
                                    <li><i class="fa fa-phone pl-2"></i> +88 25 5894 2589</li>
                                    <li><i class="fa fa-map-o pl-2"></i> First Street, New York, USA</li>
                                </ul>
                                <div class="clearfix">
                                    <div class="float-none float-lg-left mb-3 mb-lg-0 mt-1">
                                        <A href="#" class="text-white"><i class="icofont icofont-radio-mic px-2 lis-bg3 py-2 lis-rounded-circle-50 lis-f-14"></i></A>

                                    </div>
                                    <div class="float-none float-lg-right mt-1">
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-envelope-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-green-light text-white p-2 lis-rounded-circle-50 lis-f-14"><i class="fa fa-star"></i> 4.5</A>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card lis-brd-light text-center text-lg-left">
                            <a href="#">
                                <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top">
                                    <img src="dist/images/img4.jpg" alt="" class="img-fluid rounded-top w-100" />
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Closed</div>
                                </div>
                            </a>    
                            <div class="card-body pt-0">
                                <div class="media d-block d-lg-flex lis-relative">
                                    <img src="dist/images/card-logo4.png" alt="" class="lis-mt-minus-15 img-fluid d-lg-flex mx-auto mr-lg-3 mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="80" />
                                    <div class="media-body align-self-start mt-2">
                                        <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark">Pembridge Palace Hotel and Resort</A></h6>
                                    </div>
                                </div>
                                <ul class="list-unstyled my-4 lis-line-height-2 text-right">
                                    <li><i class="fa fa-phone pl-2"></i> +88 25 5894 2589</li>
                                    <li><i class="fa fa-map-o pl-2"></i> First Street, New York, USA</li>
                                </ul>
                                <div class="clearfix">
                                    <div class="float-none float-lg-left mb-3 mb-lg-0 mt-1">
                                        <A href="#" class="text-white"><i class="icofont icofont-hotel-alt px-2 lis-bg1 py-2 lis-rounded-circle-50 lis-f-14"></i></A>
                                        <A href="#" class="text-white"><i class="icofont icofont-fast-food px-2 lis-bg2 py-2 lis-rounded-circle-50 lis-f-14"></i></A>

                                    </div>
                                    <div class="float-none float-lg-right mt-1">
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-envelope-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-green-light text-white p-2 lis-rounded-circle-50 lis-f-14"><i class="fa fa-star"></i> 4.5</A>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card lis-brd-light text-center text-lg-left">
                            <a href="#">
                                <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top">
                                    <img src="dist/images/img5.jpg" alt="" class="img-fluid rounded-top w-100" />
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Open</div>
                                </div>
                            </a>    
                            <div class="card-body pt-0">
                                <div class="media d-block d-lg-flex lis-relative">
                                    <img src="dist/images/card-logo5.png" alt="" class="lis-mt-minus-15 img-fluid d-lg-flex mx-auto mr-lg-3 mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="80" />
                                    <div class="media-body align-self-start mt-2">
                                        <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark">Vintage Italian Beer Bar & Restaurant</A></h6>
                                    </div>
                                </div>
                                <ul class="list-unstyled my-4 lis-line-height-2 text-right">
                                    <li><i class="fa fa-phone pl-2"></i> +88 25 5894 2589</li>
                                    <li><i class="fa fa-map-o pl-2"></i> First Street, New York, USA</li>
                                </ul>
                                <div class="clearfix">
                                    <div class="float-none float-lg-left mb-3 mb-lg-0 mt-1">
                                        <A href="#" class="text-white"><i class="icofont icofont-travelling px-2 lis-bg5 py-2 lis-rounded-circle-50 lis-f-14"></i></A>

                                    </div>
                                    <div class="float-none float-lg-right mt-1">
                                        <a href="#" class="lis-light lis-f-14"><i class="fa fa-envelope-o lis-id-info  lis-rounded-circle-50 text-center"></i></a>
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-green-light text-white p-2 lis-rounded-circle-50 lis-f-14"><i class="fa fa-star"></i> 4.0</A>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card lis-brd-light text-center text-lg-left">
                            <a href="#">
                                <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top">
                                    <img src="dist/images/img12.jpg" alt="" class="img-fluid rounded-top w-100" />
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Open</div>
                                </div>
                            </a>    
                            <div class="card-body pt-0">
                                <div class="media d-block d-lg-flex lis-relative">
                                    <img src="dist/images/card-logo6.png" alt="" class="lis-mt-minus-15 img-fluid d-lg-flex mx-auto mr-lg-3 mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="80" />
                                    <div class="media-body align-self-start mt-2">
                                        <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark">Vintage Italian Beer Bar & Restaurant</A></h6>
                                    </div>
                                </div>
                                <ul class="list-unstyled my-4 lis-line-height-2 text-right">
                                    <li><i class="fa fa-phone pl-2"></i> +88 25 5894 2589</li>
                                    <li><i class="fa fa-map-o pl-2"></i> First Street, New York, USA</li>
                                </ul>
                                <div class="clearfix">
                                    <div class="float-none float-lg-left mb-3 mb-lg-0 mt-1">
                                        <A href="#" class="text-white"><i class="icofont icofont-travelling px-2 lis-bg5 py-2 lis-rounded-circle-50 lis-f-14"></i></A>

                                    </div>
                                    <div class="float-none float-lg-right mt-1">
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-envelope-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                                        <A href="#" class="lis-green-light text-white p-2 lis-rounded-circle-50 lis-f-14"><i class="fa fa-star"></i> 4.0</A>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        -->
        <!--End Visited Places -->

        <!-- Features -->
        <!--
        <section class="image-bg lis-grediant grediant-full">
            <div class="background-image-maker"></div>
            <div class="holder-image">
                <img src="dist/images/bg2.jpg" alt="" class="img-fluid d-none">
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h2>Get Associated With Us</h2>
                            <h5 class="font-weight-normal">Expolore top-rated attractions, activities and more</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                        <div class="lis-features-box lis-brd-light border rounded px-4 py-5">
                            <div  class="h1"><i class="fa fa-address-card-o h1"></i></div>
                            <h5>Add Your Listing</h5>
                            Maecs tempus, tellus eget rhoncus, sem condimentum quam semper libero.
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                        <div class="lis-features-box lis-brd-light border rounded px-4 py-5">
                            <div  class="h1"><i class="fa fa-bullhorn h1"></i></div>
                            <h5>Advertise & Promote</h5>
                            Maecs tempus, tellus eget rhoncus, sem condimentum quam semper libero.
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center wow fadeInUp">
                        <div class="lis-features-box lis-brd-light border rounded px-4 py-5">
                            <div  class="h1"><i class="fa fa-handshake-o h1"></i></div>
                            <h5>Partner With Us</h5>
                            Maecs tempus, tellus eget rhoncus, sem condimentum quam semper libero.
                        </div>
                    </div>
                </div>
            </div>
        </section>
        -->
        <!--End Features -->

        <!-- Upcoming Events -->
        <!--
        <section>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h2>Upcoming Events</h2>
                            <h5 class="font-weight-normal lis-light">Discover & connect with top-rated local businesses</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0">
                        <div class="card lis-brd-light bg-transparent">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded-top">
                                    <img src="dist/images/img6.jpg" alt="" class="img-fluid rounded-top w-100">
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Oct 18-20</div>
                                </div>
                            </a>    
                            <div class="card-body">
                                <div class="media d-block d-sm-flex text-center text-sm-right">
                                    <div class="d-block d-sm-flex ">
                                        <ul class="list-unstyled my-0 pr-0">
                                            <li class="lis-font-weight-600 mb-2 h6"><a href="#" class="lis-dark">Enchanted Valley Carnival</a></li>
                                            <li><i class="fa fa-map-o pl-2"></i> Bishop Avenue, New York</li>
                                        </ul>
                                    </div>
                                    <div class="media-body align-self-center text-center text-sm-left mt-3 mt-sm-0">
                                        <a href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></a>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0">
                        <div class="card lis-brd-light bg-transparent">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded-top">
                                    <img src="dist/images/img7.jpg" alt="" class="img-fluid rounded-top w-100">
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Nov 13-15</div>
                                </div>
                            </a>     
                            <div class="card-body">
                                <div class="media d-block d-sm-flex text-center text-sm-right">
                                    <div class="d-block d-sm-flex">
                                        <ul class="list-unstyled my-0 pr-0">
                                            <li class="lis-font-weight-600 mb-2 h6"><a href="#" class="lis-dark">Timeout 72 Hours</a></li>
                                            <li><i class="fa fa-map-o pl-2"></i> North Street, New York, USA</li>
                                        </ul>
                                    </div>
                                    <div class="media-body align-self-center text-center text-sm-left mt-3 mt-sm-0">
                                        <a href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></a>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-4 mb-lg-0">
                        <div class="card lis-brd-light bg-transparent">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded-top">
                                    <img src="dist/images/img8.jpg" alt="" class="img-fluid rounded-top w-100">
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Dec 28-31</div>
                                </div>
                            </a>    
                            <div class="card-body">
                                <div class="media d-block d-sm-flex text-center text-sm-right">
                                    <div class="d-block d-sm-flex">
                                        <ul class="list-unstyled my-0 pr-0">
                                            <li class="lis-font-weight-600 mb-2 h6"><a href="#" class="lis-dark">VH1 Supersonic 2018</a></li>
                                            <li><i class="fa fa-map-o pl-2"></i> Bishop Avenue, New York</li>
                                        </ul>
                                    </div>
                                    <div class="media-body align-self-center text-center text-sm-left mt-3 mt-sm-0">
                                        <a href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></a>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        -->
        <!--End Upcoming Events -->

        <!-- Featured Property -->
        <!--
        <section class="lis-bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h2>Featured Properties</h2>
                            <h5 class="font-weight-normal lis-light">Discover & connect with top-rated local businesses</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0">
                        <div class="card lis-brd-light">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded-top">
                                    <img src="dist/images/img9.jpg" alt="" class="img-fluid rounded-top w-100">
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">For Sale</div>
                                </div>
                            </a>    
                            <div class="card-body">
                                <h6 class="lis-font-weight-600 mb-2 float-right"><a href="#" class="lis-dark">Villa in Melbourne City</a></h6>
                                <ul class="list-unstyled mt-0 text-right float-right w-100 pr-0">
                                    <li><i class="fa fa-map-o pl-2"></i>1903 Hollywood, NJ 85624, USA</li>
                                </ul>
                                <div class="media pt-2 w-100">
                                    <div class="d-flex">
                                        <h5 class="lis-primary mb-0"><span class="lis-f-14 lis-dark">Price:</span> $13,80,000 </h5>
                                    </div>
                                    <div class="media-body align-self-center text-left">
                                        <a href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></a>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="lis-devider"></div>
                            <ul class="list-inline mb-0 text-center lis-property-meta pr-0">
                                <li class="list-inline-item lis-brd-right lis-brd-light lis-meta-block float-left"><i class="fa fa-object-group pl-1"></i> 5842 m2</li>
                                <li class="list-inline-item lis-brd-right lis-brd-light lis-meta-block float-left"><i class="fa fa-bed pl-1"></i> 4 Bed</li>
                                <li class="list-inline-item lis-meta-block float-left"><i class="fa fa-bath pl-1"></i> 3 Bath</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0">
                        <div class="card lis-brd-light">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded-top">
                                    <img src="dist/images/img10.jpg" alt="" class="img-fluid rounded-top w-100">
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">For Rent</div>
                                </div>
                            </a>    
                            <div class="card-body">
                                <h6 class="lis-font-weight-600 mb-2 float-right"><a href="#" class="lis-dark">Villa in Melbourne City</a></h6>
                                <ul class="list-unstyled mt-0 text-right float-right w-100 pr-0">
                                    <li><i class="fa fa-map-o pl-2"></i>1903 Hollywood, NJ 85624, USA</li>
                                </ul>
                                <div class="media pt-2 w-100">
                                    <div class="d-flex">
                                        <h5 class="lis-primary mb-0"><span class="lis-f-14 lis-dark">Price:</span> $13,80,000 </h5>
                                    </div>
                                    <div class="media-body align-self-center text-left">
                                        <a href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></a>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="lis-devider"></div>
                            <ul class="list-inline mb-0 text-center lis-property-meta pr-0">
                                <li class="list-inline-item lis-brd-right lis-brd-light lis-meta-block float-left"><i class="fa fa-object-group pl-1"></i> 5842 m2</li>
                                <li class="list-inline-item lis-brd-right lis-brd-light lis-meta-block float-left"><i class="fa fa-bed pl-1"></i> 4 Bed</li>
                                <li class="list-inline-item lis-meta-block float-left"><i class="fa fa-bath pl-1"></i> 3 Bath</li>
                            </ul>
                        </div> 
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0">
                        <div class="card lis-brd-light">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded-top">
                                    <img src="dist/images/img11.jpg" alt="" class="img-fluid rounded-top w-100">
                                </div>
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">For Rent</div>
                                </div>
                            </a>   
                            <div class="card-body">
                                <h6 class="lis-font-weight-600 mb-2 float-right"><a href="#" class="lis-dark">Villa in Melbourne City</a></h6>
                                <ul class="list-unstyled mt-0 text-right float-right w-100 pr-0">
                                    <li><i class="fa fa-map-o pl-2"></i>1903 Hollywood, NJ 85624, USA</li>
                                </ul>
                                <div class="media pt-2 w-100">
                                    <div class="d-flex">
                                        <h5 class="lis-primary mb-0"><span class="lis-f-14 lis-dark">Price:</span> $13,80,000 </h5>
                                    </div>
                                    <div class="media-body align-self-center text-left">
                                        <a href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></a>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="lis-devider"></div>
                            <ul class="list-inline mb-0 text-center lis-property-meta pr-0">
                                <li class="list-inline-item lis-brd-right lis-brd-light lis-meta-block float-left"><i class="fa fa-object-group pl-1"></i> 5842 m2</li>
                                <li class="list-inline-item lis-brd-right lis-brd-light lis-meta-block float-left"><i class="fa fa-bed pl-1"></i> 4 Bed</li>
                                <li class="list-inline-item lis-meta-block float-left"><i class="fa fa-bath pl-1"></i> 3 Bath</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        -->
        <!--End Featured Property -->

        <!-- Work -->
        <!--
        <section>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h2>How Does It Work</h2>
                            <h5 class="font-weight-normal lis-light">Discover & connect with top-rated local businesses</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                        <div class="icon-box box-line box-line-dotted1 lis-relative">
                            <img src="dist/images/icon-1.png" alt="" class="img-fluid mb-4 z-index-99  lis-relative" />
                            <h5>1. Find Interesting Place</h5>
                            <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                        <div class="icon-box box-line box-line-dotted2 lis-relative">
                            <img src="dist/images/icon-2.png" alt="" class="img-fluid mb-4 z-index-99  lis-relative" />
                            <h5>2. Choose a Category</h5>
                            <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                        <div class="icon-box">
                            <img src="dist/images/icon-3.png" alt="" class="img-fluid mb-4 z-index-99  lis-relative" />
                            <h5>3. Contact with Owners</h5>
                            <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        -->
        <!--End Work -->

        <!-- Pricing -->
        <!--
        <section class="lis-bg-light">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h2>Choose Your Plan</h2>
                            <h5 class="font-weight-normal lis-light">Discover & connect with top-rated local businesses</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0 text-center">
                        <div class="price-table">
                            <div class="price-header lis-bg-light lis-rounded-top py-4 border border-bottom-0 lis-brd-light">
                                <h5 class="text-uppercase lis-latter-spacing-2">Basic</h5>
                                <h1 class="display-4 lis-font-weight-500"><sup>$</sup> 39 <small>/mo</small></h1>
                                <p class="mb-0">Basic User Membership</p>
                            </div>
                            <div class="border border-top-0 lis-brd-light bg-white py-5 lis-rounded-bottom">
                                <ul class="list-unstyled lis-line-height-3">
                                    <li>One Time Fee</li>
                                    <li>One Listing</li>
                                    <li>90 Days Availability</li>
                                    <li>Featured In Search Results</li>
                                    <li>24/7 Support</li>
                                </ul>
                                <a href="#" class="btn btn-primary-outline btn-md lis-rounded-circle-50"><i class="fa fa-shopping-cart pl-2"></i>Order Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0 text-center">
                        <div class="price-table active">
                            <div class="price-header lis-bg-primary py-4 text-white lis-rounded-top">
                                <h5 class="text-uppercase lis-latter-spacing-2 text-white">Premium</h5>
                                <h1 class="display-4 lis-font-weight-500 text-white"><sup>$</sup> 69 <small>/mo</small></h1>
                                <p class="mb-0">Permium User Membership</p>
                            </div>
                            <div class="border border-top-0 lis-brd-light bg-white py-5 lis-rounded-bottom">
                                <ul class="list-unstyled lis-line-height-3">
                                    <li>One Time Fee</li>
                                    <li>One Listing</li>
                                    <li>90 Days Availability</li>
                                    <li>Featured In Search Results</li>
                                    <li>24/7 Support</li>
                                </ul>
                                <a href="#" class="btn btn-primary btn-md lis-rounded-circle-50"><i class="fa fa-shopping-cart pl-2"></i>Order Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp text-center">
                        <div class="price-table">
                            <div class="price-header lis-bg-light lis-rounded-top py-4 border border-bottom-0 lis-brd-light">
                                <h5 class="text-uppercase lis-latter-spacing-2">business</h5>
                                <h1 class="display-4 lis-font-weight-500"><sup>$</sup> 99 <small>/mo</small></h1>
                                <p class="mb-0">Basic User Membership</p>
                            </div>
                            <div class="border border-top-0 lis-brd-light bg-white py-5 lis-rounded-bottom">
                                <ul class="list-unstyled lis-line-height-3">
                                    <li>One Time Fee</li>
                                    <li>One Listing</li>
                                    <li>90 Days Availability</li>
                                    <li>Featured In Search Results</li>
                                    <li>24/7 Support</li>
                                </ul>
                                <a href="#" class="btn btn-primary-outline btn-md lis-rounded-circle-50"><i class="fa fa-shopping-cart pl-2"></i>Order Now</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        -->
        <!--End Pricing -->

        <!-- Blog -->
        <!--
        <section>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h2>Letest Tips &amp; Blog</h2>
                            <h5 class="font-weight-normal lis-light">Discover &amp; connect with top-rated local businesses</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0">
                        <div class="card border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="dist/images/blog1.jpg" alt="" class="img-fluid rounded">
                                </div> 

                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Bar &amp; Club</div>
                                </div>
                            </a>    
                            <div class="pt-4 text-right">
                                <span class="lis-light">16 October 2017</span>
                                <a href="#" class="lis-dark"><h6 class="mt-2">Donec vitae sapien ut libero</h6></a>
                                <p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus nullam accumsan dui.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp mb-5 mb-lg-0">
                        <div class="card border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="dist/images/blog2.jpg" alt="" class="img-fluid rounded">
                                </div> 
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Bar &amp; Club</div>
                                </div>
                            </a>
                            <div class="pt-4 text-right">
                                <span class="lis-light">24 October 2017</span>
                                <a href="#" class="lis-dark"><h6 class="mt-2">Donec vitae sapien ut libero</h6></a>
                                <p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus nullam accumsan dui.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInUp">
                        <div class="card border-0 lis-relative">
                            <a href="#">
                                <div class="modImage lis-grediant grediant-tb-light2 lis-relative rounded">
                                    <img src="dist/images/bg2.jpg" alt="" class="img-fluid rounded">
                                </div> 
                                <div class="lis-absolute lis-right-20 lis-top-20">
                                    <div class="lis-post-meta border border-white text-white rounded lis-f-14">Bar &amp; Club</div>
                                </div>
                            </a>    
                            <div class="pt-4 text-right">
                                <span class="lis-light">30 October 2017</span>
                                <a href="#" class="lis-dark"><h6 class="mt-2">Donec vitae sapien ut libero</h6></a>
                                <p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus nullam accumsan dui.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        -->
        <!--End Blog -->

        <!-- Call To Action -->
        <!--
        <section class="lis-bg-primary py-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6 text-center wow fadeInUp">
                        <div class="heading">
                            <h2 class="text-uppercase lis-line-height-1_5 text-white">WE WOULD LOVE TO HEAR ABOUT START YOUR NEW PROJECT?</h2>
                            <a href="#" class="btn btn-outline-light btn-default text-uppercase">Add Your Listing Here</a>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        -->
        <!--End Call To Action -->

        <!-- Footer-->
        
        <section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image">
                <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none">
            </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Dashboard</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Profile</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> My Listing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Favorites</A></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <h5 class="footer-head">Pages</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <h5 class="footer-head">Help</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                                    <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a> 
                        </div>
                        <p class="my-4">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu in  felis eu pede mollis enim.</p>
                        <a href="#" class="text-white"><u>App Download</u></a> 
                        <ul class="list-inline mb-0 mt-2">
                            <li class="list-inline-item"><A href="#"><img src="dist/images/play-store.png" alt="" class="img-fluid" /></a></li>
                            <li class="list-inline-item"><A href="#"><img src="dist/images/google-play.png" alt="" class="img-fluid" /></a></li>
                            <li class="list-inline-item"><A href="#"><img src="dist/images/windows.png" alt="" class="img-fluid" /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom mt-5 py-4">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 text-center text-md-right mb-3 mb-md-0">
                            <span> &copy; 2017 Lister. Powered by <a href="#" class="lis-primary">PSD2allconversion.</a></span>
                        </div> 
                        <div class="col-12 col-md-6 text-center text-md-left">
                            <ul class="list-inline footer-social mb-0">
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-tumblr"></i></a></li>
                                <li class="list-inline-item"><A href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>
        </section>
        
        <!--End  Footer-->

        <!-- Top To Bottom-->
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50"> 
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>


        <!-- End Top To Bottom-->

        <!-- Login /Register Form-->
        <div class="container">


            <div id="modal" class="popupContainer" style="display: none;">
                <header class="popupHeader">
                    <span class="header_title">Login</span>
                    <span class="modal_close"><i class="fa fa-times"></i></span>
                </header>

                <div class="popupBody">
                    <!-- Social Login -->						

                    <!-- Username & Password Login form -->
                    <div class="user_login">
                        <form>

                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " placeholder="username or email address" />
                            <br />


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " placeholder="password" />
                            <br />

                            <div class="checkbox">
                                <input id="remember" type="checkbox" />
                                <label for="remember">Remember me on this computer</label>
                            </div>

                            <div class="action_btns">

                                <a href="#" class="btn btn-primary btn-default mt-3 w-100">Login</a>
                            </div>
                        </form>
                        <br/>
                        Sign in with your social network<br/>
                        <ul class="list-inline my-0">
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                        <hr/>
                        Don't have an account <a href="#" class="register_form">Sign Up</a>
                    </div>

                    <!-- Register Form -->
                    <div class="user_register">
                        <form>

                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Username" />
                            <br />


                            <input type="email" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Email Address" />
                            <br />


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Password" />
                            <br />

                            <div class="checkbox">
                                <input id="send_updates" type="checkbox" />
                                <label for="send_updates">Send me occasional email updates</label>
                            </div>

                            <div class="action_btns">
                                <a href="#" class="btn btn-primary btn-default mt-3 w-100">Register</a>
                            </div>
                        </form>
                        <br/>
                        Register with your social network<br/>
                        <ul class="list-inline my-0">
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                        <hr/>
                        Already have an account <a href="#" class="login_form">Sign In</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Login /Register Form-->


        <style>
            
            .classMenuAutocomplete {
                padding: 10px;
            }
            
        </style>
        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>
        <script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
        
        <script >
            $(document).ready(function() { 
                var xhr;
                $('input[name="autocomplete"]').autoComplete({
                    minChars: 2,
                    source: function(term, suggest){
                        
                        $.ajax({
                            url: "index.php?getAutoSearch=true&q="+$("#autocomplete").val(), 
                            dataType : "json",
                            success: function(result){
                                suggest(result);
                            }
                        });
                        
                    },
                    renderItem: function (item, search){
                        return '<div class="autocomplete-suggestion" data-langname="'+item["tag_en"]+'" data-key="'+item["key"]+'" data-tipo="'+item["tipo"]+'" data-val="'+search+'">'+item["tag_en"]+'</div>';
                    },
                    onSelect: function(e, term, item){
                        console.log(item.data("langname"));
                        console.log(item.data("key"));
                        console.log(item.data("tipo"));
                    
                    }
                });
                    
                    
            }); 
            
        </script>
    </body>

<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/lister/rtl/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Mar 2019 21:49:26 GMT -->
</html>