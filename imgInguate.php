<?php  
       
$strSrcImagen = isset($_GET["src"]) ? trim($_GET["src"]) : "";
$strTipo = isset($_GET["up"]) ? trim($_GET["up"]) : "up";

$strSrcImagen = str_replace(" ", "+", $strSrcImagen);
if( isset($strSrcImagen) && file_exists($strSrcImagen) ){
    
                                              
    $content = file_get_contents($strSrcImagen); 
    //$strContentType = mime_content_type($strSrcImagen);
    
    $strContentType = getTypeFile(pathinfo($strSrcImagen, PATHINFO_EXTENSION));
    
    header("Content-Type: ".$strContentType);
    // Muestra la imagen
    print $content;
    
    die();
        
}
else{
    
    fntSinImagen($strTipo);
    
}

function getTypeFile($strExtencion){
    
    $arrType["png"] = "image/png";
    $arrType["jpg"] = "image/jpg";
    $arrType["jpe"] = "image/jpe";
    $arrType["webp"] = "image/webp";
    $arrType["jpeg"] = "image/jpeg";
    $arrType["pdf"] = "application/pdf";
    
    return $arrType[$strExtencion]; 
    
}



function fntSinImagen($strTipo = ""){
    
    $strSrc = "images/Noimagee.jpg?1";
    
    if( $strTipo == "up" )
        $strSrc = "images/Noimagee.jpg?1";
    
    $content = file_get_contents($strSrc); 
    $strContentType = getTypeFile(pathinfo($strSrc, PATHINFO_EXTENSION)); 
    header("Content-Type: ".$strContentType);    
    header("Content-Disposition: inline; filename=sin_imagen");                         
    print $content;
    die();
    
}
?>