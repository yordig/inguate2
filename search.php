<?php
include "core/function_servicio.php";

$pos = strpos($_SERVER["HTTP_USER_AGENT"], "Android");
    
$boolMovil = $pos === false ? false : true;

if( !$boolMovil ){
    
    $pos = strpos($_SERVER["HTTP_USER_AGENT"], "IOS");

    $boolMovil = $pos === false ? false : true;

}

$intOrigen = isset($_GET["o"]) ? intval($_GET["o"]) : 0;
$intKey = isset($_GET["key"]) ? fntCoreDecrypt($_GET["key"]) : 0;
$intTipo = isset($_GET["t"]) ? $_GET["t"] : 0;
$strTexto = isset($_GET["q"]) ? trim($_GET["q"]) : 0;
$strRango = isset($_GET["ran"]) ? trim($_GET["ran"]) : 0;
$strFiltro = isset($_GET["fil_tipo"]) ? trim($_GET["fil_tipo"]) : 0;
$strFiltroKey = isset($_GET["fil_key"]) ? trim($_GET["fil_key"]) : 0;
$strLenguaje = isset($_GET["len"]) ? trim($_GET["len"]) : "es";

define("lang", fntGetDiccionarioInternoIdioma($strLenguaje) );

if( isset($_GET["drawCotenido"]) ){
    
    if( $intTipo == 1 ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $arrProducto = array();
        
        $strQuery = "SELECT *
                     FROM   clasificacion
                     WHERE  clasificacion.id_clasificacion = {$intKey} ";
                     
        $qTMP = $objDBClass->db_consulta($strQuery);
        $arrClasificacion = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        $strQuery = "SELECT id_clasificacion_padre,
                            nombre,
                            tag_en,
                            tag_es
                     FROM   clasificacion_padre
                     WHERE  id_clasificacion = {$intKey}
                     
                     ";
        $strKeyClasificacionPadreFiltro = "";
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            
            $strKeyClasificacionPadreFiltro .= ( $strKeyClasificacionPadreFiltro == "" ? "" : "," ).$rTMP["id_clasificacion_padre"];
                                    
        }
        $objDBClass->db_free_result($qTMP);
        
        $strFiltroRangoPrecio = "";
        
        if( $strRango == "1_B" ){
            
            $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arrClasificacion["bajo_min"]}
                                     AND    place_producto.precio <= {$arrClasificacion["bajo_max"]}     ";
            
        }
        
        if( $strRango == "1_M" || $strRango == "2_M" || $strRango == "3_M" ){
            
            $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arrClasificacion["medio_min"]}
                                     AND    place_producto.precio <= {$arrClasificacion["medio_max"]}     ";
            
        }
        
        if( $strRango == "1_A" ){
            
            $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arrClasificacion["alto_min"]}
                                     AND    place_producto.precio <= {$arrClasificacion["alto_max"]}     ";
            
        }     
        
        $strFiltroCategoria = !empty($strFiltroKey) ? "AND    place_producto.id_clasificacion_padre = {$strFiltroKey}" : "";
        $strFiltroCategoriaInicio = !empty($strFiltroKey) ? "" : "AND    place_producto.id_clasificacion_padre IN ({$strKeyClasificacionPadreFiltro})";
            
        $strFiltroUsuario = "";
        $strFiltroUsuario2 = "";
        if( sesion["logIn"] ){
            
            $intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
            $strFiltroUsuario = " , usuario_place_producto_favorito.id_usuario_producto_favorito ";
            $strFiltroUsuario2 = " LEFT JOIN usuario_place_producto_favorito 
                                       ON   usuario_place_producto_favorito.id_place_producto = place_producto.id_place_producto
                                       AND  usuario_place_producto_favorito.id_usuario = {$intIdUsuario}   ";
        
                
        }
            
        $strQuery = "SELECT place.id_place,
                            place.titulo,
                            place.url_logo,
                            place_producto.id_place_producto,
                            place_producto.nombre,
                            place_producto.descripcion,
                            place_producto.precio,
                            place_producto.logo_url
                            {$strFiltroUsuario}
                     FROM   place_clasificacion,
                            place,
                            place_producto
                                {$strFiltroUsuario2}
                     WHERE  place_clasificacion.id_clasificacion = {$intKey}
                     AND    place_clasificacion.id_place = place.id_place
                     AND    place.estado = 'A'
                     AND    place.id_place = place_producto.id_place
                     
                     {$strFiltroCategoriaInicio}
                     {$strFiltroRangoPrecio}
                     {$strFiltroCategoria}
                     LIMIT 50
                     ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrProducto[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
            $arrProducto[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
            $arrProducto[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
            $arrProducto[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
            $arrProducto[$rTMP["id_place_producto"]]["id_place"] = $rTMP["id_place"];
            $arrProducto[$rTMP["id_place_producto"]]["titulo"] = $rTMP["titulo"];
            $arrProducto[$rTMP["id_place_producto"]]["url_logo"] = $rTMP["url_logo"];
            $arrProducto[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
            $arrProducto[$rTMP["id_place_producto"]]["favorito"] = "N";
            
            if( isset($rTMP["id_usuario_producto_favorito"]) && intval($rTMP["id_usuario_producto_favorito"]) )
                $arrProducto[$rTMP["id_place_producto"]]["favorito"] = "Y";
                        
        }
        $objDBClass->db_free_result($qTMP);
        
                     
        ?>
        <div class="row justify-content-center">
            <div class="col-lg-6 col-xs-12 " style="direction: rtl;">
                      
                <select id="slcRango" style="" onchange="fntSetParametroFiltro();" class="form-control text-center border-top-0 border-left-0 border-right-0 rounded-0" >

                    <?php
                    
                    if( $strRango == "2_M" || $strRango == "3_M" ){
                        ?>
                        
                        <option <?php print $strRango == "2_M" || $strRango == "3_M" ? "selected" : ""?> value="<?php print $strRango;?>"><?php print lang["medio"];?> Q<?php print number_format($arrClasificacion["medio_min"], 2)?> - Q<?php print number_format($arrClasificacion["medio_max"], 2)?></option>
                    
                        <?php
                    }
                    else{
                        ?>
                        <option class="text-center" <?php print $strRango == "1_B" ? "selected" : ""?> value="1_B"><?php print lang["bajo"];?> Q<?php print number_format($arrClasificacion["bajo_min"], 2)?> - Q<?php print number_format($arrClasificacion["bajo_max"], 2)?></option>
                        <option <?php print $strRango == "1_M" ? "selected" : ""?> value="1_M"><?php print lang["medio"];?> Q<?php print number_format($arrClasificacion["medio_min"], 2)?> - Q<?php print number_format($arrClasificacion["medio_max"], 2)?></option>
                        <option <?php print $strRango == "1_A" ? "selected" : ""?> value="1_A"><?php print lang["alto"];?> Q<?php print number_format($arrClasificacion["alto_min"], 2)?> - Q<?php print number_format($arrClasificacion["alto_max"], 2)?></option>
                    
                        <?php
                    }
                    
                    ?>
                    
                </select>
            </div>
           
        </div>
        <div class="row justify-content-center">
            <?php
            
            if( count($arrFiltroClasificacionPadre) > 0 ){
                
                ?>
                <div class="col-lg-12 col-xs-12 pt-1 text-center " style="direction: ltr;">
                    
                    <label class="custom-checkbox badge badge-light" style="font-size: 14px;">
                        All
                        <input class="rdCategoria" type="radio" <?php print empty($strFiltroKey) ? "checked" : ""?> name="rdCategoria" id="rdCategoria_0" value="">
                        <span class="radiobtn"></span>
                    </label> 
                    <?php
                    
                    $intCount = 1;
                    while( $rTMP = each($arrFiltroClasificacionPadre) ){
                        ?>
                        <label class="custom-checkbox badge badge-light" style="font-size: 14px;">
                            <?php print $rTMP["value"]["texto"]?>
                            <input class="rdCategoria" type="radio" <?php print $strFiltroKey == $rTMP["key"] ? "checked" : ""?> name="rdCategoria" id="rdCategoria_<?php print $rTMP["key"]?>" value="<?php print $rTMP["key"]?>">
                            <span class="radiobtn"></span>
                        </label>                                                   
                        
                        <?php
                        $intCount++;
                    }                            
                    
                    ?>
                        
                    
                </div>
                <?php
            }
            
            ?>
        </div>
        
        <div class="row justify-content-center pt-1 p-0 m-0 mb-3 wow fadeInUp">
                
            <div class="col-12 " id="" style="direction: ltr;">
                
                <?php
                
                if( count($arrProducto) > 0 ){
                      
                    fntDrawSearchTableProducto($arrProducto, $boolMovil);
                    
                }
                else{
                    ?>
                    <div class="alert alert-warning text-center" role="alert">
                      No results, Try again
                    </div>
                    <?php
                }
                ?>    
                
            </div>
        
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $(".rdCategoria").each(function (){
                    
                    $(this).change(function (){
                        
                        $("#hidFiltroTipo").val("C");
                        $("#hidFiltroKey").val($(this).val());
                
                        fntSetParametroFiltro()
                
                    });
                    
                });
            
            });
            
            function fntSetParametroFiltro(){
                       
                $("#hidRango").val($("#slcRango").val());
                
                fntShowContenido();
                
            }
            
        </script>  
        <?php
        
    } 
    
    if( $intTipo == 2 ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $arrProducto = array();
        $arrFiltroClasificacionPadre = array();
        
        $strQuery = "SELECT *
                     FROM   clasificacion_padre
                     WHERE  clasificacion_padre.id_clasificacion_padre = {$intKey} ";
                     
        $qTMP = $objDBClass->db_consulta($strQuery);
        $arTMPClasificacionPadre = $objDBClass->db_fetch_array($qTMP);
        $objDBClass->db_free_result($qTMP);
        
        $strFiltroRangoPrecio = "";
        if( $strRango == "1_B" ){
            
            $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arTMPClasificacionPadre["bajo_min"]}
                                     AND    place_producto.precio <= {$arTMPClasificacionPadre["bajo_max"]}     ";
            
        }
        
        if( $strRango == "1_M" || $strRango == "2_M" || $strRango == "3_M" ){
            
            $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arTMPClasificacionPadre["medio_min"]}
                                     AND    place_producto.precio <= {$arTMPClasificacionPadre["medio_max"]}    ";
            
        }
        
        if( $strRango == "1_A" ){
            
            $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arTMPClasificacionPadre["alto_min"]}
                                     AND    place_producto.precio <= {$arTMPClasificacionPadre["alto_max"]}     ";
            
        }
        
        $strFiltroCategoria = !empty($strFiltroKey) ? " INNER JOIN  place_producto_clasificacion_padre_hijo ON place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto" : "";
        $strFiltroCategoria2 = !empty($strFiltroKey) ? "AND    place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo = {$strFiltroKey}" : "";
            
        $strFiltroUsuario = "";
        $strFiltroUsuario2 = "";
        if( sesion["logIn"] ){
            
            $intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
            $strFiltroUsuario = " , usuario_place_producto_favorito.id_usuario_producto_favorito ";
            $strFiltroUsuario2 = " LEFT JOIN usuario_place_producto_favorito 
                                       ON   usuario_place_producto_favorito.id_place_producto = place_producto.id_place_producto
                                       AND  usuario_place_producto_favorito.id_usuario = {$intIdUsuario}   ";
        
                
        }
        
        $strQuery = "SELECT place.id_place,
                            place.titulo,
                            place.url_logo,
                            place_producto.id_place_producto,
                            place_producto.nombre,
                            place_producto.descripcion,
                            place_producto.precio,
                            place_producto.logo_url,
                            place_producto.id_clasificacion_padre
                            {$strFiltroUsuario}
                     FROM   place,
                            place_producto
                            {$strFiltroCategoria}
                            {$strFiltroUsuario2}
                     WHERE  place_producto.id_clasificacion_padre = {$intKey}
                     AND    place.id_place = place_producto.id_place
                     AND    place.estado = 'A'
                     {$strFiltroRangoPrecio}
                     {$strFiltroCategoria2}
                     LIMIT 50
                     ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrProducto[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
            $arrProducto[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
            $arrProducto[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
            $arrProducto[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
            $arrProducto[$rTMP["id_place_producto"]]["id_place"] = $rTMP["id_place"];
            $arrProducto[$rTMP["id_place_producto"]]["titulo"] = $rTMP["titulo"];
            $arrProducto[$rTMP["id_place_producto"]]["url_logo"] = $rTMP["url_logo"];
            $arrProducto[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
            
            $arrProducto[$rTMP["id_place_producto"]]["favorito"] = "N";
            
            if( isset($rTMP["id_usuario_producto_favorito"]) && intval($rTMP["id_usuario_producto_favorito"]) )
                $arrProducto[$rTMP["id_place_producto"]]["favorito"] = "Y";
             
                        
        }
        $objDBClass->db_free_result($qTMP);
        
        $strQuery = "SELECT id_clasificacion_padre_hijo,
                            nombre,
                            tag_en,
                            tag_es
                     FROM   clasificacion_padre_hijo
                     WHERE  id_clasificacion_padre =  {$intKey}
                     
                     ";
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre_hijo"]]["id_clasificacion_padre_hijo"] = $rTMP["id_clasificacion_padre_hijo"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre_hijo"]]["nombre"] = $rTMP["nombre"];
            $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre_hijo"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
                                
        }
        $objDBClass->db_free_result($qTMP);
            
           
        
        ?>
        
        
        <div class="row justify-content-center p-0 m-0">
            <div class="col-lg-6 col-xs-12 " style="direction: rtl;">
                      
                <select id="slcRango" style="" onchange="fntSetParametroFiltro();" class="form-control text-center border-top-0 border-left-0 border-right-0 rounded-0" >

                    <?php
                    
                    if( $strRango == "2_M" || $strRango == "3_M" ){
                        ?>
                        
                        <option <?php print $strRango == "2_M" || $strRango == "3_M" ? "selected" : ""?> value="<?php print $strRango;?>"><?php print lang["medio"];?> Q<?php print number_format($arTMPClasificacionPadre["medio_min"], 2)?> - Q<?php print number_format($arTMPClasificacionPadre["medio_max"], 2)?></option>
                    
                        <?php
                    }
                    else{
                        ?>
                        <option class="text-center" <?php print $strRango == "1_B" ? "selected" : ""?> value="1_B"><?php print lang["bajo"];?> Q<?php print number_format($arTMPClasificacionPadre["bajo_min"], 2)?> - Q<?php print number_format($arTMPClasificacionPadre["bajo_max"], 2)?></option>
                        <option <?php print $strRango == "1_M" ? "selected" : ""?> value="1_M"><?php print lang["medio"];?> Q<?php print number_format($arTMPClasificacionPadre["medio_min"], 2)?> - Q<?php print number_format($arTMPClasificacionPadre["medio_max"], 2)?></option>
                        <option <?php print $strRango == "1_A" ? "selected" : ""?> value="1_A"><?php print lang["alto"];?> Q<?php print number_format($arTMPClasificacionPadre["alto_min"], 2)?> - Q<?php print number_format($arTMPClasificacionPadre["alto_max"], 2)?></option>
                    
                        <?php
                    }
                    
                    ?>
                    
                </select>
            </div>
           
        </div>
        
        <div class="row justify-content-center p-0 m-0" style="direction: rtl;">
            <?php
            
            if( count($arrFiltroClasificacionPadre) > 0 ){
                ?>
                <div class="col-lg-12 col-xs-12 pt-1 text-center " style="direction: ltr;">
                    
                    <label class="custom-checkbox badge badge-light" style="font-size: 14px;">
                        All
                        <input class="rdCategoria" type="radio" <?php print empty($strFiltroKey) ? "checked" : ""?> name="rdCategoria" id="rdCategoria_0" value="">
                        <span class="radiobtn"></span>
                    </label> 
                    <?php
                    
                    $intCount = 1;
                    while( $rTMP = each($arrFiltroClasificacionPadre) ){
                        ?>
                        <label class="custom-checkbox badge badge-light" style="font-size: 14px;">
                            <?php print $rTMP["value"]["texto"]?>
                            <input class="rdCategoria" type="radio" <?php print $strFiltroKey == $rTMP["key"] ? "checked" : ""?> name="rdCategoria" id="rdCategoria_<?php print $rTMP["key"]?>" value="<?php print $rTMP["key"]?>">
                            <span class="radiobtn"></span>
                        </label>                                                   
                        
                        <?php
                        $intCount++;
                    }                            
                    
                    ?>
                        
                    
                </div>
                <?php
            }
            
            ?>
        </div>
        <div class="row justify-content-center p-0 m-0 wow fadeInUp">
                
            <div class="col-12  m-0 p-0 " id="" style="direction: ltr;">
                
                <?php
                
                if( count($arrProducto) > 0 ){
                        
                    fntDrawSearchTableProducto($arrProducto, $boolMovil);
                    
                }
                else{
                    ?>
                    <div class="alert alert-warning text-center" role="alert">
                      No results, Try again
                    </div>
                    <?php
                }
                ?>    
                
            </div>
        
        </div> 
        <script>
            
            $(document).ready(function() { 
                
                $(".rdCategoria").each(function (){
                    
                    $(this).change(function (){
                        
                        $("#hidFiltroTipo").val("C");
                        $("#hidFiltroKey").val($(this).val());
                
                        fntSetParametroFiltro()
                
                    });
                    
                });
            
            });
            
            function fntSetParametroFiltro(){
                       
                $("#hidRango").val($("#slcRango").val());
                
                fntShowContenido();
                
            }
            
        </script>
        <?php             
        
    } 
    
    if( $intTipo == 3 ){
        
        include "core/dbClass.php";
        $objDBClass = new dbClass();
        
        $arrProducto = array();
            
        //Agrupacion para calsificacion padre hijo
        $strQuery = "SELECT id_clasificacion_padre_hijo,
                            nombre,
                            tag_en,
                            tag_es,
                            tipo_rango_precio,
                            bajo_min,
                            bajo_max,
                            medio_min,
                            medio_max,
                            alto_min,
                            alto_max
                     FROM   clasificacion_padre_hijo
                     WHERE  tipo_rango_precio IN (1,2,3)
                     AND    nombre = '{$intKey}'
                     ORDER BY nombre
                     ";   
        $arrClasificacionPadreHijo = array();
        $arrAgrupacion = array();
        $qTMP = $objDBClass->db_consulta($strQuery);
        while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
            
            $arrClasificacionPadreHijo[$rTMP["id_clasificacion_padre_hijo"]] = $rTMP["id_clasificacion_padre_hijo"];
            
            $arrAgrupacion[$rTMP["nombre"]]["tag"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
            
            $arrTMP = array();
            
            if( $rTMP["bajo_min"] > 0 ) array_push($arrTMP, $rTMP["bajo_min"]);
            if( $rTMP["bajo_max"] > 0 ) array_push($arrTMP, $rTMP["bajo_max"]);
            if( $rTMP["medio_min"] > 0 ) array_push($arrTMP, $rTMP["medio_min"]);
            if( $rTMP["medio_max"] > 0 ) array_push($arrTMP, $rTMP["medio_max"]);
            if( $rTMP["alto_min"] > 0 ) array_push($arrTMP, $rTMP["alto_min"]);
            if( $rTMP["alto_max"] > 0 ) array_push($arrTMP, $rTMP["alto_max"]);
            
            $sinMaximoColumna = max($arrTMP);
            $sinMinimoColumna = min($arrTMP);
                
            if( !isset($arrAgrupacion[$rTMP["nombre"]]["min"]) )
                $arrAgrupacion[$rTMP["nombre"]]["min"] = $sinMinimoColumna;
                
            if( $sinMinimoColumna <= $arrAgrupacion[$rTMP["nombre"]]["min"] )
                $arrAgrupacion[$rTMP["nombre"]]["min"] = $sinMinimoColumna;
                
            if( !isset($arrAgrupacion[$rTMP["nombre"]]["max"]) )
                $arrAgrupacion[$rTMP["nombre"]]["max"] = $sinMaximoColumna;
                
            if( $sinMaximoColumna >= $arrAgrupacion[$rTMP["nombre"]]["max"] )
                $arrAgrupacion[$rTMP["nombre"]]["max"] = $sinMaximoColumna;
                
        }
        $objDBClass->db_free_result($qTMP);
        
        if( count($arrAgrupacion) > 0 && count($arrClasificacionPadreHijo) > 0 ){
            
            $arrRango = array();
            while( $rTMP = each($arrAgrupacion) ){
                
                $sinDiferencia = $rTMP["value"]["max"] - $rTMP["value"]["min"];
                
                if( $sinDiferencia > 15 ){
                    
                    $intRango = $sinDiferencia / 3;
                    $intRango = intval($intRango) ;
                    
                    $arrAgrupacion[$rTMP["key"]]["tipo"] = 1; 
                                                                                       
                    $arrAgrupacion[$rTMP["key"]]["bajo"]["min"] = $rTMP["value"]["min"]; 
                    $arrAgrupacion[$rTMP["key"]]["bajo"]["max"] = $rTMP["value"]["min"] + $intRango; 
                               
                    $arrAgrupacion[$rTMP["key"]]["medio"]["min"] = $rTMP["value"]["min"] + $intRango + 1; 
                    $arrAgrupacion[$rTMP["key"]]["medio"]["max"] = $rTMP["value"]["min"] + ( $intRango * 2 ); 
                                    
                    $arrAgrupacion[$rTMP["key"]]["alto"]["min"] = $rTMP["value"]["min"] + ( $intRango * 2 ) + 1; 
                    $arrAgrupacion[$rTMP["key"]]["alto"]["max"] = $rTMP["value"]["max"]; 
                    
                    $arrRango["bajo_min"] = $rTMP["value"]["min"]; 
                    $arrRango["bajo_max"] = $rTMP["value"]["min"] + $intRango; 
                    
                    $arrRango["medio_min"] = $rTMP["value"]["min"] + $intRango + 1; 
                    $arrRango["medio_max"] = $rTMP["value"]["min"] + ( $intRango * 2 ); 
                    
                    $arrRango["alto_min"] = $rTMP["value"]["min"] + ( $intRango * 2 ) + 1; 
                    $arrRango["alto_max"] = $rTMP["value"]["max"]; 
                                    
                }   
                else{
                    
                    $arrAgrupacion[$rTMP["key"]]["tipo"] = $rTMP["value"]["min"] != $rTMP["value"]["max"] ? 2 : 3;
                    
                    $arrAgrupacion[$rTMP["key"]]["bajo"]["min"] = 0;
                    $arrAgrupacion[$rTMP["key"]]["bajo"]["max"] = 0;
                               
                    $arrAgrupacion[$rTMP["key"]]["medio"]["min"] = $rTMP["value"]["min"];
                    $arrAgrupacion[$rTMP["key"]]["medio"]["max"] = $rTMP["value"]["max"];
                                    
                    $arrAgrupacion[$rTMP["key"]]["alto"]["min"] = 0;
                    $arrAgrupacion[$rTMP["key"]]["alto"]["max"] = 0;
                    
                    $arrRango["bajo_min"] = 0; 
                    $arrRango["bajo_max"] = 0; 
                    
                    $arrRango["medio_min"] = $rTMP["value"]["min"]; 
                    $arrRango["medio_max"] = $rTMP["value"]["max"]; 
                    
                    $arrRango["alto_min"] = 0; 
                    $arrRango["alto_max"] = 0;
                    
                }                     
                
            }  
            
            $strFiltroRangoPrecio = "";
            if( $strRango == "1_B" ){
                
                $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arrRango["bajo_min"]}
                                         AND    place_producto.precio <= {$arrRango["bajo_max"]}     ";
                
            }
            
            if( $strRango == "1_M" || $strRango == "2_M" || $strRango == "3_M" ){
                
                $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arrRango["medio_min"]}
                                         AND    place_producto.precio <= {$arrRango["medio_max"]}    ";
                
            }
            
            if( $strRango == "1_A" ){
                
                $strFiltroRangoPrecio = "AND    place_producto.precio >= {$arrRango["alto_min"]}
                                         AND    place_producto.precio <= {$arrRango["alto_max"]}     ";
                
            }
            
            $strClasificacionPadreHijo = implode(",", $arrClasificacionPadreHijo);
            
            $strFiltroCategoria = !empty($strFiltroKey) ? "AND    place_producto.id_clasificacion_padre = {$strFiltroKey}" : "";
            
            $strFiltroUsuario = "";
            $strFiltroUsuario2 = "";
            if( sesion["logIn"] ){
                
                $intIdUsuario = fntCoreDecrypt(sesion["webToken"]);
                $strFiltroUsuario = " , usuario_place_producto_favorito.id_usuario_producto_favorito ";
                $strFiltroUsuario2 = " LEFT JOIN usuario_place_producto_favorito 
                                           ON   usuario_place_producto_favorito.id_place_producto = place_producto.id_place_producto
                                           AND  usuario_place_producto_favorito.id_usuario = {$intIdUsuario}   ";
            
                    
            }                    
            
            $strQuery = "SELECT place.id_place,
                                place.titulo,
                                place.url_logo,
                                place_producto.id_place_producto,
                                place_producto.nombre,
                                place_producto.descripcion,
                                place_producto.precio,
                                place_producto.logo_url,
                                place_producto.id_clasificacion_padre
                                {$strFiltroUsuario}
                         FROM   place_producto_clasificacion_padre_hijo,
                                place,
                                place_producto
                                {$strFiltroUsuario2}
                         WHERE  place_producto_clasificacion_padre_hijo.id_clasificacion_padre_hijo IN ({$strClasificacionPadreHijo})
                         AND    place_producto_clasificacion_padre_hijo.id_place_producto = place_producto.id_place_producto
                         AND    place.id_place = place_producto.id_place
                         AND    place.estado = 'A'
                         {$strFiltroRangoPrecio}
                         LIMIT 50
                         ";
            $arrClasificacionPadre = array();
            $qTMP = $objDBClass->db_consulta($strQuery);
            while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                
                $arrClasificacionPadre[$rTMP["id_clasificacion_padre"]] = $rTMP["id_clasificacion_padre"];   
                
                if( !empty($strFiltroKey) && $strFiltroKey != $rTMP["id_clasificacion_padre"] )
                    continue;
                
                $arrProducto[$rTMP["id_place_producto"]]["id_place_producto"] = $rTMP["id_place_producto"];
                $arrProducto[$rTMP["id_place_producto"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
                $arrProducto[$rTMP["id_place_producto"]]["nombre"] = $rTMP["nombre"];
                $arrProducto[$rTMP["id_place_producto"]]["descripcion"] = $rTMP["descripcion"];
                $arrProducto[$rTMP["id_place_producto"]]["precio"] = $rTMP["precio"];
                $arrProducto[$rTMP["id_place_producto"]]["logo_url"] = $rTMP["logo_url"];
                $arrProducto[$rTMP["id_place_producto"]]["id_place"] = $rTMP["id_place"];
                $arrProducto[$rTMP["id_place_producto"]]["titulo"] = $rTMP["titulo"];
                $arrProducto[$rTMP["id_place_producto"]]["url_logo"] = $rTMP["url_logo"];
                
                $arrProducto[$rTMP["id_place_producto"]]["favorito"] = "N";
                
                if( isset($rTMP["id_usuario_producto_favorito"]) && intval($rTMP["id_usuario_producto_favorito"]) )
                    $arrProducto[$rTMP["id_place_producto"]]["favorito"] = "Y";
                
                            
            }
            $objDBClass->db_free_result($qTMP);
            
            $arrFiltroClasificacionPadre = array();
            
            if( count($arrClasificacionPadre) > 0 ){
                
                $strClasificacionPadre = implode(",", $arrClasificacionPadre);
                
                $strQuery = "SELECT id_clasificacion_padre,
                                    nombre,
                                    tag_en,
                                    tag_es
                             FROM   clasificacion_padre
                             WHERE  id_clasificacion_padre IN({$strClasificacionPadre})
                             
                             ";
                $qTMP = $objDBClass->db_consulta($strQuery);
                while( $rTMP = $objDBClass->db_fetch_array($qTMP) ){
                    
                    $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["id_clasificacion_padre"] = $rTMP["id_clasificacion_padre"];
                    $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["nombre"] = $rTMP["nombre"];
                    $arrFiltroClasificacionPadre[$rTMP["id_clasificacion_padre"]]["texto"] = $strLenguaje == "es" ? $rTMP["tag_es"] : $rTMP["tag_en"];
                                        
                }
                $objDBClass->db_free_result($qTMP);
                
                
            }
                             
        }
        
        ?>
        <div class="row justify-content-center">
            <div class="col-lg-6 col-xs-12 " style="direction: rtl;">
                         
                <select id="slcRango" style="" onchange="fntSetParametroFiltro();" class="form-control text-center border-top-0 border-left-0 border-right-0 rounded-0" >

                    <?php
                    
                    if( $strRango == "2_M" || $strRango == "3_M" ){
                        ?>
                        
                        <option <?php print $strRango == "2_M" || $strRango == "3_M" ? "selected" : ""?> value="<?php print $strRango;?>"><?php print lang["medio"];?> Q<?php print number_format($arrRango["medio_min"], 2)?> - Q<?php print number_format($arrRango["medio_max"], 2)?></option>
                    
                        <?php
                    }
                    else{
                        ?>
                        <option class="text-center" <?php print $strRango == "1_B" ? "selected" : ""?> value="1_B"><?php print lang["bajo"];?> Q<?php print number_format($arrRango["bajo_min"], 2)?> - Q<?php print number_format($arrRango["bajo_max"], 2)?></option>
                        <option <?php print $strRango == "1_M" ? "selected" : ""?> value="1_M"><?php print lang["medio"];?> Q<?php print number_format($arrRango["medio_min"], 2)?> - Q<?php print number_format($arrRango["medio_max"], 2)?></option>
                        <option <?php print $strRango == "1_A" ? "selected" : ""?> value="1_A"><?php print lang["alto"];?> Q<?php print number_format($arrRango["alto_min"], 2)?> - Q<?php print number_format($arrRango["alto_max"], 2)?></option>
                    
                        <?php
                    }
                    
                    ?>
                    
                </select>
            </div>
           
        </div>
        <div class="row justify-content-center">
            <?php
            
            if( count($arrFiltroClasificacionPadre) > 0 ){
                ?>
                <div class="col-lg-12 col-xs-12 pt-1 text-center " style="direction: ltr;">
                    
                    <label class="custom-checkbox badge badge-light" style="font-size: 14px;">
                        All
                        <input class="rdCategoria" type="radio" <?php print empty($strFiltroKey) ? "checked" : ""?> name="rdCategoria" id="rdCategoria_0" value="">
                        <span class="radiobtn"></span>
                    </label> 
                    <?php
                    
                    $intCount = 1;
                    while( $rTMP = each($arrFiltroClasificacionPadre) ){
                        ?>
                        <label class="custom-checkbox badge badge-light" style="font-size: 14px;">
                            <?php print $rTMP["value"]["nombre"]?>
                            <input class="rdCategoria" type="radio" <?php print $strFiltroKey == $rTMP["key"] ? "checked" : ""?> name="rdCategoria" id="rdCategoria_<?php print $rTMP["key"]?>" value="<?php print $rTMP["key"]?>">
                            <span class="radiobtn"></span>
                        </label>                                                   
                        
                        <?php
                        $intCount++;
                    }                            
                    
                    ?>
                        
                    
                </div>
                <?php
            }
            
            ?>
        </div>
        <div class="row justify-content-center pt-1 p-0 m-0 mb-3 wow fadeInUp">
                
            <div class="col-12 " id="" style="direction: ltr;">
                
                <?php
                
                if( count($arrProducto) > 0 ){
                        
                    fntDrawSearchTableProducto($arrProducto, $boolMovil);
                    
                }
                else{
                    drawdebug("No hay Resultados");
                }
                ?>    
                
            </div>
        
        </div>
        <script>
            
            $(document).ready(function() { 
                
                $(".rdCategoria").each(function (){
                    
                    $(this).change(function (){
                        
                        $("#hidFiltroTipo").val("C");
                        $("#hidFiltroKey").val($(this).val());
                
                        fntSetParametroFiltro()
                
                    });
                    
                });
            
            });
            
            function fntSetParametroFiltro(){
                       
                $("#hidRango").val($("#slcRango").val());
                
                fntShowContenido();
                
            }
            
        </script>
        <?php
    } 
    
    die();   
    
}

fntDrawHeaderPublico($boolMovil, true);
?>
<link href="dist_interno/select2/css/select2.min.css" rel="stylesheet" />
<link href="dist_interno/select2/css/select2-bootstrap4.min.css" rel="stylesheet"> 
<link href="dist/autocomplete/jquery.auto-complete.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="dist_interno/DataTables/datatables.min.css"/>


<input type="hidden" id="hidOrigen" value="<?php print $intOrigen;?>">
<input type="hidden" id="hidKey" value="<?php print fntCoreEncrypt($intKey);?>">
<input type="hidden" id="hidTipo" value="<?php print $intTipo;?>">
<input type="hidden" id="hidTexto" value="<?php print $strTexto;?>">
<input type="hidden" id="hidRango" value="<?php print $strRango;?>">
<input type="hidden" id="hidFiltroTipo" value="">   
<input type="hidden" id="hidFiltroKey" value="">

<br>
<div class="row justify-content-center  p-0 m-0" >
    <div class="col-xs-5 col-lg-7" style="margin-top: 60px; direction: ltr;">
        <table style="width: 100%;">
            <tr>
                <td> 
                    <div class="form-group m-0 p-0">
                        <div class="lis-search">
                            <i class="fa fa-search lis-primary"></i> 
                        </div>
                        <input type="text" value="<?php print $strTexto?>" id="txtAutocomplete" name="txtAutocomplete" class="form-control text-center border-top-0 border-left-0 border-right-0 rounded-0 " placeholder="What are you looking for?" />
                         
                    </div>
                </td>
                <td style=" <?php print $boolMovil ? "width: 20%;" : "width: 7%;"?> direction: rtl;">
                    <div class="form-group pr-2 m-0 p-0">
                    
                        <select onchange="fntShowContenido();" id="slcLenguaje" name="slcLenguaje" class="form-control text-center border-top-0 border-left-0 border-right-0 rounded-0 ">
                            <option <?php print $strLenguaje == "en" ? "selected" : ""?> value="en">en</option>
                            <option <?php print $strLenguaje == "es" ? "selected" : ""?> value="es" selected>es</option>
                        </select>
                    </div>
                </td>
            </tr>
        </table>
        
    </div>
    
</div>
<div  id="divContendo">
</div>

<section class="image-bg footer lis-grediant grediant-bt pb-0">
    <div class="background-image-maker"></div>
    <div class="holder-image">
        <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none">
    </div>
    <div class="container">
        <div class="row pb-5">
            <div class="col-12 col-md-8">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <h5 class="footer-head">Useful Links</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                        <h5 class="footer-head">My Account</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Dashboard</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Profile</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> My Listing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Favorites</A></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                        <h5 class="footer-head">Pages</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3">
                        <h5 class="footer-head">Help</h5>
                        <ul class="list-unstyled footer-links lis-line-height-2_5">
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A></li>
                            <li><A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="footer-logo">
                    <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a> 
                </div>
                <p class="my-4">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu in  felis eu pede mollis enim.</p>
                <a href="#" class="text-white"><u>App Download</u></a> 
                <ul class="list-inline mb-0 mt-2">
                    <li class="list-inline-item"><A href="#"><img src="dist/images/play-store.png" alt="" class="img-fluid" /></a></li>
                    <li class="list-inline-item"><A href="#"><img src="dist/images/google-play.png" alt="" class="img-fluid" /></a></li>
                    <li class="list-inline-item"><A href="#"><img src="dist/images/windows.png" alt="" class="img-fluid" /></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom mt-5 py-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 text-center text-md-right mb-3 mb-md-0">
                    <span> &copy; 2017 Lister. Powered by <a href="#" class="lis-primary">PSD2allconversion.</a></span>
                </div> 
                <div class="col-12 col-md-6 text-center text-md-left">
                    <ul class="list-inline footer-social mb-0">
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="list-inline-item pl-3"><A href="#"><i class="fa fa-tumblr"></i></a></li>
                        <li class="list-inline-item"><A href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</section>

<a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50"> 
    <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
</a> 

<script src="dist/autocomplete/jquery.auto-complete.min.js"></script>
<script type="text/javascript" src="dist_interno/DataTables/datatables.min.js"></script>

<script >

    $(document).ready(function() { 
        
        var xhr;
        $('input[name="txtAutocomplete"]').autoComplete({
            minChars: 2,
            source: function(term, suggest){
                
                try {
                    xhr.abort();
                }
                catch(error) {
                }

                xhr = $.ajax({
                    url: "index.php?getAutoSearch=true&q="+$("#txtAutocomplete").val()+"&len="+$("#slcLenguaje").val(), 
                    dataType : "json",
                    success: function(result){
                        suggest(result);
                    }
                });
                
            },
            renderItem: function (item, search){
                
                return '<div class="autocomplete-suggestion" data-texto-corto="'+item["texto_corto"]+'" data-texto="'+item["texto"]+'" data-identificador="'+item["identificador"]+'" data-tipo="'+item["tipo"]+'" data-rango="'+item["rango"]+'" data-val="'+search+'" style="direction: ltr;" >'+item["texto"]+'</div>';
            
            },
            onSelect: function(e, term, item){
                
                if( item.data("tipo") == "5" ){
                    
                    str = "place.php?p="+item.data("identificador")+"&len="+$("#slcLenguaje").val();
                    location.href = str;
                    return false;
                    
                }
                else if( item.data("tipo") == "4" ){
                    
                    str = "pro.php?iden="+item.data("identificador")+"&len="+$("#slcLenguaje").val();
                    location.href = str;
                    return false;
                    
                }
                else{
                    
                    str = "search.php?o=1&key="+item.data("identificador")+"&t="+item.data("tipo")+"&q="+item.data("texto-corto")+"&ran="+item.data("rango");
                    location.href = str;
                    
                }
                
            }
        }).keyup(function(e){
            
            if(e.keyCode == 13){
                
                str = "search.php?o=1&key=0&t=0&q="+this.value; 
                //location.href = str;  
                //window.open(str);
            
            }
            
        });
        
        var mySlider = $("#rangePrecio").bootstrapSlider();
        $('#rangePrecio').slider().on('slide', function () {
            
            arrRango = $('#rangePrecio').val().split(",");
            
            $("#spnRangoMin").html(arrRango[0]);
            $("#spnRangoMaz").html(arrRango[1]);
            
        });
        
        fntShowContenido();
        
    });
    
    function fntShowContenido(){
        
        $.ajax({                                                                                                                                                    
            url: "search.php?drawCotenido=true&o=2&key="+$("#hidKey").val()+"&t="+$("#hidTipo").val()+"&q="+$("#hidTexto").val()+"&ran="+$("#hidRango").val()+"&fil_tipo="+$("#hidFiltroTipo").val()+"&fil_key="+$("#hidFiltroKey").val()+"&len="+$("#slcLenguaje").val(), 
            success: function(result){
                
                $("#divContendo").html(result);
            
            }
        });
            
    } 
    
</script>
<?php
fntDrawFooterPublico();
?>